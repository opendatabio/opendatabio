<?php
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\TaxonList;
/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

// Landing page
Route::get('/', 'WelcomeController@index');

// Setting the locale:
Route::get('welcome/{locale}', 'WelcomeController@setAppLocale');
Route::get('home/{locale}', 'HomeController@setAppLocale');
// Home controller for logged in users
Route::get('/home', 'HomeController@index')->name('home');

// Default auth routes (login, logout, change password, etc)
Auth::routes();





Route::group(['middleware' => 'auth'], function() {
  
  // Users can update their own data
  Route::get('/token', 'Auth\SelfEditController@token');
  Route::post('/token', 'Auth\SelfEditController@resetToken');
  Route::get('/selfedit', 'Auth\SelfEditController@selfedit')->name('selfedit');
  Route::put('/selfupdate', 'Auth\SelfEditController@selfupdate')->name('selfupdate');

  /* IMPORT ROUTES */
  Route::post('import-locations', 'LocationController@importJob');
  Route::post('import-individuals', 'IndividualController@importJob');
  Route::post('import-individuallocations', 'IndividualController@importLocationsJob');
  Route::post('import-taxons', 'TaxonController@importJob');
  Route::post('import-vouchers', 'VoucherController@importJob');
  Route::post('import-traits', 'TraitController@importJob');
  Route::post('import-measurements', 'MeasurementController@importJob');
  Route::post('import-persons', 'PersonController@importJob');
  Route::post('import-biocollections', 'BiocollectionController@importJob');
  Route::get('import/{model}',function($model) {
    return view('common.import',compact('model'));
  });

  Route::get('individuals/identification-block','IndividualController@identificationBlock')->name('identificationblock');
  Route::post('activity/batch_delete', 'ActivityController@batchDelete');

  Route::get('persons/autocomplete', 'PersonController@autocomplete');
  Route::post('persons/batch_delete', 'PersonController@batchDelete');
  
  Route::post('userjobs/{userjob}/retry', 'UserJobController@retry');
  Route::post('userjobs/{userjob}/cancel', 'UserJobController@cancel');
  Route::post('userjobs/purge', 'UserJobController@purgeall');
  Route::get('userjobs-list', 'UserJobController@index')->name('userjobs.list');
  Route::resource('userjobs', 'UserJobController', ['only' => ['index', 'show', 'destroy']]);

  Route::get('references/autocomplete', 'BibReferenceController@autocomplete');

  Route::post('biocollections/checkih', 'BiocollectionController@checkih')->name('checkih');
  Route::get('biocollections/autocomplete','BiocollectionController@autocomplete');

  Route::post('odbrequests/{id}/annotate', 'RequestController@annotate');
  Route::get('odbrequests/{id}/biocollection', 'RequestController@indexBioCollection');
  Route::get('odbrequests/{id}/user', 'RequestController@indexUser');
  Route::post('odbrequests/individuals', 'RequestController@individualsRequest');
  Route::resource('odbrequests', 'RequestController',['only' => ['index', 'show', 'destroy']]);
  
  Route::get('dataset-version-create/{dataset_id}','\App\Http\Livewire\DatasetVersioning')->name('dataset.version.create');
  Route::get('dataset-version-edit/{dataset_version}','\App\Http\Livewire\DatasetVersioning')->name('dataset.version.edit');
  Route::get('trait-form/{odbtrait?}','\App\Http\Livewire\ODBTraitForm')->name('trait.form');
  Route::get('trait-unit-form/{trait_unit?}','\App\Http\Livewire\TraitUnitForm')->name('trait_unit.form');
  Route::get('measurement-form/{measurement?}','\App\Http\Livewire\MeasurementForm')->name('measurement.form');
  Route::get('measurement-form-object/{measured_id}/{measured_type}','\App\Http\Livewire\MeasurementForm')->name('measurement.form.object');


  Route::post('locations/individual_location', 'LocationController@saveForIndividual')->name('saveForIndividual');
  Route::post('locations/batch_delete', 'LocationController@batchDelete');

  Route::post('taxons/batch_delete', 'TaxonController@batchDelete');



Route::post('individuals/location-save', 'IndividualController@saveIndividualLocation')->name('saveIndividualLocation');
Route::post('references/findbibtexfromdoi', 'BibReferenceController@findBibtexFromDoi')->name('findbibtexfromdoi');
Route::get('individuals/location-delete', 'IndividualController@deleteIndividualLocation')->name('deleteIndividualLocation');
Route::post('individuals/batch_delete', 'IndividualController@batchDelete');
Route::post('individuals/batchidentify', 'IndividualController@batchidentifications');
Route::get('individuals/{id}/location/create', 'IndividualController@createLocations');
Route::get('individuals/create', 'IndividualController@create');
Route::post('odbrequest', 'RequestController@userRequest');
Route::get('vouchers/{id}/biocollection/create', 'VoucherController@createBioCollections');
Route::get('individuals/{id}/vouchers/create', 'VoucherController@createIndividuals');
Route::post('vouchers/batch_delete', 'VoucherController@batchDelete');
Route::get('traits/getformelement', 'TraitController@getFormElement');
Route::post('traits/batch_delete', 'TraitController@batchDelete');
Route::get('individuals/{id}/measurements/create', 'MeasurementController@createIndividuals');
Route::get('locations/{id}/measurements/create', 'MeasurementController@createLocations');
Route::get('taxons/{id}/measurements/create', 'MeasurementController@createTaxons');
Route::get('vouchers/{id}/measurements/create', 'MeasurementController@createVouchers');
Route::post('measurements/batch_delete', 'MeasurementController@batchDelete');



//PICTURES
//Batch upload pictures_files
Route::get('media/import-form', 'MediaController@uploadForm');
Route::post('import/media', 'MediaController@uploadSubmit');


Route::get('taxons/{id}/media-create', 'MediaController@createTaxons');
Route::get('locations/{id}/media-create', 'MediaController@createLocations');
Route::get('individuals/{id}/media-create','MediaController@createIndividuals');
Route::get('persons/{id}/media-create', 'MediaController@createMedia');
Route::get('traits/{id}/media-create', 'MediaController@createMedia');
Route::get('traits-categories/{id}/media-create', 'MediaController@createCategoryMedia');
Route::get('vouchers/{id}/media-create', 'MediaController@createVouchers');


/*
Route::get('forms/{id}/prepare', 'FormController@prepare');
Route::post('forms/{id}/fill', 'FormController@fill');
*/
Route::resource('forms', 'FormController',['only' => ['index']]);
Route::get('forms-edit/{form_id?}','\App\Http\Livewire\FormsEdit')->name('forms.edit');
Route::get('forms-show/{form_id}','\App\Http\Livewire\FormsShow')->name('forms.show');
Route::get('forms-fill/{form_id}','\App\Http\Livewire\FormsFill')->name('forms.fill');
Route::get('form-task-create/{form_id}','\App\Http\Livewire\FormTaskEdit')->name('form_tasks.create');
Route::get('form-task-edit/{form_task_id}','\App\Http\Livewire\FormTaskEdit')->name('form_tasks.edit');
Route::get('form-task/{form_task_id}','\App\Http\Livewire\FormTaskShow')->name('form_tasks.show');
Route::post('forms/batch_delete', 'FormController@batchDelete');
Route::get('forms-fill-task/{form_task_id}','\App\Http\Livewire\FormsFill')->name('forms.fill.task');


Route::post('exportdata', 'ExportController@exportData');
Route::get('individuals-identify','\App\Http\Livewire\IndividualsBatchIdentify')->name('individual.batchidentify');
Route::get('media-upload','\App\Http\Livewire\MediaUpload')->name('media.upload');


Route::get('individuals-job/{job_id}', 'IndividualController@indexUserJob')->name('individuals.index.userjob');
Route::get('vouchers-job/{job_id}', 'VoucherController@indexUserJob')->name('vouchers.index.userjob');
Route::get('traits-job/{job_id}', 'TraitController@indexUserJob')->name('traits.index.userjob');
Route::get('measurements-job/{job_id}', 'MeasurementController@indexUserJob')->name('measurements.index.userjob');
Route::get('persons-job/{job_id}', 'PersonController@indexUserJob')->name('persons.index.userjob');
Route::get('references-job/{job_id}', 'BibReferenceController@indexUserJob')->name('references.index.userjob');
Route::get('biocollections-job/{job_id}', 'BiocollectionController@indexUserJob')->name('biocollections.index.userjob');
Route::get('taxons-job/{job_id}', 'TaxonController@indexUserJob')->name('taxons.index.userjob');
Route::get('locations-job/{job_id}', 'LocationController@indexUserJob')->name('locations.index.userjob');



});



// Resources (with non-default BEFORE resources):
Route::get('persons/{id}/history', 'PersonController@activity');
Route::resource('persons', 'PersonController');


Route::get('references/{id}/history', 'BibReferenceController@activity');
Route::get('references/{id}/biocollection', 'BibReferenceController@indexBioCollection');
Route::resource('references', 'BibReferenceController');

Route::resource('biocollections', 'BiocollectionController');


Route::get('locations/{id}/location', 'LocationController@indexLocations');
Route::get('locations/{id}/activity', 'LocationController@activity');
Route::get('locations/{id}/project', 'LocationController@indexProjects');
Route::get('locations/{id}/dataset', 'LocationController@indexDatasets');
Route::get('locations/autocomplete', 'LocationController@autocomplete');
Route::get('locations/{id}/mapme', 'LocationController@mapMe');
Route::get('locations/{id}/mapspecies', 'LocationController@mapLocationSpecies');

Route::get('locations/autocomplete-related', 'LocationController@autocomplete_related');
Route::post('locations/autodetect', 'LocationController@autodetect')->name('autodetect');
//Route::post('locations/maprender', 'LocationController@maprender')->name('maprender');
Route::post('locations/map', 'LocationController@mapLocations');
Route::post('locations/custom-map', 'LocationController@mapCustom');
Route::resource('locations', 'LocationController');

Route::get('taxons/{id}/project', 'TaxonController@indexProjects');
Route::get('taxons/{id}/dataset', 'TaxonController@indexDatasets');
Route::get('taxons/{id}/activity', 'TaxonController@activity');
Route::get('taxons/{id}/bibreference', 'TaxonController@indexBibreferences');

Route::get('taxons/{id}/taxon', 'TaxonController@indexTaxons');
Route::get('taxons/{id}/taxon_project', 'TaxonController@indexTaxonsProjects');
Route::get('taxons/{id}/taxon_dataset', 'TaxonController@indexTaxonsDatasets');
Route::get('taxons/{id}/taxon_location', 'TaxonController@indexTaxonsLocations');
Route::get('taxons/{id}/location', 'TaxonController@indexLocations');
Route::get('taxons/{id}/location_project', 'TaxonController@indexLocationsProjects');
Route::get('taxons/{id}/location_dataset', 'TaxonController@indexLocationsDatasets');
Route::post('taxons/checkapis', 'TaxonController@checkapis')->name('checkapis');
Route::get('taxons/{id}/map', 'TaxonController@mapOccurrences');
Route::get('taxons/autocomplete', 'TaxonController@autocomplete');
Route::resource('taxons', 'TaxonController');




Route::get('projects/{id}/activity', 'ProjectController@activity');
Route::get('projects/autocomplete', 'ProjectController@autocomplete');
Route::post('projects/{id}/summary', 'ProjectController@summarize_project')->name('project_summary');
Route::post('projects/{id}/identifications-summary', 'ProjectController@summarize_identifications')->name('project_identification_summary');
Route::get('projects/{id}/tags','ProjectController@indexTags');
Route::get('projects/{id}/dataset','ProjectController@indexDatasets');
Route::post('projects/{id}/emailrequest', 'ProjectController@sendEmail');
Route::get('projects/{id}/request', 'ProjectController@projectRequestForm');
Route::resource('projects', 'ProjectController');
Route::get('project-show/{project}','\App\Http\Livewire\ProjectShow')->name('project.show');


Route::get('datasets/{id}/bibreference', 'DatasetController@indexBibreferences');
Route::get('datasets/{id}/project', 'DatasetController@indexProjects');
Route::get('datasets/autocomplete', 'DatasetController@autocomplete');
Route::get('datasets/{id}/download', 'DatasetController@prepDownloadFile');
Route::get('datasets/{id}/request', 'DatasetController@datasetRequestForm');
Route::get('datasets/{id}/activity','DatasetController@activity');
Route::get('datasets/{id}/tags','DatasetController@indexTags');
Route::post('datasets/map', 'DatasetController@mapDatasets');
Route::post('datasets/{id}/emailrequest', 'DatasetController@sendEmail');
Route::get('datasets/{id}/identifications-summary','DatasetController@summarize_identifications')->name('dataset_identification_summary');
Route::get('datasets/{id}/summary','DatasetController@summarize_contents')->name('dataset_summary');
Route::resource('datasets', 'DatasetController');



Route::get('individuals/location-show', 'IndividualController@getIndividualLocation')->name('getIndividualLocation');
Route::get('individuals/{id}/location-dataset', 'IndividualController@indexIndividualLocationsDataset');


Route::get('individuals/for-voucher', 'IndividualController@getIndividualForVoucher')->name('getIndividualForVoucher');
Route::get('individuals/autocomplete', 'IndividualController@autocomplete');
Route::get('individuals/{id}/dataset', 'IndividualController@indexDatasets');
Route::get('individuals/{id}/activity', 'IndividualController@activity');
Route::get('individuals/{id}/mapme', 'IndividualController@mapMe');
Route::get('individual-location/{id}/activity', 'IndividualController@locationActivity');
Route::get('individual-location/{id}', 'IndividualController@showLocation');

Route::post('individuals/map', 'IndividualController@mapOccurrences');
Route::post('individuals/filter', 'IndividualController@filterIndividuals');
Route::get('individuals/filtered/{params}', 'IndividualController@indexFiltered');

Route::post('vouchers/filter', 'VoucherController@filterVouchers');
Route::get('vouchers/filtered/{params}', 'VoucherController@indexFiltered');

Route::post('taxons/filter', 'TaxonController@filterTaxons');
Route::get('taxons/filtered/{params}', 'TaxonController@indexFiltered');



Route::get('individuals/{id}/location', 'IndividualController@indexLocations');
Route::get('individuals/{id}/location_project', 'IndividualController@indexLocationsProjects');
Route::get('individuals/{id}/location_dataset', 'IndividualController@indexLocationsDatasets');
Route::get('individuals/{id}/taxon', 'IndividualController@indexTaxons');
Route::get('individuals/{id}/taxon_project', 'IndividualController@indexTaxonsProjects');
Route::get('individuals/{id}/taxon_dataset', 'IndividualController@indexTaxonsDatasets');
Route::get('individuals/{id}/taxon_location', 'IndividualController@indexTaxonsLocations');
Route::get('individuals/{id}/project', 'IndividualController@indexProjects');
Route::get('persons/{id}/individuals', 'IndividualController@indexPersons');


Route::get('vouchers/{id}/biocollection', 'VoucherController@indexBioCollections');
Route::get('vouchers/{id}/bibreference', 'VoucherController@indexBibReferences');
Route::get('vouchers/{id}/dataset', 'VoucherController@indexDatasets');
Route::get('vouchers/{id}/activity', 'VoucherController@activity');

//Route::get('locations/{id}/vouchers/create', 'VoucherController@createLocations');
Route::get('locations/{id}/vouchers', 'VoucherController@indexLocations');
Route::get('individuals/{id}/vouchers', 'VoucherController@indexIndividuals');
Route::get('vouchers/{id}/taxon', 'VoucherController@indexTaxons');
Route::get('vouchers/{id}/taxon_project', 'VoucherController@indexTaxonsProjects');
Route::get('vouchers/{id}/taxon_dataset', 'VoucherController@indexTaxonsDatasets');
Route::get('vouchers/{id}/taxon_location', 'VoucherController@indexTaxonsLocations');
Route::get('vouchers/{id}/location', 'VoucherController@indexLocations');
Route::get('vouchers/{id}/location_project', 'VoucherController@indexLocationsProjects');
Route::get('vouchers/{id}/location_dataset', 'VoucherController@indexLocationsDatasets');
Route::get('vouchers/autocomplete', 'VoucherController@autocomplete');
Route::get('vouchers/{id}/project', 'VoucherController@indexProjects');
Route::get('persons/{id}/vouchers', 'VoucherController@indexPersons');

Route::post('vouchers/map', 'VoucherController@mapOccurrences');
Route::resource('vouchers', 'VoucherController');

Route::resource('tags', 'TagController');

Route::get('traits/{id}/activity', 'TraitController@activity');
Route::get('traits/autocomplete', 'TraitController@autocomplete');
Route::get('traits/{id}/unit', 'TraitController@indexUnits')->name('traits.of.unit');
Route::resource('traits', 'TraitController');

// Users can be resources for the admin
Route::get('users/autocomplete', 'UserController@autocomplete');
Route::get('users/autocomplete_all', 'UserController@autocompleteAll');
Route::resource('users', 'UserController');


// Measures use a somewhat complicated schema for routes?
Route::get('measurements/{id}/activity', 'MeasurementController@activity');
Route::get('measurements/{id}/individual', 'MeasurementController@indexIndividuals');
Route::get('measurements/{id}/individual_dataset', 'MeasurementController@indexIndividualsDatasets');
Route::get('measurements/check-gb', 'MeasurementController@checkGeneBank');

Route::get('locations/{id}/measurements', 'MeasurementController@indexLocations');
Route::get('locations/{id}/measurements_root', 'MeasurementController@indexLocationsRoot');
Route::get('measurements/{id}/taxon', 'MeasurementController@indexTaxon');
Route::get('measurements/{id}/taxon_project', 'MeasurementController@indexTaxonsProjects');
Route::get('measurements/{id}/taxon_dataset', 'MeasurementController@indexTaxonsDatasets');
Route::get('measurements/{id}/taxon_location', 'MeasurementController@indexTaxonsLocations');
Route::get('measurements/{id}/location', 'MeasurementController@indexLocations');
Route::get('measurements/{id}/location_project', 'MeasurementController@indexLocationsProjects');
Route::get('measurements/{id}/location_dataset', 'MeasurementController@indexLocationsDatasets');
Route::get('measurements/{id}/bibreference', 'MeasurementController@indexBibreferences');



Route::get('vouchers/{id}/measurements', 'MeasurementController@indexVouchers');
Route::get('measurements/{id}/dataset', 'MeasurementController@indexDatasets')->name('ajax.measurementdataset');
Route::get('measurements/{id}/trait', 'MeasurementController@indexTraits');
Route::get('measurements/{id}/media', 'MeasurementController@indexMedia');

Route::resource('measurements', 'MeasurementController', ['only' => ['show', 'store', 'edit', 'update','destroy','index']]);



//Media objects
Route::get('media/{id}/activity', 'MediaController@activity');
Route::get('media/{id}/taxons', 'MediaController@indexTaxons');
Route::get('media/{id}/locations', 'MediaController@indexLocations');
Route::get('media/{id}/individuals', 'MediaController@indexIndividuals');
Route::get('media/{id}/vouchers', 'MediaController@indexVouchers');
Route::get('media/{id}/datasets', 'MediaController@indexDatasets');
Route::resource('media', 'MediaController',['only' => ['show', 'edit', 'update', 'destroy','store']]);



//Route::get('persons/{id}/individuals', 'IndividualController@indexPersons');
Route::resource('individuals', 'IndividualController');



Route::get('dataset-version-show/{dataset_version}','\App\Http\Livewire\DatasetVersionShow')->name('dataset.version.show');
Route::get('dataset-uuid/{uuid}','\App\Http\Livewire\DatasetVersionShow')->name('dataset.version.uuid');


Route::get('dataset-show/{dataset}','\App\Http\Livewire\DatasetShow')->name('dataset.show');

Route::get('trait-units','\App\Http\Livewire\TraitUnitList')->name('trait_units.list');



Route::get('allmedia','\App\Http\Livewire\MediaGallery')->name('media.all');
Route::get('taxon-media/{taxon_id}','\App\Http\Livewire\MediaGallery')->name('media.taxon.list');
Route::get('individual-media/{individual_id}','\App\Http\Livewire\MediaGallery')->name('media.individual.list');
Route::get('tag-media/{tag_id}','\App\Http\Livewire\MediaGallery')->name('media.tag.list');
Route::get('voucher-media/{voucher_id}','\App\Http\Livewire\MediaGallery')->name('media.voucher.list');
Route::get('location-media/{location_id}','\App\Http\Livewire\MediaGallery')->name('media.location.list');
Route::get('project-media/{project_id}','\App\Http\Livewire\MediaGallery')->name('media.project.list');


//Route::get('map-dashboard','\App\Http\Livewire\MapDashboard')->name('map.dashboard');


//Route::get('taxonlivewire','\App\Http\Livewire\TaxonList')->name('taxon.list');
//Route::get('individual-list','\App\Http\Livewire\IndividualList')->name('individual.list');
//Route::get('dataset-list','\App\Http\Livewire\DatasetList')->name('dataset.list');