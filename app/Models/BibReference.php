<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use RenanBr\BibTexParser\Listener;
use RenanBr\BibTexParser\Parser;
use RenanBr\BibTexParser\ParseException;
use RenanBr\BibTexParser\Processor;
use CodeInc\StripAccents\StripAccents;

//use RenanBr\BibTexParser\Processor\LatexToUnicodeProcessor as LatexToUnicode;
//use RenanBr\BibTexParser\Exception\ProcessorException;
use Illuminate\Support\Str;
use App\Models\BibLatexTitleToUnicode;
//use Pandoc\Pandoc;
//use Pandoc\PandocException;

use DB;
use Illuminate\Support\Facades\Log;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class BibReference extends Model implements HasMedia
{
    use InteractsWithMedia, LogsActivity;

    // "cached" entries, so we don't need to parse the bibtex for every call
    protected $entries = null;
    protected $fillable = ['bibtex', 'doi'];

    //activity log trait
    //protected static $logName = 'bibreference';
    protected static $recordEvents = ['updated','deleted'];
    //protected static $ignoreChangedAttributes = ['updated_at'];
    //protected static $logFillable = true;
    //protected static $logOnlyDirty = true;
    //protected static $submitEmptyLogs = false;
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->useLogName('bibreference')
        ->logFillable()
        ->logOnlyDirty()
        ->dontLogIfAttributesChangedOnly(['updated_at'])
        ->dontSubmitEmptyLogs();      
    }


    public function rawLink()
    {
        return "<a href='".url('references/'.$this->id)."'>".htmlspecialchars($this->bibkey).'</a>';
    }

    public function datasets()
    {
      return $this->belongsToMany(Dataset::class,'dataset_bibreference')->withPivot(['mandatory']);
    }

    public function odbtraits()
    {
        return $this->belongsToMany(ODBTrait::class,'trait_bibreference','bibreference_id',
        'trait_id');
    }

    public function vouchers()
    {
      return $this->belongsToMany(Voucher::class,'voucher_bibreference')
      ->withTimestamps()->withoutGlobalScopes();
    }



    public function measurements()
    {
        return $this->hasMany(Measurement::class, 'bibreference_id')->withoutGlobalScopes();
    }

    public function taxons()
    {
        return $this->hasMany(Taxon::class, 'bibreference_id');
    }

    public function validBibtex($string)
    {
        $listener = new Listener();
        $parser = new Parser();
        $parser->addListener($listener);
        try {
            $parser->parseString($string);
        } catch (ParseException $e) {
            return false;
        }

        return true;
    }


    private function parseBibtex()
    {
        /* define modifications to be applied to the original record */
        $listener = new Listener();
        $listener->addProcessor(new Processor\TagNameCaseProcessor(CASE_LOWER));
        //$listener->addProcessor(new Processor\NamesProcessor());
        $listener->addProcessor(new Processor\KeywordsProcessor());
        $listener->addProcessor(new Processor\DateProcessor());
        $listener->addProcessor(new Processor\TrimProcessor());
        $listener->addProcessor(new Processor\UrlFromDoiProcessor());
        $listener->addProcessor(static function (array $entry) {
            $title = Str::title($entry['title']);
            $entry['title'] = $title;
            return $entry;
        });
        //this must be the last processor
        $listener->addProcessor(new BibLatexTitleToUnicode());

        /* apply the modifications */
        $parser = new Parser();
        $parser->addListener($listener);

        try {
            $parser->parseString($this->bibtex);
        } catch (ParseException $e) {
            Log::error('Error handling bibtex:'.$e->getMessage());
            $this->entries[0] = ['author' => null, 'title' => null, 'year' => null, 'citation-key' => 'Invalid'];
            return;
        }
        $this->entries = $listener->export();
    }

    public function getDoiAttribute()
    {
        if (isset($this->attributes['doi'])) {
            return $this->attributes['doi'];
        }

        // falls back to the bibtex "doi" field in case the database column is absent
        if (is_null($this->entries)) {
            $this->parseBibtex();
        }
        if (count($this->entries) > 0 and isset($this->entries[0]['doi'])) {
            return $this->entries[0]['doi'];
        } else {
            return '';
        }
    }

    public function getUrlAttribute()
    {
        // falls back to the bibtex "doi" field in case the database column is absent
        if (is_null($this->entries)) {
            $this->parseBibtex();
        }
        if (count($this->entries) > 0 and  isset($this->entries[0]['url'])) {
            return $this->entries[0]['url'];
        } else {
            return '';
        }
    }

    // NOTE, this may be called as "$newDoi = null" from a newly created resource to guess DOI from bibtex
    public function setDoi($newDoi)
    {
        // if receiving a blank and we have attr set, the user is probably trying to remove the information
        $olddoi = isset($this->entries[0]['doi']) ? $this->attributes['doi'] : null;
        if ($olddoi and  null == $newDoi) {
            $this->attributes['doi'] = null;
            return;
        }
        // if we are receiving something for $newDoi, use it
        if (!is_null($newDoi)) {
            $this->attributes['doi'] = $newDoi;
            return;
        }
        // else, guess it from the bibTex and fill it
        if (is_null($this->entries)) {
            $this->parseBibtex();
        }

        $entry = $this->entries[0];
        $hasdoi = isset($entry['doi']) ? $entry['doi'] : (isset($entry['DOI']) ? $entry['DOI'] : null );
        if (!is_null($hasdoi)) {
            //$this->attributes['doi'] = $hasdoi;
            $this->doi = $hasdoi;
            //$this->attributes['doi'] = $newDoi;
        }
    }

    public static function isValidDoi($doi)
    {
        // Regular expression adapted from https://www.crossref.org/blog/dois-and-matching-regular-expressions/
        if (1 == preg_match('/^10.\d{4,9}\/[-._;()\/:A-Z0-9]+$/i', $doi)) {
            return true;
        }

        return false;
    }

    public function getAuthorAttribute()
    {
        if (is_null($this->entries)) {
            $this->parseBibtex();
        }
        if (count($this->entries) > 0 and isset($this->entries[0]['author'])) {
            $author = $this->entries[0]['author'];
            $authors =  explode(" and ",mb_strtolower($author));
            $authors = collect($authors)->map(function($word) { return  Str::title($word);})->toArray();
            $authors = implode(" and ",$authors);
            return $authors;
        } else {
            return '';
        }
    }

    public function getFirstAuthorAttribute()
    {
        if (is_null($this->entries)) {
            $this->parseBibtex();
        }
        if (count($this->entries) > 0 and isset($this->entries[0]['author'])) {
            $author = $this->entries[0]['author'];
            $authors =  explode(" and ",$author);
            if (count($authors)>2) {
              return Str::title($authors[0])." et al.";
            } else {
              return Str::title($author);
            }
        } else {
            return '';
        }
    }

    public function identifiableName()
    {
        $name =  $this->getFirstAuthorAttribute()." ".$this->getYearAttribute();
        return "<a href='".url('references/'.$this->id)."'>".htmlspecialchars($name)."</a>";
    }


    public function getTitleAttribute()
    {
        if (is_null($this->entries)) {
            $this->parseBibtex();
        }
        if (count($this->entries) > 0 and isset($this->entries[0]['title'])) {
            return $this->entries[0]['title'];
        } else {
            return '';
        }
    }

    public function getYearAttribute()
    {
        if (is_null($this->entries)) {
            $this->parseBibtex();
        }
        if (count($this->entries) > 0 and isset($this->entries[0]['year'])) {
            return $this->entries[0]['year'];
        } else {
            return '';
        }
    }

    public function getParsedBibkeyAttribute()
    {
        if (is_null($this->entries)) {
            $this->parseBibtex();
        }
        if (count($this->entries) > 0 and isset($this->entries[0]['citation-key'])) {
            return $this->entries[0]['citation-key'];
        } else {
            return '';
        }
    }

    public function newQuery($excludeDeleted = true)
    {
        // includes the full name of a taxon in all queries
        return parent::newQuery($excludeDeleted)->addSelect(
            '*',
            DB::raw('odb_bibkey(bibtex) as bibkey')
        );
    }

    public function getBibreferenceSimpleAttribute($value='')
    {
      $result = [
        $this->first_author,$this->year,$this->title,(($this->doi!='') ? $this->doi : $this->url)
      ];
      $result = array_filter($result, function($r) { $r!='';});
      if (count($result)==0) {
        return null;
      }
      return implode(". ",$result).".";
    }

    //standardize the key
    public static function standardizeBibKey($entry,$standardize=true)
    {
      if ($standardize and isset($entry['title']) and isset($entry['author'])and isset($entry['year']))
          {
          //first title word
          $fword = trim(strtolower(strtok($entry['title'], ' ')));
          //remove special characters
          $fword = preg_replace('/[^a-zA-Z]/', '', $fword);
          while (in_array($fword, ['a', 'an', 'the', 'on', 'of', 'in', 'as', 'at', 'for', 'from', 'where', 'i', 'are', 'is', 'it', 'that', 'this']) or 1 == strlen($fword)) {
              $fword = strtok(' ');
          }
          //short author
          $author = $entry['author'][0]['von']
              .$entry['author'][0]['last'];
          //the standardized citation-key
          $citationKey =
              ucfirst($author).
              $entry['year'].
              strtolower($fword)
          ;
      } else {
        $citationKey = $entry['citation-key'];
      }
      if (isset($citationKey)) {
        //remove any not alpha characters from citation-key
        $citationKey = StripAccents::strip( (string) $citationKey);
        $citationKey = preg_replace('/[^a-zA-Z0-9]/', '', $citationKey);
        return $citationKey;
      }
      return null;
    }

    /* this function is used in Import jobs */
    /* import and/or return the ids of existing refs if already found */
    public static function importBibReference($contents,$standardize=true) {
          if ($contents==null) {
            return null;
          }
          $listener = new Listener();
          $listener->addProcessor(new Processor\TagNameCaseProcessor(CASE_LOWER));
          $listener->addProcessor(new Processor\NamesProcessor());
          $listener->addProcessor(new Processor\DateProcessor());
          $listener->addProcessor(new Processor\TrimProcessor());
          $parser = new Parser();
          $parser->addListener($listener);
          $parser->parseString($contents);
          $newentries = $listener->export();
          $bibids = [];
          $messages = [];
          foreach($newentries as $entry) {
              $citationKey = BibReference::standardizeBibKey($entry,$standardize);
              //original record with sanitized and/or standardized key
              $bibkeyAlreadyRegistered = self::whereRaw('odb_bibkey(bibtex) = ?', [$citationKey])->count()>0;
              if (isset($entry['doi'])) {
                $doiAlreadyRegistered = self::where('doi',$entry['doi'])->count()>0;
                /* doi is different but key exists, change key */
                if (!$doiAlreadyRegistered and $bibkeyAlreadyRegistered) {
                  $citationKey = $citationKey."_a";
                  $bibkeyAlreadyRegistered = self::whereRaw('odb_bibkey(bibtex) = ?', [$citationKey])->count()>0;
                }
              } else {
                $doiAlreadyRegistered = false;
              }

              if ($doiAlreadyRegistered) {
                  $bibids[] = self::where('doi',$entry['doi'])->first()->id;
                  $messages[] = 'WARNING: DOI '.$entry['doi'].' entry '.$citationKey.' not imported!';
              } elseif ($bibkeyAlreadyRegistered) {
                  $bibids[]= self::whereRaw('odb_bibkey(bibtex) = ?', [$citationKey])->first()->id;
                  $messages[] = 'WARNING: BibKey '.$citationKey.' exists, could not validate if same';
              } else {
                $text = preg_replace('/{(.*?),/', '{'.$citationKey.',', $entry['_original'], 1);
                $ref = self::create(['bibtex' => $text]);
                // guesses the DOI from the bibtex and saves it on the relevant database column
                $ref->setDoi(null);
                $ref->save();
                $bibids[] = $ref->id;
              }
           }
           $bibids = array_unique($bibids);
           return ['affected_ids' => $bibids, 'messages' => $messages];
    }

}
