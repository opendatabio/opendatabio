<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Models;

use Baum\Node;
use DB;
use Lang;
use Log;
use App\Models\Taxon;
use App\Models\Identification;
use App\Models\IndividualLocation;
use Illuminate\Support\Arr;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;


use \Colors\RandomColor;

use Spatie\MediaLibrary\MediaCollections\Models\Media as BaseMedia;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Activity;

class Location extends Node implements HasMedia
{
    use InteractsWithMedia, LogsActivity;

    public $table = "locations";
    //protected $appends = ['higher_geography'];

    // The "special" adm levels
    const LEVEL_UC = 99;
    const LEVEL_TI = 98;
    const LEVEL_ENV = 97;
    const LEVEL_PLOT = 100;
    const LEVEL_TRANSECT = 101;
    const LEVEL_POINT = 999;
    const LEVEL_SPECIAL = [
      self::LEVEL_UC,
      self::LEVEL_PLOT,
      self::LEVEL_POINT,
      self::LEVEL_TI,
      self::LEVEL_ENV,
      self::LEVEL_TRANSECT,
    ];
    // Valid geometries
    const GEOM_POINT = "POINT";
    const GEOM_POLYGON = "POLYGON";
    const GEOM_MULTIPOLYGON = "MULTIPOLYGON";
    const GEOM_LINESTRING = "LINESTRING";
    const VALID_GEOMETRIES = [
      self::GEOM_POINT,
      self::GEOM_POLYGON,
      self::GEOM_MULTIPOLYGON,
      self::GEOM_LINESTRING
    ];
    // "LineString","MultiLineString", "Polygon", "MultiPolygon"];

    protected $fillable = ['name', 'altitude', 'datum', 'adm_level', 'notes', 'x', 'y', 'startx', 'starty', 'parent_id','geojson','as_point',"country_code",'start_point', 'centroid_raw', 'geom_type', 'area_raw'];   
    protected $lat;
    protected $long;
    protected $footprintWKT;
    protected $geom_array = [];
    protected $isSimplified = false;
    protected $leftColumnName = 'lft';
    protected $rightColumnName = 'rgt';
    protected $depthColumnName = 'depth';

    //activity log trait (parent, uc and geometry are logged in controller)
    //protected static $logName = 'location';
    protected static $recordEvents = ['updated','deleted'];
    //protected static $ignoreChangedAttributes = ['updated_at','lft','rgt','depth','parent_id','geom'];
    //protected static $logAttributes = ['name','altitude','adm_level','datum','x','y','startx','starty','notes'];
    //protected static $logOnlyDirty = true;
    //protected static $submitEmptyLogs = false;
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->useLogName('location')
        ->logFillable()
        ->logOnly(['name','altitude','adm_level','datum','x','y','startx','starty','notes'])
        ->logOnlyDirty()
        ->dontLogIfAttributesChangedOnly(['updated_at','lft','rgt','depth','parent_id','geom'])
        ->dontSubmitEmptyLogs();      
    }

    public function getPrecisionAttribute()
    {
        if ($this->adm_level <= 99) {
            return Lang::get('levels.imprecise').': <strong>'.Lang::get('messages.centroidof').' '.Lang::get('levels.adm_level.'.$this->adm_level).'</strong>';
        }
        if ($this->adm_level == self::LEVEL_PLOT) {
            return Lang::get('levels.precise').': '.Lang::get('messages.centroidof').' '.Lang::get('levels.adm_level.'.$this->adm_level).'</strong>';
        }
        return Lang::get('levels.precise').':  <strong>'.Lang::get('levels.adm_level.'.$this->adm_level).'</strong>';
    }

    public function rawLink()
    {
        return "<a  href='".url('locations/'.$this->id)."'>".htmlspecialchars($this->name).'</a>';
    }

    public function scopeNoWorld($query)
    {
        return $query->where('adm_level', '>',0);
    }
    public function scopeNoPoint($query)
    {
        return $query->where('adm_level', '<>', self::LEVEL_POINT);
    }
    // quick way to get the World object
    public static function world()
    {
        return self::where('adm_level', -1)->get()->first();
    }

    // for use when receiving this as part of a morph relation
    // TODO: maybe can be changed to get_class($p)?
    public function getTypenameAttribute()
    {
        return 'locations';
    }

    // for use in views/* selects
    public static function AdmLevels()
    {
        return array_merge(config('app.adm_levels'), [
            self::LEVEL_UC,
            self::LEVEL_TI,
            self::LEVEL_ENV,
            self::LEVEL_PLOT,
            self::LEVEL_TRANSECT,
            self::LEVEL_POINT,
        ]);
    }

    public static function used_adm_levels($location_id=null)
    {
      //something wrong here, mysql complains aboud orderby in this statement and it does not has one . perhaps a baum related issue
      //return Location::noWorld()->select("adm_level")->distinct('adm_level')->pluck('adm_level')->toArray();
      if ($location_id != null ) {
        $location = Location::findOrFail($location_id);
        return  $location->descendantsAndSelf()->pluck('adm_level')->unique()->values()->toArray();
        /*
        $location = self::findOrFail($location_id);
        $sql = "SELECT DISTINCT tb.adm_level FROM ((SELECT locations.adm_level AS adm_level FROM locations WHERE locations.lft>".$location->lft." AND locations.rgt<".$location->rgt.") UNION (SELECT adm_level FROM location_related JOIN locations as loc ON loc.id=location_related.location_id WHERE related_id=".$location->id.")) AS tb ORDER BY tb.adm_level";
        $related_children = DB::select($sql);
        return array_map(function($v) { return $v->adm_level;},$related_children);
        */
      }
      return Location::select("adm_level")->noWorld()->pluck("adm_level")->unique()->values()->toArray();
    }

    public static function scopeWithDistance($query, $geom)
    {
        // this query hangs if you attempt to run it on full geom objects, so we add
        // a "where" to make sure we're only calculating distance from small objects
        return $query->addSelect(DB::Raw("id,name,ST_Distance(geom, ST_GeomFromText('$geom')) as distance"))
            ->where('adm_level', '>', self::LEVEL_UC);
    }


    public function getDistanceToSearchAttribute()
    {
      return $this->distance ? $this->distance : null;
    }

    public function measurements()
    {
        return $this->morphMany("App\Models\Measurement", 'measured');
    }

    function getCountryAttribute() : string {
      if ($this->country_code) {
        return Lang::get("countries.".$this->country_code,[],'en');        
      }
      return '';
    }

    function getCountryAdmLevelAttribute() : string {
      if ($this->country_code) {
        $crt= Lang::get("admin-levels.".$this->country_code);
        if (is_array($crt)) {
          if (isset($crt["adm_level.".$this->adm_level])) {
            return $crt["adm_level.".$this->adm_level];
          }
        }       
      }
      return Lang::get('levels.adm_level.'.$this->adm_level);
    }

    public function getLevelNameAttribute()
    {
        return Lang::get('levels.adm_level.'.$this->adm_level,[],'en');
    }


    function getGeoreferenceRemarksAttribute() : string {
      $adm_level=$this->adm_level;
      $remarks = [];
      switch ($adm_level) {
        case  self::LEVEL_POINT:
          $remarks[] = "decimal coordinates are POINT locations, ~ GPS precision;";
          break;
        case  self::LEVEL_PLOT:
        case  self::LEVEL_TRANSECT:
          if ($this->geom_type=="POINT") {
            $remarks[] = "decimal coordinates are POINT locations, ~ GPS precision";
            $remarks[] = "footprintWKT geometry drawn from informed Start Point and location dimensions. A Northern (NE) orientation is assumed.";
          } else {
            $remarks[] = "decimal coordinates are the START POINT in footprintWKT geometry";
          }
          break;
        default:
          $remarks[] = "decimal coordinates are the CENTROID of the footprintWKT geometry";
          break;
      }

      if (count($remarks)) {
        $remarks = implode(" | ",$remarks);
      } else {
        $remarks = '';
      }
      return $remarks;
    }

    public function getLatLong()
    {
        // if the "cached" values are already set, do nothing
        if ($this->long or $this->lat) {
           return;
        }
        $coords = $this->start_point ? self::latlong_from_point($this->start_point) : ($this->centroid_raw ? self::latlong_from_point($this->centroid_raw) : []);
        if ($coords) {
          // all others, extract from centroid
          $this->long = $coords[1];
          $this->lat = $coords[0];
          return;
        }
        $this->long = null;
        $this->lat = null;
    }

    public function getCentroidAttribute()
    {
        $centroid = $this->centroid_raw;
        if (empty($centroid)) {
            $centroid = $this->geom;
        }
        $point = substr($centroid, 6, -1);
        $pos = strpos($point, ' ');

        return ['x' => substr($point, 0, $pos), 'y' => substr($point, $pos + 1)];
    }

    public function getCentroidWKTAttribute()
    {
        return $this->centroid_raw;
    }


    public function getLatitudeSimpleAttribute()
    {
        $this->getLatLong();
        $letter = $this->lat > 0 ? 'N' : 'S';

        return $this->lat1.'&#176;'.$this->lat2.'\''.$this->lat3.'\'\' '.$letter;
    }

    public function getLongitudeSimpleAttribute()
    {
        $this->getLatLong();
        $letter = $this->long > 0 ? 'E' : 'W';
        return $this->long1.'&#176;'.$this->long2.'\''.$this->long3.'\'\' '.$letter;
    }

    public function getCoordinatesSimpleAttribute()
    {
        return '('.$this->latitudeSimple.', '.$this->longitudeSimple.')';
    }

    // query scope for conservation units
    public function scopeUcs($query)
    {
        return $query->where('adm_level', self::LEVEL_UC);
    }

    public function scopeRelated($query)
    {
        return $query->whereIn('adm_level',[self::LEVEL_UC,self::LEVEL_TI,self::LEVEL_ENV]);
    }

    // query scope for all except conservation units
    public function scopeExceptUcs($query)
    {
        return $query->whereNotIn('adm_level',[self::LEVEL_UC,self::LEVEL_TI,self::LEVEL_ENV]);
    }



    public function getFullNameAttribute()
    {
      $path = $this->ancestors()->where('adm_level','<>','-1')->pluck('name')->toArray();
      if (count($path)) {
        $path[] = $this->name;
        return implode(" > ",$path);
      }
      return $this->name;
    }

    public function getBelongsToAttribute(): string
    {
        if (!$this->parent) {
          return '';
        }
        $str = [$this->parent->name];
        foreach ($this->relatedLocations as $related) {
            $str[] = $related->relatedLocation->name;
        }

        return implode(" | ",$str);
    }

    public function getSearchableNameAttribute(): string
    {
        $name = $this->name;
        $name = $this->parent ? $name." << ".$this->parent->name : $name;
        return $name;
    }

    public function getParentNameAttribute(): string
    {
      return $this->parent ? $this->parent->name : '';
    }

    public function setGeomAttribute($value)
    {
        if (is_null($value)) {
            $this->attributes['geom'] = null;

            return;
        }
        // MariaDB returns 1 for invalid geoms from ST_IsEmpty ref: https://mariadb.com/kb/en/mariadb/st_isempty/
        $invalid = DB::select("SELECT ST_IsEmpty(ST_GeomFromText('$value')) as val");
        $invalid = count($invalid) ? $invalid[0]->val : 1;
        if ($invalid) {
            throw new \UnexpectedValueException('Invalid Geometry object: '.$value);
        }
        $this->attributes['geom'] = DB::raw("ST_GeomFromText('$value')");
    }

    //public function getSimplifiedAttribute()
    //{
        //$this->getGeomArrayAttribute(); // force caching
        //return $this->isSimplified;
    //}

    //if the location is drawn according to dimensions informed over a POINT location
    public function getIsDrawnAttribute()
    {
      $adm_level = $this->adm_level;
      $geomtype = $this->geom_type;
      if ($geomtype == "POINT" and ($adm_level == self::LEVEL_PLOT or $adm_level == self::LEVEL_TRANSECT)) {
        return true;
      }
      return false;
    }


    protected function extractXY($point)
    {
        $point = str_replace(["(",")"],"", $point);
        $pos = strpos($point, ' ');
        return ['x' => substr($point, 0, $pos), 'y' => substr($point, $pos + 1)];
    }    

    protected function simplify($array, $factor)
    {
        $this->isSimplified = true;
        // TODO: provide a better simplification for this, such as Douglas-Peucker
        $result = array();
        $lapse = ceil(sizeof($array) / $factor);
        $i = 0;
        foreach ($array as $value) {
            if (0 == $i++ % $lapse) {
                $result[] = $value;
            }
        }

        return $result;
    }

    public function getGeomOriginAttribute()
    {
        // "cache" geom array to reduce overhead
        if (!empty($this->geom_origin)) {
            return $this->geom_origin;
        }

        if ('POINT' == $this->geom_type) {
            return $this->geom;
        }
        if ('POLYGON' == $this->geom_type and $this->adm_level== self::LEVEL_PLOT) {
            $array = explode(',', substr($this->geom, 9, -2));
            $element = $this->extractXY($array[0]);
            return "POINT(".$element['x']." ".$element['x'].")";
        }
        if ("LINESTRING" == $this->geom_type) {
            return $this->start_point;
        }
        return $this->centroid_WKT;
    }

    public function getGeomArrayAttribute()
    {
        // "cache" geom array to reduce overhead
        if (!empty($this->geom_array)) {
            return $this->geom_array;
        }

        if ('POINT' == $this->geom_type) {
            return $this->extractXY(substr($this->geom, 6, -1));
        }
        if ('POLYGON' == $this->geom_type) {
            $array = explode(',', substr($this->geom, 9, -2));
            foreach ($array as &$element) {
                $element = $this->extractXY($element);
            }
            if (sizeof($array) > 1500) {
                //$array = $this->simplify($array, 1500);
            }
            $this->geom_array = [$array];
        }
        if ('MULTIPOLYGON' == $this->geom_type) {
            $array = explode(')),((', substr($this->geom, 15, -3));
            foreach ($array as &$polygon) {
                $p_array = explode(',', $polygon);
                foreach ($p_array as &$element) {
                    $element = $this->extractXY($element);
                }
                $factor = 1500;
                if (sizeof($p_array) > $factor) {
                    //$p_array = $this->simplify($p_array, $factor);
                }
                $polygon = $p_array;
            }
            $this->geom_array = $array;
        }

        return $this->geom_array;
    }

    public static function detectParent($geom, $max_level, $parent_uc, $ignore_level=false,$parent_buffer=0)
    {
        // there can be plots inside plots
        if (self::LEVEL_PLOT == $max_level) {
            $max_level += 1;
        }

        //the $query
        if ($parent_buffer>0) {
          //will use the informed buffer as both:
          // (1) a simplify distance (due to memory for large putative large parents)
          // (2) and the buffer distance;
          // use buffer in parent detection only if noWorld location not detected without buffer.
          // simplify remove because it does not exist i mariadb
          //$query = 'ST_Within(ST_GeomFromText(?), ST_Buffer(ST_Simplify(geom,'.$parent_buffer.'),'.$parent_buffer.'))';
          $query = 'ST_Within(ST_GeomFromText(?), ST_Buffer(geom,'.$parent_buffer.'))';
        } else {
          $query = 'ST_Within(ST_GeomFromText(?), geom)';
        }


        //check which registered polygons (except World)
        //are possible parents (CONTAIN) of submitted location
        //order by adm_level to get the most inclusive first
        $possibles = self::whereRaw($query, [$geom])
            ->where('adm_level','!=',self::LEVEL_POINT)
            ->noWorld();

        //if looking for an UC parent
        if ($parent_uc) {
            $possibles = $possibles->where('adm_level', '=', self::LEVEL_UC);
        } else {
            // only looks for NON-UCs with level smaller
            // than informed for location
            $first = $possibles->whereNotIn('adm_level', [self::LEVEL_UC,self::LEVEL_TI,self::LEVEL_ENV]);
            if (!$ignore_level) {
                $first = $first->where('adm_level', '<', $max_level);
            }
            if ($first->count()) {
                $possibles = $first;
            } elseif (!$ignore_level) {
                $possibles = $possibles->where('adm_level', '<', $max_level);
            }
        }
        //$possibles = $possibles->cursor();
        //if found return the greatest adm_level location found
        if ($possibles->count()) {
            return $possibles->orderBy('adm_level', 'desc')->first();
        }
        return null;
    }

    public function setGeomFromParts($values)
    {
        $geom = self::geomFromParts($values);
        $this->attributes['geom'] = DB::raw("ST_GeomFromText('$geom')");
    }

    public static function geomFromParts($values)
    {
        //$this->getLatLong();
        $lat = $values['lat1'] + $values['lat2'] / 60 + $values['lat3'] / 3600;
        $long = $values['long1'] + $values['long2'] / 60 + $values['long3'] / 3600;
        $long0 = isset($values['long0']) ? $values['long0'] : 1;
        $lat0 = isset($values['latO']) ? $values['latO'] : 1;
        if (0 === $long0) {
            $long = abs($long)*(-1);
        }
        if (0 === $lat0) {
            $lat = abs($lat)*(-1);
        }

        return 'POINT('.$long.' '.$lat.')';
    }






    //individuals through individual_location pivot
    public function individuals()
    {
      return $this->hasManyThrough(
                    'App\Models\Individual',
                    'App\Models\IndividualLocation',
                    'location_id', // Foreign key on individual_location table...
                    'id', // Foreign key on individual table...
                    'id', // Local key on location table...
                    'individual_id' // Local key on individual_location table...
                    );
    }


    public function all_individual_ids()
    {
      $query = DB::select("SELECT DISTINCT tb.theid FROM ((SELECT DISTINCT individual_location.individual_id as theid FROM individual_location JOIN locations ON locations.id=individual_location.location_id WHERE locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.") UNION (SELECT DISTINCT individual_location.individual_id as theid FROM individual_location JOIN location_related ON location_related.location_id=individual_location.location_id JOIN locations ON locations.id=location_related.related_id WHERE locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.")) AS tb");
      return collect($query)->map(function($q) { return $q->theid;})->toArray();
    }

    public function getAllIndividuals()
    {
      return Individual::whereIn('id',$this->all_individual_ids());
      /*
      return  Individual::whereHas('locations',function($location) {
        $location->where('lft',">",$this->lft)->where('rgt',"<",$this->rgt);
      });
      */
    }

    public function getAllProjects()
    {
      return  Project::whereHas('individuals',function($individual) {
        $individual->whereHas('locations',function($location) {
          $location->where('lft',">=",$this->lft)->where('rgt',"=<",$this->rgt);
        }); });
    }


    public function childrenByLevel($level)
    {
      return  DB::table('locations')->where('lft',">=",$this->lft)->where('lft',"<=",$this->rgt)->where('adm_level',$level)->cursor();
    }


    public function getFormatedNotesAttribute()
    {
      return ODBFunctions::formatNotes($this->notes);
    }

    public function locationsDescedantsAndSelfIds($nopoint=false)
    {
        $sqlp = "";
        if ($nopoint) {
          $sqlp = " AND locations.adm_level<>999";
        }
        /* alternative using spatial query
        $geom = self::findOrFail($this->id)->withGeom()->first()->geom;
        if (in_array($this->adm_level,[Location::LEVEL_TRANSECT])) {
          $buffer_dd = (($this->y*0.00001)/1.11);
          $query_buffer ="ST_Buffer(ST_GeomFromText('".$geom."'), ".$buffer_dd.")";
          $query = "ST_Within(locations.geom,".$query_buffer.")";
        } else {
          $query = 'ST_Within(locations.geom,ST_GeomFromText("'.$geom.'"))';
        }
        return Location::whereRaw($query.$sqlp)->pluck('id')->toArray();
        */
        if (in_array($this->adm_level,[Location::LEVEL_UC, Location::LEVEL_TI,Location::LEVEL_ENV])) {
          $ids = $this->descendantsAndSelf()->pluck('id')->toArray();
          $ids_r = LocationRelated::where('related_id',$this->id)->pluck('location_id')->toArray();
          return array_unique(array_merge($ids,$ids_r));          
        }
        $query = $this->descendantsAndSelf();
        if ($nopoint) {
          $query = $query->noPoint();
        }
        return $query->pluck('id')->toArray();
    }

    public function childrenCount()
    {
      if (in_array($this->adm_level,[Location::LEVEL_UC, Location::LEVEL_TI,Location::LEVEL_ENV])) {
        $count = count($this->locationsDescedantsAndSelfIds());
        return ($count>0 ? $count-1 : 0);
      }
      return $this->descendants()->count();
    }



    //vouchers through individual_location
    public function vouchers()
    {
      return $this->hasManyThrough(
                    'App\Models\Voucher',
                    'App\Models\IndividualLocation',
                    'location_id', // Foreign key on individual_location table...
                    'individual_id', // Foreign key on individual table...
                    'id', // Local key on location table...
                    'individual_id' // Local key on individual_location table...
                    );
    }


    public function identifications( )
    {
      return $this->hasManyThrough(
                    'App\Models\Identification',
                    'App\Models\IndividualLocation',
                    'location_id', // Foreign key on individual_location table...
                    'object_id', // Foreign key on individual table...
                    'id', // Local key on location table...
                    'individual_id' // Local key on individual_location table...
                    )->where('object_type','App\Models\Individual');
    }


    // getter method for parts of latitude/longitude
    public function getLat1Attribute()
    {
        return floor(abs($this->lat));
    }

    public function getLat2Attribute()
    {
        return floor(60 * (abs($this->lat) - $this->lat1));
    }

    public function getLat3Attribute()
    {
        return floor(60 * (60 * abs($this->lat) - 60 * $this->lat1 - $this->lat2));
    }

    public function getLatOAttribute()
    {
        return $this->lat > 0;
    }

    public function getLong1Attribute()
    {
        return floor(abs($this->long));
    }

    public function getLong2Attribute()
    {
        return floor(60 * (abs($this->long) - $this->long1));
    }

    public function getLong3Attribute()
    {
        return floor(60 * (60 * abs($this->long) - 60 * $this->long1 - $this->long2));
    }

    public function getLongOAttribute()
    {
        return $this->long > 0;
    }
    public function scopeNullGeom($query)
    {
      return $query->selectRaw('locations.id,locations.name,locations.parent_id,locations.lft,locations.rgt,locations.depth,locations.altitude,locations.adm_level,locations.datum,locations.notes,locations.x,locations.y,locations.startx,locations.starty,locations.created_at,locations.updated_at,locations.as_point,locations.country_code');
    }
    public function scopeWithoutGeom($query)
    {
      return $query->select(
            DB::raw('locations.id,locations.name,locations.parent_id,locations.lft,locations.rgt,locations.depth,locations.altitude,locations.adm_level,locations.datum,locations.notes,locations.x,locations.y,locations.startx,locations.starty,locations.created_at,locations.updated_at,locations.as_point,locations.country_code, locations.centroid_raw, locations.start_point, locations.geom_type, locations.area_raw')            
        );
    }

    public function scopeWithGeom($query)
    {
      return $query->select(
            DB::raw('locations.id,locations.name, locations.adm_level, locations.parent_id, locations.x, locations.y,locations.lft,locations.rgt,locations.depth,locations.startx,locations.starty,locations.altitude,locations.notes,locations.created_at,locations.updated_at,locations.as_point,locations.country_code,locations.centroid_raw,locations.start_point,locations.geom_type,locations.area_raw'),
            DB::raw('ST_AsText(geom) as geom'),            
      );
    }    

    public function scopeMapping($query)
    {        
      $ststart = "(CASE
      WHEN locations.adm_level=999 THEN ST_ASGEOJSON(geom)
      WHEN locations.adm_level=101 THEN ST_ASGEOJSON(ST_StartPoint(geom))
      ELSE ST_ASGEOJSON(ST_StartPoint(St_ExteriorRing(geom)))
      END) as start_pointjson";
      return $query->select(          
          DB::raw('locations.id,locations.name, locations.adm_level, locations.parent_id, locations.x, locations.y,locations.lft,locations.rgt,locations.depth,locations.startx,locations.starty,locations.altitude,locations.notes,locations.as_point,locations.country_code,geom_type,area_raw'),
          DB::raw("ST_ASGEOJSON(geom) as geomjson"),            
          DB::raw($ststart),            
      );
    }


    public function scopeWithGeojson($query)
    {
        $ststart = "(CASE
    WHEN locations.adm_level=999 THEN ST_ASGEOJSON(geom)
    WHEN locations.adm_level=101 THEN ST_ASGEOJSON(ST_StartPoint(geom))
    ELSE ST_ASGEOJSON(ST_StartPoint(St_ExteriorRing(geom)))
    END) as start_pointjson";
        return $query->addSelect(
           DB::raw("ST_ASGEOJSON(geom) as geomjson"),
           DB::raw('ST_AsText(geom) as geom'),
           DB::raw($ststart),
        );
    }

    // TODO: implement geo pacakge to deal with transformations or implement equal area srid in mysql
    public function getAreaAttribute()
    {
      if (in_array($this->adm_level,[self::LEVEL_POINT,self::LEVEL_TRANSECT])) {
        return null;
      }
      if ($this->x and $this->y) {
        return $this->x*$this->y;
      }
      // converto meters
      return round($this->area_raw*11100000000,2);
    }



    public function mediaDescendantsAndSelf()
    {
        $ids = $this->descendantsAndSelf()->pluck('id')->toArray();
        return Media::whereIn('model_id',$ids)->where('model_type','=','App\Models\Location');
    }

    /* FUNCTIONS TO INTERACT WITH THE COUNT MODEL */
    public function summary_counts()
    {
        return $this->morphMany("App\Models\Summary", 'object');
    }

    public function summary_scopes()
    {
        return $this->morphMany("App\Models\Summary", 'scope');
    }


    public function getCount($scope="all",$scopeId=null,$target='individuals')
    {
      if (IndividualLocation::count()==0) {
        return 0;
      }
      /*
      $query = $this->summary_counts()->where('scope_type',"=",$scope)->where('target',"=",$target);
      if (null !== $scopeId) {
        $query = $query->where('scope_id',"=",$scopeId);
      } else {
        $query = $query->whereNull('scope_id');
      }
      if ($query->count()) {
        return $query->first()->value;
      }
      */
      //get a fresh count
      if ($target=="individuals") {
        return $this->individualsCount($scope,$scopeId);
      }
      //get a fresh count
      if ($target=="measurements") {
        return $this->measurementsCount($scope,$scopeId);
      }
      //get a fresh count
      if ($target=="vouchers") {
        return $this->vouchersCount($scope,$scopeId);
      }
      if ($target=="taxons") {
        return $this->taxonsCount($scope,$scopeId);
      }
      if ($target=="media") {
        return $this->all_media_count();
      }
      return 0;
    }



    /* functions to generate counts */
    public function individualsCount($scope='all',$scopeId=null)
    {
      $sql = "SELECT DISTINCT tb.theid FROM ((SELECT DISTINCT individual_location.individual_id as theid FROM individual_location JOIN locations ON locations.id=individual_location.location_id WHERE locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.") UNION (SELECT DISTINCT individual_location.individual_id as theid FROM individual_location JOIN location_related ON location_related.location_id=individual_location.location_id JOIN locations ON locations.id=location_related.related_id WHERE locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.")) AS tb";
      $valid_ids = [];
      if ('projects' == $scope and $scopeId>0) {
        $valid_ids = Dataset::where('project_id',$scopeId)->cursor()->map(function($m) {
          return $m->all_individuals_ids();
        })->toArray();
        if (count($valid_ids)==0) {
          return 0;
        }
      }
      if ('datasets' == $scope and $scopeId>0) {
        $valid_ids = Dataset::findOrFail($scopeId)->all_individuals_ids();
        if (count($valid_ids)==0) {
          return 0;
        }
      }
      $query = DB::select($sql);
      $ids = array_map(function($u){ return ((array)$u)['theid'];},$query);
      if (count($valid_ids)) {
        $ids = array_intersect($ids,$valid_ids);
      }
      return count($ids);
    }

    //count direct and indirect vouchers
    public function vouchersCount($scope='all',$scopeId=null)
    {

        $sql = "SELECT DISTINCT tb.theid FROM ((SELECT DISTINCT(vouchers.id) as theid FROM vouchers,individual_location,locations where vouchers.individual_id=individual_location.individual_id AND individual_location.location_id=locations.id AND locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.") UNION (SELECT DISTINCT vouchers.id as theid FROM vouchers JOIN individual_location ON individual_location.individual_id=vouchers.individual_id JOIN location_related ON location_related.location_id=individual_location.location_id JOIN locations ON locations.id=location_related.related_id WHERE locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.")) AS tb";
        $valid_ids = [];
        if ('projects' == $scope and $scopeId>0) {
          $valid_ids = Dataset::where('project_id',$scopeId)->cursor()->map(function($m) {
            return $m->all_voucher_ids();
          })->toArray();
          if (count($valid_ids)==0) {
            return 0;
          }
        }
        if ('datasets' == $scope and $scopeId>0) {
          $valid_ids = Dataset::findOrFail($scopeId)->all_voucher_ids();
          if (count($valid_ids)==0) {
            return 0;
          }
        }
        $query = DB::select($sql);
        $ids = array_map(function($u){ return ((array)$u)['theid'];},$query);
        if (count($valid_ids)) {
          $ids = array_intersect($ids,$valid_ids);
        }
        return count($ids);
    }

    //measurement should count only LOCATION measurements, including descendants (not like taxon as descendant has not a relationship with parent like phylogenetic relationships), so should not count measurements for individuals and vouchers at locations.
    public function measurementsCount($scope='all',$scopeId=null)
    {
      $addwhere = "";
      if ('projects' == $scope and $scopeId>0) {
        $dataset_ids = Dataset::where('project_id',$scopeId)->cursor()->pluck('datasets.id')->toArray();
        if (count($dataset_ids)==0) {
          return 0;
        }
        $dataset_ids = implode(",",$dataset_ids);
        $addwhere = " AND measurements.dataset_id IN ($dataset_ids)";
      }
      if ('datasets' == $scope and $scopeId>0) {
        $addwhere = " AND measurements.dataset_id=".$scopeId;
      }
      $sql = "(SELECT DISTINCT(measurements.measured_id) FROM locations,measurements where measurements.measured_type like '%location%' AND measurements.measured_id=locations.id AND locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.$addwhere.") UNION (SELECT DISTINCT(measurements.measured_id) FROM location_related ,locations,measurements where measurements.measured_type like '%location%' AND measurements.measured_id=location_related.location_id AND location_related.related_id=locations.id AND locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.$addwhere.")";
      $query = DB::select($sql);
      return count($query);
    }




    public function taxonsCount($scope=null,$scopeId=null)
    {

      $sql = "(SELECT DISTINCT(taxon_id) FROM identifications,individual_location,locations where
      identifications.object_id=individual_location.individual_id AND (identifications.object_type LIKE '%individual%') AND individual_location.location_id=locations.id AND locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.") UNION (SELECT DISTINCT(taxon_id) FROM identifications,individual_location,location_related,locations where
      identifications.object_id=individual_location.individual_id AND (identifications.object_type LIKE '%individual%') AND individual_location.location_id=location_related.location_id AND location_related.related_id=locations.id AND locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.")";
      $valid_ids = [];
      if ('projects' == $scope and $scopeId>0) {
        $valid_ids = Dataset::where('project_id',$scopeId)->cursor()->map(function($m) {
          return $m->all_taxons_ids();
        })->toArray();
        if (count($valid_ids)==0) {
          return 0;
        }
      }
      if ('datasets' == $scope and $scopeId>0) {
        $valid_ids = Dataset::findOrFail($scopeId)->all_taxons_ids();
        if (count($valid_ids)==0) {
          return 0;
        }
      }
      $query = DB::select($sql);
      $ids = array_map(function($u){ return ((array)$u)['taxon_id'];},$query);
      if (count($valid_ids)) {
        $ids = array_intersect($ids,$valid_ids);
      }
      return count($ids);
    }


   public function all_taxons_ids()
    {
        $sql = "(SELECT DISTINCT(taxon_id) FROM identifications,individual_location,locations where
        identifications.object_id=individual_location.individual_id AND (identifications.object_type LIKE '%individual%') AND individual_location.location_id=locations.id AND locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.") UNION (SELECT DISTINCT(taxon_id) FROM identifications,individual_location,location_related,locations where
        identifications.object_id=individual_location.individual_id AND (identifications.object_type LIKE '%individual%') AND individual_location.location_id=location_related.location_id AND location_related.related_id=locations.id AND locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.")";
        $query = array_unique(collect(DB::select($sql))->pluck('taxon_id')->toArray());
        return $query;
    }

    public function all_voucher_ids()
    {
        $sql = "SELECT DISTINCT tb.theid FROM ((SELECT DISTINCT vouchers.id as theid FROM vouchers JOIN individual_location ON individual_location.individual_id=vouchers.individual_id JOIN locations ON locations.id=individual_location.location_id WHERE locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.") UNION (SELECT DISTINCT vouchers.id as theid FROM vouchers JOIN individual_location ON individual_location.individual_id=vouchers.individual_id JOIN location_related ON location_related.location_id=individual_location.location_id JOIN locations ON locations.id=location_related.related_id WHERE locations.lft>=".$this->lft." AND locations.lft<=".$this->rgt.")) AS tb";
        $query = collect(DB::select($sql))->pluck('theid')->toArray();
        return $query;
    }


    /*  MEDIA RELATED FUNCTIONS */

    /*  all media count
    * for location linked media only, including descendants
    */
    public function all_media_count()
    {
      return $this->mediaDescendantsAndSelf()->count();
    }



    /* register media modifications used by Spatie media-library trait */
    public function registerMediaConversions(BaseMedia $media = null): void
    {

        $this->addMediaConversion('thumb')
            ->fit('crop', 200, 200)
            ->performOnCollections('images');

        // TODO: this is not working for some reason
        $this->addMediaConversion('thumb')
            ->width(200)
            ->height(200)
            ->extractVideoFrameAtSecond(5)
            ->performOnCollections('videos');
    }

    /* helper  to get table name from model instance */
    public static function getTableName()
    {
        return (new self())->getTable();
    }


    public function getTransectLengthAttribute()
    {
      if ($this->adm_level != self::LEVEL_TRANSECT) {
        return null;
      }
      if ($this->geom_type == 'LINESTRING') {
          $distance = self::linestring_length($this->geom);
          return $distance;
      }
      //then is a point location with a X dimension attribute
      return $this->x;
    }

    /* Length of transects == linestrings in meters */
    /* instaed of using ST_LENGTH directly
    * because using distance_sphere seems to give more precise results
    */
    public static function linestring_length($geom)
    {
      $linestring = explode(",",$geom);
      $pattern = '/\\(|\\)|LINESTRING|\\n/i';
      $coordinates = preg_replace($pattern, '', $linestring);
      $distance = 0;
      $ncoords = count($coordinates);
      for($i=1;$i<$ncoords;$i++) {
        $point_s = "POINT(".$coordinates[($i-1)].")";
        $point_e = "POINT(".$coordinates[$i].")";
        $geom = DB::select("SELECT ROUND(ST_Distance_Sphere(ST_GeomFromText('$point_s'), ST_GeomFromText('$point_e')),2) as distance" );
        $distance_r = $geom[0]->distance;
        $distance = $distance+$distance_r;
      }
      return $distance;
    }

    /* Find a point location along a LineString, having:
    * a distance from the origin (first point of linestring )
    * by having the X distance from the start of a transect (first point in linestring)
    public static function interpolate_on_transect($location_id,$x)
    {
      if ($x == null) {
        return null;
      }
      $location = self::withGeom()->findOrFail($location_id);
      $transect_length = $location->transect_length;
      if (null != $transect_length) {
        $fraction= $x/$transect_length;
        $geom = self::selectRaw('ST_AsText(ST_LineInterpolatePoint(geom,'.$fraction.')) as point')->where('id',$location_id)->get();
        return $geom[0]->point;
      }
      return null;
    }
    /*

    /* this function replaces function above
    * because mariadb does not have ST_LineInterpolatePoint function
    */
    public static function interpolate_on_transect($location_id,$x) {
        if ($x == null) {
          return null;
        }
        $location = Location::withGeom()->findOrFail($location_id);
        $length = $location->transect_length;
        if ($x>$length) {
          return null;
        }
        $fraction = $x/$length;
        $linestring = $location->geom;
        $coords = preg_split('/\\(|\\)/', $linestring)[1];
        $coords = explode(",",trim($coords));
        if ($x==0) {
          return "POINT(".$coords[0].")";
        }
        $distance = 0;
        $idx = 0;
        $doit = true;
        while($doit) {
          $coordA = explode(" ",$coords[$idx]);
          $coordA   = new \League\Geotools\Coordinate\Coordinate([$coordA[0],$coordA[1]]);

          $coordB = explode(" ",$coords[($idx+1)]);
          $coordB   = new \League\Geotools\Coordinate\Coordinate([$coordB[0],$coordB[1]]);

          $geotools = new \League\Geotools\Geotools();
          $segment = $geotools->distance()->setFrom($coordA)->setTo($coordB)->greatCircle();
          $cumulative = $distance+$segment;
          $thefraction = $cumulative/$length;
          if ($thefraction>$fraction or ($idx+1)>count($coords)) {
            $doit = false;
          } else {
            $idx++;
            $distance = $distance+$segment;
          }
        }
        $geotool = new \League\Geotools\Geotools();
        $bearing    =  $geotool->vertex()->setFrom($coordA)->setTo($coordB)->initialBearing();
        $segment = ($length*($thefraction-$fraction))/100;
        $coordA = "POINT(".$coords[$idx].")";
        $point_along_linestring = Location::destination_point($coordA,$bearing,$segment);
        return $point_along_linestring;
    }


    /* function  to define a geometry for a subplot being imported
    *  without a geometry specification
    *  to be used only when parent plot is defined as point
    *  or as polygon with four vertices and 0,0 being SW corner,
    *  and then the other coordinates clock wise.
    */
    public static function subplot_geometry($parent_id,$start_x,$start_y)
    {
      $parent = Location::withGeom()->findOrFail($parent_id)->first();
      if (!$parent) {
        return null;
      }
      /* get the 0,0 coordinates for a subplot given the x and y dimensions */
      $subplot_origin = self::individual_in_plot($parent->plot_geometry,$start_x,$start_y);
      return $subplot_origin;
    }

    /* function to calculate a destination point having:
    * $point = wkt POINT geometry in Latitude and Longitude degrees
    * $brng = a azimuth or global bearing rangin from 0 to 360
    * $meters = a distance in meter to place the new location
    */
     public static function destination_point($point, $brng, $meters) {
          $start = preg_split('/\\(|\\)/', $point)[1];
          $start = explode(" ",trim($start));
          $lat = $start[1];
          $long = $start[0];
          $geotools = new \League\Geotools\Geotools();
          $start_point   = new \League\Geotools\Coordinate\Coordinate([$lat,$long]);

          $destinationPoint = $geotools->vertex()->setFrom($start_point)->destination($brng, $meters); //

          $destination_lat =$destinationPoint->getLatitude();
          $destination_long =$destinationPoint->getLongitude();

          return "POINT(".$destination_long." ".$destination_lat.")";
    }

    public static function latlong_from_point($point)
    {
      $coords = preg_split('[\\(|\\)]',$point)[1];
      $coords = explode(" ",trim($coords));
      return [(float) $coords[1], (float) $coords[0]];
    }


    public static function generate_plot_geometry($first_point,$dim_x,$dim_y,$azimuth)
    {
      $angle1 = ($azimuth>=360 or $azimuth<0) ? 0 : $azimuth;
      $angle2 = $angle1+90;
      $angle2 = ($angle2>=360) ? ($angle2-360) : $angle2;
      $second_point = Location::destination_point($first_point,$angle1,$dim_y);
      $third_point =  Location::destination_point($second_point,$angle2,$dim_x);
      $fourth_point = Location::destination_point($first_point,$angle2,$dim_x);
      $first_point = self::latlong_from_point($first_point);
      $second_point = self::latlong_from_point($second_point);
      $third_point = self::latlong_from_point($third_point);
      $fourth_point = self::latlong_from_point($fourth_point);
      $geom = "POLYGON((".$first_point[1]." ".$first_point[0].",".$second_point[1]." ".$second_point[0].",".$third_point[1]." ".$third_point[0].",".$fourth_point[1]." ".$fourth_point[0].",".$first_point[1]." ".$first_point[0]."))";
      return $geom;
    }

    public function getPlotGeometryAttribute()
    {
      /* draw a polygon when plot is points
        * N oriented
        * point is 0,0 SW corner
      */
      if ($this->adm_level == self::LEVEL_PLOT and $this->geom_type== "POINT") {
        return self::generate_plot_geometry($this->geom,$this->x,$this->y,0);
      }
      return $this->geom;
    }

    public static function generate_transect_geometry($first_point,$dim_x,$azimuth)
    {
      $azimuth = ($azimuth>=360 or $azimuth<0) ? 0 : $azimuth;
      $second_point = Location::destination_point($first_point,$azimuth,$dim_x);
      $first_point = self::latlong_from_point($first_point);
      $second_point = self::latlong_from_point($second_point);
      return "LineString(".$first_point[1]." ".$first_point[0].",".$second_point[1]." ".$second_point[0].")";
    }

    public function getTransectGeometryAttribute()
    {
      /* draw a polygon when plot is points
        * N oriented
        * point is 0,0 SW corner
      */
      if ($this->adm_level == self::LEVEL_TRANSECT and $this->geom_type== "POINT") {
        return self::generate_transect_geometry($this->geom,$this->x,0);
      }
      return $this->geom;
    }

    /*
    public function getGeomjsonAttribute()
    {
      $geom = $this->withGeojson();
      if ($this->adm_level == self::LEVEL_PLOT and $this->geom_type== "POINT") {
         $geom = $this->plot_geometry;
      }
      if ($this->adm_level == self::LEVEL_TRANSECT and $this->geom_type== "POINT") {
         $geom = $this->transect_geometry;
      }

      return DB::select("SELECT ST_ASGEOJSON(ST_GeomFromText('".$geom."')) as geojson")[0]->geojson;
    }
    */

    /* map individuals in plots having a geometry */
    public static function individual_in_plot($geom,$x,$y)
    {
      if ($x===null or $y===null) {
        return $geom;
      }
      $pattern = '/\\(|\\)|POLYGON|\\n/i';
      $coordinates = preg_replace($pattern, '', $geom);
      $array = explode(",",trim($coordinates));
      $first_point = "POINT(".$array[0].")";
      $last_point = "POINT(".$array[count($array)-2].")";
      $secont_point = "POINT(".$array[1].")";
      $geotools = new \League\Geotools\Geotools();
      $coordA   = new \League\Geotools\Coordinate\Coordinate(Location::latlong_from_point($first_point));
      $coordB   = new \League\Geotools\Coordinate\Coordinate(Location::latlong_from_point($secont_point));
      $coordC   = new \League\Geotools\Coordinate\Coordinate(Location::latlong_from_point($last_point));

      $bearingY    =  $geotools->vertex()->setFrom($coordA)->setTo($coordB)->initialBearing();
      $bearingX    =  $geotools->vertex()->setFrom($coordA)->setTo($coordC)->initialBearing();

      $pointatborder = Location::destination_point($first_point,$bearingY,$y);
      return Location::destination_point($pointatborder,$bearingX,$x);
    }

    public static function calculateGlobalPosition($x,$y,$angle,$distance,$location)
    {
      $location = Location::withGeom()->findOrFail($location);
      $bearing = null;
      $distance = null;
      $start_point = $location->start_point;
      $individual_point = $start_point;
      if ($location->adm_level == Location::LEVEL_POINT and $angle and $distance) {
         //map with bearing and distance (destination point)
        $bearing = $angle;
        $distance = $distance;
        $individual_point = Location::destination_point($start_point,$bearing,$distance);
      }
      //if location is PLOT
      if ($location->adm_level == Location::LEVEL_PLOT and $x and $y) {
        $geom = $location->geom;
        $individual_point = Location::individual_in_plot($geom,$x,$y);
      }
      //linestrings mapping
      if ($location->adm_level == Location::LEVEL_TRANSECT and $x) {
          $individual_point = Location::interpolate_on_transect($location->id,$x);
          if ($y) {
            $bearing = Location::bearing_at_position_for_destination($location->id,$x,$y);
            $distance = abs($y);
            $individual_point = Location::destination_point($individual_point,$bearing,$distance);
          }
      }
      return $individual_point;
    }

    /* for linestrings
      $x = the individual x position
      $y = the individual y position (perpendicular to linestring)
    */
    public static function bearing_at_position_for_destination($location_id,$x,$y)
    {
      if ($x == null) {
        return null;
      }
      $location = self::withGeom()->findOrFail($location_id);

      //the X position of the individual along the transect
      $start_point = self::interpolate_on_transect($location_id,$x);

      //define a previous point from the X position
      $pd = 2;
      $lag1 = $pd < $x ? ($x - $pd) : $x;
      $previous_point = Location::interpolate_on_transect($location_id,$lag1);

      //define an after point from the X position
      $lag2 = ($x + $pd) > $location->transect_length ? $location->transect_length : $x + $pd;
      $after_point = Location::interpolate_on_transect($location_id,$lag2);

      $geotools = new \League\Geotools\Geotools();
      $coordA   = new \League\Geotools\Coordinate\Coordinate(self::latlong_from_point($previous_point));
      $coordB   = new \League\Geotools\Coordinate\Coordinate(self::latlong_from_point($start_point));
      $coordC   = new \League\Geotools\Coordinate\Coordinate(self::latlong_from_point($after_point));

      //bearings to get a 90 degrees position in relation to transect
      $vertex1    =  $geotools->vertex()->setFrom($coordA)->setTo($coordB);
      $vertex2    =  $geotools->vertex()->setFrom($coordB)->setTo($coordC);
      //average from previous and after
      $bearing = ($vertex1->initialBearing()+$vertex2->initialBearing())/2;

      //if Y is negative (left side of transect )
      if ($y < 0) {
        $bearing = $bearing - 90;
      } else {
        //else rigth side of transect
        $bearing = $bearing + 90;
      }
      //adjust values if negative or greater than valid
      if ($bearing < 0 ) {
        $bearing = 360 - abs($bearing);
      } elseif ($bearing > 360) {
        $bearing = $bearing - 360;
      }
      //bearing to place a point in relation to the linestring
      return $bearing;
    }

    /* generates contents for mapping locations and/or individuals */
    public static function generateFeatureCollection($locations_ids,$individual_ids=null,$precise=null)
    {
        $locations = Location::whereIn('id',$locations_ids);
        $ancestors_ids = $locations->cursor()->map(function($l) { return $l->getAncestorsAndSelf()->pluck('id')->toArray(); })->toArray();
        $ancestors_ids  = array_unique(Arr::flatten($ancestors_ids));
        //add related if any
        $related_ids = $locations->cursor()->map(function($l) { return $l->relatedLocations->pluck('related_id')->toArray(); })->toArray();
        $related_ids  = array_unique(Arr::flatten($related_ids));
        if (count($related_ids)) {
          $ancestors_ids = array_merge($ancestors_ids,$related_ids);
        }
        if ($individual_ids !== null ) {
          $ancestors_ids = Location::whereIn('id',$ancestors_ids)->where('adm_level','<>',Location::LEVEL_POINT)->cursor()->pluck('id')->toArray();
        }
        $data  = Location::prepLocationsToMap($ancestors_ids,true,$respect_level=true,null);
        $location_json = $data['features'];
        $location_extent = $data['extent'];
        $location_centroid = $data['centroid'];
        if ($individual_ids != null) {
          if (!is_array($individual_ids)) {
            $individual_ids = [$individual_ids];
          }
          $data  = Location::generateFeatureCollectionOccurrences($individual_ids,null,$precise);
          if (null != $data) {
            $indjson = $data['features'];
            $location_json = array_merge($location_json,$indjson);
            $location_extent = $data['extent'];
            $location_centroid = $data['centroid'];
          }
        }
        $string = ['features' => $location_json,'extent' => $location_extent,'centroid' => $location_centroid];
        return $string;
    }

    /* generates contents for mapping individuals only */
    public static function generateFeatureCollectionOccurrences($individual_ids=null,$voucher_ids=null,$precise=null)
    {
        if ($individual_ids == null and $voucher_ids==null) {
            return null;
        }
        $isvoucher = false;
        if ($individual_ids!=null) {
          if (!is_array($individual_ids)) {
            $individual_ids = [$individual_ids];
          }
          $query = IndividualLocation::withoutGlobalScopes()->withXy()->with('individual','location_with_geom')->whereIn('individual_id',$individual_ids)->get();
        } else {
          if (!is_array($voucher_ids)) {
            $voucher_ids = [$voucher_ids];
          }
          $isvoucher = true;
          //$sql = "ST_AsText(global_position) as globalPosition, ST_AsGeoJson(global_position) as globalPositionJson";
          $query = Voucher::with(['current_location'])->whereIn('id',$voucher_ids)->get();
        }
        if ($query->count()==0) {
          return null;
        }
        if ($isvoucher) {
          $taxon_ids = array_unique($query->map(function($q){
            return isset($q->individual_identification) ? $q->individual_identification->taxon_id : null;
          })->toArray());
        } else {
          $taxon_ids = array_unique($query->map(function($q){
            return isset($q->identification) ? $q->identification->taxon_id : null;
          })->toArray());
        }
        $bytaxon = false;
        if (count($taxon_ids)<=15 and count($taxon_ids)>1) {
          $bytaxon = true;
        }
        $features = [];
        $lats =[];
        $longs = [];
        $colors = [];
        $layer_key = null;
        foreach($query as $object) {
              $identification = $object->scientificName;
              $layer_key = $identification;
              if (!$bytaxon) {
                $color = self::getMapColor(1000);
              } else {
                if (isset($colors[$layer_key])) {
                  $color = $colors[$layer_key];
                } else {
                  $color = self::getMapColor($layer_key);
                  $colors[$layer_key] = $color;
                }
              }
              $fill = str_replace(")",",0.2)",$color);
              $txt = ['Taxon' => '<em>'.$identification.'</em>'];
              if ($isvoucher) {
                $txt['Voucher'] = '<a href="'.url('vouchers/'.$object->id).'" style="cursor: pointer;">'.$object->biocollection->acronym." #".$object->biocollection_number.'</a>';
                $txt['Collector'] = $object->collector_number;
                $geom = $object->current_location->globalPositionJson;
                $point = $object->current_location->globalPosition;
              } else {
                $txt['Individual'] = $object->individual->rawLink();
                $txt['Collector'] = $object->individual->collector_number;
                $geom = $object->globalPositionJson;
                $point = $object->globalPosition;
              }
              $txt['Date'] = $object->recordedDate;
              $properties = [
                'name' => $identification,
                'description' => "<div style='font-size: 80%;'>".implode('<br>',$txt)."</div>",
                'adm_level' => 1000,
                'color' => $color,
                'fill' => $fill,
                'location_type' => Lang::get("messages.individual"),
                'visibility' => true,
                ];
              $str = [
                "type" => "Feature",
                "geometry" => $geom ? json_decode($geom) : null,
                "properties" => $properties,
              ];
              $key = $identification;
              if ($bytaxon) {
                $features[$layer_key][] = $str;
              } else {
                $features[] = $str;
              }
              $coords = Location::latlong_from_point($point);
              $lats[] = $coords[0];
              $longs[] = $coords[1];
        }
        $amount = 0.1;
        $extent = ['xmin' => min($longs)-$amount, 'ymin' => min($lats)-$amount,'xmax' => max($longs)+$amount,'ymax' => max($lats)+$amount];
        $centroid = [(array_sum($longs)/count($longs)),(array_sum($lats)/count($lats))];
        $result = [];
        if ($bytaxon) {
          krsort($features);
          foreach($features as $key => $feature) {
              $result[$key] = json_encode([ "type" => "FeatureCollection", "features" => $feature]);
          }
        } else {
          $result['Occurrences'] = json_encode([ "type" => "FeatureCollection", "features" => $features]);
        }
        return ['features' => $result,'extent' => $extent,'centroid' => $centroid];
    }

    /* generates contents for mapping datasets */
    public static function generateDatasetFeatureCollection($dataset_ids=null)
    {
        if ($dataset_ids == null) {
            return null;
        }
        if (!is_array($dataset_ids)) {
          $dataset_ids = [$dataset_ids];
        }
        $features = [];
        $lats =[];
        $longs = [];
        $trait_summary = [];
        $query = Dataset::whereIn('id',$dataset_ids);
        $byname=false;
        if ($query->count()<20) {
          $byname = true;
        }
        $colors = [];
        $layer_key = null;
        foreach($query->cursor() as $dataset) {
          if (!isset($dataset->centroid)) {
            continue;
          }
          $layer_key = $dataset->name;
          if (!$byname) {
            $color = self::getMapColor(1000);
          } else {
            if (isset($colors[$layer_key])) {
              $color = $colors[$layer_key];
            } else {
              $color = self::getMapColor(null);
              $colors[$layer_key] = $color;
            }
          }
          $fill = str_replace(")",",0.2)",$color);
          //$plot_included = $dataset->plot_included();
          //$trait_summary = $dataset->traits_summary();
          
          $html = view("datasets.geobox-summary",compact('dataset'))->render();
          $properties = [
            'name' => $dataset->rawLink(),
            'description' => "<div style='font-size: 80%;'>".$html."</div>",
            'adm_level' => 1000,
            'color' => $color,
            'fill' => $fill,
            'location_type' => Lang::get("messages.dataset"),
            'visibility' => true,
          ];
          $geojson = DB::select("SELECT ST_ASGEOJSON(ST_GeomFromText('".$dataset->centroid."')) as geojson");
          $str = [
            "type" => "Feature",
            "geometry" => json_decode($geojson[0]->geojson),
            "properties" => $properties,
            ];
          if ($byname) {
            $features[$dataset->name][] = $str;
          } else {
            $features[] = $str;
          }
          $lats[] = $dataset->latitude;
          $longs[] = $dataset->longitude;
        }
        if (count($longs)==0) {
          return null;
        }
        $extent = ['xmin' => min($longs), 'ymin' => min($lats),'xmax' => max($longs),'ymax' => max($lats)];
        $centroid = [(array_sum($longs)/count($longs)),(array_sum($lats)/count($lats))];
        $result = [];
        if ($byname) {
          krsort($features);
          foreach($features as $key => $feature) {
              $result[$key] = json_encode([ "type" => "FeatureCollection", "features" => $feature]);
          }
        } else {
          $result['Datasets'] = json_encode([ "type" => "FeatureCollection", "features" => $features]);
        }
        return ['features' => $result,'extent' => $extent,'centroid' => $centroid];
    }



    public static function generateSimpleOccurrencesToMap($individual_ids=null,$voucher_ids=null)
    {
        if ($individual_ids == null and $voucher_ids==null) {
            return null;
        }
        $isvoucher = false;
        if ($individual_ids!=null) {
          if (!is_array($individual_ids)) {
            $individual_ids = [$individual_ids];
          }
         
        } else {
          if (!is_array($voucher_ids)) {
            $voucher_ids = [$voucher_ids];
          }
          $individual_ids = Voucher::withoutGlobalScopes()->whereIn('id',$voucher_ids)->pluck('individual_id')->unique()->values()->toArray();
          $isvoucher = true;
          
        }
        $query = IndividualLocation::mapping()->whereIn('individual_id',$individual_ids);
        if ($query->count()==0) {
          return null;
        }       
        $features = $query->get()->map(function($object) 
        {
          $geom = $object->globalPositionJson;
          $url = "<a href='".url('individuals/'.$object->individual_id)."'>See more</a>";
          $str = [
            "type" => "Feature",
            "geometry" => $geom ? json_decode($geom) : null,
            "properties" => "",
          ];
          $properties = [
            'name' => $object->scientificName,
            'description' => "<div style='font-size: 80%;'>".$url."</div>",
            'adm_level' => 1000,
            'color' => "yellow",
            'fill' => "yellow",
            'location_type' => Lang::get("messages.occurrence"),
            'visibility' => true,
          ];
          return [
            'feature' => $str,
            'latitude' => $object->decimal_latitude,
            'longitude' => $object->decimal_longitude,
          ];            
        })->toArray();
        $lats =collect($features)->pluck('latitude')->toArray();
        $longs = collect($features)->pluck('longitude')->toArray();       
        $features = collect($features)->pluck('feature')->toArray();        
        $amount = 0.1;
        $extent = ['xmin' => min($longs)-$amount, 'ymin' => min($lats)-$amount,'xmax' => max($longs)+$amount,'ymax' => max($lats)+$amount];
        $centroid = [(array_sum($longs)/count($longs)),(array_sum($lats)/count($lats))];
        $result['Occurrences'] = json_encode([ "type" => "FeatureCollection", "features" => $features]);        
        return ['features' => $result,'extent' => $extent,'centroid' => $centroid];
    }




    public static function getMapColor($adm_level=null)
    {
      $colors = config('map-colors');
      $cor = null;
      if (null != $adm_level) {
        $cor = isset($colors[((string)$adm_level)]) ? $colors[((string)$adm_level)]['color'] : null;
      }
      if (null == $cor) {
        $cor = RandomColor::one([
          'luminosity' => 'bright',
          'format' => 'rgbCss'
        ]);
      }
      return $cor;
    }

    /* generates contents for mapping locations*/
    public static function prepLocationsToMap($ids,$include_origins=false,$respect_level=false,$theid=null)
    {
        $locations = Location::whereIn("id",$ids)
          ->noWorld()->withoutGeom()->withGeojson()
          ->addSelect(
              DB::raw('ST_ASTEXT(ST_ENVELOPE(geom)) as envelope')
            )
          ->orderByRaw('odb_ordered_admlevel(adm_level,parent_id)')
          ->orderBy('name')
          ->cursor();
        $n = $locations->count();
        $byname = false;
        if ($n<100) {
          $byname = true;
        }
        //each adm_level becomes a layer group
        $layers = [];
        $lats = [];
        $longs = [];
        $layer_key = null;
        $colors = [];
        foreach($locations as $location) {
            //define how to group features as geo layers
            $adm_level = $location->adm_level;
            if (!$byname) {
              $layer_key = Lang::get("levels.adm_level.".$adm_level);
              $colori = $adm_level;
            } else {
              $layer_key = $location->name;
              $colori = $layer_key;
            }
            if (isset($colors[$layer_key])) {
              $color = $colors[$layer_key];
            } else {
              if ($respect_level) {
                $color = Location::getMapColor($adm_level);
              } else {
                $color = Location::getMapColor($colori);
              }
              $colors[$layer_key] = $color;
            }
            $fill = str_replace(")",",0.2)",$color);
            $html = view("locations.geobox",compact('location'))->render();
            //$html = "";
            $properties = [
              'name' => $location->name,
              'description' => "<div style='font-size: 80%;'>".$html."</div>",
              'adm_level' => $adm_level,
              'color' => $color,
              'fill' => $fill,
              'visibility' => true,
            ];
            $str = [
              "type" => "Feature",
              "geometry" => json_decode($location->geomjson),
              "properties" => $properties,
            ];

            $layers[$layer_key][] = $str;
            //get extent
            if (null === $theid or $theid===$location->id) {
              $pattern = "/\\(|\\)|,/i";
              $coords = preg_split($pattern,$location->envelope);
              $coords = collect($coords)->filter(function($q) { 
                $xx = explode(" ",$q); 
                return count($xx)>1 ? true : false;
              })->values()->map(function($v) {
                $vv = explode(" ",$v);
                return [
                  "latitude" => (float) $vv[1],
                  "longitude" => (float) $vv[0],
                ];
              })->values();
              $lats = $coords->pluck("latitude")->toArray();
              $longs = $coords->pluck("longitude")->toArray();             
            }
            //add plot and transect origins if any
            if ($include_origins) {
                $dotcolor = $color;
                if (!$byname or $respect_level) {
                  $dotcolor = config('map-colors')[((string)Location::LEVEL_POINT)]['color'];
                }
                if (($location->adm_level==Location::LEVEL_PLOT or $location->adm_level==Location::LEVEL_TRANSECT)) {
                  $properties = [
                    'name' => $location->name." [0,0]",
                    'description' => "<div style='font-size: 80%;'>".$html."</div>",
                    'adm_level' => Location::LEVEL_POINT,
                    'color' => $dotcolor,
                    'fill' => $dotcolor,
                    'visibility' => true,
                  ];
                  $geom = $location->start_pointjson;
                  $str = [
                    "type" => "Feature",
                    "geometry" => json_decode($geom),
                    "properties" => $properties,
                  ];
                  $key = $layer_key." [0,0]";
                  $layers[$key][] = $str;
                }
            }

        }
        $extent = ['xmin' => min($longs), 'ymin' => min($lats),'xmax' => max($longs),'ymax' => max($lats)];
        $centroid = [(array_sum($longs)/count($longs)),(array_sum($lats)/count($lats))];
        $result = [];
        //krsort($layers);
        foreach($layers as $key => $features) {
          $thekey = $key;
          if (count($features)==1) {
            $thekey = $features[0]['properties']['name'];
          }
          $result[$thekey] = json_encode([ "type" => "FeatureCollection", "features" => $features]);
        }
        //krsort($result);
        return ['features' => $result,'extent' => $extent,'centroid' => $centroid];
    }


    public function getIsUnnamedPointAttribute()
    {
      $pattern = config('app.unnamedPoint_basename');
      $pattern = "/".$pattern."/i";
      return(preg_match($pattern, $this->name));
    }

    /* DARWIN CORE mutators */
    public function getHigherGeographyLinksAttribute(): string
    {
        if ($this->is_unnamed_point) {
          $path = $this->ancestors();
        } else {
          $path = $this->ancestorsAndSelf();
        }
        $path = $path->noWorld()->select("name","id")->get()->map(function($a) { 
          return $a->rawLink();})->toArray();
        if (count($path)>0) {
          return implode(" > ",$path);
        }
        return '';
    }

    public function getHigherGeographyAttribute(): string
    {
      if ($this->is_unnamed_point) {
        $path = $this->ancestors();
      } else {
        $path = $this->ancestorsAndSelf();
      }
      $path = $path->noWorld()->pluck('name')->toArray();
      if (count($path)>0) {
        return implode(" > ",$path);
      }
      return '';
    }

    public function getHigherGeographyDescAttribute(): string
    {
      if ($this->is_unnamed_point) {
        $path = $this->ancestors();
      } else {
        $path = $this->ancestorsAndSelf();
      }
      $path = $path->noWorld()->select('name','adm_level','id')->get()->sortByDesc('adm_level')->sortByDesc('id')->pluck('name')->toArray();
      if (count($path)>0) {
        return implode(" < ",$path);
      }
      return '';
    }

    public function getSelfParentAttribute()
    {
      $path = $this->getAncestorsAndSelf()->sortByDesc('id')->take(2)->pluck('name')->toArray();
      if (count($path)>0) {
        return implode(" < ",$path);
      }
      return null;
    }




    public function getFootprintWKTAttribute()
    {
      return $this->geom;
    }

    public function getDecimalLatitudeAttribute()
    {
      $this->getLatLong();
      return $this->lat;
    }
    public function getDecimalLongitudeAttribute()
    {
      $this->getLatLong();
      return $this->long;
    }

    public function getLocationRemarksAttribute()
    {
      return $this->notes;
    }
    public function getGeodeticDatumAttribute()
    {
      return $this->datum;
    }

    /* this is not in dwc only, location there but for different purpose */
    public function getLocationNameAttribute()
    {
      return $this->name;
    }

    public function getBasisOfRecordAttribute()
    {
      return 'Location';
    }


    public function parent_without_geom() {
      return $this->parent()->withoutGeom();
    }

    /* uc replacement for related */
    public function relatedLocations()
    {
        return $this->hasMany('App\Models\LocationRelated', 'location_id');
    }

    public function uc()
    {
        return $this->relatedLocations()->whereHas('relatedLocation',function($l){
          $l->where('adm_level',self::LEVEL_UC);
        });
    }

    public static function detectRelated($geom, $adm_level,$idsOnly=false)
    {
        //check only for non administrative levels
        //if (!in_array($adm_level,[Location::LEVEL_PLOT,Location::LEVEL_TRANSECT,Location::LEVEL_POINT])) {
        //  return null;
        //}
        $query = 'ST_Within(ST_GeomFromText(?), geom)';
          //check which registered polygons (except World) of special cases
        $validtypes = [Location::LEVEL_UC,Location::LEVEL_TI,Location::LEVEL_ENV];
        //are possible parents (CONTAIN) of submitted location
        //order by adm_level to get the most inclusive first
        $possibles = Location::select(['id','name','adm_level'])->whereRaw($query, [$geom])->whereIn('adm_level',$validtypes)->noWorld()->orderBy('adm_level', 'desc');

        //if found return the greatest adm_level location found
        if ($possibles->count()) {
            if ($idsOnly) {
                return $possibles->cursor()->pluck('id')->toArray();
            }
            return $possibles->cursor()->map(function($p) { return ['id'=>$p->id,'name'=>$p->name];})->toArray();
        }
        return null;
    }

    public static function fixPathAndRelated($id)
    {
      $location = Location::withGeom()->findOrFail($id);
      $adm_level = $location->adm_level;

      /*
      if (in_array($adm_level,[self::LEVEL_PLOT,self::LEVEL_TRANSECT,self::LEVEL_POINT])) {
        return null;
      }
      */
      //move locations within same parent to this as parent if
      //new location contains the sister location
      $others = Location::where('parent_id',$location->parent_id)->whereNotIn('id',[$location->id]);
      //if any
      if ($others->count() and !in_array($adm_level,[Location::LEVEL_POINT])) {
          if (in_array($adm_level,[Location::LEVEL_TRANSECT])) {
            $buffer_dd = (($location->y*0.00001)/1.11);
            $query_buffer ="ST_Buffer(ST_GeomFromText(?), ".$buffer_dd.")";
            $query = "ST_Within(geom,".$query_buffer.")";
          } else {
            $query = 'ST_Within(geom,ST_GeomFromText(?))';
          }
          $others = $others->whereRaw($query,$location->geom);
          if ($others->count()) {
            $ids = $others->cursor()->pluck('id')->toArray();
            foreach ($ids as $lid) {
              $tofix = Location::findOrFail($lid);
              $oldparentid = $tofix->parent_id;
              $tofix->makeChildOf($location->id);
              $tofix->save();
              sleep(5); //this seems to be important with the baum makeChildOf function to finish
              // TODO: or is a bug within the baum package (replacement required)
              $tofix = Location::findOrFail($lid);
              if ($tofix->parent_id!==$location->id) {
                Log::info($tofix->name." parent is still ".$tofix->parentName);
              }
              //log parent difference
              $tolog = array('attributes' => ['parent_id' => $location->id], 'old' => ['parent_id' => $oldparentid]);
              activity('location')
                ->performedOn($tofix)
                ->withProperties($tolog)
                ->log('parent changed');

             }
          }
      }
      //if special locations are new,
      //then it may contain other registered locations
      //this should be within the same location branch as it parent location
      //if so establish the relationship
      if (in_array($adm_level,[Location::LEVEL_UC,Location::LEVEL_TI,Location::LEVEL_ENV])) {
        //check spatially
        $query = 'ST_Within(geom,ST_GeomFromText(?))';
        $lft = $location->parent->lft;
        $rgt = $location->parent->rgt;
        $within = Location::where('lft','>=',$lft)->where('lft','<=',$rgt)->where('id','<>',$location->id)->cursor()->pluck('id')->toArray();
        $others = Location::select('id')->whereRaw($query,$location->geom)->whereIn('id',$within);

        //are there already any established relationships?
        $existing = $location->relatedLocations->pluck('related_id')->toArray();
        if (count($existing)) {
          $others = $others->whereNotIn('id',$existing);
        }
        //anything is contained and not yet related?
        if ($others->count()) {
          //relate
          foreach ($others->cursor() as $tocreate) {
            $related = new LocationRelated(['related_id' => $location->id]);
            $tocreate->relatedLocations()->save($related);
          }
        }
      }
      return null;
    }

}
