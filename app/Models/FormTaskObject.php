<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormTaskObject extends Model
{
    protected $fillable = ['form_task_id', 'measured_id', 'measured_type','medatada','label','status','search_label'];

    protected $casts = ['medatada' => 'array'];

    public function formTask()
    {
        return $this->belongsTo(FormTask::class);
    }

    public function measured()
    {
        return $this->morphTo();
    }
}