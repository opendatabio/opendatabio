<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherBibreference extends Model
{
    protected $fillable = ['bib_reference_id', 'voucher_id'];
    protected $table = 'voucher_bibreference';

    public function voucher()
    {
        return $this->belongsTo(Voucher::class);
    }

    public function bibreference()
    {
        return $this->belongsTo(BibReference::class,'bib_reference_id');
    }

}
