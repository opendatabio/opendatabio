<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use CodeInc\StripAccents\StripAccents;

use Auth;
use Lang;
use DB;
use App\Models\IndividualLocation;
use App\Models\Location;
use App\Models\Dataset;
use App\Models\ODBRequest;

use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Spatie\MediaLibrary\MediaCollections\Models\Media as BaseMedia;


class Voucher extends Model
{
    use IncompleteDate, LogsActivity;

    protected $fillable = [
      'individual_id', 'biocollection_id', 'biocollection_type',
      'number', 'date', 'notes', 'dataset_id','biocollection_number'];

    //add attributes for automatic use in datatabe
    //protected $appends = ['location_id'];
    //activity log trait (parent, uc and geometry are logged in controller)
    //protected static $logName = 'voucher';
    protected static $recordEvents = ['updated','deleted'];
    //protected static $ignoreChangedAttributes = ['updated_at'];
    //protected static $logFillable = true;
    //$logAttributes = ['name','altitude','adm_level','datum','x','y','startx','starty','notes'];
    //protected static $logOnlyDirty = true;
    //protected static $submitEmptyLogs = false;
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->useLogName('voucher')
        ->logFillable()
        ->logOnlyDirty()
        ->dontLogIfAttributesChangedOnly(['updated_at'])
        ->dontSubmitEmptyLogs();
        //->logOnly(['name', 'text']);
        // Chain fluent methods for configuration options
    }

    public function rawLink($addId = false)
    {
        $text = "<a href='".url('vouchers/'.$this->id)."'>".htmlspecialchars($this->fullname).'</a>';
        if ($addId) {
            if ($this->identification) {
                $text .= ' ('.$this->identification->rawLink().')';
            } else {
                $text .= ' '.Lang::get('messages.unidentified');
            }
        }
        return $text;
    }


    public function getCollectorLinkAttribute()
    {    
        $p = explode(",",$this->recordedByMain)[0];
        $p = StripAccents::strip( (string) $p);
        $p = $p." ".$this->number." ".$this->recorded_date." ".$this->biocollection->acronym;        
        $text = "<a href='".url('vouchers/'.$this->id)."'>".$p.'</a>';
        return $text;
    }

    // for use when receiving this as part of a morph relation
    public function getTypenameAttribute()
    {
        return 'vouchers';
    }


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('datasetScope', function (Builder $builder) {
            // first, the easy cases. No logged in user?
            if (is_null(Auth::user())) {
                return $builder->whereRaw('((vouchers.dataset_id IS NULL) OR vouchers.id IN (SELECT p1.id FROM vouchers AS p1 JOIN datasets ON datasets.id = p1.dataset_id WHERE datasets.privacy >='.Dataset::PRIVACY_PUBLIC.'))');
            }
            // superadmins see everything
            if (User::ADMIN == Auth::user()->access_level) {
                return $builder;
            }
            // now the complex case: the regular user
            return $builder->whereRaw('((vouchers.dataset_id IS NULL) OR vouchers.dataset_id IN (SELECT dts.id FROM datasets as dts JOIN dataset_user as dtu ON dtu.dataset_id=dts.id WHERE (dts.privacy >='.Dataset::PRIVACY_REGISTERED.') OR dtu.user_id='.Auth::user()->id.'))');
          });

        // before delete() method call this
        static::deleting(function($voucher) {
            //$voucher->bibreferences->each->delete();
            $voucher->bibreferences()->detach();
            $voucher->activities->each->delete();
            $voucher->odbrequests->each->delete();
            $voucher->collectors->each->delete();
        });



    }



    public function bibreferences()
    {
        return $this->belongsToMany(BibReference::class,'voucher_bibreference')->withTimestamps();
    }



    //get individual the voucher belongs to
    public function individual()
    {
      return $this->belongsTo(Individual::class)->withoutGlobalScopes();
    }


    //voucher location is the location of the individual (the closes in date)
    //this direct functions are to GET data only, not to set the location for a voucher
    public function locations()
    {

      return $this->belongsToMany(Location::class,'individual_location','individual_id','location_id','individual_id','id')
        ->withPivot(['date_time','notes','altitude','relative_position','first','global_position'])
        ->withTimestamps();
    }

    public function locations_simple_for_search()
    {
      return $this->belongsToMany(
        Location::class,'individual_location','individual_id','location_id','individual_id','id');
    }

    public function current_location()
    {
      return $this->hasOne(IndividualLocation::class,'individual_id', 'individual_id')
      ->withoutGlobalScopes()
      ->withXy()
      ->orderBy('individual_location.id', 'desc');
    }
   
    public function getLocationDisplayAttribute()
    {
      $theloc = $this->current_location;
      $location = $theloc->location->fullname;
      $coordinates = $theloc->coordinatesSimple;
      $altitude = $theloc->altitude ? $theloc->altitude : $theloc->location->altitude;
      $altitude = ($altitude != "" and null != $altitude) ? "<br>".$altitude."m.a.s.l." : null;
      $related = $theloc->location->relatedLocations();
      if ($related->count()) {
        $rids = $related->cursor()->map(function($r) {
          $txt = $r->relatedLocation->rawLink();
          $txt = ($r->relatedLocation->adm_level==Location::LEVEL_TI) ? Lang::get('levels.adm_level.'.Location::LEVEL_TI).": ".$txt : $txt;
          return $txt;
        })->toArray();
        $related = "<br>".implode(" | ",$rids);
      } else {
        $related = "<br>";
      }
      //remove point name if unnamedPoint_basename
      $basename = config('app.unnamedPoint_basename');
      $pattern = "/".$basename."_[0-9]*/i";
      if (preg_match($pattern,$location)) {
        $pattern = "/> ".$basename."_[0-9]*/i";
        $matches =  preg_split($pattern, $location);
        $location = isset($matches[0]) ? $matches[0] : $location;
      }
      return $location.$related.$coordinates.$altitude;
    }

    public function getCoordinatesPrecisionAttribute()
    {

      return strip_tags($this->current_location->location->precision);
    }


    //IDENTIFICATION -similarly, vouchers identification is that of the inidividual
    public function identification()
    {
      return $this->hasOneThrough(
                    'App\Models\Identification',
                    'App\Models\Individual',
                    'identification_individual_id', // Foreign key on individual table...
                    'object_id', // Foreign key on identification table...
                    'individual_id', // Local key on voucher table...
                    'id' // Local key on individual table...
                    )->where('object_type', 'App\Models\Individual');
    }

    public function getTaxonPublishedStatusAttribute()
    {
      return $this->identification ? $this->identification->taxon->published_status : 'unidentified';
    }

    public function getFullnameAttribute() : string { 
      $p = explode(",",$this->recordedByMain)[0];
      $p = StripAccents::strip( (string) $p);
      $res = [$this->record_number,$p,$this->biocollection->acronym,$this->biocollection->number];
      $res = array_filter($res);
      $res = implode(".",$res);
      $res = str_replace(" ","-",$res);
      return $res;
    }
    
    public function getCollectorNumberAttribute() : string { 
      $p = $this->recordedByMain;
      $tag = $this->recordNumber;
      return $tag." ".$p;
    }

    public function getSelectorLabelAttribute()
    {
      $label = [$this->recordNumber];
      $label[]=$this->recordedByMain;
      $location_type = $this->current_location->adm_level;
      if (!$location_type==Location::LEVEL_POINT)
      {
          $label[] = $this->location_name;
      } 
      $label[] = $this->location_parent_name;
      $label[] = $this->scientificName;
      return implode(" - ",$label);
    }

    public function measurements()
    {
        return $this->morphMany(Measurement::class, 'measured');
    }




  /*  VOUCHER COLLECTOR AND UNIQUE IDENTIFIERS
    ** vouchers may have it own collector (SELF)
    ** OR is the same of that of the individual (INDIVIDUAL COLLECTOR AND NUMBER)
  */
    //GET ONLY - RELATIONSHIPS
    public function collectors()
    {
        return $this->morphMany(Collector::class, 'object')->with('person');
    }

    public function collectors_individual()
    {
        return $this->hasMany(Collector::class, 'object_id','individual_id')
        ->with('person')
        ->where("object_type", "like", "%Individual");
    }
    public function collector_main()
    {
        return $this->collectors()->with('person')->where('main',1);
    }


    /* DATASET */
    public function dataset()
    {
        return $this->belongsTo(Dataset::class);
    }
    public function getDatasetNameAttribute()
    {
        if ($this->dataset) {
            return $this->dataset->name;
        }
        return 'Unknown dataset';
    }

    /* BIOCOLLECTION RELATIONS */
      public function biocollection()
      {
        return $this->belongsTo(Biocollection::class);
      }



      public function getIsTypeAttribute()
      {
        if ($this->biocollection_type>0) {
          return Lang::get('levels.vouchertype.1');
        } else {
          return Lang::get('levels.vouchertype.0');
        }
      }


    public function getOrganismIDAttribute()
    {
      return $this->individual->fullname;
    }

    /*
      * MEDIA FILES
      * image vouchers are linked to individuals with voucher_id added to
      * custom_properties
    */
    public function media()
    {
        $searchStr = 'voucher_id":'.$this->id;
        return $this->individual
          ->media()
          ->where('custom_properties','like','%'.$searchStr.'%');
    }



    public static function getTableName()
    {
        return (new self())->getTable();
    }



    /* NAME Used in ActivityDataTable change display */
    public function identifiableName()
    {
        return $this->fullname;
    }

    /*dwc terms */
    public function getOccurrenceIDAttribute()
    {
      return $this->fullname;
    }

    public function getCollectionCodeAttribute()
    {
      return $this->biocollection->acronym;
    }
    public function getCatalogNumberAttribute()
    {
      return $this->biocollection_number;
    }

    public function getRecordedByMainAttribute()
    {
        if ($this->collector_main()->count()) {
            return $this->collector_main()->first()->person->abbreviation;
        }
        return $this->collectors_individual()->where('main',1)->first()->person->abbreviation;
    }

    public function getRecordNumberAttribute()
    {
      if ($this->number) {
          return $this->number;
      }
      return $this->individual->tag;
    }


    //full list of collectors for display and exports, pipe delimited
    public function getRecordedByAttribute()
    {        
        $collectors = $this->collectors()->get();
        if ($collectors->count()==0) {
          $collectors = $this->collectors_individual()->get();
        }
        $persons = $collectors->map(function($q) { 
          return $q->person->abbreviation;
        })->toArray();
        $persons = implode(' | ',$persons);
        return $persons;
    }


    public function getRecordedDateAttribute()
    {
      if ($this->number) {
        return $this->formatDate;
      } else {
        return $this->individual->formatDate;
      }
    }

    /* IDENTIFICATION ATTRIBUTES ASSESSORS */
    public function getScientificNameAttribute()
    {
        if ($this->identification) {
            return $this->identification->scientific_name;
        }
        return Lang::get('messages.unidentified');
    }
    public function getFamilyAttribute()
    {
        if ($this->identification) {
            return $this->identification->family;
        }
        return Lang::get('messages.unidentified');
    }
    public function getGenusAttribute()
    {
        if ($this->identification) {
            return $this->identification->genus;
        }
        return Lang::get('messages.unidentified');
    }
    
    public function getIdentificationQualifierAttribute() : string
    {
      if ($this->identification) {
        return $this->identification->identification_qualifier;
      }
      return "";
    }

    public function getScientificNameWithAuthorAttribute()
    {
        if ($this->identification) {
            return $this->identification->scientificNameWithAuthor;
        }
        return Lang::get('messages.unidentified');
    }
    
    public function getDateIdentifiedAttribute()
    {
      if ($this->identification) {
          return $this->identification->date;
      }
      return null;
    }
    public function getIdentifiedByAttribute()
    {
      if ($this->identification) {
          return $this->identification->identified_by;
      }
      return Lang::get('messages.unidentified');
    }
    public function getIdentificationRemarksAttribute()
    {
      if ($this->identification) {
          return $this->identification->identification_remarks;          
      }
      return "";
    }

    public function getLocationNameAttribute()
    {
      return $this->current_location->location->name;
    }

    public function getLocationParentNameAttribute()
    {
      return $this->current_location->location->parent->name;
    }


    public function getHigherGeographyAttribute()
    {
      return $this->current_location->higher_geography;
    }
    public function getHigherGeographyDescAttribute()
    {
      return $this->current_location->higher_geography_desc;
    }
    
    public function getLatLong()
    {
        // if the "cached" values are already set, do nothing
        if ($this->long or $this->lat) {
           return;
        }
        // all others, extract from centroid
        $raw_coords = $this->current_location->globalPosition;
        $coords = Location::latlong_from_point($raw_coords);
        $this->long = $coords[1];
        $this->lat = $coords[0];
    }
    public function getDecimalLatitudeAttribute()
    {
      $this->getLatLong();
      return (float) $this->lat;
    }
    public function getDecimalLongitudeAttribute()
    {
      $this->getLatLong();
      return (float) $this->long;
    }

    public function getGeoreferenceRemarksAttribute()
    {
      return $this->current_location->location->georeferenceRemarks;
    }


    public function getOccurrenceRemarksAttribute()
    {
      $raw_vnote = $this->notes;
      $vjson = false;
      $vnote = [];
      if ($raw_vnote) {
        $vnote = json_decode($raw_vnote,true);
        $vjson = (json_last_error() === JSON_ERROR_NONE and is_array($vnote));
      }
      $ijson = false;
      $inote = [];
      $raw_inote = $this->individual->notes;
      if ($raw_inote) {
        $inote = json_decode($raw_inote,true);
        $ijson = (json_last_error() === JSON_ERROR_NONE and is_array($inote));
      }
      if ($vjson and $ijson) {
        return json_encode(array_merge($vnote,$inote));
      }
      if ($vjson) {
        return json_encode($vnote);
      }
      if ($ijson) {
        return json_encode($inote);
      }      
      return $this->notes;
    }

    public function getFormatedNotesAttribute()
    {
      return ODBFunctions::formatNotes($this->notes);
    }

    public function getTypeStatusAttribute()
    {
      return Lang::get('levels.vouchertype.'.$this->biocollection_type);
    }

    public function getAssociatedMediaAttribute()
    {
      //return null;
      $media = $this->media;
      if ($media->count()) {
      $result = $media->map(function($v){
                return url('media/'.$v->id);
      })->toArray();
      return implode(" | ",$result);
      }
      return null;
    }
    public function getAccessRightsAttribute()
    {
      if ($this->dataset) {
        return $this->dataset->accessRights;
      }
      return "Open access";
    }
    public function getBibliographicCitationAttribute()
    {
      if ($this->dataset) {
        return $this->dataset->bibliographicCitation;
      }
      return null;
    }
    public function getBasisOfRecordAttribute()
    {
      return 'PreservedSpecimens';
    }
    public function getLicenseAttribute()
    {
      if ($this->dataset) {
        return $this->dataset->dwcLicense;
      }
      return null;
    }

    //user requests associated with this voucher
    public function odbrequests()
    {
      return $this->belongsToMany(ODBRequest::class,'voucher_request','voucher_id','request_id')->withPivot(['status','notes'])->withTimestamps();
    }
    public function getRequestNotes($id=null)
    {
      if ($id===null) {
        return null;
      }
      $notes = $this->odbrequests()->where('request_id',$id);
      //$ids = $this->odbrequests()->wherePivot('request_id', $id)->pluck('notes')->toArray();
      if ($notes->count()>0) {
        $notes = $notes->first()->pivot->notes;
        if (null == $notes) {
          return null;
        }
        $notes = json_decode($notes);
        $text = [];
        foreach ($notes as $key => $note) {
          //$text[] = $note->notes;
          $user_id = $note->user_id;
          $user = User::findOrFail($user_id);
          $person = isset($user->person) ? $user->person->rawLink() : $user->email;
          $text[] =  $note->notes.'  <small>[By '.$person." on ".$note->date.']</small>';
        }
        if (count($text)) {
          $notes = "<ul><li>".implode("</li><li>",$text)."</li></ul>";
          if (count($text)==1) {
            return $notes;
          }
          $ret =  '<i class="far fa-comments fa-2x" style="cursor:pointer;"></i>';
          $id = "request_note_".$this->id;
          $ret .= '<br><div id="'.$id.'" hidden >'.$notes.'</div>';
          return $ret;
        }
      }
      return null;
    }

}
