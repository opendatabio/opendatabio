<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormTask extends Model
{
    protected $fillable = ['name', 'form_id','medatada','progress_max','progress','status'];

    protected $casts = ['medatada' => 'array'];

    public function rawLink()
    {
        $url = route('form_tasks.show',['form_task_id' => $this->id]);
        return "<a href='".$url."'>".htmlspecialchars($this->name).'</a>';
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    public function taskObjects()
    {
        return $this->hasMany(FormTaskObject::class);
    }

    public function getIsPreparedAttribute()
    {        
        return $this->status=="prepared";
    }

    public function getProgressDisplayAttribute()
    {
        if($this->progress_max == 0) {
            return null;
        }
        $records = $this->progress."/".$this->progress_max;
        if($this->progress == 0)
        {
            $perc = 0;
        } else {
            $perc =  round(($this->progress/$this->progress_max)*100,1);
        }
        return $records." (".$perc."%)";        
    }

    public function getDatasetIdsAttribute()
    {
        $metadata = $this->metadata ? json_decode($this->metadata,true) : null;
        if ($metadata['datasets']) {
            $ids = collect($metadata['datasets'])->pluck('id')->toArray();          
            return $ids;
        }
        return null;
    }
    public function getAuthorizedUsersAttribute()
    {
        $users = $this->users->map(function($q){
            return $q->identifiableName();
        })->toArray();
        if ($users) {
            return implode("<br>",$users);
        }        
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        // before delete() method call this
        static::deleting(function($task) {
            $task->users()->detach();
            $task->taskObjects()->delete();            
        });

    }

}
