<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class DatasetVersion extends Model
{
    
    protected $fillable = [
        'dataset_id',
        'version',
        'dataset_access',
        'license',
        'license_version',
        'policy',
        'citation',
        'notes',
        'filters',
        'metadata',
        'date',
        'uuid',
      ];
  
    public function getRawLinkAttribute(): string
    {
        return "<a href='".url('dataset-version-show/'.$this->id)."' 
        data-bs-toggle='tooltip' rel='tooltip' data-bs-placement='right' 
        title='Dataset version data'>".htmlspecialchars($this->version).'</a>';

    }

    protected static function boot()
    {
        parent::boot();
        
        // before delete() method call this
        static::deleting(function($version) {
            /* if it has been assigned as a specialist */
            $version->persons()->detach();
            $data_files = $version->files;
            foreach($data_files as $file) 
            {
                Storage::disk('datasets')->delete($file);
            }
            // do the rest of the cleanup...
        });
    }



    public function dataset()
    {
        return $this->belongsTo(Dataset::class);
    }  

    public function persons()
    {
        return $this->belongsToMany(Person::class,'dataset_version_person')
        ->withTimestamps()->orderBy('dataset_version_person.id');
    }

    public function getCitationPrintAttribute() : string
    {
        if (!$this->citation) {
            return "";
        }
        $bib = json_decode($this->citation,true);
        $authors = explode("AND",$bib['author']);
        if (count($authors)>3) {
            $authors_short = $authors[0]." et al.";
        } else {
            $authors_short = count($authors) ? implode(", ",$authors) : '';
        }
        $citation = $authors_short." (".$bib['year']."). <strong>".$bib['title']."</strong>. Dataset Version: ".$bib['version'].".";
        if ($bib['license']) {
            $citation .= " ".$bib['license'];
        } 
        if ($bib['url']) {
            $citation .= ". Published online ".$bib['url'];
        }
        return $citation;
    }
    
    public function getFiltersShowAttribute() : array
    {
        $data_filter = $this->filters ? json_decode($this->filters,true) : null;
        if (!$data_filter) {
            return [];
        }        
        $taxon_root = isset($data_filter['selected_taxons']) ? implode(',',collect( $data_filter['selected_taxons'])->pluck('label')->toArray()) : null;
        $location_root = isset($data_filter['selected_locations']) ? implode(',',collect( $data_filter['selected_locations'])->pluck('label')->toArray()) : null;
        $persons = isset($data_filter['persons']) ? implode(',',collect( $data_filter['persons'])->pluck('label')->toArray()) : null;
        $traits = isset($data_filter['selected_traits']) ? implode(',',collect( $data_filter['selected_traits'])->pluck('label')->toArray()) : null;
        $min_date = isset($data_filter['filter_min_date']) ? $data_filter['filter_min_date'] : null;
        $max_date = isset($data_filter['filter_max_date']) ? $data_filter['filter_max_date'] : null;           
        $filter_params = [
            'Taxon' => $taxon_root,
            'Location' => $location_root,
            'Person' => $persons,
            'DateFrom' => $min_date,
            'DateTo' => $max_date,
            'Trait' => $traits,
        ];     
        $filter_params = array_filter($filter_params);
        return $filter_params;
    }

    public function getBibtexPrintAttribute() : string
    {
        if (!$this->citation) {
            return '';
        }
        $bib = json_decode($this->citation,true);        
        $bibc = "@Misc{".$bib['bibkey'].",\n";
        unset($bib['bibkey']);
        foreach($bib as $k => $v) {
            $bibc .= $k." = {".$v."},\n";
        }
        $bibc .= "}";
        return $bibc;
    }

    public function getFilesAttribute(): array
    {
        if ($this->metadata) {
            $data = json_decode($this->metadata,true);
            if (isset($data['files'])) {
                return $data['files'];
            }
        }
        return [];
    }


    
}
