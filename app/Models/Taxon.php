<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Models;

use Baum\Node;
use DB;
use Lang;
use App\Models\Measurement;
use App\Models\Identification;
use Activity;
use Illuminate\Support\Arr;

use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

use Spatie\MediaLibrary\MediaCollections\Models\Media as BaseMedia;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;


class Taxon extends Node implements HasMedia
{

    use InteractsWithMedia, LogsActivity;

    public $table = "taxons";
    protected $leftColumnName = 'lft';
    protected $rightColumnName = 'rgt';
    protected $depthColumnName = 'depth';


    protected $fillable = ['name', 'level', 'valid', 'validreference', 'senior_id', 'author', 'author_id',
                'bibreference', 'bibreference_id', 'parent_id', 'notes', ];

    //protected $appends = ['family','genus','higher_classification','fullname'];

    //activity log trait (parent, uc and geometry are logged in controller)
    //protected static $logName = 'taxon';
    protected static $recordEvents = ['updated','deleted'];
    //protected static $ignoreChangedAttributes = ['updated_at','lft','rgt','depth'];
    //protected static $logFillable = true;
    //$logAttributes = ['name','altitude','adm_level','datum','x','y','startx','starty','notes'];
    //protected static $logOnlyDirty = true;
    //protected static $submitEmptyLogs = false;
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->useLogName('taxon')
        ->logFillable()
        ->logOnlyDirty()
        ->dontLogIfAttributesChangedOnly(['updated_at','lft','rgt','depth'])
        ->dontSubmitEmptyLogs();      
    }

    // for use when receiving this as part of a morph relation
    // TODO: maybe can be changed to get_class($p)?
    public function getTypenameAttribute()
    {
        return 'taxons';
    }

    public function rawLink()
    {
        return "<em><a href='".url('taxons/'.$this->id)."'>".htmlspecialchars($this->qualifiedFullname).'</a></em>';
    }


    protected static function boot()
    {
        parent::boot();

        // before delete() method call this
        static::deleting(function($taxon) {
            $taxon->reference()->delete();
            $taxon->externalrefs->each->delete();
            $taxon->activities->each->delete();
        });

    }

    public function setFullnameAttribute($value)
    {
        // Full names have only the first letter capitalized
        $value = ucfirst(strtolower($value));

        if ($this->level <= 200) {
            $this->name = ucfirst(mb_strtolower($value));
        }
        if ($this->level >= 210) {
            // sp. or below, strips evertyhing before the last space
            //then name cannot have spaces, otherwise may upload errors.
            $this->name = mb_strtolower(trim(substr($value, strrpos($value, ' ') - strlen($value))));
        }
    }


    /* similarly to Location, this will be the root of the taxon tree */
    public function scopeNoRoot($query)
    {
        return $query->where('level', '<>', -1);
    }

    public static function lifeRoot()
    {
        return self::where('level', -1)->get()->first();
    }


    // for use in selects, lists the most common tax levels
    public static function TaxLevels()
    {
        return [0, 10, 30, 40, 60, 70, 80, 90, 100, 120, 130, 150, 180, 190, 210, 220, 240, 270, -100];
    }

    /*
    public function newQuery($excludeDeleted = true)
    {
        // includes the full name of a taxon in all queries
        return parent::newQuery($excludeDeleted)->addSelect(
                '*',
                DB::raw('odb_txname(taxons.name, taxons.level, taxons.parent_id,0,0) as fullname'),
                DB::raw('odb_txname(taxons.name, taxons.level, taxons.parent_id,0,1) as scientificName'),
                DB::raw('odb_txparent(taxons.lft,120) as family'),
            );
    }
    */

    public function getDescendantsCountAttribute()
    {
        $n_descendants =  self::select("id")->where('lft',">",$this->lft)->where('lft',"<",$this->rgt)->count();
        return $n_descendants;
    }

    public function mediaDescendantsAndSelf()
    {
        //$ids =  self::select("id")->where('lft',">=",$this->lft)->where('lft',"<=",$this->rgt)->pluck('id')->toArray();
        $ids = $this->all_media_ids();
        return Media::whereIn('id',$ids);
    }

    public function individualsMedia()
    {
        return $this->hasManyThrough(
                      'App\Models\Media',
                      'App\Models\Identification',
                      'taxon_id', // Foreign key on identification table...
                      'model_id', // Foreign key on media table...
                      'id', // Local key on taxon table...
                      'object_id' // Local key on identification table...
                      )->where('identifications.object_type', 'App\Models\Individual')->where('media.model_type', 'App\Models\Individual');
    }


    /* THIS MAY BE ELIMIATED AND MEDIA VOUCHER SHOULD BE LINKED TO INDIVIDUAL WITH voucher_id as custom property
    public function voucherMedia()
    {
        return $this->hasManyThrough(
                          'App\Models\Media',
                          'App\Models\Identification',
                          'taxon_id', // Foreign key on identification table...
                          'model_id', // Foreign key on media table...
                          'id', // Local key on taxon table...
                          'object_id' // Local key on identification table...
                          )->where('identifications.object_type', 'App\Models\Voucher')->where('media.model_type', 'App\Models\Voucher');
    }
    */

    public function getTaxonRankAttribute()
    {
        return Lang::get('levels.tax.'.$this->level,[],'en');
    }

    public function getQualifiedFullnameAttribute()
    {
        return ($this->valid ? '' : '**').$this->scientificName;
    }

    public function identifiableName()
    {
        return $this->getQualifiedFullnameAttribute();
    }

    public function measurements()
    {
        return $this->morphMany("App\Models\Measurement", 'measured');
    }

    // returns rank numbers from common abbreviations
    public static function getRank($rank)
    {
        $rank = strtolower($rank);
        switch ($rank) {
                case 'kingdom':
                    return 0;
                case 'subkingdom':
                case 'subkingd.':
                    return 10;
                case 'div.':
                case 'phyl.':
                case 'phylum':
                case 'division':
                        return 30;
                case 'subdiv.':
                case 'subphyl.':
                case 'subphylum':
                case 'subdivision':
                        return 40;
                case 'cl.':
                case 'class':
                        return 60;
                case 'subcl.':
                case 'subclass':
                        return 70;
                case 'superord.':
                case 'superorder':
                        return 80;
                case 'ord.':
                case 'order':
                        return 90;
                case 'subord.':
                case 'suborder':
                        return 100;
                case 'fam.':
                case 'family':
                        return 120;
                case 'subfam.':
                case 'subfamily':
                        return 130;
                case 'tr.':
                case 'tribe':
                        return 150;
                case 'gen.':
                case 'genus':
                        return 180;
                case 'subg.':
                case 'subgenus':
                case 'sect.':
                case 'section':
                        return 190;
                case 'sp.':
                case 'spec.':
                case 'species':
                        return 210;
                case 'subsp.':
                case 'subspecies':
                        return 220;
                case 'var.':
                case 'variety':
                        return 240;
                case 'f.':
                case 'fo.':
                case 'form':
                    return 270;
                case 'clade':
                    return -100;
                default:
                    return null;
                }
    }

    public function author_person()
    {
        return $this->belongsTo('App\Models\Person', 'author_id');
    }

    public function getPublishedStatusAttribute()
    {
      return $this->author_id ? "unpublished" : "published";
    }

    function getFullnameAttribute() : string {
      $level = $this->level;
      if ($level<=190)  {
        return $this->name;
      }
      if ($this->level==210) {
        return $this->parent->name." ".$this->name;        
      }
      return $this->parent->parent->name." ".$this->parent->name." ".$this->name;             
    }

    public function getFullnameWithAuthor()
    {
      return $this->fullname." ".$this->scientificNameAuthorship;
    }


    public function reference()
    {
        return $this->belongsTo('App\Models\BibReference', 'bibreference_id');
    }

    public function senior()
    {
        return $this->belongsTo('App\Models\Taxon', 'senior_id');
    }

    public function juniors()
    {
        return $this->hasMany('App\Models\Taxon', 'senior_id');
    }


    public function individuals()
    {
        return $this->hasManyThrough(
                      'App\Models\Individual',
                      'App\Models\Identification',
                      'taxon_id', // Foreign key on identification table...
                      'identification_individual_id', // Foreign key on individual table...
                      'id', // Local key on taxon table...
                      'object_id' // Local key on identification table...
                      )->where('object_type', 'App\Models\Individual');
    }

    public function vouchers()
    {
      return $this->hasManyThrough(
                    'App\Models\Voucher',
                    'App\Models\Identification',
                    'taxon_id', // Foreign key on identification table...
                    'individual_id', // Foreign key on voucher table...
                    'id', // Local key on taxon table...
                    'object_id' // Local key on identification table...
                    )->where('object_type', 'App\Models\Individual');
    }


    // For specialists
    public function persons()
    {
        return $this->belongsToMany('App\Models\Person');
    }

    public function scopeValid($query)
    {
        return $query->where('valid', '=', 1);
    }

    public function scopeLeaf($query)
    {
        // partially reimplemented from Etrepat/Baum
        $grammar = $this->getConnection()->getQueryGrammar();
        $rgtCol = $grammar->wrap($this->getQualifiedRightColumnName());
        $lftCol = $grammar->wrap($this->getQualifiedLeftColumnName());

        return $query->where('level', '>=', 210)->whereRaw($rgtCol.' - '.$lftCol.' = 1');
    }

    // Functions for handling API keys
    public function setapikey($name, $reference)
    {
        $refs = $this->externalrefs()->where('name', $name);
        $oldref = $this->externalrefs()->select(['name','reference'])->where('name',$name);
        if ($reference) { // if reference is set...
            if ($refs->count()) { // and we have one already, update it
                $refs->update(['reference' => $reference]);
            } else { // none already, so create one
                $this->externalrefs()->create([
                    'name' => $name,
                    'reference' => $reference,
                ]);
            }
        } else { // no reference set
            if ($refs->count()) { // if there was one, delete it
                $refs->delete();
            }
        }

        $newref = $this->externalrefs()->select(['name','reference'])->where('name',$name);

        //if there is change, then log
        if ($newref->count() and $oldref->count()) {
          $newref = $newref->first()->toArray();
          $oldref = $oldref->first()->toArray();
          $old = array_diff_assoc($oldref,$newref);
          $new= array_diff_assoc($newref,$oldref);
          if (count($old)>0 || count($new)>0 ) {
            $tolog = array('attributes' => $newref, 'old' => $oldref);
            activity('taxon')
              ->performedOn($this)
              ->withProperties($tolog)
              ->log('taxon ExternalAPIs changed');
            }
        }
    }

    public function externalrefs()
    {
        return $this->hasMany('App\Models\TaxonExternal', 'taxon_id');
    }

    public function getMobotAttribute()
    {
        foreach ($this->externalrefs as $ref) {
            if ('Mobot' == $ref->name) {
                return $ref->reference;
            }
        }
    }

    public function getIpniAttribute()
    {
        foreach ($this->externalrefs as $ref) {
            if ('IPNI' == $ref->name) {
                return $ref->reference;
            }
        }
    }

    public function getMycobankAttribute()
    {
        foreach ($this->externalrefs as $ref) {
            if ('Mycobank' == $ref->name) {
                return $ref->reference;
            }
        }
    }

    public function getGbifAttribute()
    {
        foreach ($this->externalrefs as $ref) {
            if ('GBIF' == $ref->name) {
                return $ref->reference;
            }
        }
    }

    public function getZoobankAttribute()
    {
        foreach ($this->externalrefs as $ref) {
            if ('ZOOBANK' == $ref->name) {
                return $ref->reference;
            }
        }
    }


    public function identifications()
    {
        return $this->hasMany("App\Models\Identification");
    }


    // returns: mixed. May be string if not found in DB, or int (id) if found, or null if no query possible
    public static function getParent($name, $rank, $family)
    {
        switch (true) {
            case $rank <= 120:
                return null; // Family or above, MOBOT has no parent information
            case $rank > 120 and $rank <= 180: // sub-family to genus, parent should be a family
                if (is_null($family)) {
                    return null;
                }
                $searchstr = $family;
                break;
            case $rank > 180 and $rank <= 210: // species, parent should be genus
                $searchstr = substr($name, 0, strpos($name, ' '));
                break;
            case $rank > 210 and $rank <= 280: // subsp, var or form, parent should be a species
                preg_match('/^(\w+\s\w+)\s/', $name, $match);
                $searchstr = trim($match[1]);
                break;
            default:
                return null;
            }
        $toparent = self::whereRaw('odb_txname(taxons.name, taxons.level, taxons.parent_id,0,0) = ?', [$searchstr]);
        if ($toparent->count()) {
            return [$toparent->first()->id, $searchstr];
        } else {
            $toparent = self::whereRaw('odb_txname(taxons.name, taxons.level, taxons.parent_id,0,1) = ?', [$searchstr]);
            if ($toparent->count()) {
                return [$toparent->first()->id, $searchstr];
            } else {
                return $searchstr;
            }
        }
    }

    public function getParentNameAttribute()
    {
      if ($this->parent) {
        return $this->parent->fullname;
      }
      return null;
    }

    public function parentByLevel($level)
    {
      return  DB::table('taxons')->where('lft',"<=",$this->lft)->where('rgt',">=",$this->lft)->where('level',$level)->get();
      // code...
    }


    public function getGenusAttribute() : string
    {
       
        if ($this->level < $this->getRank('genus')) {
            return '';
        }
        if ($this->getRank('genus') == $this->level) {
            return $this->name;
        }
        $genus = $this->ancestors()->where('level',$this->getRank('genus'));
        if ($genus->count()) {
          return $genus->first()->name;
        }
        return "";
    }

    public function getFamilyAttribute() : string
    {
        
        if ($this->level < $this->getRank('family')) {
            return '';
        }
        if ($this->getRank('family') == $this->level) {
            return $this->name;
        }
        $genus = $this->ancestors()->where('level',$this->getRank('family'));
        if ($genus->count()) {
          return $genus->first()->name;
        }
        return "";
    }


    public function getSpeciesAttribute()
    {
        if ($this->level >= $this->getRank('species')) {
            return $this->fullname;
        }
        return null;
    }

    public function familyRawLink()
    {
        if ($this->level < 120) {
            return '';
        }
        if (120 == $this->level) {
            return $this->name;
        }
        // else
        $parent = $this->parent;
        while ($parent->parent and $parent->level > 120) {
            $parent = $parent->parent;
        }

        return $parent->rawLink();
    }

    public function familyModel()
    {
      $id = $this->getAncestorsAndSelf()->where('level',$this->getRank('family'))->pluck('id')->toArray();
      if ($id) {
        return Taxon::whereIn('id',$id)->first();
      } else {
        return null;
      }
    }

    public function genusModel()
    {
      $id = $this->getAncestorsAndSelf()->where('level',$this->getRank('genus'))->pluck('id')->toArray();
      if ($id) {
        return Taxon::whereIn('id',$id)->first();
      } else {
        return null;
      }
    }

    public function references()
    {
        return $this->belongsToMany(BibReference::class,'taxon_bibreference')->withTimestamps();
    }






    /* this are DARWIN CORE TERMS translations for the API */
    /* https://dwc.tdwg.org/terms/#dwc:scientificNameAuthorship */
    public function getScientificNameAuthorshipAttribute()
    {
        if ($this->author) {
            return $this->author;
        }
        $person = $this->author_person;
        if ($person) {
            return isset($person->full_name) ? $person->full_name." - ".$person->abbreviation : $person->abbreviation;        
        }
        return '';
    }

    //here without authors
    //added to newQuery
    public function getScientificNameAttribute()
    {
      return $this->fullname;
    }


    //external ids
    public function getScientificNameIDAttribute() : string
    {
        $refs = $this->externalrefs()->get();
        $external = "";
        if ($refs->count()>0) {
          $external = [];
          $urls = config('external-apis');
          foreach($refs as $rf) {
              $db = mb_strtolower($rf->name);
              $id = $rf->reference;
              if ($db=='mobot') {
                $linkto = $urls['tropicos']['linkto'];
                $pattern = "/urn:lsid:tropicos.org:taxon:|tro-|trop-/i";
                $id = preg_replace($pattern, '', $id);
              } else {
                $linkto = $urls[$db]['linkto'];
              }
              $external[] = $linkto.$id;
          }
          $external = implode(" | ",$external);
      }
      return $external;
    }


    public function getAcceptedNameUsageAttribute()
    {
      if ($this->senior()->count()) {
        return $this->senior->fullname;
      }
      return null;
    }

    public function getAcceptedNameUsageIDAttribute()
    {
      if ($this->senior()->count()) {
        return $this->senior->scientificNameID;
      }
      return null;
    }

    public function getParentNameUsageAttribute()
    {
        return $this->parentName;
    }

    public function getNamePublishedInAttribute()
    {
        if ($this->bibreference) {
            return $this->bibreference;
        }
        if ($this->reference) {
            return $this->reference->bibreferenceSimple;
        }
    }
    public function getNamePublishedInIDAttribute()
    {
        if ($this->bibreference) {
            return null;
        }
        if ($this->reference) {
            return ($this->reference->doi!=null ? $this->reference->doi : $this->reference->url);
        }
    }
    public function getNamePublishedInYearAttribute()
    {
        if ($this->bibreference) {
          $pattern = "/(\d{4})/i";
          $result = preg_match_all($pattern,$this->bibreference,$matches);
          if (count($matches[0])==1) {
            return $matches[0][0];
          }
          return null;
        }
        if ($this->reference) {
            return ($this->reference->doi!=null ? $this->reference->doi : $this->reference->url);
        }
    }
    public function getHigherClassificationAttribute()
    {
      $path = $this->ancestors()->where('level','<>','-1')->get()->map(function($t){
        return $t->fullname;
      })->toArray();
      if (count($path)>0) {
        return implode(" > ",$path);
      }
      return '';
    }
  
    public function getHigherClassificationDescAttribute()
    {
      $path = $this->ancestors()->where('level','<>','-1')->get()->map(function($t){
        return [
          'level' => $t->level,
          'name' => $t->fullname,
        ];
      })->sortByDesc('level')->pluck('name')->values()->toArray();
      if (count($path)>0) {
        return implode(" < ",$path);
      }
      return null;
    }
    


    public function getTaxonomicStatusAttribute()
    {
      $valid = $this->valid;
      $hasperson = $this->author_person()->count()>0;
      if (!$valid and $hasperson) {
        return 'unpublished and invalid';
      }
      if ($hasperson) {
        return 'unpublished';
      }
      if (!$valid) {
        return 'invalid';
      }
      return 'accepted';
    }
    public function getTaxonRemarksAttribute()
    {
      return $this->notes;
    }

    public function getFormatedNotesAttribute()
    {
      return ODBFunctions::formatNotes($this->notes);
    }


    /* FUNCTIONS TO INTERACT WITH THE COUNT MODEL */
    public function summaryCounts()
    {
        return $this->morphMany("App\Models\Summary", 'object');
    }


    public function getCount($scope="all",$scopeId=null,$target='individuals')
    {

      if (Identification::count()==0) {
        return 0;
      }
      /*
      $query = $this->summaryCounts()->where('scope_type',"=",$scope)->where('target',"=",$target);
      if (null !== $scopeId) {
        $query = $query->where('scope_id',"=",$scopeId);
      } else {
        $query = $query->whereNull('scope_id');
      }
      if ($query->count()) {
        return $query->first()->value;
      }
      */
      //get a fresh count
      if ($target=="individuals") {
        return $this->individualsCount($scope,$scopeId);
      }
      //get a fresh count
      if ($target=="measurements") {
        return $this->measurementsCount($scope,$scopeId);
      }
      //get a fresh count
      if ($target=="vouchers") {
        return $this->vouchersCount($scope,$scopeId);
      }
      if ($target=="taxons") {
        return $this->taxonsCount($scope,$scopeId);
      }
      if ($target=="media") {
        return $this->all_media_count();
      }
      return 0;
    }


    /* functions to generate counts */

    public function individualsCount($scope='all',$scopeId=null)
    {
      $sql = "SELECT DISTINCT identifications.object_id FROM identifications,taxons where  (identifications.object_type LIKE '%individual%') AND identifications.taxon_id=taxons.id AND taxons.lft>=".$this->lft." AND taxons.lft<=".$this->rgt;
      if (('projects' == $scope or 'App\Models\Project' == $scope) and $scopeId>0) {
        $sql = "SELECT DISTINCT individuals.id FROM individuals,identifications,taxons where
            (identifications.object_type LIKE '%individual%' AND identifications.object_id=individuals.id)
        AND (identifications.taxon_id=taxons.id  AND taxons.lft>='".$this->lft."' AND taxons.lft<='".$this->rgt."')
        AND (
          individuals.dataset_id IN (SELECT datasets.id FROM datasets WHERE datasets.project_id='".$scopeId."')
          OR individuals.id IN (SELECT measurements.measured_id FROM measurements WHERE measurements.measured_type LIKE '%individual%' AND measurements.dataset_id IN (SELECT mdataset.id FROM datasets as mdataset WHERE mdataset.project_id='".$scopeId."'))
        )";
      }
      if (('datasets' == $scope or 'App\Models\Dataset' == $scope) and $scopeId>0) {
        $sql = "SELECT DISTINCT individuals.id FROM individuals,identifications,taxons where
            (identifications.object_type LIKE '%individual%' AND identifications.object_id=individuals.id)
        AND (identifications.taxon_id=taxons.id  AND taxons.lft>='".$this->lft."' AND taxons.lft<='".$this->rgt."')
        AND (
          individuals.dataset_id='".$scopeId."'
          OR individuals.id IN (SELECT measurements.measured_id FROM measurements WHERE measurements.measured_type LIKE '%individual%' AND measurements.dataset_id='".$scopeId."')
        )";
      }
      if (('locations' == $scope or 'App\Models\Location' == $scope) and $scopeId>0) {
        $location = Location::select(['lft','rgt'])->findOrFail($scopeId);
        $sql = "(SELECT DISTINCT individual_location.individual_id FROM individual_location,identifications,taxons,locations where individual_location.individual_id=identifications.object_id AND locations.id=individual_location.location_id AND (identifications.object_type LIKE '%individual%') AND identifications.taxon_id=taxons.id AND taxons.lft>=".$this->lft." AND taxons.lft<=".$this->rgt." AND locations.lft>=".$location->lft." AND locations.lft<=".$location->rgt.") UNION (SELECT DISTINCT individual_location.individual_id FROM individual_location,identifications,taxons,location_related,locations where individual_location.individual_id=identifications.object_id AND location_related.location_id=individual_location.location_id AND (identifications.object_type LIKE '%individual%') AND identifications.taxon_id=taxons.id AND taxons.lft>=".$this->lft." AND taxons.lft<=".$this->rgt." AND location_related.related_id=locations.id AND locations.lft>=".$location->lft." AND locations.lft<=".$location->rgt.")";
      }
      $query = DB::select($sql);
      return count($query);
    }


    public function vouchersCount($scope='all',$scopeId=null)
    {
      $sql = "SELECT DISTINCT vouchers.id FROM vouchers,identifications,taxons where vouchers.individual_id=identifications.object_id AND (identifications.object_type LIKE '%individual%') AND identifications.taxon_id=taxons.id  AND taxons.lft>=".$this->lft." AND taxons.lft<=".$this->rgt;

      /*DATASET SCOPE */
      if (('project' == $scope or 'App\Models\Project' == $scope) and $scopeId>0) {
        $sql = "SELECT DISTINCT vouchers.id FROM vouchers WHERE vouchers.individual_id IN (
          SELECT identifications.object_id FROM identifications,taxons WHERE (identifications.object_type LIKE '%individual%')
          AND (identifications.taxon_id=taxons.id  AND taxons.lft>='".$this->lft."' AND taxons.lft<='".$this->rgt."')) AND (
            vouchers.dataset_id IN (SELECT datasets.id FROM datasets WHERE datasets.project_id='".$scopeId."')
            OR vouchers.id IN (SELECT measurements.measured_id FROM measurements WHERE measurements.measured_type LIKE '%vouchers%' AND measurements.dataset_id IN (SELECT mdataset.id FROM datasets as mdataset WHERE mdataset.project_id='".$scopeId."'))
          )";
      }

      /*DATASET SCOPE */
      if (('datasets' == $scope or 'App\Models\Dataset' == $scope) and $scopeId>0) {
        $sql = "SELECT DISTINCT vouchers.id FROM vouchers WHERE vouchers.individual_id IN (
          SELECT identifications.object_id FROM identifications,taxons WHERE (identifications.object_type LIKE '%individual%')
          AND (identifications.taxon_id=taxons.id  AND taxons.lft>='".$this->lft."' AND taxons.lft<='".$this->rgt."')) AND (
            vouchers.dataset_id='".$scopeId."'
            OR vouchers.id IN (SELECT measurements.measured_id FROM measurements WHERE measurements.measured_type LIKE '%vouchers%' AND measurements.dataset_id='".$scopeId."')
          )";
      }

      if (('locations' == $scope or 'App\Models\Location' == $scope) and $scopeId>0) {
        $location = Location::select(['lft','rgt'])->findOrFail($scopeId);
        $sql = "(SELECT DISTINCT vouchers.id FROM vouchers,identifications,taxons,individual_location,locations  where
          (identifications.object_id=vouchers.individual_id AND (identifications.object_type LIKE '%individual%'))
          AND (identifications.taxon_id=taxons.id  AND taxons.lft>='".$this->lft."' AND taxons.lft<='".$this->rgt."')
          AND (
            individual_location.individual_id=vouchers.individual_id AND locations.id=individual_location.location_id AND locations.lft>=".$location->lft." AND locations.lft<='".$location->rgt."'
          )) UNION (SELECT DISTINCT vouchers.id FROM vouchers,identifications,taxons,individual_location,locations,location_related  where
            (identifications.object_id=vouchers.individual_id AND (identifications.object_type LIKE '%individual%'))
            AND (identifications.taxon_id=taxons.id  AND taxons.lft>='".$this->lft."' AND taxons.lft<='".$this->rgt."')
            AND (
              individual_location.individual_id=vouchers.individual_id AND location_related.location_id=individual_location.location_id AND location_related.related_id=locations.id AND locations.lft>=".$location->lft." AND locations.lft<='".$location->rgt."'
        ))";
    }
      $query = DB::select($sql);
      return count($query);
    }

    public function measurementsCount($scope='all',$scopeId=null)
    {
      $scope_where = null;
      $scope_table = null;
      $all_vouchers = TRUE;
      $all_ind = TRUE;
      if (('projects' == $scope or 'App\Models\Project' == $scope) and $scopeId>0) {
        $scope_table = ",datasets";
        $scope_where = " AND measurements.dataset_id=datasets.id AND datasets.project_id=".$scopeId;
      }
      if (('datasets' == $scope or 'App\Models\Dataset' == $scope) and $scopeId>0) {
        $scope_where = " AND measurements.dataset_id=".$scopeId;
      }
      $scope_where2 = $scope_where;
      $scope_where3 = $scope_where;
      $include_direct = true;
      if (('locations' == $scope or 'App\Models\Location' == $scope) and $scopeId>0) {
        //$scope_table="locations";
        $all_individual_ids = Location::findOrFail($scopeId)->all_individual_ids();
        $all_voucher_ids = Location::findOrFail($scopeId)->all_voucher_ids();
        $all_vouchers = false;
        $all_ind = false;
        if (count($all_voucher_ids)) {
          $scope_where2 = " AND measurements.measured_id IN (".implode(',',$all_voucher_ids).")";
          $all_vouchers = true;
        }
        if (count($all_individual_ids)) {
          $scope_where = " AND measurements.measured_id IN (".implode(',',$all_individual_ids).")";
          $all_ind = true;
        }
        $scope_where3 = null;
        $include_direct = false;
      }
      $sql_individuals = ("SELECT measurements.id FROM measurements,identifications,taxons".$scope_table." WHERE measurements.measured_type LIKE '%individual%' AND measurements.measured_id=identifications.object_id AND identifications.object_type LIKE '%individual%' AND identifications.taxon_id=taxons.id AND taxons.lft>=".$this->lft." AND taxons.lft<=".$this->rgt.$scope_where);

      $sql_vouchers = "SELECT measurements.id FROM measurements,identifications,taxons,vouchers".$scope_table." WHERE measurements.measured_type LIKE '%voucher%' AND measurements.measured_id=vouchers.id AND identifications.object_type LIKE '%individual%' AND identifications.object_id=vouchers.individual_id AND identifications.taxon_id=taxons.id AND taxons.lft>=".$this->lft." AND taxons.lft<=".$this->rgt.$scope_where2;
      $sql_direct = "SELECT measurements.id FROM measurements,taxons".$scope_table." WHERE measurements.measured_type LIKE '%taxon%' AND measurements.measured_id=taxons.id AND taxons.lft>=".$this->lft." AND taxons.lft<=".$this->rgt.$scope_where3;

      $sql = "SELECT DISTINCT tb.id FROM (";
      $parts = [];
      if ($include_direct) {
        $parts[] = "(".$sql_direct.")";
      }
      if ($all_vouchers) {
        $parts[] = "(".$sql_vouchers.")";
      }
      if ($all_ind) {
        $parts[] = "(".$sql_individuals.")";
      }
      $sql .= implode(' UNION ',$parts).") as tb";
      $query = DB::select($sql);
      return count($query);
    }


    public function all_media_ids()
    {
      //media linked to individuals identified by the taxon or taxon descendants
      $type = "%individual%";

      $sql = "SELECT DISTINCT(media.id) FROM individuals,media,identifications,taxons where  identifications.object_id=individuals.id AND media.model_id=individuals.id AND (media.model_type LIKE '$type') AND (identifications.object_type LIKE '$type') AND identifications.taxon_id=taxons.id AND taxons.lft>=$this->lft AND taxons.lft <=$this->rgt";
      $query = DB::select($sql);

      //media linked to taxon or descendants
      $sql = "SELECT DISTINCT(media.id) FROM media,taxons where (media.model_type LIKE '%taxon%') AND media.model_id=taxons.id AND taxons.lft>=$this->lft AND taxons.lft<=$this->rgt";
      $query2 = DB::select($sql);
      if (count($query2)==0 and count($query)==0) {
        return [];
      }
      $query = array_map(function ($value) {
          return (array)$value;
      }, $query);
      $query2 = array_map(function ($value) {
          return (array)$value;
      }, $query2);
      $query = array_merge($query,$query2);
      $query= array_unique(Arr::flatten($query));
      return $query;
    }

    /* all media count*/
    public function all_media_count()
    {
      return count($this->all_media_ids());
    }


    /* count species or below species identifications for taxon and its descendant taxons */
    public function taxonsCount($scope=null,$scopeId=null)
    {
      if ('projects' == $scope and $scopeId>0) {
          $taxons_list = $this->getDescendantsAndSelf()->pluck('id')->toArray();
          $taxonsp = Identification::with('taxon')->whereHasMorph('object',['App\Models\Individual'],function($individual) use($scopeId){
            $individual->withoutGlobalScopes()->where('project_id','=',$scopeId);
          })->whereIn('taxon_id',$taxons_list)->whereHas('taxon',function($taxon) { $taxon->where('level',">=",Taxon::getRank('species'));})->distinct('taxon_id')->pluck('taxon_id')->toArray();
          $taxons = array_unique($taxonsp);
      }
      if ('datasets' == $scope and $scopeId>0) {
        $taxons_list = $this->getDescendantsAndSelf()->pluck('id')->toArray();
        $taxons_list = Taxon::whereIn('id',$taxons_list)->where('level',">=",Taxon::getRank('species'))->pluck('id')->toArray();
        $taxonsp = Measurement::withoutGlobalScopes()->where('dataset_id',$scopeId)->cursor()->map(function($object) use($taxons_list) {
            if ($object->measured_type=="App\Models\Taxon") {
              $taxon_id = $object->measured_id;
            } elseif ($object->measured_type!="App\Models\Location") {
              $taxon_id = $object->measured->identification->taxon_id;
            }
            if (in_array($taxon_id,$taxons_list)) {
              return $taxon_id;
            }
        });
        $taxons = array_unique($taxonsp);
      }
      if ('locations' == $scope and $scopeId>0) {
          $locations_ids = Location::findOrFail($scopeId)->getDescendantsAndSelf()->pluck('id')->toArray();
          $taxons_list = $this->getDescendantsAndSelf()->pluck('id')->toArray();
          $taxons_list = Taxon::whereIn('id',$taxons_list)->where('level',">=",Taxon::getRank('species'))->pluck('id')->toArray();
          $taxonsp = Identification::with('taxon')->whereHasMorph('object',['App\Models\Individual'],function($individual) use($scopeId){
            $individual->withoutGlobalScopes()->whereHas('locations',function($location) use($locations_ids) {
              $location->whereIn('location_id',$locations_ids);
            }
          );})->whereIn('taxon_id',$taxons_list)->distinct('taxon_id')->pluck('taxon_id')->toArray();
          $taxons = array_unique($taxonsp);
      }

      if (!isset($taxons) or null == $scopeId) {
        $taxons  = $this->getDescendantsAndSelf()->map(function($taxon) {
                  $query = $taxon->identifications()->with('taxon')->withoutGlobalScopes()->whereHas('taxon',function($taxon) { $taxon->where('level',">=",Taxon::getRank('species'));})->distinct('taxon_id')->pluck('taxon_id')->toArray();
                  return array_unique($query);
                })->toArray();
      }
      $taxons = array_unique(Arr::flatten($taxons));
      return count($taxons);
    }





    /* register media modifications */
    public function registerMediaConversions(BaseMedia $media = null): void
    {

        $this->addMediaConversion('thumb')
            ->fit('crop', 200, 200)
            ->performOnCollections('images');

        $this->addMediaConversion('thumb')
            ->width(200)
            ->height(200)
            ->extractVideoFrameAtSecond(5)
            ->performOnCollections('videos');
    }

    public static function getTableName()
    {
        return (new self())->getTable();
    }

    public function getBasisOfRecordAttribute()
    {
      return 'Taxon';
    }


}
