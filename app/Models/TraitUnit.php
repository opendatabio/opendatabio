<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class TraitUnit extends Model
{
    use Translatable;

    protected $table = 'trait_units';

    protected $fillable = ['unit']; 
       
    public function odbtraits()
    {
        return $this->hasMany(ODBTrait::class,'trait_unit_id');
    }

    public function getDisplayUnitAttribute()
    {
        return $this->unit." - ".$this->name;
    }

}
