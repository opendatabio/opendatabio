<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Auth;
use Log;
use File;
use Lang;

class UserJob extends Model
{
    protected $fillable = ['dispatcher', 'log', 'status', 'rawdata', 'user_id','affected_ids','affected_model'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    // event fired from job on success
    public function setSuccess()
    {
      if ($this->percentage>=100) {
        /* check for errors */
        if (preg_match("/ERROR/s", $this->log)) {
          $this->status = 'Ended with errors';
        } else {
          $this->status = 'Success';
        }
        $this->save();
      }
    }

    // event fired from job on failure
    public function setFailed()
    {
        $this->status = 'Failed';
        $this->save();
    }

    // event fired from job when it starts processing
    public function setProcessing()
    {
        $this->status = 'Processing';
        $this->save();
    }

    // user sent a "retry" from the interface
    public function retry()
    {
        $this->update(['status' => 'Submitted']);
        $job = $this->dispatcher::dispatch($this);
    }

    public function getDataAttribute()
    {
        return unserialize($this->rawdata);
    }

    public function setProgressMax($value)
    {
        $this->progress_max = $value;
        $this->progress = 0;
        $this->save();
    }

    public function tickProgress()
    {
        ++$this->progress;
        $this->save();
    }

    // show formatted progress
    public function getPercentageAttribute()
    {
        if (0 == $this->progress_max) {
            return ' - %';
        }

        return round(100 * $this->progress / $this->progress_max).'%';
    }

    // entry point for jobs. place the job on queue
    public static function dispatch($dispatcher, $rawdata)
    {
        // create Job entry
        $userjob = self::create(['dispatcher' => $dispatcher, 'rawdata' => serialize($rawdata), 'user_id' => Auth::user()->id]);
        $job = $dispatcher::dispatch($userjob);

        return $userjob->id;
    }

    public function getSubmittedFileAttribute()
    {
      if (!isset($this->data['data'])) {
        return null;
      }
      $data = $this->data['data'];
      return isset($data['filename']) ? $data['filename'] : null;
    }

    public function getAffectedIdsCountAttribute()
    {
      if ($this->affected_ids) {
        $ids = json_decode($this->affected_ids, true);
        return count($ids);
      }
      return 0;
    }

    public function getAffectedIdsArrayAttribute()
    {
      if ($this->affected_ids) {
        $ids = json_decode($this->affected_ids, true);
        return $ids;
      }
      return null;
    }

    public function getAffectedRecordsLinkAttribute()
    {
      $ids = $this->affected_ids_count;
      if ($ids==0) {
        return '';
      }
      $model = str_replace("App\Models\\","",$this->affected_model);
      $route = config('model_routes')[$model];
      return "<a href='".route($route,
      ['job_id' => $this->id])."'>".Lang::get('messages.affected_records').'</a>';

    }

}
