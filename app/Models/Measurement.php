<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Lang;

class Measurement extends Model
{
    use IncompleteDate, LogsActivity;

    protected $fillable = ['trait_id', 'measured_id', 'measured_type',
        'date', 'dataset_id', 'person_id', 'bibreference_id',
        'value', 'value_i', 'value_a', 'notes','parent_id','location_id'];

    protected $appends = ['linked_type','linked_id'];

    //activity log trait (parent, uc and geometry are logged in controller)
    //protected static $logName = 'measurement';
    protected static $recordEvents = ['updated','deleted'];
    //protected static $ignoreChangedAttributes = ['updated_at'];
    //protected static $logFillable = true;
    //protected static $logAttributes = ['name','altitude','adm_level','datum','x','y','startx','starty','notes'];
    //protected static $logOnlyDirty = true;
    //protected static $submitEmptyLogs = false;
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->useLogName('measurement')
        ->logFillable()
        ->logOnlyDirty()
        ->dontLogIfAttributesChangedOnly(['updated_at'])
        ->dontSubmitEmptyLogs();      
    }


    public function rawLink()
    {
        return '<a href="'.url('measurements/'.$this->id).'">'.
                // Needs to escape special chars, as this will be passed RAW
                htmlspecialchars($this->valueDisplay).'</a>';
    }


    public function rawLinkWithTraitName()
    {
        return '<a href="'.url('measurements/'.$this->id).'">'.$this->odbtrait->name.
                '</a>';
    }

    public function rawLinkWithSelectorName()
    {
        return '<a href="'.url('measurements/'.$this->id).'">'.$this->selector_name.
                '</a>';
    }

    /* for popups on linking measurements */
    public function getSelectorNameAttribute()
    {
        return $this->odbtrait->export_name." - ".$this->date." - id: ".$this->id;
    }
    
    public function location()
    {
        return $this->belongsTo(Location::class);
    }


    public function measured()
    {
        return $this->morphTo()->withoutGlobalScopes();
    }

    /* parent relationship link measurements of different traits for the same object and same date */

    /* this measurement depends on another measurement for different trait */
    public function parentMeasurement()
    {
        return $this->belongsTo(Measurement::class, 'parent_id');
    }

    public function getMeasurementParentIdAttribute()
    {
        if ($this->parentMeasurement) {
          return $this->parentMeasurement->id;
        }
        return null;
    }


    /* this measurement is associated with other measurements for other traits, on the same date and object */
    public function dependentMeasurements()
    {
        return $this->hasMany(Measurement::class, 'parent_id');
    }

    /* collectors are persons  who have taken a measurement. */
    public function collectors()
    {
        return $this->belongsToMany(Person::class, 'measurement_person')->withPivot('id')->orderBy('pivot_id');
    }

    public function getMeasurementDeterminedByAttribute()
    {
      return $this->recorded_by;
    }

    public function getRecordedByMainAttribute()
    {
      return $this->collectors->first()->abbreviation;
    }

    public function getRecordedByAttribute()
    {
      return implode(' | ',$this->collectors->pluck('abbreviation')->toArray());
    }

      /** NEED FIX ***/
    public function getMeasurementLocationIdAttribute()
    {
      if ($this->location_id) {
        return $this->location_id;
      }
      if ($this->measured_type == Individual::class) {
        return $this->measured->current_location->location_id;
      }
      if ($this->measured_type == Location::class) {
        return $this->measured_id;
      }
      if ($this->measured_type == Voucher::class) {
        return $this->measured->current_location->location_id;
      }
      return NULL;
    }

    public function getDecimalLatitudeAttribute()
    {
      if ($this->location_id) {
        return $this->location->decimalLatitude;
      }
      if (in_array($this->measured_type,[Individual::class,Voucher::class,Location::class])) {
          return $this->measured->decimalLatitude;        
      }
      return null;
    }
    
    public function getDecimalLongitudeAttribute()
    {
      if ($this->location_id) {
        return $this->location->decimalLongitude;
      }
      if (in_array($this->measured_type,[Individual::class,Voucher::class,Location::class])) {
          return $this->measured->decimalLongitude;        
      }
      return null;
    }


    public function getTaxonIdAttribute()
    {
      if ($this->measured_type == Taxon::class) {
        return $this->measured_id;
      }
      
      if ($this->measured_type == Individual::class && isset($this->measured->identification)) {
        return $this->measured->identification->taxon_id;
      }

      if ($this->measured_type == Voucher::class && isset($this->measured->identification))
      {
        return $this->measured->identification->taxon_id;
      }
      
      return NULL;
    }



    public function getMeasuredProjectAttribute()
    {
      return $this->measured->projectName;
    }

    public function getDatasetNameAttribute()
    {
      return $this->dataset->name;
    }

    // These functions use Laravel magic to create a "linked_id" and "linked_type"
    // fields, used by the $measurement->linked() function below
    public function getLinkedTypeAttribute()
    {
        return $this->odbtrait->link_type;
    }

    public function getLinkedIdAttribute()
    {
        return $this->value_i;
    }
        
    public function linked()
    {
      return $this->belongsTo($this->linked_type,'value_i');
    }
    

    public function odbtrait()
    {
        return $this->belongsTo(ODBTrait::class, 'trait_id');
    }

    public function getTypeAttribute() // easy accessor
    {
        return $this->odbtrait->type;
    }

    public function bibreference()
    {
        return $this->belongsTo(BibReference::class);
    }

    public function getSourceCitationAttribute()
    {
        return $this->bibreference ? $this->bibreference->bibkey : null;
    }

    /*
    public function person()
    {
        return $this->belongsTo(Person::class);
    }
    */

    public function dataset()
    {
        return $this->belongsTo(Dataset::class);
    }

    public function categories()
    {
        return $this->hasMany(MeasurementCategory::class);
    }


    //ge value for display in pages and tables
    public function getValueDisplayAttribute()
    {
        switch ($this->type) {
        case ODBTrait::QUANT_INTEGER:
            return $this->value_i;
            break;
        case ODBTrait::QUANT_REAL:
            return $this->value;
            break;
        case ODBTrait::TEXT:
            if (strlen($this->value_a)>191) {
              $val = substr($this->value_a,0,191)."...";
            } else {
              $val = $this->value_a;
            }
            return $val;
            break;
        case ODBTrait::COLOR:
        case ODBTrait::GENEBANK:
            return $this->value_a;
            break;
        case ODBTrait::CATEGORICAL:
            return $this->categories()->first()->traitCategory->name;
            break;
        case ODBTrait::CATEGORICAL_MULTIPLE:
            $cats = collect($this->categories)->map(function ($newcat) {
                return $newcat->traitCategory->name;
            })->all();

            return implode(', ', $cats);
            break;
        case ODBTrait::ORDINAL:
            $tcat = $this->categories()->first()->traitCategory;

            return $tcat->rank.' - '.$tcat->name;
            break;
        /*
        case ODBTrait::LINK:
            $val = '';
            if (!empty($this->value)) {
                $val = $this->value;
            }
            return $val.' '.(empty($this->linked) ? 'ERROR' : $this->linked->fullname);
            break;
        */
       case ODBTrait::SPECTRAL:
            $val = explode(";",$this->value_a);
            return 'Spectrum with '.count($val).' values';
            break;
        }
    }

    // provides a common interface for getting/setting value for different types of measurements
    public function getValueActualAttribute()
    {
        switch ($this->type) {
        case ODBTrait::QUANT_INTEGER:
            return $this->value_i;
            break;
        case ODBTrait::QUANT_REAL:
            return $this->value;
            break;
        case ODBTrait::TEXT:
        case ODBTrait::COLOR:
        case ODBTrait::SPECTRAL:
        case ODBTrait::GENEBANK:
            return $this->value_a;
            break;
        case ODBTrait::CATEGORICAL:
            return $this->categories()->first()->traitCategory->name;
            break;
        case ODBTrait::CATEGORICAL_MULTIPLE:
            $cats = collect($this->categories)->map(function ($newcat) {
                return $newcat->traitCategory->name;
            })->all();

            return implode(', ', $cats);
            break;
        case ODBTrait::ORDINAL:
            $tcat = $this->categories()->first()->traitCategory;

            return $tcat->rank.' - '.$tcat->name;
            break;
        /*
        case ODBTrait::LINK:
            $val = '';
            if (!empty($this->value)) {
                $val = $this->value;
            }
            return $val.' '.(empty($this->linked) ? 'ERROR' : $this->linked->fullname);
            break;
        */
        }
    }

    // provides a common interface for getting/setting value for different types of measurements
    public function getRawValueActualAttribute()
    {
        switch ($this->type) {
        case ODBTrait::QUANT_INTEGER:
            return $this->value_i;
            break;
        case ODBTrait::QUANT_REAL:
        //case ODBTrait::LINK:
            return $this->value;
            break;
        case ODBTrait::TEXT:
        case ODBTrait::COLOR:
        case ODBTrait::SPECTRAL:
        case ODBTrait::GENEBANK:
            return $this->value_a;
            break;
        case ODBTrait::CATEGORICAL:
        case ODBTrait::CATEGORICAL_MULTIPLE:
        case ODBTrait::ORDINAL:
            return $this->categories->pluck("category_id")->toArray();
            break;
        }
    }

    //this is used only in import_jobs
    public function setValueActualAttribute($value)
    {
        switch ($this->type) {
        case ODBTrait::QUANT_INTEGER:
            $this->value_i = $value;
            break;
        case ODBTrait::QUANT_REAL:
            $this->value = $value;
            break;
        case ODBTrait::TEXT:
        case ODBTrait::COLOR:
        case ODBTrait::SPECTRAL:
        case ODBTrait::GENEBANK:
            $this->value_a = $value;
            break;
        case ODBTrait::CATEGORICAL:
        case ODBTrait::ORDINAL:
        case ODBTrait::CATEGORICAL_MULTIPLE:
            $this->categories()->delete();
            if (!is_array($value)) {
                $value = explode(";",$value);
            }
            if (is_array($value)) {
                foreach ($value as $v) {
                    $this->categories()->create(['category_id' => $v]);
                }
            } else {
                $this->categories()->create(['category_id' => $value]);
            }
            break;
        case ODBTrait::LINK:
            // handled by MeasurementController, as it requires value AND value_i
            //deprecated, LINK trait is disabled
            break;
        }
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('datasetScope', function (Builder $builder) {
            // first, the easy cases. No logged in user?
            $user = Auth::user();
            if (is_null($user)) {
                return $builder->whereRaw("(measurements.dataset_id IN (SELECT dts.id FROM datasets as dts WHERE dts.privacy>='".Dataset::PRIVACY_PUBLIC."'))");
            }
            // superadmins see everything
            if (User::ADMIN == $user->access_level) {
                return $builder;
            }
            //user datasets 
            $user_dts = $user->visibleDatasets();
            $user_dts = $user_dts!==null ? $user_dts->pluck('datasets.id')->toArray() : [];
            // now the complex case: the regular user
            if (count($user_dts)>0) {
              return $builder->whereRaw("(measurements.dataset_id IN
              (SELECT dts.id FROM datasets as dts WHERE dts.privacy>='".Dataset::PRIVACY_REGISTERED."')
              OR measurements.dataset_id IN( ".implode(',',$user_dts)." ))");                          
            } else {
              return $builder->whereRaw("(measurements.dataset_id IN (SELECT dts.id FROM datasets as dts WHERE dts.privacy>='".Dataset::PRIVACY_REGISTERED."'))");
            }            
          });
            // before delete() method call this
            static::deleting(function($measurement) {
                $measurement->collectors()->detach();
                $measurement->categories->each->delete();
                $measurement->activities->each->delete();
            });
    }

    /*
    public function newQuery($excludeDeleted = true)
    {
        // This uses the explicit list to avoid conflict due to global scope
        return parent::newQuery($excludeDeleted)->addSelect(
            'measurements.id',
            'measurements.trait_id',
            'measurements.measured_id',
            'measurements.measured_type',
            'measurements.date',
            'measurements.dataset_id',
            'measurements.person_id',
            'measurements.bibreference_id',
            'measurements.value',
            'measurements.value_i',
            'measurements.value_a',
            'measurements.notes'
        );
    }
    */

    /* dwc terms */
    public function getMeasurementTypeAttribute()
    {
        return $this->odbtrait->export_name;
    }
    public function getMeasurementUnitAttribute()
    {
        $uu = $this->odbtrait->trait_unit;
        return $uu ? $uu->unit : null;
    }

    /* this attribute is only used during data export
    if genetic must retrieve sequence data from genebank
    should store sequence locally?
    */
    public function getMeasurementValueAttribute()
    {
      if ($this->odbtrait->type==ODBTrait::GENEBANK) {
        $gene = ExternalAPIs::getGeneBankData($this->valueActual,'nucleotide');
        if (null==$gene) {
          return Lang::get('message.genebank_not_found');
        }
        return $gene[$this->valueActual]['gene'];
      }
      return $this->valueActual;
    }

    public function getMeasurementRemarksAttribute()
    {
      return $this->notes;
    }
    public function getFormatedNotesAttribute()
    {
      return ODBFunctions::formatNotes($this->notes);
    }

    
    
    public function getMeasurementDeterminedDateAttribute()
    {
      return $this->formatDate;
    }

    public function getResourceRelationshipAttribute()
    {
      $measured_type = str_replace("App\\Models\\","", $this->measured_type);
      switch ($measured_type) {
          case Individual::class:
            $type = 'organism';
            break;
          case Voucher::class:
            $type = 'preservedSpecimen';
            break;
          case Taxon::class:
            $type = 'taxon';
            break;
          case Location::class:
            $type = 'location';
            break;
        default:
          $type=null;
          break;
      }
      return $type;
    }

    public function getResourceRelationshipIDAttribute()
    {
      return $this->measured->fullname;
    }

    public function getRelationshipOfResourceAttribute()
    {
      return "measurement of";
    }

    public function getScientificNameAttribute()
    {
      if(in_array($this->measured_type,[Individual::class,Voucher::class,Taxon::class,Media::class])) {
        return $this->measured->scientificName;
      }
      return null;
    }
    public function getFamilyAttribute()
    {
      if(in_array($this->measured_type,[Individual::class,Voucher::class,Taxon::class,Media::class])) {
        return $this->measured->family;
      }
      return null;
    }

    

    /*
    public function getAccessRightsAttribute()
    {
      return $this->dataset->accessRights;
    }
    public function getBibliographicCitationAttribute()
    {
      return $this->dataset->bibliographicCitation;
    }
    public function getLicenseAttribute()
    {
      return $this->dataset->dwcLicense;
    }

    */

    /* will be returned in english */
    public function getMeasurementMethodAttribute()
    {
        return $this->odbtrait->measurementMethod;
    }
    public function getModifiedAttribute()
    {
      return $this->updated_at->toJson();
    }
    public function getBasisOfRecordAttribute()
    {
      return 'MeasurementsOrFact';
    }
  
    /* used by controller to prevent duplications */
    public static function checkDuplicateMeasurement($registry,$oldregistry=null)
    {
      $dataset_id = $registry['dataset_id'];
      $trait_id = $registry['trait_id'];
      $measured_id = $registry['measured_id'];
      $measured_type = $registry['measured_type'];
      $value = $registry['value'];
      $year =str_pad((int)$registry['date_year'], 4, '0', STR_PAD_LEFT);
      $month = str_pad((int)$registry['date_month'], 2, '0', STR_PAD_LEFT);
      $day = str_pad((int)$registry['date_day'], 2, '0', STR_PAD_LEFT);
      $date = $year."-".$month."-".$day;
      /* "dataset_id='".$dataset_id."' REMOVED AS A CONSTRAINT TO DUPLICATION 
      * increased CONTROL */
      $sql = "trait_id='".$trait_id."' AND measured_id ='".$measured_id."' AND measured_type='".addslashes($measured_type)."' AND date='".$date."'";
      if (isset($oldregistry)) {
        $sql .= " AND id<>'".$oldregistry->id."' ";
      }
      $trait_type = ODBTrait::findOrFail($trait_id)->type;
      if (in_array($trait_type, [ODBTrait::CATEGORICAL, ODBTrait::CATEGORICAL_MULTIPLE, ODBTrait::ORDINAL])) {
        $same = Measurement::withoutGlobalScopes()->with('categories')->whereRaw($sql);
        if ($same->count()>0) {
          $count =0;
          foreach($same->get() as $val) {
              $cats = collect($val->categories)->map(function ($newcat) {
                  return $newcat->traitCategory->id;
              })->all();
              if (!is_array($value) and in_array($value,$cats)) {
                 $count++;
              } else {
                if (is_array($value) and count(array_diff($value,$cats))==0) {
                  $count++;
                }
              }
          }
          return $count;
        }
      } else {
        if (in_array($trait_type, [ODBTrait::LINK])) {
          $sql .= " AND value_i='".$registry['link_id']."'";
        }
        if (in_array($trait_type, [ODBTrait::QUANT_INTEGER])) {
          $sql .= " AND value_i='".$value."'";
        }
        if (in_array($trait_type, [ODBTrait::QUANT_REAL])) {
          $sql .= " AND `value`='".$value."'";
        }
        if (in_array($trait_type, [ODBTrait::TEXT, ODBTrait::COLOR, ODBTrait::SPECTRAL,ODBTrait::GENEBANK])) {
          $sql .= " AND value_a LIKE '".$value."'";
        }
        $same = Measurement::withoutGlobalScopes()->whereRaw($sql);
        return $same->count();          
      }
      return 0;
    }


    /* used by controller to validade measurement value */
    public static function valueValidation($value,$odbtrait,$measurement_id=null)
    {
        $messages = [];
        if (ODBTrait::SPECTRAL !== $odbtrait->type and isset($odbtrait->range_min) and $value < $odbtrait->range_min) {
            $messages[] = Lang::get('messages.value_out_of_range',[
              'export_name' => $odbtrait->export_name
            ]);
        }
        if (ODBTrait::SPECTRAL !== $odbtrait->type and isset($odbtrait->range_max) and $value > $odbtrait->range_max) {
            $messages[] = Lang::get('messages.value_out_of_range',[
              'export_name' => $odbtrait->export_name
            ]);
        }
        // Checks if spectral has the correct number of values and if values are numeric
        if (ODBTrait::SPECTRAL == $odbtrait->type) {
           $spectrum = explode(";",$value);
           if (count($spectrum) != $odbtrait->value_length or count($spectrum) != count(array_filter($spectrum, "is_numeric"))) {
            $messages[] = Lang::get('messages.value_spectral',[
              'export_name' => $odbtrait->export_name
            ]).": ".count(explode(";",$value))." v.s. ".$odbtrait->value_length;
           }
        }
        // Checks if integer variable is integer type
        if (ODBTrait::QUANT_INTEGER == $odbtrait->type and strval($value) != strval(intval($value))) {
            $messages[] =  Lang::get('messages.value_integer',[
              'export_name' => $odbtrait->export_name
            ]);
        }
        if (in_array($odbtrait->type, [ODBTrait::QUANT_REAL, ODBTrait::LINK]) and isset($value)) {
            if (!is_numeric($value)) {
                $messages[] =  Lang::get('messages.value_numeric',[
                  'export_name' => $odbtrait->export_name
                ]);
            }
        }
        if (in_array($odbtrait->type, [ODBTrait::CATEGORICAL, ODBTrait::ORDINAL, ODBTrait::CATEGORICAL_MULTIPLE])) {
            // validates that the chosen category is ACTUALLY from the trait
            $valid = $odbtrait->categories->pluck('id')->all();
            if (is_array($value)) {
                foreach ($value as $v) {
                    if (!in_array($v, $valid)) {
                        $messages[] =  Lang::get('messages.trait_measurement_mismatch',[
                          'export_name' => $odbtrait->export_name
                        ]);
                    }
                }
            } elseif ($value) {
                if (!in_array($value, $valid)) {
                    $messages[] = Lang::get('messages.trait_measurement_mismatch',[
                      'export_name' => $odbtrait->export_name
                    ]);
                }
            }
        }
        // Checks if spectral has the correct number of values and if values are numeric
        if (ODBTrait::GENEBANK == $odbtrait->type) {
           $hasgenebank = ExternalAPIs::getGeneBankData($value,$db = 'nucleotide');
           if (null == $hasgenebank) {
            $messages[] =   $value." ".Lang::get('messages.genebank_not_found',[
              'export_name' => $odbtrait->export_name
            ]);
           }
           //check if measurement already exists
           $accession= $value;
           $hasvalue = Measurement::whereHas('odbtrait',function($t){
             $t->where('type',ODBTrait::GENEBANK);
           })->where('value_a','like',$accession);
           if ($measurement_id) {
             $hasvalue = $hasvalue->where('measurements.id','<>',$measurement_id);
           }
           if ($hasvalue->count() ) {
            $messages[] =   $value." ".Lang::get('messages.genebank_already_registered',[
              'export_name' => $odbtrait->export_name
            ])." ".$hasvalue->first()->measured->rawLink();
           }
        }
        return $messages;
    }


}
