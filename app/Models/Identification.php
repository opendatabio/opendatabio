<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Identification extends Model
{
    use IncompleteDate;

    // Possible modifiers for the identification
    const NONE = 0;
    const SS = 1;
    const SL = 2;
    const CF = 3;
    const AFF = 4;
    const VEL_AFF = 5;
    const MODIFIERS = [
        self::NONE,
        self::SS,
        self::SL,
        self::CF,
        self::AFF,
        self::VEL_AFF,
    ];

    protected $fillable = ['person_id', 'taxon_id', 'object_id', 'object_type', 'date', 'modifier', 'biocollection_id', 'biocollection_reference', 'notes'];
    //protected $appends = ['identifiedDate','family','genus','identifiedBy','identificationRemarks','scientificName'];


    public function object()
    {
        return $this->morphTo('object');
    }

    public function rawLink()
    {
        if ($this->taxon) {
            return $this->taxon->rawLink().
                ($this->modifier ? ' '.Lang::get('levels.modifier.'.$this->modifier) : '');
        }

        return Lang::get('messages.unidentified');
    }

    /* replaced by collectors - to be removed */
    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function collectors()
    {
        return $this->belongsToMany(Person::class, 'identification_person');
    }
    
    public function getRecordedByMainAttribute()
    {
      return $this->collectors->first()->abbreviation;
    }

    public function getRecordedByAttribute()
    {
      return implode(' | ',$this->collectors->pluck('abbreviation')->toArray());
    }

    public function taxon()
    {
        return $this->belongsTo(Taxon::class);
    }
    
    public function getPublishedStatusAttribute()
    {
      return $this->taxon->published_status;
    }

    public function biocollection()
    {
        return $this->belongsTo(Biocollection::class);
    }
    public function getFormatedNotesAttribute()
    {
      return ODBFunctions::formatNotes($this->notes,Lang::get('messages.identification_notes'),'identity-notes');
    }

    function getIdentificationRemarksAttribute() : string {
        $text = "";
        if ($this->biocollection_id) {
            $text = Lang::get('messages.identification_based_on')." ".Lang::get('messages.voucher')." #".$this->biocollection_reference." @".$this->biocollection->acronym.". ";
        }
        return $text.$this->notes;
    }

    public function getIdentificationQualifierAttribute() : string
    {
      if ($this->modifier) {
        $modifier = $this->modifier;
        if ($modifier>0) {
            return Lang::get('levels.modifier.'.$modifier);
        } 
      }
      return '';
    }

    public function getFamilyAttribute()
    {
        if ($this->taxon) {
            return $this->taxon->family;
        }
        return Lang::get('messages.unidentified');
    }
    public function getGenusAttribute()
    {
        if ($this->taxon) {
            return $this->taxon->genus;
        }
        return Lang::get('messages.unidentified');
    }

    public function getDateIdentifiedAttribute()
    {
      return $this->format_date;      
    }

    public function getIdentifiedByAttribute()
    {
      if ($this->collectors) {
          return implode(" | ",$this->collectors->pluck('abbreviation')->toArray());
      }
      return Lang::get('messages.unidentified');
    }

    public function getScientificNameAttribute()
    {
        if ($this->taxon) {
            return $this->taxon->fullname;
        }
        return Lang::get('messages.unidentified');
    }

    public function getScientificNameWithAuthorAttribute()
    {
        if ($this->taxon) {
            return $this->taxon->getFullnameWithAuthor();
        }
        return Lang::get('messages.unidentified');
    }
}
