<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Voucher;
use App\Models\Individual;
use App\Models\Biocollection;
use Auth;
use Mail;
use DB;
use Lang;

class ODBRequest extends Model
{

  const REQUESTED =0;
  const CHECKED = 1;
  const LENDED = 2;
  const RETURNED = 3;
  const DONATED = 4;
  const IDENTIFICATION_CHANGED = 5;
  const IDENTIFICATION_DENIED = 6;
  const VOUCHERS_REGISTERED = 7;
  const VOUCHERS_REGISTRATION_DENIED = 8;
  const STATUS = [
    self::REQUESTED,
    self::CHECKED,
    self::LENDED,
    self::RETURNED,
    self::DONATED,
  ];
  const STATUS_INDIVIDUALS = [
    self::REQUESTED,
    self::CHECKED,
    self::IDENTIFICATION_CHANGED,
    self::IDENTIFICATION_DENIED,
    self::VOUCHERS_REGISTERED,
    self::VOUCHERS_REGISTRATION_DENIED,
  ];
  protected $table = 'requests';

  protected $fillable = ['email', 'institution', 'message','user_id','biocollection_id','type'];

  public function vouchers()
  {
    return $this->belongsToMany(Voucher::class,'voucher_request','request_id','voucher_id')
                ->withPivot(['status','notes'])
                ->withTimestamps();
  }

  public function individuals()
  {
    return $this->belongsToMany(Individual::class,'individual_request','request_id','individual_id')
                ->withPivot(['biocollection_id','status','notes'])
                ->withTimestamps();
  }

  public function user()
  {
    return $this->belongsTo(User::class)->with(['person']);
  }

  public function biocollection()
  {
    return $this->belongsTo(BioCollection::class);
  }

  public function getStatusAttribute()
  {
    if ($this->vouchers->count()) {
      $table = 'voucher_request';
    } else {
      $table = 'individual_request';
    }
    $status = DB::select("SELECT status,COUNT(*) AS count FROM `".$table."` WHERE request_id=? GROUP BY status",[$this->id]);
    return $status;
  }

  public function getStatusFormatedAttribute()
  {
    $status = $this->status;
    $text = "";
    foreach ($status as $value) {
      $text .= Lang::get('levels.status.'.$value->status).': '.$value->count.'<br>';
    }
    return $text;
  }

  public function getStatusClosedAttribute()
  {
    if ($this->vouchers->count()) {
      $table = 'voucher_request';
    } else {
      $table = 'individual_request';
    }
    $status = DB::select("SELECT COUNT(*) AS count FROM `".$table."` WHERE request_id=? AND status<?",[$this->id,self::RETURNED]);
    return $status[0]->count==0;
  }

  public function getUserFormatedAttribute()
   {
     $name = isset($this->user) ? (isset($this->user->person) ? $this->user->person->rawLink() : $this->user->email) : $this->email." NOT REGISTERED";
     $email = isset($this->user) ? $this->user->email : $this->email." NOT REGISTERED";
     if ($name==$email) {
       return $email;
     }
     return $name.' ['.$email.']';
   }

   public function getInstitutionFormatedAttribute()
   {
     return isset($this->biocollection_id) ? $this->biocollection->rawLink() : (isset($this->institution) ? $this->institution." NOT REGISTERED" : null);
   }

   //send email to user
   public static function sendEmail($odbrequest)
   {
       $from_email = isset($odbrequest->user_id) ? $odbrequest->user->email : $odbrequest->email;
       if ($odbrequest->vouchers->count()) {
         $biocollection_ids = array_unique($odbrequest->vouchers->pluck('biocollection_id')->toArray());
       } else {
         $biocollection_ids = array_unique($odbrequest->individuals->pluck('pivot.biocollection_id')->toArray());
       }
       $biocollection = Biocollection::findOrFail($biocollection_ids[0]);
       //send to the first dataset admin with cc to rest
       $admins = $biocollection->admins()->pluck('email')->toArray();
       $person = $biocollection->admins()->first()->person;
       $to_email = $admins[0];
       if (isset($person)) {
           $to_name = $person->full_name;
       } else {
           $to_name = $to_email;
       }
       //with copy cc to requester and other admins
       if (Auth::user()) {
         $from_email =  Auth::user()->email;
         if (isset(Auth::user()->person)) {
           $from_name= Auth::user()->person->fullname;
         } else {
           $from_name = Auth::user()->email;
         }
       } else {
         $from_email = $odbrequest->email;
         $from_name = $odbrequest->email;
       }
       $admins[0] = $from_email;
       $cc_email = $admins;


       //prep de content html to send as the email text
       $content = Lang::get('messages.request_action')."  <strong>".$biocollection->name."</strong> <br>".Lang::get('messages.from')."  <strong>".
             htmlentities($from_name)
       ."</strong> @ <a href='".env('APP_URL')."'>".htmlentities(env('APP_URL'))."</a>";
       if ($odbrequest->vouchers->count()) {
         $count = $odbrequest->vouchers->count();
         $content .= "<hr><strong>".Lang::get('messages.vouchers')."</strong>: ".$count;
         if ($odbrequest->institution) {
         $content .= "<br><strong>".Lang::get('messages.institution')."</strong>:".$odbrequest->institution;
         }
       } else {
         $count = $odbrequest->individuals->count();
         $content .= "<hr><strong>".Lang::get('messages.individuals')."</strong>: ".$count;
       }
       $content .= "<br><strong>".Lang::get('messages.request_message')."</strong>: <br>".$odbrequest->message;
       $content .=  "<br><strong>".Lang::get('messages.request_link')."</strong>: <a href='".url('odbrequests/'.$odbrequest->id)."'>".htmlentities(url('odbrequests/'.$odbrequest->id))."</a>";
       $content .= "<br><hr>";
       $content .= "You are recieving this message because you are an authorized user to manage the data for BioCollection <strong>".$biocollection->name."</strong>. A request was open in the datatabase with the list of <strong>".$count."</strong> records requested by <strong>".$from_name."</strong>. <br> This email is copied to ".$from_name." and other authorized users, if any.<hr>";

       $content .= "<br><br>**".Lang::get('messages.no_reply_email')."**<br>";
       $subject = Lang::get('messages.request_action').' by '.$from_name.' - '.env('APP_NAME');
       $data = array(
         'to_name' => $to_name,
         'content' => $content
       );
       //send email
       try {
         Mail::send('common.email', $data, function($message) use ($to_name, $to_email, $subject,$cc_email) {
             $message->to($to_email, $to_name)->cc($cc_email)->subject($subject);
         });
       } catch (\Exception $e) {
         return false;
       }
       return true;
   }


}
