<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\IndividualLocationScope;

use App\Models\Location;
use App\Models\Individual;
use App\Models\Identification;
use DB;
use Lang;

class IndividualLocation extends Model
{
    protected $table = 'individual_location';


    //protected $fillable = ['location_id','individual_id','relative_position','global_position','date_time','notes','first'];
    public function rawLink()
    {
        $text = "<a href='".url('individual-location/'.$this->id)."'>".Lang::get('messages.location')." ".Lang::get('messages.for').' '.htmlspecialchars($this->individual->fullname).'</a>';
        return $text;
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function individual()
    {
        return $this->belongsTo(Individual::class)->withoutGlobalScopes();
    }

    public function all_locations_ids()
    {
      $related = $this->location()->cursor()->map(function($l) {
        $ids = [$l->id];
        $rids = $l->relatedLocations->pluck('related_id')->toArray();
        return array_merge($ids,$rids);
      })->toArray();
      return array_unique($related);
    }

    public function identification()
    {
      return $this->hasOne(Identification::class, 'object_id', 'individual_id')
      ->where('object_type',"App\Models\Individual");
    }

    public function location_with_geom()
    {
      return $this->location()->withGeom();
    }

    public function getLocationNameAttribute()
    {
      return $this->location->name;
    }

    public function getLocationParentNameAttribute()
    {
      return $this->location->parent->name;
    }

    public function getLocationFullnameAttribute()
    {
      return $this->location->higher_geography;
    }

    public function getLatLong()
    {
        // if the "cached" values are already set, do nothing
        if ($this->long or $this->lat) {
           return;
        }
        // all others, extract from centroid
        $coords = Location::latlong_from_point($this->globalPosition);
        $this->long = $coords[1];
        $this->lat = $coords[0];
    }
    public function getDecimalLatitudeAttribute()
    {
      $this->getLatLong();
      return (float) $this->lat;
    }
    public function getDecimalLongitudeAttribute()
    {
      $this->getLatLong();
      return (float) $this->long;
    }

    public function getLatitudeAttribute()
    {
      $this->getLatLong();
      return $this->lat;
    }

    public function getLongitudeAttribute()
    {
      // if the "cached" values are already set, do nothing
      $this->getLatLong();
      return $this->long;
    }

    public function getLatitudeSimpleAttribute()
    {
        $this->getLatLong();
        $letter = $this->lat > 0 ? 'N' : 'S';

        return $this->lat1.'&#176;'.$this->lat2.'\''.$this->lat3.'\'\' '.$letter;
    }

    public function getLongitudeSimpleAttribute()
    {
        $this->getLatLong();
        $letter = $this->long > 0 ? 'E' : 'W';

        return $this->long1.'&#176;'.$this->long2.'\''.$this->long3.'\'\' '.$letter;
    }

    // getter method for parts of latitude/longitude
    public function getLat1Attribute()
    {
        $this->getLatLong();

        return floor(abs($this->lat));
    }

    public function getLat2Attribute()
    {
        $this->getLatLong();

        return floor(60 * (abs($this->lat) - $this->lat1));
    }

    public function getLat3Attribute()
    {
        $this->getLatLong();

        return floor(60 * (60 * abs($this->lat) - 60 * $this->lat1 - $this->lat2));
    }

    public function getLatOAttribute()
    {
        $this->getLatLong();
        return $this->lat > 0;
    }

    public function getLong1Attribute()
    {
        $this->getLatLong();
        return floor(abs($this->long));
    }

    public function getLong2Attribute()
    {
        $this->getLatLong();

        return floor(60 * (abs($this->long) - $this->long1));
    }

    public function getLong3Attribute()
    {
        $this->getLatLong();

        return floor(60 * (60 * abs($this->long) - 60 * $this->long1 - $this->long2));
    }

    public function getLongOAttribute()
    {
        $this->getLatLong();

        return $this->long > 0;
    }
    

    public function getCoordinatesSimpleAttribute()
    {
        return '('.$this->latitudeSimple.', '.$this->longitudeSimple.')';
    }


    /* include individual permissions here */
    /**
   * The "booted" method of the model.
   *
   * @return void
   */
   protected static function booted()
   {
      static::addGlobalScope(new IndividualLocationScope);
   }

   
   public function scopeMapping($query) {
    return $query->withoutGlobalScopes()->join('identifications','individual_id','object_id')->join('taxons','taxon_id','taxons.id')->select(
      'individual_location.id',
      'individual_location.individual_id',      
      DB::raw('ST_AsText(individual_location.relative_position) as relativePosition'),
      DB::raw('ST_AsText(individual_location.global_position) as globalPosition'),
      DB::raw('ST_AsGeoJson(individual_location.global_position) as globalPositionJson'),      
      DB::raw("odb_txname(taxons.name, taxons.level, taxons.parent_id, identifications.modifier,0) as scientificName")
    );
   }

   public function scopeWithXy($query) {
    return $query->select(
      'individual_location.id',
      'individual_location.location_id',
      'individual_location.individual_id',
      'individual_location.date_time',
      'individual_location.notes',
      'individual_location.altitude',
      'individual_location.first',
      'individual_location.created_at',
      'individual_location.updated_at',
      DB::raw('ST_AsText(individual_location.relative_position) as relativePosition'),
      DB::raw('ST_AsText(individual_location.global_position) as globalPosition'),
      DB::raw('ST_AsGeoJson(individual_location.global_position) as globalPositionJson')      
    );
   }
  


    /*
    public function newQuery($excludeDeleted = true)
    {
        // This uses the explicit list to avoid conflict due to global scope
        // maybe check http://lyften.com/journal/user-settings-using-laravel-5-eloquent-global-scopes.html ???
        //return parent::newQuery($excludeDeleted);
        return parent::newQuery($excludeDeleted)->select(
            'individual_location.id',
            'individual_location.location_id',
            'individual_location.individual_id',
            'individual_location.date_time',
            'individual_location.notes',
            'individual_location.altitude',
            'individual_location.first',
            DB::raw('ST_AsText(relative_position) as relativePosition'),
            DB::raw('ST_AsText(global_position) as globalPosition'),
            DB::raw('ST_AsGeoJson(global_position) as globalPositionJson'),
        );
    }
    */



    // getters for the Relative Position
    public function getXAttribute()
    {
        if ($this->relativePosition== "") {
          return;
        }
        $point = substr($this->relativePosition, 6, -1);
        $pos = strpos($point, ' ');

        return substr($point, $pos + 1);
    }

    public function getYAttribute()
    {
        if ($this->relativePosition == "") {
          return;
        }
        $point = substr($this->relativePosition, 6, -1);
        $pos = strpos($point, ' ');

        return substr($point, 0, $pos);
    }

    public function getAngleAttribute()
    {
        $x = $this->x;
        if (null == $x) {
            return ;
        }
        $y = $this->y;
        $angle = (180 / M_PI) * atan2((float) $y, (float) $x);
        if ($angle<0) {
          $angle = $angle+360;
        }
        return round($angle,2);
    }

    public function getDistanceAttribute()
    {
        $x = $this->getXAttribute();
        if (null == $x) {
            return ;
        }
        $y = $this->getYAttribute();
        $distance = sqrt((float) $x * (float) $x + (float) $y * (float) $y);
        return round($distance,2);
    }

    /* dwc terms */
    public function getOrganismIDAttribute()
    {
      return $this->individual->fullname;
    }
    public function getRecordedDateAttribute()
    {
      return $this->date_time ? $this->date_time : $this->individual->recorded_date;
    }

    public function getHigherGeographyAttribute()
    {
      return $this->location->higher_geography;
    }

    public function getHigherGeographyDescAttribute()
    {
      return $this->location->higher_geography_desc;
    }
    
    public function getGeoreferenceRemarksAttribute()
    {
      return $this->location->georeferenceRemarks;
    }

    public function getOccurrenceRemarksAttribute()
    {
      return $this->notes;
    }

    public function getFormatedNotesAttribute()
    {
      return ODBFunctions::formatNotes($this->notes);
    }

    public function getDtformatedNotesAttribute()
    {
      return ODBFunctions::formatNotes($this->notes,$title=false,'locnotes-'.$this->id);
    }

    public function getOrganismRemarksAttribute()
    {
      return $this->individual->organismRemarks;
    }
    public function getMinimumElevationAttribute()
    {
      return $this->altitude;
    }
    public function getDatasetNameAttribute()
    {
      return $this->individual->datasetName;
    }
    public function getBibliographicCitationAttribute()
    {
      return $this->individual->bibliographicCitation;
    }
    public function getAccessRightsAttribute()
    {
      return $this->individual->accessRights;
    }

    public function getBasisOfRecordAttribute()
    {
      return 'Occurrence';
    }

    public function getScientificNameAttribute()
    {
        if ($this->identification) {
            return $this->identification->scientific_name;
        }
        return Lang::get('messages.unidentified');
    }

    public function getFamilyAttribute()
    {
      if ($this->identification) {
          return $this->identification->family;
      }
      return Lang::get('messages.unidentified');
    }


    public function getOccurrenceIDAttribute()
    {
      #return $this->individual->tag.":".$this->individual->normalizedAbbreviation.":".strtotime($this->date_time);
      if (!$this->date_time) {
        //in this case the individual has a single date
        return $this->individual->fullname.".".$this->individual->recordedDate;
      }
      return $this->individual->fullname.".".strtotime($this->date_time);
    }

    public function getLicenseAttribute()
    {
      return $this->individual->license;
    }
}
