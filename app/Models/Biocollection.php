<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use DB;

use Spatie\MediaLibrary\MediaCollections\Models\Media as BaseMedia;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;


class Biocollection extends Model implements HasMedia
{

    use HasAuthLevels, InteractsWithMedia;

    const NOT_TYPE =0;
    const GENERIC_TYPE = 1;
    const HOLOTYPE = 2;
    const ISOTYPE = 3;
    const PARATYPE = 4;
    const LECTOTYPE = 5;
    const ISOLECTOTYPE = 6;
    const SYNTYPE = 7;
    const ISOSYNTYPE = 8;
    const NEOTYPE = 9;
    const EPITYPE = 10;
    const ISOEPITYPE = 11;
    const CULTIVARTYPE = 12;
    const CLONOTYPE = 13;
    const TOPOTYPE = 14;
    const PHOTOTYPE = 15;
    const NOMENCLATURE_TYPE = [
      self::NOT_TYPE,
      self::GENERIC_TYPE,
      self::HOLOTYPE,
      self::ISOTYPE,
      self::PARATYPE,
      self::LECTOTYPE,
      self::ISOLECTOTYPE,
      self::SYNTYPE,
      self::ISOSYNTYPE,
      self::NEOTYPE,
      self::EPITYPE,
      self::ISOEPITYPE,
      self::CULTIVARTYPE,
      self::CLONOTYPE,
      self::TOPOTYPE,
      self::PHOTOTYPE,
    ];

    protected $table = 'biocollections';

    protected $fillable = ['name', 'acronym', 'irn','description'];

    public function rawLink()
    {
        return "<a href='".url('biocollections/'.$this->id)."'>".htmlspecialchars($this->acronym).'</a>';
    }

    public function persons()
    {
        return $this->hasMany(Person::class);
    }

    public function vouchers()
    {
        //return $this->belongsToMany(Voucher::class)->withPivot('biocollection_number');
        return $this->hasMany(Voucher::class);
    }

    public function odbrequests()
    {
        return $this->hasMany(ODBRequest::class);
    }

    public function getIrnUrlAttribute() : string {
      if ($this->irn) {
        return '<a class="text-nowrap"  href="'.config("external-apis.indexHerbariorum.linkto").$this->irn.'"  title="Index Herbariorum" target="_blank">
        <img src="'.asset('images/nybg-logo-idxh.png').'"  height="12px"> #'.$this->irn.'</a>';
      }
      return '';
    }

    protected static function boot()
    {
        parent::boot();
        // before delete() method call this
        static::deleting(function($biocollection) {
            $biocollection->users()->detach();
            $biocollection->media->each->delete();
            $biocollection->activities->each->delete();

            // do the rest of the cleanup...
        });

    }


    // For Revisionable
    public function identifiableName()
    {
        return $this->acronym;
    }

    public function taxonsIds(): array 
    {
      $nv = $this->vouchers()->withoutGlobalScopes()->count();
      if ($nv==0) {
        return [];
      }
      $ids = $this->vouchers()->withoutGlobalScopes()->select("vouchers.*")
      ->with('identification')->cursor()->map(function($v) { 
        return $v->identification ? $v->identification->taxon_id : null;
      })->unique()->values()->toArray();
      $ids = array_filter($ids);
      return($ids);
    }

    public function getPeopleAttribute()
    {
      $admins = $this->admins->map(function($u) {
        $person = isset($u->person_id) ? $u->person->rawLink() : null;
        return [$u->email,$person];
      });
      $collabs = $this->collabs->map(function($u) {
        $person = isset($u->person_id) ? $u->person->rawLink() : null;
        return [$u->email,$person];
      });
      return [
        'curators' => $admins->toArray(),
        'collabs' => $collabs->toArray(),
      ];
    }

    public function bibreferences_ids( )
    {
        $bibs = $q =DB::select('SELECT bib_reference_id FROM voucher_bibreference LEFT JOIN vouchers ON vouchers.id=voucher_bibreference.voucher_id WHERE biocollection_id='.$this->id);
        $bibs = array_values(array_unique(array_map(function($b){ return (int) $b->bib_reference_id; },$bibs)));
        return $bibs;
    }

    public static function getTableName()
    {
        return (new self())->getTable();
    }
    
    /* register media modifications, this is for the project logo object */
    public function registerMediaConversions(BaseMedia $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->fit('crop', 200, 200)
            ->performOnCollections('logos');
    }


}
