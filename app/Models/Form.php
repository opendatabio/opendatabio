<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $fillable = ['name', 'measured_type', 'user_id', 'notes','medatada'];
    protected $casts = ['medatada' => 'array'];

    public function rawLink()
    {
        $url = route('forms.show',['form_id' => $this->id]);
        return "<a href='".$url."'>".htmlspecialchars($this->name).'</a>';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function form_tasks()
    {
        return $this->hasMany(FormTask::class);
    }

    public function getShortMeasuredTypeAttribute()
    {
        return str_ireplace("App\Models\\","",$this->measured_type);
    }
    public function getTraitsListAttribute()
    {
        $traits = $this->metadata ? json_decode($this->metadata,true) : null;
        if ($traits) {
            $traits = isset($traits['traits']) ? $traits['traits'] : null;            
        }
        return $traits;
    }

    protected static function boot()
    {
        parent::boot();

        // before delete() method call this
        static::deleting(function($form) {
            $form->form_tasks->each->delete();            
        });

    }

}
