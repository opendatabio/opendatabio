<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Lang;

class User extends Authenticatable
{
    use Notifiable;

    // Access levels
    const REGISTERED = 0;
    const USER = 1;
    const ADMIN = 2;
    const LEVELS = [self::REGISTERED, self::USER, self::ADMIN];

    protected $fillable = ['email', 'password', 'person_id', 'project_id', 'dataset_id'];
    protected $hidden = ['password', 'remember_token', 'api_token'];

    public function rawLink()
    {
        return '<a href="'.url('users/'.$this->id).'">'.
            // Needs to escape special chars, as this will be passed RAW
            htmlspecialchars($this->email).'</a>';
    }

    public function identifiableName()
    {
        return $this->person ? $this->person->fullname : $this->email;
    }

    public function getNameAttribute()
    {
        return $this->identifiableName();
    }


    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('email', 'asc');
        });
        // before delete() method call this
        static::deleting(function($user) {
            $user->userjobs->each->delete();
            $user->projects()->detach();
            $user->datasets()->detach();
            $user->biocollections()->detach();
            $user->odbrequests->each->delete();
            // do the rest of the cleanup...

        });
    }

    public function setToken()
    {
        $this->api_token = substr(bcrypt($this->email.date('YmdHis').config('app.key')), 8, 12);
        $this->save();
    }

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    // NOTE, these functions should never be used to VALIDATE authorization, only to display a default in forms.
    // Use $user->projects and $user->datasets for authorization
    public function defaultDataset()
    {
        return $this->belongsTo(Dataset::class, 'dataset_id');
    }

    public function defaultProject()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function userjobs()
    {
        return $this->hasMany(UserJob::class);
    }

    public function odbrequests()
    {
        return $this->hasMany(ODBRequest::class);
    }

    public function getTextAccessAttribute()
    {
        return Lang::get('levels.access.'.$this->access_level);
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class)->withPivot('access_level');
    }

    public function datasets()
    {
        return $this->belongsToMany(Dataset::class)->withPivot('access_level');
    }

    public function biocollections()
    {
        return $this->belongsToMany(Biocollection::class)->withPivot('access_level');
    }

    public function editableDatasets()
    {
        $projects = $this->projects()->where('access_level','>',Project::VIEWER)->pluck('projects.id')->toArray();
        $dts = $this->datasets()->where('access_level','>',Project::VIEWER)->pluck('datasets.id')->toArray();
        $datasets=null;
        if (count($dts)>0 and count($projects)) {
            $datasets = Dataset::whereIn('id',$dts)->orWhereIn('project_id',$projects);    
        } elseif(count($dts)>0) {
            $datasets = Dataset::whereIn('id',$dts);
        } elseif (count($projects)>0) {
            $datasets = Dataset::whereIn('project_id',$projects);        
        } else {
            $datasets = null;
        }        
        return $datasets;
    }

    public function visibleDatasets()
    {
        $projects = $this->projects()->where('access_level','>=',Project::VIEWER)->pluck('projects.id')->toArray();
        $dts = $this->datasets()->where('access_level','>=',Project::VIEWER)->pluck('datasets.id')->toArray();
        if (count($dts)>0 and count($projects)) {
            $datasets = Dataset::whereIn('id',$dts)->orWhereIn('project_id',$projects);    
        } elseif(count($dts)>0) {
            $datasets = Dataset::whereIn('id',$dts);
        } elseif (count($projects)>0) {
            $datasets = Dataset::whereIn('project_id',$projects);        
        } else {
            $datasets = null;
        }        
        return $datasets;
    }


    public function forms()
    {
        return $this->hasMany(Form::class);
    }

    public function form_tasks()
    {
        return $this->hasMany(FormTask::class);
    }
}
