<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;
use App\Models\User;
use App\Models\Taxon;
use DB;
use CodeInc\StripAccents\StripAccents;
use Illuminate\Support\Arr;

use Activity;

use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Dataset extends Model
{
    use HasAuthLevels, LogsActivity;

    const PRIVACY_AUTH = 0;
    const PRIVACY_PROJECT = 1;
    const PRIVACY_REGISTERED = 2;
    const PRIVACY_PUBLIC = 3;
    const PRIVACY_LEVELS = [
      self::PRIVACY_AUTH,
      self::PRIVACY_PROJECT,
      self::PRIVACY_REGISTERED,
      self::PRIVACY_PUBLIC
    ];

    protected $fillable = [
      'name',
      'description',
      'privacy',
      'policy',
      'metadata',
      'title',
      'license',
      'project_id'
    ];

    //activity log trait to audit changes in record
    //protected static $logName = 'dataset';
    protected static $recordEvents = ['updated','deleted'];
    //protected static $ignoreChangedAttributes = ['updated_at'];
    //protected static $logFillable = true;
    //protected static $logOnlyDirty = true;
    //protected static $submitEmptyLogs = false;
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->useLogName('dataset')
        ->logFillable()
        ->logOnlyDirty()
        ->dontLogIfAttributesChangedOnly(['updated_at'])
        ->dontSubmitEmptyLogs();      
    }


    public function rawLink()
    {
        return "<a href='".url('dataset-show/'.$this->id)."' data-bs-toggle='tooltip' rel='tooltip' data-bs-placement='right' title='Dataset details'>".htmlspecialchars($this->name).'</a>';
    }

    public function getTitleLinkAttribute(): string
    {
        $title = $this->title ? $this->title : $this->name;
        return "<a href='".url('dataset-show/'.$this->id)."' data-bs-toggle='tooltip' rel='tooltip' data-bs-placement='right' title='Dataset details'>".htmlspecialchars($title).'</a>';
    }


    public function getFullnameAttribute()
    {
      return $this->name;
    }

    //related models
    public function measurements()
    {
        return $this->hasMany(Measurement::class);
    }
    public function individuals()
    {
        return $this->hasMany(Individual::class);
    }
    public function vouchers()
    {
        return $this->hasMany(Voucher::class);
    }
    public function media()
    {
        return $this->hasMany(Media::class);
    }


    protected static function boot()
    {
        parent::boot();

        // before delete() method call this
        static::deleting(function($dataset) {
              $dataset->authors->each->delete();
              $dataset->tags()->sync([]);
              $dataset->measurements->each->delete();
              $dataset->versions->each->delete();
              $dataset->individuals->each->delete();
              $dataset->vouchers->each->delete();
              $dataset->media->each->delete();
              $dataset->references()->sync([]);
              $dataset->users()->sync([]);
              $dataset->activities->each->delete();
              /* remove dataset if referenced as a default one to any user */
              $sql = "UPDATE users SET dataset_id=NULL WHERE dataset_id='".$dataset->id."'";
              DB::statement($sql);
        });
    }

    public function measuredVouchers( )
    {
        return $this->hasManyThrough(
                      'App\Models\Voucher',
                      'App\Models\Measurement',
                      'dataset_id', // Foreign key on Measurement table...
                      'id', // Foreign key on individual table...
                      'id', // Local key on Dataset table...
                      'measured_id' // Local key on Measurement table...
                      )->where('measurements.measured_type', 'App\Models\Voucher')->distinct();
    }
    public function measuredIndividuals( )
    {
        return $this->hasManyThrough(
                      'App\Models\Individual',
                      'App\Models\Measurement',
                      'dataset_id', // Foreign key on Measurement table...
                      'id', // Foreign key on individual table...
                      'id', // Local key on Dataset table...
                      'measured_id' // Local key on Measurement table...
                      )->where('measurements.measured_type', 'App\Models\Individual')->distinct();
    }

    public function individualLocations( )
    {
        return $this->hasManyThrough(
                      'App\Models\IndividualLocation',
                      'App\Models\Individual',
                      'dataset_id', // Foreign key on Measurement table...
                      'individual_id', // Foreign key on individual table...
                      'id', // Local key on Dataset table...
                      'id' // Local key on Measurement table...
                      );
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }


    /* authorship */
    public function authors()
    {
      return $this->morphMany(Collector::class, 'object')->with('person');
    }

    public function author_first()
    {
        return $this->authors()->where('main',1);
    }


    public function getTagLinksAttribute()
    {
        if ($this->tags->count()) {
            $tgs = $this->tags->map(function($tag){
              return "<a href='".url('tags/'.$tag->id)."'>".htmlspecialchars($tag->name).'</a>';
            })->toArray();
            return implode(" | ",$tgs);

        }
        return null;
    }

    public function getTaggedWidthAttribute()
    {
        if ($this->tags->count()) {
          $tgs = $this->tags->map(function($tag){
            return $tag->name;
          })->toArray();
          return implode(" | ",$tgs);
      }
      return null;
    }

    public function versions()
    {
        return $this->hasMany(DatasetVersion::class);
    }


    public function last_version()
    {
        return $this->hasOne(DatasetVersion::class)->orderBy('id','desc');
    }


    public function references()
    {
        return $this->belongsToMany(BibReference::class,'dataset_bibreference')->withPivot(['mandatory'])->withTimestamps();
    }


    // for use in the trait edit dropdown
    public function getPrivacyLevelAttribute()
    {
        return Lang::get('levels.privacy.'.$this->privacy);
    }

    public function getContactEmailAttribute()
    {
        if ($this->privacy==self::PRIVACY_PROJECT) {
          return $this->project->admins->first()->email;
        }
        return $this->admins->first()->email;
    }

    public function measured_classes_counts()
    {
      return DB::table('measurements')->selectRaw('measured_type,count(*) as count')->where('dataset_id',$this->id)->groupBy('measured_type')->get();
    }

    /* the id of all vouchers related to the dataset */
    public function all_bibref_ids()
    {
      $id = $this->id;
      $q1 = "SELECT bibreference_id FROM measurements WHERE measurements.dataset_id=".$id;
      $q2 = "SELECT bib_reference_id  as  bibreference_id FROM dataset_bibreference WHERE dataset_bibreference.dataset_id=".$id;
      $sql = "SELECT DISTINCT tb.bibreference_id FROM ((".$q1.") UNION (".$q2.")) AS tb";
      $get = DB::select($sql);
      return array_map(function($v) { return (int)$v->bibreference_id;},$get);    
    }

    /* the id of all vouchers related to the dataset */
    public function all_voucher_ids_query(): string
    {
      $id = $this->id;
      $q1 = "SELECT measured_id AS voucher_id FROM measurements WHERE measured_type like '%Voucher' AND measurements.dataset_id=".$id;
      $q2 = "SELECT id AS voucher_id FROM vouchers WHERE vouchers.dataset_id=".$id;
      $q3 = "SELECT media.model_id AS voucher_id FROM media WHERE media.model_type like '%Voucher' AND media.dataset_id=".$id;
      $q4 = "SELECT vouchers.id as voucher_id FROM vouchers JOIN individuals ON individuals.id=vouchers.individual_id WHERE individuals.dataset_id=".$id;
      $sql = "SELECT DISTINCT tb.voucher_id FROM ((".$q1.") UNION (".$q2.") UNION (".$q3.") UNION (".$q4.")) AS tb";
      return $sql;
    }

    public function all_voucher_ids()
    {
      $sql =$this->all_voucher_ids_query();
      $get = DB::select($sql);
      return array_map(function($v) { return (int)$v->voucher_id;},$get);
    }

    public function all_location_ids_query(): string 
    {
      $sql = $this->all_individuals_ids_query();
      $id = $this->id;
      $q1 = "SELECT location_id FROM individual_location WHERE individual_id IN (".$sql.")";
      $q2 = "SELECT measured_id AS location_id FROM measurements WHERE measured_type like '%Location' AND measurements.dataset_id=".$id;
      $q3 = "SELECT media.model_id AS location_id FROM media WHERE media.model_type like '%Location' AND media.dataset_id=".$id;
      $sql = "SELECT DISTINCT tb.location_id FROM ((".$q1.") UNION (".$q2.") UNION (".$q3.")) AS tb";
      return $sql;
    }

    public function all_locations_ids()
    {
      $sql = $this->all_location_ids_query();  
      $get = DB::select($sql);
      return array_map(function($v) { return (int)$v->location_id;},$get);
    }

    public function all_collectors_ids_query(): string 
    {
      $id = $this->id;
      $q1 = "SELECT DISTINCT collectors.person_id as person_id FROM collectors WHERE object_type LIKE '%Individual' AND object_id IN(SELECT individuals.id FROM individuals WHERE dataset_id=".$id.")";
      $q2 = "SELECT DISTINCT collectors.person_id as person_id FROM collectors WHERE object_type LIKE '%Voucher' AND object_id IN(SELECT vouchers.id FROM vouchers WHERE dataset_id=".$id.")";
      $q3 = "SELECT DISTINCT collectors.person_id as person_id FROM collectors WHERE object_type LIKE '%Media' AND object_id IN(SELECT media.id FROM media WHERE dataset_id=".$id.")";
      $q4 = "SELECT DISTINCT person_id FROM measurement_person WHERE measurement_id IN(SELECT measurements.id FROM measurements WHERE dataset_id=".$id.")";      
      $sql = "SELECT DISTINCT tb.person_id FROM ((".$q1.") UNION (".$q2.") UNION (".$q3.") UNION (".$q4.")) AS tb";
      return $sql;
    }  

    public function all_individuals_ids_query(): string 
    {
      $id = $this->id;
      $q1 = "SELECT measured_id AS individual_id FROM measurements WHERE measured_type like '%Individual' AND measurements.dataset_id=".$id;
      $q2 = "SELECT id AS individual_id FROM individuals WHERE individuals.dataset_id=".$id;
      $q3 = "SELECT media.model_id AS individual_id FROM media WHERE media.model_type like '%Individual' AND media.dataset_id=".$id;
      $q4 = "SELECT individual_id FROM vouchers WHERE vouchers.dataset_id=".$id;
      $q5 = "SELECT vo.individual_id FROM measurements as me JOIN vouchers as vo ON vo.id=me.measured_id WHERE me.measured_type like '%Voucher' AND me.dataset_id=".$id;
      $q6 = "SELECT vou.individual_id FROM media as med JOIN vouchers as vou ON vou.id=med.model_id WHERE med.model_type like '%' AND med.dataset_id=".$id;
      $sql = "SELECT DISTINCT tb.individual_id FROM ((".$q1.") UNION (".$q2.") UNION (".$q3.") UNION (".$q4.") UNION (".$q5.") UNION (".$q6.")) AS tb";
      return $sql;
    }  

    /* the id of all individulas related to the dataset */
    public function all_individuals_ids()
    {
      $sql = $this->all_individuals_ids_query();
      $get = DB::select($sql);
      return array_map(function($v) { return (int)$v->individual_id;},$get);
    }

    public static function summarize_contents($id)
    {
      $dataset = Dataset::findOrFail($id);
      $trait_summary = $dataset->traits_summary();
      //$plot_included = $dataset->plot_included();
      $plot_included = [];
      $html = view("datasets.summary",compact('dataset','trait_summary','plot_included'))->render();
      return $html;
    }

    public function all_media_ids()
    {
      $ids_i = $this->all_individuals_ids_query();
      $query = [];
      $query[] = "SELECT mediai.id AS media_id FROM media as mediai WHERE mediai.model_type like '%Individual' AND mediai.model_id IN (".$ids_i.")";
      $ids_b = $this->all_voucher_ids_query();
      $query[] = "SELECT mediav.id AS media_id FROM media as mediav WHERE mediav.model_type like '%Voucher' AND mediav.model_id IN (".$ids_b.")";
      $ids_l= $this->all_location_ids_query();
      $query[] = "SELECT medial.id AS media_id FROM media as medial WHERE medial.model_type like '%Location' AND medial.model_id IN (".$ids_l.")";
      $ids_t= $this->all_taxons_ids_query();
      $query[] = "SELECT mediat.id AS media_id FROM media as mediat WHERE mediat.model_type like '%Location' AND mediat.model_id IN (".$ids_t.")";
      $query[] = "SELECT media.id AS media_id FROM media WHERE media.dataset_id=".$this->id;
      $query = implode(') UNION (',$query);
      $sql = "SELECT DISTINCT tb.media_id FROM ((".$query.")) AS tb";
      $get = DB::select($sql);
      return array_map(function($v) { return (int)$v->media_id;},$get);
    }

    public function identification_summary()
    {
      $ids = $this->all_individuals_ids_query();
      return DB::table('identifications')->join('taxons','taxon_id','=','taxons.id')->selectRaw("taxons.level, SUM(IF(taxons.author_id IS NULL,1,0)) as published,  SUM(IF(taxons.author_id IS NULL,0,1)) as unpublished, COUNT(taxons.id) as total")->where('object_type','like','%Individual')->whereRaw('object_id IN ('.$ids.')')->groupBy('taxons.level')->get();      
    }

    public function traits_summary()
    {
      if ($this->measurements()->withoutGlobalScopes()->count()==0) {
        return [];
      }
      $trait_summary = DB::select("SELECT measurements.trait_id,traits.export_name,
        SUM(CASE measured_type WHEN 'App\\\Models\\\Individual' THEN 1 ELSE 0 END) AS individuals,
        SUM(CASE measured_type WHEN 'App\\\Models\\\Voucher' THEN 1 ELSE 0 END) AS vouchers,
        SUM(CASE measured_type WHEN 'App\\\Models\\\Taxon' THEN 1 ELSE 0 END) AS taxons,
        SUM(CASE measured_type WHEN 'App\\\Models\\\Location' THEN 1 ELSE 0 END) AS locations,
        count(*)  as total
        FROM measurements LEFT JOIN traits ON traits.id=measurements.trait_id WHERE measurements.dataset_id= ? GROUP BY measurements.trait_id,traits.export_name  ORDER BY traits.export_name",[$this->id]);
      return $trait_summary;
    }

   public function all_taxons_ids_query(): string
   {
    $id = $this->id;
    $sql = "SELECT DISTINCT measured_id AS taxon_id FROM measurements WHERE measured_type like '%Location' AND measurements.dataset_id=".$id;
    $sqlin = $this->all_individuals_ids_query();
    $q2 = "SELECT taxon_id FROM identifications WHERE object_id IN (".$sqlin.")";
    $sql = "SELECT DISTINCT tb.taxon_id FROM ((".$sql.") UNION (".$q2.")) AS tb";
    return $sql;
   }

   public function all_taxons_ids()
   {
     $sql = $this->all_taxons_ids_query();
     $get = DB::select($sql);
     return array_map(function($v) { return (int)$v->taxon_id;},$get);
  }

   public function taxonomic_summary(): array
   {
     $taxons_ids = $this->all_taxons_ids();
     if (count($taxons_ids)) {
       $ids = implode(",",$taxons_ids);
       $query = DB::select('SELECT COUNT(DISTINCT tb.fam) as families, COUNT(DISTINCT tb.genus) as genera, COUNT(DISTINCT tb.species) as species FROM (SELECT odb_txparent(taxons.lft,120) as fam,odb_txparent(taxons.lft,180) as genus, IF(taxons.level=210,CONCAT(odb_txparent(taxons.lft,180)," ",taxons.name),odb_txparent(taxons.lft,210)) as species FROM taxons WHERE taxons.id IN('.$ids.')) as tb');
       $query  = array_map(function($value) { return (array)$value;},$query);
       return $query[0];
     } else {
       return [];
     }
   }

   /* functions to interact with COUNT model*/
   public function summary_counts()
   {
       return $this->morphMany("App\Models\Summary", 'object');
   }

   public function summary_scopes()
   {
       return $this->morphMany("App\Models\Summary", 'scope');
   }

   public function getCount($scope="all",$scope_id=null,$target='individuals')
   {
     if (Measurement::count()==0) {
       return 0;
     }
     $query = $this->summary_counts()->where('scope_type',"=",$scope)->where('target',"=",$target);
     if (null !== $scope_id) {
       $query = $query->where('scope_id',"=",$scope_id);
     } else {
       $query = $query->whereNull('scope_id');
     }
     if ($query->count()) {
       return $query->first()->value;
     }
     return 0;
   }

   /* functions to generate counts */
   public function individualsCount()
   {
      return $this->individuals()->withoutGlobalScopes()->count();
   }

   public function vouchersCount()
   {
       return $this->vouchers()->withoutGlobalScopes()->count();
   }

   public function measurementsCount()
   {
     return $this->measurements()->withoutGlobalScopes()->count();
   }

   public function taxonsCountOld()
   {
     $count_level = Taxon::getRank('species');
     $taxons_individuals  = $this->individuals()->withoutGlobalScopes()->cursor()->map(function($individual) {
       return $individual->identification->taxon_id;
     });
     $taxons_vouchers  = $this->vouchers()->withoutGlobalScopes()->cursor()->map(function($voucher) {
       return $voucher->identification->taxon_id;
     });
     $taxons_direct =  $this->measurements()->withoutGlobalScopes()->where('measured_type','like','%Taxon')->pluck('measured_id')->toArray();
     $taxons = array_unique(array_merge($taxons_individuals,$taxons_vouchers,$taxons_direct));
     $count = Taxon::whereIn('id',$taxons)->where('level',">=",$count_level);
     return  $count;
   }

   // TODO: weird count for dataset taxa
   public function taxonsCount()
   {
     return count($this->all_taxons_ids());
   }


   public function locationsCount()
   {
     return count($this->all_locations_ids());
     //$individuals = $this->individuals()->withoutGlobalScopes()->cursor()->map(function($individual) {
      // return $individual->location_first->pluck('location_id')->toArray()[0];
     //})->toArray();
     //return count(array_unique(array_merge($individuals)));
   }

   public function mediaCount()
   {
     return $this->media()->withoutGlobalScopes()->count();
   }


   /*
   public function projectsCount()
   {
     $individuals = $this->individuals()->withoutGlobalScopes()->cursor()->pluck('project_id')->toArray();
     $vouchers = $this->vouchers()->withoutGlobalScopes()->cursor()->pluck('project_id')->toArray();
     return count(array_unique(array_merge($individuals,$vouchers)));
   }
   */
   /* PROJECT */
   public function project()
   {
       return $this->belongsTo(Project::class);
   }


   public function getProjectNameAttribute()
   {
       if ($this->project) {
           return $this->project->name;
       }
       return 'not defined';
   }

   public function getAllAuthorsAttribute()
   {
     if ($this->authors->count()==0) {
       return null;
     }
     $persons = $this->authors->map(function($person) { return $person->person->abbreviation;})->toArray();
     $persons = implode(' and ',$persons);
     return $persons;
   }

   public function getShortAuthorsAttribute()
   {
     if ($this->authors->count()==0) {
       return null;
     }
     $persons = $this->authors->map(function($person) { return $person->person->abbreviation;})->toArray();
     if (count($persons)>2) {
       $persons = implode(' and ',$persons);
     } elseif (count($persons)>0) {
       $persons = $persons[0]." et al.";
     }
     return $persons;
   }

   public function getCitationAttribute()
   {
     return $this->generateCitation($short=false,$for_dt=false);
   }

   public function generateCitation($short=true,$for_dt=false,$html=true)
   {
     if ($short) {
       $author = $this->short_authors;
     } else {
       $author = $this->all_authors;
     }
     $when = today()->format("Y-m-d");
     //$year = isset($this->last_edition_date) ? $this->last_edition_date->format('Y') : 'no data yet';
     $year = "YYYY";
     //$version = isset($this->last_edition_date) ? $this->last_edition_date->format('Y-m-d') : 'no data yet';
     $version ="v1.0.0";
     $license = (null != $this->license) ? $this->license : 'Not defined, some restrictions may apply';
     if (preg_match("/CC0/i",$license)) {
       $license = "Public domain - CC0";
     }
     $title = isset($this->title) ? $this->title : $this->name;
     if ($for_dt) {
       if ($html) {
         $title = "<a href='".url('dataset-show/'.$this->id)."'>".htmlspecialchars($title).'</a>';
       }
     }
     if (null != $author) {
       if ($html) {
         $citation = $author." (".$year.").  <strong>".$title."</strong>. Version: ".$version.".";
       } else {
         $citation = $author." (".$year."). ".$title.". Version: ".$version.".";
       }
     } else {
       if ($html) {
         $citation = "<strong>".$title."</strong>. Version: ".$version.".";
       } else {
         $citation = $title." Version: ".$version.".";
       }
     }
     /* ONLY ADDED WHEN NOT RESTRICTED */
     if ($this->privacy > self::PRIVACY_PROJECT) {
       $citation .= " License: ".$license;
     } else {
       $citation .= " License: access is private, data has restrictions";
     }
     if (!$for_dt) {
       $url =  url('dataset-show/'.$this->id);
       $citation .= '. From '.$url.', accessed '.$when.".";
     }
     return $citation;
   }

  public function getYearAttribute()
  {
    return "YYYY";
    /*
    if ($this->last_edition_date) {
      return $this->last_edition_date->format('Y');
    }
    */
    return null;
  }

  public function getVersionAttribute()
  {
    return "V.1.0";
    /*
    if ($this->last_edition_date) {
      return $this->last_edition_date->format("Y-m-d");
    }
    */
    return null;
  }

   public function getBibtexAttribute()
   {
     /*    
     if (!isset($this->last_edition_date)) {
       return null;
     }
     */
     $url =  $this->name;
     ///$year= $this->last_edition_date->format("Y")
     $year = "YYYY";
     $bibkey = preg_replace('[,| |\\.|-|_]','',StripAccents::strip( (string) $this->name ));
     $license = (null != $this->license and $this->privacy <= self::PRIVACY_PROJECT) ? $this->license : 'Not public, some restrictions may apply.';

     if (preg_match("/CC0/i",$license)) {
       $license = "Public domain - CC0";
     }

     //$version = "Version: ".$this->last_edition_date->format("Y-m-d")." ";
     //$this->last_edition_date->format("Y"),
     $version = "V.1.0";
     $bib =  [
        'title' => isset($this->title) ? $this->title : $this->name,
        'year' => $year,
        'author' => $this->all_authors,
        'howpublished' => "url\{".url('dataset/'.$this->id)."}",
        'version' => $version,
        'license' => $license,
        'note' => $version.$license." Accessed: ".today()->format("Y-m-d"),
        'url' => "{".url('dataset/'.$this->id)."}",
     ];
     return "@misc{".$bibkey.",\n".json_encode($bib,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
   }

   /* FUNCTIONS FOR GETTING SUMMARY CONTENT FOR THE DATASET */

   /* the percentage of individuals that belong to a plot in the dataset */
   public function plot_included()
   {
      /* individuals are from plots or transects */
      $id = $this->id;

      $adm_levels = implode(",",[Location::LEVEL_PLOT,Location::LEVEL_TRANSECT]);
      $sql = $this->all_location_ids_query();      
      $plots = Location::whereIn('adm_level',[100,101])->whereRaw('id IN('.$sql.')')->pluck('id');
      if (count($plots)==0) {
        return [];
      }
      $sql2 = $this->all_individuals_ids_query();
      $ninplots = IndividualLocation::withoutGlobalScopes()->selectRaw('DISTINCT(individual_id)')->whereRaw('individual_id IN('.$sql2.')')
      ->whereIn('location_id',$plots)->count();

      $sql3 = "SELECT DISTINCT individual_id FROM individual_location WHERE
      individual_id IN(".$sql2.") AND location_id IN(".$sql.")";
      $inplots = DB::select($sql3);
      $ninplots = count($inplots);

      /* measurements are from individuals in plots */
      $ninds = 0;
      $ninplots_meas =0;
      if ($this->measurements()->withoutGlobalScopes()->count()) {
        $query = Measurement::withoutGlobalScopes()->select("id","measured_id")->where('dataset_id',$id)->where('measured_type','like',"%Individual")->whereRaw('measured_id IN('.$sql3.')');
        $ninplots_meas = $query->count();
        $ninds = $query->pluck('measured_id')->unique()->count();
      }

      $result = [
        Lang::get("messages.number_plots") => count($plots),
        Lang::get("messages.individuals_in_plots")  => $ninplots,
        Lang::get("messages.individuals_in_plots_measured") => $ninplots_meas,
        Lang::get("messages.measurements_of_plot_individuals")  => $ninds,        
      ];
      $result = array_filter($result,function($v) { return $v>0;});
      return $result;
   }

   /* DEṔRECATED
   public function data_included()
   {
     $type = [
       "MeasurementsOrFacts" => [
            'Individuals' => $this->measurements()->withoutGlobalScopes()->where('measured_type','like','%individual%')->count(),
           'Vouchers' => $this->measurements()->withoutGlobalScopes()->where('measured_type','like','%voucher%')->count(),
           'Taxons' => $this->measurements()->withoutGlobalScopes()->where('measured_type','like','%taxon%')->count(),
           'Locations' => $this->measurements()->withoutGlobalScopes()->where('measured_type','like','%location%')->count(),
           'Total' => $this->measurements()->withoutGlobalScopes()->count(),
       ],
       "MediaFiles" => [
           'Individuals' => $this->media()->withoutGlobalScopes()->where('model_type','like','%individual%')->count(),
           'Vouchers' => $this->media()->withoutGlobalScopes()->where('model_type','like','%voucher%')->count(),
           'Taxons' => $this->media()->withoutGlobalScopes()->where('model_type','like','%taxon%')->count(),
           'Locations' => $this->media()->withoutGlobalScopes()->where('model_type','like','%location%')->count(),
           'Total' => $this->media()->withoutGlobalScopes()->count(),
        ],
     "Occurrences" => [
          'Individuals' => $this->individuals()->withoutGlobalScopes()->count(),
          'Vouchers' => null,
          'Taxons' => null,
          'Locations' => null,
          'Total' => $this->individuals()->withoutGlobalScopes()->count(),
       ],
     "PreservedSpecimens" => [
         'Individuals' => count(array_unique($this->vouchers()->withoutGlobalScopes()->pluck("individual_id")->toArray())),
         'Vouchers' => $this->vouchers()->withoutGlobalScopes()->count(),
         'Taxons' => count(array_unique($this->vouchers()->withoutGlobalScopes()->pluck("individual_id")->toArray())),
         'Locations' => null,
         'Total' => $this->vouchers()->withoutGlobalScopes()->count(),
      ],
    ];
    $type = array_filter($type,function($v) { return $v["Total"]>0;});
    return $type;
   }
   */

    public function getDataTypeAttribute()
    {
      $type = [];
      $type["MeasurementsOrFacts"] = $this->measurements()->withoutGlobalScopes()->count();
      $type["MediaFiles"] = $this->media()->withoutGlobalScopes()->count();
      $type["Organisms"] = $this->individuals()->withoutGlobalScopes()->count();
      $type["Occurrences"] = $this->individualLocations()->withoutGlobalScopes()->count();
      $type["PreservedSpecimens"] = $this->vouchers()->withoutGlobalScopes()->count();
      $type = array_filter($type,function($v) { return $v>0;});
      return $type;
    }

    public function getDataTypeRawLink()
    {
      $types = $this->data_type;
      if (count($types)==0) {
        return null;
      }
      $urls = [
        'MeasurementsOrFacts' => 'measurements/'.$this->id.'/dataset',
        'MediaFiles' => 'media/'.$this->id.'/datasets',
        'Organisms' => 'individuals/'.$this->id.'/dataset',
        'Occurrences' => 'individuals/'.$this->id.'/location-dataset',
        'PreservedSpecimens' => 'vouchers/'.$this->id.'/dataset',
      ];
      $tips = [
        'MeasurementsOrFacts' => Lang::get('messages.tooltip_view_measurements'),
        'MediaFiles' => Lang::get('messages.tooltip_view_media'),
        'Organisms' => Lang::get('messages.tooltip_view_individuals'),
        'Occurrences' => Lang::get('messages.tooltip_view_individual_locations'),
        'PreservedSpecimens' => Lang::get('messages.tooltip_view_vouchers')
      ];
      $string = [];
      foreach($types as $key => $val) {
          $string[] = '<a href="'.url($urls[$key]).'" data-bs-toggle="tooltip" rel="tooltip" data-bs-placement="right" title="'.$tips[$key].'" >'.$key.':&nbsp;'.$val.'</a>';
      }
      $string = implode("<br>",$string);
      return $string;
    }

    //get date of last edit in this dataset
    public function getLastEditionDateAttribute()
    {
      $lastdate = [];
      $line = $this->measurements()->withoutGlobalScopes()->select('measurements.updated_at')->orderBy('updated_at','desc');
      if ($line->count()) {
        $lastdate[] = $line->first()->updated_at;
      }
      $line = $this->media()->withoutGlobalScopes()->select('media.updated_at')->orderBy('updated_at','desc');
      if ($line->count()) {
        $lastdate[] = $line->first()->updated_at;
      }
      $line = $this->individuals()->withoutGlobalScopes()->select('individuals.updated_at')->orderBy('updated_at','desc');
      if ($line->count()) {
        $lastdate[] = $line->first()->updated_at;
      }
      $line = $this->vouchers()->withoutGlobalScopes()->select('vouchers.updated_at')->orderBy('updated_at','desc');
      if ($line->count()) {
        $lastdate[] = $line->first()->updated_at;
      }      
      if (count($lastdate)==0) {
        return null;
      }
      asort($lastdate);
      return $lastdate[count($lastdate)-1];
   }

   public function getDownloadsAttribute()
   {
     return $this->morphMany("Activity", 'subject')->where('description','like','%download%')->count();
   }

   public function activities()
   {
     return $this->morphMany("Activity", 'subject');
   }

   public static function getTableName()
   {
       return (new self())->getTable();
   }


   /* dwc termsn */
   public function getAccessRightsAttribute()
   {
     if (in_array($this->privacy,[self::PRIVACY_AUTH,self::PRIVACY_PROJECT])) {
       $rights =  'Restricted access.';
     } else {
       $rights = "Open access.";
       if ($this->privacey==self::PRIVACY_REGISTERED) {
         $rights .=  ' Require user registration.';
       }
    }
    if ($this->policy) {
      $rights .= " Has the following policy: ".$this->policy;
    }
    $url = url('dataset-show/'.$this->id);
    return $rights." URL: ".$url;
   }

   public function getFormatedPrivacyAttribute()
   {
     $txt = "";
     if (in_array($this->privacy,[self::PRIVACY_PROJECT, self::PRIVACY_AUTH])) {
       $txt .='<i class="fas fa-lock"></i> ';
     }
     if ($this->privacy == self::PRIVACY_PUBLIC) {
       $txt .='<i class="fas fa-lock-open"></i> ';
     }
     if ($this->privacy == self::PRIVACY_REGISTERED) {
       $txt .='<i class="fas fa-sign-in-alt"></i> ';
     }
     $txt .= Lang::get('levels.privacy.'.$this->privacy);
     return $txt;
   }

    public function getBibliographicCitationAttribute()
    {
      return $this->generateCitation($short=false,$for_dt=false,$html=false);
    }

    public function getDwcLicenseAttribute()
    {
      if ($this->policy) {
        return $this->license." | Policy:".$this->policy;
      }
      return $this->license;
    }

    public function getPeopleAttribute()
    {
      if ($this->privacy==self::PRIVACY_PROJECT) {
        return $this->project->people;
      }

      $admins = $this->admins->map(function($u) {
        $person = isset($u->person_id) ? $u->person->full_name : null;
        return [$u->email,$person];
      });
      $collabs = $this->collabs->map(function($u) {
        $person = isset($u->person_id) ? $u->person->full_name : null;
        return [$u->email,$person];
      });
      $viewers = $this->viewers->map(function($u) {
        $person = isset($u->person_id) ? $u->person->full_name : null;
        return [$u->email,$person];
      });
      return [
        'admins' => $admins->toArray(),
        'collabs' => $collabs->toArray(),
        'viewers' => $viewers->toArray(),
      ];
    }

    public function getCentroidAttribute($wkt=null)
    {
      $locids = $this->all_locations_ids();
      if (count($locids)==0) {
        return null;
      }
      $locids = implode(',',$locids);

      $coords = "SELECT (CASE
  WHEN adm_level=999 THEN ST_X(geom)
  WHEN adm_level=101 THEN ST_X(ST_StartPoint(geom))
  ELSE ST_X(ST_Centroid(geom))
END) as longitude,
(CASE
WHEN adm_level=999 THEN ST_Y(geom)
WHEN adm_level=101 THEN ST_Y(ST_StartPoint(geom))
ELSE ST_Y(ST_Centroid(geom))
END) as latitude FROM locations WHERE locations.id IN (".$locids.")";
      $query = DB::select($coords);
      $longitudes  = array_map(function($l) { return $l->longitude;},$query);
      $latitudes  = array_map(function($l) { return $l->latitude;},$query);
      $x = array_filter($longitudes);
      $longitude = array_sum($x)/count($x);
      $y = array_filter($latitudes);
      $latitude = array_sum($y)/count($y);
      if ($wkt or !isset($wkt)) {
        return 'POINT('.$longitude." ".$latitude.')';
      }
      return [ 'longitude' => $longitude, 'latitude' => $latitude];
    }
    public function getLatitudeAttribute()
    {
      return $this->getCentroidAttribute(false)['latitude'];
    }
    public function getLongitudeAttribute()
    {
      return $this->getCentroidAttribute(false)['longitude'];
    }


    public function getLicenseLogoAttribute(): string{
        $license_logo = 'images/cc_srr_primary.png';
        if ($this->privacy >= self::PRIVACY_REGISTERED) {
          $license = explode(" ",$dataset->license);
          $license_logo = 'images/'.mb_strtolower($license[0]).".png";
        }
        return $license_logo;
    }

}
