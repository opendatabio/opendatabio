<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Activity;
use App\Models\ActivityFunctions;
use App\Models\Individual;
use App\Models\User;
use App\Models\IndividualLocation;
use App\Models\Person;
use App\Models\Voucher;
use App\Models\Taxon;
use App\Models\ODBTrait;
use App\Models\Location;
use App\Models\Measurement;
use App\Models\Dataset;
use App\Models\BibReference;
use App\Models\Project;
use App\Models\Media;

use Lang;
use Auth;
use Log;

class ActivityDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->editColumn('causer_id', function ($activity) {
            if ($activity->causer_id) {
                $user = User::find($activity->causer_id);
                if (null !== $user->person_id) {
                return  Person::find($user->person_id)->rawLink();
                } else {
                return User::find($activity->causer_id)->email;
                }
            }
        })
        ->editColumn('created_at', function ($activity) {
            return $activity->created_at->format('Y-m-d'); // human readable format
        })
        ->editColumn('subject_type', function ($activity) {
            if (null !== $activity->subject) {
              return $activity->subject->rawLink();
            }
            return '';
        })
        ->editColumn('properties', function ($activity) {
            return ActivityFunctions::formatActivityProperties($activity);
        })
        ->addColumn('select_history',  function ($activity) {
            return $activity->id;
        })
        ->rawColumns(['subject_type','properties','causer_id']);

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Activity::query()->select([
            'id',
            'log_name',
            'description',
            'properties',
            'subject_type',
            'subject_id',
            'causer_id',
            'created_at',
        ]);
        if ($this->individual_location) {
            $query = $query->where('subject_type' , IndividualLocation::class)->where('subject_id',$this->individual_location);
        }
        if ($this->individual) {
            $ids = IndividualLocation::where("individual_id",$this->individual)->pluck('id')->toArray();
            $query = $query->where(function($q) {
                $q->where('subject_type' , Individual::class)->where('subject_id',$this->individual);
                });
            if (count($ids)) {
                $query = $query->orWhere(function($ql) use ($ids) {
                    $ql->where('subject_type' , IndividualLocation::class)->whereIn('subject_id',$ids);
                    });
            }
        }
        if ($this->location) {
            $query = $query->where('subject_type' , Location::class)->where('subject_id',$this->location);
        }
        if ($this->taxon) {
            $query = $query->where('subject_type' , Taxon::class)->where('subject_id',$this->taxon);
        }
        if ($this->measurement) {
            $query = $query->where('subject_type' , Measurement::class)->where('subject_id',$this->measurement);
        }
        if ($this->dataset) {
            $query = $query->where('subject_type' , Dataset::class)->where('subject_id',$this->dataset);
        }
        if ($this->odbtrait) {
            $query = $query->where('subject_type' , ODBTrait::class)->where('subject_id',$this->odbtrait);
        }
        if ($this->bibreference) {
            $query = $query->where('subject_type' , BibReference::class)->where('subject_id',$this->bibreference);
        }
        if ($this->voucher) {
            $query = $query->where('subject_type' , Voucher::class)->where('subject_id',$this->voucher);
        }
        if ($this->person) {
            $query = $query->where('subject_type' , Person::class)->where('subject_id',$this->person);
        }
        if ($this->project) {
            $query = $query->where('subject_type' , Project::class)->where('subject_id',$this->project);
        }
        if ($this->media) {
            $query = $query->where('subject_type' , Media::class)->where('subject_id',$this->media);
        }

        $query = $query->orderby('created_at','DESC');
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {

        $buttons = [
            'pageLength',
            'print',
            'selectAll',
            'selectNone',
            [
              'extend' => 'colvis',
              'columns' => 'th:nth-child(n+2)',
            ],
          [
            'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
            'className' => 'odb-expand-dt',
            'titleAttr' => Lang::get('datatables.expand'),
          ],
          [
            'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
            'className' => 'odb-wrap-dt',
            'titleAttr' => Lang::get('datatables.wrapnowrap'),
          ],
        ];
        $hidcol = [0,1,2,5];
        $object = collect([]);
        /* needs to be a full user for seen the following buttons */
        if ($this->individual) {
            $object = Individual::findOrFail($this->individual);
        }
        if ($this->location) {
            $object = Location::findOrFail($this->location);
        }
        if ($this->taxon) {
            $object = Taxon::findOrFail($this->taxon);
        }
        if ($this->measurement) {
            $object = Measurement::findOrFail($this->measurement);
        }
        if ($this->dataset) {
            $object = Dataset::findOrFail($this->dataset);
        }
        if ($this->odbtrait) {
            $object = ODBTrait::findOrFail($this->odbtrait);
        }
        if ($this->bibreference) {
          $object = BibReference::findOrFail($this->bibreference);
        }
        if ($this->voucher) {
          $object = Voucher::findOrFail($this->voucher);
        }
        if ($this->person) {
          $object = Person::findOrFail($this->person);
        }
        if ($this->project) {
          $object = Project::findOrFail($this->project);
        }
        if ($this->media) {
          $object = Media::findOrFail($this->media);
        }
        if ($object->count()) {
          /*
          * if the user can update the object
          * then can delete history of object
          */
          if (Auth::user() ? Auth::user()->can("update",$object) : false) {
            $hidcol = [1,2,5];
            $buttons[] = [
                'text' => '<i class="fa-solid fa-trash-can fa-lg"></i>',
                'className' => 'odb-delete-button',
                'titleAttr' => Lang::get('messages.delete_selected'),
            ];
          }
        }
        return $this->builder()
            ->columns([
                'checkbox' =>  [
                    'title'     => "Sel",
                    'searchable' => false,                   
                    'orderable' => false,                  
                    ],    
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => false],
                'log_name' => ['title' => Lang::get('messages.activity_logname'), 'searchable' => true, 'orderable' => true],
                'description' => ['title' => Lang::get('messages.activity_description'), 'searchable' => true, 'orderable' => true],
                'properties' => ['title' => Lang::get('messages.changes'), 'searchable' => false, 'orderable' => false],
                'subject_type' => ['title' => Lang::get('messages.object'), 'searchable' => false, 'orderable' => false],
                'causer_id' => ['title' => Lang::get('messages.modified_by'), 'searchable' => false, 'orderable' => false],
                'created_at' => ['title' => Lang::get('messages.when'), 'searchable' => false, 'orderable' => true]
            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                'lengthMenu' => [10,25,50,100],
                'buttons' => $buttons,
                'autoWidth' => false,
                'columnDefs' => [[
                    'targets' => $hidcol,
                    'visible' => false,
                ],
                [
                    'orderable' => false,
                    'defaultContent' => '',
                    'className' => 'select-checkbox',
                    'targets' => 0,                    
                    'data' => null    
                  ],
                ],
                'select' => [
                  'style' => 'multi',
                  'selector' => 'td:first-child',
                ],
                'scrollX' => true,
            ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_activity_'.time();
    }
}
