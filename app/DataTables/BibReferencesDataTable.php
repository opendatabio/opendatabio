<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use App\Models\BibReference;
use App\Models\Biocollection;
use App\Models\UserJob;

use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Lang;
use DB;
use Gate;
use Auth;

class BibReferencesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->editColumn('bibkey', function ($reference) { return $reference->rawLink(); })
        ->addColumn('author', function ($reference) { return $reference->first_author; })
        ->addColumn('year', function ($reference) { return $reference->year; })
        ->addColumn('title', function ($reference) { return $reference->title; })
        ->addColumn('doi', function ($reference) {
          if (!empty($reference->doi)) {
            return '<a href="https://dx.doi.org/'.$reference->doi.'">'.Lang::get('messages.externallink').'</a>';
          } else {
            if (!empty($reference->url)) {
              return '<a href="'.$reference->url.'">'.Lang::get('messages.externallink').'</a>';
            }
          }
        })
        ->filterColumn('title', function ($query, $keyword) {
            $query->where('bibtex', 'like', ["%{$keyword}%"]);
        })
        ->editColumn('datasets_count', function ($reference) {
              $url = url('datasets/'.$reference->id.'/bibreference');
              return '<a href="'.$url.'">'.$reference->datasets_count.'</a>';
        })
        ->editColumn('taxons_count', function ($reference) {
          $url = url('taxons/'.$reference->id.'/bibreference');
          return '<a href="'.$url.'">'.$reference->taxons_count.'</a>';
        })
        ->editColumn('measurements_count', function ($reference) {
          $url = url('measurements/'.$reference->id.'/bibreference');
          return '<a href="'.$url.'">'.$reference->measurements_count.'</a>';
        })
        ->rawColumns(['bibkey', 'doi','datasets_count','taxons_count','measurements_count']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = BibReference::query()
            ->select([
                'id',
                'bibtex',
                'doi',
            ])->addSelect(DB::raw('odb_bibkey(bibtex) as bibkey'))
            ->withCount(['datasets','measurements','taxons']);
            
        if ($this->biocollection) {
            $bibids = Biocollection::findOrFail($this->biocollection)->bibreferences_ids();
            if (count($bibids)) {
              $query = $query->whereIn('id',$bibids);
            }
        }
        if ($this->job_id)
        {
          $ids = UserJob::find($this->job_id)->affected_ids_array;
          $query = $query->whereIn('id',$ids);
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {

      $buttons = [
        'pageLength',
        'print',
        'selectAll',
        'selectNone',
        [
          'extend' => 'colvis',
          'columns' => 'th:nth-child(n+2)',
        ],  
        [
          'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
          'className' => 'odb-expand-dt',
          'titleAttr' => Lang::get('datatables.expand'),
        ],
        [
          'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
          'className' => 'odb-wrap-dt',
          'titleAttr' => Lang::get('datatables.wrapnowrap'),
        ],
      ];
      if (Auth::user()) {
        $buttons[] = [
          'text' => Lang::get('datatables.export'),
          'className' => 'odb-export-button',
        ];
      }
      if (Gate::allows('create',BibReference::class)) {
        $url = url ('references/create');
        $buttons[] = [
          'text' => Lang::get('messages.create'),
          'className' => 'odb-create-button',
          'action' => "function(e) {
            window.location.href = '".$url."';
          }",
        ];
      }

        return $this->builder()
            ->columns([
              'checkbox' =>  [
                'title'     => "Sel",
                'searchable' => false,                   
                'orderable' => false,                  
                ],        
                'bibkey' => ['title' => Lang::get('messages.bibtex_key'), 'searchable' => false, 'orderable' => true],
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
                'author' => ['title' => Lang::get('messages.authors'), 'searchable' => false, 'orderable' => false],
                'year' => ['title' => Lang::get('messages.year'), 'searchable' => false, 'orderable' => false],
                'title' => ['title' => Lang::get('messages.title'), 'searchable' => true, 'orderable' => false],
                'doi' => ['title' => Lang::get('messages.linktobib'), 'searchable' => true, 'orderable' => false],
                'datasets_count' => ['title' => Lang::get('messages.datasets'), 'searchable' => false, 'orderable' => true],
                'measurements_count' => ['title' => Lang::get('messages.measurements'), 'searchable' => false, 'orderable' => true],
                'taxons_count' => ['title' => Lang::get('messages.taxons'), 'searchable' => false, 'orderable' => true],
            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                'order' => [[7, 'desc']],
                'lengthMenu' => [10,25,50,100],
                'buttons' => $buttons,
                'autoWidth' => false,
                'columnDefs' => [
                  [
                    'targets' => [2],
                    'visible' => false,
                  ],                  
                  [
                    "targets" => [7,8,9],
                    "className" => "text-center",
                  ],
                  [
                    'orderable' => false,
                    'defaultContent' => '',
                    'className' => 'select-checkbox',
                    'targets' => 0,                    
                    'data' => null    
                  ],
                ],
                'select' => [
                  'style' => 'multi',
                  'selector' => 'td:first-child',
                ],
                'scrollX' => true,
            ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_references_'.time();
    }
}
