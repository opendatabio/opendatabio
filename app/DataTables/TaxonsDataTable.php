<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use App\Models\Taxon;
use App\Models\Location;
use App\Models\Measurement;
use App\Models\Biocollection;

use Baum\Node;
use App\Models\Project;
use App\Models\Dataset;
use App\Models\UserJob;

use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Arr;

use Lang;
use DB;
use Auth;
use Gate;
use Log;

class TaxonsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */

    public function dataTable(DataTables $dataTables, $query)
    {
        $table = (new EloquentDataTable($query))
        ->editColumn('scientificName', function ($taxon) {
            return $taxon->rawLink();
        })
        ->orderColumn('scientificName', 'odb_txname(taxons.name, taxons.level, taxons.parent_id, 0,1) $1')
        ->filterColumn('scientificName', function ($query, $keyword) {
            $query->whereRaw('odb_txname(taxons.name, taxons.level, taxons.parent_id, 0,0) like ?', ["%".$keyword."%"]);
            //$query->whereRaw('odb_highertaxon(taxons.lft) like ?', ["%".$keyword."%"]);
        })
        ->editColumn('taxonRank', function ($taxon) { return $taxon->taxonRank; })
        ->addColumn('scientificNameAuthorship', function ($taxon) { return $taxon->scientificNameAuthorship; })
        ->addColumn('parentname', function ($taxon) {
          if (isset($taxon->parent)) {
            $url = url('taxons/'.$taxon->parent->id.'/taxon');
            return '<a href="'.$url.'">'.$taxon->parent->fullname.'</a>';
          }
          return null;
        })
        ->orderColumn('parentname', function ($query, $order, $taxon) {
          $query->orderBy('odb_txparent(taxons.lft,'.$taxon->parent->level.')', $order);
        })
        ->addColumn('family', function ($taxon) {
          return $taxon->family;
        })
        ->orderColumn('family', 'odb_txparent(taxons.lft,120) $1')
        ->addColumn('higher_classification', function ($taxon) {
          return $taxon->higher_classification;
        })
        ->addColumn('external', function ($taxon) {
            $ret = '';
            if ($taxon->mobot) {
                $ret .= '<a href="'.config("external-apis.tropicos.linkto").$taxon->mobot.'"  title="MOBOT-Tropicos.org" target="_blank"><img src="'.asset('images/TropicosLogo.gif').'"  height="24px"></a>&nbsp;';
            }
            if ($taxon->ipni) {
                $ret .= '<a href="'.config("external-apis.ipni.linkto").$taxon->ipni.'" title="International Plant Names Index - IPNI" target="_blank"><img src="'.asset('images/IpniLogo.png').'" height="24px"></a>&nbsp;';
            }
            if ($taxon->mycobank) {
                $ret .= '<a href="'.config("external-apis.mycobank.linkto").$taxon->mycobank.'&Fields=All"  title="MycoBank.org" target="_blank"><img src="'.asset('images/MBLogo.png').'" height="24px"></a>&nbsp;';
            }
            if ($taxon->zoobank) {
                $ret .= '<a href="'.config("external-apis.zoobank.linkto").$taxon->zoobank.'"  title="ZooBank.org" target="_blank"><img src="'.asset('images/zoobank.png').'" height="24px"></a>&nbsp;';
            }
            if ($taxon->gbif) {
                $ret .= '<a href="'.config("external-apis.gbif.linkto").$taxon->gbif.'" title="GBIF.org" target="_blank"><img src="'.asset('images/GBIF-2015-mark.png').'" height="24px"></a>&nbsp;';
            }
            return $ret;
        });
        if ($this->add_summary_counts) {
          return $table
          ->addColumn('individuals', function ($taxon) {
            //return $taxon->individuals()->withoutGlobalScopes()->count();
            if ($this->project) {
              $individual_count = $taxon->getCount('App\Models\Project',$this->project,'individuals');
              return '<a href="'.url('individuals/'.$taxon->id.'|'.$this->project.'/taxon_project').'">'.$individual_count.'</a>';
            }
            if ($this->dataset) {
                $individual_count = $taxon->getCount('App\Models\Dataset',$this->dataset,'individuals');
                return '<a href="'.url('individuals/'.$taxon->id.'|'.$this->dataset.'/taxon_dataset').'">'.$individual_count.'</a>';
            }
            if ($this->location) {
                $individual_count = $taxon->getCount('App\Models\Location',$this->location,'individuals');
                return '<a href="'.url('individuals/'.$taxon->id.'|'.$this->location.'/taxon_location').'">'.$individual_count.'</a>';
            }
            $individual_count = $taxon->getCount('all',null,"individuals");
            return '<a href="'.url('individuals/'.$taxon->id.'/taxon').'">'.$individual_count.'</a>';
          })
          ->addColumn('vouchers', function ($taxon) {
            $voucher_count =0;
            if ($this->project) {
              $voucher_count = $taxon->getCount('App\Models\Project',$this->project,'vouchers');
              return '<a href="'.url('vouchers/'.$taxon->id.'|'.$this->project.'/taxon_project').'">'.$voucher_count.'</a>';
            }
            if ($this->dataset) {
                $voucher_count = $taxon->getCount('App\Models\Dataset',$this->dataset,'vouchers');
                return '<a href="'.url('vouchers/'.$taxon->id.'|'.$this->dataset.'/taxon_dataset').'">'.$voucher_count.'</a>';
            }
            if ($this->location) {
                $voucher_count = $taxon->getCount('App\Models\Location',$this->location,'vouchers');
                return '<a href="'.url('vouchers/'.$taxon->id.'|'.$this->location.'/taxon_location').'">'.$voucher_count.'</a>';
            }
            $voucher_count = $taxon->getCount('all',null,"vouchers");
            return '<a href="'.url('vouchers/'.$taxon->id.'/taxon').'">'.$voucher_count.'</a>';
          })
          ->addColumn('measurements', function ($taxon) {
            if ($this->project) {
              $measurements_count = $taxon->getCount('App\Models\Project',$this->project,"measurements");
              return '<a href="'.url('measurements/'.$taxon->id.'|'.$this->project.'/taxon_project').'">'.$measurements_count.'</a>';
            }
            if ($this->dataset) {
                $measurements_count = $taxon->getCount('App\Models\Dataset',$this->dataset,'measurements');
                return '<a href="'.url('measurements/'.$taxon->id.'|'.$this->dataset.'/taxon_dataset').'">'.$measurements_count.'</a>';
            }
            if ($this->location) {
                $measurements_count = $taxon->getCount('App\Models\Location',$this->location,'measurements');
                return '<a href="'.url('measurements/'.$taxon->id.'|'.$this->location.'/taxon_location').'">'.$measurements_count.'</a>';
            }
            $measurements_count = $taxon->getCount('all',null,"measurements");
            return '<a href="'.url('measurements/'.$taxon->id.'/taxon').'">'.$measurements_count.'</a>';
          })
          ->addColumn('media', function ($taxon) {
            $mediaCount = $taxon->getCount('all',null,"media");
            $urlShowAllMedia = "media/".$taxon->id."/taxons";
            return '<a href="'.url($urlShowAllMedia).'">'.$mediaCount.'</a>';
          })
          ->rawColumns(['scientificName', 'individuals', 'vouchers', 'measurements', 'media', 'external','family','parentname','checkbox']);
        } else {
          /*
          $table =$table
          ->addColumn('individuals', function ($taxon) {
            $individual_count = $taxon->getCount('all',null,"individuals");
            return '<a href="'.url('individuals/'.$taxon->id.'/taxon').'">'.$individual_count.'</a>';
          })
          ->addColumn('vouchers', function ($taxon) {
            $voucher_count = $taxon->getCount('all',null,"vouchers");
            return '<a href="'.url('vouchers/'.$taxon->id.'/taxon').'">'.$voucher_count.'</a>';
          });
          */
        }

        return $table->rawColumns(['scientificName', 'external','family','parentname','checkbox']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Taxon::query()->where('taxons.level', '<>', -1);

        if ($this->job_id)
        {
          $ids = UserJob::find($this->job_id)->affected_ids_array;
          $query = $query->whereIn('id',$ids);
        }

        if ($this->project) {
          $project = $this->project;
          if (!is_array($this->project)) {
              $project = [$this->project];
          }
          $all_taxons_ids = array_unique(Arr::flatten(Project::whereIn('id',$project)
          ->get()->map(function($d){
            return $d->all_taxons_ids();
          })->toArray()));
          if (count($all_taxons_ids)>0) {
            $query->whereIn('id',$all_taxons_ids);
          }
        }

        if ($this->dataset) {
          $dataset = $this->dataset;
          if (!is_array($this->dataset)) {
              $dataset = [$this->dataset];
          }
          $all_taxons_ids = array_unique(Arr::flatten(Dataset::whereIn('id',$dataset)
          ->get()
          ->map(function($d){
            return $d->all_taxons_ids();
          })->toArray()));
          if (count($all_taxons_ids)>0) {
            $query->whereIn('taxons.id',$all_taxons_ids);
          }
        }

        if ($this->taxon) {
          $taxon = $this->taxon;
          if (!is_array($taxon)) {
            $taxon = [$taxon];
          }
          $taxon_list = array_unique(Arr::flatten(Taxon::whereIn('id',$taxon)->get()
          ->map(function($t) {
            return $t->descendantsAndSelf()->pluck('id')->toArray();
          })->unique()->toArray()));
          $query->whereIn('taxons.id',$taxon_list);
        }

        if ($this->request()->has('level')) {
          $level =  $this->request()->get('level');
          if ($level != "" and null !== $level) {
            $query = $query->where('taxons.level',$level);
          }
        }
        if ($this->location) {
          $location = $this->location;
          if (!is_array($this->location)) {
              $location = [$this->location];
          }
          $all_taxons_ids = array_unique(Arr::flatten(Location::whereIn('id',$location)
          ->get()->map(function($d){
            return $d->all_taxons_ids();
          })->toArray()));
          if (count($all_taxons_ids)>0) {
            $query->whereIn('id',$all_taxons_ids);
          } else {
            $query->where('id',0);
          }
        }

        if ($this->biocollection) {
            $biocollection = $this->biocollection;
            if (!is_array($biocollection)) {
                $biocollection = [$biocollection];
            }

            $query->whereHas('vouchers',function($voucher) use($biocollection) {
              $voucher->whereIn('vouchers.biocollection_id',$biocollection);
            });
            /*
            $all_taxons_ids = array_unique(Arr::flatten(
              Biocollection::whereIn('id',$biocollection)->get()->
              map(function($d){
                return $d->taxonsIds();}
                )->toArray()
              ));
            if (count($all_taxons_ids)>0) {
              $query->whereIn('taxons.id',$all_taxons_ids);
            }
            */
        }
        if ($this->bibreference) {
            $query->whereHas('reference',function($bib) { $bib->where('bib_references.id',$this->bibreference);});
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {

      $taxons_level =  Taxon::where('level','<>',-1)->select("level")->distinct()->pluck('level')->toArray();
      $title_level = Lang::get('messages.level');
        if (count($taxons_level)) {
          $title_level  = Lang::get('messages.level').'&nbsp;<select class="form-control-sm" name="level" id="taxon_level" ><option value="">'.Lang::get('messages.all').'</option>';
          foreach ($taxons_level as $level) {
                 $title_level  .= '<option value="'.$level.'" >'.Lang::get('levels.tax.' . $level).'</option>';
          }
          $title_level  .= '</select>';
        }

        $buttons = [
          'pageLength',
          'selectAll',
          'selectNone',
          'print',
          'colvis', 
          [
            'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
            'className' => 'odb-expand-dt',
            'titleAttr' => Lang::get('datatables.expand'),
          ],
          [
            'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
            'className' => 'odb-wrap-dt',
            'titleAttr' => Lang::get('datatables.wrapnowrap'),
          ],
          [
            'text' => Lang::get('datatables.filter'),
            'className' => 'odb-filter-button',
            'titleAttr' => Lang::get('messages.filter_data'),
          ],
        ];



        $columns = [
          'checkbox' =>  [
            'title'     => "Sel",
            'searchable' => false,                   
            'orderable' => false,                  
            ],
            'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
            'scientificName' => ['title' => Lang::get('messages.name'), 'searchable' => true, 'orderable' => true],
            'taxonRank' => ['title' => $title_level,'searchable' => false, 'orderable' => false],
            'parentname' => ['title' => Lang::get('messages.parent'), 'searchable' => false, 'orderable' => true],
            'family' => ['title' => Lang::get('messages.family'), 'searchable' => false, 'orderable' => true],
            'higher_classification' => ['title' => Lang::get('messages.higher_classification'), 'searchable' => false, 'orderable' => false],
            'scientificNameAuthorship' => ['title' => Lang::get('messages.author'), 'searchable' => false, 'orderable' => false],            
            'external' => ['title' => Lang::get('messages.external'), 'searchable' => false, 'orderable' => false],
        ];    
        $hidcol = [1,6];
        $center_cols = [];
        if ($this->related_taxa) {
          $hidcol = [1,4,6];
        }
        if ($this->add_summary_counts) {
          $columns2 = [          
            'individuals' => ['title' => Lang::get('messages.individuals'), 'searchable' => false, 'orderable' => false],
            'vouchers' => ['title' => Lang::get('messages.vouchers'), 'searchable' => false, 'orderable' => false],
            'measurements' => ['title' => Lang::get('messages.measurements'), 'searchable' => false, 'orderable' => false],
            'media' => ['title' => Lang::get('messages.media_files'), 'searchable' => false, 'orderable' => false],            
          ];
          $columns = array_merge($columns,$columns2);
          $center_cols = [8,9,10,11];
        }
        if (Auth::user()) {
          $buttons[] = [
            'text' => Lang::get('datatables.export'),
            'className' => 'odb-export-button',];
        }
        if (Gate::allows('create',Taxon::class)) {
          $buttons[] = [
            'text' => Lang::get('messages.create'),
            'className' => 'odb-create-button',
            'action' => "function(e) {
              window.location.href = '".url('taxons/create')."';
            }",
          ];
          $buttons[] = [
            'text' => '<i class="fa-solid fa-trash-can fa-lg"></i>',
            'className' => 'odb-delete-button',
            'titleAttr' => Lang::get('messages.delete_selected'),
          ];
        }

        return $this->builder()
            ->columns($columns)
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                'order' => [[0, 'asc']],
                'lengthMenu' => [10,20,50,100],
                'buttons' => $buttons,
                'autoWidth' => false,
                'columnDefs' => [ 
                  [
                    'targets' => $hidcol,
                    'visible' => false,
                  ],
                [
                  "targets" => $center_cols, 
                  "className" => "text-center",
                ],
                [
                  'orderable' => false,
                  'defaultContent' => '',
                  'className' => 'select-checkbox',
                  'targets' => 0,                    
                  'data' => null    
                ],
            ],
            'select' => [
                  'style' => 'multi',
                  'selector' => 'td:first-child',
            ],
            'scrollX' => true,

            ]);
    }




    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_taxons_'.time();
    }
}

