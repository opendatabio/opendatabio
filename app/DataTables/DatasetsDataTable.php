<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use App\Models\Dataset;
use App\Models\Project;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Gate;
use Lang;
use DB;
use Auth;

class DatasetsDataTable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->editColumn('name',function($dataset) {
          return $dataset->rawLink();
        })
        ->editColumn('title', function ($dataset) {
            //return $dataset->rawLink();
            $title = $dataset->title ? $dataset->title : $dataset->name;
            $ret =  "<strong style='cursor:pointer;'>".$title."</strong>";
            $description_brief = "";
            if ($dataset->description) {
              $description_brief = substr($dataset->description,0,100);
              if (strlen($dataset->description)>100) {
                $description_brief .= "...";
              }                                   
              $ret .= "<span id='".$dataset->id."description_brief'> ".$description_brief."</span>";
            }
            $btn1 = '        
              <span style="cursor:pointer;">
                  <i class="fa-solid fa-circle-info text-secondary"></i>
              </span>';
            $ret .= $btn1;
            $id = "description_".$dataset->id;
            $btn= $dataset->description.'<div class="mt-2" >
            <a href="'.url('dataset-show/'.$dataset->id).'" class="btn btn-outline-primary btn-sm" title="'.Lang::get('messages.details').'">
              <i class="fas fa-info fa-lg"></i>&nbsp;Detalhes
            </a>&nbsp;&nbsp;';
            $ret .= '<div id="'.$id.'" class="collapse" >'.$btn.'</div></div>';
            return $ret;
            //return $dataset->rawLink();
        })
        ->editColumn('privacy', function ($dataset) {
            return $dataset->formated_privacy;
        })
        ->editColumn('license', function ($dataset) {
            $license_logo = 'images/cc_srr_primary.png';
            if (null != $dataset->license and null == $dataset->policy) {
              $license = explode(" ",$dataset->license);
              $license_logo = 'images/'.mb_strtolower($license[0]).".png";
            }
            $txt = '<img src="'.asset($license_logo).'" width="80px">';
            if (null != $dataset->license) {
              $txt = $dataset->license."<br>".$txt;
            }
            return $txt;
        })
        ->addColumn('version', function ($dataset) {
          return implode(" | ",$dataset->versions()->where('dataset_access',0)->get()->map(function($d){
            return $d->raw_link;
          })->toArray());
        })
        ->addColumn('contents', function ($dataset) {
            return $dataset->getDataTypeRawLink();
        })
        ->addColumn('downloads', function ($dataset) {
            return $dataset->downloads;
        })
        ->addColumn('tags', function ($dataset) {
            return $dataset->tagLinks;
        })
        ->addColumn('project', function ($dataset) {
          return $dataset->project ? $dataset->project->rawLink() : '';
        })
        ->filterColumn('name', function ($query, $keyword) {
            $query->whereHas('tags',function($tag) use($keyword) {
                $tag->whereHas('translations',function($trn) use ($keyword) { $trn->where('translation','like','%'.$keyword.'%');
                });
            })
            ->orWhere('title','like','%'.$keyword.'%')
            ->orWhere('name','like','%'.$keyword.'%');
        })
        ->rawColumns(['name', 'title','tags','contents','license','privacy','checkbox','version','project'])
        ;
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Dataset::query()->with('project')->select("datasets.*");
        //->withCount(
          //['measurements'])->with(['users', 'tags.translations']);

        if ($this->project) {
            $query = $query->where('project_id',$this->project);
        }
        if ($this->tag) {
            $query->whereHas('tags',function($tag) { $tag->where('tags.id',$this->tag);});
        }
        if ($this->bibreference) {
            $query->whereHas('references',function($bib) { $bib->where('bib_references.id',$this->bibreference);});
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {

        $buttons = [
          'pageLength',
          'csv',
          'excel',
          'print',
          'reload',
          'colvis',
          [
            'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
            'className' => 'odb-expand-dt',
            'titleAttr' => Lang::get('datatables.expand'),
          ],
          [
            'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
            'className' => 'odb-wrap-dt',
            'titleAttr' => Lang::get('datatables.wrapnowrap'),
          ],
          /*
          [
            'text' => '<i class="fas fa-map-marked-alt fa-lg"></i>',
            'className' => 'odb-map-selected',
            'titleAttr' => Lang::get('messages.map'),
          ],
          */
        ];
        if (Gate::allows('create',Dataset::class)) {
          $url = url ('datasets/create');
          $buttons[] = [
            'text' => Lang::get('messages.create'),
            'className' => 'odb-create-button',
            'action' => "function(e) {
              window.location.href = '".$url."';
            }",
          ];
        }


        return $this->builder()
            ->columns([
                'checkbox' =>  [
                  'title'     => "Check",          
                  'searchable' => false,
                  'orderable' => false,                  
                ],
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
                'name' => ['title' => Lang::get('messages.name'), 'searchable' => true, 'orderable' => true],
                'title' => ['title' => Lang::get('messages.title'), 'searchable' => false, 'orderable' => true],
                'privacy' => ['title' => Lang::get('messages.privacy'), 'searchable' => false, 'orderable' => true],
                'license' => ['title' => Lang::get('messages.license'), 'searchable' => false, 'orderable' => true],
                'version' => ['title' => Lang::get('messages.public_versions'), 'searchable' => false, 'orderable' => false],
                'contents' => ['title' => Lang::get('messages.contains'), 'searchable' => false, 'orderable' => false],
                'tags' => ['title' => Lang::get('messages.tags'), 'searchable' => false, 'orderable' => false],
                'project'=> ['title' => Lang::get('messages.project'), 'searchable' => false, 'orderable' => false],
                'downloads' => ['title' => Lang::get('messages.downloads'), 'searchable' => false, 'orderable' => false],
            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                'lengthMenu' => [10,25,50,100],
                'buttons' => $buttons,
                'autoWidth' => false,
                'columnDefs' => [
                  [
                    'targets' => [1,2,4,5],
                    'visible' => false,
                  ],
                  [
                    'orderable' => false,
                    'defaultContent' => '',
                    'className' => 'select-checkbox',
                    'targets' => 0,                    
                    'data' => null    
                  ],
                  [
                    "targets" => [8], 
                    "className" => "text-center",
                  ],
              ],
              'select' => [
                    'style' => 'multi+shift',
                    'selector' => 'td:first-child',
              ],
              'scrollX' => true,
            ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_datasets_'.time();
    }
}
