<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use App\Models\Biocollection;
use App\Models\UserJob;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Lang;
use Gate;

class BiocollectionsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->addColumn('details', function ($biocollection) {
            if($biocollection->irn >0) {
              return '<a href="http://sweetgum.nybg.org/science/ih/herbarium_details?irn='.($biocollection->irn).'>'.Lang::get('messages.details').'</a>';
            } else {
              return Lang::get('messages.noih');
            }
        })
        ->editColumn('acronym', function($biocollection) {
          return $biocollection->rawLink();
        })
        ->addColumn('vouchers', function ($biocollection) {
            if($biocollection->vouchers_count >0) {
              return '<a href="'.url('vouchers/'.$biocollection->id.'/biocollection').'">'.$biocollection->vouchers_count .'</a>';
            } else {
              return 0;
            }
        })
        ->editColumn('irn', function($biocollection) {
          return $biocollection->irn_url;
        })
        ->rawColumns(['details','acronym','vouchers','irn']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Biocollection::query()->withCount('vouchers');
        if ($this->job_id)
        {
          $ids = UserJob::find($this->job_id)->affected_ids_array;
          $query = $query->whereIn('id',$ids);
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
      $buttons = [
        'pageLength',
        'csv',
        'excel',
        'print',
        'selectAll',
        'selectNone',
        [
          'extend' => 'colvis',
          'columns' => 'th:nth-child(n+2)',
        ],  
        [
          'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
          'className' => 'odb-expand-dt',
          'titleAttr' => Lang::get('datatables.expand'),
        ],
        [
          'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
          'className' => 'odb-wrap-dt',
          'titleAttr' => Lang::get('datatables.wrapnowrap'),
        ],
      ];

      if (Gate::allows('create',Biocollection::class)) {
        $url = url ('biocollections/create');
        $buttons[] = [
          'text' => Lang::get('messages.create'),
          'className' => 'odb-create-button',
          'action' => "function(e) {
            window.location.href = '".$url."';
          }",
        ];
      }
        return $this->builder()
            ->columns([
                'checkbox' =>  [
                  'title'     => "Sel",
                  'searchable' => false,                   
                  'orderable' => false,                  
                ],    
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
                'acronym' => ['title' => Lang::get('messages.acronym'), 'searchable' => true, 'orderable' => true],
                'name' => ['title' => Lang::get('messages.institution'), 'searchable' => true, 'orderable' => true],
                'details' => ['title' => Lang::get('messages.details'), 'searchable' => true, 'orderable' => false],
                'vouchers' => ['title' => Lang::get('messages.vouchers'), 'searchable' => true, 'orderable' => false],
                'irn' => ['title' => "IndexHerbariorum", 'searchable' => true, 'orderable' => true],

            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                'lengthMenu' => [10,25,50,100],
                'buttons' => $buttons,
                'autoWidth' => false,
                'columnDefs' => [
                    [
                    'orderable' => false,
                    'defaultContent' => '',
                    'className' => 'select-checkbox',
                    'targets' => 0,                    
                    'data' => null    
                  ],
                ],
                'select' => [
                  'style' => 'multi',
                  'selector' => 'td:first-child',
                ],
                'scrollX' => true,
            ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_biocollections_'.time();
    }
}
