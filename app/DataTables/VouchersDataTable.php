<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use Baum\Node;
use App\Models\Voucher;
use App\Models\Location;
use App\Models\Taxon;
use App\Models\ODBRequest;
use App\Models\UserJob;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Arr;

use Lang;
use DB;
use Auth;
use Gate;

class VouchersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        $table = (new EloquentDataTable($query))
        ->editColumn('fullname', function ($voucher) {
            return $voucher->rawLink();
        })
        /*->orderColumn('fullname', 'odb_voucher_fullname(vouchers.id,vouchers.number,vouchers.individual_id,vouchers.biocollection_id,vouchers.biocollection_number,vouchers.date) $1')*/
        ->editColumn('collector_number',function ($voucher) {          
          return "<a href='".url('vouchers/'.$voucher->id)."'>".htmlspecialchars($voucher->collector_number).'</a>';
        })
        /*->orderColumn('collector_number', 'odb_collector_number(vouchers.individual_id, vouchers.id,vouchers.number) $1')*/
        ->filterColumn('collector_number', function ($query, $keyword) {
          $key = str_replace("=","",$keyword);
          if ($key!=$keyword) {
            $query->where('number',$key)->orWhere('biocollection_number',$key)->orWhereHas('individual',function($q)use($key){
              $q->where('tag',$key);
            });  
          } else {
            $keyword = $keyword."%";
            $query->where('number','like',$keyword)->orWhere('biocollection_number','like',$keyword)->orWhereHas('individual',function($q)use($keyword){
              $q->where('tag','like',$keyword);
            });  
          }
        })
        ->editColumn('acronym', function ($voucher) use($query) {
            return $voucher->biocollection->rawLink();
        })
        ->editColumn('biocollection_type', function ($voucher) {
            return $voucher->is_type;
        })
        ->editColumn('biocollection_number', function ($voucher) {
            return "<a href='".url('vouchers/'.$voucher->id)."'>".htmlspecialchars($voucher->biocollection_number).'</a>';
        })
        ->addColumn('voucher_identification', function ($voucher) use($query) {
            if (!isset($voucher->identification)) {
              return Lang::get('messages.unidentified');
            }
            //return $voucher->fullname;
            $url = 'taxons/'.$voucher->identification->taxon_id;
            return "<a href='".url($url)."'><em>".htmlspecialchars($voucher->scientific_name).'</em></a>';
        })
        ->orderColumn('voucher_identification', 'odb_identification(vouchers.individual_id,0) $1')
        ->addColumn('collectors', function ($voucher) {
            return $voucher->recordedBy;
        })
        ->addColumn('individual', function ($voucher) {
            return $voucher->individual->rawLink();
        })
        ->addColumn('location',function($voucher) {
            return $voucher->higher_geography_desc;
            //return $voucher->LocationDisplay;
        })
        ->editColumn('date', function ($voucher) {
            return $voucher->recordedDate;
        })
        ->orderColumn('date', 'vouchers.date $1')
        ->addColumn('measurements', function ($voucher) {
            return '<a href="'.url('vouchers/'.$voucher->id.'/measurements').'">'.$voucher->measurements()->withoutGlobalScopes()->count().'</a>';
        })
        ->addColumn('dataset', function ($voucher) {
          return '<a href="'.url('dataset-show/'.$voucher->dataset_id).'">'.$voucher->dataset_name.'</a>';
        });
        if ($this->request_id or $this->request()->has('request_id')) {
          $request_id = $this->request_id or $this->request()->get('request_id');
          $table = $table->addColumn('status',function($voucher) {
            $st = $voucher->odbrequests->first()->pivot->status;
            return Lang::get('levels.status.'.$st);
          })->addColumn('notes',function($voucher) use($request_id) {
            return $voucher->getRequestNotes($request_id);
          });
        }
        return $table->rawColumns(['fullname', 'voucher_identification','location','measurements','parent_type','individual','biocollection_id','acronym','biocollection_number','dataset','collector_number','notes']);

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Voucher::query()->with(['biocollection',"current_location"]);

        if ($this->job_id)
        {
          $ids = UserJob::find($this->job_id)->affected_ids_array;
          $query = $query->whereIn('id',$ids);
        }
        if ($this->number) {
          $tags = explode(",",$this->number);
          $query = $query->where(function($n) use($tags) {
            $n->whereIn('number',$tags)->orWhereHas('individual',function($i) use($tags){
              $i->whereIn('tag',$tags);
            })->orWhereIn('biocollection_number',$tags);
          });
        }
        if ($this->location) {
            $location_ids = $this->location;
            if (!is_array($location_ids)) {
              $location_ids = [$location_ids];
            }
            $location = Location::withoutGeom()->whereIn('id',$location_ids);
            $locationsids = array_unique(Arr::flatten($location->cursor()->map(function($l) {
              return $l->descendantsAndSelf()->pluck('id')->toArray();
            })->unique()->toArray()));
            $query = $query->whereHas('locations',function($q) use($locationsids) {
                $q->whereIn('location_id',$locationsids)
                ->orWhereHas('relatedLocations',function($r) use($locationsids) {
                  $r->whereIn('related_id',$locationsids);
                });
              });            
        }
        if ($this->individual) {
            $query = $query->where('individual_id',$this->individual);
        }
        if ($this->dataset) {
            $dataset = $this->dataset;
            if (!is_array($dataset)) {
              $dataset = [$dataset];
            }
            $query = $query->whereIn('dataset_id', $dataset)
            ->orWhereHas('measurements', function ($q) use ($dataset) {
              $q->whereIn('dataset_id', $dataset); 
            });
        }
        if ($this->taxon) {
            $taxon = $this->taxon;
            if (!is_array($taxon)) {
              $taxon = [$taxon];
            }
            $taxon_list = array_unique(Arr::flatten(Taxon::whereIn('id',$taxon)->get()->map(function($t) {return $t->descendantsAndSelf()->pluck('id')->toArray();})->unique()->toArray()));
            $query = $query->whereHas('identification', function ($q) use ($taxon_list) { 
              $q->whereIn('taxon_id',$taxon_list); 
            });
        }
        if ($this->person) {
            $person = $this->person;
            if (!is_array($person)) {
              $person = [$person];
            }
            $query = $query->whereHas('collectors', function ($q) use ($person) {
                $q->whereIn('person_id', $person);
            })->orWhere(function($p) use($person) {
              $p->whereDoesntHave('collectors')->whereHas('collectors_individual',function($i) use($person) {
                $i->whereIn('person_id',$person);
              });
            });          
        }
        if ($this->project) {
            $project  = $this->project;
            if (!is_array($project)) {
              $project = [$project];
            }
            $query = $query->whereHas('dataset', function ($q) use ($project) {
              $q->whereIn('project_id',  $project); 
            })->orWhereHas('measurements', function ($q) use ($project) {
              $q->whereHas('dataset',function($dt) use ($project) {
                $dt->whereIn('project_id', $project);
              });
            });
        }
        if ($this->biocollection_id) {
            $biocollection_id  = $this->biocollection_id;
            if (!is_array($biocollection_id)) {
              $biocollection_id = [$biocollection_id];
            }
            $query = $query->whereIn('biocollection_id',$biocollection_id);
        }
        if ($this->bibreference_id) {
            $bibreference = $this->bibreference_id;
            $query = $query->whereHas('bibreferences',function($q) use($bibreference) {
                $q->where('bib_references.id','=',$bibreference);
            });
        }


        if ($this->date_min) {
          $date_min = $this->date_min;
          $query = $query->where(function($q) use($date_min) {
            return $q->where('date','>=',$date_min)
            ->orWhereHas('individual',function($i) use($date_min){
              return $i->where('date','>=',$date_min);
            });
          });
        }
        if ($this->date_max) {
          $date_max = $this->date_max;
          $query = $query->where(function($q) use($date_max) {
            return $q->where('date','<=',$date_max)
            ->orWhereHas('individual',function($i) use($date_max) {
              return $i->where('date','<=',$date_max);
            });
          });
        }





        if ($this->request_id or $this->request()->has('request_id')) {
          $request_id = isset($this->request_id) ? $request_id : (int) $this->request()->get('request_id');
          $status = null;
          if ($this->request()->has('status')) {
            $status =  $this->request()->get('status')=="" ? null : (int) $this->request()->get('status');
          }
          if ($status===null) {
            $query = $query->with('odbrequests')->whereHas('odbrequests', function($q) use($request_id) { $q->where('request_id',$request_id);});
          } else {
            $query = $query->with('odbrequests')->whereHas('odbrequests', function($q) use($request_id,$status) { $q->where('request_id',$request_id)->where('status',$status);});
          }
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        $buttons = [
          'pageLength',
          'selectAll',
          'selectNone',
          [
            'extend' => 'colvis',
            'columns' => 'th:nth-child(n+2)',
            'collectionLayout' => 'dropdown two-column',          
          ],                    
          [
            'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
            'className' => 'odb-expand-dt',
            'titleAttr' => Lang::get('datatables.expand'),
          ],
          [
            'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
            'className' => 'odb-wrap-dt',
            'titleAttr' => Lang::get('datatables.wrapnowrap'),
          ],
          [
            'text' => '<i class="fas fa-map-marked-alt fa-lg"></i>',
            'className' => 'odb-map-selected',
            'titleAttr' => Lang::get('messages.map'),
          ],
          [
            'text' => Lang::get('datatables.filter'),
            'className' => 'odb-filter-button',
            'titleAttr' => Lang::get('messages.filter_data'),
          ],
        ];
        $hidcol = [1,2,7,8,12,13];

        if (Auth::user()) {
          $hidcol = [1,2,7,8,12,13];
          $buttons[] = [
            'text' => Lang::get('datatables.export'),
            'className' => 'odb-export-button',];
        }
        if (Gate::allows('create',Voucher::class)) {
          if ($this->individual) { //<!-- we're inside a Location, Project or Taxon view -->
            $url = url ('individuals/' . $this->individual. '/vouchers/create');
          } elseif ($this->biocollection_id && !is_array($this->biocollection_id)) {
            $url = url ('vouchers/' . $this->biocollection_id. '/biocollection/create');
          } else {
            $url = url ('vouchers/create');
          }
          $buttons[] = [
            'text' => Lang::get('messages.create'),
            'className' => 'odb-create-button',
            'action' => "function(e) {
              window.location.href = '".$url."';
            }",
          ];
          $buttons[] = [
            'text' => '<i class="fa-solid fa-trash-can fa-lg"></i>',
            'className' => 'odb-delete-button',
            'titleAttr' => Lang::get('messages.delete_selected'),
          ];
        }

        $columns = [
            'checkbox' =>  [
            'title'     => "Sel",
            'searchable' => false,                   
            'orderable' => false,                  
            ],            
            'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
            'biocollection_number' => ['title' => Lang::get('messages.biocollection_number'), 'searchable' => false, 'orderable' => true],
            'acronym' => ['title' => Lang::get('messages.biocollection'), 'searchable' => false, 'orderable' => false],
            'collector_number' => ['title' => Lang::get('messages.collector_and_number'), 'searchable' => true, 'orderable' => true],
            'date' => ['title' => Lang::get('messages.date'), 'searchable' => false, 'orderable' => true],
            'voucher_identification' => ['title' => Lang::get('messages.identification'), 'searchable' => false, 'orderable' => true],
            'individual' => ['title' => Lang::get('messages.individual'), 'searchable' => false, 'orderable' => false],
            'biocollection_type' => ['title' => Lang::get('messages.voucher_isnomenclatural_type'), 'searchable' => false, 'orderable' => true],
            'location' => ['title' => Lang::get('messages.location'), 'searchable' => false, 'orderable' => false],
            'measurements' => ['title' => Lang::get('messages.measurements'), 'searchable' => false, 'orderable' => false],
            'dataset' => ['title' => Lang::get('messages.dataset'), 'searchable' => false, 'orderable' => false],
            'collectors' => ['title' => Lang::get('messages.collectors'), 'searchable' => false, 'orderable' => false],
            'fullname' => ['title' => 'preservedSpecimenID', 'searchable' => false, 'orderable' => true],
        ];
        $centercols = [10];
        if ($this->request_id or $this->request()->has('request_id')) {
          $title_status  = Lang::get('messages.status').'<select name="status" id="status" ><option value="">'.Lang::get('messages.all').'</option>';
          foreach(ODBRequest::STATUS as $st) {
            $title_status .= '<option value="'.$st.'">'.Lang::get('levels.status.'.$st).'</option>';
          }
          $title_status .="</select>";
          $columns['status'] = ['title' => $title_status, 'searchable' => false, 'orderable' => false];
          $columns['notes'] = ['title' => Lang::get('messages.notes'), 'searchable' => false, 'orderable' => false];
          $hidcol = [1,3,7,9,11,12,13];
          $centercols = [10,14];
        }

        return $this->builder()
            ->columns($columns)
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                'buttons' => $buttons,
                'lengthMenu' => [10,25,50],
                'autoWidth' => false,
                'columnDefs' => [
                  [
                    'targets' => $hidcol,
                    'visible' => false,
                  ],
                  [
                    "targets" => $centercols, 
                    "className" => "text-center",
                  ],
                  [
                    'targets' => 5,
                    'width' => '70px',
                  ],
                  [
                    'orderable' => false,
                    'defaultContent' => '',
                    'className' => 'select-checkbox',
                    'targets' => 0,                    
                    'data' => null    
                  ],
                ],
                'select' => [
                  'style' => 'multi',
                  'selector' => 'td:first-child',
                ],
                'scrollX' => true,
                'searching' => false,
          ]);

    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_vouchers_'.time();
    }
}
