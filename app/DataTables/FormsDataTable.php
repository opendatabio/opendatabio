<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use App\Models\Form;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Lang;
use Gate;

class FormsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->editColumn('name', function ($form) {
            return $form->rawLink();
        })
        ->editColumn('measured_type', function ($form) { return Lang::get('classes.'.$form->measured_type); })
        ->editColumn('user', function ($form) {
            $user = $form->user;
            return $user->identifiableName(); 
        })        
        ->editColumn('traits_count', function ($form) {
            $traits_list = $form->traits_list;
            return $traits_list ? count($form->traits_list) : 0; 
        })
        ->filterColumn('name', function ($query, $keyword) {
            $query->where('name','like', $keyword."%")
            ->orWhereHas('user',function($u) use ($keyword){
                $u->whereHas('person',function($p) use ($keyword){
                    $p->where('full_name','like', $keyword."%");
                })->orWhere('email','like',$keyword."%");
            });
        })
        ->rawColumns(['name']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Form::query();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        $buttons = [
            'reload',
            ['extend' => 'colvis'],
        ];
        if (Gate::allows('create',Form::class)) {
            $url = route('forms.edit');
            $buttons[] = [
              'text' => Lang::get('messages.create'),
              'className' => 'odb-create-button',
              'action' => "function(e) {
                window.location.href = '".$url."';
              }",
            ];
            $buttons[] = [
              'text' => '<i class="fa-solid fa-trash-can fa-lg"></i>',
              'className' => 'odb-delete-button',
                'titleAttr' => Lang::get('messages.delete_selected'),
            ];
        }
        return $this->builder()
            ->columns([
                'checkbox' =>  [
                    'title'     => "Sel",
                    'searchable' => false,                   
                    'orderable' => false,                  
                ],
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
                'name' => ['title' => Lang::get('messages.name'), 'searchable' => true, 'orderable' => true],
                'measured_type' => ['title' => Lang::get('messages.type'), 'searchable' => false, 'orderable' => true],
                'traits_count' => ['title' => Lang::get('messages.traits'), 'searchable' => false, 'orderable' => false],
                'user' => ['title' => Lang::get('messages.user'), 'searchable' => false, 'orderable' => false],
            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                'order' => [[2, 'asc']],
                'buttons' => $buttons,
                'columnDefs' => [
                    [
                        'orderable' => false,
                        'defaultContent' => '',
                        'className' => 'select-checkbox',
                        'targets' => 0,                    
                        'data' => null    
                    ],
                    [
                    'targets' => [1],
                    'visible' => false,
                ]],
                'select' => [
                    'style' => 'multi',
                    'selector' => 'td:first-child',
                ],
                'scrollX' => true,
            ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_forms_'.time();
    }
}
