<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use Baum\Node;
use App\Models\Individual;
use App\Models\Location;
use App\Models\Biocollection;
use App\Models\ODBRequest;
use App\Models\Taxon;
use App\Models\UserJob;
use App\Models\HasAuthLevels;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Arr;
use Lang;
use DB;
use App\Models\User;
use Auth;
use Gate;
use Log;

class IndividualsDataTable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */

    public function dataTable(DataTables $dataTables, $query)
    {
        $table = (new EloquentDataTable($query))
        ->editColumn('fullname', function ($individual) {
            return $individual->rawLink();

        })
        ->orderColumn('fullname', 'odb_ind_fullname(individuals.id,individuals.tag) $1')
        ->editColumn('tag', function ($individual) {
            return '<a href="'.url('individuals/'.$individual->id).'">'.$individual->tag.'</a>';
        })
        ->addColumn('dataset', function ($individual) {
          return '<a href="'.url('dataset-show/'.$individual->dataset_id).'">'.$individual->dataset_name.'</a>';
        })
        ->addColumn('individual_identification', function ($individual) use($query) {
            //$url = isset($individual->identification) ? 'taxons/'.$individual->identification->taxon_id : null;
            //return "<em>".htmlspecialchars($individual->individual_identification)."</em>";
            return $individual->scientificName;
            //
        })
        ->orderColumn('individual_identification', 'odb_identification(individuals.id,0) $1')
        ->addColumn('collectors', function ($individual) {
            $col = $individual->collectors;
            return implode(', ', $col->map(function ($c) {return $c->person->fullname; })->all());
        })
        ->editColumn('date', function ($individual) { return $individual->formatDate; })
        ->addColumn('measurements', function ($individual) {
            if ($this->dataset and !is_array($this->dataset)) {
              return '<a href="'.url('measurements/'.$individual->id.'|'.$this->dataset.'/individual_dataset').'">'.$individual->measurements()->withoutGlobalScopes()->where('dataset_id','=',$this->dataset)->count().'</a>';
            }
            return '<a href="'.url('measurements/'.$individual->id.'/individual').'">'.$individual->measurements()->withoutGlobalScopes()->count().'</a>';
        })
        ->addColumn('location', function ($individual) {
            return $individual->higher_geography;
            //return $individual->LocationDisplay();
        })
        ->addColumn('vouchers',function($individual) {
            return '<a href="'.url('individuals/'.$individual->id.'/vouchers').'">'.$individual->vouchers()->withoutGlobalScopes()->count().'</a>';
        });        
        if ($this->request_id or $this->request()->has('request_id')) {
          $request_id = isset($this->request_id) ? $this->request_id : $this->request()->get('request_id');
          $table = $table->addColumn('status',function($individual) use($request_id) {
            $st = $individual->odbrequests->where('id',$request_id)->first()->pivot->status;
            return Lang::get('levels.status.'.$st);
          })->addColumn('notes',function($individual) use($request_id)
          {
            return $individual->getRequestNotes($request_id);
          });
        }

        
        

        $table = $table->rawColumns(['tag', 'individual_identification','measurements','location','vouchers','dataset','fullname','notes']);
        return $table;

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Individual::query()
        ->with(['current_location.location','collector_main','identification.taxon']);

        // customizes the datatable query
        if ($this->job_id)
        {
          $ids = UserJob::find($this->job_id)->affected_ids_array;
          $query = $query->whereIn('id',$ids);
        }
        if ($this->tag) {
            $tags = explode(",",$this->tag);
            $query = $query->whereIn('tag',$tags);
        }
        if ($this->location) {
            $location_ids = $this->location;
            if (!is_array($location_ids)) {
              $location_ids = [$location_ids];
            }
            $location = Location::withoutGeom()->whereIn('id',$location_ids);
            $locationsids = array_unique(Arr::flatten($location->cursor()->map(function($l) {
              return $l->descendantsAndSelf()->pluck('id')->toArray();
            })->unique()->toArray()));
            //Log::info("HERE".json_encode($locationsids));
            $query = $query->whereHas('locations',function($q) use($locationsids) {
              $q->whereIn('location_id',$locationsids)->orWhereHas('relatedLocations',
                  function($r) use($locationsids) {
                    $r->whereIn('related_id',$locationsids);
                  });
                });            
        }
        if ($this->dataset) {
            $dataset = $this->dataset;
            if (!is_array($this->dataset)) {
              $dataset = [$this->dataset];
            }
            $query = $query->where(function($u) use($dataset) {
              $u->whereIn('dataset_id', $dataset)->orWhereHas('measurements', function ($q) use ($dataset) {$q->whereIn('dataset_id', $dataset); });
            });
        }
        if ($this->taxon) {
          $taxon = $this->taxon;
          if (!is_array($taxon)) {
            $taxon = [$taxon];
          }
          $taxon_list = array_unique(Arr::flatten(Taxon::whereIn('id',$taxon)->get()->map(function($t) {
            return $t->descendantsAndSelf()->pluck('id')->toArray();
          })->unique()->toArray()));
          $query = $query->whereHas('identification', function ($q) use ($taxon_list) {
            $q->whereIn('identifications.taxon_id',$taxon_list); });
        }
        if ($this->person) {
            $person = $this->person;
            if (!is_array($person)) {
              $person = [$person];
            }
            $query = $query->whereHas('collectors', function ($q) use ($person) {
              $q->whereIn('person_id', $person); });
        }
        if ($this->project) {
            $project  = $this->project;
            if (!is_array($project)) {
              $project = [$project];
            }
            $query = $query->whereHas('dataset', 
              function ($q) use ($project) {
                $q->whereIn('project_id', $project); 
              })
              ->orWhereHas('measurements', function ($q) use ($project) {
                $q->whereHas('dataset',function($dt) use ($project) {
                  $dt->whereIn('project_id', $project);
                });
              });
        }
        if ($this->date_min) {
          $query = $query->where('date','>=',$this->date_min);
        }
        if ($this->date_max) {
          $query = $query->where('date','<=',$this->date_max);
        }

        if ($this->request_id or $this->request()->has('request_id')) {
          $request_id = isset($this->request_id) ? $request_id : (int) $this->request()->get('request_id');
          $status = null;
          if ($this->request()->has('status')) {
            $status =  $this->request()->get('status')=="" ? null : (int) $this->request()->get('status');
          }
          if ($status===null) {
            $query = $query->with('odbrequests')->whereHas('odbrequests', function($q) use($request_id) { $q->where('request_id',$request_id);});
          } else {
            $query = $query->with('odbrequests')->whereHas('odbrequests', function($q) use($request_id,$status) { $q->where('request_id',$request_id)->where('status',$status);});
          }
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {

      $buttons = [
        'pageLength',
        'selectAll',
        'selectNone',
        [
          'extend' => 'colvis',
          'columns' => 'th:nth-child(n+2)',
          'collectionLayout' => 'two-column'],
        [
          'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
          'className' => 'odb-expand-dt',
          'titleAttr' => Lang::get('datatables.expand'),
        ],
        [
          'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
          'className' => 'odb-wrap-dt',
          'titleAttr' => Lang::get('datatables.wrapnowrap')
        ],
        [
          'text' => '<i class="fas fa-map-marked-alt fa-lg"></i>',
          'className' => 'odb-map-selected',
          'titleAttr' => Lang::get('messages.map'),
        ],
        [
          'text' => Lang::get('datatables.filter'),
          'className' => 'odb-filter-button',
          'titleAttr' => Lang::get('messages.filter_data'),
        ],
      ];
      $hidcol = [1,2,9,10];
      if (Auth::user()) {
        $hidcol = [1,2,9,10];
        $buttons[] = [
          'text' => Lang::get('datatables.export'),
          'className' => 'odb-export-button',
        ];
      }
      /* needs to be a full user for seen the following buttons */
      if (Gate::allows('create',Individual::class)) {
        if (!is_array($this->location) and $this->location) { //<!-- we're inside a Location, Project or Taxon view -->
          $url = url ('individuals/' . $this->location. '/location/create');
        } else {
          $url = url ('individuals/create');
        }
        if (!$this->request_id and !$this->request()->has('request_id')) {           
            $buttons[] = [
              'text' => Lang::get('messages.request'),
              'className' => 'odb-make-request-bt',
            ];
            $buttons[] = [
              'text' => Lang::get('messages.create'),
              'className' => 'odb-create-button',
              'action' => "function(e) {
                window.location.href = '".$url."';
              }",
            ];
            $url2=url('individuals-identify');
            $buttons[] = [
              'text' => '<i class="fa-solid fa-retweet fa-lg"></i>',
              'className' => 'odb-batch-identify-bt',
              'action' => "function(e) {
                window.location.href ='".$url2."' ;
              }",
              'titleAttr' => Lang::get('messages.individuals_batch_identify'),
            ];
            $buttons[] = [
              'text' => '<i class="fa-solid fa-trash-can fa-lg"></i>',
              'className' => 'odb-delete-button',
              'titleAttr' => Lang::get('messages.delete_selected'),
            ];

            

        }
      }
      $columns = [
        'checkbox' =>  [
          'title'     => "Sel",
          'searchable' => false,                   
          'orderable' => false,                  
          ],
          'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
          'tag' => ['title' => Lang::get('messages.individual_tag'), 'searchable' => true, 'orderable' => true],
          'fullname' => ['title' => Lang::get('messages.individual'), 'searchable' => false, 'orderable' => true],
          'individual_identification' => ['title' => Lang::get('messages.identification'), 'searchable' => false, 'orderable' => true],
          'location' => ['title' => Lang::get('messages.location'), 'searchable' => false, 'orderable' => false],
          'date' => ['title' => Lang::get('messages.date'), 'searchable' => false, 'orderable' => true],
          'measurements' => ['title' => Lang::get('messages.measurements'), 'searchable' => false, 'orderable' => false],
          'vouchers' => ['title' => Lang::get('messages.voucher'), 'searchable' => false, 'orderable' => false],
          'dataset' => ['title' => Lang::get('messages.dataset'), 'searchable' => false, 'orderable' => false],
          'collectors' => ['title' => Lang::get('messages.collectors'), 'searchable' => false, 'orderable' => false]
      ];

      //'collector_number' => ['title' => Lang::get('messages.collector_and_number'), 'searchable' => true, 'orderable' => true],

      $centercols = [6,7];

      if ($this->request_id or $this->request()->has('request_id')) {
        $title_status  = Lang::get('messages.status').'&nbsp;<select name="status" id="status" ><option value="">'.Lang::get('messages.status').' '.Lang::get('messages.all').'</option>';
        foreach(ODBRequest::STATUS_INDIVIDUALS as $st) {
          $title_status .= '<option value="'.$st.'">'.Lang::get('levels.status.'.$st).'</option>';
        }
        $title_status .="</select>";
        $columns['status'] = ['title' => $title_status, 'searchable' => false, 'orderable' => false];
        $columns['notes'] = ['title' => Lang::get('messages.notes'), 'searchable' => false, 'orderable' => false];
        $hidcol = [1,2,9,10];
        $centercols = [6,7,11];
      }

      return $this->builder()
            ->columns($columns)
            ->parameters([
              'dom' => 'Bfrtip',
              'language' => DataTableTranslator::language(),
              'order' => [[0, 'asc']],
              'lengthMenu' => [10,20,50],
              'buttons' => $buttons,
              'pageLength' => 10,
              'autoWidth' => false,
                'columnDefs' => [
                  [
                    'targets' => $hidcol,
                    'visible' => false,
                  ],
                  [
                    'orderable' => false,
                    'defaultContent' => '',
                    'className' => 'select-checkbox',
                    'targets' => 0,                    
                    'data' => null    
                  ],
                  [
                    "targets" => $centercols, 
                    "className" => "text-center",
                  ],
                  [
                    'targets' => 6,
                    'width' => '70px',
                  ],
                ],
                'select' => [
                  'style' => 'multi',
                  'selector' => 'td:first-child',
            ],
            'scrollX' => true,
            'searching' => false,
            ]);


    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_individuals_'.time();
    }
}
