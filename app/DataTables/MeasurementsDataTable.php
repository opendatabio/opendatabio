<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use App\Models\Measurement;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Lang;
use Auth;
use Gate;
use App\Models\Taxon;
use App\Models\Location;
use App\Models\ODBTrait;
use App\Models\UserJob;


class MeasurementsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public $object;
    public $object_id;

    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->editColumn('value', function ($measurement) {
            $text = $measurement->rawLink();
            if ($measurement->type == ODBTrait::COLOR) {
              $text .= '&nbsp;<span class="measurement-thumb" style="background-color:'.$measurement->valueActual.'">';
            }
            return $text;
        })
        ->editColumn('trait_id', function ($measurement) {
            return $measurement->rawLinkWithTraitName();
        })
        ->editColumn('measured_type', function ($measurement) {
            return Lang::get('classes.'.$measurement->measured_type);
        })
        ->editColumn('identification', function ($measurement) {
          return '<em>'.htmlspecialchars($measurement->scientificName).'</em>';
        })
        ->editColumn('measured_id', function ($measurement) {
            return $measurement->measured->rawLink();
        })
        ->editColumn('dataset_id', function ($measurement) {
          return '<a href="'.url('dataset-show/'.$measurement->dataset_id).'">'.$measurement->dataset->name.'</a>';
        })
        ->editColumn('unit', function ($measurement) { return $measurement->measurement_unit; })
        ->editColumn('date', function ($measurement) { return $measurement->formatDate; })
        ->editColumn('persons', function ($measurement) { 
          return $measurement->recorded_by;})
        ->rawColumns(['value', 'trait_id', 'measured_id','dataset_id','identification']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $query = Measurement::query()
            ->select([
                'measurements.id',
                'value',
                'trait_id',
                'value_i',
                'value_a',
                'dataset_id',
                'measured_id',
                'measured_type',
                'date',
                'bibreference_id',
            ]);
        // customizes the datatable query
        if ($this->job_id)
        {
          $ids = UserJob::find($this->job_id)->affected_ids_array;
          $query = $query->whereIn('id',$ids);
        }

        if ($this->measured) {
            $query = $query->where('measured_id', '=', $this->measured)->where('measured_type', $this->measured_type);
        } else {
          if ($this->measured_type) {
            $query = $query->where('measured_type', 'like', "%".$this->measured_type."%");
          }
        }
        if ($this->dataset) {
            $query = $query->where('dataset_id', $this->dataset);
        }
        if ($this->project) {
            //$query = $query->whereHasMorph('measured',['App\Models\Voucher',"App\Models\Individual"],function($measured) {
              //  $measured->where('project_id','=',$this->project);
            //});
            $query = $query->whereHas('dataset',function($dataset) {
                $dataset->where('project_id','=',$this->project);
            });
        }

        if ($this->odbtrait) {
            $query = $query->where('trait_id', $this->odbtrait);
        }
        if ($this->taxon and !isset($this->location)) {
          $taxon_list = Taxon::findOrFail($this->taxon)->getDescendantsAndSelf()->pluck('id')->toArray();
          $query = $query->where(function($subquery) use($taxon_list) {
            $subquery->whereHasMorph('measured',['App\Models\Individual','App\Models\Voucher'],function($measured) use($taxon_list) {
              $measured->whereHas('identification',function($idd) use($taxon_list)  {
                $idd->whereIn('taxon_id',$taxon_list);});})->orWhere(function($qq) use($taxon_list) {
                    $qq->where('measured_type',"=",'App\Models\Taxon')->whereIn('measured_id',$taxon_list);
          });});          
        }
        if ($this->location and !isset($this->taxon)) {
          $locations_ids = Location::findOrFail($this->location)->getDescendantsAndSelf()->pluck('id')->toArray();
          $query = $query->whereIn('measured_id', $locations_ids)->where('measured_type', "App\Models\Location");
        }

        if ($this->location and $this->taxon) {
            $taxon_list = Taxon::findOrFail($this->taxon)->getDescendantsAndSelf()->pluck('id')->toArray();
            $locations_ids = Location::findOrFail($this->location)->getDescendantsAndSelf()->pluck('id')->toArray();
            $query = $query->where(function($q) use ($taxon_list,$locations_ids){
              $q->whereIn('measured_id',$taxon_list)->where('measured_type','App\Models\Taxon')->whereIn('location_id',$locations_ids);
            })->orWhereHasMorph('measured',['App\Models\Individual','App\Models\Voucher'],function($measured) use($taxon_list,$locations_ids) {
                  $measured->whereHas('identification',function($idd) use($taxon_list)  {
                    $idd->whereIn('taxon_id',$taxon_list);})
                    ->whereHas('locations',function($loc) use ($locations_ids) {
                      $loc->whereIn('location_id',$locations_ids);
                  });
        });
        }
        if ($this->bibreference) {
            $query->where('bibreference_id',$this->bibreference);
        }
        return $this->applyScopes($query);

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {

        $buttons = [
          'pageLength',
          'selectAll',
          'selectNone',        
          [
            'extend' => 'colvis',
            'columns' => 'th:nth-child(n+2)',
            'collectionLayout' => 'dropdown two-column',          
          ],      
          [
            'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
            'className' => 'odb-expand-dt',
            'titleAttr' => Lang::get('datatables.expand'),
          ],
          [
            'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
            'className' => 'odb-wrap-dt',
            'titleAttr' => Lang::get('datatables.wrapnowrap'),
          ],
        ];
        $hidcol = [0,1];
        if (Auth::user()) {
          $buttons[] = [
            'text' => Lang::get('datatables.export'),
            'className' => 'odb-export-button',];
        }
        if (Gate::allows('create',Measurement::class)) {
          $hidcol = [1];
          if ($this->measured) {
            $url = route('measurement.form.object',[
              'measured_id' => $this->measured,
              'measured_type' => $this->measured_type,
            ]);
          } else {
            $url = route('measurement.form');
          }
          $buttons[] = [
            'text' => Lang::get('messages.create'),
            'className' => 'odb-create-button',
            'action' => "function(e) {
              window.location.href = '".$url."';
            }",
          ];
          $buttons[] = [
            'text' => '<i class="fa-solid fa-trash-can fa-lg"></i>',
            'className' => 'odb-delete-button',
              'titleAttr' => Lang::get('messages.delete_selected'),
          ];
        }
  


        return $this->builder()
            ->columns([
              'checkbox' =>  [
                'title'     => "Sel",
                'searchable' => false,                   
                'orderable' => false,                  
                ],    
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
                'trait_id' => ['title' => Lang::get('messages.trait'), 'searchable' => true, 'orderable' => false],
                'value' => ['title' => Lang::get('messages.value'), 'searchable' => true, 'orderable' => true],
                'unit' => ['title' => Lang::get('messages.unit'), 'searchable' => false, 'orderable' => false],
                'date' => ['title' => Lang::get('messages.date'), 'searchable' => true, 'orderable' => true],
                'measured_id' => ['title' => Lang::get('messages.measured_object'), 'searchable' => false, 'orderable' => false],
                'measured_type' => ['title' => Lang::get('messages.object_type'), 'searchable' => false, 'orderable' => true],
                'persons' => ['title' => Lang::get('messages.measurement_measurer'), 'searchable' => false, 'orderable' => false],
                'dataset_id' => ['title' => Lang::get('messages.dataset'), 'searchable' => false, 'orderable' => false],
                'identification' => ['title' => Lang::get('messages.taxon'), 'searchable' => false, 'orderable' => false],
            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                'lengthMenu' => [10,25,50,100],
                'buttons' => $buttons,
                'autoWidth' => false,
                'columnDefs' => [
                  [
                    'orderable' => false,
                    'defaultContent' => '',
                    'className' => 'select-checkbox',
                    'targets' => 0,                    
                    'data' => null    
                  ],
                ],
                'select' => [
                  'style' => 'multi',
                  'selector' => 'td:first-child',
                ],
                'scrollX' => true,
            ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_measurements_'.time();
    }
}
