<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use App\Models\IndividualLocation;
use Lang;
use DB;
use Auth;

class IndividualLocationsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->editColumn('name', function($indloc) {
          return $indloc->name;
        })
        ->editColumn('latitude', function($indloc) {
          return round($indloc->latitude,6);
        })
        ->editColumn('longitude', function($indloc) {
          return round($indloc->longitude,6);
        })
        ->addColumn('scientificName', function($indloc) {
          return $indloc->taxonName;
        })
        ->addColumn('individual', function($indloc) {
          return $indloc->individual->rawLink();
        })
        ->addColumn('higherGeography', function($indloc) {
          return $indloc->fullGeography;
        })
        ->editColumn('notes', function($indloc) {
          if ($indloc->notes) {
            return $indloc->dt_formated_notes;
          }
          return null;
        })
        ->addColumn('action',  function ($indloc) {
          $url = url('individual-location/'.$indloc->id);
          $show = '<a href="'.$url.'" class="text-success" title="'.Lang::get('messages.details').'"><i class="far fa-eye"></i></a>';
          if (!isset($indloc->noaction)) {
            return $show.'&nbsp;&nbsp;<button type="button"  data-indloc="'.$indloc->id.'" class="unstyle text-primary far fa-edit editlocation" data-bs-toggle="modal" data-bs-target="#locationModal"></button>&nbsp;<button type="button"  data-indloc="'.$indloc->id.'" class="unstyle text-danger far fa-trash-alt deletelocation"></button>';
          } else {
            return $show;
          }
          return "";
        })
        ->rawColumns(['name','action','individual','notes']);

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = IndividualLocation::query()->join('locations','location_id','=','locations.id')->select(
          [
            'individual_id',
            'individual_location.id',
            'location_id',
            'locations.name',
            'individual_location.notes',
            'individual_location.date_time',
            'individual_location.altitude',
            DB::raw('ST_AsText(individual_location.relative_position) as relativePosition'),
            DB::raw('ST_AsText(individual_location.global_position) as globalPosition'),
            DB::raw('odb_highergeography(individual_location.location_id,null) as fullGeography'),
            DB::raw('odb_identification(individual_location.individual_id,1) as taxonName'),
          ]);
        if ($this->noaction) {
          $query = $query->addSelect(DB::raw('1 as noaction'));
        }
        if ($this->dataset) {
          $dataset = $this->dataset;
          $query = $query->whereHas("individual",function($i) use($dataset) {
            $i->where('dataset_id',$dataset);
          });
        }
        if ($this->individual) {
          $query = $query->where('individual_id',$this->individual);
        }
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
          $buttons = [
            'pageLength',
            'csv',
            'excel',
            'print',
            'selectAll',
            'selectNone',
            [
              'extend' => 'colvis',
              'columns' => 'th:nth-child(n+2)',
              'collectionLayout' => 'dropdown two-column',          
            ],      
            [
              'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
              'className' => 'odb-expand-dt',
              'titleAttr' => Lang::get('datatables.expand'),
            ],
            [
              'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
              'className' => 'odb-wrap-dt',
              'titleAttr' => Lang::get('datatables.wrapnowrap'),
            ],
          ];
          $hidcol = [1,3,5,6,9,10,11];
          if (Auth::user()) {
            $hidcol = [1,8,9,10,11];
            $buttons[] = [
              'text' => Lang::get('datatables.export'),
              'className' => 'odb-export-button',];
            //$buttons[] = [
            //  'text' => Lang::get('datatables.delete'),
            //  'className' => 'odb-delete-button',
            //  'titleAttr' => Lang::get('messages.delete_selected'),
            //];
          }
          if ($this->noaction) {
            $hidcol = [1,3,5,6,9,10,11];
          }

          return $this->builder()
            ->columns([
                'checkbox' =>  [
                  'title'     => "Sel",
                  'searchable' => false,                   
                  'orderable' => false,                  
                ],    
                'action' => ['title' => Lang::get('messages.actions'), 'searchable' => false, 'orderable' => false],
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => false],
                'name' => ['title' => Lang::get('messages.location'), 'searchable' => true, 'orderable' => true],
                'notes' => ['title' => Lang::get('messages.notes'), 'searchable' => true, 'orderable' => true],
                'date_time' => ['title' => Lang::get('messages.date'), 'searchable' => true, 'orderable' => true],
                'altitude' => ['title' => Lang::get('messages.altitude'), 'searchable' => true, 'orderable' => true],
                'relativePosition' => ['title' => Lang::get('messages.relative_position'), 'searchable' => true, 'orderable' => true],
                'latitude' => ['title' => Lang::get('messages.latitude'), 'searchable' => false, 'orderable' => false],
                'longitude' => ['title' => Lang::get('messages.longitude'), 'searchable' => false, 'orderable' => false],
                'individual' => ['title' => Lang::get('messages.individual'), 'searchable' => false, 'orderable' => true],
                'scientificName' => ['title' => Lang::get('messages.taxon'), 'searchable' => false, 'orderable' => true],
                'individual' => ['title' => Lang::get('messages.individual'), 'searchable' => false, 'orderable' => true],
                'higherGeography' => ['title' => Lang::get('messages.higherGeography'), 'searchable' => false, 'orderable' => false],
            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                'lengthMenu' => [10,25,50],
                'buttons' => $buttons,
                'autoWidth' => false,
                'columnDefs' => [
                  [
                    'targets' => $hidcol,
                    'visible' => false,
                  ],
                  [
                    "targets" => [0], 
                    "className" => "text-center",
                  ],
                  [
                    'orderable' => false,
                    'defaultContent' => '',
                    'className' => 'select-checkbox',
                    'targets' => 0,                    
                    'data' => null    
                  ],
                ],
                'select' => [
                  'style' => 'multi',
                  'selector' => 'td:first-child',
                ],
                'scrollX' => true,            ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_individual_locations_'.time();
    }
}
