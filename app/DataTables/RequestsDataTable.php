<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use App\Models\ODBRequest;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Lang;
use Gate;
use DB;

class RequestsDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->addColumn('date', function ($odbrequest) { return $odbrequest->updated_at; })
        ->addColumn('records', function ($odbrequest) {
          $count = $odbrequest->vouchers_count>0 ? $odbrequest->vouchers_count : $odbrequest->individuals_count;
          return '<a href="'.url('odbrequests/'.$odbrequest->id).'">'.$count.'</a>';
        })
        ->addColumn('user', function ($odbrequest) {
          return isset($odbrequest->user) ? (isset($odbrequest->user->person) ? $odbrequest->user->person->rawLink() : $odbrequest->user->email) : null;
        })
        ->addColumn('status', function ($odbrequest) {
            if($odbrequest->status_closed) {
              return '<i class="fas fa-lock" style="color: red;"></i>';
            }
            return '<i class="fas fa-lock-open" style="color: green;"></i>';
        })
        ->rawColumns(['status','records','user'])
        ;
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = ODBRequest::query()->withCount(['vouchers','individuals']);

        if ($this->biocollection_id or $this->request()->has('biocollection_id')) {
            $biocollection_id = isset($this->biocollection_id) ? $this->biocollection_id : (int) $this->request()->get('biocollection_id');
            $query = $query->where(function($w) use($biocollection_id) {
              $w->whereHas('vouchers',function($v)use($biocollection_id) {
              $v->where('biocollection_id','=',$biocollection_id);
            })->orWhereHas('individuals',function($q)use($biocollection_id) {
              $q->where('biocollection_id','=',$biocollection_id);
            });});
        }
        $user_id = null;
        if ($this->user_id or $this->request()->has('user_id')) {
          $user_id = ($this->user_id !=null) ? $this->user_id : (int) $this->request()->get('user_id');
          $query = $query->where('user_id',$user_id);
        }

        if ($this->status or $this->request()->has('status')) {
          $status = isset($this->status) ? (int) $this->status : (int) $this->request()->get('status');
          if ($status==2) {
            $condition = ">=";
          } elseif ($status==1) {
            $condition = "<";
          }
          if ($status>0) {
            $query = $query->where(function($st) use($status,$condition,$user_id){
                $st->whereHas('vouchers',function($v) use($status,$condition) {
                    $v->where('status',$condition,3);})
                  ->orWhereHas('individuals',function($v) use($status,$condition) {
                  $v->where('status',$condition,3);});
                  if ($status==2 and $user_id !==null) {
                    $st->orWhere(function($u) { $u->doesntHave('vouchers')->doesntHave('individuals'); });
                  }
                });
          }
          //if ($status==2) {
          //  $query = $query->orWhere(function($u) { $u->doesntHave('vouchers')->doesntHave('individuals'); });
          //}
        }


        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
      $title_status  = Lang::get('messages.status').'&nbsp;<select name="status" id="status" ><option>'.Lang::get('messages.status').' '.Lang::get('messages.all').'</option>
      <option value="1">'.Lang::get('messages.open').'</option>
      <option value="2">'.Lang::get('messages.closed').'</option></select>';
      $buttons = [
        'pageLength',
        'reload',
        'csv',
        'excel',
        'print',
        ['extend' => 'colvis',  'columns' => ':gt(0)'],
        [
          'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
          'className' => 'odb-expand-dt',
          'titleAttr' => Lang::get('datatables.expand'),
        ],
        [
          'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
          'className' => 'odb-wrap-dt',
          'titleAttr' => Lang::get('datatables.wrapnowrap'),
        ],
      ];

      if (Gate::allows('create',ODBRequest::class)) {
        $url = url ('requests/create');
        $buttons[] = [
          'text' => Lang::get('messages.create'),
          'className' => 'odb-create-button',
          'action' => "function(e) {
            window.location.href = '".$url."';
          }",
        ];
      }
        return $this->builder()
            ->columns([
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
                'date' => ['title' => Lang::get('messages.date'), 'searchable' => true, 'orderable' => true],
                'user' => ['title' => Lang::get('messages.user'), 'searchable' => true, 'orderable' => true],
                'email' => ['title' => Lang::get('messages.email'), 'searchable' => true, 'orderable' => true],
                'status' => ['title' => $title_status, 'searchable' => false, 'orderable' => false],
                'records' => ['title' => Lang::get('messages.records'), 'searchable' => false, 'orderable' => false],
                'type' => ['title' => Lang::get('messages.type'), 'searchable' => false, 'orderable' => true],
                'institution' => ['title' => Lang::get('messages.institution'), 'searchable' => true, 'orderable' => true],
                'message' => ['title' => Lang::get('messages.request_message'), 'searchable' => false, 'orderable' => false],
            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                //'order' => [[0, 'asc']],
                'buttons' => $buttons,
                'columnDefs' => [[
                    'targets' => [0,7],
                    'visible' => false,
                ],
                [
                  "targets" => [4,5], // your case first column
                  "className" => "text-center",
                ],
              ],
                'scrollX' => true,
            ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_request_'.time();
    }
}
