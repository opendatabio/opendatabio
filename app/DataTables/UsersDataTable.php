<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Lang;
use Auth;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->editColumn('email', function ($user) {
            if (Auth::user() and Auth::user()->can('update', $user)) {
                return $user->rawLink();
            }

            return htmlspecialchars($user->email);
        })
        ->editColumn('access_level', function ($user) { return $user->textAccess; })
        ->editColumn('person', function ($user) {
            if ($user->person) {
                return $user->person->rawLink();
            }
        })
        ->rawColumns(['email','person']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = User::query();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {

        $buttons = [
            'pageLength',
            'csv',
            'excel',
            'print',
            'selectAll',
            'selectNone',
            [
              'extend' => 'colvis',
              'columns' => 'th:nth-child(n+2)',
            ],
          [
            'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
            'className' => 'odb-expand-dt',
            'titleAttr' => Lang::get('datatables.expand'),
          ],
          [
            'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
            'className' => 'odb-wrap-dt',
            'titleAttr' => Lang::get('datatables.wrapnowrap'),
          ],
        ];

        return $this->builder()
            ->columns([
                'checkbox' =>  [
                    'title'     => "Sel",
                    'searchable' => false,                   
                    'orderable' => false,                  
                ],    
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
                'email' => ['title' => Lang::get('messages.email'), 'searchable' => true, 'orderable' => true],
                'access_level' => ['title' => Lang::get('messages.access_level'), 'searchable' => false, 'orderable' => true],
                'person'=> ['title' => Lang::get('messages.person'), 'searchable' => false, 'orderable' => false],

            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                'buttons' => $buttons,
                'lengthMenu' => [10,25,50,100,250,500],
                'autoWidth' => false,
                'columnDefs' => [
                  [
                    'orderable' => false,
                    'defaultContent' => '',
                    'className' => 'select-checkbox',
                    'targets' => 0,                    
                    'data' => null    
                  ],
                ],
                'select' => [
                  'style' => 'multi',
                  'selector' => 'td:first-child',
                ],
                'scrollX' => true,
            ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_users_'.time();
    }
}
