<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use App\Models\Tag;
use App\Models\Language;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Lang;
use DB;
use Gate;

class TagsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->addColumn('name', function ($tag) { return $tag->rawLink(); })
        ->orderColumn('name', function ($query, $order) {
          $query->orderBy('name', $order);
        })
        ->filterColumn('name', function ($query, $keyword) {
          $query->whereHas('translations',function($translation) use($keyword) { $translation->where('translation','like','%'.$keyword.'%')->where('translation_type',0);});
        })
        ->addColumn('description', function ($tag) { return $tag->description; })
        ->addColumn('datasets', function ($tag) {
              return '<a href="'.url('datasets/'. $tag->id. '/tags').'" >'.$tag->datasets()->count().'</a>';
        })
        ->addColumn('projects', function ($tag) {
                return '<a href="'.url('projects/'. $tag->id. '/tags').'" >'.$tag->projects()->count().'</a>';
        })
        ->addColumn('media', function ($tag) { return $tag->media()->count(); })
        ->rawColumns(['name','projects','datasets']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $lang_id = Language::where('code',config('app.locale'))->get()->first()->id;
        $query = Tag::query()->select(["tags.*",DB::raw('tr.translation as name')])->join('user_translations as tr','tr.translatable_id','tags.id')->where('tr.translatable_type',"App\Models\Tag")->where('tr.translation_type',0)->where('tr.language_id',$lang_id);
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
      $buttons = [
        'pageLength',
        'csv',
        'excel',
        'print',
        'selectAll',
        'selectNone',
        [
          'extend' => 'colvis',
          'columns' => 'th:nth-child(n+2)',
          'collectionLayout' => 'dropdown two-column',          
        ],      
        [
          'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
          'className' => 'odb-expand-dt',
          'titleAttr' => Lang::get('datatables.expand'),
        ],
        [
          'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
          'className' => 'odb-wrap-dt',
          'titleAttr' => Lang::get('datatables.wrapnowrap'),
        ],
      ];
      if (Gate::allows('create',Tag::class)) {
        $url = url ('tags/create');
        $buttons[] = [
          'text' => Lang::get('messages.create'),
          'className' => 'odb-create-button',
          'action' => "function(e) {
            window.location.href = '".$url."';
          }",
        ];
      }
        return $this->builder()
            ->columns([
              'checkbox' =>  [
                'title'     => "Sel",
                'searchable' => false,                   
                'orderable' => false,                  
                ],  
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
                'name' => ['title' => Lang::get('messages.name'), 'searchable' => true, 'orderable' => true],
                'description' => ['title' => Lang::get('messages.description'), 'searchable' => false, 'orderable' => false],
                'datasets' => ['title' => Lang::get('messages.datasets'), 'searchable' => false, 'orderable' => false],
                'projects' => ['title' => Lang::get('messages.projects'), 'searchable' => false, 'orderable' => false],
                'media' => ['title' => Lang::get('messages.media_files'), 'searchable' => false, 'orderable' => false],
            ])
            ->parameters([
              'dom' => 'Bfrtip',
              'language' => DataTableTranslator::language(),
              'lengthMenu' => [10,25,50,100],
              'buttons' => $buttons,
              'autoWidth' => false,
                'columnDefs' => [[
                    'targets' => [1],
                    'visible' => false,
                ],[
                  'orderable' => false,
                  'defaultContent' => '',
                  'className' => 'select-checkbox',
                  'targets' => 0,                    
                  'data' => null    
                ],
              ],
              'select' => [
                'style' => 'multi',
                'selector' => 'td:first-child',
              ],                
                'scrollX' => true,
            ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_references_'.time();
    }
}
