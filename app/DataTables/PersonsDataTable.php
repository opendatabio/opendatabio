<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use App\Models\Person;
use App\Models\UserJob;

use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Lang;
use Gate;
use Auth;
class PersonsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->editColumn('abbreviation', function ($person) {
            return $person->rawLink();
        })
        ->addColumn('biocollection', function ($person) {
            return empty($person->biocollection) ? '' : $person->biocollection->name;
        })      
        ->editColumn('email',  function ($person) {
            $valid = (null != Auth::user()) ? Auth::user()->access_level>=1 : false;
            if ($person->email_public or $valid) {
              return $person->email;
            }
        })
        ->editColumn('notes', function($person) {
          if ($person->notes) {
            return $person->dt_formated_notes;
          }
          return null;
        })
        ->rawColumns(['abbreviation','notes']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Person::query()->select(['id', 'full_name', 'abbreviation', 'email','email_public', 'institution', 'biocollection_id','notes'])->with('biocollection');
        if ($this->job_id)
        {
          $ids = UserJob::find($this->job_id)->affected_ids_array;
          $query = $query->whereIn('id',$ids);
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */

    public function html()
    {
        $buttons = [
          'pageLength',
          'csv',
          'selectAll',
          'selectNone',
          [
          'extend' => 'colvis',
          'columns' => 'th:nth-child(n+2)',
          ],
          [
            'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
            'className' => 'odb-expand-dt',
            'titleAttr' => Lang::get('datatables.expand'),
          ],
          [
            'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
            'className' => 'odb-wrap-dt',
            'titleAttr' => Lang::get('datatables.wrapnowrap'),
          ],
        ];
        $hidcol = [0,1];
        if (Auth::user()) {
          $buttons[] = [
            'text' => Lang::get('datatables.export'),
            'className' => 'odb-export-button',];
        }

        if (Gate::allows('create',Person::class)) {
          $url = url ('persons/create');
          $buttons[] = [
            'text' => Lang::get('messages.create'),
            'className' => 'odb-create-button',
            'action' => "function(e) {
              window.location.href = '".$url."';
            }",
          ];
          $buttons[] = [
            'text' => '<i class="fa-solid fa-trash-can fa-lg"></i>',
            'className' => 'odb-delete-button',
              'titleAttr' => Lang::get('messages.delete_selected'),
          ];
          $hidcol = [1];
        }
        return $this->builder()
            ->columns([
                'checkbox' =>  [
                  'title'     => "Sel",
                  'searchable' => false,                   
                  'orderable' => false,                  
                ],    
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
                'abbreviation' => ['title' => Lang::get('messages.abbreviation'), 'searchable' => true, 'orderable' => true],
                'full_name' => ['title' => Lang::get('messages.full_name'), 'searchable' => true, 'orderable' => true],
                'email' => ['title' => Lang::get('messages.email'), 'searchable' => true, 'orderable' => true],
                'institution' => ['title' => Lang::get('messages.institution'), 'searchable' => true, 'orderable' => true],
                'biocollection' => ['title' => Lang::get('messages.biocollection'), 'searchable' => false, 'orderable' => false],
                'notes' => ['title' => Lang::get('messages.notes'), 'searchable' => false, 'orderable' => false],
            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                'lengthMenu' => [10,25,50,100],
                'buttons' => $buttons,
                'autoWidth' => false,
                'columnDefs' => [[
                    'targets' => $hidcol,
                    'visible' => false,
                ],
                [
                  'orderable' => false,
                  'defaultContent' => '',
                  'className' => 'select-checkbox',
                  'targets' => 0,                    
                  'data' => null    
                ],
              ],
              'select' => [
                'style' => 'multi',
                'selector' => 'td:first-child',
              ],
              'scrollX' => true,
            ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_persons_'.time();
    }
}
