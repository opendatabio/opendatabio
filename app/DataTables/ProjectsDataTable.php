<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use App\Models\Project;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Lang;
use Gate;
use Auth;
use Log;
class ProjectsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->editColumn('acronym', function ($project) {
          $title = $project->acronymLink;
          if ($project->logoUrl) {            
            $fileUrl = $project->logoUrl;
            $title = '<div class="text-nowrap d-flex align-items-center">
            <img src="'.url($fileUrl).'" style="max-height: 120px;" class="p-3">
            <br>'.$title."</div>";
          }
          return $title;
        })
        ->addColumn('title', function ($project) {
          $title = isset($project->name) ? $project->name  : $project->acronym;          
          return $title;
        })
        ->addColumn('description', function ($project) {
          $title = $project->description;
          return $title;
        })
        ->addColumn('datasets', function ($project) {
          $datasets_count = $project->datasets()->count();
          return '<a href="'.url('datasets/'. $project->id. '/project').'" >'.$datasets_count.'</a>';
        })
        ->filterColumn('acronym', function ($query, $keyword) {
            $query->orWhereHas('tags',function($tag) use($keyword) {
                $tag->whereHas('translations',function($trn) use ($keyword) { 
                  $trn->where('translation','like','%'.$keyword.'%');
                });
            })->orWhereHas('translations',function($tr) use($keyword) {
              $tr->where('translation','like','%'.$keyword.'%');
            });
        })        
        ->rawColumns(['acronym','datasets','title']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Project::query()->with('users')->withCount(['datasets','media']);
        if ($this->tag) {
            $tagid = $this->tag;
            $query = $query->whereHas('tags',function($tag) use($tagid) { $tag->where('tags.id',$tagid);});
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {

        $buttons = [
          'pageLength',
          'csv',
          'excel',
          'print',
          'selectAll',
          'selectNone',
          [
            'extend' => 'colvis',
            'columns' => 'th:nth-child(n+2)',            
            'collectionLayout' => 'two-column'
          ],  
          [
            'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
            'className' => 'odb-expand-dt',
            'titleAttr' => Lang::get('datatables.expand'),
          ],
          [
            'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
            'className' => 'odb-wrap-dt',
            'titleAttr' => Lang::get('datatables.wrapnowrap'),
          ],
        ];
        if (Gate::allows('create',Project::class)) {
          $url = url ('projects/create');
          $buttons[] = [
            'text' => Lang::get('messages.create'),
            'className' => 'odb-create-button',
            'action' => "function(e) {
              window.location.href = '".$url."';
            }",
          ];
        }


        return $this->builder()
            ->columns([
                'checkbox' =>  [
                'title'     => "Sel",
                'searchable' => false,                   
                'orderable' => false,                  
                ],  
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
                'acronym' => ['title' => Lang::get('messages.name'), 'searchable' => true, 'orderable' => true],
                'title' => ['title' => Lang::get('messages.title'), 'searchable' => false, 'orderable' => false],
                'description' => ['title' => Lang::get('messages.description'), 'searchable' => false, 'orderable' => false],
                'datasets' => ['title' => Lang::get('messages.datasets'), 'searchable' => false, 'orderable' => false],
            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                'buttons' => $buttons,
                'lengthMenu' => [10,25,50,100],
                'autoWidth' => false,
                'columnDefs' => [[
                    'targets' => [1,5],
                    'visible' => false,
                ],
                [
                  'orderable' => false,
                  'defaultContent' => '',
                  'className' => 'select-checkbox',
                  'targets' => 0,                    
                  'data' => null    
                ],
              ],
              'select' => [
                'style' => 'multi',
                'selector' => 'td:first-child',
              ],                
                'scrollX' => true,
            ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_projects_'.time();
    }
}
