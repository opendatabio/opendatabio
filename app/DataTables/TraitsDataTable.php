<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use App\Models\ODBTrait;
use App\Models\UserTranslation;
use App\Models\Language;
use App\Models\UserJob;

use Lang;
use DB;
use Auth;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use Gate;

class TraitsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */

     /* not work for
     protected $exportColumns = [
      ['data' => 'id', 'title' => 'id'],
      ['data' => 'name', 'title' => 'name'],
      ['data' => 'export_name', 'title' => 'export_name'],
      ['data' => 'type', 'title' => 'typename'],
      ['data' => 'details', 'title' => 'details'],
      ['data' => 'measurements', 'title' => 'measurementsCount'],
    ];
    */

    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->editColumn('export_name', function ($odbtrait) {
            return $odbtrait->rawLink();
        })
        ->editColumn('name', function ($odbtrait) {
          return $odbtrait->name;
        })
        ->orderColumn('name', function ($query, $order) {
          $query->orderBy('name', $order);
        })
        ->filterColumn('name', function ($query, $keyword) {
          $query->where('export_name', "LIKE", "{$keyword}%")
          ->orWhereHas('translations',function($translation) use($keyword) { 
            $translation->where('translation','like','%'.$keyword.'%')->where('translation_type',0);
          });
        })
        ->editColumn('type', function ($odbtrait) { return Lang::get('levels.traittype.'.$odbtrait->type); })
        ->addColumn('details', function ($odbtrait) {return $odbtrait->details(); })
        ->addColumn('measurements', function ($odbtrait) {
          if ($this->dataset) {
            return '<a href="'.url('measurements/'.$odbtrait->id.'|'.$this->dataset.'/individual_dataset').'">'.$odbtrait->measurements()->withoutGlobalScopes()->where('dataset_id','=',$this->dataset)->count().'</a>';
          }
          return '<a href="'.url('measurements/'.$odbtrait->id.'/trait').'">'.$odbtrait->measurements()->withoutGlobalScopes()->count().'</a>';
        })    
        ->rawColumns(['export_name','measurements']);

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
      $lang_id = Language::where('code',config('app.locale'))->get()->first()->id;
      $query = ODBTrait::query()->withCount("measurements");
      //->select(["traits.*",DB::raw('tr.translation as name')])->leftJoin('user_translations as tr','tr.translatable_id','traits.id')->where('tr.translatable_type',"App\Models\ODBTrait")->where('tr.translation_type',0)->where('tr.language_id',$lang_id);
      //->orderBy('export_name', 'asc');
      if ($this->job_id)
      {
        $ids = UserJob::find($this->job_id)->affected_ids_array;
        $query = $query->whereIn('id',$ids);
      }

      if ($this->request()->has('type')) {
        $type =  (int) $this->request()->get('type');
        if ($type>0) {
          $query = $query->where('type',$type);
        }
      }
      if ($this->unit_id) {
        $query = $query->where('trait_unit_id',$this->unit_id);        
      }

      return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {

        $trait_types =  DB::table('traits')->selectRaw('DISTINCT type')->cursor()->pluck('type')->toArray();
        $title_type = Lang::get('messages.type');
        if (count($trait_types)) {
          $title_type  .= '&nbsp;&nbsp;<select name="type" id="trait_type" ><option value="">'.Lang::get('messages.all').'</option>';
          foreach ($trait_types as $type) {
                 $title_type  .= '<option value="'.$type.'" >'.Lang::get('levels.traittype.' . $type).'</option>';
          }
          $title_type  .= '</select>';
        }

        $buttons = [
          'pageLength',
          'print',
          'selectAll',
          'selectNone',
          [
            'extend' => 'colvis',
            'columns' => 'th:nth-child(n+2)',
            'collectionLayout' => 'two-column'],
          [
            'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
            'className' => 'odb-expand-dt',
            'titleAttr' => Lang::get('datatables.expand'),
          ],
          [
            'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
            'className' => 'odb-wrap-dt',
            'titleAttr' => Lang::get('datatables.wrapnowrap'),
          ],
        ];
        $hidcol = [0,1];

        if (Auth::user()) {
          $hidcol = [1];
          $buttons[] = [
            'text' => Lang::get('datatables.export'),
            'className' => 'odb-export-button',];
        }

      if (Gate::allows('create',ODBTrait::class)) {
        $url = route('trait.form');
        $buttons[] = [
          'text' => Lang::get('messages.create'),
          'className' => 'odb-create-button',
          'action' => "function(e) {
            window.location.href = '".$url."';
          }",
        ];
        $buttons[] = [
          'text' => '<i class="fa-solid fa-trash-can fa-lg"></i>',
          'className' => 'odb-delete-button',
            'titleAttr' => Lang::get('messages.delete_selected'),
        ];
      }

   

        return $this->builder()
            ->columns([
                'checkbox' =>  [
                'title'     => "Sel",
                'searchable' => false,                   
                'orderable' => false,                  
                ],    
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
                'export_name' => ['title' => Lang::get('messages.export_name'), 'searchable' => false, 'orderable' => true],
                'name' => ['title' => Lang::get('messages.name'), 'searchable' => true, 'orderable' => true],                
                'type' => ['title' => $title_type, 'searchable' => false, 'orderable' => true],
                'details' => ['title' => Lang::get('messages.details'), 'searchable' => false, 'orderable' => false],
                'measurements' => ['title' => Lang::get('messages.measurements'), 'searchable' => false, 'orderable' => false],
            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),                
                'buttons' =>   $buttons,
                'lengthMenu' => [10,25,50,100,250,500],
                'autoWidth' => false,
                'columnDefs' => [
                  [
                    'targets' => $hidcol,
                    'visible' => false,
                  ],
                  [
                    'orderable' => false,
                    'defaultContent' => '',
                    'className' => 'select-checkbox',
                    'targets' => 0,                    
                    'data' => null    
                  ],
                ],
                'select' => [
                  'style' => 'multi',
                  'selector' => 'td:first-child',
                ],
                'scrollX' => true,

            ]);
    }




    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'odb_odbtraits_'.time();
    }
}
