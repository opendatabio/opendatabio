<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\DataTables;

use Illuminate\Support\Collection;
use App\Models\Location;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\DataTables;
use DB;
use App\Models\Dataset;
use App\Models\Project;
use App\Models\UserJob;
use Lang;
use Log;
use Auth;
use Gate;

class LocationsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return (new EloquentDataTable($query))
        ->editColumn('name', function ($location) {
            return $location->rawLink();
        })
        ->filterColumn('name', function ($query, $keyword) {
            $sql = " name LIKE '%".$keyword."%' ";
            $query->whereRaw($sql);
        })
        ->editColumn('adm_level', function ($location) {
          return $location->country_adm_level;
          //return Lang::get('levels.adm_level.'.$location->adm_level);
        })
        ->editColumn('country_code', function ($location) {
          return $location->country;
        })
        ->editColumn('higher_geography', function ($location) {
          return $location->higher_geography;
        })
        ->editColumn('geom_type', function ($location) {
          return ucfirst($location->geom_type);
        })
        /*        
        ->addColumn('individuals', function ($location) {
          if ($this->project) {
            $individuals_count = $location->getCount('App\Models\Project',$this->project,'individuals');
            return '<a href="'.url('individuals/'.$location->id.'|'.$this->project.'/location_project').'">'.$individuals_count.'</a>';
          } else {
            if ($this->dataset) {
              $individuals_count = $location->getCount('App\Models\Dataset',$this->dataset,'individuals');
              return '<a href="'.url('individuals/'.$location->id.'|'.$this->dataset.'/location_dataset').'">'.$individuals_count.'</a>';
            } else {
              $individuals_count = $location->getCount('all',null,"individuals");
              return '<a href="'.url('individuals/'.$location->id.'/location').'">'.$individuals_count.'</a>';
            }
          }
        })        
        ->addColumn('vouchers', function ($location) {
          if ($this->project) {
            $voucher_count = $location->getCount('App\Models\Project',$this->project,'vouchers');
            return '<a href="'.url('vouchers/'.$location->id.'|'.$this->project.'/location_project').'">'.$voucher_count.'</a>';
          } else {
            if ($this->dataset) {
              $voucher_count = $location->getCount('App\Models\Dataset',$this->dataset,'vouchers');
              return '<a href="'.url('vouchers/'.$location->id.'|'.$this->dataset.'/location_dataset').'">'.$voucher_count.'</a>';
            } else {
              $voucher_count = $location->getCount('all',null,"vouchers");
              return '<a href="'.url('vouchers/'.$location->id.'/location').'">'.$voucher_count.'</a>';
            }
          }
        })
        ->addColumn('measurements', function ($location) {
          if ($this->project) {
            $measurements_count = $location->getCount('App\Models\Project',$this->project,"measurements");
            return '<a href="'.url('measurements/'.$location->id.'|'.$this->project.'/location_project').'">'.$measurements_count.'</a>';
          } else {
            if ($this->dataset) {
              $measurements_count = $location->getCount('App\Models\Dataset',$this->dataset,'measurements');
              return '<a href="'.url('measurements/'.$location->id.'|'.$this->dataset.'/location_dataset').'">'.$measurements_count.'</a>';
            } else {
              $measurements_count = $location->getCount('all',null,"measurements");
              return '<a href="'.url('measurements/'.$location->id.'/location').'">'.$measurements_count.'</a>';
            }
          }
        })
        ->addColumn('taxons', function ($location) {
          if ($this->project) {
            $taxons_count = $location->taxonsCount('App\Models\Project',$this->project);
            return '<a href="'.url('taxons/'.$location->id.'|'.$this->project.'/location_project').'">'.$taxons_count.'</a>';
          } else {
            if ($this->dataset) {
              $taxons_count = $location->taxonsCount('App\Models\Dataset',$this->dataset);
              return '<a href="'.url('taxons/'.$location->id.'|'.$this->dataset.'/location_dataset').'">'.$taxons_count.'</a>';
            } else {
              $taxons_count = $location->taxonsCount('all',null);
              return '<a href="'.url('taxons/'.$location->id.'/location').'">'.$taxons_count.'</a>';
            }
          }
        })
        ->addColumn('media', function ($location) {
          $mediaCount = $location->mediaDescendantsAndSelf()->count();
          $urlShowAllMedia = "media/".$location->id."/locations";
          return '<a href="'.url($urlShowAllMedia).'">'.$mediaCount.'</a>';
        })
        */
        //->addColumn('latitude', function ($location) {return $location->latitudeSimple; })
        //->addColumn('longitude', function ($location) {return $location->longitudeSimple; })
        ->addColumn('parent', function ($location) {
            if (null != $location->parent_id) {
              if ($location->parent->adm_level==(-1)) {
                return '';
              }
              return $location->parent->rawLink();
              /*
              if ($this->project) {
                $url = url('locations/'.$location->parent_id.'|'.$this->project.'/location_project');
              } else {
                if ($this->dataset) {
                  $url = url('locations/'.$location->parent_id.'|'.$this->dataset.'/location_dataset');
                } else {
                  $url = url('locations/'.$location->parent_id);
                }
              }
              return '<a href="'.$url.'">'.$location->parent->name.'</a>';
              */
            }
        })        
        ->addColumn('dimensions',  function ($location) {
            if ($location->x and $location->y) {
              if ($location->startx and $location->starty) {
                  return $location->x." x ".$location->y." [".Lang::get('messages.start').": X: ".$location->startx." Y: ".$location->starty."]";
              }
              return $location->x." x ".$location->y;
            }
            return null;
        })
        ->rawColumns(['name', 'media', 'higher_geography', 'measurements','taxons','parent','checkbox']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Location::query()->with('parent')->withoutGeom()->noWorld();
        if ($this->job_id)
        {
          $ids = UserJob::find($this->job_id)->affected_ids_array;
          $query = $query->whereIn('id',$ids);
        }
        if ($this->project) {
          $locations_ids = Project::findOrFail($this->project)->all_locations_ids();
          $query->whereIn('id',$locations_ids);
        }
        if ($this->dataset) {
          $locations_ids = Dataset::findOrFail($this->dataset)->all_locations_ids();
          $query->whereIn('id',$locations_ids);
        }
        if ($this->location) {
            $location = Location::select(['id','lft','rgt','adm_level'])->findOrFail($this->location);
            $query = $query->where(function($w) use($location) {
              $w->where('locations.lft','>',$location->lft)->where('locations.rgt','<',$location->rgt);
            });
            if (in_array($location->adm_level,[Location::LEVEL_UC, Location::LEVEL_TI,Location::LEVEL_ENV])) {
              $related_children = DB::table('location_related')->select('location_id')->where('related_id',$location->id)->cursor()->pluck('location_id')->toArray();
              $query = $query->orWhereIn('id',$related_children);
            }
        }
        if ($this->request()->has('adm_level')) {
          $adm_level =  (int) $this->request()->get('adm_level');
          if ($adm_level>0) {
            $query = $query->where('adm_level',$adm_level);
          }
        }

        //$query = $query->orderBy('adm_level')->orderBy('name');
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        $locations_level =  Location::used_adm_levels($this->location);
        $title_level = Lang::get('messages.level');
        if (count($locations_level)) {
          $title_level  = Lang::get('messages.level').'&nbsp;<select class="form-control-sm" name="location_level" id="location_level" ><option value="">'.Lang::get('messages.all_admin_level').'</option>';
          foreach ($locations_level as $level) {
                 if ($level>0) {
                   $title_level  .= '<option value="'.$level.'" >'.Lang::get('levels.adm_level.' . $level).'</option>';
                 }
          }
          $title_level  .= '</select>';
        }


        $buttons = [
          'pageLength',        
          'print',
          'selectAll',
          'selectNone',
          [
            'extend' => 'colvis',
            'columns' => 'th:nth-child(n+2)',
            'collectionLayout' => 'two-column'],
          [
            'text' => '<i class="fas fa-expand-arrows-alt" id="expand_button"></i>',
            'className' => 'odb-expand-dt',
            'titleAttr' => Lang::get('datatables.expand'),
          ],
          [
            'text' => '<i class="fas fa-compress-alt" id="wrap_button"></i>',
            'className' => 'odb-wrap-dt',
            'titleAttr' => Lang::get('datatables.wrapnowrap'),
          ],
          [
            'text' => '<i class="fas fa-map-marked-alt fa-lg"></i>',
            'className' => 'odb-map-selected',
            'titleAttr' => Lang::get('messages.map'),
          ],
        ];
        $hidcol = [1,3,6];//,10];

        if (Auth::user()) {
          $hidcol = [1,3,6];//,10];
          $buttons[] = [
            'text' => Lang::get('datatables.export'),
            'className' => 'odb-export-button',];
        }
        if (Gate::allows('create',Location::class)) {
          $buttons[] = [
            'text' => Lang::get('messages.create'),
            'className' => 'odb-create-button',
            'action' => "function(e) {
              window.location.href = '".url('locations/create')."';
            }",
          ];
          $buttons[] = [
            'text' => '<i class="fa-solid fa-trash-can fa-lg"></i>',
            'className' => 'odb-delete-button',
            'titleAttr' => Lang::get('messages.delete_selected'),
          ];
        }




        return $this->builder()
            ->columns([
                'checkbox' =>  [
                  'title'     => "Sel", 
                  'searchable' => false,         
                  'orderable' => false,                  
                ],
                'id' => ['title' => Lang::get('messages.id'), 'searchable' => false, 'orderable' => true],
                'name' => ['title' => Lang::get('messages.name'), 'searchable' => true, 'orderable' => true],
                'adm_level' => ['title' => $title_level, 'searchable' => false, 'orderable' => true],
                'country_code' => ['title' => Lang::get('messages.country'), 'searchable' => false, 'orderable' => false],
                'parent' => ['title' => Lang::get('messages.parent'), 'searchable' => false, 'orderable' => false],
                'higher_geography' => ['title' => Lang::get('messages.higherGeography'), 'searchable' => false, 'orderable' => false],
                'geom_type' => ['title' => Lang::get('messages.geomtype'), 'searchable' => false, 'orderable' => false],
                //'individuals' => ['title' => Lang::get('messages.individuals'), 'searchable' => false, 'orderable' => false],
                //'vouchers' => ['title' => Lang::get('messages.vouchers'), 'searchable' => false, 'orderable' => false],
                //'measurements' => ['title' => Lang::get('messages.measurements'), 'searchable' => false, 'orderable' => false],
                //'taxons' => ['title' => Lang::get('messages.taxons'), 'searchable' => false, 'orderable' => false],
                //'media' => ['title' => Lang::get('messages.media_files'), 'searchable' => false, 'orderable' => false],
                //'latitude' => ['title' => Lang::get('messages.latitude'), 'searchable' => false, 'orderable' => false],
                //'longitude' => ['title' => Lang::get('messages.longitude'), 'searchable' => false, 'orderable' => false],
                //'altitude' => ['title' => Lang::get('messages.altitude'), 'searchable' => false, 'orderable' => false],
                'dimensions' => ['title' => Lang::get('messages.dimensions'), 'searchable' => false, 'orderable' => false],
            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'language' => DataTableTranslator::language(),
                'order' => [[0, 'asc']],
                'lengthMenu' => [3,5,10,15,20,50,100],
                'buttons' => $buttons,
                'pageLength' => 10,
                'autoWidth' => false,
                'columnDefs' => [
                  [
                    'targets' => $hidcol,
                    'visible' => false,
                  ],
                  /*
                  [
                  "width" => "20%",
                  "targets" => [2]
                  ],
                  [
                    "targets" => [5,6,7,8,9], // your case first column
                    "className" => "text-center",
                  ],
                  */
                  [
                    'orderable' => false,
                    'defaultContent' => '',
                    'className' => 'select-checkbox',
                    'targets' => 0,                    
                    'data' => null    
                  ],
              ],
              'select' => [
                    'style' => 'multi',
                    'selector' => 'td:first-child',
              ],
              'scrollX' => true,
            ]);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() : string {
        return 'odb_locations_'.time();      
    }

   
    
}
