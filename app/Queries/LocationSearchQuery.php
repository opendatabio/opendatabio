<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */
/*
  * This file prepares a builder query to be used by the API or by the ExportData job 
*/

namespace App\Queries;

use Illuminate\Http\Request;
use App\Models\Location;
use App\Models\Project;
use App\Models\Dataset;
use App\Models\ODBFunctions;
use App\Models\UserJob;

use DB;
use Log;

class LocationSearchQuery
{

    public static function prepQuery($request)
    {
        $fields = ($request->fields ? $request->fields : 'simple');
        $possible_fields = config('api-fields.locations');
        $field_sets = array_keys($possible_fields);
        if (in_array($fields,$field_sets)) {
          $trimmed_array = array_map('trim',$possible_fields[$fields]);  
          $fields = implode(",",$trimmed_array);
        } else {
          $trimmed_array = array_map('trim',explode(",",$fields));  
          $fields = implode(",",$trimmed_array);
        }   
        $require_geom = ['footprintWKT','locationRemarks','decimalLatitude','decimalLongitude','georeferenceRemarks'];
        $fields_all = explode(',',$fields);
        $has_geom=array_intersect($require_geom,$fields_all);        
        if ($fields=="id") {
          $locations = Location::select('id')->where('adm_level','<>','-1');
        } elseif(count($has_geom)) {
          $locations = Location::withGeom()->where('adm_level','<>','-1');
        } else {
          $locations = Location::noWorld();
        }


        if ($request->job_id)
        {
          $ids = UserJob::find($request->job_id)->affected_ids_array;
          $locations = $locations->whereIn('id',$ids);
        }


        if ($request->root or $request->location_root) {
            $location_query = $request->root ? $request->root : $request->location_root;
            $locations_ids = ODBFunctions::asIdList($location_query, Location::select('id'), 'name');
            $locs = Location::whereIn('id',$locations_ids);
            $locations_ids = array_unique(Arr::flatten($locs->cursor()->map(
                function($l) { 
                  return $l->descendantsAndSelf()->pluck('id')->toArray();
                })->unique()->values()->toArray()));
            $locations->whereIn('locations.id',$locations_ids);
        }
        if ($request->id) {
            $locations->whereIn('locations.id', explode(',', $request->id));
        }
        if ($request->parent_id) {
            $locations->whereIn('parent_id', explode(',', $request->parent_id));
        }
        if ($request->search) {
            $where = " name like '%".$request->search."%' ";
            $locations->whereRaw($where);
        }
        if ($request->name) {
            $locations_ids = ODBFunctions::asIdList($request->name, Location::select('id'), 'name');
            $locations->whereIn('locations.id',$locations_ids);
        }

        if ($request->adm_level) {
            $levels = array_filter(explode(',', $request->adm_level));
            if (count($levels)) {
              $locations->whereIn('adm_level', $levels);
            }
        }
        // For lat / long searches
        if ($request->querytype and isset($request->lat) and isset($request->long)) {
            $geom = "POINT($request->long $request->lat)";
            if ('exact' == $request->querytype) {
                $locations->whereRaw('ST_AsText(geom) = ?', [$geom]);
            }
            if ('parent' == $request->querytype) {
                $parent = Location::detectParent($geom, 100, false,false,0);
                if ($parent) {
                    $locations->where('id', $parent->id);
                } else {
                    // no results should be shown
                    $locations->whereRaw('1 = 0');
                }
            }
            if ('closest' == $request->querytype) {
                $locations = $locations->withDistance($geom)->orderBy('distance', 'ASC');
            }
        }

        if ($request->project) {
          $project_ids = ODBFunctions::asIdList($request->dataset,Project::select('id'),'name',false);
          $all_locations_ids = Project::whereIn('id',$project_ids)->cursor()->map(function($d) {
            return $d->all_locations_ids();
          })->toArray();
          if (count($all_locations_ids)) {
            $locations = $locations->whereIn('id',$all_locations_ids);
          } else {
            $request->limit=0;
            $request->offset=0;
          }
        }

        if ($request->dataset) {
          $dataset_ids = ODBFunctions::asIdList($request->dataset,Dataset::select('id'),'name',false);
          $all_locations_ids = Dataset::whereIn('id',$dataset_ids)->cursor()->map(function($d) {
            return $d->all_locations_ids();
          })->toArray();
          if (count($all_locations_ids)) {
            $locations = $locations->whereIn('id',$all_locations_ids);
          } else {
            $request->limit=0;
            $request->offset=0;
          }
        }

        if ($request->limit and $request->offset) {
            $locations->offset($request->offset)->limit($request->limit);
        } elseif ($request->limit) {
            $locations->limit($request->limit);
        }


        // NOTE that "distance" as a field is only defined for querytype='closest', but it is ignored for other queries
        
        return [
            'query' => $locations,
            'fields' => $fields,
          ];
      }
  }
  