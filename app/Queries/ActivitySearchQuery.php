<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */
/*
  * This file prepares a builder query to be used by the API or by the ExportData job 
*/

namespace App\Queries;

use App\Models\Individual;
use Activity;
use App\Models\ActivityFunctions;
use App\Models\ODBFunctions;
use App\Models\Language;
use Illuminate\Http\Request;
use DB;
use Lang;
use Log;

class ActivitySearchQuery
{

    public static function prepQuery($request)
    {
        $activities = Activity::query();
        if ($request->id) {
            $activities->whereIn('id', explode(',', $request->id));
        }
        if ($request->subject_id and $request->subject) {
            $activities->whereRaw('subject_id IN('.$request->subject_id.') AND subject_type LIKE "%'.$request->subject.'%"');
        } elseif ($request->subject) {
            $activities->where('subject_type','like', '%'.$request->subject.'%');
        }
        if ($request->log_name) {
            $activities->where('log_name','like', '%'.$request->log_name.'%');
        }
        if ($request->description) {
            $activities->where('description','like', '%'.$request->description.'%');
        }

        if ($request->limit and $request->offset) {
            $activities->offset($request->offset)->limit($request->limit);
        } else {
          if ($request->limit) {
            $activities->limit($request->limit);
          }
        }

        $fields = ($request->fields ? $request->fields : 'simple');
        $possible_fields = config('api-fields.activities');
        $field_sets = array_keys($possible_fields);
        if (in_array($fields,$field_sets)) {
            $fields = implode(",",$possible_fields[$fields]);
        }

        $lang = "en";
        if ($request->language) {
           $lang = ODBFunctions::validRegistry(Language::select('id','code'),$request->language, ['id', 'code','name'])->code;           
        } 

        return [
            'query' => $activities,
            'fields' => $fields,
            'lang' => $lang,
        ];
    }

}