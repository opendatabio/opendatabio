<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */
/*
  * This file prepares a builder query to be used by the API or by the ExportData job 
*/

namespace App\Queries;

use Illuminate\Http\Request;
use App\Models\Taxon;
use App\Models\Location;
use App\Models\ODBFunctions;
use Illuminate\Support\Arr;
use App\Models\UserJob;

use Response;
use Log;

class TaxonSearchQuery
{

    public static function prepQuery($request)
    {
        $taxons = Taxon::query()
        ->with(['author_person', 'reference','parent.parent']);
        if ($request->root or $request->taxon_root) {
            $txroot = $request->root ? $request->root : $request->taxon_root;
            $taxon_ids = ODBFunctions::asIdList($txroot,Taxon::select('id'),'odb_txname(name, level, parent_id,0,0)',true);
            if (count($taxon_ids)) {
                $all_taxons = Taxon::whereIn('id',$taxon_ids);
                $taxon_ids = array_unique(Arr::flatten($all_taxons->cursor()->map(
                    function($taxon) { 
                        return $taxon->descendantsAndSelf()->pluck('id')->toArray();
                    })->unique()->values()->toArray()));
                $taxons->whereIn('id',$taxon_ids);
            }
        }

        if ($request->job_id)
        {
          $ids = UserJob::find($request->job_id)->affected_ids_array;
          $taxons->whereIn('id',$ids);
        }

        if ($request->id) {
            $taxons->whereIn('id', explode(',', $request->id));
        }
        if ($request->name) {
            $taxon_ids = ODBFunctions::asIdList($request->name,Taxon::select('id'),'odb_txname(name, level, parent_id,0,0)',true);
            //$taxon_ids = Taxon::whereRaw("odb_txname(name, level, parent_id,0,0) LIKE '".$request->name."'")->pluck('id')->toArray();
            if (count($taxon_ids)) {
                $taxons->whereIn('id',$taxon_ids);        
            }
        }
        if (isset($request->level)) {
            $levels = explode(",",$request->level);
            $taxons->whereIn('level',$levels);
        }
        if (isset($request->valid)) {
            $taxons->valid();
        }
        if ($request->external) {
            $taxons->with('externalrefs');
        }

        if ($request->project) {
          $project_ids = ODBFunctions::asIdList($request->project,Project::select('id'),'name',false);
          $all_taxons_ids = Project::whereIn('id',$project_ids)->cursor()
          ->map(function($d) {
            return $d->all_taxons_ids();
          })->toArray();
          if (count($all_taxons_ids)) {
            $taxons = $taxons->whereIn('id',$all_taxons_ids);
          } 
        }

        if ($request->dataset) {
          $dataset_ids = ODBFunctions::asIdList($request->dataset,Dataset::select('id'),'name',false);
          $all_taxons_ids = Dataset::whereIn('id',$dataset_ids)->cursor()->map(function($d) {
            return $d->all_taxons_ids();
          })->toArray();
          if (count($all_taxons_ids)) {
            $taxons = $taxons->whereIn('id',$all_taxons_ids);
          } 
        }

        if ($request->location_root) {
            $taxon_ids = Location::find($request->location_root)->taxonsIDS();
            $taxons = $taxons->whereIn('id',$taxon_ids);
        }


        if (isset($request->limit) && isset($request->offset)) {
            $taxons->offset($request->offset)->limit($request->limit);
        } elseif (isset($request->limit)) {
            $taxons->limit($request->limit);          
        }

        $fields = ($request->fields ? $request->fields : 'simple');
        $possible_fields = config('api-fields.taxons');
        $field_sets = array_keys($possible_fields);
        if (in_array($fields,$field_sets)) {
            $trimmed_array = array_map('trim',$possible_fields[$fields]);  
            $fields = implode(",",$trimmed_array);
          } else {
            $trimmed_array = array_map('trim',explode(",",$fields));  
            $fields = implode(",",$trimmed_array);
          }   
        return [
            'query' => $taxons,
            'fields' => $fields,
          ];
      }
  }
  
