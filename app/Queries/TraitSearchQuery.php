<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */
/*
  * This file prepares a builder query to be used by the API or by the ExportData job 
*/

namespace App\Queries;

use Illuminate\Http\Request;
use App\Models\ODBTrait;
use App\Models\ODBFunctions;
use App\Models\Language;

use App\Models\UserJob;

use DB;
use Lang;
use Log;

class TraitSearchQuery
{

    public static function prepQuery($request)
    {
        $odb_traits = ODBTrait::query()->with(['bibreference','categories']);
        $odbtraits_ids =[];
        if ($request->job_id)
        {
            $odbtraits_ids = UserJob::find($request->job_id)->affected_ids_array;
        }

        if ($request->trait) {            
            $odbtraits_ids = ODBFunctions::asIdList($request->trait, ODBTrait::select('id'), 'export_name');
        }
        if ($request->name) {
            $odbtraits = ODBFunctions::asIdList($request->name, ODBTrait::select('id'), 'export_name');
            if (count($odbtraits_ids)) {
                $odbtraits_ids = array_unique(array_merge($odbtraits_ids,$odbtraits));
            } else {
                $odbtraits_ids = $odbtraits;
            }
        }    
        if ($request->id) {            
            $odbtraits = explode(',', $request->id);
            if (count($odbtraits_ids)) {
                $odbtraits_ids = array_unique(array_merge($odbtraits_ids,$odbtraits));
            } else {
                $odbtraits_ids = $odbtraits;
            }
        }
        if (count($odbtraits_ids)) {
            $odb_traits->whereIn('id', $odbtraits_ids);
        }        
        if ($request->type) {
            $odb_traits->whereIn('type', explode(',', $request->type));
        }

        if ($request->limit && $request->offset) {
            $odb_traits->offset($request->offset)->limit($request->limit);
        } else {
          if ($request->limit) {
            $odb_traits->limit($request->limit);
          }
        }
        //if language is specified, 
        //need to mutate name and description for current language
        $lang = 1;
        if ($request->language) {
           $has_lang = ODBFunctions::validRegistry(Language::select('id'),$request->language, ['id', 'code','name']);           
            if ($has_lang) {
                $lang = $has_lang->id;
            }
        } 

        //add categories for categorical traits
        //there is probably a more elegant way;
        $fields = ($request->fields ? $request->fields : 'simple');
        $possible_fields = config('api-fields.odbtraits');
        $field_sets = array_keys($possible_fields);
        if (in_array($fields,$field_sets)) {
            $trimmed_array = array_map('trim',$possible_fields[$fields]);  
            $fields = implode(",",$trimmed_array);
          } else {
            $trimmed_array = array_map('trim',explode(",",$fields));  
            $fields = implode(",",$trimmed_array);
          }   
        if ($request->bibreference) {
            $fields .= ",bibreference";
        }
        return [
            'query' => $odb_traits,
            'fields' => $fields,
            'lang' => $lang,
        ];
    }
  
  
  
    public static function prepCategories($odb_traits,$fields,$lang)
    {
        $final = $odb_traits->map(function($obj) use($lang,$fields) {
            $result = [];
            $field_arr = explode(",",$fields);
            foreach ($field_arr as $field) {
                if ($fields!="categories") {
                    $result[$field] = isset($obj[$field]) ? $obj[$field] : null;
                    if ($field=="name") {
                        $result[$field] = $obj->translate(0,$lang); 
                    }
                    if ($field=="description") {
                        $result[$field] = $obj->translate(1,$lang); 
                    }
                }
            }
            $has_category = in_array('categories',$field_arr);
            $cats = [];
            if ($obj->categories->count() && $has_category) {
                $tren = $obj->export_name;
                $cats = $obj->categories->map(function($cat) use($lang,$tren) {
                    return [
                        'id' => $cat->id,
                        'name' => $cat->translate(0,$lang), 
                        'description' => $cat->translate(1,$lang), 
                        'rank' => $cat->rank,
                        'belongs_to_trait' => $tren,
                    ];
                })->toArray();
                $result['categories'] = $cats;
            } elseif ($has_category) {
                $result['categories'] = null;
            }
            return $result;        
        }); 
        return $final;
    }

}