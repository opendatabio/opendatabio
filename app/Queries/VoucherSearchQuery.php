<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */
/*
  * This file prepares a builder query to be used by the API or by the ExportData job 
*/

namespace App\Queries;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Voucher;
use App\Models\Individual;
use App\Models\Location;
use App\Models\Taxon;
use App\Models\Dataset;
use App\Models\Identification;
use App\Models\Project;
use App\Models\Person;
use App\Models\ODBFunctions;
use App\Models\UserJob;


class VoucherSearchQuery
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function prepQuery($request)
    {
        $vouchers = Voucher::query()->select('vouchers.*')->with(['collectors.person', 'collector_main.person', 'individual', 'individual.collectors', 'identification.taxon.parent.parent', 'identification.taxon.author_person', 'identification.person', 'identification.biocollection', 'biocollection', 'dataset', 'current_location',]);

        if ($request->job_id)
        {
            $ids = UserJob::find($request->job_id)->affected_ids_array;
            $vouchers->whereIn('id',$ids);
        }

        if ($request->id) {
            $vouchers->whereIn('id', explode(',', $request->id));
        }

        if ($request->number) {
            //cleans request and build query
            //multiple number may be provided separated by comma
            $numbers = explode(',',$request->number);
            $vouchers = $vouchers->where(function($q) use($numbers) {
            $q->whereIn('number',$numbers);
            $q->orWhereHas('individual',function($i) use($numbers) {
                $i->whereIn('tag',$numbers);
            });
            });
            //ODBFunctions::advancedWhereIn($vouchers, 'number', $request->number);
        }
        //location may have two options, root means all descentants
        if ($request->location or $request->location_root) {
            $location_query= $request->location ? $request->location : $request->location_root;            
            $locations_ids = ODBFunctions::asIdList($location_query, Location::select('id'), 'name');
            if ($request->location_root) {
            $locations = Location::whereIn('id',$locations_ids);
            $locations_ids = array_unique(Arr::flatten($locations->cursor()->map(
                function($location) { 
                return $location->descendantsAndSelf()->pluck('id')->toArray();
                })->unique()->values()->toArray()));
            }
            $vouchers->whereHas('locations',function($q) use($locations_ids) {
                $q->whereIn('location_id',$locations_ids);
            });
        }

        //individual can be queried by ids or fullname
        if ($request->individual) {
            $individual_ids = ODBFunctions::asIdList(
                $request->individual,
                Individual::select('id'),
                'odb_ind_fullname(id,tag)');
            $vouchers->whereIn('individual_id',$individual_ids);
        }


        if ($request->collector or $request->person or $request->main_collector) {
            $persons = isset($request->collector) ? $request->collector : ($request->person ? $request->person  : $request->main_collector);
            $persons_ids = ODBFunctions::asIdList($persons, Person::select('id'), 'full_name');
            $relation = $request->main_collector ? 'collector_main' : 'collectors';
            if (count($persons_ids)) {
                $vouchers->where(function($q) use($persons_ids) {
                $q->whereHas($relation,function($col) use($persons_ids)
                    {
                    $col->whereIn('person_id',$persons_ids);
                    })
                    ->orWhereHas('individual',function($ind) use($persons_ids)
                    {
                    $ind->whereHas($relation,function($col) use($persons_ids)
                    {
                        $col->whereIn('person_id',$persons_ids);
                    });
                });
                });
            }
        }

        if ($request->project) {
            $projects = ODBFunctions::asIdList($request->project, Project::select('id'), 'name');
            $vouchers->whereHas('dataset', function($d) use($projects) {
            $d->whereIn('project_id',$projects);
            });
        }

        if ($request->biocollection_id or $request->biocollection) {
            $bioc = $request->biocollection_id ? $request->biocollection_id : $request->biocollection;
            $biocollections = ODBFunctions::asIdList($bioc, Biocollection::select('id'), 'name');
            $vouchers->whereIn('biocollection_id',$biocollections);            
        }

        if ($request->taxon or $request->taxon_root) {
            $taxon_query= $request->taxon ? $request->taxon : $request->taxon_root;
            $taxon_ids = ODBFunctions::asIdList($taxon_query,Taxon::select('id'),'odb_txname(name, level, parent_id,0,0)',true);
            if ($request->taxon_root) {
              $taxons = Taxon::whereIn('id',$taxon_ids);
              $taxon_ids = array_unique(Arr::flatten($taxons->cursor()->map(
                function($taxon) { 
                  return $taxon->descendantsAndSelf()->pluck('id')->toArray();
                })->unique()->values()->toArray()));
            }
            //the individual identification is directly linked to their vouchers
            $vouchers->whereHas('identification', function ($q) use ($taxon_ids) {
                $q->whereIn('taxon_id',$taxon_ids);
            });
        }

        if ($request->dataset) {
            $datasets = ODBFunctions::asIdList($request->dataset, Dataset::select('id'), 'name');
            $vouchers->whereIn('dataset_id',$datasets)
            ->orWhere(function($q) use($datasets){
                $q->whereHas('measurements',function($m)use($datasets){
                $m->whereIn('dataset_id',$datasets);
                })->orWhereHas('media',function($media) use($datasets) {
                $m->whereIn('dataset_id',$datasets);
                });
            });
        }
        if ($request->odbrequest_id) {
            $request_id = $request->odbrequest_id;
            $vouchers->whereHas('odbrequests',function($rq) use($request_id) {
                $rq->where('requests.id',$request_id);
            });
        }

        if ($request->date_min) {
            $date_min = $request->date_min;
            $query = $query->where(function($q) use($date_min) {
              return $q->where('date','>=',$date_min)
              ->orWhereHas('individual',function($i) use($date_min){
                return $i->where('date','>=',$date_min);
              });
            });
          }
          if ($request->date_max) {
            $date_max = $request->date_max;
            $query = $query->where(function($q) use($date_max) {
              return $q->where('date','<=',$date_max)
              ->orWhereHas('individual',function($i) use($date_max) {
                return $i->where('date','<=',$date_max);
              });
            });
          }

        if ($request->limit && $request->offset) {
            $vouchers->offset($request->offset)->limit($request->limit);
        } else {
        if ($request->limit) {
            $vouchers->limit($request->limit);
        }
        }
        $fields = ($request->fields ? $request->fields : 'simple');
        $possible_fields = config('api-fields.vouchers');
        $field_sets = array_keys($possible_fields);
        if (in_array($fields,$field_sets)) {
            $trimmed_array = array_map('trim',$possible_fields[$fields]);  
            $fields = implode(",",$trimmed_array);
          } else {
            $trimmed_array = array_map('trim',explode(",",$fields));  
            $fields = implode(",",$trimmed_array);
          }   
    return [
        'query' => $vouchers,
        'fields' => $fields,
    ];

    }
}