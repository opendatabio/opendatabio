<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */
/*
  * This file prepares a builder query to be used by the API or by the ExportData job 
*/

namespace App\Queries;

use Illuminate\Support\Arr;
use App\Models\Individual;
use App\Models\IndividualLocation;
use App\Models\ODBFunctions;
use App\Models\Dataset;
use App\Models\Taxon;
use App\Models\Location;
use DB;
use Log;

class IndividualLocationSearchQuery
{

    public static function prepQuery($request)
    {
        $indlocations = IndividualLocation::query()->withXy()->with(['individual','individual.dataset','location_with_geom','location_with_geom.parent_without_geom','identification.taxon','identification.person','identification.taxon.parent.parent', 'individual.collectors.person','individual.collector_main.person','identification.taxon.author_person','identification.biocollection']);
        if ($request->id) {
          $indlocations = $indlocations->whereIn('individual_location.id', explode(',', $request->id));
        }
        if ($request->individual) {
            $indlocations->whereIn('individual_id', explode(',', $request->individual));
        }
        if ($request->taxon or $request->taxon_root) {
            if ($request->taxon) {
              $taxon_query= $request->taxon;
            } else {
              $taxon_query =  $request->taxon_root;
            }
            $taxon_ids = ODBFunctions::asIdList(
                    $taxon_query,
                    Taxon::select('id'),
                    'odb_txname(name, level, parent_id,0,0)',true);
            if ($request->taxon_root) {
              $taxons = Taxon::whereIn('id',$taxon_ids);
              $taxon_ids = array_unique(Arr::flatten($taxons->cursor()->map(function($taxon) { return $taxon->descendantsAndSelf()->pluck('id')->toArray();})->toArray()));
            }
            //the individual identification is directly linked to their vouchers
            $indlocations = $indlocations->whereHas('identification', function ($q) use ($taxon_ids) {
              $q->whereIn('taxon_id',$taxon_ids);
            });
        }
        if ($request->location or $request->location_root) {
            if ($request->location) {
              $location_query= $request->location;
            } else {
              $location_query =  $request->location_root;
            }
            $locations_ids = ODBFunctions::asIdList($location_query, Location::select('id'), 'name');
            if ($request->location_root) {
              $query_locations = Location::whereIn('id',$locations_ids);
              $locations_ids = array_unique(Arr::flatten($query_locations->cursor()->map(function($location) { return $location->descendantsAndSelf()->pluck('id')->toArray();})->toArray()));
            }
            $indlocations = $indlocations->whereIn('location_id',$locations_ids);
        }

        if ($request->dataset) {
            $datasets = ODBFunctions::asIdList($request->dataset, Dataset::select('id'), 'name');
            $indlocations = $indlocations->whereHas('individual', function($d) use($datasets){
                $d->whereIn('individuals.dataset_id',$datasets);
              }
            );
        }

        if ($request->date_min) {
          $indlocations = $indlocations->where(function($i) use($request) {
            $i->where('date_time','>=',$request->date_min)->orWhereHas('individual',function($parent) {
              $parent->where('date','>=',$request->date_min);
            });
          }) ;
        }
        if ($request->date_max) {
          $indlocations = $indlocations->where(function($i) use($request) {
            $i->where('date_time','<=',$request->date_max)->orWhereHas('individual',function($parent) {
              $parent->where('date','<=',$request->date_max);
            });
          }) ;
        }

        if ($request->limit && $request->offset) {
          $indlocations = $indlocations->offset($request->offset)->limit($request->limit);
        } else {
          if ($request->limit) {
            $indlocations = $indlocations->limit($request->limit);
          }
        }
        $fields = ($request->fields ? $request->fields : 'simple');
        $possible_fields = config('api-fields.individuallocations');
        $field_sets = array_keys($possible_fields);
        if (in_array($fields,$field_sets)) {
          $trimmed_array = array_map('trim',$possible_fields[$fields]);  
          $fields = implode(",",$trimmed_array);
        } else {
          $trimmed_array = array_map('trim',explode(",",$fields));  
          $fields = implode(",",$trimmed_array);
        }      
        return [
            'query' => $indlocations,
            'fields' => $fields,
          ];
    }
}