<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */
/*
  * This file prepares a builder query to be used by the API or by the ExportData job 
*/

namespace App\Queries;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\BibReference;
use App\Models\Taxon;
use App\Models\ODBFunctions;
use App\Models\UserJob;
use DB;
use Log;

class BibReferenceSearchQuery 
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public static function prepQuery($request)
    {
        $bibrefences = BibReference::select('*', DB::raw('odb_bibkey(bibtex) as bibkey'));

        if ($request->job_id)
        {
          $ids = UserJob::find($request->job_id)->affected_ids_array;
          $bibrefences->whereIn('id',$ids);
        }

        if ($request->id) {
            $bibrefences->whereIn('id', explode(',', $request->id));
        }
        if ($request->bibkey) {
            $keys = explode(',',$request->bibkey);
            $keys = implode("','",$keys);
            $bibrefences->whereRaw(" odb_bibkey(bibtex) IN('".$keys."')");
        }
        if ($request->taxon) {
            $taxons_ids = ODBFunctions::asIdList(
                  $request->taxon,
                  Taxon::select('id'),
                  'odb_txname(name, level, parent_id,0,0)',
                  true);
            $taxons = Taxon::whereIn('id',$taxons_ids);
            $bib_ids = Arr::flatten($taxons->cursor()
            ->map(function($taxon) { 
                return $taxon->references()->pluck('id')->toArray();})->toArray());
            $bibrefences->whereIn('id',$bib_ids);
        }
        if ($request->dataset) {
          $dataset_ids = ODBFunctions::asIdList(
            $request->dataset,
            Dataset::select('id'),
            'name',
            true);
            $bibrefences->whereHas('datasets',function($b) use($dataset_ids) {
              $b->whereIn('datasets.id',$dataset_ids);
            });
        }
        if ($request->biocollection) {
          $biocollection_ids = ODBFunctions::asIdList(
            $request->datbiocollectionaset,
            Biocollection::select('id'),
            'acronym',
            true);
            $bibids = array_unique(Arr::flatten(Biocollection::whereIn('id',$biocollection_ids)->cursor()->map(
              function($b) {
                return $b->bibreferences_ids();
              })->toArray()));
            if (count($bibids)) {
              $bibrefences->whereIn('id',$bibids);
            }
        }
        if ($request->limit && $request->offset) {
            $bibrefences->offset($request->offset)->limit($request->limit);
        } else {
          if ($request->limit) {
            $bibrefences->limit($request->limit);
          }
        }
        $fields = ($request->fields ? $request->fields : 'simple');
        $possible_fields = config('api-fields.bibreferences');
        $field_sets = array_keys($possible_fields);
        if (in_array($fields,$field_sets)) {
            $trimmed_array = array_map('trim',$possible_fields[$fields]);  
            $fields = implode(",",$trimmed_array);
        } else {
          $trimmed_array = array_map('trim',explode(",",$fields));  
          $fields = implode(",",$trimmed_array);
        } 
        return [
          'query' => $bibrefences,
          'fields' => $fields,
        ];
    }

  }
