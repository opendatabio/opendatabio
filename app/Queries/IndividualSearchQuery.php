<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */
/*
  * This file prepares a builder query to be used by the API or by the ExportData job 
*/

namespace App\Queries;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Individual;
use App\Models\Location;
use App\Models\Taxon;
use App\Models\Identification;
use App\Models\Project;
use App\Models\Dataset;
use App\Models\Person;
use App\Models\ODBFunctions;
use App\Models\UserJob;

use DB;
use Log;

class IndividualSearchQuery
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function prepQuery($request)
    {
        $fields = ($request->fields ? $request->fields : 'simple');
        $possible_fields = config('api-fields.individuals');
        $field_sets = array_keys($possible_fields);
        if (in_array($fields,$field_sets)) {
          $trimmed_array = array_map('trim',$possible_fields[$fields]);  
          $fields = implode(",",$trimmed_array);
        } else {
          $trimmed_array = array_map('trim',explode(",",$fields));  
          $fields = implode(",",$trimmed_array);
        }   
        if($request->with_locations) {
          $fields = $fields.",locations";
        }
        if($request->with_collectors) {
          $fields = $fields.",collectors";
        }
        
        if ($fields=="id") {
          $individuals = Individual::select('id');  
        } else {
          $individuals = Individual::query()->select('individuals.*')->with(['current_location.location',
        'collectors.person', 'collector_main.person', 'identification.taxon.parent.parent',
        'identification.taxon.author_person', 'identification.person', 'identification.biocollection', 'dataset']);
        }

        if ($request->job_id)
        {
          $ids = UserJob::find($request->job_id)->affected_ids_array;
          $individuals = $individuals->whereIn('id',$ids);
        }



        if ($request->id) {
            $individuals = $individuals->whereIn('individuals.id', explode(',', $request->id));
        }
        if ($request->location or $request->location_root) {
            $location_query= $request->location ? $request->location : $request->location_root;            
            $locations_ids = ODBFunctions::asIdList($location_query, Location::select('id'), 'name');
            if ($request->location_root) {
              $locations = Location::whereIn('id',$locations_ids);
              $locations_ids = array_unique(Arr::flatten($locations->cursor()->map(
                function($location) { 
                  return $location->descendantsAndSelf()->pluck('id')->toArray();
                })->unique()->values()->toArray()));
            }
            $individuals = $individuals->whereHas('locations',function($q) use($locations_ids) {
                $q->whereIn('locations.id',$locations_ids);
            });
        }
        if ($request->tag) {
            ODBFunctions::advancedWhereIn($individuals, 'tag', $request->tag);
        }

        if ($request->taxon or $request->taxon_root) {
            $taxon_query= $request->taxon ? $request->taxon : $request->taxon_root;
            $taxon_ids = ODBFunctions::asIdList($taxon_query,Taxon::select('id'),'odb_txname(name, level, parent_id,0,0)',true);
            if ($request->taxon_root) {
              $taxons = Taxon::whereIn('id',$taxon_ids);
              $taxon_ids = array_unique(Arr::flatten($taxons->cursor()->map(
                function($taxon) { 
                  return $taxon->descendantsAndSelf()->pluck('id')->toArray();
                })->unique()->values()->toArray()));
            }
            //the individual identification is directly linked to their vouchers
            $individuals = $individuals->whereHas('identification', function ($q) use ($taxon_ids) {
              $q->whereIn('taxon_id',$taxon_ids);
            });
        }

        if ($request->project) {
            $projects = ODBFunctions::asIdList($request->project, Project::select('id'), 'name');
            $individuals->whereHas('dataset', function($d) use($projects) {
              $d->whereIn('project_id',$projects);
            });
        }

        if ($request->dataset) {
            $datasets = ODBFunctions::asIdList($request->dataset, Dataset::select('id'), 'name');
            $individuals = $individuals->whereIn('dataset_id',$datasets)
            ->orWhere(function($q) use($datasets){
                $q->whereHas('measurements',function($m)use($datasets){
                  $m->whereIn('dataset_id',$datasets);
                })->orWhereHas('media',function($media) use($datasets) {
                  $media->whereIn('dataset_id',$datasets);
                });
            });
        }
        if($request->with_locations) {
          $individuals = $individuals->with('locations');
        }
        if($request->with_collectors) {
          $individuals = $individuals->with('collectors');
        }
        if ($request->odbrequest_id) {
          $request_id = $request->odbrequest_id;
          $individuals= $individuals->whereHas('odbrequests',function($rq) use($request_id) {
            $rq->where('requests.id',$request_id);
          });
        }
        if ($request->person) {
          $persons = ODBFunctions::asIdList($request->person, Person::select('id'), 'name');
          $individuals = $individuals->whereHas('collectors', function ($q) use ($persons) {
            $q->whereIn('person_id', $persons); });
        }

        if ($request->date_min) {
          $individuals = $individuals->where('date','>=',$request->date_min);
        }
        if ($request->date_max) {
          $individuals = $individuals->where('date','<=',$request->date_max);
        }
        if ($request->limit && $request->offset) {
            $individuals = $individuals->offset($request->offset)->limit($request->limit);
        } else {
          if ($request->limit) {
            $individuals = $individuals->limit($request->limit);
          }
        }
                  
        return [
          'query' => $individuals,
          'fields' => $fields,
        ];
    }
}
