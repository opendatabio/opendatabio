<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */
/*
  * This file prepares a builder query to be used by the API or by the ExportData job 
*/

namespace App\Queries;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Measurement;
use App\Models\Location;
use App\Models\Taxon;
use App\Models\ODBFunctions;
use App\Models\ODBTrait;
use App\Models\UserJob;

use Log;

class MeasurementSearchQuery
{

    public static function prepQuery($request)
    {
        $measurements = Measurement::select('measurements.*','measurements.date as valueDate');

        if ($request->job_id)
        {
          $ids = UserJob::find($request->job_id)->affected_ids_array;
          $measurements = $measurements->whereIn('id',$ids);
        }


        if ($request->id) {
            $measurements = $measurements->whereIn('measurements.id', explode(',', $request->id));
        }
        if ($request->trait) {
            $odbtraits = ODBFunctions::asIdList($request->trait, ODBTrait::select('id'), 'export_name');
            $measurements = $measurements->whereIn('trait_id', $odbtraits);
        }
        if ($request->dataset) {
          $datasetids = ODBFunctions::asIdList($request->dataset, ODBTrait::select('id'), 'name');
          $measurements = $measurements->whereIn('dataset_id',$datasetids);
        }
        if ($request->measured_type) {
          $measurements = $measurements->where('measured_type', 'like', "%".$request->measured_type."%");
          if ($request->measured_id) {
            $ids = explode(",",$request->measured_id);
            $measurements = $measurements->whereIn('measured_id', $ids);
          }          
        }
        if ($request->taxon or $request->taxon_root) {
            $taxon_query= $request->taxon ? $request->taxon : $request->taxon_root;
            $taxon_ids = ODBFunctions::asIdList($taxon_query,Taxon::select('id'),'odb_txname(name, level, parent_id,0,0)',true);   
            if ($request->taxon_root) {
              $taxons = Taxon::whereIn('id',$taxon_ids);
              $taxon_ids = array_unique(Arr::flatten($taxons->cursor()->map(
                function($taxon) { 
                  return $taxon->descendantsAndSelf()->pluck('id')->toArray();
                })->unique()->values()->toArray()));
            }        
            $measurements = $measurements->where(function($subquery) use($taxon_ids) {
              $subquery->whereHasMorph('measured',['App\Models\Individual','App\Models\Voucher'],function($mm) use($taxon_ids) { $mm->whereHas('identification',function($idd) use($taxon_ids)  { $idd->whereIn('taxon_id',$taxon_ids);});})->orWhereRaw('measured_type = "App\Models\Taxon" AND measured_id IN ('.implode(",",$taxon_ids).')');
            });

        }
        if ($request->location or $request->location_root) {
            if ($request->location) {
                $location_query= $request->location;
              } else {
                $location_query =  $request->location_root;
              }
              $location_ids = ODBFunctions::asIdList(
                      $location_query,
                      Location::select('id'),
                      'name');
              //asked for descendants
            if ($request->location_root) {
                $locations = Location::whereIn('locations.id',$location_ids);
                $locations_ids = Arr::flatten($locations->cursor()->map(function($lc) { return $lc->descendantsAndSelf()->pluck('locations.id')->toArray();})->toArray());
            }   
            $measurements = $measurements->where('measured_type', 'App\Models\Location')->whereIn('measured_id', $locations_ids);
        }
        if ($request->individual) {            
            $measurements = $measurements->where('measured_type', 'App\Models\Individual')->whereIn('measured_id', explode(',', $request->individual));
        }
        if ($request->voucher) {
            $measurements = $measurements->where('measured_type', 'App\Models\Voucher')->whereIn('measured_id', explode(',', $request->voucher));
        }
        if ($request->person) {
          $persons = ODBFunctions::asIdList($request->person, Person::select('id'), 'name');
          $measurements = $measurements->whereHas('collectors', function ($q) use ($persons) {
            $q->whereIn('persons.id', $persons); });
        }
        if ($request->date_min) {
          $measurements = $measurements->where('date','>=',$request->date_min);
        }
        if ($request->date_max) {
          $measurements = $measurements->where('date','<=',$request->date_max);
        }

        if ($request->limit && $request->offset) {
            $measurements = $measurements->offset($request->offset)->limit($request->limit);
        } else {
          if ($request->limit) {
            $measurements = $measurements->limit($request->limit);
          }
        }
        $fields = ($request->fields ? $request->fields : 'simple');
        $possible_fields = config('api-fields.measurements');
        $field_sets = array_keys($possible_fields);
        if (in_array($fields,$field_sets)) {
            $trimmed_array = array_map('trim',$possible_fields[$fields]);  
            $fields = implode(",",$trimmed_array);
        } else {
          $trimmed_array = array_map('trim',explode(",",$fields));  
          $fields = implode(",",$trimmed_array);
        }    
        return [
            'query' => $measurements,
            'fields' => $fields,
          ];
      }
  }
  