<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */
/*
  * This file prepares a builder query to be used by the API or by the ExportData job 
*/

namespace App\Queries;

use Illuminate\Http\Request;
use App\Models\Person;
use App\Models\ODBFunctions;
use App\Models\UserJob;

use Illuminate\Support\Arr;

use Response;
use Log;

class PersonSearchQuery
{
    public static function prepQuery($request)
    {

        $persons = Person::select('*');
        if ($request->id) {
            $persons->whereIn('id', explode(',', $request->id));
        }
        if ($request->job_id)
        {
          $ids = UserJob::find($request->job_id)->affected_ids_array;
          $persons->whereIn('id',$ids);
        }

        if ($request->search) {
            ODBFunctions::moreAdvancedWhereIn($persons, ['full_name', 'abbreviation', 'email'], '*'.$request->search.'*');
        }
        if ($request->name) {
            ODBFunctions::advancedWhereIn($persons, 'full_name', $request->name);
        }
        if ($request->abbrev) {
            ODBFunctions::advancedWhereIn($persons, 'abbreviation', $request->abbrev);
        }
        if ($request->email) {
            ODBFunctions::advancedWhereIn($persons, 'email', $request->email);
        }
        if (isset($request->limit) and isset($request->offset)) {
            $persons->offset($request->offset)->limit($request->limit);
        } elseif (isset($request->limit)) {
            $persons->limit($request->limit);          
        }


        $fields = ($request->fields ? $request->fields : 'simple');
        $possible_fields = config('api-fields.persons');
        $field_sets = array_keys($possible_fields);
        if (in_array($fields,$field_sets)) {
            $trimmed_array = array_map('trim',$possible_fields[$fields]);  
            $fields = implode(",",$trimmed_array);
        } else {
            $trimmed_array = array_map('trim',explode(",",$fields));  
            $fields = implode(",",$trimmed_array);
        }   
        return [
            'query' => $persons,
            'fields' => $fields,
        ];
  }



}