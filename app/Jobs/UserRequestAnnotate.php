<?php
/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Individual;
use App\Models\Voucher;

use App\Models\Identification;
use App\Models\ODBFunctions;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\ODBRequest;
use App\Models\ActivityFunctions;
use App\Models\UserJob;
use Activity;
use Auth;
use Lang;

//UPDATES INDIVIDUAL IDENTIFICATIONS ONLY

class UserRequestAnnotate extends ImportCollectable
{

    use AuthorizesRequests;

    /**
     * Execute the job.
     *
     * @return void
     */
     public function inner_handle()
     {

         $data =  $this->extractEntrys();
         //If comming from the web interface, fix date to extract identifications
         if (null !== $this->header['not_external']) {
           $request_id = $this->header['request_id'];
           if (!$this->hasRequiredKeys(["request_type"], $data) or !$request_id) {
               $this->endJobStatus();
               return false;
           }
           $odbrequest = ODBRequest::findOrFail($request_id);
           $voucher_ids = isset($data['voucher_ids']) ? explode(',',$data['voucher_ids']) : $odbrequest->vouchers->pluck('id')->toArray();
           $individual_ids = isset($data['individual_ids']) ? explode(',',$data['individual_ids']) : $odbrequest->individuals->pluck('id')->toArray();

           $attributes = [];
           $status = $data['newstatus'] == "" ? null : (int) $data['newstatus'];
           $notes = $data['notes'] == "" ? null : (string) $data['notes'];
           $newnote = [];
           if (null !== $notes) {
             $newnote = [
               'notes' => $notes,
               'user_id' => Auth::user()->id,
               'date' => now()->toDateTimeString(),
             ];
           }
           if (count($voucher_ids)) {
             $hasvouchers = true;
             $records = $odbrequest->vouchers()->whereIn('vouchers.id',$voucher_ids);
           } elseif (count($individual_ids)) {
             $hasvouchers = false;
             $records = $odbrequest->individuals()->whereIn('individuals.id',$individual_ids);
           } else {
             $this->appendLog('ERROR: Records not informed for this request');
             $this->endJobStatus();
             return false;
           }
           if ($data['request_type']=="register" and !$hasvouchers) {
               $dataset_id = $data['dataset_id'];
               $startnumber = $data['biocollection_voucher_number'];
               if ($startnumber==null and $dataset_id==null) {
                 $this->endJobStatus();
                 $this->appendLog('ERROR: A start number and a Dataset must be informed for Voucher creation');
                 return false;
               }
               $pattern = '/[\.]|[-]|[_]/';
               $components = preg_split($pattern, $startnumber);
               $startbase = null;
               $number = $startnumber;
               if (count($components)>1) {
                  $number = $components[count($components)-1];
                  unset($components[count($components)-1]);
                  $startbase = implode("-",$components);
               }
               $start_register_number = (int) $number;
               if (!($start_register_number == $number)) {
                 $this->endJobStatus();
                 $this->appendLog('ERROR: A least the last part of code must be numeric (integer)!');
                 return false;
               }
               $newnumber = $startbase."-".$start_register_number;
               $isunique = Voucher::where('biocollection_id',$data['biocollection_id'])->where('biocollection_number',$newnumber);
               if ($isunique->count()) {
                 $this->endJobStatus();
                 $this->appendLog('ERROR: The BioCollection number informed already exists in the database for another Voucher of the same BioCollection. Inform a valid new number for the first Voucher to be created');
                 return false;
               };
           }
           $this->userjob->setProgressMax($records->count());
           $torun = $records->count();
           foreach ($records->cursor() as $record){
             if ($this->isCancelled()) {
               break;
             }
             $this->userjob->tickProgress();
             /* allow annotation only if it is closed */
             $oldstatus =  $record->pivot->status;



             $attributes = [];
             $oldnote = isset($record->pivot->notes) ? json_decode($record->pivot->notes, true) : [];
             $key = now()->format('YmdHim');
             if ($data['request_type']=='notes' and count($newnote)) {
              $oldnote[$key] = $newnote;
              $attributes['notes'] = json_encode($oldnote);
              //$this->appendLog(json_encode($oldnote));
             }
             if ($oldstatus<ODBRequest::RETURNED) {

                 if($data['request_type']=='status' and null !== $status) {
                   $statusnote = [
                     'notes' => Lang::get('messages.request_statuschanged').': '.Lang::get('levels.status.'.$oldstatus)." >> ".Lang::get('levels.status.'.$status),
                     'user_id' => Auth::user()->id,
                     'date' => now()->toDateTimeString(),
                   ];
                   $oldnote[$key] = $statusnote;
                   $attributes['notes'] = json_encode($oldnote);
                   $attributes['status'] = $status;
                 }
            //new identification requested
                if ($data['request_type']=='identify' and !$hasvouchers) {
                  $identification =json_decode($record->pivot->notes, true);
                  if (isset($identification['identification']) and $this->authorize('update', $record)) {
                    $newidentification = $identification['identification'];
                    $note = $newidentification['notes'];
                    $person = isset(Auth::user()->person) ? Auth::user()->person->fullname : Auth::user()->email;
                    $addToNote= "[Identification changed by ".$person." upon request].";
                    if ($note !== null ) {
                      $note = $note." ".$addToNote;
                    } else {
                      $note = $addToNote;
                    }
                    $identifiers_nodate = [
                      'taxon_id' => $newidentification['taxon_id'],
                      //'person_id' => $newidentification['person_id'],
                      'biocollection_id' => $newidentification['biocollection_id'],
                      'biocollection_reference' => $newidentification['biocollection_reference'],
                      'notes' => $note,
                      'modifier' => $newidentification['modifier']
                    ];
                    ###############3
                    //identification will be set to self if explicitly informed
                    $oldidentification = null;
                    if ($record->identification) {
                      $oldidentification = $record->identification()->first()->toArray();
                      if ($oldidentification['taxon_id']==$newidentification['taxon_id']) {
                        $this->appendLog('WARNING: Identification of individual '.$record->rawLink()." was already as the one informed. No change made");
                        continue;
                      }
                    }
                    $old_identification_individual_id = $record->identification_individual_id;

                    //has old update or else create
                    $makechange = false;
                    $date = $newidentification['date'];
                    $olddetby=[];
                    if ($record->identificationSet) {
                        $record->identificationSet()->update($identifiers_nodate);
                        $record->identificationSet->collectors()->detach();
                        $record->identificationSet->collectors()->attach($newidentification['identifiers']);
                        $makechange = true;
                        $record->identificationSet->setDate($date);
                        $record->identificationSet->save();
                    } else {
                        if ($old_identification_individual_id != $record->id) {
                          $makechange = true;
                        }
                        $record->identification_individual_id  =  $record->id;
                        $record->save();
                        $record->identificationSet = new Identification(array_merge($identifiers_nodate, ['object_id' => $record->id, 'object_type' => 'App\Models\Individual']));
                        $record->identificationSet->setDate($date);
                        $record->identificationSet->save();

                        $olddetby = $record->identificationSet->collectors->pluck('id');
                        $record->identificationSet->collectors()->detach();
                        $record->identificationSet->collectors()->attach($newidentification['identifiers']);
                        $record->identificationSet->save();
                    }
                    $attributes['status'] = ODBRequest::IDENTIFICATION_CHANGED;
                    $oldstatus = $record->pivot->status;
                    $statusnote = [
                      'notes' => Lang::get('messages.request_statuschanged').': '.Lang::get('levels.status.'.$oldstatus)." >> ".Lang::get('levels.status.'.ODBRequest::IDENTIFICATION_CHANGED),
                      'user_id' => Auth::user()->id,
                      'date' => now()->toDateTimeString(),
                    ];
                    $oldnote[$key] = $statusnote;
                    $attributes['notes'] = json_encode($oldnote);
                    if ($makechange) {
                      //log identification changes if any
                      $identifiers_nodate['date'] = $record->identificationSet->date;
                      if ($old_identification_individual_id != $record->id and null != $old_identification_individual_id) {
                          $oldidentification['identification_individual_id']  = $old_identification_individual_id;
                          $identifiers_nodate['identification_individual_id'] = $record->id;
                      }
                      ActivityFunctions::logCustomChanges($record,$oldidentification,$identifiers_nodate,'individual','identification updated',null);

                      ActivityFunctions::logCustomPivotChanges($record,$olddetby->all(),$newidentification['identifiers'],'individual','identification updated',$pivotkey='person');

                      
                    }
                  } elseif (!$this->authorize('update', $record)) {
                     $attributes['status'] = ODBRequest::IDENTIFICATION_DENIED;
                     $this->appendLog('WARNING: You do not have permission to alter identification of record'.$record->rawLink());
                  }
                 }


                 //new voucher registration requested
                 if ($data['request_type']=="register" and !$hasvouchers) {
                     if ($odbrequest->vouchers->count()) {
                       $biocollection_ids = array_unique($odbrequest->vouchers->pluck('biocollection_id')->toArray());
                     } else {
                       $biocollection_ids = array_unique($odbrequest->individuals->pluck('pivot.biocollection_id')->toArray());
                     }
                     $newnumber = $startbase."-".$start_register_number;
                     $start_register_number = $start_register_number+1;
                     $newvoucher = [
                         'individual_id' => $record->id,
                         'biocollection_id' => $data['biocollection_id'],
                         'biocollection_number' => $newnumber,
                         'biocollection_type' => 0,
                         'dataset_id' => $data['dataset_id'],
                     ];
                     $isunique = Voucher::where('individual_id',$record->id)
                     ->where('biocollection_id',$data['biocollection_id'])
                     ->where('biocollection_number',$newnumber);
                     if ($isunique->count()) {
                       $this->appendLog('ERROR: There is an Voucher for the same BioCollection with same Number for the individual '.$record->rawLink().'. A Voucher was not created for this Individual.');
                       continue;
                     };
                     $voucher = new Voucher($newvoucher);
                     $voucher->save();
                     $oldstatus = $record->pivot->status;
                     $statusnote = [
                       'notes' => Lang::get('levels.status.'.ODBRequest::VOUCHERS_REGISTERED)." for this Individual",
                       'user_id' => Auth::user()->id,
                       'date' => now()->toDateTimeString(),
                     ];
                     $oldnote[$key] = $statusnote;
                     $attributes['status'] = ODBRequest::VOUCHERS_REGISTERED;
                     $attributes['notes'] = json_encode($oldnote);
                 }

            }
            if($hasvouchers and count($attributes)) {
              $odbrequest->vouchers()->updateExistingPivot($record->id,$attributes);
              $this->affectedId($record->id);
            } elseif (count($attributes)) {
              $this->affectedId($record->id);
              $odbrequest->individuals()->updateExistingPivot($record->id,$attributes);
            } else {
              $this->appendLog('WARNING: Status for Individual '.$record->rawLink()." is <strong>".Lang::get('levels.status.').$oldstatus."</strong>. No change made!");
            }

           }

           $done = count(json_decode($this->userjob->affected_ids));
           $msg = Lang::get("messages.request_posted")."  <strong>".$done."/".$torun." records</strong> where annotated. Request link: ";
           $url = url('odbrequests/'.$odbrequest->id);
           $url = ": <a href='".$url."' >".$url."</a>";
           $this->appendLog($msg.$url);
        } else {
          // TODO: Include handler to deal with external API data
        }
     }

}
