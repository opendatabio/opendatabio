<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Individual;
use App\Models\ExternalAPIs;
use Activity;
use App\Models\ActivityFunctions;
use Log;
use Auth;

class ImportActivity extends ImportCollectable
{
    /**
     * Execute the job.
     */
    public function inner_handle()
    {
        $data = $this->extractEntrys();
        if (!$this->setProgressMax($data)) {
            return;
        }
        foreach ($data as $history) {
            if ($this->isCancelled()) {
                break;
            }
            $this->userjob->tickProgress();
            //Log::info($history);            
            //$this->appendLog('os valores sao: '.json_encode($history));        
            
            if ($this->validateData($history)) {
                // Arrived here: let's import it!!
                try {
                    $this->import($history);
                } catch (\Exception $e) {
                    $this->setError();
                    $this->appendLog('Exception '.$e->getMessage().' at '.$e->getFile().'+'.$e->getLine().' on activity log '.$e->getTraceAsString());
                }

            }
            
        }
    }

    protected function validateData(&$history)
    {
        if (!$this->hasRequiredKeys(['log_name','subject_type','subject_id','properties','description','created_at'], $history)) {
            return false;
        }

        if (!$this->validateModelAndUserAndDate($history)) {
            return false;
        }
        if (!$this->validateIdentificationFields($history)) {
            return false;
        }

        return true;

    }

    protected function validateIdentificationFields(&$history)
    {
        $properties = json_decode($history['activity']['properties'],TRUE);
        $attributes = isset($properties['attributes']) ? $properties['attributes'] : null;
        $old = isset($properties['old']) ? $properties['old'] : null;
        //validate identifications
        $newdet = $this->extractIdentification($attributes);
        $olddet = $this->extractIdentification($old);
        if (isset($newdet['date'])) {
            $newdet['date'] = $newdet['date'][2]."-".$newdet['date'][1]."-".$newdet['date'][0];
        }
        if (isset($olddet['date'])) {
            $olddet['date'] = $olddet['date'][2]."-".$olddet['date'][1]."-".$olddet['date'][0];
        }
        $validfields = ["identifier or identifier_id**","taxon or taxon_id**", "modifier", "identification_based_on_biocollection and identification_based_on_biocollection_id", "identification_notes","identification_date or any of identification_date_year, identification_date_month, identification_date_day"];
        if (!$newdet or !$olddet) {
            $this->skipEntry($history, 'The properties field must have two entries: "attributes" and "old", each containing some valid identifications fields. Valid fields are '.implode(" | ",$valid_fields));
            return false;
        }
        $new = array_diff($newdet,$olddet);
        $old = array_diff($olddet,$newdet);
        if (count($new)==0 & count($old)==0) {
            $this->skipEntry($history, 'The properties field must have two entries: "attributes" and "old" and the fields must contain differences. These have no differences.');
            return false;
        }
        $newprop = [
            'attributes' => $new,
            'old' => $old,
        ];

        $history['activity']['properties'] = $newprop;
        return true;
    }


    protected function validateModelAndUserAndDate(&$history)
    {
        $valid_models = ['Individual' => 'App\Models\Individual'];
        $valid_inputs = array_keys($valid_models);
        $subject = $history["subject_type"];
        if (in_array($subject,$valid_inputs)) {
          $subject = $valid_models[$subject];
        } elseif (!in_array($subject,$valid_models)) {
          $this->skipEntry($history, 'subject_type is required, yours is invalid or not allowed. Allowed values:'.implode(" | ",$valid_inputs)." or ".implode(" | ",$valid_models) );
          return false;
        }
        $subject_id = app($subject)::find($history['subject_id']);
        if (!$subject_id) {
            $this->skipEntry($history, 'subject_id is required, yours is invalid for subject_type. '.$subject);  
          return false;
        }
        $subject_id = $subject_id->id;
        $created_at = explode("-",$history['created_at']);  
        $date_valid = false;
        if (count($created_at)==3)   {
            if (is_numeric($created_at[1]) and is_numeric($created_at[2]) and is_numeric($created_at[0])) {
                $date_valid = checkDate($created_at[1],$created_at[2],$created_at[0]);
            }
        }
        if (!$date_valid) {
            $this->skipEntry($history, 'created_at has an invalid date value');              
            return false;
        }
        $prop = json_decode($history['properties'],true);
        if (!is_array($prop)) {
          $this->skipEntry($history, 'properties field has invalid content');  
          return false;
        }


        $keys = array_keys($prop);
        $history['activity'] = [];
        if (in_array('old',$keys) && in_array('attributes',$keys) && count($keys)==2) {
            $record = [
               'log_name' => isset($history['log_name']) ? $history['log_name'] : "individual",
               'subject_type' => $subject,
               'subject_id' => $subject_id,
               'properties' => json_encode($prop),
               'description' => isset($history['description']) ? $history['description'] : "identification updated",
               'created_at' => $history['created_at'],
               'causer_id' => Auth::user()->id,
               'causer_type' => "App\Models\User",
            ];
            $history['activity'] = $record;
            return true;
        }
        $this->skipEntry($history, 'The properties field must have two entries: "attributes" and "old". Yours does not have.');  
        return false;
    }

    public function import($history)
    {
        /* ready to import */
        $activity = $history['activity'];
        //Log::info($activity);
        $properties = $activity['properties'];
        $activity['properties'] = NULL;
        $obj = Activity::create($activity);
        $obj->properties = collect($properties);
        $obj->save();

        $this->affectedId($obj->id);
        return;
    }
}
