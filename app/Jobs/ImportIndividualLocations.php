<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\IndividualLocation;
use App\Models\Location;
use App\Models\Individual;
use App\Models\ODBFunctions;
use Illuminate\Http\Request;

use Spatie\SimpleExcel\SimpleExcelReader;
use Storage;


class ImportIndividualLocations extends ImportCollectable
{
    private $requiredKeys;

    /**
     * Execute the job.
     */
    public function inner_handle()
    {

      $data = $this->extractEntrys();

      $hasfile = $this->userjob->data['data'];
      /* if a file has been uploaded */
      if (isset($hasfile['filename'])) {
        $filename = $hasfile['filename'];
        $filetype = $hasfile['filetype'];
        $path = storage_path('app/public/tmp/'.$filename);
        /* this will be a lazy collection to minimize memory issues*/
        $howmany = SimpleExcelReader::create($path)->getRows()->count();
        $this->userjob->setProgressMax($howmany);
        /* I have to do twice, not understanding why loose the collection if I just count on it */
        $data = SimpleExcelReader::create($path)->getRows();
      } else {
        /* this has recieved a json */
        if (!$this->setProgressMax($data)) {
            return;
        }
      }
      $this->affectedModel(Individual::class);

      //if these fields are provided in header, remove from there
      foreach ($data as $individual_location) {

            if ($this->validateData($individual_location)) {
                try {
                    $this->import($individual_location);
                } catch (\Exception $e) {
                    $this->setError();
                    $this->appendLog('Exception '.$e->getMessage().' at '.$e->getFile().'+'.$e->getLine());
                }
            }
            if ($this->isCancelled()) {
                break;
            }
            $this->userjob->tickProgress();
        }
    }

    protected function validateData(&$individual_location)
    {
        //dataset may have been informed, will fail only if informed not FOUND
        //else will place individual in its own default dataset
        if (!$this->validateIndividual($individual_location)) {
            return false;
        }
        $newentry = $this->extractLocationFields($individual_location);
        $newentry['id'] = null;
        if (!$this->validateLocations($newentry)) {
            return false;
        }
        $individual_location = $newentry;
        return true;
    }


    public function import($entry)
    {
        $location = $entry['individual_locations'][0];
        $location['individual_id'] = $entry['individual_id'];
        $individual = Individual::findOrFail($entry['individual_id']);

        $newindlocation = new Request;
        $newindlocation->merge($location);
        //store the record, which will result in the individual
        $savedindlocation = app('App\Http\Controllers\IndividualController')->saveIndividualLocation($newindlocation);
        if ($savedindlocation->getData()->errors == 1) {
          $this->append('ERROR: location '.json_encode($location).' could not be saved for individual '.$individual->fullname.'  Errors:'.$savedindlocation->getData()->saved);
        }
        $this->affectedId($individual->id);
        return;
    }
}
