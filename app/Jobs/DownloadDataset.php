<?php
/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use Spatie\SimpleExcel\SimpleExcelWriter;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Dataset;
use App\Models\Project;
use App\Models\Measurement;
use App\Models\BibReference;
use App\Models\Individual;
use App\Models\Voucher;
use App\Models\Taxon;
use App\Models\Location;
use App\Models\Media;
use App\Models\ODBFunctions;
use App\Models\ODBTrait;
use App\Models\DatasetVersion;
use App\Queries\BibReferenceSearchQuery;
use App\Queries\IndividualLocationSearchQuery;
use App\Queries\IndividualSearchQuery;
use App\Queries\LocationSearchQuery;
use App\Queries\MeasurementSearchQuery;
use App\Queries\VoucherSearchQuery;
use App\Queries\TaxonSearchQuery;
use App\Queries\TraitSearchQuery;

use File;
use DB;
use Auth;
use Lang;
use Mail;
use Activity;
use ZipArchive;
use Log;


class DownloadDataset extends AppJob
{

    /**
     * Execute the job.
     *
     * @return void
     */
     public function inner_handle()
     {


        $data =  $this->extractEntrys();

        $jobid = "job-".$this->userjob->id;

        //is this generating a version?
        $dataset_version = isset($data['dataset_version']) ? $data['dataset_version'] : null;
        
        //get all ids
        $dataset = Dataset::findOrFail($data['id']);
                
        $individuals_ids = $dataset->all_individuals_ids();
        $vouchers_ids = $dataset->all_voucher_ids();
        $measurements_ids = $dataset->measurements()->pluck('id')->toArray();
        $locations_ids = $dataset->all_locations_ids();
        $taxons_ids = $dataset->all_taxons_ids();
        $media_ids = $dataset->media()->pluck('id')->toArray();
        $odbtrait_ids = $dataset->measurements()->pluck('trait_id')->toArray();
        $bibrefs_ids = $dataset->all_bibref_ids();
      
        $export_set = [
          'individuals' => $individuals_ids,
          'vouchers' => $vouchers_ids,
          'measurements' => $measurements_ids,
          'taxons' => $taxons_ids,
          'locations' => $locations_ids,
          'media' => $media_ids,
          'traits' => $odbtrait_ids,
        ];
        $export_set = array_filter($export_set,function($set) { return count($set)>0;});

        if (count($export_set)==0) {
          $this->appendLog('Error: there is nothing to be exported THE ID IS'.$data['id']);
          return false;
        }
        $chunked_set = [];
        $nsteps =0;
        foreach($export_set as $endpoint => $ids ) {
            $ids = array_chunk($ids,500);
            $nsteps = $nsteps+count($ids);
            $chunked_set[$endpoint] = $ids;
        }
        if (!$dataset_version) {
          $nsteps=$nsteps+2+count($media_ids);
        }
        $this->userjob->setProgressMax($nsteps);
        $files = [];
        foreach($chunked_set as $endpoint => $chunks) {
              $prepared_files = $this->saveModel($endpoint,$chunks,$dataset_version);
              $files = array_merge($files,$prepared_files);
        }

        /* bibtex file */
        if (count($bibrefs_ids)) {          
          $filename = "job-".$jobid."_DatasetBibliography.bib";
          $path = 'app/public/downloads/'.$filename;
          if ($dataset_version) {
            $dt_version = DatasetVersion::find($dataset_version);
            $filename = "Bibliography_".$dt_version->uuid.".bib";
            $path = 'app/public/datasets/'.$filename;
          }
          $text = '';
          $references = BibReference::whereIn('id',$bibrefs_ids)->cursor();
          foreach($references as $reference)
          {
            $text .= $reference->bibtex."\n\n";
          }
          $files[] = $filename;
          $fn = fopen(storage_path($path),'w');
          fwrite($fn,$text);
          fclose($fn);
        }


        $dataset = Dataset::find($data['id']);
        $public_dir=storage_path('app/public/downloads');
        $today = now();
        $datasetname = $dataset->name;

        /*media files */
        // TODO: MUST LIMIT SIZE

        if (count($media_ids)>0 and !$dataset_version) {
          $jobid = "job-".$this->userjob->id;
          $mediazipfilename = $jobid."_MediaFiles_".$today->toDateString().".zip";
          $mediazipfile =  $public_dir . '/' . $mediazipfilename;

          // Create ZipArchive Obj
          $mediazip = new ZipArchive;
          if (true === ($mediazip->open($mediazipfile, ZipArchive::CREATE | ZipArchive::OVERWRITE))) {
            // Add Files in ZipArchive
            $medias = Media::whereIn("id",$media_ids)->cursor();
            foreach($medias as $media) {
                $newname = $media->file_name;
                $mediazip->addFile($media->getPath(),$newname);
            }
            $mediazip->close();
         }
       }

        $this->prepReadme($dataset,$files,$dataset_version);
                
        //ZIP THE FILES INTO A SINGLE BUNDLE        
        // Zip File Name
        if (!$dataset_version) {
          $zipFileName = Str::ascii($datasetname);
          $zipFileName = str_replace("  "," ",$zipFileName);
          $zipFileName = str_replace(" ","_",$zipFileName);
          $zipFileName = $zipFileName."_".$today->toDateString().".zip";

          //add job id to name to find and delete when user delete his job.
          $jobid = "job-".$this->userjob->id;
          $zipFileName = $jobid."_".$zipFileName;

          // Create ZipArchive Obj
          $zip = new ZipArchive;
          $zipfile =  $public_dir . '/' . $zipFileName;
          if (true === ($zip->open($zipfile, ZipArchive::CREATE | ZipArchive::OVERWRITE))) {
              // Add Files in ZipArchive
              foreach($files as $file) {
                  $newname = explode("_",$file);
                  unset($newname[0]);
                  $newname = implode("_",$newname);
                  $zip->addFile($public_dir."/".$file,$newname);
              }
              $zip->close();

              //delete files
              //keep files for seen 
              /*
              foreach($files as $file) {
                  $pathToDel = $public_dir."/".$file;
                  File::delete($pathToDel);
              }
              */
          }

          //LOG THE FILE FOR USER DOWNLOAD
          $file = "Files for dataset <strong>".$datasetname."</strong> prepared ".$today." ";
          $tolog = $file."<br><a href='".url('storage/downloads/'.$zipFileName)."' download >".$zipFileName."</a>";

          if (count($media_ids)>0) {
            $tolog .= "<br><a href='".url('storage/downloads/'.$mediazipfilename)."' download >".$mediazipfilename."</a>";
          }

          $tolog .= "<br>".Lang::get('messages.dataset_download_file_tip');
          $this->appendLog($tolog);


          //log dataset export for tracking download and use history
          $logName  = 'dataset_exports';
          $tolog = [
              'attributes' => [
                'dataset_id' => $data['id'],
                'user_id' => Auth::user()->id
              ],
              'old' => NULL
            ];
          activity($logName)
            ->performedOn(Dataset::find($data['id']))
            ->withProperties($tolog)
            ->log('Authorized download');
          

          //send email to user
          if (null != env('MAIL_USERNAME')) {
            $to_email = Auth::user()->email;
            if (isset(Auth::user()->person->full_name)) {
              $to_name = Auth::user()->person->full_name;
            } else {
              $to_name = $to_email;
            }
            $content = Lang::get('messages.dataset_downloaded_message').":<br>".$zipFileName;
            if (isset($mediazipfilename)) {
              $content .= "<br>".$mediazipfilename;
            }
            $content .= "<br><br>".Lang::get('messages.dataset').":  &nbsp;<strong>".$datasetname."</strong> [".now().",  @ Job Id# ".$this->userjob->id."].";
            $content .= "<br>URL:&nbsp;<a href='".env('APP_URL')."/userjobs/".$this->userjob->id."'>";
            $content .= env('APP_URL')."/userjobs/".$this->userjob->id."</a><br>";
            $content .= "<br>".Lang::get('messages.dataset_download_file_tip');
            $data = [
              'to_name' => $to_name,
              'content' => $content,
            ];
            try {
              Mail::send('common.email', $data, function($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)->subject(Lang::get('messages.dataset_request').' - '.env('APP_NAME'));
              });
            } catch (\Exception $e) {
              $tolog = "Error: Could not send email";
              $this->appendLog($tolog);
            }
          }
        } else  {
          $dt_version = DatasetVersion::find($dataset_version);
          $metadata = $dt_version->metadata;
          if ($metadata) {
            $metadata = json_decode($metadata,true);
            $metadata['files'] = $files;            
          } else {
            $metadata = ['files'=> $files ];
          }
          $dt_version->metadata = json_encode($metadata);
          $dt_version->save();
        }

        $this->userjob->progress = $this->userjob->progress_max;
        $this->userjob->save();
      }


      public function getDW($endpoint)
    {
        switch ($endpoint) {
            case 'individuals':
              return "Organisms";
              break;
            case 'individuallocations':
              return "Occurrences";
              break;
            case 'vouchers':
              return "PreservedSpecimens";
              break;
            case 'taxons':
              return "Taxons";
              break;
            case 'measurements':
              return "MeasurementsOrFacts";
              break;
            case 'traits':
              return "MeasurementTypes";
              break;
            case 'media':
              return "MediaAttributes";
              break;
            case 'locations':
              return "Locations";
              break;
          default:
            return null;
            break;
        }
    }

    public function saveModel($endpoint,$chunks,$dataset_version=null)
    {
       //create temporary file to store the data (add the job id to the file name to be able to destroy it)
       $jobid = $this->userjob->id;
       $filename = "job-".$jobid."_".self::getDW($endpoint).".csv";
       $path = 'app/public/downloads/'.$filename;
       $data_filter = [];
       if ($dataset_version) {
        $dt_version = DatasetVersion::find($dataset_version);
        $data_filter = $dt_version->filters ? json_decode($dt_version->filters,true) : [];
        $filename = self::getDW($endpoint)."_".$dt_version->uuid.".csv";
        $path = 'app/public/datasets/'.$filename;
       }
       
       $writer = SimpleExcelWriter::create(storage_path($path));
       $categories=[];
       $counter = 1;
       foreach($chunks as $ids) {
          //if user cancels job
          if ($this->isCancelled()) {
            break;
          }
          $fields = "all";
          if ($endpoint=='traits') {
            $fields = "exceptcategories";
          }
          $params = array('fields' => $fields, 'id' => implode(',',$ids));
          $request = new Request;
          $request = $request->merge($params);
          if (count($data_filter)) {
            $taxon_root = isset($data_filter['selected_taxons']) ? implode(',',collect( $data_filter['selected_taxons'])->pluck('id')->toArray()) : null;
            $location_root = isset($data_filter['selected_locations']) ? implode(',',collect( $data_filter['selected_locations'])->pluck('id')->toArray()) : null;
            $persons = isset($data_filter['persons']) ? implode(',',collect( $data_filter['persons'])->pluck('id')->toArray()) : null;
            $traits = isset($data_filter['selected_traits']) ? implode(',',collect( $data_filter['selected_traits'])->pluck('id')->toArray()) : null;
            $min_date = isset($data_filter['filter_min_date']) ? $data_filter['filter_min_date'] : null;
            $max_date = isset($data_filter['filter_max_date']) ? $data_filter['filter_max_date'] : null;           
            $filter_params = [
              'taxon_root' => $taxon_root,
              'location_root' => $location_root,
              'person' => $persons,
              'date_min' => $min_date,
              'date_max' => $max_date,
              'trait' => $traits,
            ];      
            $filter_params = array_filter($filter_params);
            $request = $request->merge($filter_params);
          }
          switch ($endpoint) {
            case "individuals":                  
              $prepared_query = IndividualSearchQuery::prepQuery($request);
              break;
            case "vouchers":
              $prepared_query = VoucherSearchQuery::prepQuery($request);
              break;            
            case "individual-locations":
              $prepared_query = IndividualLocationSearchQuery::prepQuery($request);  
              break;
            case "bibreferences":
                $prepared_query = BibReferenceSearchQuery::prepQuery($request);  
                break;  
            case "locations":
              $prepared_query = LocationSearchQuery::prepQuery($request);      
              break;
            case "measurements":
              $prepared_query = MeasurementSearchQuery::prepQuery($request);        
              break;
            case "taxons":
              $prepared_query = TaxonSearchQuery::prepQuery($request);        
              break;
            case "traits":              
              $prepared_query = TraitSearchQuery::prepQuery($request);          
              break;                  
            default:
              $prepared_query = [];
          }
          $fields = $prepared_query['fields'];                
          $query = $prepared_query['query'];               
          $collection = $query->cursor();
          if ($fields!="raw") {
            if ($endpoint=="traits") {
              $lang = $prepared_query['lang'];
              $final = TraitSearchQuery::prepCategories($collection,$fields,$lang);
              $newvalues = [];
              foreach ($final as $key => $value) {
                  if (!empty($value['categories'])) {
                    $categories[] = $value['categories'];
                  }
                  unset($value['categories']);
                  $newvalues[] = $value;
              }
              $final = $newvalues;
            } else {
              $final = $collection->map(function ($obj) use ($fields) {
                $result = [];
                $fields_arr = explode(",",$fields);
                foreach ($fields_arr as $field) {
                  $result[$field] = isset($obj[$field]) ? $obj[$field] : null;      
                }                  
                return $result;  
              });
            } 
          } else {
            $final = $collection->map(function ($obj) {
                return $obj->toArray();
            });
          }
          $writer->addRows($final);          
          $this->userjob->tickProgress();       
        }
        $prepared_files = [$filename];
        if (count($categories)) {
          $filename_cats = "job-".$jobid."_".self::getDW($endpoint)."_categories.csv";          
          $path_cats = 'app/public/downloads/'.$filename_cats;
          if ($dataset_version) {
            $filename_cats = self::getDW($endpoint)."_categories_".$dt_version->uuid.".csv";
            $path_cats = 'app/public/datasets/'.$filename;
          }
          $writercat = SimpleExcelWriter::create(storage_path($path_cats));
          foreach($categories as $key => $cats) {
            foreach($cats as $cat) {
               $writercat->addRow($cat);
            }
          }          
          $prepared_files[] = $filename_cats;
        }
        return $prepared_files;
    }

    public function prepReadme($dataset,&$files=[],$dataset_version=null) : void
    {     
        $readme = "\n========== README =========\n";           
        if (!$dataset_version) {
          $readme .= Lang::get('messages.dataset').": ".$dataset->name."\n";
          $readme .= "URL: ".url('dataset/'.$dataset->id)."\n";
          if ($dataset->description) {
            $readme .= "\n================\n";
            $readme .= $dataset->description;
            $readme .= "\n\n================\n";
          }
          //$readme .= Lang::get('messages.howtocite').":\n";
          //$readme .= $dataset->bibliographicCitation."\n\n";
          //$readme .= $dataset->bibtex."\n\n";
          //$readme .= "\n\n================\n";
          if ($dataset->license) {
            $readme .=  Lang::get('messages.license').":\n".$dataset->license;
          }
          if ($dataset->policy) {
            $readme .=  "\nSome restrictions may apply. ".Lang::get('messages.data_policy').":\n".$dataset->policy;
          }
          $readme .= "\n\n================\n";  
          $mandatory = $dataset->references()->where('mandatory',1);
          if ($mandatory->count()) {
            $readme .=  Lang::get('messages.dataset_bibreferences_mandatory').": \n";
            foreach($mandatory->cursor() as $reference)
            {
              $readme .=  $reference->first_author." ".$reference->year.". ".$reference->title."\n\n";
              $readme .=  $reference->bibtex."\n\n";
            }
            $readme .= "\n\n================\n";
          }
  
          /* who are the administrators */
          $readme .= Lang::get('messages.admins').":\n";
          $people = $dataset->people['admins'];
          foreach($people as $admin) {
            $adm = $admin[0]." ".$admin[1];
            $readme .= "\t".$adm."\n";
          }
          $readme .= "\n\n================\n";  
          if ($dataset->project) {
            $readme .= Lang::get('messages.ṕroject').":\n";
            $title = isset($dataset->project->name) ? $dataset->project->name : $dataset->project->acronym;
            $readme .= "\t".$title."\n\tURL: ".url("projects/".$dataset->project->id)."\n";
          }          
          $filename = "job-".$jobid."_README.txt";
          $path = 'app/public/downloads/'.$filename;
        } else {
            $dt_version = DatasetVersion::find($dataset_version);
            $dt_uuid = $dt_version->uuid;
            $filename = "README_".$dt_uuid.".txt";
            $path = 'app/public/datasets/'.$filename; 
            
            $readme .= Lang::get('messages.dataset').": ".$dt_version->dataset->name."\n";
            $readme .= Lang::get('messages.version').": ".$dt_version->version." ".$dt_version->date."\n";
            $readme .= "URL: ".url('dataset-uuid/'.$dt_version->uuid)."\n";
            $citation = $dt_version->bibtex_print;
            if ($citation) {
                $readme .=  Lang::get('messages.citation').": \n";
                $readme .=  strip_tags($dt_version->citation_print)." \n\n";
                $readme .=  $citation." \n";
                $readme .= "\n================\n";
            }  
            $policy = $dt_version->policy;
            if ($policy) {
              $readme .=  Lang::get('messages.data_policy').": \n";
              $readme .=  $dt_version->policy." \n";
              $readme .= "\n================\n";
          }  

        }
        if (count($files)) {
          $readme .=  Lang::get('messages.files').": \n";
          foreach($files as $file) {
            $readme .=  $file." \n";
          }
          $readme .= "\n================\n";
        }
        $fn = fopen(storage_path($path),'w');
        fwrite($fn,$readme);
        fclose($fn);
        $files[] = $filename;     
    }





























}
