<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Person;
use App\Models\Biocollection;
use CodeInc\StripAccents\StripAccents;
use Illuminate\Http\Request;
use Auth;

class UpdatePersons extends AppJob
{

    /**
       * The number of times the job may be attempted.
       * Only the Persons job, for some reason
       * @var int
       */
    public $tries = 1;


    /**
     * Execute the job.
     */


    public function inner_handle()
    {
        $data = $this->extractEntrys();
        if (!$this->setProgressMax($data)) {
            return;
        }
        $this->affectedModel(Person::class);

        foreach ($data as $registry) {
            if ($this->isCancelled()) {
                break;
            }
            $this->userjob->tickProgress();

            if ($this->validateData($registry)) {
                try {
                    $this->updatePerson($registry);
                } catch (\Exception $e) {
                    $this->setError();
                    $this->appendLog('Exception '.$e->getMessage().' at '.$e->getFile().'+'.$e->getLine().' on person '.$registry['full_name'].$e->getTraceAsString());
                }
            }
        }
    }

    protected function validateData(&$registry)
    {
        $theid = isset($registry['person_id']) ? $registry['person_id'] : (isset($registry['id']) ? $registry['id'] : null);
        if (!isset($theid)) {
          $this->skipEntry($registry, 'Which Person? You must provide the Person id to update!');
          return false;
        }
        $theperson = Person::findOrFail($theid);
        if ($theperson->count()==0) {
          $this->skipEntry($registry, 'Person with id '.$theid.' not found. Nothing to update.');
          return false;
        }
        if(!Auth::user()->can('update',$theperson)) {
          $this->skipEntry($registry, 'You are not authorized to update this record!');
          return false;
        }
        $registry['id'] = $theid;

        /*field mandatory */
        if (!isset($registry['abbreviation'])) {
          $registry['abbreviation'] = $theperson->abbreviation;
        }
        if (!isset($registry['full_name'])) {
          $registry['full_name'] = $theperson->full_name;
        }

        $registry['email_public'] = (null !== $registry['email_public']) ? $registry['email_public'] : $theperson->email_public;

        if (!isset($registry['email'])) {
          $registry['email'] = $theperson->email;
        } elseif (!filter_var($registry['email'], FILTER_VALIDATE_EMAIL)) {
            $this->skipEntry($registry, ' Email invalid');
            return false;
        }
        if (!isset($registry['institution'])) {
          $registry['institution'] = $theperson->institution;
        }
        if (!isset($registry['notes'])) {
          $registry['notes'] = $theperson->notes;
        }
        if (!$this->validateBiocollectionSimple($registry)) {
            return false;
        }
        if (!$this->validateTaxonsSimples($registry)) {
            return false;
        }
        return true;
    }

    public function updatePerson($registry)
    {
        $theperson = Person::findOrFail($registry['id']);

        $update_request = [
            'from_the_api' => 1,
            'full_name' => $registry['full_name'],
            'abbreviation' => $registry['abbreviation'],
            'email' => $registry['email'],
            'institution' => $registry['institution'],
            'biocollection_id' => isset($registry['biocollection_id']) ? $registry['biocollection_id'] : $theperson->biocollection_id,
            'notes' =>$registry['notes'],
            'email_public' =>$registry['email_public'],
            'specialist' => isset($registry['taxons_ids']) ? $registry['taxons_ids'] : $theperson->taxons()->pluck('id')->toArray(),
        ];
        $update_request = array_filter($update_request,function($a) { return null !== $a;});


        //check of exact abbreviation duplicates
        $normalized_abbreviation = trim(mb_strtolower($registry['abbreviation']));
        $normalized_abbreviation = StripAccents::strip( (string) $normalized_abbreviation);
        $normalized_abbreviation = preg_replace('/[^a-zA-Z0-9]/', '', $normalized_abbreviation);
        $person_id = $theperson->id;
        $same = Person::all()->filter(function ($aperson) use ($normalized_abbreviation,$person_id) {
              $nm_abbreviation = $aperson->abbreviation;
              $nm_abbreviation = trim(mb_strtolower($nm_abbreviation));
              $nm_abbreviation = StripAccents::strip( (string) $nm_abbreviation);
              $nm_abbreviation = preg_replace('/[^a-zA-Z0-9]/', '', $nm_abbreviation);
              return ($normalized_abbreviation==$nm_abbreviation and $aperson->id!=$person_id);
        });
        if ($same->count()>0) {
            $this->skipEntry($registry, ' There are other Persons with this abbreviation. Found were '.implode(" | ",$same->pluck('abbreviation')->toArray()));
            return;
        }

        //transform info in a request
        $saverequest = new Request;
        $saverequest = $saverequest->merge($update_request);
        //$this->skipEntry($saverequest->all(),'error here');
        //return;
        //store the record, which will result in the individual
        $updatedPerson = app('App\Http\Controllers\PersonController')->update($saverequest,$theperson->id);
        if (is_string($updatedPerson)) {
          $this->skipEntry($registry,'This Person could not be updated. Possible errors may be: '.$updatedPerson);
          return ;
        }
        $this->affectedId($theperson->id);
        return;
    }
}
