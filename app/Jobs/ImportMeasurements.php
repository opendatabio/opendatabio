<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Measurement;
use Illuminate\Http\Request;
use Lang;
use Spatie\SimpleExcel\SimpleExcelReader;

class ImportMeasurements extends MeasurementsJobs
{

    protected $sourceType;

    /**
     * Execute the job.
     */
    public function inner_handle()
    {
        $data = $this->extractEntrys();

        $hasfile = $this->userjob->data['data'];
        /* if a file has been uploaded */
        if (isset($hasfile['filename'])) {
          $filename = $hasfile['filename'];
          $filetype = $hasfile['filetype'];
          $path = storage_path('app/public/tmp/'.$filename);
          /* this will be a lazy collection to minimize memory issues*/
          $howmany = SimpleExcelReader::create($path)->getRows()->count();
          $this->userjob->setProgressMax($howmany);
          /* I have to do twice, not understanding why loose the collection if I just count on it */
          $data = SimpleExcelReader::create($path)->getRows();
        } else {
          if (!$this->setProgressMax($data)) {
              return;
          }
        }
        $this->affectedModel(Measurement::class);

        foreach ($data as $measurement) {
            if ($this->isCancelled()) {
                break;
            }
            $this->userjob->tickProgress();

            if ($this->validateData($measurement)) {
                // Arrived here: let's import it!!
                try {
                    $this->import($measurement);
                } catch (\Exception $e) {
                    $this->setError();
                    $this->appendLog('Exception '.$e->getMessage().' at '.$e->getFile().'+'.$e->getLine().' on measurement '.$measurement['object_id'].$e->getTraceAsString());
                }
            }
        }
    }

    protected function validateData(&$measurement)
    {
        $measurement['trait_id'] = isset($measurement['trait_id']) ? $measurement['trait_id'] : (isset($measurement['trait']) ? $measurement['trait'] : null);
        $measurement['measured_id'] = isset($measurement['object_id']) ? $measurement['object_id'] : (isset($measurement['measured_id']) ? $measurement['measured_id'] : null);
        $requiredKeys = ['measured_id','trait_id'];
        if (!$this->hasRequiredKeys($requiredKeys, $measurement)) {
            return false;
        }    
        if (!$this->validatePerson($measurement)) {
              return false;
        }
        if (!$this->validateObject($measurement)) {
            return false;
        }
        if (!$this->validateMeasurementDataset($measurement)) {
              return false;
        }
        if (!$this->validateDatasetPolicies($measurement)) {
              return false;
        }
        if (!$this->extractMeasurementDate($measurement)) {
            return false;
        }
        if (!$this->validateMeasurements($measurement)) {
            return false;
        }
        if (!$this->validateMeasurementBibReference($measurement)) {
            return false;
        }
        if (!$this->validateMeasurementParent($measurement)) {
            return false;
        }
        if (!$this->validateMeasurementLocation($measurement)) {
            return false;
        }
        return true;
    }

    public function import($measurement)
    {
        $record = [
            'trait_id' => $measurement['trait_id'],
            'measured_id' => $measurement['measured_id'],
            'measured_type' => $measurement['measured_type'],
            'dataset_id' => $measurement['dataset_id'],
            'bibreference_id' => $measurement['bibreference_id'],
            'notes' => isset($measurement['notes']) ? $measurement['notes'] : null,
            'link_id' => isset($measurement['link_id']) ? $measurement['link_id'] : null,
            'parent_id' => isset($measurement['parent_id']) ? $measurement['parent_id'] : null,
            'location_id' => isset($measurement['location_id']) ? $measurement['location_id'] : null,
        ];
        $allow_duplicated = 0;
        if (isset($measurement['duplicated'])) {
                $allow_duplicated = (int) $measurement['duplicated'];
        }      
        //check for duplicated measurements
        $registry = $record;
        $registry['value'] = $measurement['value'];
        $registry['date_month'] = $measurement['date_month'];
        $registry['date_day'] = $measurement['date_day'];
        $registry['date_year'] = $measurement['date_year'];
        $exists_duplicated = Measurement::checkDuplicateMeasurement($registry);
        if ($exists_duplicated>0 and $allow_duplicated<=$exists_duplicated) {
            $this->skipEntry($measurement, Lang::get('messages.duplicated_measurement',[
                'nrecords' => $exists_duplicated,
                'duplicated' => ($exists_duplicated+1),
            ]));
            return false;
        }
        /*store record */
        $new_measurement = new Measurement($record);
        $new_measurement->setDate($measurement['date_month'], $measurement['date_day'],$measurement['date_year']);
        $new_measurement->save();   
        $persons = $measurement['persons'];
        $new_measurement->collectors()->attach($persons);        
        $new_measurement->valueActual = $measurement['value'];
        $new_measurement->save();
        $this->affectedId($new_measurement->id);
        return;
    }
}
