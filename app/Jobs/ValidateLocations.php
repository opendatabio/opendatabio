<?php
/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;
use App\Models\Location;
use App\Models\LocationRelated;
use App\Models\ODBFunctions;
use Spatie\SimpleExcel\SimpleExcelReader;
use Spatie\SimpleExcel\SimpleExcelWriter;
use Storage;
use DB;
use File;
use Lang;
use Illuminate\Support\Arr;
use Log;

class ValidateLocations extends LocationsJobs
{
    /**
     * Execute the job.
     */
    public function inner_handle()
    {

        $data = $this->extractEntrys();
        //$this->appendLog(serialize($data));
        //return false;
        $hasfile = $this->userjob->data['data'];
        /* if a file has been uploaded */
        if (isset($hasfile['filename'])) {
            $filename = $hasfile['filename'];
            $filetype = $hasfile['filetype'];
            $path = storage_path('app/public/tmp/'.$filename);
            /* this will be a lazy collection to minimize memory issues*/
            $howmany = SimpleExcelReader::create($path)->getRows()->count();
            $this->userjob->setProgressMax($howmany);
            /* I have to do twice, not understanding why loose the collection if I just count on it */
            $data = SimpleExcelReader::create($path)->getRows();
        } else {
            /* this has recieved a json */
            if (!$this->setProgressMax($data)) {
                return;
            }
        }
        /* store file with results */
        $jobid = $this->userjob->id;
        $filename = "job-".$jobid."_locationValidation.csv";
        $path = 'app/public/downloads/'.$filename;
        $writer = SimpleExcelWriter::create(storage_path($path));                
        foreach ($data as $location) {
            if ($this->isCancelled()) {
                break;
            }            
            $this->userjob->tickProgress();
            $newlocs  =$this->validateData($location);
            $writer->addRows($newlocs);
        }
         //set progress to max if got here
         //LOG THE FILE FOR USER DOWNLOAD
         $today = now();
         $tolog = "The following columns were added to the original data with the results of your search:<br><ul><li>withinLocationName</li><li>withinLocationParent</li><li>withinLocationCountry</li><li>withinLocationHigherGeography</li><li>withinLocationID</li><li>withinLocationType</li><li>searchObs</li></ul><br>
         More than one result per query may exist, when the point is also part of a conservation area, indigenous territory or environmental layer.<br><a href='".url('storage/downloads/'.$filename)."' download >".$filename."</a>";
         $this->appendLog($tolog);
         $this->userjob->progress = $this->userjob->progress_max;
         $this->userjob->save();
    }

    protected function validateData($location)
    {    
        $newlocations = [];
        $empty = [
            'withinLocationName' => '',
            'withinLocationParent' => '',            
            'withinLocationCountry' => '',
            'withinLocationHigherGeography' => '',
            'withinLocationType' => '',
            'withinLocationID' => '',
            'searchObs' => '',            
        ];  
        $baseloc = array_merge($location,$empty);
        $found = [];
        if ($this->hasRequiredKeys(['latitude', 'longitude'], $location)) {
            if ($this->validateGeom($location,$oldlocation=null,$justvalidation=true)) {            
                $guessedParent = $this->guessParent($location['geom'],999);
                $detected_related = Location::detectRelated($location['geom'],999,true);
                if ($detected_related != null) {
                    $found = array_unique(array_merge($found,$detected_related));
                }
                if ($guessedParent) {
                    $found[] = $guessedParent;
                }                
            }
        }
        if (count($found)) {
            foreach($found as $id) {
                $bl = $baseloc;
                $loc = Location::findOrFail($id);
                $bl['withinLocationName'] = $loc->name;
                $bl['withinLocationParent'] = $loc->parentName;
                $bl['withinLocationCountry'] = $loc->country;
                $bl['withinLocationID'] = $loc->id;
                $bl['withinLocationHigherGeography'] = $loc->higherGeography;
                $bl['withinLocationType'] = $loc->country_adm_level;
                $newlocations[] = $bl;
            }
        } else {
            $baseloc['searchObs'] = "This location was not found";
            $newlocations[] = $baseloc;
        }
        return $newlocations;
    }

}
