<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Location;
use App\Models\LocationRelated;
use App\Models\ODBFunctions;
use Spatie\SimpleExcel\SimpleExcelReader;
use Spatie\SimpleExcel\SimpleExcelWriter;
use \JsonMachine\Items;

use Storage;
use DB;
use File;
use Lang;
use Illuminate\Support\Arr;
use Log;

class ImportLocations extends LocationsJobs
{
    /**
     * Execute the job.
     */
    public function inner_handle()
    {

        $data = $this->extractEntrys();
        //$this->appendLog(serialize($data));
        //return false;

        $hasfile = $this->userjob->data['data'];
        $geojsonFile = false;
        $parentIgnore = false;
        /* if a file has been uploaded */
        if (isset($hasfile['filename'])) {
          $filename = $hasfile['filename'];
          $filetype = $hasfile['filetype'];
          $parentIgnore = $hasfile['parent_options'] == 'ignore' ? true : false;

          $path = storage_path('app/public/tmp/'.$filename);

          /*if the file is a geojson collection extract data */
          if (mb_strtolower($filetype) == 'geojson') {
            $geojsonFile = true;
            try {
               /* this will run outof memory for large files
                $json = json_decode(file_get_contents($path),true);
                JsonMachine does not permit total counts however
               */
               $data = Items::fromFile($path,['pointer' => '/features']);
            } catch (\Exception $e) {
               $this->setError();
               $this->appendLog('Exception '.$e->getMessage());
               return;
            }
            /* this is faking the progress status
             because JsonMachine does not allow total counts */
            $howmany = 100;
            $this->userjob->setProgressMax($howmany);
          } else {
            /* this will be a lazy collection to minimize memory issues*/
            $howmany = SimpleExcelReader::create($path,$filetype)->getRows()->count();
            $this->userjob->setProgressMax($howmany);
            /* I have to do twice, not understanding why loose the collection if I just count on it */
            $data = SimpleExcelReader::create($path,$filetype)->getRows();
          }
        } else {
          /* this has recieved a json */
          if (!$this->setProgressMax($data)) {
              return;
          }
        }
        $this->affectedModel(Location::class);

        /* first validate and save to import if valid
        * save by adm_level and the import from there
        * will minimize errors by ordering the data by admin level */
        $filesSaved = [];
        $writers = [];
        $jobId = $this->userjob->id;

        /* double time */
        $howmany = ($this->userjob->progress_max)*2;
        $this->userjob->setProgressMax($howmany);
        $counter = 1;
        foreach ($data as $location) {             
             if ($geojsonFile) {
               $location = self::parseGeoJsonFeature($location);
               if ($counter<50) {
                  $this->userjob->tickProgress();
               }
             } else {
               if (isset($location['geojson'])) {
                  $location =  self::parseGeoJsonFeature($location['geojson']);
               }
               $this->userjob->tickProgress();
             }

            if ( null === $location) {
              continue;
            }

            if ($this->isCancelled()) {
                break;
            }
            $nvalues = count($location);
            $admLevel = $location['adm_level']."_".$nvalues;
            $filename = "job-".$jobId."_locationImport_admLevel-".$admLevel.".csv";
            $path = 'app/public/downloads/'.$filename;
            $key = (string) $admLevel;
            if (!isset($writers[$key])) {
              $writer = SimpleExcelWriter::create(storage_path($path));
              $writers[$key] = $writer;
            } else {
              $writer = $writers[$key];
            }
            if ($parentIgnore) {
              $location['parentIgnore'] = 1;
            }

            $writer->addRow($location);

            $filesSaved[$filename] = $location['adm_level'];
            $counter++;
        }
        //$filesSaved = array_unique($filesSaved);
        // sort files by adm_level
        $filesSaved = Arr::sort($filesSaved);
        $filesSaved =  array_unique(array_keys($filesSaved));
        //restart counter
        $this->userjob->progress_max = ($counter*2);
        $this->userjob->progress = $counter;
        $this->userjob->save();

        // import records
        foreach ($filesSaved as $filename) {
            $path = storage_path('app/public/downloads/'.$filename);
            $data = SimpleExcelReader::create($path,'csv')->getRows();
            foreach($data as $location) {

                if ($this->validateData($location)) {
                  try {
                    $this->import($location);
                  } catch (\Exception $e) {
                    $this->setError();
                    $this->appendLog('Exception '.$e->getMessage().' at '.$e->getFile().'+'.$e->getLine().' on location '.$location['name']);
                  }
                }
                $this->userjob->tickProgress();
            }
            if ($this->isCancelled()) {
               break;
            }
        }
        foreach ($filesSaved as $filename) {
            $path = storage_path('app/public/downloads/'.$filename);
            File::delete($path);
        }
        //set progress to max if got here
        $this->userjob->progress = $this->userjob->progress_max;
        $this->userjob->save();
    }

    protected function validateData(&$location)
    {
        $locationLog = $location;
        $locationLog['geom'] = isset($location['geom']) ? substr($locationLog['geom'],0,20) : null;

        if (!$this->hasRequiredKeys(['name', 'adm_level'], $location)) {
            return false;
        }
        if (!$this->validateAdmLevel($location)) {
            $this->skipEntry($locationLog,'adm_level is not valid');
            return false;
        }
        if (!$this->validateAltitude($location)) {
          return false;
        }
        if (!$this->validateInformedParent($location)) {
            return false;
        }
        if (!$this->validateDimensions($location)) {
            return false;
        }
        if (!$this->validateGeom($location)) {
            return false;
        }
        if (!$this->validateParent($location)) {
            return false;
        }
        //second pass, for cases when parent is not informed but was detected above
        if (!$this->validateDimensions($location)) {
            return false;
        }
        if (!$this->validateOtherParents($location)) {
            return false;
        }
        if (!$this->adjustAdmLevel($location)) {
            return false;
        }
        return true;
    }





    public function import($location)
    {     
        $name = $location['name'];
        $adm_level = $location['adm_level'];
        $geom = $location['geom'];
        $parent = $location['parent'];
        //$uc = $location['uc'];
        $related_locations = isset($location["related_locations"]) ? $location['related_locations'] : null;
        $altitude = null;
        if (isset($location['altitude']) and !empty($location['altitude'])) {
          $altitude = $location['altitude'];
        }

        $datum = null;
        if (isset($location['datum']) and !empty($location['datum'])) {
          $datum = $location['datum'];
        }

        $notes = null;
        if (isset($location['notes']) and !empty($location['notes'])) {
          $notes = $location['notes'];
        }

        $startx = null;
        if (isset($location['startx']) and ($location['startx']== '0' or $location['startx']>0)) {
          $startx = $location['startx'];
        }

        $starty = null;
        if (isset($location['starty']) and ($location['starty']== '0' or $location['starty']>0)) {
          $starty = $location['starty'];
        }
        $x = null;
        if (isset($location['x']) and ($location['x'])>0) {
          $x = $location['x'];
        }

        $y = null;
        if (isset($location['y']) and ($location['y'])>0) {
          $y = $location['y'];
        }

        $geojson = null;
        if (isset($location['geojson']) and !empty($location['geojson'])) {
          $geojson = $location['geojson'];
        }
        $aspoint =0;
        if (isset($location['as_point'])) {
          $aspoint = $location['as_point'];
        }
        $country_code = null;
        if ($parent>1) {
          $country = DB::select(DB::raw("SELECT Child.id,Parent.name,Parent.country_code FROM locations as Parent, locations as Child WHERE Child.lft BETWEEN Parent.lft AND Parent.rgt AND Parent.adm_level=2 AND Child.id=".$parent));          
          $country_code = $country[0]->country_code ? $country[0]->country_code : null;          
        } elseif($location['adm_level']==2) {
          $countries = Lang::get('countries',[],'en');
          $country_code = array_search(strtolower($name), array_map('strtolower', $countries));
          if (!$country_code) {
            $this->skipEntry($location,"Could not find a country_code in ODB for this country");
            return null;
          }
        }
        
        $geom_type = mb_strtoupper(DB::select(DB::raw('SELECT ST_GeometryType(ST_GeomFromText("'.$geom.'")) as gg'))[0]->gg);
        
        
        $centroid_raw = DB::select(DB::raw('SELECT ST_AsText(ST_Centroid(ST_GeomFromText("'.$geom.'"))) as gg'))[0]->gg;
        if ($geom_type=="POINT") {
          $start_point = $geom;
        } elseif ($geom_type=="LINESTRING") {
          $start_point = DB::select(DB::raw('SELECT ST_AsText(ST_StartPoint(ST_GeomFromText("'.$geom.'"))) as gg'))[0]->gg;
        } elseif ($geom_type=="POLYGON") {
          $start_point = DB::select(DB::raw('SELECT ST_AsText(ST_StartPoint(St_ExteriorRing(ST_GeomFromText("'.$geom.'")))) as gg'))[0]->gg;
        } elseif ($geom_type=="MULTIPOLYGON") {
          $start_point = $centroid_raw;
        }
        $area_raw = null;
        if (in_array($geom_type,["POLYGON","MULTIPOLYGON"])) {
          $area_raw = DB::select(DB::raw('SELECT ST_AREA(ST_GeomFromText("'.$geom.'")) as gg'))[0]->gg;
        }
           
        $location = new Location([
            'name' => $name,
            'adm_level' => $adm_level,
            'altitude' => $altitude,
            'datum' => $datum,
            'notes' => $notes,
            'startx' => $startx,
            'starty' => $starty,
            'x' => $x,
            'y' => $y,
            'parent_id' => $parent,
            'geojson' => $geojson,
            'as_point' => $aspoint,
            'country_code' => $country_code,
            'start_point' => $start_point,
            'centroid_raw' => $centroid_raw,
            'geom_type' => $geom_type,
            'area_raw' => $area_raw,
        ]);
        $location->geom = $geom;
        $location->save();
        if ($related_locations) {
            foreach ($related_locations as $related_id) {
                $related = new LocationRelated(['related_id' => $related_id]);
                $location->relatedLocations()->save($related);
            }
        }
        $this->affectedId($location->id);

        $fixed = Location::fixPathAndRelated($location->id);
        return;
    }


}
