<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Voucher;
use App\Models\Location;
use App\Models\Individual;
use App\Models\Dataset;
use App\Models\ODBFunctions;
use App\Models\Biocollection;
use App\Models\BibReference;
use Illuminate\Http\Request;

use Lang;
use Auth;



class UpdateVouchers extends ImportCollectable
{
    private $requiredKeys;

    /**
     * Execute the job.
     */
    public function inner_handle()
    {
        $data = $this->extractEntrys();
        if (!$this->setProgressMax($data)) {
            return;
        }
        $this->affectedModel(Voucher::class);

        foreach ($data as $registry) {
            if ($this->isCancelled()) {
                break;
            }
            $this->userjob->tickProgress();

            if ($this->validateData($registry)) {
                try {
                    $this->updateVoucher($registry);
                } catch (\Exception $e) {
                    $this->setError();
                    $this->appendLog('Exception '.$e->getMessage().' at '.$e->getFile().'+'.$e->getLine());
                }
            }
        }
    }

    protected function validateData(&$registry)
    {
        $theid = isset($registry['voucher_id']) ? $registry['voucher_id'] : (isset($registry['id']) ? $registry['id'] : null);
        if (!isset($theid)) {
          $this->skipEntry($registry, 'Which Voucher? You must provide the voucher id to update!');
          return false;
        }
        $thevoucher = Voucher::findOrFail($theid);
        if ($thevoucher->count()==0) {
          $this->skipEntry($registry, 'Voucher with id '.$theid.' not found. Nothing to update.');
          return false;
        }
        if(!Auth::user()->can('update',$thevoucher)) {
          $this->skipEntry($registry, 'You are not authorized to update this record!');
          return false;
        }
        $registry['id'] = $theid;


        if (isset($registry['dataset'])) {
          $valid = ODBFunctions::validRegistry(Dataset::select('id'),$registry['dataset'],['id','name']);
          if (null === $valid) {
              $this->skipEntry($registry, 'dataset'.' '.$registry['dataset'].' was not found in the database');
              return false;
          }
          //the user has authorization to add records to this dataset?
          $user = Auth::user();
          if (!$valid->isAdmin($user) and !$valid->isCollab($user)) {
            $this->skipEntry($registry, 'You do not have permissions for dataset '.$valid->name);
            return false;
          }
          $registry['dataset'] = $valid->id;
        }
        /* if individual_id is informed and is different from current id, validate */
        if (!$this->validateIndividual($registry)) {
            return false;
        }

        $hascollector = false;
        $oldcollector = $thevoucher->collectors->pluck('id')->toArray();
        $collectors = $this->extractCollectors('Voucher', $registry, 'collector');
        if (is_array($collectors) and count($collectors)==0) {
          $this->skipEntry($registry, ' Informed collector'.' '.$registry['collector'].' was not found in the database');
          return false;
        } elseif (is_array($collectors)) {
          $hascollector = true;
          $registry['collector'] = $collectors;
        }

        //if collector is informed, then number is mandatory if does not exist
        $oldnumber = isset($thevoucher->number) ? $thevoucher->number : $thevoucher->individual->tag;
        $hasnumber   = isset($registry['number']) ? ((null != $registry['number']) ? $registry['number'] : null) : null;
        if ($hascollector and !$hasnumber and !$oldnumber) {
          $this->skipEntry($registry, 'Because you informed the collector you must also inform number. This voucher has no old number');
          return false;
        } elseif ($hasnumber) {
          /* if number informed, only accept if has a collector, old or new */
          if (!$hascollector and count($oldcollector)==0) {
            $registry['number']  = null;
          }
        }

        //validate date if informed
        $this->extractDate($registry);
        $date = isset($registry['date']) ? $registry['date'] : null;
        if ($date!=null) {
          /*date can only be informed if either has new or old collectors */
          if (!$hascollector and count($oldcollector)==0) {
            $this->skipEntry($registry, 'You informed a date to update, but lack a collector and number');
            return false;
          }
          if (!(Voucher::checkDate($date))) {
            $this->skipEntry($registry,'Informed date is invalid!');
            return false;
          }
        }
        if (!$this->validateBiocollection($registry,$mandatory=false)) {
            $this->skipEntry($registry,'BioCollection informed was not found in the database');
            return false;
        }
        if (!$this->validateBibReferences($registry)) {
          $this->skipEntry($registry,'Bibreferences informed were not found in the database');
          return false;
        }
        return true;
    }



    public function validateIndividual(&$registry)
    {
      $hasindividual = isset($registry['individual']) ? $registry['individual'] : (isset($registry['individual_id']) ? $registry['individual_id'] : null);
      if (null===$hasindividual) {
        /* no update to individual requested */
        return true;
      }
      $individual = $hasindividual;
      if (((int)($individual))>0) {
            $ref = Individual::where('id',$individual);
      } else {
              $ref = Individual::whereRaw('odb_ind_fullname(id,tag) like "'.$individual.'"');
      }
      if ($ref->count()==1) {
          $registry['individual_id'] = $ref->get()->first()->id;
          return true;
      }
      $this->skipEntry($registry, ' Individual '.$individual.' not found in the database');
      return false;
    }


    public function updateVoucher($registry)
    {

        /*create a update request */
        $thevoucher = Voucher::findOrFail($registry['id']);

        $keys_mandatory = ['individual','biocollection'];
        $keys_other = ['biocollection_type','biocollection_number','dataset','collector','number','notes','date'];

        $curdate = explode("-",$thevoucher->date);
        if (count($curdate)==3) {
          $curdate = [(int) $curdate[1], (int) $curdate[2],(int) $curdate[0]];
        } else {
          $curdate = null;
        }

        /* clone original values if not provided */
        $update_request = [
          'from_the_api' => 1,
          'individual_id' => isset($registry['individual_id']) ? ((int) $registry['individual_id']) : $thevoucher->individual_id,
          'biocollection_id' => isset($registry['biocollection_id']) ? ((int) $registry['biocollection_id']) : $thevoucher->biocollection_id,
          'biocollection_type' => isset($registry['biocollection_type']) ? ((int) $registry['biocollection_type']) : $thevoucher->biocollection_type,
          'dataset_id' =>  isset($registry['dataset_id']) ? ((int) $registry['dataset_id']) : $thevoucher->dataset_id,
          'biocollection_number' => isset($registry['biocollection_number']) ? $registry['biocollection_number'] : $thevoucher->biocollection_number,
          'number' =>  isset($registry['number']) ? ((string)$registry['number']) : $thevoucher->number,
          'collector' => isset($registry['collector']) ? $registry['collector'] : $thevoucher->collectors->pluck('person_id')->toArray(),
          'notes' => isset($registry['notes']) ? (string) $registry['notes'] :  $thevoucher->notes,
          'date' => isset($registry['date']) ? $registry['date'] : ((isset($registry['number']) or $thevoucher->collectors->count()) ? $curdate : null),
          'bibreferences' => isset($registry['bibreferences']) ? $registry['bibreferences'] : $thevoucher->bibreferences->pluck('id')->toArray(),
        ];
        $update_request = array_filter($update_request,function($a) { return null !== $a;});


        //transform info in a request
        $saverequest = new Request;
        $saverequest = $saverequest->merge($update_request);
        //$this->skipEntry($saverequest->all(),'error here');
        //return;
        //store the record, which will result in the individual
        $updatedvoucher = app('App\Http\Controllers\VoucherController')->update($saverequest,$thevoucher->id);
        if (is_string($updatedvoucher)) {
          $this->skipEntry($registry,'This voucher could not be updated. Possible errors may be: '.$savedvoucher);
          return ;
        }
        $this->affectedId($thevoucher->id);
        return;
    }
}
