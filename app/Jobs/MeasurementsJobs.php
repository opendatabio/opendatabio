<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Measurement;
use App\Models\Location;
use App\Models\Taxon;
use App\Models\Person;
use App\Models\Individual;
use App\Models\Voucher;
use App\Models\ODBFunctions;
use App\Models\ODBTrait;
use App\Models\BibReference;
use App\Models\Summary;
use App\Models\Dataset;
use App\Models\ExternalAPIs;
use Auth;
use Lang;
use Log;

/* contains functions used in Import and Update Measurements */
class MeasurementsJobs extends ImportCollectable
{


  public function validateMeasurementParent(&$registry,$oldregistry=null)
  {
    $parent = (isset($registry['parent']) ? $registry['parent'] : (isset($registry['parent_id']) ? $registry['parent_id'] : (isset($registry['parent_measurement']) ? $registry['parent_measurement'] : null)));        
    if (!$parent) {
      $registry['parent_id'] = $oldregistry ? $oldregistry->parent_id : null;
      return true;
    }
    if ($parent) {
      $np = (int) $parent;
      if ($np==0) {
        $this->skipEntry($registry,' Parent must be a single numeric value. The id of a measurement for the same object and same date.');
        return false;
      }
      $is_valid = Measurement::withoutGlobalScopes()->findOrFail($np);
      if ($is_valid) {
        $condition1 = $registry['measured_id']==$is_valid->measured_id;
        $condition2 = $registry['measured_type']==$is_valid->measured_type;
        $colldate = [
          $registry['date_year']==$is_valid->year,
          $registry['date_month']==$is_valid->month,
          $registry['date_day']==$is_valid->day,         
        ];
        $condition = $condition1 and $condition2 and array_sum($colldate)==3;
      } 
      if (!$condition) {
        $this->skipEntry($registry,"The indicated parent_measurement is not valid. Not found or not from the same object and same date.");
        return false;
      } 
      $registry['parent_id'] = $np;         
    } 
    return true;    
  }

  public function validatePerson(&$registry,$oldregistry=null)
  {
      $person = isset($registry['person_id']) ? $registry['person_id'] : (
          isset($registry['person']) ? $registry['person'] : (isset($this->header['person_id']) ? $this->header['person_id'] : (isset($this->header['person']) ? $this->header['person'] : null))
        );
      if (null == $person and isset($registry['persons'])) {
        $person = $registry['persons'];
      }      
      if (null == $person and isset($oldregistry)) {
        $registry['persons'] = $oldregistry->collectors->pluck('persons.id')->toArray();
        return true;
      }
      if (null == $person) {
        $this->skipEntry($registry,' At least 1 person required as measurer');
        return false;
      }
      if (!isset($registry['persons'])) {
        $registry['persons'] = $person;
      }
      $person = $this->extractCollectors('Header', $registry, 'persons');
      if (null === $person) {
          $this->skipEntry($registry," None of the informed  'persons' responsible for the measurement were found in the 'persons' table.");
          return false;
      }
      $registry['persons'] = $person;
      return true;
  }


  protected function validateObjectType(&$registry,$oldregistry=null)
  {
      $object_type = isset($registry['measured_type']) ? $registry['measured_type'] : 
      (isset($registry['object_type']) ? $registry['object_type'] : (
        isset($this->header['object_type']) ? $this->header['object_type'] : 
        null)
      );
      //Log::info('OBJECT-TYPE HERE'.$object_type);      
      if ($object_type!=null) {
        $types =  ODBTrait::OBJECT_TYPES;
        if (in_array($object_type,$types)) {
          $registry['measured_type'] = $object_type;
          return true;
        }
        $object_type = preg_replace("/App\\\Models\\\/","",$object_type);
        $object_type = trim(ucfirst(mb_strtolower($object_type))); 
        $simple_types = preg_replace("/App\\\Models\\\/","",$types);
        if (in_array($object_type,$simple_types)) {
          $key = array_search($object_type,$simple_types);
          $registry['measured_type'] = $types[$key];
          return true;
        }
      }
      //Log::info($oldregistry->toArray());
      if ($oldregistry) {
        $registry['measured_type'] = $oldregistry->measured_type;
        //Log::info($oldregistry->measured_type);
        return true;
      }
      return false;
  }

  protected function validateMeasurementLocation(&$registry,$oldregistry=null)
  {
    $location = isset($registry['location_id']) ? $registry['location_id'] : (
      isset($registry['location']) ? $registry['location'] : null
    );
    if (!$location)
    {
      $registry['location_id'] = $oldregistry ? $oldregistry->location_id : null;
      return true;
    }
    $type = str_replace("App\Models\\","",$registry['measured_type']);
    if ($type!="Taxon" and $location) {
      $this->appendLog("WARNING: Location can only be associated to measurements for measured Taxon. Type is ".$type.", so location was ignored.");
      return true;
    }
    $fields = ['id', 'name'];
    $query = Location::select(['id', 'name']);
    $valid = ODBFunctions::validRegistry($query, $location, $fields);    
    if (!$valid) {
      $this->skipEntry($registry, "Location informed ".$location." was not found in the database");
      return false;
    }
    $registry['location_id'] = $valid->id;
    return true;
  }


  protected function validateObject(&$registry,$oldregistry=null)
  {
      if (!$this->validateObjectType($registry,$oldregistry=$oldregistry)) {
          $this->skipEntry($registry, "Invalid object_type");
          return false;
      }
      $object_type = $registry['measured_type'];
      $object_id = isset($registry['object_id']) ? $registry['object_id'] : (isset($registry['measured_id']) ? $registry['measured_id'] : null);
      if (null == $object_id and $oldregistry!==null) {
        $registry['measured_id'] = $oldregistry->measured_id;        
        return true;
      }
      if (null == $object_id) {
        $this->skipEntry($registry, "Missing object_id");
        return false;
      }
      $valid = app($object_type)::where('id', $object_id);
      if ($valid->count()==1) {
          $registry['measured_id'] = $valid->get()->first()->id;
          return true;
      } else {
          $this->skipEntry($registry, "The informed Measured object was not found in the database or you do not have permissions");
          return false;
      }
  }


  //duplicated from ImportCollectable
  protected function validateMeasurementDataset(&$registry,$oldregistry=null)
  {
      $header = $this->header;
      $dataset = isset($registry['dataset_id']) ? $registry['dataset_id'] : (isset($registry['dataset']) ? $registry['dataset'] : null);
      $header = isset($header['dataset_id']) ? $header['dataset_id'] : (isset($header['dataset']) ? $header['dataset'] : null);
      if (null == $dataset and $header == null) {
        if (isset($oldregistry)) {
          $registry['dataset_id'] = $oldregistry->dataset_id;
          return true;
        }
      }
      if (null == $dataset and $header != null) {
          $dataset = $header;
      }
      if (null != $dataset) {
          $valid = ODBFunctions::validRegistry(Dataset::select('id'),$dataset,['id','name']);
          if (null === $valid) {
              $this->skipEntry($registry, 'dataset'.' '.$dataset.' was not found in the database');
              return false;
          }
          /* the user is authorized in this dataset */
          $user = Auth::user();
          $editable_dts = $user->editableDatasets()->pluck('id')->toArray();          
          if (!in_array($valid->id,$editable_dts)) {  
            $this->skipEntry($registry, ' You have no permissions to add data to dataset '.$valid->name);
            return false;
          }
          $registry['dataset_id'] = $valid->id;
          return true;
      }
      /* importing data without informing dataset */
      /* will not arrive here on updates */
      if (isset(Auth::user()->defaultDataset)) {
        $registry['dataset_id'] = Auth::user()->defaultDataset->id;
        return true;
      } else {
        return false;
      }
  }

  //the informed dataset is open access while the measurement dataset is restricted access.
  //making this data not completely open access
  //prevent importation ad warn
  public function validateDatasetPolicies($registry)
  {
      //measurements must have a dataset, although measured object need not..
      $object = app($registry['measured_type'])::where('id', $registry['measured_id']);
      $parent_dataset = isset($object->get()->first()->dataset) ? $object->get()->first()->dataset_id : null;
      $types =  ODBTrait::NON_DATASET_OBJECT_TYPES;
      if (null == $parent_dataset AND in_array($registry['measured_type'],$types))
      {
         //locations and taxons objects should not have datasets defined.
         return true;
      }
      $parent = Dataset::findOrFail($parent_dataset);
      $current = Dataset::findOrFail($registry['dataset_id']);
      $parent_privacy = $parent->privacy;
      $current_privacy = $current->privacy;
      if ($current_privacy>=Dataset::PRIVACY_REGISTERED and $parent_privacy<Dataset::PRIVACY_REGISTERED) {
        $this->skipEntry($registry,'Privacy for the dataset '.$parent->name.' to which belongs the measured object has restricted access privacy, while the dataset of the measurement is open access. Therefore, the access is not complete and this must be prevented. In this case both should be open acess. Else, restrict the privacy of dataset '.$current->name);
        return false;
      }
      return true;
  }

  /////////////////////////////
  protected function validateMeasurements(&$registry,$oldregistry=null)
  {
      $valids = array();
      //check that trait exists;
      $odbtrait = ODBFunctions::validRegistry(ODBTrait::with('categories')->select('*'), $registry['trait_id'], ['id', 'export_name']);
      if (!$odbtrait->id) {
        $this->skipEntry($registry,' Trait_id for trait '.$odbtrait->id.' not found, this measurement will be ignored.');
        return false;
      }
      $registry['trait_id'] = $odbtrait->id;
      ///////////////
      //can the trait be linked to the object type?
      $teste = !$odbtrait->valid_type( $registry['measured_type']);
      if ($teste)
      {
        $this->skipEntry($registry,Lang::get('messages.invalid_trait_type_error'));
        return false;
      }
      //must test for parent trait requirement
      //if trait has a parent trait, a measurement for it must exist
      if ($odbtrait->parent_id) {
        $has_measurement = Measurement::withoutGlobalScopes()->where('trait_id',$odbtrait->parent_id);
        $has_measurement = $has_measurement->where('measured_id',$registry['measured_id']);
        $has_measurement = $has_measurement->where('measured_type',$registry['measured_type']);
        $year =str_pad((int)$registry['date_year'], 4, '0', STR_PAD_LEFT);
        $month = str_pad((int)$registry['date_month'], 2, '0', STR_PAD_LEFT);
        $day = str_pad((int)$registry['date_day'], 2, '0', STR_PAD_LEFT);
        $dt = $year."-".$month."-".$day;
        $has_measurement = $has_measurement->where('date',$dt);
        if ($has_measurement->count()==0) {
          $parent = $odbtrait->parentTrait->export_name;
          $this->skipEntry($registry,'Storing a measurement for this Trait requires having a measurement for trait '.$parent.' on the same date for the same object. None found!');
          return false;          
        }
      }

      ///////////////////////
      if ($odbtrait->type==ODBTrait::LINK and !isset($registry['link_id']) and !isset($oldregistry)) {
        $this->skipEntry($registry,' Link_id required for trait '.$odbtrait->id.' key not found, this measurement will be ignored.');
        return false;
      } elseif ($odbtrait->type==ODBTrait::LINK and !isset($registry['link_id']) and isset($oldregistry)) {
        $registry['link_id'] = $oldregistry->link_id;
      }
      if ($odbtrait->type != ODBTrait::LINK and !isset($registry['value']) and !isset($oldregistry)) {
        $this->skipEntry($registry,' There is no value to import for this measurement');
        return false;
      }
      //this validation gets proper values
      if (!$this->validateValue($odbtrait,$registry,$oldregistry)) {
        //$this->appendLog('WARNING: Value for trait '.$odbtrait->id.' is invalid, this measurement will be ignored.'.serialize($registry));
        return false;
      }
      //this makes the basic validation of values
      $measurement_id = $oldregistry ? $oldregistry->id : null;
      $valid = Measurement::valueValidation($registry['value'],$odbtrait,$measurement_id);
      if (count($valid)>0) {
        $this->skipEntry($registry,implode("<br>",$valid));        
        return false;
      }
      return true;
  }



      public function validateColor($color) {
        if(preg_match("/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/", $color))
        {
            return true;
        }
        return false;
      }


      protected function validateValue($odbtrait,&$registry,$oldregistry=null)
      {
          $condition1 = !$odbtrait->link_type == ODBTrait::LINK;
          $condition2 = (empty($registry['value']) || (is_array($registry['value']) and count($registry['value'])==0));
          if ($condition1 and $condition2 and (null ===$oldregistry)) {
            $this->skipEntry($registry,'Value for '.$odbtrait->export_name.' is missing');
            return false;
          }
          if ($condition1 and $condition2 and isset($oldregistry)) {
            $registry['value'] = $oldregistry->rawValueActual;
            return true;
          }
          if (!$condition1 and !isset($registry['link_id']) and isset($oldregistry)) {            
            return true;
          }
          switch ($odbtrait->type) {
            case ODBTrait::QUANT_INTEGER:
            case ODBTrait::QUANT_REAL:
                if (!is_numeric($registry['value'])) {
                  $this->skipEntry($registry,'Value for '.$odbtrait->export_name.' must be numeric');
                  return false;
                }
                break;
            case ODBTrait::CATEGORICAL:
            case ODBTrait::ORDINAL:
                if (is_array($registry['value']) && count($registry['value'])>1) {
                   $this->skipEntry($registry,'Value for '.$odbtrait->export_name.' must have ONE category only');
                   return false;
                }
                if (!$this->validateCategories($odbtrait,$registry['value'])) {
                  return false;
                }
                break;
            case ODBTrait::CATEGORICAL_MULTIPLE:
                if (!$this->validateCategories($odbtrait,$registry['value'])) {
                  return false;
                }
                break;
            case ODBTrait::COLOR:
                if (!$this->validateColor($registry['value'])) {
                  $this->skipEntry($registry,'Value for '.$odbtrait->export_name.' color is invalid');
                  return false;
                }
                break;
            case ODBTrait::LINK:
                switch ($odbtrait->link_type) {
                  case (Taxon::class):
                    $taxon = Taxon::where('id','=',$registry['link_id']);
                    if ($taxon->count()==0) {
                      $this->skipEntry($registry,'Value for '.$odbtrait->export_name.' has and invalid Taxon link');
                      return false;
                    }
                    break;
                  case (Person::class):
                    $person = Person::where('id','=',$registry['link_id']);
                    if ($person->count()==0) {
                      $this->skipEntry($registry,'Value for '.$odbtrait->export_name.' has and invalid Person link');
                      return false;
                    }
                    break;
                  case (Voucher::class):
                    $voucher = Voucher::where('id','=',$registry['link_id']);
                    if ($voucher->count()==0) {
                      $this->skipEntry($registry,'Value for '.$odbtrait->export_name.' has and invalid Voucher link');
                      return false;
                    }
                    break;
               }
               if (isset($registry['value']) && !is_numeric($registry['value']) && !empty($registry['value'])) {
                 $this->skipEntry($registry,'Value for '.$odbtrait->export_name.' must be a number');
                 return false;
               }
               break;
            case ODBTrait::SPECTRAL:
               $registrys = explode(";",$registry['value']);
               if (count($registrys) != $odbtrait->value_length) {
                 $this->skipEntry($registry,'Value for '.$odbtrait->export_name.' must have '.$odbtrait->value_length." but it has ".count($registrys));
                 return false;
               }
               break;
           case ODBTrait::GENEBANK:
              $hasgenebank = ExternalAPIs::getGeneBankData($registry['value'],$db = 'nucleotide');
              if (null == $hasgenebank) {
                $this->skipEntry($registry,'Value for '.$odbtrait->export_name.'  '.$registry['value']." ".Lang::get('messages.genebank_not_found'));
                return false;
                break;
              }
              $accession= $registry['value'];
              $hasvalue = Measurement::withoutGlobalScopes()->whereHas('odbtrait',function($t){
                $t->where('type',ODBTrait::GENEBANK);
              })->where('value_a','like',$accession);
              if ($hasvalue->count()) {
                $this->skipEntry($registry,'Value for '.$odbtrait->export_name.' '.$registry['value']." ".Lang::get('messages.genebank_already_registered')." ".$hasvalue->first()->measured->rawLink());
                return false;
                break;
              }
          }
          return true;
      }


      public function validateCategories($odbtrait,&$value)
      {
          $cats = [];
          if (!is_array($value)) {
            if (strpos($value, '|') !== false) {
                $value_arr = explode('|', $value);
            } elseif (strpos($value, ';') !== false) {
                $value_arr = explode(';', $value);
            } elseif (strpos($value, ',') !== false) {
                $value_arr = explode(',', $value);
            } else {
              $value_arr = [$value];
            }
          } else {
            $value_arr = $value;
          }
          $msg = [];
          foreach ($value_arr as $key => $cat) {
            if (null != $cat) {
              $thetrait = clone $odbtrait;
              if (is_numeric($cat)) {
                 $valid = $thetrait->categories()->where('id',$cat);
              } else {
                $valid = $thetrait->categories()->whereHas('translations',function($tr) use($cat){ $tr->where('translation','like',$cat);});
              }
              if ($valid->count() == 1) {
                $cats[] = $valid->first()->id;
              } else {
                $msg[] = $cat.' is an invalid category for trait '.$thetrait->export_name;
              }
            }
          }
          if (count($msg)>0) {
            $this->appendLog('ERROR:'.implode(" | ",$msg));
            return false;
          }
          /* save ids and return valid */
          $value = $cats;
          return true;
      }

      public function extractMeasurementDate(&$registry,$oldregistry=null)
      {
        //validate date
        $date = isset($registry['date']) ? $registry['date'] : (isset($this->header['date']) ? $this->header['date'] : null);

        if (null == $date) {
          $year = isset($registry['date_year']) ? $registry['date_year'] : (isset( $registry['year']) ? $registry['year'] : null);
          $month = isset($registry['date_month']) ? $registry['date_month'] : (isset( $registry['month']) ? $registry['month'] : null);
          $day = isset($registry['date_day'])  ? $registry['date_day'] : (isset( $registry['day']) ? $registry['day'] : null);
          $date = ['date_month' => $month,'date_day' => $day,'date_year' =>$year];
          $date = array_filter($date);
          if (count($date)==0 and isset($oldregistry)) {
            $registry['date_year'] = $oldregistry->year;
            $registry['date_month'] = $oldregistry->month;
            $registry['date_day'] = $oldregistry->day;
            return true;
          }
        }
        if (is_string($date)) {
          if (preg_match("/\//",$date)) {
              $date = explode("/",$date);
              $date = ['date_month' => $date[1],'date_day' => $date[2],'date_year' =>$date[0]];
          } elseif (preg_match("/-/",$date)) {
              $date = explode("-",$date);
              $date = ['date_month' => $date[1],'date_day' => $date[2],'date_year' =>$date[0]];
          }
        } elseif (!is_array($date)) {
          if (get_class($date)==="DateTime") {
             $year = $date->format('Y');
             $day = $date->format('d');
             $month = $date->format('m');
             $date = ['date_month' => $month,'date_day' => $day,'date_year' =>$year];
           }
        }
        $date = array_filter($date);
        if (is_array($date) and count($date)>0) {
          $year = isset($date['date_year']) ? $date['date_year'] : (isset( $date['year']) ? $date['year'] : (isset($date[2]) ? $date[2] : null));
          $month = isset($date['date_month']) ? $date['date_month'] : (isset( $date['month']) ? $date['month'] : (isset($date[0]) ? $date[0] : null));
          $day = isset($date['date_day'])  ? $date['date_day'] : (isset( $date['day']) ? $date['day'] : (isset($date[1]) ? $date[1] : null));
          $registry['date_year'] = $year;
          $registry['date_month'] = $month;
          $registry['date_day'] = $day;
          return true;
        }

        
        return false;
      }

      protected function validateMeasurementBibReference(&$registry,$oldregistry=null)
      {
        $bibreference = isset($registry['bibreference_id']) ? $registry['bibreference_id']  : (isset($registry['bibreference']) ? $registry['bibreference'] : null);
        if (null == $bibreference) {
          $registry['bibreference_id'] = isset($oldregistry) ? $oldregistry->bibreference_id : null;
          return true;
        }
        if (is_numeric($bibreference)) {
          $valid = BibReference::where('id',$bibreference);
        } else {
          $valid = BibReference::whereRaw('odb_bibkey(bibtex) = ?', [$bibreference]);
        }
        if ($valid->count()) {
          $registry['bibreference_id'] = $valid->get()->first()->id;
          return true;
        }
        $this->skipEntry($registry,'Bibreference '.$bibreference.' not found in database');
        return false;
      }


}
