<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Individual;
use App\Models\Dataset;
use App\Models\ODBFunctions;
use Illuminate\Http\Request;
use Auth;
use Log;

class UpdateIndividuals extends ImportCollectable
{

    private $requiredKeys;
    /**
     * Execute the job.
     */
    public function inner_handle()
    {
      $data = $this->extractEntrys();
      /* this has recieved a json */
      if (!$this->setProgressMax($data)) {
          return;
      }
      $this->affectedModel(Individual::class);

      foreach ($data as $individual) {
            if ($this->isCancelled()) {
                break;
            }
            $this->userjob->tickProgress();
            if ($this->validateData($individual)) {
                try {
                    $this->updateIndividual($individual);
                } catch (\Exception $e) {
                    $this->setError();
                    $this->appendLog('Exception '.$e->getMessage().' at '.$e->getFile().'+'.$e->getLine());
                }
            }
        }
    }
    protected function validateData(&$registry)
    {
        $theid = isset($registry['individual_id']) ? $registry['individual_id'] : (isset($registry['id']) ? $registry['id'] : null);
        if (!isset($theid)) {
          $this->skipEntry($registry, 'Which Individual? You must provide the individual id to update!');
          return false;
        }
        $individual = Individual::findOrFail($theid);
        if ($individual->count()==0) {
          $this->skipEntry($registry, 'Individual with id '.$theid.' not found. Nothing to update.');
          return false;
        }
        if(!Auth::user()->can('update',$individual)) {
          $this->skipEntry($registry, 'You are not authorized to update this record!');
          return false;
        }
        $registry['id'] = $theid;

        if (isset($registry['dataset'])) {
          $valid = ODBFunctions::validRegistry(Dataset::select('id'),$registry['dataset'],['id','name']);
          if (null === $valid) {
              $this->skipEntry($registry, 'dataset'.' '.$registry['dataset'].' was not found in the database');
              return false;
          }
          //the user has authorization to add records to this dataset?
          $user = Auth::user();
          if (!$valid->isAdmin($user) and !$valid->isCollab($user)) {
            $this->skipEntry($registry, 'You do not have permissions for dataset '.$valid->name);
            return false;
          }
          $registry['dataset'] = $valid->id;
        }

        //collectors (at least a valid one, must be informed)
        $collectors = $this->extractCollectors('Individual', $registry, 'collector');
        //Log::info("extracted collectors");
        //Log::info($collectors);
        if (is_array($collectors) and count($collectors)==0) {
          $this->skipEntry($registry, 'collector'.' '.$registry['collector'].' was not found in the database');
          return false;
        } elseif (is_array($collectors)) {
          $registry['collector'] = $collectors;
        }


        //validate date if informed
        $this->extractDate($registry);
        $date = isset($registry['date']) ? $registry['date'] : null;
        if ($date!=null) {
          if (!(Individual::checkDate($date))) {
            $this->skipEntry($registry,'Informed date is invalid!');
            return false;
          }
        }

        $identification = null;
        $taxon = isset($registry['taxon_id']) ? $registry['taxon_id'] : (isset($registry['taxon']) ? $registry['taxon'] : null);
        $oldidentification = $individual->identification;
        $identification = $this->extractIdentification($registry,$oldidentification);
        if (null == $identification) {
          $this->skipEntry($registry,'Problem in the NEW IDENTIFICATION of this individual');
          return false;
        }
        $registry['identification'] = $identification;
      
        /* alternatively, the identification is from another individual */
        $hasidother = isset($registry['identification_individual']) ? ("" == $registry['identification_individual'] ? null : $registry['identification_individual']) : null;
        if (null != $hasidother and null == $identification) {
          $registry['identification_individual_id'] = null;
          $hasIndividual = Individual::where('id',$registry['identification_individual'])->orWhereRaw('odb_ind_fullname(individuals.id,individuals.tag) like "'.$registry['identification_individual'].'"');
          if ($hasIndividual->count()==1) {
            $registry['identification_individual_id'] = $registry->get()->first()->id;
          } else {
            $this->skipEntry($registry,'Problem in the identification_individual value for which a single match was notfound in the database');
            return false;
          }
        } elseif (null != $hasidother) {
          $this->appendLog(' WARNING: identification_individual value was informed but also a self identification. Therefore, self identification was used.');
        }

        return true;
    }

    public function updateIndividual($registry)
    {
        /*create a update request */
        $theindividual = Individual::findOrFail($registry['id']);
        /* fields have already been validate
        *  some old values must be retrieved if not informed
        *  for customValidate function to work
        */
        $curdate = explode("-",$theindividual->date);
        $curdate = [(int) $curdate[1], (int) $curdate[2],(int) $curdate[0]];
        //$curdate = array_filter($curdate);
        $update_request = [
          'from_the_api' => 1,  /* this to prevents the controller from redirecting */
          'id' => $registry['id'],
          'dataset_id' => isset($registry['dataset']) ? $registry['dataset'] : $theindividual->dataset_id,
          'tag' => isset($registry['tag']) ? ((string) $registry['tag']) : $theindividual->tag,
          'notes' => isset($registry['notes']) ? $registry['notes'] : null,
          'date' => isset($registry['date']) ? $registry['date'] : $curdate,
          'collector' =>isset($registry['collector']) ? $registry['collector'] : $theindividual->collectors->pluck('person_id')->toArray(),
          'identification_individual_id' => isset($registry['identification_individual_id']) ? $registry['identification_individual_id'] : null,
        ];
        if (isset($registry['identification'])) {
            $update_request['taxon_id'] = $registry['identification']['taxon_id'];
            $update_request['identifiers'] = $registry['identification']['identifiers'];
            $update_request['modifier'] = $registry['identification']['modifier'];
            $update_request['biocollection_id'] = $registry['identification']['biocollection_id'];
            $update_request['biocollection_reference'] = $registry['identification']['biocollection_reference'];
            $update_request['identification_notes'] = $registry['identification']['notes'];
            $update_request['identification_date'] = $registry['identification']['date'];
        }
        $update_request = array_filter($update_request,function($a) { return null !== $a;});

        //transform info in a request
        $saverequest = new Request;
        $saverequest = $saverequest->merge($update_request);
        //store the record, which will result in the individual
        $savedindividual = app('App\Http\Controllers\IndividualController')->update($saverequest, $theindividual->id);
        //if this is true, errors where found
        if (is_string($savedindividual)) {
          $this->appendLog("ERROR:  individual with tag #".$update_request['tag']." could not be updated: ".$savedindividual);
          return ;
        }

        // - Then create the related registries (for identification and collector), if requested
        $this->affectedId($theindividual->id);
        return;
    }
}
