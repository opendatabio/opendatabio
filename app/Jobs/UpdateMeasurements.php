<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Measurement;
use Illuminate\Http\Request;
use App\Models\ActivityFunctions;
use Auth;
use Log;
use Lang;

class UpdateMeasurements extends MeasurementsJobs
{
    /**
     * Execute the job.
     */
    public function inner_handle()
    {
        $data = $this->extractEntrys();
        if (!$this->setProgressMax($data)) {
            return;
        }
        $this->affectedModel(Measurement::class);
        foreach ($data as $registry) {
            if ($this->validateData($registry)) {
                // Arrived here: let's import it!!
                try {
                    $this->updateMeasurement($registry);
                } catch (\Exception $e) {                    
                    $this->setError();
                    $this->appendLog('Exception '.$e->getMessage().' at '.$e->getFile().'+'.$e->getLine().' on measurement '.$registry['measured_id'].$e->getTraceAsString());
                }
            }
            if ($this->isCancelled()) {
                break;
            }
            $this->userjob->tickProgress();
        }
    }

    protected function validateData(&$registry)
    {
        $theid = isset($registry['measurement_id']) ? $registry['measurement_id'] : (isset($registry['id']) ? $registry['id'] : null);
        if (!isset($theid)) {
          $this->skipEntry($registry, 'Which Measurement? You must provide the Measurement id to update!');
          return false;
        }
        $themeasurement = Measurement::findOrFail($theid);
        if ($themeasurement->count()==0) {
          $this->skipEntry($registry, 'Measurement with id '.$theid.' not found. Nothing to update.');
          return false;
        }
        if(!Auth::user()->can('update',$themeasurement)) {
          $this->skipEntry($registry, 'You are not authorized to update this record!');
          return false;
        }
        $registry['id'] = $theid;

        $registry['trait_id'] = isset($registry['trait_id']) ? $registry['trait_id'] : (isset($registry['trait']) ? $registry['trait'] : $themeasurement->trait_id);
        //$registry['object_id'] = isset($registry['object_id']) ? $registry['object_id'] : null;
        
        
        if (!$this->validatePerson($registry,$themeasurement)) {
            return false;
        }
        if (!$this->validateObject($registry,$themeasurement)) {
            return false;
        }
        if (!$this->validateMeasurementDataset($registry,$themeasurement)) {
              return false;
        }
        if (!$this->validateDatasetPolicies($registry,$themeasurement)) {
              return false;
        }
        if (!$this->extractMeasurementDate($registry,$themeasurement)) {
            return false;
        }
        if (!$this->validateMeasurements($registry,$themeasurement)) {
            return false;
        }
        if (!$this->validateMeasurementBibReference($registry,$themeasurement)) {
            return false;
        }
        if (!$this->validateMeasurementParent($registry,$themeasurement)) {
            return false;
        }
        if (!$this->validateMeasurementLocation($registry,$themeasurement)) {
            return false;
        }
        return true;
    }


    public function updateMeasurement($measurement)
    {
        $themeasurement = Measurement::findOrFail($measurement['id']);
        
        $current_categories = [];
        if ($themeasurement->categories->count()) {
          $current_categories = $themeasurement->categories->pluck('category_id')->toArray();
        }

        $record = [
            'trait_id' => $measurement['trait_id'],
            'measured_id' => $measurement['measured_id'],
            'measured_type' => $measurement['measured_type'],
            'dataset_id' => $measurement['dataset_id'],
            'bibreference_id' => $measurement['bibreference_id'],
            'notes' => isset($measurement['notes']) ? $measurement['notes'] : null,
            'link_id' => isset($measurement['link_id']) ? $measurement['link_id'] : null,
            'parent_id' => isset($measurement['parent_id']) ? $measurement['parent_id'] : null,
            'location_id' => isset($measurement['location_id']) ? $measurement['location_id'] : null,
        ];
        $themeasurement->update($record);
        $themeasurement->setDate($measurement['date_month'], $measurement['date_day'],$measurement['date_year']);
        $themeasurement->save();   
        $persons = $measurement['persons'];
        $current_persons = $themeasurement->collectors->pluck('id');
        $themeasurement->collectors()->detach();
        $themeasurement->collectors()->attach($persons);        
        $themeasurement->valueActual = $measurement['value'];
        $themeasurement->save();

        //log changes in categories
        $new_categories = [];
        if ($themeasurement->categories->count()) {
            $new_categories = $themeasurement->categories->pluck('category_id')->toArray();
        }
        ActivityFunctions::logCustomPivotChanges($themeasurement,$current_categories,$new_categories,'measurement','categories updated',$pivotkey='traitCategory');
        
        //log changes in persons
        ActivityFunctions::logCustomPivotChanges($themeasurement,$current_persons->all(),$persons,'measurement','measurer updated',$pivotkey='person');

        $this->affectedId($themeasurement->id);
        return;     
    }
}
