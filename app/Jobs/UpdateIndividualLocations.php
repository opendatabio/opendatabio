<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\IndividualLocation;
use App\Models\Location;
use App\Models\Individual;
use App\Models\ODBFunctions;
use Illuminate\Http\Request;
use Log;

class UpdateIndividualLocations extends ImportCollectable
{
    private $requiredKeys;

    /**
     * Execute the job.
     */
    public function inner_handle()
    {

      $data = $this->extractEntrys();
      if (!$this->setProgressMax($data)) {
          return;
      }
      $this->affectedModel(Individual::class);

      //if these fields are provided in header, remove from there
      foreach ($data as $registry) {
            if ($this->validateData($registry)) {
                try {
                    $this->UpdateIndividualLocation($registry);
                } catch (\Exception $e) {
                    $this->setError();
                    $this->appendLog('Exception '.$e->getMessage().' at '.$e->getFile().'+'.$e->getLine());
                }
            }
            if ($this->isCancelled()) {
                break;
            }
            $this->userjob->tickProgress();
        }
    }

    protected function validateData(&$registry)
    {
        //dataset may have been informed, will fail only if informed not FOUND
        //else will place individual in its own default dataset
        if (!$this->validateIndividual($registry)) {
            return false;
        }
        //get the individual_location_id
        $individual_location_id = isset($registry['individuallocation_id']) ? $registry['individuallocation_id'] : (isset($registry['individual_location_id']) ? $registry['individual_location_id'] : (isset($registry['id']) ? $registry['id'] : null));
        if (null == $individual_location_id) {
          //check if individual has a single location
          $individual =  Individual::findOrFail($registry['individual_id']);
          if ($individual->locations()->count()>1) {
            $this->skipEntry($registry,'Missing individual_location_id value. The individual has multiple locations, I cannot guess');
            return false;
          }
          $individual_location_id = $individual->location_first()->withPivot("id")->first()->pivot->id;

        }
        $registry['id'] = $individual_location_id;
        $registry['individual_location_id'] = null;
        $registry['individuallocation_id'] = null;

        $location = isset($registry['location']) ? $registry['location'] : (isset($registry['location_id']) ? $registry['location_id'] : null);
        $valid = ODBFunctions::validRegistry(Location::select('id'), $location);
        if (null === $valid) {
            $this->skipEntry($registry,'Location is invalid or empty!');
            return false;
        } else {            
          $registry['location_id'] = $valid->id;
          $registry['location'] = null;          
        }
        $oldregistry = IndividualLocation::withXy()->findOrFail($individual_location_id);
        $newentry = $this->extractLocationFields($registry,$oldregistry);
        if (!$this->validateLocations($newentry)) {
            return false;
        }
        $registry = $newentry;
        return true;
    }


    public function UpdateIndividualLocation($entry)
    {
        /* each entry is a single location, but returned as arry by validateLocations */

        $location = $entry['individual_locations'][0];
        $location['individual_id'] = $entry['individual_id'];
        $individual = Individual::findOrFail($entry['individual_id']);
        $location = array_filter($location,function($q){
            return !is_null($q);
        });
        $newindlocation = new Request;
        $newindlocation = $newindlocation->merge($location);

        //update the location record
        $savedindlocation = app('App\Http\Controllers\IndividualController')->saveIndividualLocation($newindlocation);
        if ($savedindlocation->getData()->errors == 1) {
          $this->appendLog('ERROR: location '.json_encode($location).' could not be updated for individual '.$individual->fullname.'  Errors:'.$savedindlocation->getData()->saved);
        }
        $this->affectedId($location['individual_id']);
        return;
    }

}
