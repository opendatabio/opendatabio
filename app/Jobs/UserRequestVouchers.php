<?php
/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\ODBRequest;
use Lang;
use Auth;
//UPDATES INDIVIDUAL IDENTIFICATIONS ONLY

class UserRequestVouchers extends ImportCollectable
{

    use AuthorizesRequests;

    /**
     * Execute the job.
     *
     * @return void
     */
     public function inner_handle()
     {

         $data =  $this->extractEntrys();
         //If comming from the web interface, fix date to extract identifications
         if (null !== $this->header['not_external']) {

           //check if user exists, if so link properly
           $user_id = $data['user_id'];
           $email = $data['email'];
           if (null==$user_id) {
             $isuser = User::where('email','like',$email);
             $user_id = ($isuser->count()>0) ? $isuser->first()->id : null;
             $email = ($isuser->count()>0) ? null : $email;
           }
           $data = [
            'message' => $data['message'],
            'email' => $email,
            'user_id' => $user_id,
            'institution' => $data['institution'],
            'biocollection_id' => $data['biocollection_id'],
           ];
           $data['type'] = 'vouchers';
           $odbrequest = ODBRequest::create($data);
           //store selected vouchers
           $voucher_ids = explode(',',$request->voucher_ids);
           $vouchers  = [];
           if (!$this->setProgressMax($voucher_ids)) {
               return;
           }
           foreach($voucher_ids as $voucher) {
               if ($this->isCancelled()) {
                 break;
               }
               $this->userjob->tickProgress();
               $vouchers[] = array(
                 'request_id' => $odbrequest->id,
                 'voucher_id' => $voucher,
                 'status' => 0,
               );
               $this->affectedId($voucher);
           }
           $odbrequest->vouchers()->sync($vouchers);

           //send email to user and curators
           $emailsent = ODBRequest::sendEmail($odbrequest);
           //return to view with message
           $url = url('odbrequests/'.$odbrequest->id);
           $url = ": <a href='".$url."' >".$url."</a>";
           $this->appendLog(Lang::get("messages.request_posted").$url);
           if (!$emailsent) {
             $this->appendLog("WARNING: ".Lang::get('messages.email_not_sent'));
           } else {
             $this->appendLog(Lang::get('messages.dataset_request_email_sent'));
           }
        } else {
          // TODO: Include handler to deal with external API data
        }
     }


}
