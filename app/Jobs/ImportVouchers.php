<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Voucher;
use App\Models\Location;
use App\Models\Individual;
use App\Models\Dataset;
use App\Models\ODBFunctions;
use App\Models\Biocollection;
use App\Models\BibReference;
use Illuminate\Http\Request;

use Lang;
use Auth;



class ImportVouchers extends ImportCollectable
{
    private $requiredKeys;

    /**
     * Execute the job.
     */
    public function inner_handle()
    {
        $data = $this->extractEntrys();
        if (!$this->setProgressMax($data)) {
            return;
        }
        $this->affectedModel(Voucher::class);

        $this->requiredKeys = $this->removeHeaderSuppliedKeys(['individual', 'biocollection']);
        $this->validateHeader();
        foreach ($data as $voucher) {
            if ($this->isCancelled()) {
                break;
            }
            $this->userjob->tickProgress();

            if ($this->validateData($voucher)) {
                // Arrived here: let's import it!!
                //am I entering here?
                //$this->appendLog('YESSESSSSS');
                try {
                    $this->import($voucher);
                } catch (\Exception $e) {
                    $this->setError();
                    $this->appendLog('Exception '.$e->getMessage().' at '.$e->getFile().'+'.$e->getLine().' on voucher '.$voucher['number']);
                }
            }
        }
    }

    protected function validateData(&$voucher)
    {
        if (!$this->hasRequiredKeys($this->requiredKeys, $voucher)) {
            return false;
        }

        if (!$this->validateIndividual($voucher)) {
            return false;
        }

        if (!$this->validateDataset($voucher)) {
            return false;
        }

        //collectors (at least a valid one must exist if informed) but is not mandatory
        $hascollector  = isset($voucher['collector']) ? ((null != $voucher['collector']) ? $voucher['collector'] : null) : null;
        $collectors = $this->extractCollectors('Voucher', $voucher, 'collector');
        if (null == $collectors  and $hascollector) {
          return false;
        }
        $voucher['collector'] = $collectors;

        //if collector is informed, then number is mandatory
        $hasnumber   = isset($voucher['number']) ? ((null != $voucher['number']) ? $voucher['number'] : null) : null;
        if ($hascollector and !$hasnumber) {
          $this->skipEntry($voucher, 'Because you informed the collector you must also inform number. Note that neither is mandatory for voucher');
          return false;
        }
        //if collector and number is informed, a date must be provided as well
        if ($hascollector and !$this->validateDate($voucher)) {
          $this->skipEntry($voucher,'Collector was informed, then date must also be informed. Note that neither is mandatory for voucher, which inherits these from the individual if empty');
          return false;
        }

        if (!$this->validateBiocollection($voucher)) {
            return false;
        }
        if (!$this->validateBibReferences($voucher)) {
          $this->skipEntry($voucher,'Bibreferences informed were not found in the database');
          return false;
        }
        return true;
    }



    public function validateDate(&$voucher)
    {
      //validate date
      $date = isset($voucher['date']) ? $voucher['date'] : (isset($this->header['date']) ? $this->header['date'] : null);
      if (null == $date) {
        $year = isset($voucher['date_year']) ? $voucher['date_year'] : (isset( $voucher['year']) ? $voucher['year'] : null);
        $month = isset($voucher['date_month']) ? $voucher['date_month'] : (isset( $voucher['month']) ? $voucher['month'] : null);
        $day = isset($voucher['date_day']) ? $voucher['date_day'] : (isset( $voucher['day']) ? $voucher['day'] : null);
        $date = [$month,$day,$year];
      }
      if (is_string($date)) {
        if (preg_match("/\//",$date)) {
            $date = explode("/",$date);
            $date = [$date[1],$date[2],$date[0]];
        } elseif (preg_match("/-/",$date)) {
            $date = explode("-",$date);
            $date = [$date[1],$date[2],$date[0]];
        }
      } elseif (!is_array($date)) {
        if (get_class($date)==="DateTime") {
           $year = $date->format('Y');
           $day = $date->format('d');
           $month = $date->format('m');
           $date = [$month,$day,$year];
        }
      }
      $hasdate = array_filter($date);
      if (count($hasdate)>0) {
        if (!(Individual::checkDate($date))) {
          $this->skipEntry($voucher,'Informed date is invalid! Date'.json_encode($date));
          return false;
        }
        $voucher['date'] = $date;
        return true;
      }
      return false;
    }

    public function validateIndividual(&$voucher)
    {
      if (isset($voucher['individual'])) {
          $individual = $voucher['individual'];
          if (((int)($individual))>0) {
              $ref = Individual::where('id',$individual);
          } else {
              $ref = Individual::whereRaw('odb_ind_fullname(id,tag) like "'.$individual.'"');
          }
          if ($ref->count()==1) {
              $voucher['individual_id'] = $ref->get()->first()->id;
              //add dataset if does not exists
              if (null == $voucher['dataset']) {
                $voucher['dataset'] = $ref->get()->first()->dataset_id;
              }
              return true;
          }
      }
      $this->skipEntry($voucher, ' Individual '.$voucher['individual'].' not found in the database');
      return false;
    }


    public function import($voucher)
    {

        $keys_mandatory = ['individual','biocollection'];
        $keys_other = ['biocollection_type','biocollection_number','dataset','collector','number','notes','date'];
        $store_request = [
          'from_the_api' => 1,
          'individual_id' => (int) $voucher['individual_id'],
          'biocollection_id' => (int) $voucher['biocollection_id'],
          'biocollection_type' => isset($voucher['biocollection_type']) ? (int) $voucher['biocollection_type'] : 0,
          'dataset_id' => (int) $voucher['dataset'],
          'biocollection_number' => isset($voucher['biocollection_number']) ? $voucher['biocollection_number'] : null,
          'number' =>  isset($voucher['number']) ? (string) $voucher['number'] : null,
          'collector' => isset($voucher['collector']) ? $voucher['collector'] : null,
          'notes' => isset($voucher['notes']) ? (string) $voucher['notes'] : null,
          'date' => isset($voucher['date']) ? $voucher['date'] : null,
          'bibreferences' => isset($voucher['bibreferences']) ? $voucher['bibreferences'] : null,
        ];
        //$store_request = array_filter($store_request,function($a) { return null !== $a;});
        //transform info in a request
        $saverequest = new Request;
        $saverequest->merge($store_request);

        //store the record, which will result in the individual
        $savedvoucher = app('App\Http\Controllers\VoucherController')->store($saverequest);
        if (is_string($savedvoucher)) {
          $this->skipEntry($voucher,'This voucher could not be imported. Possible errors may be: '.$savedvoucher);
          return ;
        }
        $this->affectedId($savedvoucher->id);
        return;
    }
}
