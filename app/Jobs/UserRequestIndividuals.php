<?php
/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Individual;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\ODBRequest;
use Lang;
use Auth;
//UPDATES INDIVIDUAL IDENTIFICATIONS ONLY

class UserRequestIndividuals extends ImportCollectable
{

    use AuthorizesRequests;

    /**
     * Execute the job.
     *
     * @return void
     */
     public function inner_handle()
     {

         $data =  $this->extractEntrys();
         //If comming from the web interface, fix date to extract identifications
         if (null !== $this->header['not_external']) {
           if (!$this->hasRequiredKeys(["individualids_list","message","user_id","request_type","request_biocollection_id"], $data)) {
               return false;
           }
           $individual_ids = explode(',',$data['individualids_list']);
           if (count($individual_ids)==0) {
             $this->appendLog('WARNING: Missing individual list'.$individual->fullname);
             return false;
           }

         //check if user exists, if so link properly
         $newrequest = [
           'message' => $data['message'],
           'user_id' => $data['user_id'],
           'type' => $data['request_type'],
         ];
         $odbrequest = ODBRequest::create($newrequest);
         //store selected vouchers
         $individuals  = [];
         $identifiers = $this->extractIdentification($data);
         if ($identifiers) {
           $identifiers = json_encode(['identification' => $identifiers]);
         } elseif ($data['request_type']=='identification') {
           $this->skipEntry($data,"Please inform the new identification for your update request");
           return false;
         }
         if (!$this->setProgressMax($individual_ids)) {
             return;
         }
         foreach($individual_ids as $individual_id) {
            if ($this->isCancelled()) {
              break;
            }
            $this->userjob->tickProgress();
            $individual = Individual::findOrFail($individual_id);
            if (Auth::user()->can('makerequest', $individual))  {
             $individuals[] = [
               'request_id' => $odbrequest->id,
               'individual_id' => $individual_id,
               'status' => 0,
               'biocollection_id' => $data['request_biocollection_id'],
               'notes' => $identifiers,
             ];
             $this->affectedId($individual->id);
            } else {
             $this->appendLog("You do not have permissions for Individual ".$individual->fullname.", which was not included in the request!");
            }
         }
         if (count($individuals)==0) {
           $odbrequest->delete();
           $this->appendLog("You do not have permissions to edit any of the Individuals selected. The request was not made");
           return;
         }
         $odbrequest->individuals()->sync($individuals);
         //send email to user and curators
         $emailsent = ODBRequest::sendEmail($odbrequest);
         //return to view with message
         $url = url('odbrequests/'.$odbrequest->id);
         $url = ": <a href='".$url."' >".$url."</a>";
         $this->appendLog(Lang::get("messages.request_posted").$url);
         if (!$emailsent) {
           $this->appendLog("WARNING: ".Lang::get('messages.email_not_sent'));
         } else {
           $this->appendLog(Lang::get('messages.dataset_request_email_sent'));
         }
        } else {
          // TODO: Include handler to deal with external API data
        }
     }

}
