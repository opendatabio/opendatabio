<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use Illuminate\Http\Request;
use Response;
use App\Models\Form;
use App\Models\FormTask;
use App\Models\Measurement;
use App\Models\FormTaskObject;
use Auth;

use Lang;
use Log;

class FormTaskPrepare extends AppJob
{

    public function inner_handle()
    {
        $data = $this->extractEntrys();
        $form_task_id = isset($data['form_task_id']) ? $data['form_task_id'] : null;
        if (!$form_task_id) {
            $this->appendLog('ERROR: no form_task_id informed!');
            return;
        }
        $form_task = FormTask::find($form_task_id);
        $ids = $form_task->dataset_ids;
        if (!$ids) {
            $this->appendLog('ERROR: no datasets informed for this task!');
            return;
        }
        $measured_type = $form_task->form->measured_type;
        $short_measured_type = $form_task->form->short_measured_type;
        
        $sql_raws=[];
        $sql_raws[] = "(SELECT DISTINCT measured_id FROM measurements WHERE dataset_id IN('".implode(",",$ids)."') AND measured_type LIKE '%".$short_measured_type."')";
        if ($short_measured_type=="Individual")
        {
            $sql_raws[] = "(SELECT DISTINCT individuals.id as measured_id FROM individuals WHERE individuals.dataset_id IN('".implode(",",$ids)."'))";
        }
        if ($short_measured_type=="Voucher")
        {
            $sql_raws[] = "(SELECT DISTINCT vouchers.id as measured_id FROM vouchers WHERE vouchers.dataset_id IN('".implode(",",$ids)."'))";
        }
        if (count($sql_raws)==1) {
            $sql_raw=  $sql_raws[0];
        } else {
            $sql_raw = "(SELECT DISTINCT tb.measured_id FROM (".implode(" UNION ",$sql_raws).") as tb)";
        }

        $query = app($measured_type)::whereRaw("id IN(".$sql_raw.")");                

        $progress_max = $query->count();
        $this->userjob->setProgressMax($progress_max);       

        //$this->appendLog($query->toSql());
        if ($query->count()==0) {
            $this->userjob->status = "Failed";
            $this->appendLog('ERROR: your options resulted in no data');
            return;
        }
        foreach($query->cursor() as $measured) {
            
            if ($this->isCancelled()) {
                break;
            }
            $this->userjob->tickProgress();
           

            if ($short_measured_type=="Individual")
            {                
                $label = $measured->selector_label;
                $tag = trim($measured->tag);
                $tag = str_replace(" ","-",$tag);
                $tag = str_replace("_","-",$tag);
                $tags = explode("-",$tag);
                if (count($tags)>1) {
                    $index = count($tags)-1;
                    $tag = $tags[$index] ? $tags[$index] : $tags[($index-1)];
                } 
                $search_label = $tag;

            } elseif ($short_measured_type=='Taxon') 
            {
                $label = $measured->scientificName;
                $search_label = $label;

            } elseif ($short_measured_type=='Voucher') 
            {
                $label = $measured->selector_label;
                $search_label = $measured->recordNumber;
            } elseif ($short_measured_type=='Location') 
            {
                $label = $measured->name." - ".$measured->parentName;
                $search_label = $label;
            } else {
                $label = $measured->name;
                $search_label = $label;
            }
            $records = [
                'form_task_id' => $form_task->id,
                'measured_id' =>$measured->id,
                'measured_type' => $measured_type,
                'label' => $label,                
                'search_label' => $search_label,
            ];
            /* prevent duplications */
            $exists = FormTaskObject::where('form_task_id',$form_task->id)->where('measured_id',$measured->id)->where('measured_type',$measured_type)->count();
            if ($exists) {
                $this->appendLog("WARNING: duplicated object, skipped ".$short_measured_type.": id ".$measured->id);                
            } else {
                $form_task_measured = FormTaskObject::create($records);
                $form_task_measured_id = $form_task_measured->id;
                $this->affectedId($form_task_measured_id);
            }
        };
        $stored = $this->userjob->affected_ids_count;
        if ($form_task->progress_max==0) {
            $form_task->progress_max = $stored;        
        } else {
            $form_task->progress_max = ($form_task->progress_max)+$stored;        
        }
        $form_task->status = "prepared";
        $form_task->save();

        $this->userjob->progress = $progress_max;
        $this->userjob->save();
    }



}