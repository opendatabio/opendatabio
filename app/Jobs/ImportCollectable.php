<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Taxon;
use App\Models\Location;
use App\Models\Identification;
use App\Models\Person;
use App\Models\Collector;
use App\Models\Biocollection;
use App\Models\Project;
use App\Models\Voucher;
use App\Models\Dataset;
use App\Models\ODBFunctions;
use App\Models\Individual;
use Auth;
use Lang;
use Illuminate\Http\Request;
use Response;
use Log;

class ImportCollectable extends AppJob
{

    protected function validateHeader($field = 'collector')
    {
        if (isset($this->header['dataset'])) {
            $this->validateDataset($this->header);
        }
        if (isset($this->header[$field])) {
            $person = $this->extractCollectors('Header', $this->header, $field);
            if ($person) {
                $this->header[$field] = $person;
            }
        }
    }

    /*
     * Changes the $fieldName item of the $registry array to the id of valid Project.
     * If this item is not present in the array, it uses the defaultProject of the user.
     * Otherwise it interprets the value of this item as id or name of a project.
     * @retuns true if the project is validated; false if it fails.
     */
     // TODO: FUNCTION OBSOLETE NOT BEING USED
    protected function validateProject(&$registry)
    {
        $project = isset($registry['project']) ? $registry['project'] : null;
        if (null == $project  and isset($this->header['project'])) {
            $project = $this->header['project'];
        }
        if (null != $project) {
            $valid = ODBFunctions::validRegistry(Project::select('id'),$project,['id','name']);
            if (null === $valid) {
                $this->skipEntry($registry, 'project'.' '.$registry['project'].' was not found in the database');
                return false;
            }
            $registry['project'] = $valid->id;
            return true;
        }
        $registry['project'] = Auth::user()->defaultProject->id;
        return true;
    }


    /* if dataset is not informed return true, return false only if informed and invalid */
    protected function validateDataset(&$registry)
    {
        $header = $this->header;
        $dataset = isset($registry['dataset_id']) ? $registry['dataset_id'] : (isset($registry['dataset']) ? $registry['dataset'] : null);
        $header = isset($header['dataset_id']) ? $header['dataset_id'] : (isset($header['dataset']) ? $header['dataset'] : null);
        if (null == $dataset and $header != null) {
            $dataset = $header;
        }
        if (null != $dataset) {
            $valid = ODBFunctions::validRegistry(Dataset::select('id','name'),$dataset,['id','name']);
            if (null === $valid) {
                $this->skipEntry($registry, 'dataset'.' '.$dataset.' was not found in the database');
                return false;
            }
            $registry['dataset'] = $valid->id;
            $user = Auth::user();
            $valid = Dataset::findOrFail($dataset);
            $privacy = $valid->privacy;
            if ($privacy==Dataset::PRIVACY_PROJECT) {
              if (!$valid->project->isAdmin($user) and !$valid->project->isCollab($user)) {
                $this->skipEntry($registry, 'You do not have permissions for project of dataset '.$valid->name);
                return false;
              }
            } else {
              if (!$valid->isAdmin($user) and !$valid->isCollab($user)) {
                $this->skipEntry($registry, 'You do not have permissions for dataset '.$valid->name);
                return false;
              }
            }
            return true;
        }
        if (Auth::user()->defaultDataset) {
          $registry['dataset'] = Auth::user()->defaultDataset->id;
          return true;
        }
        $this->skipEntry($registry, 'dataset not informed and you do not have a default dataset defined');
        return false;

    }


    protected function extractCollectors($callerName, $registry, $field = 'collector')
    {
        if (('Header' !== $callerName) and isset($this->header[$field])) {
            return $this->header[$field];
        }
        if (!isset($registry[$field]) or null == $registry[$field]) {
            return null;
        }
        #explode comma will fail when abbreviation is provided and contain commas
        #replace by | which is gbif standard
        $persons = $registry[$field];
        if (strpos($registry[$field], '|') !== false) {
            $persons = explode('|', $registry[$field]);
        } else {
            if (strpos($registry[$field], ';') !== false) {
              $persons = explode(';', $registry[$field]);
            } else {
              if (strpos($registry[$field], ',') !== false) {
                $persons = explode(',', $registry[$field]);
                $persons = array_filter($persons,function($p) { return ((int)$p)>0; });
                if (count($persons)==0) {
                  /* there are commas, but the values are not numeric,
                  * so it may be abbreviations
                  */
                  $persons = [$registry[$field]];
                }
              }
            }
        }
        if (!is_array($persons)) {
          $persons = [$persons];
        }
        $ids = [];
        $counter = 0;
        //Log::info(json_encode($persons));
        //return null;

        foreach ($persons as $person) {
            $valid = ODBFunctions::validRegistry(Person::select('id'), $person, ['id', 'abbreviation', 'full_name', 'email']);
            if (null === $valid) {
                if($counter==0) {
                  $this->skipEntry($registry,'The first person is the required Main Collector and your value '.$person." is was not found in the database.");
                  break;
                }
                $this->appendLog('WARNING: '.$callerName.' reffers to '.$person.' as member of '.$field.', but this person was not found in the database. Ignoring person '.$person);
            } else {
                $ids[] = $valid->id;
            }
            $counter = $counter+1;
        }
        return array_unique($ids);
    }


    public function validateModifier($modifier)
    {
      if (null == $modifier) {
        return 0;
      }
      $validmods = Identification::MODIFIERS;
      if (in_array($modifier,$validmods)) {
        return (int) $modifier;
      }
      $validcodes = [];
      foreach($validmods as $md) {
        $validcodes[Lang::get('levels.modifier.'.$md)] = (string) $md;        
      }
      if (isset($validcodes[$modifier])) {
        return (int) $validcodes[$modifier];
      }
      $modifier = (string) $modifier;
      if (!in_array($modifier,$validcodes)) {
          $this->appendLog("WARNING: Identification modifier informed ".$modifier." is not valid");
          return null;
      }
      return (int) $modifier;
    }



    protected function extractIdentification($registry,$oldidentification=null)
    {
        if (!isset($registry['taxon']) and !isset($registry['taxon_id']) and !$oldidentification) {
          return null;
        }
        $taxon_id = null;
        if (isset($registry['taxon_id'])) {
          $taxon = $registry['taxon_id'];
          $taxon_id = Taxon::select('id')->where('id', '=', $taxon)->get();
          $message = "Search of taxon id ".$taxon." found ".$taxon_id->count();
        } elseif (isset($registry['taxon'])) {
          $taxon = $registry['taxon'];
          $taxon_id = Taxon::select('id')->whereRaw('odb_txname(name, level, parent_id,0,0) = ?', [$taxon])->get();
          $message = "Search of taxon string ".$taxon." found ".$taxon_id->count();
        } 
        if ($taxon_id) {
          if ($taxon_id->count()==1) {
              $identification['taxon_id'] = $taxon_id->first()->id;
          }
        }         
        if (!isset($identification['taxon_id']) and $oldidentification) {
          $identification['taxon_id'] = $oldidentification->taxon->id;
        } elseif(!isset($identification['taxon_id'])) {
          $this->appendLog("WARNING: ".$message);
          return null;
        }
        // Map $registry['identifier'] to $identification['person_id']
        if (!isset($registry['identifier']) and isset($registry['identifier_id'])) {
              $registry['identifier'] = $registry['identifier_id'];
        }        
        if (isset($registry['identifier'])) {
            $person = $this->extractCollectors('Header', $registry, 'identifier');
            if (null === $person) {
                $this->appendLog('WARNING: Taxonomic identifier '.$registry['identifier'].' was not found in the person table.');
                return null;
            }
            //$identification['person_id'] = $person->id;
            $identification['identifiers'] = $person;
        } else {
            if ($oldidentification) {
              $identification['identifiers'] = $oldidentification->collectors()->pluck('persons.id')->toArray(); 
            } else {
              //$this->appendLog('WARNING: Taxonomic identifier was not informed and was assigned the Individual main_collector');
              $identification['identifiers'] = (isset($registry['collector']) and is_array($registry['collector'])) ? $registry['collector'] : null;
            }
        }
        //the following refer to external(or internal) records upon which the identification was based upon (we may remove this likely without harm)
        $identification['biocollection_id'] = null;
        $identification['biocollection_reference'] = null;
        if (isset($registry['identification_based_on_biocollection']) && isset($registry['identification_based_on_biocollection_id'])) {
           if (null != $registry['identification_based_on_biocollection']) {
            $biocollectid = ODBFunctions::validRegistry(Biocollection::select('id'), $registry['identification_based_on_biocollection'], ['id', 'acronym', 'name', 'irn']);
            if (null === $biocollectid) {
                $this->appendLog("WARNING: Biocollection ".$registry['identification_based_on_biocollection']." was not found in the biocollection table or their reference is missed! Ignoring in the identification the relationship with this external reference");
                $identification['biocollection_reference'] = null;
            } else {
                $identification['biocollection_id'] = $biocollectid->id;
                $identification['biocollection_reference'] = $registry['identification_based_on_biocollection_id'];
            }
          }
        } elseif ($oldidentification) {
          $identification['biocollection_id'] = $oldidentification->biocollection_id;
           $identification['biocollection_reference'] = $oldidentification->biocollection_reference;
        }
        $identification['notes'] = isset($registry['identification_notes']) ? $registry['identification_notes'] : ($oldidentification ? $oldidentification->notes : null);

        //modifier must be a valid code else is false
        $modifier = isset($registry['modifier']) ? $registry['modifier'] : ($oldidentification ? $oldidentification->modifier : null);
        $identification['modifier'] = self::validateModifier($modifier);

        //implemented to account for incomplete dates in identification (most commom)
        $date = null;
        if (isset($registry['identification_date_year'])) {
          $year = ((int) $registry['identification_date_year'])>0 ? $registry['identification_date_year'] : null;
          $month = isset($registry['identification_date_month']) ? $registry['identification_date_month'] : null;
          $day = isset($registry['identification_date_day']) ? $registry['identification_date_day'] : null;
          $date = array("month" => $month,"day" => $day,'year' => $year);
        } elseif (isset($registry['identification_date'])) {
            $date = $registry['identification_date'];
        } elseif ($oldidentification) {
          $date = $oldidentification->date;
        }
        //ic string assumes "-" or "/" as separators and format YYYY-MM-DD
        if (is_string($date)) {
          if (preg_match("/\//",$date)) {
              $date = explode("/",$date);
              $date = [$date[1],$date[2],$date[0]];
          } elseif (preg_match("/-/",$date)) {
              $date = explode("-",$date);
              $date = [$date[1],$date[2],$date[0]];
          }
        } elseif (!is_array($date) and $date!=null) {
          if (is_object($date)) {
            if (get_class($date)==="DateTime") {
              $year = $date->format('Y');
              $day = $date->format('d');
              $month = $date->format('m');
              $date = [$month,$day,$year];
            }
          } else {
            $this->appendLog("ERROR: identification_date informed could not be validated");
            return false;
          }
        }
        if (null == $date and isset($registry['date'])) {
          $this->appendLog("WARNING: identification_date not informed. Record date used instead!");
          $identification['date'] = $registry['date'];
        } elseif(count($date)==3) {
          if (!(Identification::checkDate($date))) {
            $this->appendLog("FAILED: identification_date YYY=".$date[2]." MM=".$date[0]." DD=".$date[1]." is invalid");
            return false;
          }
          $identification['date'] = $date;
        } 
        return $identification;
    }


    protected function createCollectorsAndIdentification($object_type, $object_id, $collectors = null, $identification = null)
    {
        if ($identification) {
            $date = $identification['date'];
            $identification = new Identification([
                'object_id' => $object_id,
                'object_type' => $object_type,
                'taxon_id' => $identification['taxon_id'],
                'person_id' => $identification['person_id'],
                'biocollection_id' => $identification['biocollection_id'],
                'biocollection_reference' => $identification['biocollection_reference'],
                'notes' => $identification['notes'],
                'modifier' => $identification['modifier'],
            ]);
            $identification->setDate($date[0],$date[1],$date[2]);
            $identification->save();
        }
        if ($collectors) {
            foreach ($collectors as $collector) {
                Collector::create([
                        'person_id' => $collector,
                        'object_id' => $object_id,
                        'object_type' => $object_type,
                ]);
            }
        }
    }

    /* location may be uploaded for an individual as :
    a) a string for name or id
    b) latitude and longitude
    c) array of arrays with keys  location_id or latitude+longitude, location_date_time,location_notes*/
    protected function validateLocations(&$registry)
    {
      $location = isset($registry['location']) ? $registry['location'] : (isset($this->header['location']) ? $this->header['location'] : null);
      $longitude = isset($registry['longitude']) ? (float) $registry['longitude'] : null;
      $latitude = isset($registry['latitude']) ? (float) $registry['latitude'] : null;

      if (null == $location and (null == $longitude or null == $latitude)) {
        $this->skipEntry($registry, 'location is missing or incomplete');
        return false;
      }

      $thelocations = [];
      if (null != $location and !is_array($location)) {
        $record =  [
          'location' => $location,
          'altitude' => isset($registry['altitude']) ? (float) $registry['altitude'] : null,
          'notes' => isset($registry['location_notes']) ? $registry['location_notes'] : null,
          'date_time' => isset($registry['location_date_time']) ? $registry['location_date_time'] : null,
          'x' => isset($registry['x']) ? (((float) $registry['x'])>=0 ? (float) $registry['x'] : null) : null,
          'y' => isset($registry['y']) ? (((float) $registry['y'])>=0 ? (float) $registry['y'] : null) : null,
          'distance' => isset($registry['distance']) ? (float) $registry['distance'] : null,
          'angle' => isset($registry['angle']) ? (float) $registry['angle'] : null,
        ];
        $thelocations[] = array_filter($record,
          function ($var){
            return ($var !== NULL && $var !== FALSE && $var !== '');
          }
        );
      }
      if (null == $location and (abs($longitude)+abs($latitude))>0 ) {
        $record =  [
          'latitude' => $latitude,
          'longitude' => $longitude,
          'altitude' => isset($registry['altitude']) ? (float) $registry['altitude'] : null,
          'notes' => isset($registry['location_notes']) ? $registry['location_notes'] : null,
          'date_time' => isset($registry['location_date_time']) ? $registry['location_date_time'] : null,
          'x' => isset($registry['x']) ? (((float) $registry['x'])>=0 ? (float) $registry['x'] : null) : null,
          'y' => isset($registry['y']) ? (((float) $registry['y'])>=0 ? (float) $registry['y'] : null) : null,
          'distance' => isset($registry['distance']) ? (float) $registry['distance'] : null,
          'angle' => isset($registry['angle']) ? (float) $registry['angle'] : null,
        ];
        $thelocations[] = array_filter($record,
          function ($var){
            return ($var !== NULL && $var !== FALSE && $var !== '');
          }
        );
      }
      if (is_array($location)) {
        $possible_keys = ['id','location','longitude','latitude','notes','date_time','altitude','x','y','distance','angle','location_date_time'];
        $locationkeys = array_filter(array_keys($location));
        if (count($locationkeys)>0) {
          $issingle = array_diff($locationkeys,$possible_keys);
          if (count($issingle)==0) {
            $record =  [
            'id' => isset($location['id']) ? (int) $location['id'] : null,
            'location' => isset($location['location']) ? (float) $location['location'] : null,
            'latitude' => isset($location['latitude']) ? (float) $location['latitude'] : null,
            'longitude' => isset($location['longitude']) ? (float) $location['longitude'] : null,
            'altitude' => isset($location['altitude']) ? (float) $location['altitude'] : (isset($registry['altitude']) ? (float) $registry['altitude'] : null),
            'notes' => isset($location['notes']) ? $location['notes'] : (isset($registry['location_notes']) ? $registry['location_notes'] : null),
            'date_time' => isset($location['date_time']) ? $location['date_time'] : (isset($registry['location_date_time']) ? $registry['location_date_time'] : null),
            'x' => isset($location['x']) ? (float) $location['x'] : (isset($registry['x']) ? (float) $registry['x'] : null),
            'y' => isset($location['y']) ? (float) $location['y'] : (isset($registry['y']) ? (float) $registry['y'] : null),
            'distance' => isset($location['distance']) ? (float) $location['distance'] : (isset($registry['distance']) ? (float) $registry['distance'] : null),
            'angle' => isset($location['angle']) ? (float) $location['angle'] : (isset($registry['angle']) ? (float) $registry['angle'] : null),
            ];
            $thelocations[] = array_filter($record,function($q){
              return !is_null($q);
             });
          }
          /* if it is a single record, but some informed keys are not present, then is invalid */
          if (count($issingle)>0 and count($issingle)<count($location)) {
            $this->skipEntry($registry, 'The location keys '.implode('|',$issingle).' are invalid.');
            return false;
          }
        }

        /* if this is true, there should be multiple locations informed and each array element is a location value */
        if (count($thelocations)==0) {
          foreach ($location as $value) {
            $keysvalid = array_diff(array_keys($value),$possible_keys);
            if (count($keysvalid)==0) {
              $record =  [
                'id' => isset($location['id']) ? (int) $location['id'] : null,
                'location' => isset($value['location']) ? (float) $value['location'] : null,
                'latitude' => isset($value['latitude']) ? (float) $value['latitude'] : null,
                'longitude' => isset($value['longitude']) ? (float) $value['longitude'] : null,
                'altitude' => isset($value['altitude']) ? (float) $value['altitude'] : null,
                'notes' => isset($value['notes']) ? $value['notes'] : null,
                'date_time' => isset($value['date_time']) ? $value['date_time'] : null,
                'x' => ((float) $value['x'])>=0  ? (float) $value['x'] : (((float) $registry['x'])>=0  ? (float) $registry['x'] : null),
                'y' => ((float) $value['y'])>=0  ? (float) $value['y'] : (((float) $registry['y'])>=0 ? (float) $registry['y'] : null),
                'distance' => isset($value['distance']) ? (float) $value['distance'] : (isset($registry['distance']) ? (float) $registry['distance'] : null),
                'angle' => isset($value['angle']) ? (float) $value['angle'] : (isset($registry['angle']) ? (float) $registry['angle'] : null),
              ];
              $thelocations[] = array_filter($record,function($q){
                return !is_null($q);
               });
            }
            if (count($keysvalid)>0) {
              $this->skipEntry($registry, 'The location keys '.implode('|',$keysvalid).' are invalid.');
              break;
              return false;
            }
         }
       }
      }

      /*if got here fields for location exist and must be validated */
      $validatelocations = [];
      $messages = [];
      //Log::info("here dddd: ".json_encode($registry));

      foreach($thelocations as $alocation) {
        $location = isset($alocation['location']) ? $alocation['location'] : null;
        $latitude = isset($alocation['latitude']) ? $alocation['latitude'] : null;
        $longitude = isset($alocation['longitude']) ? $alocation['longitude'] : null;
        if (null != $location and (abs($longitude)+abs($latitude))==0) {
            $valid = ODBFunctions::validRegistry(Location::select('id'), $location);
            if (null === $valid) {
              $messages[] = 'location'.' '.$location.' was not found in the database';
            } else {
              $alocation['location'] = null;
              $alocation['location_id'] = $valid->id;
              $validatelocations[] = $alocation;

            }
        }
        // if coordinates were informed detect parent or self, not saving
        elseif (null == $location and (abs($longitude)+abs($latitude))>0) {
            $data = [];
            $data['lat1'] = $latitude;
            $data['long1']= $longitude;
            $data['adm_level'] = Location::LEVEL_POINT;
            $data['geom_type'] = "point";
            $locrequest = new Request;
            $locrequest->merge($data);
            //autodetect parent or self if the case
            $detected_locations = app('App\Http\Controllers\LocationController')->autodetect($locrequest);

            //if found an exact match retrieve location
            $hadlocation =  isset($detected_locations->getData()->detectedLocation) ? $detected_locations->getData()->detectedLocation : [];
            $newlocation =  isset($detected_locations->getData()->detectdata) ? $detected_locations->getData()->detectdata : [];
            $hadrelated = isset($detected_locations->getData()->detectrelated) ? $detected_locations->getData()->detectrelated : [];
            $related = [];
            if ($hadrelated) {
              foreach ($hadrelated as $value) {
                $related[] = $value->id;
              }
            }
            //if found nothing, neither parent nor self, then issue error
            if (!array_filter($hadlocation) and !array_filter($newlocation)) {
              $messages[] =  'Location latitude '.$latitude.' and/or location longitude'.$longitude.' are invalid!';
            } elseif (array_filter($hadlocation)) {
                //if a location with the same coordinates was found, then use it for the individual
                $alocation['latitude'] = null;
                $alocation['longitude'] = null;
                $alocation['location_id'] = $hadlocation[0];
                $validatelocations[] = $alocation;
            } elseif (!array_filter($hadlocation) and array_filter($newlocation)) {
                //if a location with the same coordinates was found, then use it for the individual
                $alocation['latitude'] = null;
                $alocation['longitude'] = null;
                $alocation['location_tosave']  = [
                  'name' => config('app.unnamedPoint_basename')."_".preg_replace("/[A-Z\(\)-\.\s]/","",$newlocation[2]),
                  'parent_id' => $newlocation[1],
                  'geom' => $newlocation[2],
                  'adm_level' => Location::LEVEL_POINT,
                  'related_locations' => $related,
                ];
                $validatelocations[] = $alocation;
            }
        }
        else {
            $messages[] =  'Location record has invalid keys: '.json_encode($alocation);
        }
      }

      if (count($messages)>0 or count($validatelocations)<count($thelocations)) {
        $this->skipEntry($registry, 'One of more locations with problems: '.implode('|',$messages));
        return false;
      }

      /* save locations if any to save */
      /* validate all individual_location attributes */
      $validatedlocation_messages = [];
      $finallocations = [];
      foreach($validatelocations as $individual_location) {
          // if a new location need to be save, then save it
          if(isset($individual_location['location_tosave'])) {
            $saverequest = new Request;
            $saverequest->merge($individual_location['location_tosave']);
            $savedlocation = app('App\Http\Controllers\LocationController')->saveForIndividual($saverequest);
            //$savedlocation = json_decode($savedlocation);
            $savedlocation = isset($savedlocation->getData()->savedLocation) ? $savedlocation->getData()->savedLocation : null;
            if ($savedlocation===null) {
                $validatedlocation_messages [] = 'Could not save detected location: '.implode('|',$individual_location);
            } else {
              $individual_location['location_id'] = $savedlocation[0];
              $individual_location['location_tosave'] = null;
            }
          }
          if (isset($individual_location['location_id'])) {
            $locationrequest = new Request;
            $locationrequest->merge($individual_location);
            //check the individual location attributes are valid
            $vallocation = app('App\Http\Controllers\IndividualController')->validateIndividualLocation($locationrequest);
            if ($vallocation->fails()) {
              $validatedlocation_messages [] = 'Individual location defined by: '.json_encode($individual_location).' is not valid. Errors: '.implode(" | ",$vallocation->errors()->all());
            }
            $finallocations[] = $individual_location;
          }
      }
      if (count($validatedlocation_messages)>0) {
        $this->skipEntry($registry, 'Problems in individual location validation: '.implode('|',$validatedlocation_messages));
        return false;
      }

      $registry['individual_locations'] = $finallocations;
      return true;
    }



    protected function extractBioCollection(&$registry)
    {
        // if none are present or both are null then this is missing
        if (!isset($registry['biocollection'])) {
          return true;
        }
        if (null == $registry['biocollection']) {
          return true;
        }
        $biocollections = [];
        $validtypes = [];
        $locales =  config("app.available_locales");
        foreach (Biocollection::NOMENCLATURE_TYPE as $type) {
          foreach($locales as $locale) {
            $validtypes[Lang::choice('levels.vouchertype.'.$type,1,[],$locale)] = $type;
          }
        }
        $validtypes_keys = array_keys($validtypes);
        $user_id = Auth::user()->id;
        //if is a string, then acronyms only are expected, validate them
        if (!is_array($registry['biocollection'])) {
          //then we expect a single or a list of acronyms
          $pattern = "/[;,|]/";
          $biocols= preg_split($pattern, $registry['biocollection']);
          $biocolsnumbers = [];
          if (isset($registry['biocollection_number'])) {
            $biocolsnumbers = preg_split($pattern, $registry['biocollection_number']);
          }
          $biocolstypes = [];
          if (isset($registry['biocollection_type'])) {
            $biocolstypes = preg_split($pattern, $registry['biocollection_type']);
          }
          foreach ($biocols as $key => $value) {
            $query = Biocollection::select(['biocollections.id', 'acronym', 'biocollections.name', 'irn'])->doesntHave('users')->orWhereHas('users',function($u) use($user_id){ $u->where('users.id',$user_id);});
            $fields = ['id', 'acronym', 'name', 'irn'];
            $valid = ODBFunctions::validRegistry($query, $value, $fields);
            if (!$valid) {
              break;
            }
            $thetype = isset($biocolstypes[$key]) ? $biocolstypes[$key] : 0;
            if (!in_array($thetype,$validtypes_keys) and !in_array($thetype,Biocollection::NOMENCLATURE_TYPE)) {
              break;
            } elseif (in_array((string) $thetype,$validtypes_keys) ) {
              $kk = array_search($thetype,$validtypes_keys);
              if ($kk) {
                $thetype = array_values($validtypes)[$kk];
              } else {
                break;
              }
            }
            $biocollections[] = [
              'biocollection_id' => $valid->id,
              'biocollection_number' => isset($biocolsnumbers[$key]) ? $biocolsnumbers[$key] : null,
              'biocollection_type' => $thetype,
            ];
          }
        } else {
          //expects at least biocollection_number, biocollection,
          foreach($registry['biocollection'] as $key => $value) {
            $biocollection_number = null;
            $biocollection_type = isset($value['biocollection_type']) ? $value['biocollection_type']: 0;
            if (!in_array($biocollection_type,$validtypes_keys) and !in_array($biocollection_type,Biocollection::NOMENCLATURE_TYPE)) {
              break;
            } elseif (in_array((string) $biocollection_type,$validtypes_keys) ) {
              $kk = array_search($biocollection_type,$validtypes_keys);
              if ($kk===null) {
                break;
              } else {
                $biocollection_type = array_values($validtypes)[$kk];
              }
            }
            if (is_array($value)) {
                $biocollection_id = isset($value['biocollection_code']) ? $value['biocollection_code'] :
                (isset($value['biocollection']) ? $value[ 'biocollection'] : null);
                $biocollection_number = !isset($value['biocollection_number']) ? (isset($value[1]) ? $value[1] : null) : $value['biocollection_number'];
            } else {
               $biocollection_id = $value;
            }
            $query = Biocollection::select(['biocollections.id', 'acronym', 'biocollections.name', 'irn'])->where(function($q) use($user_id) {
              $q->doesntHave('users')->orWhereHas('users',function($u)  use($user_id)  { $u->where('users.id',$user_id);});
            });
            $valid = ODBFunctions::validRegistry($query, $biocollection_id, ['id','acronym','name','irn']);
            if (!$valid) {
              break;
            }
            $biocollections[] = [
              'biocollection_id' => $valid->id,
              'biocollection_number' => $biocollection_number,
              'biocollection_type' => $biocollection_type,
            ];
          }
        }
        if (count($biocollections)==0) {
          return false;
        }
        $registry['biocollections'] = $biocollections;
        return true;
    }


    protected function extractDate(&$registry) {
        $date = isset($registry['date']) ? $registry['date'] : (isset($this->header['date']) ? $this->header['date'] : null);
        if (null == $date) {
          $year = isset($registry['date_year']) ? $registry['date_year'] : (isset( $registry['year']) ? $registry['year'] : null);
          $month = isset($registry['date_month']) ? $registry['date_month'] : (isset( $registry['month']) ? $registry['month'] : null);
          $day = isset($registry['date_day']) ? $registry['date_day'] : (isset( $registry['day']) ? $registry['day'] : null);
          if ($year or $month or $day) {
            $date = [$month,$day,$year];
          }
        }
        if ($date == null) {
          return;
        }
        if (is_string($date)) {
          if (preg_match("/\//",$date)) {
              $date = explode("/",$date);
              $date = [$date[1],$date[2],$date[0]];
          } elseif (preg_match("/-/",$date)) {
              $date = explode("-",$date);
              $date = [$date[1],$date[2],$date[0]];
          }
        } elseif (!is_array($date)) {
          if (get_class($date)==="DateTime") {
             $year = $date->format('Y');
             $day = $date->format('d');
             $month = $date->format('m');
             $date = [$month,$day,$year];
          }
        }
        if (count(array_filter($date))>0) {
          $registry['date'] = $date;
        } else {
          $registry['date'] = null;
        }
    }

    public function validateBibReferences(&$registry)
    {
        if (!isset($registry['bibreferences']) or null == $registry['bibreferences']) {
            return true;
        }
        $bibreferences = $registry['bibreferences'];
        $bibrefs = [$bibreferences];
        if (strpos($bibreferences, '|') !== false) {
            $bibrefs = explode('|', $bibreferences);
        } else {
            if (strpos($bibreferences, ';') !== false) {
              $bibrefs = explode(';', $bibreferences);
            } else {
              if (strpos($bibreferences, ',') !== false) {
                $bibrefs = explode(',', $bibreferences);
              }
            }
        }
        $bibrefs = array_unique($bibrefs);
        $ids =[];
        foreach($bibrefs as $bib) {
          if (is_numeric($bib)) {
            $found = BibReference::where('id',$bib);
          } else {
            $found = BibReference::whereRaw('odb_bibkey(bibtex) = ?',[$bib]);
          }
          if ($found->count()==1) {
            $ids[] = $found->first()->id;
          }
        }
        if (count($ids)==count($bibrefs)) {
          $registry['bibreferences'] = $ids;
          return true;
        }
        $registry['bibreferences'] = null;
        return false;
    }


        public function validateBiocollection(&$registry,$mandatory=true)
        {
          /* if update this is not mandatory*/
          if (!isset($registry['biocollection']) and !$mandatory) {
            return true;
          }
          /* check if the user is authorized */
          $user_id = Auth::user()->id;
          $query = Biocollection::select(['biocollections.id', 'acronym', 'biocollections.name', 'irn'])->where(function($v) use($user_id) {
            $v->doesntHave('users')->orWhereHas('users',function($u) use($user_id){ $u->where('users.id',$user_id);});
          });
          $fields = ['id', 'acronym', 'name', 'irn'];
          $valid = ODBFunctions::validRegistry($query, $registry['biocollection'], $fields);
          if (!$valid) {
            $this->skipEntry($registry,'You informed an invalid Biocollection reference or you do not have permissions: '.$registry['biocollection']);
            return false;
          }
          $registry['biocollection_id'] = $valid->id;

          $biocollection_type =  isset($registry['biocollection_type']) ? ((null != $registry['biocollection_type']) ? $registry['biocollection_type'] : 0) : 0;
          if (!in_array($biocollection_type,Biocollection::NOMENCLATURE_TYPE)) {
            $validtype = null;
            $btype = mb_strtolower($biocollection_type);
            $locales =  config("app.available_locales");
            foreach (Biocollection::NOMENCLATURE_TYPE as $type) {
              foreach($locales as $locale) {
                $possibletype = mb_strtolower(Lang::choice('levels.vouchertype.'.$type,1,[],$locale));
                if ($possibletype==$btype) {
                  $validtype = $type;
                  break;
                }  
              }  
            }             
            //not found in translations nor as numeric return
            if (null == $validtype) {
              $this->skipEntry($registry,'You informed an invalid NOMENCLATURE_TYPE reference: '.$biocollection_type);
              return false;
            }
            $registry['biocollection_type'] = $validtype;
          }
          if (isset($registry['biocollection_number'])) {
            $number = $registry['biocollection_number'];
            $biocollection_id = $registry['biocollection_id'];
            $exists = Voucher::where('biocollection_id',$biocollection_id)->where('biocollection_number',$number)->where('id','<>',$biocollection_id);
            if ($exists->count() >0 ) {
              $this->skipEntry($registry,'There is already another Voucher ['.$exists->first()->fullname.'] in this biocollection with biocollection_id='.$number);
              return false;
            }
          }
          return true;
         }

         /* function for individual locations */
         protected function validateIndividual(&$entry)
         {
           $individual = isset($entry['individual_id']) ? $entry['individual_id'] : (
              isset($this->header['individual_id']) ? $this->header['individual_id'] : (
                isset($entry['individual']) ? $entry['individual'] : null
                )
             );
           if ($individual!=null) {

               if (((int) $individual) != null) {
                 $valid = Individual::where('id',$individual)->get();
               } else {
                 $valid = Individual::whereRaw('odb_ind_fullname(individuals.id,individuals.tag) LIKE "'.$individual.'"')->get();
               }
               if ($valid->count()==1) {
                 $validindividual = $valid->first();
                 $entry['individual_id'] = $validindividual->id;
                 $user = Auth::user();
                 if (!$user->can('update', $validindividual)) {
                   $this->appendLog('ERROR: You do not have permissions to add locations to individual'.$validindividual->fullname);
                   return false;
                 }
                 return true;
               } else {
                 $this->appendLog('ERROR: individual '.$individual." not valid or you do not have permissions ");
                 return false;
               }
           }
           $this->appendLog('ERROR: individual MUST be informed');
           return false;
         }

         public function extractLocationFields($entry,$oldregistry=null)
         {

           $possible_keys = ['location','longitude','latitude','notes','date_time','altitude','x','y','distance','angle'];
           $record =  [
           'id' => isset($entry['id']) ? $entry['id']  : null,
           'location' => isset($entry['location_id']) ? $entry['location_id'] : 
           (isset($entry['location']) ? $entry['location'] : ((isset($oldregistry) and (!isset($entry['longitude']) or !isset($entry['latitude']))) ? $oldregistry->location_id : null)),
           'latitude' => isset($entry['latitude']) ? (float) $entry['latitude'] : null,
           'longitude' => isset($entry['longitude']) ? (float) $entry['longitude'] : null,
           'altitude' => isset($entry['altitude']) ? (float) $entry['altitude'] : (isset($registry['altitude']) ? (float) $registry['altitude'] : (isset($oldregistry) ? $oldregistry->altitude : null)),
           'notes' => isset($entry['notes']) ? $entry['notes'] : (isset($registry['location_notes']) ? $registry['location_notes'] : (isset($oldregistry) ? $oldregistry->notes : null)),
           'date_time' => isset($entry['date_time']) ? $entry['date_time'] : (isset($registry['location_date_time']) ? $registry['location_date_time'] : (isset($oldregistry) ? $oldregistry->date_time : null)),
           'x' => isset($entry['x']) ? (float) $entry['x'] : (isset($registry['x']) ? (float) $registry['x'] : (isset($oldregistry) ? $oldregistry->x : null)),
           'y' => isset($entry['y']) ? (float) $entry['y'] : (isset($registry['y']) ? (float) $registry['y'] : (isset($oldregistry) ? $oldregistry->y : null)),
           'distance' => isset($entry['distance']) ? (float) $entry['distance'] : (isset($registry['distance']) ? (float) $registry['distance'] : (isset($oldregistry) ? $oldregistry->distance : null)),
           'angle' => isset($entry['angle']) ? (float) $entry['angle'] : (isset($registry['angle']) ? (float) $registry['angle'] : (isset($oldregistry) ? $oldregistry->angle : null)),
           ];
           $record = array_filter($record,function($q){
            return !is_null($q);
           });
           $newentry= [];
           $newentry['individual_id'] = $entry['individual_id'];
           $newentry['location'] = $record;
           return $newentry;
         }




}
