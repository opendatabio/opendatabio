<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Location;
use App\Models\LocationRelated;
use App\Models\ODBFunctions;
use JsonMachine\JsonMachine;

use DB;
use File;
use Lang;
use Illuminate\Support\Arr;
use Response;

class LocationsJobs extends AppJob
{

  public function validateAdmLevel(&$location)
  {
    $admLevel = (int) $location['adm_level'];
    $validLevels = array_merge(
      (array) config('app.adm_levels'),
      Location::LEVEL_SPECIAL);
      if (!in_array($admLevel,$validLevels)) {
          return false;
      }
    return true;
  }

  public function validateAltitude(&$location) {
    $altitude = isset($location['altitude']) ? $location['altitude'] : null;
    if ($altitude) {
        $altitude = filter_var ($altitude, FILTER_VALIDATE_INT );
        if($altitude === false or $altitude < 0){
          $this->skipEntry($location,"Informed altitude ".$location['altitude']." is not a valid integer");
          return false;
        }  
    }
    return true;
  }



  public function validateInformedParent(&$location)
  {
    $parent = isset($location['parent']) ? $location['parent'] : (isset($location['parent_id']) ? $location['parent_id'] : (isset($location['parent_name']) ? $location['parent_name'] : null ) );
    if (null != $parent) {
      /* if interger, then must be the id */
      if (((int) $parent) >0) {
        $infomedParent = Location::where('id',$parent);
      } else {
        $infomedParent = Location::where('name','like',$parent);
      }
      $maxadmin = $location['adm_level'];
      if ($maxadmin==Location::LEVEL_PLOT) {
          $maxadmin = $maxadmin+1;
      }
      $infomedParent = $infomedParent->where('adm_level','<',$maxadmin);
      if ($infomedParent->count()!=1) {
        $locationLog = $location;
        $locationLog['geom'] = substr($locationLog['geom'],0,20);
        $this->skipEntry($locationLog,"Informed Parent ".$parent." for location ".$location['name']." was not found in the database or location adm_level is not greater than parent adm_level. Or there are multiple parents with the same name. Try without parent specification.");
        return false;
      }
      $location['parent'] = $infomedParent->first()->id;
    }
    return true;
  }

  public function validateDimensions(&$location,$oldlocation=null)
  {
    $adm_level = isset($location['adm_level']) ? $location['adm_level'] : (isset($oldlocation) ? $oldlocation->adm_level : null);
    if ($adm_level != Location::LEVEL_PLOT) {
      return true;
    }
    if (!isset($oldlocation)  and (!isset($location['x']) or $location['x']==0 or !isset($location['y']) or $location['y']==0)) {
      $locationLog = $location;
      $locationLog['geom'] = substr($locationLog['geom'],0,20);
      $this->skipEntry($locationLog,'Plot x or y dimension missing or 0');
      return false;
    }
    /* subplot dimensions */
    $parent = isset($location['parent']) ? $location['parent'] : (isset($oldlocation) ? $oldlocation->parent_id : null);
    if (isset($parent)) {
      $parent = Location::withGeom()->findOrFail($parent);
      if ($parent->adm_level==Location::LEVEL_PLOT) {
        $parsx = isset($parent->startx) ? $parent->startx : 0;
        $parsy = isset($parent->starty) ? $parent->starty : 0;

        $startx = isset($location['startx']) ? $location['startx'] : (isset($oldlocation) ? $oldlocation->startx : null);
        $starty = isset($location['starty']) ? $location['starty'] : (isset($oldlocation) ? $oldlocation->starty : null);
        $x = isset($location['x']) ? $location['x'] : (isset($oldlocation) ? $oldlocation->x : null);
        $y = isset($location['y']) ? $location['y'] : (isset($oldlocation) ? $oldlocation->y : null);

        $condition1 = (!isset($startx) and isset($oldlocation));
        $condition2 = (isset($startx) and isset($x)) ? ($startx+$x)>($parent->x+$parsx) : false;
        $condition3 = (!isset($starty) and isset($oldlocation));
        $condition4 = (isset($starty) and isset($x)) ? ($starty+$y)>($parent->y+$parsy) : false;
        if ($condition1 or $condition2 or $condition3 or $condition4) {
          $locationLog = $location;
          $locationLog['geom'] = substr($locationLog['geom'],0,20);
          $this->skipEntry($locationLog,'Subplot x,y, startx or starty, is missing or invalid');
          return false;
        }
      }
    }
    return true;
  }


      protected function validateGeom(&$location,$oldlocation=null,$justvalidation=false)
      {
          /* if not set, then must be a point */
          $hasgeometry = isset($location['geom']) ? (trim($location['geom'])!="" ? $location['geom'] : null) : null;
          $mandatory = isset($oldlocation) ? false : true;
          if ($hasgeometry===null) {
            /* and we expect lat and long attributes for location */
            $lat = isset($location['lat']) ? $location['lat'] : (isset($location['latitude']) ? $location['latitude'] : null);
            $long = isset($location['long']) ? $location['long'] : (isset($location['longitude']) ? $location['longitude'] : null);
            if (is_null($lat) or is_null($long)) {
              /* create geometry if subplot */
              $geom = self::subplotGeometry($location,$oldlocation);
              if (!$geom and $mandatory) {
                $name = isset($location['name']) ? $location['name'] : "";
                $this->skipEntry($location, "Coordinates for location ".$name." not available");
                return false;
              } elseif ($geom) {
                $location['geom'] = $geom;
              }
            } else {
              $location['geom'] = "POINT($long $lat)";
            }
          }
          /* a shorter geometry for logging */
          $locationLog = $location;
          if (isset($location['geom'])) {
            $locationLog['geom'] = substr($locationLog['geom'],0,50);
          } elseif (!$mandatory) {
            return true; //when updating locations and not informing geometries
          }
          $locationLog['geojson'] = null;

          $geom = $location['geom'];
          /* get the geometry type from the string submitted */
          $geomType = self::geomTypeFromGeom($location['geom']);
          if($geomType == null) {
            $this->skipEntry($locationLog, "Invalid geometry type for this location");
            return false;
          }

          //check if geometry is valid
          //Understand https://dev.mysql.com/doc/refman/8.0/en/geometry-well-formedness-validity.html
          //but mariadb does not have st_valid of mysql, so we can evaluate some of these issues only //

          /* 1. If geometry conversion from text pass, then is valid*/
          try {
            $validGeom = DB::statement("SELECT ST_GeomFromText('".$geom."')");
          } catch (\Exception $e) {
            $this->skipEntry($locationLog,Lang::get('messages.geometry_invalid'));
            return false;
          }
          // MariaDB returns 1 for invalid geoms from ST_IsEmpty ref: https://mariadb.com/kb/en/mariadb/st_isempty/
          $invalid = DB::select("SELECT ST_IsEmpty(ST_GeomFromText('$geom')) as val");
          $invalid = count($invalid) ? $invalid[0]->val : 1;
          if ($invalid) {
            $this->skipEntry($locationLog,Lang::get('messages.geometry_invalid'));
            return false;
          }

          /* 2 validate by calculating the area and the centroid if polygon or multipolygon*/
          /* because these are included in the queries */
          if (in_array($geomType,['POLYGON','MULTIPOLYGON'])) {
            /* this functions should work for these geometries otherwise is an error */
            /* area */
            try {
              $area  = DB::select("SELECT ST_Area(ST_GeomFromText('".$geom."')) as area");
            } catch (\Exception $e) {
              $this->skipEntry($locationLog,"Could not calculate ST_Area of the informed polygon");
              return false;
            }
            $area = count($area) ? $area[0]->area : null;
            if ($area===null) {
              $this->skipEntry($locationLog,"Could not calculate ST_Area of the informed polygon");
              return false;
            }
            /*centroid */
            try {
              $centroid  = DB::select("SELECT ST_AsText(ST_Centroid(ST_GeomFromText('".$geom."'))) as centroid");
            } catch (\Exception $e) {
              $this->skipEntry($locationLog,"Could not calculate the ST_Centroid for the informed geometry");
              return false;
            }
            $centroid = count($centroid) ? $centroid[0]->centroid : null;
            if ($centroid===null) {
              $this->skipEntry($locationLog,"Could not calculate the ST_Centroid for the informed geometry");
              return false;
            }
          }
          // TODO: implemente validation for linestrings (transects)
          if (in_array($geomType,['LINESTRING'])) {
            try {
              $start_point  = DB::select("SELECT ST_AsText(ST_StartPoint(ST_GeomFromText('".$geom."'))) as start_point");
            } catch (\Exception $e) {
              $this->skipEntry($locationLog,"Could not calculate the ST_StartPoint for the informed geometry");
              return false;
            }
            $start_point = count($start_point) ? $start_point[0]->start_point : null;
            if ($start_point==null) {
              $this->skipEntry($locationLog,"Could not calculate the ST_StartPoint for the informed geometry");
              return false;
            }
          }

          // finally check if this exact geometry is already registered
          //$alreadyPresent= Location::noWorld()->whereRaw("ST_AsText(geom) LIKE '".$geom."'")->count();
          if (!$justvalidation) {
            $alreadyPresent = Location::whereRaw("ST_Equals(geom,ST_GeomFromText('$geom')) > 0")->count();
            if ($alreadyPresent>0) {
                $this->skipEntry($locationLog,Lang::get('messages.geom_duplicate'));
                return false;
            }
            /* if plot or transect informed as points, define geometry for storage*/
            $angle = isset($location['azimuth']) ? $location['azimuth'] : 0;
            $angle = $angle>=360 ? ($angle-360) : $angle;
            if ($location['adm_level']==Location::LEVEL_TRANSECT and $geomType=='POINT') {
              $geom = Location::generate_transect_geometry($geom,$location['x'],$angle);
              $location['geom'] = $geom;
              $location['as_point'] = 1;
            } elseif ($location['adm_level']==Location::LEVEL_PLOT and $geomType=='POINT') {
              if (isset($location['parent'])) {
                $parent = Location::withGeom()->findOrFail($location['parent']);
                /* if subplot angle must fit parent geometry and is retrieved from there */
                if ($parent->adm_level==Location::LEVEL_PLOT) {
                  $parent_wkt = $parent->footprintWKT;
                  $pattern = '/\\(|\\)|POLYGON|\\n/i';
                  $coordinates = preg_replace($pattern, '', $parent_wkt);
                  $coordinates = explode(",",$coordinates);
                  $coordA = "POINT(".$coordinates[0].")";
                  $coordB = "POINT(".$coordinates[1].")";
                  $geotools = new \League\Geotools\Geotools();
                  $coordA   = new \League\Geotools\Coordinate\Coordinate(Location::latlong_from_point($coordA));
                  $coordB   = new \League\Geotools\Coordinate\Coordinate(Location::latlong_from_point($coordB));
                  $angle    =  $geotools->vertex()->setFrom($coordA)->setTo($coordB)->initialBearing();
                }
              }
              $geom = Location::generate_plot_geometry($geom,$location['x'],$location['y'],$angle);
              $location['geom'] = $geom;
              $location['as_point'] = 1;
            }
          }
          return true;
      }

      public function geomTypeFromGeom($geom)
      {
        $geom_type = DB::select(DB::raw('SELECT ST_GeometryType(ST_GeomFromText("'.$geom.'")) as gg'))[0]->gg;
        $strGeom = mb_strtoupper($geom_type);
        if (in_array($strGeom,Location::VALID_GEOMETRIES)) {
          return $strGeom;
        }
        return null;
      }

      public static function subplotGeometry($location,$oldlocation=null)
      {
        $adm_level = isset($location['adm_level']) ? $location['adm_level'] : (isset($oldlocation) ? $oldlocation->adm_level : null);
        if ($adm_level != Location::LEVEL_PLOT) {
          return false;
        }
        $parent = isset($location['parent']) ? $location['parent'] : (isset($oldlocation) ? $oldlocation->parent_id : null);
        if (!is_null($parent)) {
          $parent = Location::withGeom()->findOrFail($parent);
          if ($parent->adm_level==Location::LEVEL_PLOT) {
            /* get the 0,0 coordinates for a subplot given the x and y positions in parent */
            return Location::individual_in_plot($parent->footprintWKT,$location['startx'],$location['starty']);
          }
        }
        return false;
      }

      public function adjustAdmLevel(&$location, $oldlocation=null)
      {
        $parent =  isset($location['parent']) ? $location['parent'] : null;
        $locname = isset($location['name']) ? $location['name'] : (isset($oldlocation) ? $oldlocation->name : null);
        $adm_level =  isset($location['adm_level']) ? $location['adm_level'] : (isset($oldlocation) ? $oldlocation->adm_level: null);
        if (!is_null($parent)) {
          $parent = Location::withGeom()->findOrFail($parent);
          $p_adm_level = $parent->adm_level;
          if ($adm_level <= $p_adm_level and in_array($adm_level,config('app.adm_levels'))) {
            $msg = $locname.'  adm_level '.$adm_level.' is equal or smaller than its parent '.$parent->name.' adm_level '.$p_adm_level;
            $new_level = $adm_level+1;
            if (!in_array($new_level,config('app.adm_levels'))) {
              $msg .= "The new location CANNOT BE registered as adm_level ".$new_level." because this is not set in the config. Ask the administrator to add an additional adm_level to config/app!";
              $locationLog = $location;
              $locationLog['geom'] = substr($locationLog['geom'],0,20);
              $this->skipEntry($locationLog,$msg);
              return false;
            }
            $location['adm_level'] = $new_level;
            $msg .= " The location was registered as adm_level ".$new_level;
            $this->appendLog($msg);
          }
        }

        return true;
      }


      protected function validateParent(&$location,$oldlocation=null)
      {
          $parent = isset($location['parent']) ? $location['parent'] : (isset($location['parent_id']) ? $location['parent_id'] : null );
          $location['parent'] = $parent;

          /* in case updating */
          $mandatory=true;
          if (null==$parent and null!=$oldlocation and !isset($location['geom'])) {
            return true;
          }
          if (null != $oldlocation) {
            $mandatory = false;
          }
          /* updating conditions */

          if (!$this->validateParentValues($location,$mandatory,$oldlocation)) {
              return false;
          }
          $parent =  isset($location['parent']) ? $location['parent'] : (isset($oldlocation) ? $oldlocation->parent_id: null);
          $locname = isset($location['name']) ? $location['name'] : (isset($oldlocation) ? $oldlocation->name : null);
          if (null != $locname) {
            $sameName = Location::where('name', 'like', $locname)->where('parent_id', '=', $parent);
            if (isset($oldlocation)) {
              $sameName = $sameName->where("id","<>",$oldlocation->id);
            }
            if ($sameName->count() > 0) {
              $locationLog = $location;
              $locationLog['geom'] = substr($locationLog['geom'],0,20);
              $this->skipEntry($locationLog, 'location '.$locname.' already exists in database at same parent location. Must be unique within parent');
              return false;
            }
          }
          return true;
      }

      /* $oldgeometry must be informed if updating parent */
      protected function validateParentValues(&$location,$mandatory=true,$oldlocation=null)
      {
           /* do not check uc if location is country */
           if ($location['adm_level'] == config('app.adm_levels')[0] and $mandatory) {
             $location['parent'] = Location::world()->id;
             return true;
           }

           /* check whether parent contains the geometry of the location
           * if updating a new geometry and not a parent, must use the old parent
           * if a new parent and not a new geometry, must use the old geometry to validate
           */
           $parent =  isset($location['parent']) ? $location['parent'] : (isset($oldlocation) ? $oldlocation->parent_id: null);
           $currentgeometry = isset($location['geom']) ? $location['geom'] : (isset($oldlocation) ? $oldlocation->footprintWKT: null);
           $informedParent = null;
           if (null != $parent) {
             $informedParent = Location::withGeom()->findOrFail($parent);
             if ($informedParent->adm_level != Location::LEVEL_TRANSECT) {
               $parent_dim = !is_null($informedParent->x) ? (($informedParent->x >= $informedParent->y) ? $informedParent->x : $informedParent->y) : null;
             } else {
               /* transects the buffer is the y parameter */
               $parent_dim = !is_null($informedParent->y) ? $informedParent->y : null;
             }
             $parent_geom = $informedParent->footprintWKT;
             if (!is_null($parent_dim)) {
               /* add a buffer to parent point in the ~ size of its dimension if set */
               $buffer_dd = (($parent_dim*0.00001)/1.11);
               $query_buffer ="ST_Buffer(ST_GeomFromText('".$parent_geom."'), ".$buffer_dd.")";
             } else {
               /* else use config buffer */
               if ($informedParent->adm_level == config('app.adm_levels')[0]) {
                 //if country, allow bigger buffer
                 $buffer_dd = 0.2;
                 $query_buffer ="ST_Buffer(ST_GeomFromText('".$parent_geom."'),".$buffer_dd.")";
               } else {
                 //the config buffer
                 $buffer_dd = config('app.location_parent_buffer');
                 $query_buffer ="ST_Buffer(ST_GeomFromText('".$parent_geom."'), ".$buffer_dd.")";
               }
             }
            //test without buffer nor simplification
            $query = "SELECT ST_Within(ST_GeomFromText('".$currentgeometry."'),ST_GeomFromText('".$parent_geom."')) as isparent";
            $isparent = DB::select($query);
            //if not valid, test with buffer
            if ($isparent[0]->isparent) {
              return true ;
            }
            //test with buffered parent
            $query = "SELECT ST_Within(ST_GeomFromText('".$currentgeometry."'),".$query_buffer.") as isparent";
            $isparent = DB::select($query);
            if ($isparent[0]->isparent) {
              return true ;
            }
            //if still not fall within buffered parent, then accept only if ismarine informed
            $ismarine = isset($location['ismarine']) ? ($location['ismarine'] != null) : false;
            if ($ismarine and in_array($location['adm_level'], Location::LEVEL_SPECIAL)) {
              $this->appendLog("WARNING: Location ".$location['name']." does not fall within parent ".$parent." but the relation was established because you informed to be a marine location.");
              return true;
            }
            //else informed parent is invalid
            $locationLog = $location;
            $locationLog['geom'] = substr($locationLog['geom'],0,20);
            $this->skipEntry($locationLog,"Location parent ".$parent." is not a valid parent for this location. Not even with considering a buffer around it.");
            return false;
          } elseif ($mandatory) {
            /* if parent was not informed try to guess */
            $guessedParent = $this->guessParent($currentgeometry,$location['adm_level']);
            /* if still not guessed, then it cannot be imported and nothing to do*/
            if (null == $guessedParent) {
              $locationLog = $location;
              $locationLog['geom'] = substr($locationLog['geom'],0,20);
              $this->skipEntry($locationLog,"Location parent was not detected nor informed. Only adm_level ".config('app.adm_levels')[0]." and can be imported without parent assignment.");
              return false;
            }
            $location['parent'] = $guessedParent;
          }
          return true;
      }

      protected function guessParent($geom, $adm_level,$maxdim=0)
      {
          if (config('app.adm_levels')[0] == $adm_level) {
              return Location::world()->id;
          }
          //DETECT ST_WITHIN parent
          $parent = Location::detectParent($geom, $adm_level, null, $ignore_level=false,$parent_buffer=0);
          if ($parent) {
            return $parent->id;
          }
          // IF STILL NOT FOUND TRY IGNORING ADMIN LEVEL
          $parent = Location::detectParent($geom, $adm_level, null, $ignore_level=1,$parent_buffer=0);
          if ($parent) {
              $this->appendLog("Parent detected but of different adm_level than expected");
              return $parent->id;
          }
          return null;
      }


      /* on update this will be the case if a geometry is specified */
      protected function validateOtherParents(&$location,$oldlocation=null)
      {
        if (!isset($location['geom']) and !isset($location['related_locations']) and isset($oldlocation)) {
          return true;
        }
        /* else if a new geometry is informed, then must detect parents */
        $adm_level = isset($location['adm_level']) ? $location['adm_level'] : (isset($oldlocation) ? $oldlocation->adm_level : null);
        $detected_related = null;
        if (isset($location['geom'])) {
          $detected_related = Location::detectRelated($location['geom'],$adm_level,true);
          $geom = $location['geom'];
        } else {
          /* then is updating and informed related_locations
           * needs current geometry to validate
          */
          $geom = $oldlocation->footprintWKT;
        }
        //validate informed if any
        $related = [];
        if (isset($location['related_locations'])) {
            $informed_related = explode(",",$location['related_locations']);
            foreach($informed_related as $loc) {
              $valid = ODBFunctions::validRegistry(Location::select('id'), $loc, ['id', 'name']);
              if (null === $valid) {
                  $this->skipEntry($location,'Informed other parent <strong>'.$loc.'</strong>not found');
                  break;
              }
              $query = "ST_Within(ST_GeomFromText('".$geom."'),geom) as inparent";
              $inparent = Location::selectRaw($query)->where('id',$valid->id)->get();
              /* validate geometry now */
              if ($inparent[0]->inparent) {
                $related[] = $valid->id;
              } else {
                #test with buffer
                $buffer_dd = config('app.location_parent_buffer');
                $inparent = DB::select("SELECT ST_Within(ST_GeomFromText('".$geom."'), ST_BUFFER(geom,".$buffer_dd.")) as inparent FROM locations where id=".$valid->id);
                if ($inparent[0]->inparent) {
                  $related[] = $valid->id;
                } else {
                  $this->skipEntry($location,'Location does not fall within informed <strong>'.$loc.'</strong>');
                  break;
                }
              }
            }
        }
        if ($detected_related != null) {
          $related = array_unique(array_merge($detected_related,$related));
        }
        if (count($related)>0) {
          $location['related_locations'] = $related;
          return true;
        } elseif (isset($location['related_locations'])) {
          return false;
        }
        return true;
      }



          /*
            *
            @jsonFeature is a Single Feature of a FeatureCollection
            return an array with keys for data import
          */
          public function parseGeoJsonFeature($jsonFeature)
          {
            $jsonFeature = json_decode(json_encode($jsonFeature), true);
            $jsonType = isset($jsonFeature['type']) ? mb_strtolower($jsonFeature['type']) : null;
            $errors = [];
            if (!$jsonType=='feature') {
              $this->skipEntry(
                  $jsonFeature,
                  "ERROR: geojson needs to contain a Feature"
                );
              return null;
            }
            /* which geometries are valid for this ODB instalation? */
            $geomType = mb_strtoupper($jsonFeature['geometry']['type']);
            if (!in_array($geomType,Location::VALID_GEOMETRIES)) {
                $this->skipEntry($jsonFeature,
                    " Invalid geometry".$geomType);
                return null;
            }

            $featureGeometry = $jsonFeature['geometry']['coordinates'];
            //if ($geomType == mb_strtolower(Location::GEOM_MULTIPOLYGON)) {
              //$featureGeometry = $featureGeometry[0];
            //}

            /* if MultiPolygon but with different type attribute*/
            if (count($featureGeometry)>1
                and $geomType != Location::GEOM_MULTIPOLYGON
            ) {
              /* removed this, because poligons with holes may come as polygons not multipolygons*/
              $geomType = Location::GEOM_MULTIPOLYGON;
              //$this->skipEntry($jsonFeature," Invalid geometry ".$geomType." should be ".Location::GEOM_MULTIPOLYGON);
              //return null;
            }
            /* if single geometry, then not a MultiPolygon */
            if (count($featureGeometry)==1
                and $geomType == Location::GEOM_MULTIPOLYGON)
            {
                /* only one polygon */
                $geomType = Location::GEOM_POLYGON;
            }

            /* create WKT geometry for odb import */
            /* if has multiple polygons */
            if (count($featureGeometry) >1) {
              $wktGeom = [];
              foreach($featureGeometry as $polygon) {
                  if (count($polygon)==1) {
                    $polygon = $polygon[0];
                  }
                  $wktPolygon = [];
                  foreach($polygon as $coordinates) {
                    //$this->appendLog("THE WRONG VALUE IS : has ".count($polygon[1])."  polygons which have ".count($coordinates)." but coordinates have ".count($coordinates[1]));
                    //if true then polygons have holes
                    if (count($coordinates)>2) {
                      $wktSubPol = [];
                      foreach ($coordinates as $subcoordinates) {
                        $wktSubPol[] =  implode(" ",$subcoordinates);
                      }
                      $wktSubPol = "(".implode(", ",$wktSubPol).")";
                      $wktPolygon[] =  $wktSubPol;
                    } else {
                      $wktPolygon[] =  implode(" ",$coordinates);
                    }
                  }
                  $wktPolygon = "((".implode(", ",$wktPolygon)."))";
                  $wktPolygon = str_replace("(((","((",$wktPolygon);
                  $wktPolygon = str_replace(")))","))",$wktPolygon);
                  $wktGeom[] = $wktPolygon;
              }
              $wktGeom = "(".implode(",",$wktGeom).")";
              $wktGeom = mb_strtoupper(Location::GEOM_MULTIPOLYGON).$wktGeom;
            }

            if ($geomType == Location::GEOM_POINT) {
              $wktGeom = count($featureGeometry[0])==2 ?  $featureGeometry[0] : $featureGeometry[0][0];
              if (count($wktGeom)==2) {
                $wktGeom = "(".implode(",",$wktGeom).")";
                $wktGeom = mb_strtoupper(Location::GEOM_POINT).$wktGeom;
              }
            }

            if ($geomType == Location::GEOM_POLYGON) {
              $polygon = count($featureGeometry[0])==1 ? $featureGeometry[0][0] : $featureGeometry[0];
              $wktPolygon = [];
              foreach($polygon as $coordinates) {
                //$wktPolygon[] =  implode(" ",$coordinates);
                if (count($coordinates)>2) {
                  $wktSubPol = [];
                  foreach ($coordinates as $subcoordinates) {
                    $wktSubPol[] =  implode(" ",$subcoordinates);
                  }
                  $wktSubPol = "(".implode(", ",$wktSubPol).")";
                  $wktPolygon[] =  $wktSubPol;
                } else {
                  $wktPolygon[] =  implode(" ",$coordinates);
                }
              }
              $wktGeom = mb_strtoupper(Location::GEOM_POLYGON);
              $wktGeom .= "((".implode(", ",$wktPolygon)."))";
              $wktGeom = str_replace("(((","((",$wktGeom);
              $wktGeom = str_replace(")))","))",$wktGeom);
            }

            /* get attributes if found */
            $properties = isset($jsonFeature['properties']) ? $jsonFeature['properties'] : null;
            if (null == $properties) {
              $this->skipEntry(
                $jsonFeature,
                "No properties attributes found for this geojson feature");
              return null;
            }
            $name = isset($properties['local_name']) ? $properties['local_name'] : (isset($properties['name']) ? $properties['name'] : null);
            $admLevel = isset($properties['adm_level']) ? $properties['adm_level'] : (isset($properties['admin_level']) ? $properties['admin_level'] : (isset($properties['admin_leve']) ? $properties['admin_leve'] : null));

            if (!isset($wktGeom)) {
              $this->skipEntry($jsonFeature,"There seem to be no geometry in this feature");
              return null;
            }
            $admLevel = (int) $admLevel;
            $parent = isset($properties['parent']) ? $properties['parent'] : null;

            /* if admLevel is the first configured, then link to World */
            if ($admLevel <= config('app.adm_levels')[0]) {
              $parent = Location::world()->id;
              $force_parent = 1;
            }
            $altitude = isset($properties['altitude']) ? $properties['altitude'] : null;
            $x = isset($properties['x']) ? $properties['x'] : null;
            $y = isset($properties['y']) ? $properties['y'] : null;
            $startx = isset($properties['startx']) ? $properties['starty'] : null;
            $starty = isset($properties['notes']) ? $properties['notes'] : null;
            $featureRecord = [
              'name' => $name,
              'adm_level' => $admLevel,
              'geom' => $wktGeom,
              'parent' => $parent,
              'x' => $x,
              'y' => $y,
              'startx' => $startx,
              'starty' => $starty,
              'geojson' => json_encode($jsonFeature),
            ];
            return $featureRecord;
          }


}
