<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Location;
use App\Models\LocationRelated;
use App\Models\ODBFunctions;
use Illuminate\Http\Request;
use DB;
use Lang;
use Auth;
use Log;

use Illuminate\Support\Arr;

class UpdateLocations extends LocationsJobs
{
    /**
     * Execute the job.
     */
    public function inner_handle()
    {
        $data = $this->extractEntrys();
        if (!$this->setProgressMax($data)) {
            return;
        }
        $this->affectedModel(Location::class);
        foreach ($data as $registry) {
            if ($this->validateData($registry)) {
              try {
                  $this->updateLocation($registry);
                } catch (\Exception $e) {
                  $this->setError();
                  $this->appendLog('Exception '.$e->getMessage().' at '.$e->getFile().'+'.$e->getLine().'HERE THAT IS THE ERROR ');
                }
              }
              if ($this->isCancelled()) {
                  break;
              }
              $this->userjob->tickProgress();
        }
        $this->userjob->save();
    }

    protected function validateData(&$registry)
    {
      $theid = isset($registry['location_id']) ? $registry['location_id'] : (isset($registry['id']) ? $registry['id'] : null);
      if (!isset($theid)) {
        $this->skipEntry($registry, 'Which Location? You must provide the location id to update!');
        return false;
      }
      $oldlocation = Location::findOrFail($theid);
      if ($oldlocation->count()==0) {
        $this->skipEntry($registry, 'Location with id '.$theid.' not found. Nothing to update.');
        return false;
      }
      if(!Auth::user()->can('update',$oldlocation)) {
        $this->skipEntry($registry, 'You are not authorized to update this record!');
        return false;
      }
      $registry['id'] = $theid;
      if (isset($registry['adm_level'])) {
        if (!$this->validateAdmLevel($registry)) {
            return false;
          }
      }

      /* will return true if not informed */
      if (!$this->validateInformedParent($registry)) {
          return false;
      }

      /* subplot validation */
      if (!$this->validateDimensions($registry,$oldlocation)) {
          return false;
      }

      /* validate new geometry if informed */
      if (!$this->validateGeom($registry,$oldlocation)) {
          return false;
      }

      /* validate new parent with geometry if new parent informed */
      if (!$this->validateParent($registry,$oldlocation)) {
          return false;
      }

      //second pass, for cases when parent is not informed but was detected above
      if (!$this->validateDimensions($registry,$oldlocation)) {
          return false;
      }

      if (!$this->validateOtherParents($registry,$oldlocation)) {
          return false;
      }

      if (!$this->adjustAdmLevel($registry,$oldlocation)) {
          return false;
      }
      return true;
    }


    public function updateLocation($registry)
    {

      /*create a update request */
      $thelocation = Location::withGeom()->findOrFail($registry['id']);

      /* clone original values if not provided */
      $update_request = [
        'from_the_api' => 1,
        'name' => (isset($registry['name']) ? $registry['name'] : $thelocation->name),
        'adm_level' => (isset($registry['adm_level']) ? $registry['adm_level'] : $thelocation->adm_level),
        'geom' => (isset($registry['geom']) ? $registry['geom'] : $thelocation->footprintWKT),
        'parent_id' => (isset($registry['parent']) ? $registry['parent'] : $thelocation->parent_id),
        'altitude' => (isset($registry['altitude']) ? $registry['altitude'] :  $thelocation->altitude),
        'notes' => (isset($registry['notes']) ? $registry['notes'] :  $thelocation->notes),
        'startx' => (isset($registry['startx']) ? $registry['startx'] :  $thelocation->startx),
        'starty' => (isset($registry['starty']) ? $registry['starty'] :  $thelocation->starty),
        'x' => (isset($registry['x']) ? $registry['x'] :  $thelocation->x),
        'y' => (isset($registry['y']) ? $registry['y'] :  $thelocation->y),
        'datum' => (isset($registry['datum']) ? $registry['datum'] :  $thelocation->datum),
        'as_point' => (isset($registry['as_point']) ? $registry['as_point'] :  $thelocation->as_point),
        'related_locations' => (isset($registry['related_locations']) ? $registry['related_locations'] :  null),
      ];
      $update_request = array_filter($update_request,function($a) { return null !== $a;});
      if (count($update_request)==0) {
        $this->skipEntry($registry,' Nothing to update for location '.$thelocation->name);
        return ;
      }
      //$this->skipEntry($update_request["notes"],' aqui '.$thelocation->name);
      //return ;

      //transform info in a request
      $saverequest = new Request;
      $saverequest = $saverequest->merge($update_request);

      //store the record
      $updatedLocation = app('App\Http\Controllers\LocationController')->update($saverequest,$thelocation->id);
      if (is_string($updatedLocation)) {
        $this->skipEntry($registry,' This location could not be updated. Possible errors may be: '.$updatedLocation);
        return ;
      }
      $this->affectedId($thelocation->id);
      return;
    }



}
