<?php
/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */
namespace App\Jobs;

use App\Models\Location;
use App\Models\Taxon;
use App\Models\Individual;
use App\Models\Measurement;
use App\Models\Voucher;
use App\Models\ODBFunctions;
use Auth;
use Activity;

class DeleteMany extends AppJob
{

    /**
     * Execute the job.
     *
     * @return void
     */
     public function inner_handle()
     {
         $data =  $this->extractEntrys();

         if (!$this->hasRequiredKeys(["ids_to_delete","model",], $data)) {
               return false;
         }
         //get ids to delete
         $ids = explode(",",$data['ids_to_delete']);
         rsort($ids);
         $ids_to_delete = collect($ids);
         $model = $data["model"];
         if (!$this->setProgressMax($ids_to_delete)) {
            return;
         }
         foreach ($ids_to_delete as $id) {
              if ($this->isCancelled()) {
                 break;
              }
              try {
                  $this->deleteObject($id,$model);
              } catch (\Exception $e) {
                  $this->setError();
                  $this->appendLog('Exception '.$e->getMessage());
              }
              $this->userjob->tickProgress();
              $this->affectedId($id);
        }

     }


     public function deleteObject($id,$model)
     {
       if ($model=="Activity") {
         $object = Activity::findOrFail($id);
       } else {
         $object = app("App\\Models\\".$model)::findOrFail($id);
         if (!Auth::user()->can('delete', $object)) {
            $name = ( null != $object->rawLink()) ? $object->rawLink() : $object->name;
            $this->appendLog('ERROR: you cannot delete '.$model." ".$name);
            return false;
         }
       }
       $object_name = $object->name;
       /* location needs special treatment */
       /* this will only run if location has childrenCount
       deleting location having children may be controlled
       by the location delete policy */
       if (mb_strtolower($model)=='location') {
         /* first update children parent id  if any */
         $children_ids = Location::where('parent_id',$id)->cursor()->pluck('id')->toArray();
         /* new parent will be current parent as location will be removed */
         $newparent = $object->parent_id;
         $oldparent = $object->name;
         $errors=[];
         if (count($children_ids)>0) {
           foreach($children_ids as $cid) {
               $tofix = Location::findOrFail($cid);
               $tofix->makeChildOf($newparent);
               $tofix->save();
               sleep(5);
               //this seems to be important with the baum makeChildOf function to finish
               // TODO: or is a bug within the baum package (replacement required)
               $tofix = Location::findOrFail($cid);
               if ($tofix->parent_id!==$newparent) {
                 $errors[]= $tofix->name." parent is still ".$tofix->parentName;
               } else {
                 //log parent difference
                 $tolog = array('attributes' => ['parent_id' => $newparent], 'old' => ['parent' => $oldparent]);
                 activity('location')
                   ->performedOn($tofix)
                   ->withProperties($tolog)
                   ->log('parent changed due removal');
               }
            }
         }
         if (count($errors)>0) {
           $this->appendLog('ERROR: Children parent change '.implode("<br>",$errors));
           return false;
         }
         sleep(5);
       }
       try {
           $object->delete();
       } catch (\Illuminate\Database\QueryException $e) {
           $this->appendLog('ERROR: was not able to delete '.$model." ".$object->name.": ".$e);
           return false;
       }

       //delete also logged activities for the resource
       if ($model!="Activity") {
         $activity = Activity::where("subject_type",'App\Models\\'.$model)->where("subject_id",$id);
         if ($activity->count()) {
           $activity->delete();
         }
       }
       $this->appendLog('WARNING:  '.$model." ".$object_name." deleted!");
       return true;
     }

}
