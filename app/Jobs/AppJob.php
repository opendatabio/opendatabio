<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserJob;
use App\Models\Biocollection;
use App\Models\ODBFunctions;
use App\Models\Taxon;


use DB;
use Log;
use Auth;

// All app jobs must extend this:
// This class intermediates between the jobs dispatched and the UserJob model
class AppJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $userjob;
    protected $errors;
    protected $header;

    /**
     * Create a new job instance.
     */
    public function __construct(UserJob $userjob)
    {
        $this->userjob = $userjob;
        $this->userjob->log = json_encode([]);
        $this->userjob->affected_ids = $this->userjob->affected_ids ? $this->userjob->affected_ids : json_encode([]);
        $this->userjob->save();
        $this->errors = false;
    }

    /**
     * Execute the job.
     */
    public function inner_handle()
    {
        // Virtual method!! Should be implemented by all jobs
        Log::info('Running inner handle');
        Log::info('id: #'.$this->job->getJobId().'#');
        Log::info('queue: #'.$this->job->getQueue().'#');
    }

    public function setError()
    {
        $this->errors = true;
    }

    public function appendLog($text)
    {
        $log = json_decode($this->userjob->fresh()->log, true);
        array_push($log, $text);
        $this->userjob->log = json_encode($log);
        $this->userjob->save();
        Log::info($text);
    }
    
    public function endJobStatus()
    {
      $this->userjob->setProgressMax(1);
      $this->userjob->progress=1;
      $this->userjob->save();
    }
    public function affectedId($id)
    {
        $ids = json_decode($this->userjob->fresh()->affected_ids, true);
        array_push($ids, $id);
        $this->userjob->affected_ids = json_encode($ids);
        $this->userjob->save();
    }

    public function affectedModel($model)
    {
        $this->userjob->affected_model = $model;
        $this->userjob->save();
    }


    public function handle()
    {
        // temporarily removing rollback capabilities:
        Auth::loginUsingId($this->userjob->user_id);
        $this->userjob->setProcessing();
        $this->userjob->job_id = $this->job->getJobId();
        $this->userjob->save();
        //	    DB::beginTransaction();
        try {
            $this->inner_handle();
            // mark jobs with reported errors as "Failed", EXCEPT if they have already been cancelled
            if ($this->errors and 'Cancelled' != $this->userjob->fresh()->status) {
                //			    DB::rollback();
                $this->userjob->setFailed();
            } else {
                //			    DB::commit();
                $this->userjob->setSuccess();
            }
        } catch (\Exception $e) {
            //			    DB::rollback();
            $this->appendLog('BLOCKING EXCEPTION '.$e->getMessage());
            Log::warning($e->getTraceAsString());
            $this->userjob->setFailed();
        }
    }

    public function extractEntrys()
    {
        $data = $this->userjob->data['data'];
        $this->header = isset($data['header']) ? $data['header'] : array();

        return $data['data'];
    }

    public function setProgressMax($data)
    {
        if (!count($data)) {
            $this->setError();
            $this->appendLog('ERROR: data received is empty!');

            return false;
        }
        $this->userjob->setProgressMax(count($data));

        return true;
    }

    public function isCancelled()
    {
        // calls "fresh" to make sure we're not receiving a cached object
        if ('Cancelled' == $this->userjob->fresh()->status) {
            $this->appendLog('WARNING: received CANCEL signal');

            return true;
        }

        return false;
    }

    public function removeHeaderSuppliedKeys(array $keys)
    {
        $notPresent = array();
        foreach ($keys as $key) {
            if (!isset($this->header[$key])) {
                $notPresent[] = $key;
            }
        }

        return $notPresent;
    }

    public function hasRequiredKeys($requiredKeys, $entry)
    {
        // if $entry is not an array it has not the $requiredKeys
        if (!is_array($entry)) {
            $this->skipEntry($entry, 'entry is not formatted as array');

            return false;
        }
        foreach ($requiredKeys as $key) {
            if (!isset($entry[$key]) or (null === $entry[$key])) {
                $this->skipEntry($entry, 'entry needs a '.$key);
                return false;
            }
        }

        return true;
    }

    public function skipEntry($entry, $cause)
    {
        if (is_array($entry)) {
            $entry = json_encode($entry,JSON_PRETTY_PRINT);
        } elseif ('object' == gettype($entry)) {
            //$entry = serialize($entry);
            $entry = json_encode($entry,JSON_PRETTY_PRINT);
        }
        $uid = uniqid();
        $entryShow = '<a data-bs-toggle="collapse" href="#a'.$uid.'" class="btn btn-warning btn-sm">this record</a>&nbsp;
        <div id="a'.$uid.'" class="collapse" ><pre>'.$entry.'</pre></div>';
        $this->appendLog('ERROR: '.$cause.'. Skipping import of '.$entryShow);
    }


    /* used in Persons jobs */
    public function validateBiocollectionSimple(&$registry)
    {
      /* not mandatory */
      if (!isset($registry['biocollection'])) {
        $registry['biocollection'] = null;
        return true;
      }

      /* check if the user is authorized */
      $user_id = Auth::user()->id;
      $query = Biocollection::select(['biocollections.id', 'acronym', 'biocollections.name', 'irn'])->where(function($v) use($user_id) {
        $v->doesntHave('users')->orWhereHas('users',function($u) use($user_id){ $u->where('users.id',$user_id);});
      });
      $fields = ['id', 'acronym', 'name', 'irn'];
      $valid = ODBFunctions::validRegistry($query, $registry['biocollection'], $fields);
      if (!$valid) {
        $this->skipEntry($registry,' You informed an invalid Biocollection reference or you do not have permissions to link to '.$registry['biocollection']);
        return false;
      }
      $registry['biocollection_id'] = $valid->id;
      return true;
    }

    public function validateTaxonsSimples(&$registry)
    {
      /* not mandatory */
      $taxons_ids = isset($registry['taxons']) ? $registry['taxons'] : (
        isset($registry['taxons_ids']) ? $registry['taxons_ids'] : (
            isset($registry['specialist']) ? $registry['specialist'] : null
          )
      );
      /* not mandatory */
      if (!isset($taxons_ids)) {
        return true;
      }
      $taxons = Taxon::select('*');
      $pattern = "/[,;|]/";
      $ids = preg_split($pattern, $taxons_ids);
      $ids = array_filter($ids,function($id) { return ((int)$id)>0; });
      if (count($ids)>0) {
          $taxons->whereIn('id',$ids);
      } else {
        $strs = preg_split($pattern, $taxons_ids);
        $strs = array_filter($strs,function($id) { return (((string)$id) != "") and ($id != null); });
        if (count($strs)>0) {
          ODBFunctions::advancedWhereIn($taxons,
                  'odb_txname(name, level, parent_id,0,0)',
                  implode(",",$strs),
                  true);
        }
      }
      if ($taxons->count()==0) {
        $this->skipEntry($registry,' You informed an invalid Taxon reference '.$taxons_ids);
        return false;
      }
      $registry['taxons_ids'] = $taxons->cursor()->pluck('id')->toArray();
      return true;
    }


}
