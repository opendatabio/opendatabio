<?php
/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use Illuminate\Http\Request;
use Spatie\SimpleExcel\SimpleExcelWriter;
use Illuminate\Support\Facades\Storage;
use Auth;
use App\Models\Biocollection;
use App\Models\BibReference;

use App\Queries\BibReferenceSearchQuery;
use App\Queries\IndividualLocationSearchQuery;
use App\Queries\IndividualSearchQuery;
use App\Queries\LocationSearchQuery;
use App\Queries\MeasurementSearchQuery;
use App\Queries\VoucherSearchQuery;
use App\Queries\TaxonSearchQuery;
use App\Queries\TraitSearchQuery;
use App\Queries\ActivitySearchQuery;
use App\Models\ActivityFunctions;
use App\Queries\PersonSearchQuery;

use Log;

class ExportData extends AppJob
{

    /**
     * Execute the job.
     *
     * @return void
     */
     public function inner_handle()
     {

        $data =  $this->extractEntrys();

        //which model to export
        $object_type = $data['object_type'];
        $endpoint = strtolower($object_type)."s";

        //records to export
        //if user selected records, this is set
        $params = isset($data['params']) ? $data['params'] : [];
        $export_ids = [];
        if (isset($data['export_ids'])) {
          $export_ids = explode(",",$data['export_ids']);
        } else {
          //if none informed get the scope from the list and export all that apply
          if (isset($data['filters'])) {
            foreach($data['filters'] as $key => $val) {
              $params[$key] = $val;
            }
          }
          if (isset($data['project'])) {
            $params['project'] = $data['project'];
          }
          if (isset($data['request_id'])) {
            $params['odbrequest_id'] = $data['request_id'];
          }
          if (isset($data['dataset'])) {
            $params['dataset'] = $data['dataset'];
          }
          if (isset($data['measured_type'])) {
            $params['measured_type'] = $data['measured_type'];
          }
          if (isset($data['trait'])) {
            $params['trait'] = $data['trait'];
          }
          if (isset($data['taxon_root'])) {
            $params['taxon_root'] = $data['taxon_root'];
          } elseif (isset($data['taxon'])) {
            $params['taxon'] = $data['taxon'];
          }
          if (isset($data['location_root'])) {
            $params['location_root'] = $data['location_root'];
          }
          if (isset($data['individual'])) {
            $params['individual'] = $data['individual'];
          }
          if (isset($data['voucher'])) {
            $params['voucher'] = $data['voucher'];
          }
          if (isset($data['person'])) {
            $params['person'] = $data['person'];
          }
          
          $params['fields'] = 'id'; 
          $request = new Request;
          $request = $request->merge($params);
          switch ($endpoint) {
          case "individuals":
            $prepared_query = IndividualSearchQuery::prepQuery($request);
            break;
          case "vouchers":
            $prepared_query = VoucherSearchQuery::prepQuery($request);
            break;
          case "bibreferences":
            $prepared_query = BibReferenceSearchQuery::prepQuery($request);
            break;  
          case "individual-locations":
            $prepared_query = IndividualLocationSearchQuery::prepQuery($request);  
            break;
          case "locations":
            $prepared_query = LocationSearchQuery::prepQuery($request);      
            break;
          case "measurements":
            $prepared_query = MeasurementSearchQuery::prepQuery($request);        
            break;
          case "taxons":
            $prepared_query = TaxonSearchQuery::prepQuery($request);        
            break;
          case "traits":
            $prepared_query = TraitSearchQuery::prepQuery($request);          
            break;
          case "activities":
            $prepared_query = ActivitySearchQuery::prepQuery($request);          
            break;  
          case "persons":
            $prepared_query = PersonSearchQuery::prepQuery($request);          
            break;    
          default:
            $prepared_query = [];
        }
          $query = $prepared_query['query'];
          $export_ids = $query->pluck('id')->toArray();
        }
           

           if (count($export_ids)>0) {
             $prepared_files =[] ;
             //chunk the records 
             $chunks = array_chunk($export_ids,1000);

             //will log progress on each chunk
             $this->setProgressMax($chunks);

             //create file to store the data (add the job id to the file name to be able to destroy it)
             $jobid = $this->userjob->id;
             $base_filename = "job-".$jobid."_".uniqid();
             $filename = $base_filename.".".$data['filetype'];
             $prepared_files[] = $filename;
             $path = 'app/public/downloads/'.$filename;
             $writer = SimpleExcelWriter::create(storage_path($path));

             //for each chunk get data from the api
             $counter = 1;
             $categories = [];                 
             $bibtexts = [];
             $activities = [];
             foreach($chunks as $ids) {
                //if user cancels job
                if ($this->isCancelled()) {
                  break;
                }
                //Log::info($counter."  chunk ");   

                $fields = $data['fields'];
                $lang = null;
                if (isset($data['params'])) {
                  $lang = isset($data['params']['language']) ?  $data['params']['language'] : null;
                }
                $params = ['fields' => $fields, 'id' => implode(',',$ids),'language' => $lang];
                $request = new Request;
                $request = $request->merge($params);
                switch ($endpoint) {
                  case "individuals":
                    $prepared_query = IndividualSearchQuery::prepQuery($request);
                    break;
                  case "vouchers":
                    $prepared_query = VoucherSearchQuery::prepQuery($request);
                    break;
                  case "bibreferences":
                    $prepared_query = BibReferenceSearchQuery::prepQuery($request);  
                    break;
                  case "individual-locations":
                    $prepared_query = IndividualLocationSearchQuery::prepQuery($request);  
                    break;
                  case "locations":
                    $prepared_query = LocationSearchQuery::prepQuery($request);      
                    break;
                  case "measurements":
                    $prepared_query = MeasurementSearchQuery::prepQuery($request);        
                    break;
                  case "taxons":
                    $prepared_query = TaxonSearchQuery::prepQuery($request);        
                    break;
                  case "traits":
                    $prepared_query = TraitSearchQuery::prepQuery($request);          
                    break;   
                  case "activities":
                    $prepared_query = ActivitySearchQuery::prepQuery($request);          
                    break; 
                  case "persons":
                    $prepared_query = PersonSearchQuery::prepQuery($request);          
                    break;      
                  default:
                    $prepared_query = [];
                }
                $fields = $prepared_query['fields'];                
                $query = $prepared_query['query'];               
                $collection = $query->cursor();
                if ($fields!="raw") {
                  if ($endpoint=="traits") {
                    $lang = $prepared_query['lang'];
                    $final = TraitSearchQuery::prepCategories($collection,$fields,$lang);
                    $newvalues = [];
                    foreach ($final as $key => $value) {
                        if (!empty($value['categories'])) {
                          $categories[] = $value['categories'];
                        }
                        unset($value['categories']);
                        $newvalues[] = $value;
                    }
                    $final = $newvalues;
                  } elseif ($endpoint=="activities") {
                    $lang = $prepared_query['lang'];
                    $fields = explode(",",$fields);
                    $final = [];
                    foreach($collection as $activity) {
                      $res = ActivityFunctions::exportProperties($activity,$lang);                      
                      $newres = [];
                      foreach($res as $change) {
                        foreach ($fields as $field) {
                          if (!in_array($field,["properties","created_at","updated_at"])) {                            
                            $change[$field] = isset($activity[$field]) ? $activity[$field] : null;
                          }                         
                        }          
                        $newres[] = $change;              
                      }
                      $final = array_merge($final,$newres);  
                    } 
                    //Log::info($final);
                    //$final = collect($final);
                  }
                  else {
                    $final = $collection->map(function ($obj) use ($fields) {
                      $result = [];
                      $fields_arr = explode(",",$fields);
                      foreach ($fields_arr as $field) {
                        $result[$field] = isset($obj[$field]) ? $obj[$field] : null;      
                      }                  
                      return $result;  
                    });
                  } 
                } else {
                  $final = $collection->map(function ($obj) {
                      return $obj->toArray();
                  });
                }
                $writer->addRows($final);
                if ($endpoint=="bibreferences") {
                  foreach($final as $reference)
                  {
                    $bibtexts[] = $reference['bibtex'];                    
                  }
                }               
                $this->userjob->tickProgress();
             }

             if (count($bibtexts)>0) {
              $filename_bib = $base_filename.".bib";
              $path = 'app/public/downloads/'.$filename_bib;
              $fn = fopen(storage_path($path),'w');
              fwrite($fn,implode("\n\n",$bibtexts));
              fclose($fn);   
              $prepared_files[] = $filename_bib;           
             }

             if (count($categories)) {
              $filename_cats = $base_filename."_categories.".$data['filetype'];
              $path_cats = 'app/public/downloads/'.$filename_cats;
              $writercat = SimpleExcelWriter::create(storage_path($path_cats));
              foreach($categories as $key => $cats) {
                foreach($cats as $cat) {
                   $writercat->addRow($cat);
                }
              }
              $prepared_files[] = $filename_cats;           
             }
                          
             //LOG THE FILE FOR USER DOWNLOAD
             $today = now();
             $tolog = "Requested data for <strong>".$object_type."</strong> prepared ";
             $tolog .= $today." contains:<br>";
             $files = collect($prepared_files)->map(function($filename){
              return "<a href='".url('storage/downloads/'.$filename)."' download >".$filename."</a>";
             })->toArray();
             $tolog  .= implode("<br>",$files);
             $this->appendLog($tolog);
            } else {
              $this->appendLog("Nothing to export or you don't have permissions");
            }
  }
}
