<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Person;
use App\Models\Biocollection;
use CodeInc\StripAccents\StripAccents;
use Illuminate\Http\Request;

class ImportPersons extends AppJob
{

    /**
       * The number of times the job may be attempted.
       * Only the Persons job, for some reason
       * @var int
       */
    public $tries = 1;


    /**
     * Execute the job.
     */


    public function inner_handle()
    {
        $data = $this->extractEntrys();
        if (!$this->setProgressMax($data)) {
            return;
        }
        $this->affectedModel(Person::class);

        foreach ($data as $person) {
            if ($this->isCancelled()) {
                break;
            }
            $this->userjob->tickProgress();

            if ($this->validateData($person)) {
                // Arrived here: let's import it!!
                try {
                    $this->import($person);
                } catch (\Exception $e) {
                    $this->setError();
                    $this->appendLog('Exception '.$e->getMessage().' at '.$e->getFile().'+'.$e->getLine().' on person '.$person['full_name'].$e->getTraceAsString());
                }
            }
        }
    }

    protected function validateData(&$person)
    {
        if (!$this->hasRequiredKeys(['full_name'], $person)) {
            return false;
        }

        if (!$this->validateAbbreviation($person)) {
            return false;
        }

        if (!$this->validateBiocollectionSimple($person)) {
            return false;
        }
        if (!$this->validateTaxonsSimples($person)) {
            return false;
        }

        if (isset($person['email'])) {
          if (!filter_var($person['email'], FILTER_VALIDATE_EMAIL)) {
            $this->skipEntry($person, ' Email invalid');
            return false;
          }
       }
        return true;
    }

    protected function validateAbbreviation(&$person)
    {
        if (!isset($person['abbreviation'])) {
            $names = explode(' ', mb_strtoupper($person['full_name']));
            $size = count($names);
            $abbreviation = $names[$size - 1].",";
            for ($i = 0; $i < $size - 1; ++$i) {
                $abbreviation = $abbreviation.' '.mb_substr($names[$i], 0, 1).".";
            }
            $person['abbreviation'] = $abbreviation;
        }
        return true;

    }

    public function import($registry)
    {
        $store_request = [
            'from_the_api' => 1,
            'full_name' => $registry['full_name'],
            'abbreviation' => $registry['abbreviation'],
            'email' => isset($registry['email']) ? $registry['email'] : null,
            'institution' => isset($registry['institution']) ? $registry['institution'] : null,
            'biocollection_id' => isset($registry['biocollection_id']) ? $registry['biocollection_id'] : null,
            'notes' => isset($registry['notes']) ? $registry['notes'] : null,
            'email_public' => isset($registry['email_public']) ? $registry['email_public'] : 0,
            'specialist' => isset($registry['taxons_ids']) ? $registry['taxons_ids'] : null,
        ];
        $store_request = array_filter($store_request,function($a) { return null !== $a;});


        //check of exact abbreviation duplicates
        $normalized_abbreviation = trim(mb_strtolower($registry['abbreviation']));
        $normalized_abbreviation = StripAccents::strip( (string) $normalized_abbreviation);
        $normalized_abbreviation = preg_replace('/[^a-zA-Z0-9]/', '', $normalized_abbreviation);
        $same = Person::all()->filter(function ($aperson) use ($normalized_abbreviation) {
              $nm_abbreviation = $aperson->abbreviation;
              $nm_abbreviation = trim(mb_strtolower($nm_abbreviation));
              $nm_abbreviation = StripAccents::strip( (string) $nm_abbreviation);
              $nm_abbreviation = preg_replace('/[^a-zA-Z0-9]/', '', $nm_abbreviation);
              return ($normalized_abbreviation==$nm_abbreviation);
        });
        if ($same->count()>0) {
            $this->skipEntry($registry, ' There are other Persons with this abbreviation. Found were '.implode(" | ",$same->pluck('abbreviation')->toArray()));
            return;
        }
        //transform info in a request
        $saverequest = new Request;
        $saverequest->merge($store_request);
        //$this->skipEntry($saverequest->all(),'error here');
        //return;
        //store the record, which will result in the individual
        $importedPerson = app('App\Http\Controllers\PersonController')->store($saverequest);
        if (is_string($importedPerson)) {
          $this->skipEntry($registry,'This Person could not be imported. Possible errors may be: '.$importedPerson);
          return ;
        }
        $this->affectedId($importedPerson->id);
        return;
    }
}
