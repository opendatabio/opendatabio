<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Jobs;

use App\Models\Taxon;
use App\Models\ExternalAPIs;
use App\Models\Person;
use App\Models\ODBFunctions;
use App\Models\BibReference;


use RenanBr\BibTexParser\Listener;
use RenanBr\BibTexParser\Parser;
use RenanBr\BibTexParser\Processor;
use RenanBr\BibTexParser\ParseException;


use Lang;
use Log;
use Illuminate\Http\Request as therequest;


class ImportTaxons extends AppJob
{
    /**
     * Execute the job.
     */
    public function inner_handle()
    {
        $data = $this->extractEntrys();
        if (!$this->setProgressMax($data)) {
            return;
        }
        $this->affectedModel(Taxon::class);

        foreach ($data as $taxon) {
            if ($this->isCancelled()) {
                break;
            }
            $this->userjob->tickProgress();

            if ($this->validateData($taxon)) {
                // Arrived here: let's import it!!
                try {
                    $this->import($taxon);
                } catch (\Exception $e) {
                    $this->setError();
                    $this->appendLog('Exception '.$e->getMessage().' at '.$e->getFile().'+On line'.$e->getLine().' on taxon '.$taxon['name']);
                }
            }
        }
    }

    protected function validateData(&$taxon)
    {
        if (!$this->hasRequiredKeys(['name'], $taxon)) {
            return false;
        }
        $this->cleanRecord($taxon);

        //check if alredy registered


        //validate informed parent to the parent field if present
        $parent = isset($taxon['parent_name']) ? $taxon['parent_name'] : (isset($taxon['parent']) ? $taxon['parent'] : (isset($taxon['parent_id']) ? $taxon['parent_id'] : null ));
        if (!is_null($parent)) {
          // parent might be numeric (ie, already the ID) or a name. if it's a name, let's get the id
          $checkedparent = $this->getTaxonId($parent);
          if (null === $checkedparent) {
              $taxon['parent'] = ($parent=="") ? null : $parent;
          } else {
              $taxon['parent_id'] = $checkedparent->id;
              $taxon['parent'] = $checkedparent->fullname;
          }
        }
        $senior = isset($taxon['senior']) ? $taxon['senior'] : null;
        if (!is_null($senior)) {
          // parent might be numeric (ie, already the ID) or a name. if it's a name, let's get the id
          $checkedsenior = $this->getTaxonId($senior);
          if (!is_null($checkedsenior)) {
            $taxon['senior_id'] = $checkedsenior->id;
            $taxon['senior'] = $checkedsenior->fullname;
          } else {
            $taxon['senior'] = ($senior=="") ? null : $senior;
          }
        }

        //of person or author id was informed, then this is a morphotype
        $person = isset($taxon['person']) ? $taxon['person'] : (isset($taxon['author_id']) ? $taxon['author_id'] : null);
        if ($person==null) {
          //VALIDATE MOBOT, IPNI, MYCOBANK, GBIF AND ZOOBANK apis
          $this->validateAPIs($taxon);
        } elseif (!isset($taxon['parent_id']) or $taxon['parent_id']==null) {
          $this->appendLog('WARNING: taxon '.$name.' is unpublished you must inform a parent and level');
          return false;
        } elseif (!isset($taxon['level'])) {
          $this->appendLog('WARNING: taxon '.$name.' is unpublished you must inform a parent and level');
          return false;
        }

          //check if registered if parent is registered
        //DUPLICATED, THIS IS CHECKED BEFORE SAVING
        //if (isset($taxon['parent_id'])) {
          //$name = trim($taxon['name']);
          //$parent_id = $taxon['parent_id'];
          //$hadtaxon = Taxon::whereRaw('(odb_txname(name,level,parent_id,0,0) LIKE ?) AND parent_id = ?', [$name, $parent_id])->count();
          //if ($hadtaxon>0) {
              /* commented, too many messages in batch, this is not needed */
              //$this->appendLog('WARNING: taxon '.$name.' under the parent '.$taxon['parent'].' is already imported into the database');
              //return false;
          //}
        //}

        //validate parents and senior paths and create array to import them as needed
        //this will got down the root
        if (!$this->validateRelatedAndLevel($taxon)) {
              return false;
        }
        if (!$this->validateValid($taxon)) {
            return false;
        }

        //for unpublished names validation must include a check of persons id
        //then assumes that it is an unpublished name and requires a valid person
        if (!isset($taxon['apiwaschecked']) ) {
          $this->validatePerson($taxon);
        }

        //IF BIBKEY PROVIDED VALIDATE
        if (isset($taxon['bibkey'])) {
          if (!$this->validateBibKey($taxon['bibkey']))
          {
              return false;
          }
        }
        //if author is not informed for levels genus or below, then missing info
        $sub_equal_sp = explode(" ",$taxon['name']);
        $not_sub_equal = true;
        if (count($sub_equal_sp)==3) {
          $not_sub_equal = trim($sub_equal_sp[1])!=trim($sub_equal_sp[2]);
        }
        $condition = (!isset($taxon['author_id']) and !isset($taxon['author']) and $taxon['level']>=Taxon::getRank('genus') and $not_sub_equal);
        if ($condition)
        {
          $this->skipEntry($taxon, 'Author is mandatory for levels genus or below FAILED HERE' );
          return false;
        }
        //or clade is the level and bibreference is missing, then issues a warning only,
        $condition = ($taxon['level']==Taxon::getRank('clade') and !isset($taxon['bibreference']) and !isset($taxon['bibkey']) and !isset($taxon['author']) and !isset($taxon['author_id']));
        if ($condition)
        {
          $this->appendLog("WARNING: taxon ".$taxon['name']." was registered without a bibreference or a Authorship. Consider adding this information to the record");
          //return false;
        }
        return true;
    }

    protected function validateAPIs(&$taxon)
    {

        //this only makes sense if author_id
        //check API for taxon name as it may be the only thing informed
        //this will validate the name and get info
        //transform info in a request
        $checkname = $taxon['name'];
        $request = new therequest;
        $request = $request->merge(['name' => $checkname]);


        //get results from api checks
        $apicheck = app('App\Http\Controllers\TaxonController')->checkapis($request);

        $apiresults = $apicheck->getData(true);
        if (isset($apiresults["error"])) {
          $this->appendLog("WARNING: ".$taxon['name']." :".$apiresults["error"]);
          return false;
        }
        $apidata = $apiresults['apidata'];
        //Log::info($apiresults);
        //$this->appendLog(json_encode($apidata, JSON_PRETTY_PRINT));
        #$this->skipEntry($taxon, 'HERE');
        //return false;
        //
        //if level and author the api has found something by the name informed
        //if this is the case all fields that could be retrieved by the api are informed
        if (isset($apidata['rank'])) {
          if (isset($apidata['name'])) {
            $taxon['name'] = $apidata['name']; //this will fix the name in case of misspells
          }
          $info_level = isset($taxon['level']) ? Taxon::getRank($taxon['level']) : null;
          if (is_null($info_level)) {
            $taxon['level'] = $apidata['rank'];
          } elseif (!is_null($info_level) and $info_level != $apidata['rank'] ) {
            $apilevel = Lang::get('levels.tax.'.$apidata['rank']);
            $this->appendLog('WARNING: the informed level "'.$taxon['level'].'" for taxon "'.$taxon['name'].'"  is different from the API detected level: "'.$apilevel.'". The informed level was used for the record.');
          }
          //if this true, the informed parent exists and was validated (requires the validationParent to be executed before validateAPIs)
          $taxon['author'] = isset($apidata['author']) ? $apidata['author'] : null;
          $taxon['author_id'] = null;
          $taxon['bibreference'] = !isset($apidata['reference']) and isset($taxon['bibreference']) ? $taxon['bibreference'] : (isset($apidata['reference']) ? $apidata['reference'] : null);
          $apiparent = isset($apidata['parent']) ? is_array($apidata['parent']) : false;
          if ($apiparent) {
            if (!isset($taxon['parent_id'])) {
              $taxon['parent_id'] =  $apidata['parent'][0];
            }
            //if this true, the informed parent exists and was validated (requires the validationParent to be executed before validateAPIs)
            if ($taxon['parent_id'] != $apidata['parent'][0]) {
                $this->appendLog('WARNING: the parent '.$taxon['parent'].'  informed for taxon '.$taxon['name'].' is different from the one found by the API: '.$apidata['parent'][1].'. The Informed parent was used for the record.');
            } else {
                //just add the api detected parent as the parent, regardless of whether it is registered or not in odb
                $taxon['parent'] =  $apidata['parent'][1];
            }
          }
          if (isset($apidata['senior'])) {
            if (is_array($apidata['senior'])) {
              $taxon['senior_id'] = $apidata['senior'][0];
              $taxon['senior'] = $apidata["senior"][1];
            }
          }
          $taxon['mobotkey'] = isset($apidata["mobot"]) ? $apidata["mobot"] : null;
          $taxon['ipnikey'] = isset($apidata["ipni"]) ? $apidata["ipni"] : null;
          $taxon['mycobankkey'] = isset($apidata["mycobank"]) ? $apidata["mycobank"] : null;
          $taxon['gbifkey'] =  isset($apidata["gbif"]) ? $apidata["gbif"] : null;
          $taxon['apiwaschecked'] = 1;
          //return true;
        }

        //then maybe this is un unpublished name and will be and either person or author_id must have benn informed
        //if (!isset($taxon['person']) and !isset($taxon['author_id']) and !isset($taxon['author'])) {
          //  return false;
        //} 
        $match_type = isset($apidata['match_type']) ? !in_array($apidata['match_type'],['EXACT','NONE']) : false;
        if ($match_type) {
          $msg = 'WARNING: The informed taxon  <strong>'.$checkname.'</strong> was not found by the external taxonomic nomenclatural repositories search. However, a name was found <strong>'.$taxon['name'].'</strong> and refers to a <strong>'.$apidata['match_type'].'</strong> search.';
          $this->appendLog($msg);
        }
        
        return true;
    }

    protected function cleanRecord(&$taxon)
    {
      foreach($taxon as $key => $value) {
          if ($value == "" or empty($value)) {
            unset($taxon[$key]);
          }
      }
    }

    public function getRelated($checkname,$level) {
        $run = true;
        $related_locations = [];
        $name_run = $checkname;
        $related_array = [];
        while($run) {
            $checkname = ['name' => $name_run];
            $request = new therequest;
            $request = $request->merge($checkname);
            //$this->appendLog(' searched for '.$name_run);
            $apicheck = app('App\Http\Controllers\TaxonController')->checkapis($request);
            $apiresults = $apicheck->getData(true);
            if (isset($apiresults['apidata'])) {
              $data = $apiresults['apidata'];
              $gbif = $data['gbif'];
              $rank = $data['rank'];
              if ($rank>$level) {
                $run=false;
                break;
              }
              $parent_detected = isset($data['parent']) ? isset($data['parent'][0]) : false;
              if (isset($gbif) and !$parent_detected) {
                $related_array = ExternalAPIs::getGBIFParentPathData($gbif,$include_first=true);
                $run=false;
                break;
              } else {
                $data = $apiresults['apidata'];
                //$this->appendLog(json_encode($data,JSON_PRETTY_PRINT));
                $rank = $data['rank'];
                if (!isset($data['name'])) {
                  $data['name'] = $name_run;
                }
                if (isset($related_locations[$rank])) {
                  $rank = $rank+$incr;
                  $incr++;
                }
                if (!isset($data['parent'])) {
                  //$this->appendLog("The API found the record for taxon ".$name_run." but parent is missing and the taxon cannot be imported. Register its parent beforehand.");
                  $run=false;
                  break;
                }
                if ($data['parent'][0]==null) {
                  $name_run = $data['parent'][1];
                  $level = $data['rank'];
                  $data['parent'] = $name_run;
                  $data['parent_id'] = null;
                  $related_locations[$rank] = $data;
                } else {
                  //then parent is already registered
                  $data['parent_id'] = $apiresults['apidata']['parent'][0];
                  $data['parent'] = $apiresults['apidata']['parent'][1];
                  $related_locations[$rank] = $data;
                  $run=false;
                  break;
                }
              }
            } else {
              $run=false;
              break;
            }
         }
         $result = null;
         if (count($related_array)>0 and count($related_locations)>0) {
           $result = array_merge($related_locations,$related_array);
         }
         if (count($related_array)>0) {
           $result = $related_array;
         }
         if (count($related_locations)>0) {
           $result = $related_locations;
         }
         if(is_array($result)) {
           $newresult = [];
           foreach ($result as $key => $value) {
              $rank = $value["rank"];
              if (isset($newresult[$rank])) {
                $rank = $rank+1;
              }
              $newresult[$rank] = $value;
           }
           $result = $newresult;
         }
         return $result;
    }


    protected function validateRelatedAndLevel(&$taxon)
    {
        if (!$this->validateLevel($taxon)) {
            return false;
        }

        //will only test if api validation has not already found a registered parent
        $gbifkey = isset($taxon['gbifkey']) ? $taxon['gbifkey'] : null;
        $parent_id = isset($taxon['parent_id']) ? $taxon['parent_id'] : null;
        $parent = (isset($taxon['parent']) and $taxon['parent']!="") ? $taxon['parent'] : null;
        $senior = (isset($taxon['senior']) and $taxon['senior']!="") ? $taxon['senior'] : null;
        $senior_id = isset($taxon['senior_id']) ? $taxon['senior_id'] : null;

        $condition1 = (!isset($parent_id) and null != $parent);
        $condition2 = (!isset($senior_id) and null != $senior);
        $condition3 = isset($gbifkey);

        //if ($condition3 and ($condition1 or $condition2)) {
        //    $related = ExternalAPIs::getGBIFParentPathData($gbifkey,$include_first=false);
        //    $taxon['related_to_import'] = $related;
        //}
        //!$condition3 and (
        $related_data = null;
        $related_data2 = null;
        if ($condition1 or $condition2) {
            $level = $taxon['level'];
            if ($condition1) {
               $checkname =$taxon['parent'];
               $related_data = $this->getRelated($checkname,$level);
            }
            if ($condition2) {
               $checkname = $taxon['senior'];
               $related_data2 = $this->getRelated($checkname,$level);
               if (is_array($related_data) and is_array($related_data2)) {
                 $related_data = array_merge($related_data,$related_data2);
               }
            }
            if (is_array($related_data)) {
              $related_data = array_unique($related_data,SORT_REGULAR);
              ksort($related_data);
              $taxon['related_to_import'] = $related_data;
            }
        }
        if ($condition3 and ($condition1 or $condition2) and $related_data===null) {
            $related = ExternalAPIs::getGBIFParentPathData($gbifkey,$include_first=false);
            $taxon['related_to_import'] = $related;
        }
        $condition4 = isset($taxon['related_to_import']) ? count($taxon['related_to_import'])>0 : false;
        $condition5 = ($taxon['level'] > 0);
        if ($condition1 and $condition2 and !$condition4 and $condition5) {
            $name = $taxon['name'];
            $parent = $taxon['parent'];
            $senior = $taxon['senior'];
            $message = $condition1 ? ("The Parent for taxon $name is $parent, but this is not registered and was not found by the API") : "";
            $message2 = $condition2 ? ("The Senior for taxon $name is $senior, but this is not registered and was not found by the API") : "";
            $message = $message." ".$message2;
            $this->skipEntry($taxon,$message);
            return false;
        }
        if (!$condition5 and $condition1 and !$condition4) {
            $root = Taxon::root();
            $taxon['parent_id'] = $root->id;
            $parent = $root->fullname;
            $this->appendLog("WARNING: missing parent for taxon ".$taxon['name']." The root node of the taxon table  '".$parent."' was used");
        }
        return true;
    }



    //validate author of unpublished names
    protected function validatePerson(&$taxon)
    {

       $person = isset($taxon['person']) ? $taxon['person'] : (isset($taxon['author_id']) ? $taxon['author_id'] : null);
       $name = $taxon['name'];
       if (null != $person)  {
         $valid = ODBFunctions::validRegistry(Person::select('id'), $person, ['id', 'abbreviation', 'full_name', 'email']);
         if (null != $valid) {
           $taxon['author_id'] = $valid->id;
           return true;
         }
       }
       $taxon['author_id'] = null;
       //$this->skipEntry($taxon, "Author_id for unpublished taxon $name is listed as $person, but this was not found in the database");
       return true;
    }


    //must be run after parent validation
    protected function validateLevel(&$taxon)
    {
        //level must be greater than parent level if is not a clade, which can be anywehre
        $level = isset($taxon['level']) ? $taxon['level'] : null;
        if (!is_numeric($level) and !is_null($level)) {
            $level = Taxon::getRank($level);
        }
        if (is_null($level)) {
            /* some infraspecies with same name as species case
             * the condition below will retrieve missing level
             * from sister taxon
            */
            if (isset($taxon['parent_id'])) {
                $same_level = Taxon::where('parent_id',$taxon['parent_id'])->cursor();
                if ($same_level->count()) {
                  $same_level = $same_level->pluck('level')->toArray();
                  $same_level = array_unique($same_level);
                  if (count($same_level)==1) {
                    $taxon['level'] = $same_level[0];
                    return true;
                  }
                }
            }
            $isspecies = explode(" ",$taxon['name']);
            if (count($isspecies)==2) {
              $taxon['level'] =  Taxon::getRank("species");
              return true;
            }
            /* this assumes sub species if not present */
            if (count($isspecies)==3) {
              $taxon['level'] =  Taxon::getRank("subspecies");
              return true;
            }
            $name = $taxon['name'];
            $this->skipEntry($taxon, "Level for taxon $name not available");
            return false;
        }
        if (isset($taxon['parent_id'])) {
          $parent = Taxon::findOrFail($taxon['parent_id']);
          $name = $taxon['name'];
          if ($level != Taxon::getRank('clade')  and $level <= $parent->level) {
            $this->appendLog("FAILED Level $level for taxon $name is invalid in relation to the parent ".$parent->fullname." taxon level ".$parent->level);
            return false;
          }
        }
        $taxon['level'] = $level;
        return true;
    }

    protected function getTaxonId($ref)
    {
        if (is_null($ref)) {
            return null;
        }
        // ref might be numeric (ie, already the ID) or a name. if it's a name, let's get the id
        if (is_numeric($ref)) {
            $ref = Taxon::where('id',$ref)->get();
        } else {
            $ref = Taxon::whereRaw('odb_txname(name, level, parent_id,0,0) = ?', [$ref])->get();
        }
        if ($ref->count() == 1) {
            return $ref->first();
        }

        return null;
    }

    protected function validateBibKey(&$bibkey)
    {
      if (is_numeric($bibkey)) {
        $valid = BibReference::where('id',$bibkey);
      } else {
        $valid = BibReference::whereRaw('odb_bibkey(bibtex) = ?', [$bibkey]);
      }
      if ($valid->count()) {
        $bibkey = $valid->get()->first()->id;
        return true;
      }
      $this->appendLog('FAILED: Provided bibkey '.$bibkey.' not found in database.');
      return false;
    }

    protected function validateValid(&$taxon)
    {
        //this depends on senior, if it exist or no
        if (!isset($taxon['senior_id']) and !isset($taxon['senior'])) {
            $taxon['valid'] = true;
            //$taxon
            return $taxon['valid'];
        } else {
            $taxon['valid'] = false;
            //returns true
            return !$taxon['valid'];
        }
    }

    public function import($taxon)
    {        
        $name = $taxon['name'];


        $parent = isset($taxon['parent_id']) ? $taxon['parent_id'] : null;
        $related_to_import = isset($taxon['related_to_import']) ? $taxon['related_to_import'] : [];
        //import related taxa as needed retrieving last parent
        if (count($related_to_import)>0) {
          self::importRelated($taxon);
          $parent = isset($taxon['parent_id']) ? $taxon['parent_id'] : null;
        }

        //$this->skipEntry($taxon, 'taxon '.$name.' ARRIVED  here to be save');
        //return;
        if (is_null($parent)) {
          $m = "Taxon '$name' could not be imported into the database. <strong>Parent missing</strong>.";
          if (count($related_to_import)) {
            $m .= " Related detected were: ".json_encode($related_to_import,JSON_PRETTY_PRINT );
          }
          $this->skipEntry($taxon,$m);
          return;
        }

        // Is this taxon already imported?
        // missing level option here
        $level = $taxon['level'];
        $valid = $taxon['valid'];

        $this->validateReference($taxon);
        $bibreference = isset($taxon['bibreference']) ? $taxon['bibreference'] : null;
        $bibreference_id = isset($taxon['bibreference_id'])  ? $taxon['bibreference_id'] : (isset($taxon['bibkey'])  ? $taxon['bibkey'] : null);

        $author = isset($taxon['author']) ? $taxon['author'] : null;
        /*if is empty and infraspecies==especies, then author is same as parent */
        if ($author===null) {
          $sub_equal_sp = explode(" ",$taxon['name']);
          if (count($sub_equal_sp)==3) {
            if(trim($sub_equal_sp[1])==trim($sub_equal_sp[2])) {
                $author = Taxon::findOrFail($parent)->author;
                if (!isset($bibreference) and !isset($bibreference_id)) {
                  $bibreference = Taxon::findOrFail($parent)->bibreference;
                  $bibreference_id = Taxon::findOrFail($parent)->$bibreference_id;
                }
            }
          }
        }

        $author_id = isset($taxon['author_id']) ? $taxon['author_id'] : null;
        $senior = isset($taxon['senior_id']) ? $taxon['senior_id'] : null;
        $notes = isset($taxon['notes']) ? $taxon['notes'] : null;
        $mobot = isset($taxon['mobotkey']) ? $taxon['mobotkey'] : (isset($taxon['mobot']) ? $taxon['mobot'] : null);
        $ipni = isset($taxon['ipnikey']) ? $taxon['ipnikey'] : (isset($taxon['ipni']) ? $taxon['ipni'] : null);
        $mycobankkey = isset($taxon['mycobankkey']) ? $taxon['mycobankkey'] : (isset($taxon['mycobank']) ? $taxon['mycobank'] : null);
        $gbifkey = isset($taxon['gbifkey']) ? $taxon['gbifkey'] : (isset($taxon['gbif']) ? $taxon['gbif'] : null);
        $zoobankkey = isset($taxon['zoobankkey'])  ? $taxon['zoobankkey'] : (isset($taxon['zoobank']) ? $taxon['zoobank'] : null);

        $values = [
            'level' => $level,
            'parent_id' => $parent,
            'valid' => $valid,
            'senior_id' => $senior,
            'author' => $author,
            'author_id' => $author_id,
            'bibreference' => $bibreference,
            'bibreference_id' => $bibreference_id,
            'notes' => $notes,
        ];


        $containparent = true;
        /* allow unpublished names to be binomials
         * when parent is not a genus or species
        */
        $theparent = Taxon::findOrFail($parent);
        $pattern = "/".$theparent->fullname."/i";
        if ($level==210 and $theparent->level!=180 and is_null($values['author_id'])) {
          $this->skipEntry($taxon, "Species must have a genus as parent");
          return;
        }
        if ($level>210 and $theparent->level!=210 and is_null($values['author_id'])) {
          $this->skipEntry($taxon, "InfraSpecies must have a species as parent");
          return;
        }
        if (!is_null($parent) and !is_null($values['author_id'])) {
          /* if name contains the parent name */
          $containparent = preg_match($pattern,$name);
          /* only if match failds, containparent will be false, allowing binomials under different parent */
        }
        /* name to check for duplicates */
        $reqname = $name;
        if ($containparent) {
          $reqname = trim(preg_replace($pattern,"",$name));
        }
        // checks for [name, parent] matches and [name, parent, author] matches (ref issue #61)
        //$hadtaxon = Taxon::whereRaw('odb_txname(name, level, parent_id,0,0) LIKE ? AND parent_id = ?', [$name, $parent])->count();
        //check if registered. if published name and parent must be unique
        //if unpublished name + parent + author must be unique
        if (!is_null($parent) and !is_null($values['author_id'])) {
          $hadtaxon = Taxon::where('name','like',$reqname)->where('author_id', $values['author_id'])->where('parent_id', $parent);
        } else {
          $hadtaxon = Taxon::whereRaw('odb_txname(name, level, parent_id,0,0) LIKE ? AND parent_id = ?', [$name, $parent]);
        }
        if ($hadtaxon->count()) {
            $this->skipEntry($taxon, Lang::get('messages.taxon_duplicate',['name'=> $taxon['name']]));
            if ($hadtaxon->count()==1) {
              $this->affectedId($hadtaxon->get()->first()->id);
            }            
            return;
        }
        $newtaxon = new Taxon($values);
        if ($containparent) {
          $newtaxon->fullname = $name;
        } else {
          /* allow unpublished as binomials */
          $newtaxon->name = $name;
        }
        $newtaxon->save();
        //sleep(2);
        if (!is_null($mobot)) {
          $newtaxon->setapikey('Mobot', $mobot);
        }
        if (!is_null($ipni)) {
          $newtaxon->setapikey('IPNI', $ipni);
        }
        if (!is_null($gbifkey)) {
          $newtaxon->setapikey('GBIF', $gbifkey);
        }
        if (!is_null($zoobankkey)) {
          $newtaxon->setapikey('ZOOBANK', $zoobankkey);
        }
        if (!is_null($mycobankkey)) {
          $newtaxon->setapikey('Mycobank', $mycobankkey);
        }
        $newtaxon->save();
        $this->affectedId($newtaxon->id);

        return;
    }


    public function importRelated(&$taxon)
    {
      $related_to_import = isset($taxon['related_to_import']) ? $taxon['related_to_import'] : [];
      $parent = isset($taxon['parent']) ? $taxon['parent'] : null;
      $senior = isset($taxon['senior']) ? $taxon['senior'] : null;

      //$parent_id = isset($related_to_import[0]['parent_id']) ? $related_to_import[0]['parent_id'] : 1;
      //$this->appendLog(serialize($related_to_import));
      //return false;
      $finalid = null;
      $previous_id = null;
      foreach($related_to_import as $related) {
            if ($related['parent_id']) {
              $previous_id = $related['parent_id'];
            } else {
              $thisparent = $related['parent'];
              if ($thisparent) {
                $hadtaxon = Taxon::whereRaw('odb_txname(name, level, parent_id,0,0) = ?', [$thisparent]);
                if ($hadtaxon->count()==1) {
                  $previous_id = $hadtaxon->first()->id;
                }
              }
            }
            if (null === $previous_id) {
              break;
              return false;
            }
            $values = [
                'level' => $related['rank'],
                'parent_id' => $previous_id,
                'valid' => $related['valid'],
                'author' => $related['author'],
                'bibreference' => $related['reference'],
            ];
            $this->validateReference($values);

            /*check again if already registered */
            $hadtaxon = Taxon::whereRaw('odb_txname(name, level, parent_id,0,0) LIKE ? AND parent_id = ?', [$related['name'], $previous_id]);
            if ($hadtaxon->count()>0) {
              $previous_id = $hadtaxon->first()->id;
            } else {
              $newtaxon = new Taxon($values);
              $newtaxon->fullname = $related['name'];
              $newtaxon->save();
              if (isset($related['mobot'])) {
                $newtaxon->setapikey('Mobot', $related['mobot']);
              }
              if (isset($related['ipni'])) {
                $newtaxon->setapikey('IPNI', $related['ipni']);
              }
              if (isset($related['gbif'])) {
                $newtaxon->setapikey('GBIF', $related['gbif']);
              }
              if (isset($related['zoobank'])) {
                $newtaxon->setapikey('ZOOBANK', $related['zoobank']);
              }
              if (isset($related['mycobank'])) {
                $newtaxon->setapikey('Mycobank', $related['zoobank']);
              }
              $newtaxon->save();
              $previous_id = $newtaxon->id;
            }
            if ($related['name']==$parent) {
              $taxon['parent_id'] = $previous_id;
            }
            if ($related['name']==$senior) {
              $taxon['senior_id'] = $previous_id;
            }
    }
    return true;
  }


  /* this function checks if the string has a doi and if is the case
  *  attempts to get the bitext with the doi else save as is
  * most references are short, but gbif is exporting full length refs without doi
  * this cannot be save, length of reference field expanded
  * to acomodate this
  */
  public function validateReference(&$entry) {
    if (!isset($entry['bibreference']) or ($entry['bibreference'])==null) {
      return;
    }
    preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $entry['bibreference'], $match);
    //may have a doi?
    if (count($match[0])) {
      $url = $match[0][0];
      if (preg_match("/doi.org/i",$url)) {
          //import reference
          $doi = preg_split("/doi.org\//i",$url)[1];
          $contents = ExternalAPIs::getBibtexFromDoi($doi);
          $ids = BibReference::importBibReference($contents);
          if (count($ids["affected_ids"])==1) {
              $entry['bibreference_id'] = $ids["affected_ids"][0];
              $entry['bibreference'] = null;
          }
      }
    }
    return;
  }



}
