<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Policies;

use App\Models\User;
use App\Models\Form;
use Illuminate\Auth\Access\HandlesAuthorization;
use Log;
class FormPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        $editable_datasets = $user->editableDatasets();
        if (User::ADMIN == $user->access_level or $editable_datasets)  {
            return true;
        } 
        return false;
    }

    public function update(User $user, Form $form)
    {
        return User::ADMIN == $user->access_level or (User::USER == $user->access_level and $form->user->id == $user->id);
    }

    public function delete(User $user, Form $form)
    {
        $uid = $user->id;
        $can = $form->form_tasks()->get()->map(function($t) use($uid) {
            $ids = $t->users->pluck('id')->toArray();            
            return in_array($uid,$ids);
        })->toArray();
        $can = array_sum($can);
        $can1 = $form->form_tasks()->count();
        $can3 = (User::ADMIN == $user->access_level) ;
        $can4 = (User::USER == $user->access_level and $form->user->id == $user->id);
        //Log::info([$can,$can1,$can3,$form->user->id,$user->id,$user->access_level,User::USER,$can4]);
        return ($can==$can1) and ($can3 or $can4);
    }


}
