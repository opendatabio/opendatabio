<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Policies;

use App\Models\User;
use App\Models\Dataset;
use App\Models\Individual;
use App\Models\Voucher;
use App\Models\Measurement;
use DB;
use Illuminate\Auth\Access\HandlesAuthorization;

class DatasetPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return $user->access_level >= User::USER;
    }

    public function update(User $user, Dataset $dataset)
    {
        $has_admin = $dataset->admins()->count();
        $is_admin = false;
        if ($has_admin) {
          $is_admin = $dataset->isAdmin($user);
        } elseif ($dataset->project) {
          $is_admin = $dataset->project->isAdmin($user);
        }
        return User::ADMIN == $user->access_level or
            (User::USER == $user->access_level and $is_admin);
    }


    public function export(User $user, Dataset $dataset)
    {
      //direct users or project users
      $is_open = in_array($dataset->privacy,[Dataset::PRIVACY_REGISTERED,Dataset::PRIVACY_PUBLIC]);
      $valid_users = [];
      if ($dataset->privacy==Dataset::PRIVACY_PROJECT) {
        $valid_users = $dataset->project->users()->pluck('users.id')->toArray();
      } else {
        $valid_users = $dataset->users()->pluck('users.id')->toArray();
      }
      return (User::ADMIN == $user->access_level) or in_array($user->id,$valid_users) or $is_open;
    }


    public function delete(User $user, Dataset $dataset)
    {
      //only admins can delete the full dataset
      $has_admin = $dataset->admins->count();
      if ($dataset->privacy==Dataset::PRIVACY_PROJECT and $has_admin==0) {
        $valid_users = $dataset->project->admins->contains($user);
      } else {
        $valid_users = $dataset->admins->contains($user);
      }

      $is_user = (User::ADMIN == $user->access_level or (User::USER == $user->access_level and $valid_users));
      $no_other_data = true;
      
      if (!$is_user) {
        return false;
      }
      //and if no data belonging to other datasets are indirectly related
      //check all related models for additional data belonging to other datasets
      $nrelated = 0;
      $sql = "SELECT COUNT(*) as otherMeasurements FROM measurements as me, individuals as ind WHERE me.measured_type LIKE '%individual' AND me.measured_id=ind.id AND ind.dataset_id=".$dataset->id." AND me.dataset_id<>".$dataset->id;
      $q = DB::select($sql);
      $nrelated = $nrelated+$q[0]->otherMeasurements;

      $sql = "SELECT COUNT(*) as otherMeasurements FROM measurements as me, vouchers as vou WHERE me.measured_type LIKE '%voucher' AND me.measured_id=vou.id AND vou.dataset_id=".$dataset->id." AND me.dataset_id<>".$dataset->id;
      $q = DB::select($sql);
      $nrelated = $nrelated+$q[0]->otherMeasurements;

      $sql = "SELECT COUNT(*) as otherMedia FROM media as me, individuals as ind WHERE me.model_type LIKE '%individual' AND me.model_id=ind.id AND ind.dataset_id=".$dataset->id." AND me.dataset_id<>".$dataset->id;
      $q = DB::select($sql);
      $nrelated = $nrelated+$q[0]->otherMedia;

      $sql = "SELECT COUNT(*) as otherMedia FROM media as me, vouchers as vou WHERE me.model_type LIKE '%voucher%' AND me.model_id=vou.id AND vou.dataset_id=".$dataset->id." AND me.dataset_id<>".$dataset->id;
      $q = DB::select($sql);
      $nrelated = $nrelated+$q[0]->otherMedia;

      //check all related models for additional data belonging to other datasets
      $no_other_data = $nrelated==0;
      return ($is_user and $no_other_data);
    }



}
