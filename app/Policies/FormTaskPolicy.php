<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Policies;

use App\Models\User;
use App\Models\FormTask;
use Illuminate\Auth\Access\HandlesAuthorization;

class FormTaskPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        $editable_datasets = $user->editableDatasets();
        if (User::ADMIN == $user->access_level or $editable_datasets)  {
            return true;
        } 
        return $false;        
    }

    public function update(User $user, FormTask $form_task)
    {
        $ids = $form_task->users()->pluck('id')->toArray();
        return User::ADMIN == $user->access_level or
            (User::USER == $user->access_level and in_array($user->id,$ids));
    }

    public function fill(User $user, FormTask $form_task)
    {
        $ids = $form_task->users()->pluck('id')->toArray();
        $prepared = $form_task->is_prepared;
        $can1 = User::ADMIN == $user->access_level or (User::USER == $user->access_level and in_array($user->id,$ids));
        return $can1 and $prepared;
    }

    public function delete(User $user, FormTask $form_task)
    {
        $ids = $form_task->users->pluck('id')->toArray();
        return User::ADMIN == $user->access_level or
            (User::USER == $user->access_level and in_array($user->id,$ids));
    }
}
