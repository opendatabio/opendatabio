<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Policies;

use App\Models\User;
use App\Models\Person;

use Illuminate\Auth\Access\HandlesAuthorization;

class PersonPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the person.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Person $person
     *
     * @return mixed
     */
    public function view(User $user, Person $person)
    {
        // everyone can view persons
        return true;
    }

    /**
     * Determine whether the user can create people.
     *
     * @param \App\Models\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        // full users and admins
        return $user->access_level >= User::USER;
    }

    /**
     * Determine whether the user can update the person.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Person $person
     *
     * @return mixed
     */
    public function update(User $user, Person $person)
    {
        //if person is set as the default person of a user it can only be updated by the user or and admin user
        $userpersons = User::where('id','<>',$user->id)->whereRaw('person_id IS NOT NULL')->pluck('person_id')->toArray();

        // full users and admins
        return $user->access_level == User::ADMIN or ($user->access_level == User::USER  and  !in_array($person->id,$userpersons));
    }


    /**
     * Determine whether the user can delete the person.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Person $person
     *
     * @return mixed
     */
    public function delete(User $user, Person $person)
    {
        /* is not a person user except self */
        $no_user = User::where('person_id',$person->id)
                      ->where('id','<>',$user->id)
                      ->count()==0;
        /* is not used as a collector */
        $no_collector  = $person->collected()
                          ->withoutGlobalScopes()
                          ->count()==0;
        /* is not a taxon author  */
        $no_author = $person->taxonsAsAuthor()->count()==0;
        /* is not a taxon specialist */
        $no_specialist = $person->taxons()->count()==0;
        /* is not an identifier */
        $no_identifier = $person->identifications()
                          ->withoutGlobalScopes()
                          ->count()==0;
        /*if no relations  defined and full user, then can */
        return (($user->access_level >= User::USER) and ($no_user and $no_collector and $no_author and $no_identifier));
        //remove this => and $no_specialist as that is not mandatory
    }


}
