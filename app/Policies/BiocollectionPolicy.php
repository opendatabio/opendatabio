<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Policies;

use App\Models\User;
use App\Models\Biocollection;
use Illuminate\Auth\Access\HandlesAuthorization;

class BiocollectionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the biocollection.
     *
     * @param \App\Models\User      $user
     * @param \App\Models\Biocollection $biocollection
     *
     * @return mixed
     */
    public function view(User $user, Biocollection $biocollection)
    {
        // everyone can view biocollections
        return true;
    }

    /**
     * Determine whether the user can create biocollections.
     *
     * @param \App\Models\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return User::ADMIN == $user->access_level;
    }

    /**
     * Determine whether the user can update the biocollection.
     *
     * @param \App\Models\User      $user
     * @param \App\Models\Biocollection $biocollection
     *
     * @return mixed
     */
    public function update(User $user, Biocollection $biocollection)
    {
        // Currently impossible!
        $has_admin = $biocollection->admins()->count();
        $is_admin = false;
        if ($has_admin) {
          $is_admin = $biocollection->isAdmin($user);
        }
        return User::ADMIN == $user->access_level or $is_admin;
    }

    /**
     * Determine whether the user can delete the biocollection.
     *
     * @param \App\Models\User      $user
     * @param \App\Models\Biocollection $biocollection
     *
     * @return mixed
     */
    public function delete(User $user, Biocollection $biocollection)
    {
        $has_admin = $biocollection->admins()->count();
        $is_admin = false;
        if ($has_admin) {
          $is_admin = $biocollection->isAdmin($user);
        }
        $has_vouchers = $biocollection->vouchers()->withoutGlobalScopes()->count();
        return (User::ADMIN == $user->access_level or $is_admin) and $has_vouchers==0;
    }
}
