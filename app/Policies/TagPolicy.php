<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Policies;

use App\Models\User;
use App\Models\Tag;
use Illuminate\Auth\Access\HandlesAuthorization;

class TagPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Tag $tag)
    {
        return true;
    }

    public function create(User $user)
    {
        return in_array($user->access_level,[User::ADMIN,User::USER]);
    }

    public function update(User $user, Tag $tag)
    {
        $has_project = $tag->projects->count();
        $has_media = $tag->media->count();
        $has_datasets = $tag->datasets->count();
        $has_project = $tag->projects->count();
        $can = $has_datasets+$has_media+$has_project;
        $can = $user->access_level==User::USER and $can==0;
        $can2 = $user->access_level==User::ADMIN;
        return $can or $can2;
    }

    public function delete(User $user, Tag $tag)
    {
        /* if it is not used */
        $has_project = $tag->projects->count();
        $has_media = $tag->media->count();
        $has_datasets = $tag->datasets->count();
        $has_project = $tag->projects->count();
        $can = $has_datasets+$has_media+$has_project;
        return in_array($user->access_level,[User::ADMIN,User::USER]) and $can==0;
    }
}
