<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Policies;

use App\Models\User;
use App\Models\Taxon;
use Illuminate\Auth\Access\HandlesAuthorization;
use DB;

class TaxonPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return $user->access_level >= User::USER;
    }

    public function update(User $user, Taxon $taxon)
    {
        $no_identity = $taxon->individuals()->withoutGlobalScopes()->count()==0;
        $no_media = $taxon->media()->withoutGlobalScopes()->count()==0;
        $no_measurements = $taxon->measurements()->withoutGlobalScopes()->count()==0;
        $is_user = $user->access_level >= User::USER;
        $not_used = $no_identity and $no_media  and $no_measurements;
        if (($not_used and $is_user) or $user->access_level==User::ADMIN) {
          return true;
        }
        /* else must be an authorized user in all datasets using the taxon */
        /* either a collab or an admin in all datasets */
        $user_id = $user->id;
        $m_dt = $taxon->measurements()->withoutGlobalScopes()->distinct('dataset_id')->pluck('dataset_id')->toArray();
        $p_dt = $taxon->individuals()->withoutGlobalScopes()->distinct('dataset_id')->pluck('dataset_id')->toArray();
        $media_dt = $taxon->media()->withoutGlobalScopes()->distinct('dataset_id')->pluck('dataset_id')->toArray();
        $dt_ids = array_unique(array_merge($m_dt,$p_dt,$media_dt));
        $collabs = DB::table('dataset_user')->whereIn("dataset_id",$dt_ids)
        ->where('user_id',$user_id)->where('access_level','>=',1);
        $is_collabs = $collabs->count() == count($dt_ids);
        return ($is_user and $is_collabs);
    }

    public function delete(User $user, Taxon $taxon)
    {
        /* any full user can delete if there is no data associated with the taxon
        * nor the taxon has children
        */
        if ($user->access_level >= User::USER) {
            $m = $taxon->measurements()->withoutGlobalScopes()->count();
            $p = $taxon->individuals()->withoutGlobalScopes()->count();
            $media = $taxon->media()->withoutGlobalScopes()->count();
            $children = $taxon->getDescendants()->count();
            return 0 == ($m + $p + $media + $children);
        }

        return false;
    }
}
