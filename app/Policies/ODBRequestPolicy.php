<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Policies;

use App\Models\User;
use App\Models\ODBRequest;
use App\Models\Biocollection;
use Illuminate\Auth\Access\HandlesAuthorization;

class ODBRequestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the biocollection.
     *
     * @param \App\Models\User      $user
     * @param \App\Models\ODBRequest $odbrequest
     *
     * @return mixed
     */
    public function view(User $user, ODBRequest $odbrequest)
    {
        // everyone can view odbrequests
        return true;
    }

    /**
     * Determine whether the user can create odbrequests.
     *
     * @param \App\Models\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
       // everyone can create a odbrequests
       return true;
    }

    /**
     * Determine whether the user can update the biocollection.
     *
     * @param \App\Models\User      $user
     * @param \App\Models\ODBRequest $odbrequest
     *
     * @return mixed
     */
    public function update(User $user, ODBRequest $odbrequest)
    {
        return false;
        if ($odbrequest->vouchers->count()) {
          $biocollection_ids = array_unique($odbrequest->vouchers->pluck('biocollection_id')->toArray());
        } else {
          $biocollection_ids = array_unique($odbrequest->individuals->pluck('pivot.biocollection_id')->toArray());
        }
        $is_admin = 0;
        foreach($biocollection_ids as $biocollection_id) {
            $biocollection = BioCollection::findOrFail($biocollection_id);
            $is_admin = $is_admin + $biocollection->isAdmin($user);
        }
        $is_admin = $is_admin == count($biocollection_ids);
        return $is_admin or User::ADMIN == $user->access_level;
    }

    /**
     * Determine whether the user can delete the biocollection.
     *
     * @param \App\Models\User      $user
     * @param \App\Models\ODBRequest $odbrequest
     *
     * @return mixed
     */
    public function delete(User $user, ODBRequest $odbrequest)
    {
        $hasvouchers = $odbrequest->vouchers->count()==0;
        $hasindividuals = $odbrequest->individuals->count()==0;

        $biocollection_ids = array_unique($odbrequest->vouchers->pluck('biocollection_id')->toArray());
        if ($odbrequest->vouchers->count()) {
          $biocollection_ids = array_unique($odbrequest->vouchers->pluck('biocollection_id')->toArray());
          $status = array_unique($odbrequest->vouchers->pluck('pivot.status')->toArray());
        } else {
          $biocollection_ids = array_unique($odbrequest->individuals->pluck('pivot.biocollection_id')->toArray());
          $status = array_unique($odbrequest->individuals->pluck('pivot.status')->toArray());
        }
        $requested_only = (count($status)==1 and $status[0]==0) ? true : false;
        $is_admin = 0;
        $is_buser= 0;
        foreach($biocollection_ids as $biocollection_id) {
            $biocollection = Biocollection::findOrFail($biocollection_id);
            $is_admin = $is_admin + $biocollection->isAdmin($user);
            $is_buser = $is_buser + $biocollection->isUser($user);
        }
        $is_admin = $is_admin == count($biocollection_ids);
        $isclosed = $odbrequest->status_closed;
        $is_user = $odbrequest->user_id;
        //$has_vouchers = $odbrequest->vouchers()->withoutGlobalScopes()->count();
        $condition1 = (User::ADMIN == $user->access_level and ($requested_only or $isclosed));
        $condition2 = ($is_admin and ($requested_only or $isclosed));
        $condition3 = (($is_user or $is_buser) and $requested_only);
        $condition4 = ($hasvouchers and $hasindividuals);
        return $condition1 or $condition2 or $condition3 or $condition4;
    }

    public function annotate(User $user, ODBRequest $odbrequest)
    {
        /*
        $isclosed = $odbrequest->status_closed;
        if ($isclosed) {
          return false;
        }
        */
        if ($odbrequest->vouchers->count()) {
          $biocollection_ids = array_unique($odbrequest->vouchers->pluck('biocollection_id')->toArray());
        } else {
          $biocollection_ids = array_unique($odbrequest->individuals->pluck('pivot.biocollection_id')->toArray());
        }
        $is_admin = 0;
        foreach($biocollection_ids as $biocollection_id) {
            $biocollection = Biocollection::findOrFail($biocollection_id);
            $is_admin = $is_admin + $biocollection->isUser($user);
        }
        $is_admin = $is_admin == count($biocollection_ids);
        return User::ADMIN == $user->access_level or $is_admin;
    }

}
