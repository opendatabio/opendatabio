<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\ODBTrait;
use App\Models\TraitUnit;

class TraitUnitPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view.
     */
    public function view(User $user)
    {
        return $user->access_level >= User::REGISTERED;
    }

    /**
     * Determine whether the user can create
     */
    public function create(User $user)
    {
        return $user->access_level >= User::USER;
    }

    /**
     * Determine whether the user can update.
     */
    public function update(User $user, TraitUnit $trait_unit)
    {
        $trait_ids = array_unique($trait_unit->odbtraits()->pluck('traits.id')->toArray());
        if (User::ADMIN == $user->access_level) {
            return true;
        }
        $no_measurements = Measurement::withoutGlobalScopes()->whereIn('trait_id',$trait_ids)->count();
        if (User::USER == $user->access_level and $no_measurements==0) {
            return true;
        }    

        //persons that are admins to all datasets that use trait
        $trait_used_datasets = Measurement::withoutGlobalScopes()->whereIn('trait_id',$trait_ids)->distinct('dataset_id')->pluck('dataset_id')->toArray();

        $trait_datasets_admins = Dataset::whereIn('id',$trait_used_datasets)->cursor()->map(function($dataset) { return $dataset->admins()->pluck('person_id');})->toArray();
        $person_id=$user->person_id;
        $dataset_count = array_filter($trait_datasets_admins,function($admins) use($person_id){ return in_array($person_id,$admins);});
        //condition 2 user is admin to all datasets using trait
        $is_admin_datasets = count($dataset_count)==count($trait_datasets_admins);

        if (User::USER == $user->access_level and $is_admin_datasets) {
            return true;
        }
        return false;
    }


    /**
     * Determine whether the user can delete the trait unit.
     */
    public function delete(User $user, TraitUnit $trait_unit)
    {
        $is_used = $trait_unit->odbtraits->count(); 
        /* if full user and no measurement */
        if (($user->access_level >= User::USER) and $is_used==0) {
            return true;
        }
        return false;
    }

}
