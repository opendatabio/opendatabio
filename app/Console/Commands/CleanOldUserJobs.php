<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\UserJob;
use Storage;
use File;

class CleanOldUserJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'odb:clean-userjobs {days=30}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean user jobs and associated files older than days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $days = (int) $this->argument('days');
        $limit_date = today()->subDays($days)->format("Y-m-d");
        $jobs = UserJob::whereDate('created_at',"<=",$limit_date);
        
        // list all possible job related files
        $allFiles = Storage::disk('downloads')->files('');

        $hasjobs = $jobs->count();
        if ($hasjobs>0) {
        //echo "I found".$jobs->count()." jobs to erase \n\n";        
            foreach($jobs->get() as $userjob) {
                //echo $userjob->created_at." \n";
        
                /* if job is a web interface import it has a file */
                if ($userjob->submitted_file) {
                $path = storage_path('app/public/tmp/'.$userjob->submitted_file);
                File::delete($path);
                }
                // filter the ones that match the filename.*
                $searchStr = "job-".$userjob->id;
                $todelete = preg_grep("/^".$searchStr."/i", $allFiles);
                //should be just one file found
                if (count($todelete)) {
                    $todelete = array_values($todelete);
                    //echo "has".implode("\n",$todelete)."\n";
                    foreach($todelete as $file) {
                        Storage::disk('downloads')->delete($file);
                    }
                }
                $userjob->delete();            
            }
            echo $hasjobs." user jobs and associated files deleted!\n";
        } else {
            echo "No jobs older than ".$days." days to delete!\n";
        }
        return ;
    }
}
