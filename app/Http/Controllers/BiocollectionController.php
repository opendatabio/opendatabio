<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\DataTables\VouchersDataTable;
use App\Models\Biocollection;
use App\Models\ExternalAPIs;
use Illuminate\Support\Facades\Lang;
use App\Jobs\ImportBiocollections;
use Spatie\SimpleExcel\SimpleExcelReader;
use App\Models\UserJob;
use App\Models\User;
use App\DataTables\BiocollectionsDataTable;
use Auth;
use Validator;

class BiocollectionController extends Controller
{

  public function autocomplete(Request $request)
  {
    $biocollections = Biocollection::where('acronym','like',$request->input('query')."%")
    ->orWhere('name','like', "%".$request->input('query')."%")
    ->selectRaw("biocollections.id as data, CONCAT(acronym,' ',name) as value")
    ->orderBy('acronym',"ASC")
    ->take(10)->get();
    return Response::json(['suggestions' => $biocollections]);
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BiocollectionsDataTable $dataTable)
    {
      return $dataTable->render('biocollections.index', []);
    }
    public function indexUserJob($job_id,BiocollectionsDataTable $dataTable)
    {        
        return $dataTable->with([
          'job_id' => $job_id,
      ])->render('biocollections.index');
    }
    public function checkih(Request $request)
    {
        if (is_null($request['acronym'])) {
            return Response::json(['error' => Lang::get('messages.acronym_error')]);
        }
        $apis = new ExternalAPIs();
        $ihdata = $apis->getIndexHerbariorum($request->acronym);
        if (is_null($ihdata)) {
            return Response::json(['error' => Lang::get('messages.acronym_not_found')]);
        }

        return Response::json(['ihdata' => $ihdata]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function create()
    {
        if (!Auth::user()) {
            return view('common.unauthorized');
        }
        $fullusers = User::where('access_level', '=', User::USER)->orWhere('access_level', '=', User::ADMIN)->get();
        return view('biocollections.create',compact('fullusers'));
    }

    protected function edit($id)
    {
        $biocollection = Biocollection::findOrFail($id);
        $this->authorize('update', $biocollection);
        $fullusers = User::where('access_level', '=', User::USER)->orWhere('access_level', '=', User::ADMIN)->get();

        $logoUrl = null;
        if ($biocollection->media->count())
        {
          $logo = $biocollection->media()->first();
          $fileUrl = $logo->getUrl();
          if (file_exists($logo->getPath('thumb'))) {
            $logoUrl = $logo->getUrl('thumb');
          } else {
            $logoUrl = $fileUrl;
          }
        }
        return view('biocollections.create',compact('fullusers','biocollection','logoUrl'));
    }


    /**
     * Validate dataset request
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return Validator
     */
    public function customValidate(Request $request, $id = null)
    {
        /* define rules */
        $fullusers = User::where('access_level', '=', User::USER)
        ->orWhere('access_level', '=', User::ADMIN)->get()->pluck('id');
        $fullusers = implode(',', $fullusers->all());
        $mimes = 'mimes:gif,jpeg,png';
        $rules = [
            'name' => 'required_if:irn,<0|max:191',
            'acronym' => 'required|max:20',
            'irn' => 'required',
            'admins' => 'nullable|array',
            'admins.*' => 'numeric|in:'.$fullusers,
            'collabs' => 'nullable|array',
            'collabs.*' => 'numeric|in:'.$fullusers,
            'logo' => 'file|nullable|'.$mimes,
        ];
        $validator = Validator::make($request->all(), $rules);

        /*check for duplicated entries */
        $validator->after(function ($validator) use ($request,  $id) {
            $has_similar = Biocollection::where('acronym','like',$request->acronym);
            if ($id) {
              $has_similar = $has_similar->where('id','<>',$id);
            }
            if ($has_similar->count() > 0) {
              $validator->errors()->add('acronym', Lang::get('messages.acronym_duplicated'));
              return;
            }
            /*
            if ($id) {
              $biocollection = Biocollection::findOrFail($id);
              if ($biocollection->admins->count() and !$request->admins) {
                $validator->errors()->add('admins', Lang::get('messages.admins_required'));
                return;
              }
            }
            */
        });

        return $validator;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Biocollection::class);
        $validator = $this->customValidate($request);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $biocollection = Biocollection::create([
          'name' => $request->name,
          'irn' => $request->irn,
          'acronym' => mb_strtoupper($request->acronym),
          'description' => $request->description,
        ]);
        $biocollection->setusers(null, $request->collabs, $request->admins);

        /* store logo if exists */
        if ($request->hasFile('logo')) {
          $mediaExtension = mb_strtolower($request->file('logo')->getClientOriginalExtension());
          $newMediaName = 'biocollection_'.$biocollection->id.'_logo';
          $newFileName = $newMediaName.".".$mediaExtension;
          $biocollection->addMedia($request->file('logo')->getRealPath())
          ->usingFileName($newFileName)
          ->usingName($newMediaName)
          ->toMediaCollection('logos');
        }

        return redirect('biocollections')->withStatus(Lang::get('messages.stored'));
    }

    public function update(Request $request, $id)
    {
        $biocollection = Biocollection::findOrFail($id);
        $this->authorize('update', $biocollection);
        $validator = $this->customValidate($request,$id);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data = $request->only(['name', 'irn', 'acronym','description']);
        $biocollection->update($data);

        if (!isset($request->ismanaged) and $request->admins) {
          /* remove people if there are no open requests */
          $status = $biocollection->odbrequests()->cursor()->map(function($r) {
            return !$r->status_closed;
          })->toArray();
          $status = array_sum($status);
          if ($status==0) {
            $biocollection->odbrequests()->delete();
            $request->collabs = [];
            $request->admins = [];
          }
        }
        $biocollection->setusers(null, $request->collabs, $request->admins);

        /* store logo if exists */
        if ($request->hasFile('logo')) {
          $mediaExtension = mb_strtolower($request->file('logo')->getClientOriginalExtension());
          $newMediaName = 'biocollection_'.$biocollection->id.'_logo';
          $newFileName = $newMediaName.".".$mediaExtension;
          //delete old if exists
          if ($biocollection->media->count())
          {
            $logo = $biocollection->media()->first();
            try {
                /* this will remove model and media files */
                $logo->delete();
            } catch (\Illuminate\Database\QueryException $e) {
                $message .= Lang::get('messages.fk_error');
            }
          }
          //add new logo
          $biocollection->addMedia($request->file('logo')->getRealPath())
          ->usingFileName($newFileName)
          ->usingName($newMediaName)
          ->toMediaCollection('logos');
        }



        return redirect('biocollections/'.$biocollection->id)->withStatus(Lang::get('messages.saved'));

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id, VouchersDataTable $dataTable)
    {
        $biocollection = Biocollection::findOrFail($id);
        $biocollections = Biocollection::orderBy('acronym')->cursor();
        $logoUrl = null;
        if ($biocollection->media->count())
        {
          $logo = $biocollection->media()->first();
          $fileUrl = $logo->getUrl();
          if (file_exists($logo->getPath('thumb'))) {
            $logoUrl = $logo->getUrl('thumb');
          } else {
            $logoUrl = $fileUrl;
          }
        }
        return $dataTable->with([
               'biocollection_id' => $id
           ])->render('biocollections.show', compact('biocollection','biocollections','logoUrl'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $biocollection = Biocollection::findOrFail($id);
        $this->authorize('delete', $biocollection);
        try {
            $biocollection->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->back()
                ->withErrors([Lang::get('messages.fk_error')]);
        }

        return redirect('biocollections')->withStatus(Lang::get('messages.removed'));
    }


    public function importJob(Request $request)
    {
      $this->authorize('create', Biocollection::class);
      $this->authorize('create', UserJob::class);
      if (!$request->hasFile('data_file')) {
          $message = Lang::get('messages.invalid_file_missing');
      } else {
        /*
            Validate attribute file
            Validate file extension and maintain original if valid or else
            Store may save a csv as a txt, and then the Reader will fail
        */
        $valid_ext = array("csv","ods",'xlsx');
        $ext = mb_strtolower($request->file('data_file')->getClientOriginalExtension());
        if (!in_array($ext,$valid_ext)) {
          $message = Lang::get('messages.invalid_file_extension');
        } else {
          try {
            $data = SimpleExcelReader::create($request->file('data_file'),$ext)->getRows()->toArray();
          } catch (\Exception $e) {
            $data = [];
            $message = json_encode($e);
          }
          if (is_array($data)) {
            UserJob::dispatch(ImportBiocollections::class,[
              'data' => ['data' => $data]
            ]);
            $message = Lang::get('messages.dispatched',['url' => Route('userjobs.list') ]);
          } else {
            $message = 'Something wrong with file';
          }
        }
      }
      return redirect('import/biocollections')->withStatus($message);
    }

}
