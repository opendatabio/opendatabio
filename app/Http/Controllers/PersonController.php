<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Person;
use App\Models\Biocollection;
use Illuminate\Support\Facades\Lang;
//use Illuminate\Support\Facades\Request;
use App\DataTables\PersonsDataTable;
use App\DataTables\ActivityDataTable;

use App\Jobs\ImportPersons;
use App\Jobs\DeleteMany;

use Spatie\SimpleExcel\SimpleExcelReader;
use Validator;

use Response;
use App\Models\UserJob;
use Auth;

class PersonController extends Controller
{
    // Functions for autocompleting person names, used in dropdowns. Expects a $request->query input
    public function autocomplete(Request $request)
    {
        $persons = Person::where('full_name', 'LIKE', ['%'.$request->input('query').'%'])
            ->orWhere('abbreviation', 'LIKE', ['%'.$request->input('query').'%'])
            ->selectRaw("id as data, CONCAT(full_name, ' [',abbreviation, ']') as value")
            ->orderBy('value', 'ASC')
            ->get();

        return Response::json(['suggestions' => $persons]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PersonsDataTable $dataTable)
    {
        $biocollections = Biocollection::all();

        return $dataTable->render('persons.index', [
            'biocollections' => $biocollections,
    ]);
    }
    public function indexUserJob($job_id,PersonsDataTable $dataTable)
    {
        $biocollections = Biocollection::all();
        return $dataTable->with([
          'job_id' => $job_id,
        ])->render('persons.index', compact('biocollections','job_id'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected function create()
     {
         if (!Auth::user()) {
             return view('common.unauthorized');
         }
         $biocollections = Biocollection::all();
         return view('persons.create',compact('biocollections'));
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Person::class);
        $this->checkValid($request);
        $validator = $this->checkValid($request);
        if ($validator->errors()->count()) {
            if ($request->from_the_api) {
              return implode(" | ",$validator->errors()->all());
            }
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        // checks for duplicates, except if the request is already confirmed
        // or importation is conducted through the API (this then is ignored)
        if (!$request->confirm and !$request->from_the_api) {
            $dupes = Person::duplicates($request->full_name, $request->abbreviation);
            if (sizeof($dupes)) {
                $request->flash();
                return view('persons.confirm', compact('dupes'));
            }
        }
        $person = Person::create($request->all());
        if (!isset($request->email_public)) {
          $public = 0;
        } else {
          $public = $request->email_public;
        }
        $person->email_public = $public;
        $person->save();

        if ($request->from_the_api) {
           return $person;
        }
        return redirect('persons')->withStatus(Lang::get('messages.stored'));
    }

    protected function checkValid(Request $request, $id = null)
    {
        $rules = [
        'full_name' => 'required|max:191',
        'abbreviation' => ['required', 'max:191', 'regex:'.config('app.valid_abbreviation'), 'unique:persons,abbreviation,'.$id],
        'email' => ['nullable', 'max:191', 'email', 'unique:persons,email,'.$id],
        ];
        //apply rules
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $person = Person::findOrFail($id);
        /*
        // TODO: obtaining collected is complicated when number of records is too large
        The portion below was commented and it may not be needed except for the count of
        collected.objects. So, this could be modified to show only counts.
        $person->load('collected.object');
        $vouchers = $person->vouchers;
        $vouchers->load(['identification', 'parent']);
        $collected = collect($person->vouchers)->merge($person->collected->map(function ($x) {return $x->object; }));
        $collected = $collected->reject(function ($x) {return is_null($x); });
        return view('persons.show', compact('person', 'collected'));
        */
        return view('persons.show', compact('person'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $person = Person::findOrFail($id);
        $biocollections = Biocollection::all();
        $taxons = $person->taxons->map(function($q) {
          return [
            'id' => $q->id,
            'fullname' => $q->scientificName
          ];
        });

        return view('persons.create', compact('person', 'biocollections', 'taxons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $person = Person::findOrFail($id);
        $this->authorize('update', $person);
        $validator = $this->checkValid($request,$id);
        if ($validator->errors()->count()) {
            if ($request->from_the_api) {
              return implode(" | ",$validator->errors()->all());
            }
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $person->update($request->only(['full_name', 'abbreviation', 'email','institution', 'biocollection_id','notes']));
        if (!isset($request->email_public)) {
          $person->email_public = 0;
        } else {
          $person->email_public = $request->email_public;
        }
        $person->save();

        // add/remove specialists
        $person->taxons()->sync($request->specialist);

        if ($request->from_the_api) {
           return $person;
        }
        return redirect('persons')->withStatus(Lang::get('messages.saved'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $person = Person::findOrFail($id);
        $this->authorize('delete', $person);
        try {
            $person->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->back()
                ->withErrors([Lang::get('messages.fk_error')])->withInput();
        }

        return redirect('persons')->withStatus(Lang::get('messages.removed'));
    }

    /*
      * Batch delete locations
    */
    public function batchDelete(Request $request)
    {
        $data = $request->all();
        $data['model'] = 'Person';
        UserJob::dispatch(DeleteMany::class,
        [
          'data' => ['data' => $data,]
        ]);
        return redirect('persons')->withStatus(Lang::get('messages.dispatched',['url' => Route('userjobs.list') ]));
    }



    public function activity($id, ActivityDataTable $dataTable)
    {
      $object = Person::findOrFail($id);
      return $dataTable->with('person', $id)->render('common.activity',compact('object'));
    }


    public function importJob(Request $request)
    {
      $this->authorize('create', Person::class);
      $this->authorize('create', UserJob::class);
      if (!$request->hasFile('data_file')) {
          $message = Lang::get('messages.invalid_file_missing');
      } else {
        /*
            Validate attribute file
            Validate file extension and maintain original if valid or else
            Store may save a csv as a txt, and then the Reader will fail
        */
        $valid_ext = array("csv","ods",'xlsx');
        $ext = mb_strtolower($request->file('data_file')->getClientOriginalExtension());
        if (!in_array($ext,$valid_ext)) {
          $message = Lang::get('messages.invalid_file_extension');
        } else {
          try {
            $data = SimpleExcelReader::create($request->file('data_file'),$ext)->getRows()->toArray();
          } catch (\Exception $e) {
            $data = [];
            $message = json_encode($e);
          }
          if (count($data)>0) {
            UserJob::dispatch(ImportPersons::class,[
              'data' => ['data' => $data]
            ]);
            $message = Lang::get('messages.dispatched',['url' => Route('userjobs.list') ]);
          } else {
            $message = 'Something wrong with file';
          }
        }
      }
      return redirect('import/persons')->withStatus($message);
    }

}
