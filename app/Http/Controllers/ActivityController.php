<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Controllers;

use Activity;
use Illuminate\Http\Request;
use App\Models\UserJob;
use App\Jobs\DeleteMany;
use Lang;

class ActivityController extends Controller
{
    /*
      * Batch delete locations
    */
    public function batchDelete(Request $request)
    {
        $data = $request->all();
        $data['model'] = 'Activity';
        UserJob::dispatch(DeleteMany::class,
        [
          'data' => ['data' => $data,]
        ]);
        return redirect()->back()->withStatus(Lang::get('messages.dispatched',['url' => Route('userjobs.list') ]));
    }

}
