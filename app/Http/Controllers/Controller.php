<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private function padZero($what, $howmany = 2)
    {
        return str_pad($what, $howmany, '0', STR_PAD_LEFT);
    }

    public function convertDateToString($month, $day = null, $year = null) {
        $year = (int) ((null == $year and is_array($month)) ? (isset($month['year']) ? $month['year'] : $month[2]) : $year);
        $day = (int) ((null == $day and is_array($month)) ? (isset($month['day']) ? $month['day'] : $month[1]) : $day);
        $month = (int) (is_array($month) ? (isset($month['month']) ? $month['month'] : $month[0]) : $month);
        if($year>0) {
          return $this->padZero($year, 4).'-'.$this->padZero($month).'-'.$this->padZero($day);
        }
        return null;
    }

}
