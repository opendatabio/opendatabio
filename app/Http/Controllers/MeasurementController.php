<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\MeasurementsDataTable;
use App\Models\Measurement;
use App\Models\Project;
use App\Models\Individual;
use App\Models\Voucher;
use App\Models\Taxon;
use App\Models\Location;
use App\Models\ODBTrait;
use App\Models\Person;
use App\Models\Dataset;
use App\Models\Media;
use App\Models\BibReference;
use App\Models\UserJob;
use App\Models\Summary;
use App\Jobs\ImportMeasurements;
use App\Jobs\DeleteMany;

use App\Models\ExternalAPIs;
use Response;
use Storage;
use Auth;
use Validator;
use Lang;
use Activity;
use App\Models\ActivityFunctions;
use App\DataTables\ActivityDataTable;


class MeasurementController extends Controller
{

    // The usual index method is hidden to provide a common interface to all requests
    // coming from different nested routes
    public function indexIndividuals($id, MeasurementsDataTable $dataTable)
    {
        $object = Individual::findOrFail($id);

        return $dataTable->with([
            'measured_type' => 'App\Models\Individual',
            'measured' => $id,
        ])->render('measurements.index', compact('object'));
    }

    // The usual index method is hidden to provide a common interface to all requests
    // coming from different nested routes
    public function index(MeasurementsDataTable $dataTable)
    {
        return $dataTable->render('measurements.index');
    }
    public function indexUserJob($job_id,MeasurementsDataTable $dataTable)
    {        
        return $dataTable->with([
            'job_id' => $job_id,
        ])->render('measurements.index',compact('job_id'));
    }
    public function indexIndividualsDatasets($id, MeasurementsDataTable $dataTable)
    {
      $ids = explode('|',$id);
      $object = Individual::findOrFail($ids[0]);
      $object_second = Dataset::findOrFail($ids[1]);
      return $dataTable->with(['measured' => $ids[0],'measured_type'=> 'App\Models\Individual','dataset' => $ids[1]])->render('measurements.index', compact('object','object_second'));
    }

    public function indexLocations($id, MeasurementsDataTable $dataTable)
    {
        $object = Location::findOrFail($id);
        return $dataTable->with([
            'measured' => $id,
            'measured_type' => 'App\Models\Location',
        ])->render('measurements.index', compact('object'));
    }

    public function indexLocationsProjects($id, MeasurementsDataTable $dataTable)
    {
        $ids = explode('|',$id);
        $object = Location::findOrFail($ids[0]);
        $object_second = Project::findOrFail($ids[1]);
        return $dataTable->with(['location' => $ids[0],'project' => $ids[1]])->render('measurements.index', compact('object','object_second'));
    }

    public function indexLocationsDatasets($id, MeasurementsDataTable $dataTable)
    {
        $ids = explode('|',$id);
        $object = Location::findOrFail($ids[0]);
        $object_second = Dataset::findOrFail($ids[1]);
        return $dataTable->with(['location' => $ids[0],'dataset' => $ids[1]])->render('measurements.index', compact('object','object_second'));
    }

    public function indexBibreferences($id, MeasurementsDataTable $dataTable)
    {
        $object = BibReference::findOrFail($id);
        return $dataTable->with([
            'bibreference' => $id,
        ])->render('measurements.index', compact('object'));
    }


    public function indexLocationsRoot($id, MeasurementsDataTable $dataTable)
    {
        $object = Location::findOrFail($id);

        return $dataTable->with([
            'location' => $id,
        ])->render('measurements.index', compact('object'));
    }
    public function indexVouchers($id, MeasurementsDataTable $dataTable)
    {
        $object = Voucher::findOrFail($id);
        return $dataTable->with([
            'measured_type' => 'App\Models\Voucher',
            'measured' => $id,
        ])->render('measurements.index', compact('object'));
    }

    public function indexTaxons($id, MeasurementsDataTable $dataTable)
    {
        $object = Taxon::findOrFail($id);

        return $dataTable->with([
            'taxon' => $id
        ])->render('measurements.index', compact('object'));
    }

    public function indexTaxon($id, MeasurementsDataTable $dataTable)
    {
        $object = Taxon::findOrFail($id);
        return $dataTable->with([
            'measured_type' => 'App\Models\Taxon',
            'measured' => $id,
        ])->render('measurements.index', compact('object'));
    }

    public function indexMedia($id, MeasurementsDataTable $dataTable)
    {
        $object = Media::withoutGlobalScopes()->findOrFail($id);
        return $dataTable->with([
            'measured_type' => 'App\Models\Media',
            'measured' => $id,
        ])->render('measurements.index', compact('object'));
    }

    public function indexTaxonsProjects($id, MeasurementsDataTable $dataTable)
    {
        $ids = explode('|',$id);
        $object = Taxon::findOrFail($ids[0]);
        $object_second = Project::findOrFail($ids[1]);
        return $dataTable->with(['taxon' => $ids[0],'project' => $ids[1]])->render('measurements.index', compact('object','object_second'));
    }

    public function indexTaxonsDatasets($id, MeasurementsDataTable $dataTable)
    {
        $ids = explode('|',$id);
        $object = Taxon::findOrFail($ids[0]);
        $object_second = Dataset::findOrFail($ids[1]);
        return $dataTable->with(['taxon' => $ids[0],'dataset' => $ids[1]])->render('measurements.index', compact('object','object_second'));
    }

    public function indexTaxonsLocations($id, MeasurementsDataTable $dataTable)
    {
        $ids = explode('|',$id);
        $object = Taxon::findOrFail($ids[0]);
        $object_second = Location::findOrFail($ids[1]);
        return $dataTable->with(['taxon' => $ids[0],'location' => $ids[1]])->render('measurements.index', compact('object','object_second'));
    }

    public function indexDatasets($id, MeasurementsDataTable $dataTable)
    {
        //$dataset = Dataset::with(['measurements.measured', 'measurements.odbtrait'])->findOrFail($id);
        //check if dataset and trait are informed in id
        $ids = preg_split("/\|/", $id);
        $dataset = isset($ids[0]) ? $ids[0] : null;
        $odbtrait = isset($ids[1]) ? $ids[1] : null;
        $measured_type = isset($ids[2]) ? $ids[2] : null;
        $with = [
          'dataset' => $dataset,
          'odbtrait' => $odbtrait,
          'measured_type' => $measured_type,
        ];
        $object_second = null;
        if (null != $odbtrait) {
          $object_second = ODBTrait::findOrFail($odbtrait);
        }
        if (null != $dataset) {
          $object = Dataset::findOrFail($dataset);
        }

        return $dataTable->with($with)->render('measurements.index', compact('object','object_second','measured_type'));
    }


    public function indexTraits($id, MeasurementsDataTable $dataTable)
    {
        $object = ODBTrait::findOrFail($id);

        return $dataTable->with([
            'odbtrait' => $id,
        ])->render('measurements.index', compact('object'));
    }

    protected function create($object)
    {
        if (!Auth::user()) {
            return view('common.unauthorized');
        }
        $persons = Person::all();
        $references = BibReference::all();
        $datasets = Auth::user()->datasets;

        $other_measurements = Measurement::where('measured_id',$object->id)->where('measured_type',get_class($object))->get();
        if ($other_measurements->count()) {
            $other_measurements = $other_measurements->map(function($tr){
                return [
                    'id' => $tr->id,
                    'label' => $tr->selector_name,
                    'date' => $tr->date,
                ];
            })->sortByDesc('date')->values()->toArray();
        } else {
            $other_measurements = [];
        }
        return view('measurements.create', compact('object', 'references', 'datasets', 'persons','other_measurements'));
    }

    public function createIndividuals($id)
    {
        $object = Individual::findOrFail($id);

        return $this->create($object);
    }

    public function createVouchers($id)
    {
        $object = Voucher::findOrFail($id);

        return $this->create($object);
    }

    public function createLocations($id)
    {
        $object = Location::findOrFail($id);

        return $this->create($object);
    }

    public function createTaxons($id)
    {
        $object = Taxon::findOrFail($id);

        return $this->create($object);
    }

    public function show($id)
    {
        $measurement = Measurement::findOrFail($id);
        //if spectral pass graph
        if ($measurement->odbtrait->type==ODBTrait::SPECTRAL) {
          $odbtrait = $measurement->odbtrait;
          $min = $odbtrait->range_min;
          $max = $odbtrait->range_max;
          $step = ($max-$min)/(($odbtrait->value_length)-1);
          $xvalues = range($min,$max,$step);
          $yvalues = explode(";",$measurement->value_a);
          $values = array_combine($xvalues,$yvalues);
          $data = array_map(function($val) use($values) {
            return ['x' => $val, 'y' => $values[(string)$val]+0];
          },$xvalues);
          $chartjs = app()->chartjs
              ->name('SpectralMeasurement')
              ->type('scatter')
              ->datasets([
                  [
                      'label' => 'Spectrum',
                      'backgroundColor' => "#ffffff",
                      'borderColor' => "#339933",
                      "pointBorderColor" => "#339933",
                      "pointBackgroundColor" => "#339933",
                      "pointHoverBackgroundColor" => "#fff",
                      "pointHoverBorderColor" => "rgba(220,220,220,1)",
                      'pointRadius' => 1,
                      'fill' => false,
                      'data' => $data,
                  ]
              ])
              ->options([
                  'maintainAspectRatio' => true,
              ]);
              return view('measurements.show', compact('measurement','chartjs'));
        }


        return view('measurements.show', compact('measurement'));
    }

    public function customValidate(Request $request,$measurement_id=null)
    {
        $rules = [
            'trait_id' => 'required|integer',
            'measured_id' => 'required|integer',
            'measured_type' => 'required|string',
            'date_year' => 'required|integer',
            'dataset_id' => 'required|integer',
            'persons' => 'required|array',
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->sometimes('value', 'required', function ($request) {
            $odbtrait = ODBTrait::findOrFail($request->trait_id);

            return ODBTrait::LINK != $odbtrait->type;
        });
        $validator->sometimes('link_id', 'required', function ($request) {
            $odbtrait = ODBTrait::findOrFail($request->trait_id);
            return ODBTrait::LINK == $odbtrait->type;
        });
        $validator->after(function ($validator) use ($request,$measurement_id) {
            $odbtrait = ODBTrait::findOrFail($request->trait_id);

            if ($request->location_id and !$request->measured_type=="App\Models\Taxon") {
                $validator->errors()->add('location_id', Lang::get('messages.measured_location_taxon_only'));                    
            }

            if (!$odbtrait->valid_type($request->measured_type)) {
                $validator->errors()->add('trait_id', Lang::get('messages.invalid_trait_type_error'));
            }
            $colldate = [$request->date_month, $request->date_day, $request->date_year];
            if (!Measurement::checkDate($colldate)) {
                $validator->errors()->add('date_day', Lang::get('messages.invalid_date_error'));
            }
            // measurement date must be in the past or today
            if (!Measurement::beforeOrSimilar($colldate, date('Y-m-d'))) {
                $validator->errors()->add('date_day', Lang::get('messages.date_future_error'));
            }
            if (ODBTrait::SPECTRAL !== $odbtrait->type and isset($odbtrait->range_min) and $request->value < $odbtrait->range_min) {
                $validator->errors()->add('value', Lang::get('messages.value_out_of_range'));
            }
            if (ODBTrait::SPECTRAL !== $odbtrait->type and isset($odbtrait->range_max) and $request->value > $odbtrait->range_max) {
                $validator->errors()->add('value', Lang::get('messages.value_out_of_range'));
            }
            // Checks if spectral has the correct number of values and if values are numeric
            if (ODBTrait::SPECTRAL == $odbtrait->type) {
               $spectrum = explode(";",$request->value);
               if (count($spectrum) != $odbtrait->value_length or count($spectrum) != count(array_filter($spectrum, "is_numeric"))) {
                $validator->errors()->add('value', Lang::get('messages.value_spectral').": ".count(explode(";",$request->value))." v.s. ".$odbtrait->value_length);
               }
            }
            // Checks if integer variable is integer type
            if (ODBTrait::QUANT_INTEGER == $odbtrait->type and strval($request->value) != strval(intval($request->value))) {
                $validator->errors()->add('value', Lang::get('messages.value_integer'));
            }
            if (in_array($odbtrait->type, [ODBTrait::QUANT_REAL, ODBTrait::LINK]) and isset($request->value)) {
                if (!is_numeric($request->value)) {
                  $validator->errors()->add('value', Lang::get('messages.value_numeric'));
                }
            }
            if (in_array($odbtrait->type, [ODBTrait::CATEGORICAL, ODBTrait::ORDINAL, ODBTrait::CATEGORICAL_MULTIPLE])) {
                // validates that the chosen category is ACTUALLY from the trait
                $valid = $odbtrait->categories->pluck('id')->all();
                if (is_array($request->value)) {
                    foreach ($request->value as $value) {
                        if (!in_array($value, $valid)) {
                            $validator->errors()->add('value', Lang::get('messages.trait_measurement_mismatch'));
                        }
                    }
                } elseif ($request->value) {
                    if (!in_array($request->value, $valid)) {
                        $validator->errors()->add('value', Lang::get('messages.trait_measurement_mismatch'));
                    }
                }
            }
            // Checks if spectral has the correct number of values and if values are numeric
            if (ODBTrait::GENEBANK == $odbtrait->type) {
               $hasgenebank = ExternalAPIs::getGeneBankData($request->value,$db = 'nucleotide');
               if (null == $hasgenebank) {
                $validator->errors()->add('value', $request->value." ".Lang::get('messages.genebank_not_found'));
               }
               //check if measurement already exists
               $accession= $request->value;
               $hasvalue = Measurement::whereHas('odbtrait',function($t){
                 $t->where('type',ODBTrait::GENEBANK);
               })->where('value_a','like',$accession);
               if ($measurement_id) {
                 $hasvalue = $hasvalue->whereNotIn('measurements.id',$measurement_id);
               }
               if ($hasvalue->count() ) {
                 $validator->errors()->add('value', $request->value." ".Lang::get('messages.genebank_already_registered')." ".$hasvalue->first()->measured->rawLink());
               }
            }

            $registry = $request->all();
            $oldregistry = null;
            if ($measurement_id) {
              $oldregistry = Measurement::findOrFail($measurement_id);
            } else {
                //check only on creation check for duplication
                $existing_dups = Measurement::checkDuplicateMeasurement($registry,$oldregistry);
                $allowdups = isset($request->allow_duplicated) ? $request->allow_duplicated : 0;
                if ($allowdups<=$existing_dups) {
                    $validator->errors()->add(' value ', Lang::get('messages.duplicated_measurement',[
                        'nrecords' => $existing_dups,
                        'duplicated' => ($existing_dups+1),
                    ]));            
                }
            }

            //validade parent measurement
            if(isset($request->parent_id))  {
                $pm = Measurement::find($request->parent_id);                           
                if(is_object($pm)){
                    if ($oldregistry) {
                        $dt = $oldregistry->date;
                    } else {
                        $dt = implode("-",[$colldate[2],$colldate[0],$colldate[1]]);
                    }
                    $valid = $pm->measured_type==$request->measured_type;
                    $valid = $valid and $pm->measured_id==$request->measured_id;
                    $valid = $valid and !$pm->trait_id==$request->trait_id;
                    $valid = $valid and $pm->date==Date("Y-m-d",strtotime($dt));
                } else {
                    $valid =  false;
                }   
                if(!$valid){
                    $validator->errors()->add(' parent_id ', Lang::get('messages.measurement_parent_invalid'));     
                }
            }

        });

        return $validator;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataset = Dataset::findOrFail($request->dataset_id);
        $this->authorize('create', [Measurement::class, $dataset]);
        $validator = $this->customValidate($request);
        if ($validator->fails()) {
            if ($request->from_the_api) {
              return implode(" | ",$validator->errors()->all());
            }
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        // Fixes https://github.com/opendatabio/opendatabio/issues/218
        $odbtrait = ODBTrait::findOrFail($request->trait_id);
        if (ODBTrait::QUANT_REAL == $odbtrait->type) {
            $request->value = str_replace(',', '.', $request->value);
        }
        $new_record = $request->only([
            'trait_id', 'measured_id', 'measured_type', 'dataset_id', 'bibreference_id', 'notes','parent_id','location_id'
        ]);
        $measurement = new Measurement($new_record);
        $measurement->setDate($request->date_month, $request->date_day, $request->date_year);
        $measurement->save();
        
        $measurement->collectors()->attach($request->persons);
        if (ODBTrait::LINK == $measurement->type) {
            $measurement->value = $request->value;
            $measurement->value_i = $request->link_id;
        } else {
            $measurement->valueActual = $request->value;
        }
        $measurement->save();

        if ($request->from_the_api) {
          return $measurement;
        }
        return redirect('measurements/'.$measurement->id)->withStatus(Lang::get('messages.stored'));
    }

    public function edit($id)
    {
        if (!Auth::user()) {
            return view('common.unauthorized');
        }
        $measurement = Measurement::findOrFail($id);
        $object = $measurement->measured;
        $persons = Person::all();
        $references = BibReference::all();
        $datasets = Auth::user()->datasets;

        $other_measurements = Measurement::where('measured_id',$measurement->measured_id)->where('measured_type',$measurement->measured_type)->where('date',$measurement->date)->where('id',"<>",$measurement->id)->get();
        if ($other_measurements->count()) {
            $other_measurements = $other_measurements->map(function($tr){
                return [
                    'id' => $tr->id,
                    'label' => $tr->selector_name,
                    'date' => $tr->date,
                ];
            })->sortByDesc('date')->values()->toArray();
        } else {
            $other_measurements = [];
        }
        return view('measurements.create', compact('measurement', 'object', 'references', 'datasets', 'persons','other_measurements'));
    }

    public function update(Request $request, $id)
    {
        $measurement = Measurement::findOrFail($id);
        $this->authorize('update', $measurement);
        $validator = $this->customValidate($request,$id);
        if ($validator->fails()) {
            if ($request->from_the_api) {
              return implode(" | ",$validator->errors()->all());
            }
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        // Fixes https://github.com/opendatabio/opendatabio/issues/218
        $odbtrait = ODBTrait::findOrFail($request->trait_id);
        if (ODBTrait::QUANT_REAL == $odbtrait->type) {
            $request->value = str_replace(',', '.', $request->value);
        }
    
        $current_categories = [];
        if ($measurement->categories->count()) {
          $current_categories = $measurement->categories->pluck('category_id')->toArray();
        }

        $measurement->update($request->only([
            'trait_id', 'measured_id', 'measured_type', 'dataset_id', 'bibreference_id', 'notes','parent_id','location_id'
        ]));
        if (ODBTrait::LINK == $measurement->type) {
            $measurement->value = $request->value;
            $measurement->value_i = $request->link_id;
        } else {
            $measurement->valueActual = $request->value;
        }
        $current_persons = $measurement->collectors->pluck('id');
        $measurement->setDate($request->date_month, $request->date_day, $request->date_year);
        $measurement->save();

        $measurement->collectors()->detach();
        $measurement->collectors()->attach($request->persons);


        //log changes in categories
        $new_categories = [];
        $measurement = Measurement::findOrFail($id);
        if ($measurement->categories->count()) {
          $new_categories = $measurement->categories->pluck('category_id')->toArray();
        }
        ActivityFunctions::logCustomPivotChanges($measurement,$current_categories,$new_categories,'measurement','categories updated',$pivotkey='traitCategory');
        
        //log changes in persons
        ActivityFunctions::logCustomPivotChanges($measurement,$current_persons->all(),$request->persons,'measurement','measurer updated',$pivotkey='person');

        /*
        $new_link_id = null;
        $new_value = null;
        if (!in_array($measurement->type,ODBTrait::CATEGORICAL_TRAITS)) {
          $new_value = $measurement->raw_value_actual;
        }
        if (ODBTrait::LINK == $measurement->type) {
          $new_link_id = $measurement->value_i;
          $pivotkey = class_basename($measurement->linked_type);
          ActivityFunctions::logCustomPivotChanges($measurement,[$old_link_id],[$new_link_id],'measurement','link value updated',$pivotkey);
        }
        */
        //ActivityFunctions::logCustomChanges($measurement,['value' => $old_value],['value' => $new_value],'measurement','value updated',['value']);

        if ($request->from_the_api) {
          return $measurement;
        }
        return redirect('measurements/'.$id)->withStatus(Lang::get('messages.saved'));
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $measurement = Measurement::findOrFail($id);

        /* will authorize only if no related models are found */
        $this->authorize('delete', $measurement);

        try {
            /*remove in related if found */
            $measurement->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->back()
                ->withErrors([Lang::get('messages.fk_error')])->withInput();
        }
        $measured = $measurement->measured;
        $measured_link = class_basename($measured);
        $measured_link = mb_strtolower($measured_link).'s';
        return redirect($measured_link.'/'.$measured->id)->withStatus(Lang::get('messages.removed'));
    }



    public function activity($id, ActivityDataTable $dataTable)
    {
        $object = Measurement::findOrFail($id);
        return $dataTable->with('measurement', $id)->render('common.activity',compact('object'));
    }

    public function importJob(Request $request)
    {
      $this->authorize('create', Measurement::class);
      $this->authorize('create', UserJob::class);
      if (!$request->hasFile('data_file')) {
          $message = Lang::get('messages.invalid_file_missing');
      } else {
        /*
            Validate attribute file
            Validate file extension and maintain original if valid or else
            Store may save a csv as a txt, and then the Reader will fail
        */
        $valid_ext = array("csv","ods",'xlsx');
        $ext = mb_strtolower($request->file('data_file')->getClientOriginalExtension());
        if (!in_array($ext,$valid_ext)) {
          $message = Lang::get('messages.invalid_file_extension');
        } else {
          $filename = uniqid().".".$ext;
          $request->file('data_file')->storeAs("public/tmp",$filename);

          UserJob::dispatch(ImportMeasurements::class,[
            'data' => [
                'data' => null,
                'filename' => $filename,
                'filetype' => $ext,
              ],
          ]);
          $message = Lang::get('messages.dispatched',['url' => Route('userjobs.list') ]);

        }
      }
      return redirect('import/measurements')->withStatus($message);
    }



    public function checkGeneBank(Request $request)
    {
        if (is_null($request['accession'])) {
            return Response::json(['error' => Lang::get('messages.genebank_missing_accession')]);
        }
        $hasgenebank = ExternalAPIs::getGeneBankData($request->accession,$db = 'nucleotide');
        if (is_null($hasgenebank)) {
            return Response::json(['error' => Lang::get('messages.genebank_not_found')]);
        }
        $gbnote = $hasgenebank[$request->accession]['genebank_title'];
        $gbnote = "<strong>".Lang::get('messages.genebank_found')."</strong>:<br>".$gbnote;
        return Response::json(['gbdata' => $gbnote]);
    }


    /*
      * Batch delete measurements
    */
    public function batchDelete(Request $request)
    {
        $data = $request->all();
        $data['model'] = 'Measurement';
        UserJob::dispatch(DeleteMany::class,
        [
          'data' => ['data' => $data,]
        ]);
        return redirect('home')->withStatus(Lang::get('messages.dispatched',['url' => Route('userjobs.list') ]));
    }
}
