<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Controllers;

use Config;
use Session;
use Auth;
use App\Models\Individual;
use App\Models\Voucher;
use App\Models\Project;
use App\Models\Dataset;
use App\Models\Identification;
use App\Models\Measurement;
use App\Models\IndividualLocation;
use App\Models\Media;
use App\Models\Biocollection;
use DB;
use Lang;

class WelcomeController extends Controller
{




    public function index()
    {
        /*
        if (Auth::user()) {
          return redirect('home/'.Session::get('applocale'));
        }
        */
        $counts = [
          Lang::get('messages.datasets') =>
          [
            Dataset::withoutGlobalScopes()->count(),
            url('datasets'),
          ],
          Lang::get('messages.measurements') =>
          [
            Measurement::withoutGlobalScopes()->count(),
            url('measurements'),
          ],
          Lang::get('messages.organisms') =>
          [
            IndividualLocation::withoutGlobalScopes()->count(),
            url('individuals'),
          ],
          Lang::get('messages.species') =>
          [
            Identification::pluck('taxon_id')->unique()->count(),
            url('taxons'),
          ],
          Lang::get('messages.preserved_specimens') =>
          [
            Voucher::withoutGlobalScopes()->count(),
            url('vouchers'),
          ],
          Lang::get('messages.projects') =>
          [
            Project::withoutGlobalScopes()->count(),
            url('projects'),
          ],
        ];
        $biocollections = Biocollection::whereHas('admins')->get();
        $projects = Project::inRandomOrder()->limit(100)->get();
        //$datasets = null;
        ksort($counts);
        return view('welcome',compact('counts','biocollections','projects'));
    }

    // This method sets the locale for the session:
    public function setAppLocale($locale)
    {
        if (isset(Config::get('languages')[$locale])) {
            Session::put('applocale', $locale);
        }

        return redirect()->back();
    }
}
