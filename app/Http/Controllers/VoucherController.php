<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\DataTables\VouchersDataTable;
use Validator;
use App\Models\Individual;
use App\Models\Person;
use App\Models\Project;
use App\Models\Location;
use App\Models\Biocollection;
use App\Models\Identification;
use App\Models\Voucher;
use App\Models\Taxon;
use App\Models\Dataset;
use App\Models\Summary;
use App\Models\BibReference;
use App\Models\Collector;

use Auth;

use Lang;
use DB;
use Log;
use Activity;
use Response;
use App\Models\ActivityFunctions;
use App\DataTables\ActivityDataTable;

use App\Models\UserJob;
use App\Jobs\ImportVouchers;
use App\Jobs\DeleteMany;

use Spatie\SimpleExcel\SimpleExcelReader;



class VoucherController extends Controller
{

    /**
      * Autocompleting in dropdowns. Expects a $request->query input
    **/
    public function autocomplete(Request $request)
    {
      $query = $request->input('query');
      $vouchers = Voucher::join('persons','person_id','=','persons.id')
      ->where('number', 'like', ["{$query}%"])
      ->orWhere('persons.full_name','like',["%{$query}%"])->take(30)->get();
      $vouchers = collect($vouchers)->transform(function ($voucher) {
          $voucher->value = $voucher->fullname;
          return $voucher;
      });
      return Response::json(['suggestions' => $vouchers]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(VouchersDataTable $dataTable)
    {
        return $dataTable->render('vouchers.index', []);
    }
    public function indexUserJob($job_id,VouchersDataTable $dataTable)
    {        
        return $dataTable->with([
          'job_id' => $job_id,
        ])->render('vouchers.index',compact('job_id'));
    }

    public function indexBioCollections($id, VouchersDataTable $dataTable)
    {
        $object = Biocollection::findOrFail($id);
        return $dataTable->with('biocollection_id', $id)->render('vouchers.index', compact('object'));
    }
    public function indexBibReferences($id, VouchersDataTable $dataTable)
    {
        $object = BibReference::findOrFail($id);
        return $dataTable->with('bibreference_id', $id)->render('vouchers.index', compact('object'));
    }

    public function indexTaxons($id, VouchersDataTable $dataTable)
    {
        $object = Taxon::findOrFail($id);

        return $dataTable->with('taxon', $id)->render('vouchers.index', compact('object'));
    }
    public function indexTaxonsProjects($id, VouchersDataTable $dataTable)
    {
        $ids = explode('|',$id);
        $object = Taxon::findOrFail($ids[0]);
        $object_second = Project::findOrFail($ids[1]);
        return $dataTable->with(['taxon' => $ids[0],'project' => $ids[1]])->render('vouchers.index', compact('object','object_second'));
    }
    public function indexTaxonsDatasets($id, VouchersDataTable $dataTable)
    {
        $ids = explode('|',$id);
        $object = Taxon::findOrFail($ids[0]);
        $object_second = Dataset::findOrFail($ids[1]);
        return $dataTable->with(['taxon' => $ids[0],'dataset' => $ids[1]])->render('vouchers.index', compact('object','object_second'));
    }

    public function indexTaxonsLocations($id, VouchersDataTable $dataTable)
    {
        $ids = explode('|',$id);
        $object = Taxon::findOrFail($ids[0]);
        $object_second = Location::findOrFail($ids[1]);
        return $dataTable->with(['taxon' => $ids[0],'location' => $ids[1]])->render('measurements.index', compact('object','object_second'));
    }

    public function indexProjects($id, VouchersDataTable $dataTable)
    {
        $object = Project::findOrFail($id);

        return $dataTable->with('project', $id)->render('vouchers.index', compact('object'));
    }

    public function indexPersons($id, VouchersDataTable $dataTable)
    {
        $object = Person::findOrFail($id);

        return $dataTable->with('person', $id)->render('vouchers.index', compact('object'));
    }

    public function indexLocations($id, VouchersDataTable $dataTable)
    {
        $object = Location::findOrFail($id);

        return $dataTable->with('location', $id)->render('vouchers.index', compact('object'));
    }

    public function indexLocationsProjects($id, VouchersDataTable $dataTable)
    {
        $ids = explode('|',$id);
        $object = Location::findOrFail($ids[0]);
        $object_second = Project::findOrFail($ids[1]);
        return $dataTable->with(['location' => $ids[0],'project' => $ids[1]])->render('vouchers.index', compact('object','object_second'));
    }
    public function indexLocationsDatasets($id, VouchersDataTable $dataTable)
    {
        $ids = explode('|',$id);
        $object = Location::findOrFail($ids[0]);
        $object_second = Dataset::findOrFail($ids[1]);
        return $dataTable->with(['location' => $ids[0],'dataset' => $ids[1]])->render('vouchers.index', compact('object','object_second'));
    }


    public function indexIndividuals($id, VouchersDataTable $dataTable)
    {
        $object = Individual::findOrFail($id);

        return $dataTable->with('individual', $id)->render('vouchers.index', compact('object'));
    }

    public function indexDatasets($id, VouchersDataTable $dataTable)
    {
        $object = Dataset::findOrFail($id);
        return $dataTable->with('dataset', $id)->render('vouchers.index',compact('object'));
    }

    public function indexFiltered($params, VouchersDataTable $dataTable)
    {
        $filterparams = unserialize($params);
        return $dataTable->with($filterparams)
        ->render('vouchers.index',compact('filterparams'));
    }

    public function filterVouchers(Request $request)
    {
      $requested = [
        "filter_taxon_id" => $request->filter_taxon_id,
        "filter_taxon_data" => isset($request->filter_taxon_id) ? Taxon::whereIn('id',$request->filter_taxon_id)->cursor()->map(function($t){
          return [ 
            'id' => $t->id,
            'name' => $t->fullname];})->sortBy('name')->values()->pluck('name','id')->toArray() : [],
        "filter_location_id" => $request->filter_location_id,
        "filter_location_data" => isset($request->filter_location_id) ? Location::withoutGeom()
        ->whereIn('id',$request->filter_location_id)->pluck('name','id') ->toArray() : [],
        "filter_persons_ids" => $request->filter_persons_ids,
        "filter_persons_data" => isset($request->filter_persons_ids) ? Person::whereIn('id',$request->filter_persons_ids)->pluck('full_name','id') ->toArray() : [],
        "filter_min_date" => $request->filter_min_date,
        "filter_max_date" => $request->filter_max_date,
        "filter_number" => $request->filter_number,
        "filter_datasets_ids" => $request->filter_datasets_ids,
        "filter_datasets_data" => isset($request->filter_datasets_ids) ? Dataset::whereIn('id',$request->filter_datasets_ids)->pluck('name','id') ->toArray() : [],
        "filter_projects_ids" => $request->filter_projects_ids,
        "filter_projects_data" => isset($request->filter_projects_ids) ? Project::whereIn('id',$request->filter_projects_ids)->pluck('name','id') ->toArray() : [],
        "filter_biocollections_ids" => $request->filter_biocollections_ids,
        "filter_biocollections_data" => isset($request->filter_biocollections_ids) ? Biocollection::whereIn('id',$request->filter_biocollections_ids)->pluck('acronym','id') ->toArray() : [],
      ];
      $params = [
        "taxon" => $request->filter_taxon_id,
        "location" => $request->filter_location_id,
        "person" => $request->filter_persons_ids,
        "date_min" => $request->filter_min_date,
        "date_max" => $request->filter_max_date,
        "dataset" => $request->filter_datasets_ids,
        "project" => $request->filter_projects_ids,
        "biocollection_id" => $request->filter_biocollections_ids,
        'number' => $request->filter_number,
      ];
      $params = array_filter($params,function($q) { return $q!=null;});
      $params = array_merge($requested,$params);
      //dd($params);
      return redirect('vouchers/filtered/'.serialize($params));


    }










    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createIndividuals($id)
    {
        $individual = Individual::findOrFail($id);
        return $this->create($individual,null);
    }
    /*
    public function createLocations($id)
    {
        $location = Location::findOrFail($id);

        return $this->create($location);
    }
    */

    public function createBioCollections($id)
    {
        $biocollection = Biocollection::findOrFail($id);
        $isuser = ($biocollection->isUser(Auth::user()) or $biocollection->users->count()==0);
        if ($isuser) {
          return $this->create(null,$biocollection->id);
        }
        return redirect()->back()
            ->withErrors("You have no permission to add vouchers to this BioCollection. Request BioCollection staff to add such voucher to your registered individuals");
    }


    protected function create($individual=null,$biocollection_id=null)
    {
        if (!Auth::user()) {
            return view('common.unauthorized');
        }
        $user = Auth::user()->id;
        //only biocollections available if can be used
        $biocollections = Biocollection::doesntHave('users')->orWhereHas('users', function($query) use($user) {
          $query->where('users.id', '=', $user);
        })->orderBy('acronym')->cursor();
        $persons = Person::all();
        $datasets = Auth::user()->datasets;
        $bibreferences = BibReference::select('id',DB::raw('odb_bibkey(bibtex) as bibkey'))->orderByRaw('odb_bibkey(bibtex)')->get();
        return view('vouchers.create', compact('persons','biocollections','individual','datasets','biocollection_id','bibreferences'));
    }



    //validate request values when storing or updating records
    public function customValidate(Request $request, Voucher $voucher = null)
    {
        // to check for duplicates
        $voucherid = null;
        if ($voucher) {
            //if editing ignores self
            $voucherid = $voucher->id;
        }

        //these are mandatory in any case
        $rules = [
            'individual_id' => 'required|integer',
            'biocollection_id' => 'required|integer',
            'biocollection_type' => 'required|integer',
            'dataset_id' => 'required|integer'
        ];
        $checkcollector = false;
        //if number or collector is provided, then this required different validation
        if (null != $request->number or null != $request->collector) {
            $rules['collector'] = 'array';
            $rules['number'] = [
                'required',
                'string',
                'max:191'
            ];
            $checkcollector = true;
        }
        //apply rules
        $validator = Validator::make($request->all(), $rules);

        $validator->after(function ($validator) use ($request,$checkcollector,$voucherid) {
            //validate if collector, number and date if provided
            if ($checkcollector) {
              //validate date
              // validates date
              if (isset($request->date_month)) {
                $colldate = [$request->date_month, $request->date_day, $request->date_year];
              } else {
                $colldate = $request->date;
              }
              if (null == $colldate) {
                $validator->errors()->add('date_day', Lang::get('messages.missing_date'));
              } else {
                if (!Voucher::checkDate($colldate)) {
                  $validator->errors()->add('date_day', Lang::get('messages.invalid_date_error'));
                }
                // collection date must be in the past or today
                if (!Voucher::beforeOrSimilar($colldate, date('Y-m-d'))) {
                    $validator->errors()->add('date_day', Lang::get('messages.date_future_error'));
                }


                //validate unique records
                //vouchers belong to individuals which already controls uniqueness of collector_main+numberOrtag+location
                //a new voucher with a collector info, can not have: a) an individual other then selfwith same location, tag=number and same main collector; b) a voucher with same number and main collector and same biocollection_id+biocollection_number
                //Log::info($request->individual_id." and ".$request->individual);
                $individual = Individual::findOrFail($request->individual_id);
                $locationid = $individual->locations()->pluck('locations.id')->toArray();
                $number = $request->number;
                $maincollector= $request->collector[0];

                $isunique = Voucher::whereHas('collector_main',function($q) use($maincollector) {
                  $q->where('collectors.person_id',$maincollector);
                })->where('number',$number)->where('biocollection_id',$request->biocollection_id);
                $msg = "messages.voucher_duplicate";
                if ($voucherid) {
                  $isunique = $isunique->where('id',"<>",$voucherid);
                }
                if ($request->biocollection_number) {
                  $isunique = $isunique->where('biocollection_number',$request->biocollection_number);
                  $msg .= ".biocollection_number";
                } else {
                  $msg .= ".same_individual";
                  //$date_str = $this->convertDateToString($colldate);
                  $isunique = $isunique->where('individual_id',$individual->id)->whereNull('biocollection_number');
                }
                $isunique = ($isunique->count());
                if ($isunique > 0) {
                  $validator->errors()->add('number', Lang::get($msg));
                }
              }
            } else {

              $isunique = Voucher::where('individual_id',$request->individual_id)->where('biocollection_id',$request->biocollection_id);
              //ignore if editing;
              if ($voucherid) {
                $isunique = $isunique->where('id',"<>",$voucherid);
              }
              $isalsounique = 0;
              if ($request->biocollection_number) {
                $isunique = $isunique->where('biocollection_number',$request->biocollection_number);
                //biocollection_number and id must be unique as well
                $isalsounique = Voucher::where('biocollection_id',$request->biocollection_id)->where('biocollection_number',$request->biocollection_number);
                if ($voucherid) {
                  $isalsounique = $isalsounique->where('id',"<>",$voucherid);
                }
                $isalsounique = $isalsounique->count();
              } else {
                $isunique = $isunique->whereNull('biocollection_number');
              }
              if ($isunique->count() > 0 or $isalsounique >0 ) {
                $validator->errors()->add('biocollection_id', Lang::get('messages.voucher_duplicate_identifier2'));
              }
            }
        });

        return $validator;
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $voucher = Voucher::withoutGlobalScopes()->with("current_location")->findOrFail($id);
        $identification = $voucher->identification;
        $collectors = $voucher->collectors;
        return view('vouchers.show', compact('voucher', 'identification', 'collectors'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()) {
            return view('common.unauthorized');
        }
        $voucher = Voucher::findOrFail($id);
        $biocollections = Biocollection::all();
        $persons = Person::all();
        $datasets = Auth::user()->datasets;
        $bibreferences = BibReference::select('id',DB::raw('odb_bibkey(bibtex) as bibkey'))->orderByRaw('odb_bibkey(bibtex)')->get();
        return view('vouchers.create', compact('voucher', 'persons', 'datasets', 'biocollections','bibreferences'));
    }





    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataset= null;
        if ($request->dataset_id) {
          $dataset = Dataset::findOrFail($request->dataset_id);
        }
        $this->authorize('create', [Voucher::class, $dataset]);
        $validator = $this->customValidate($request);
        if ($validator->errors()->count()) {
            if ($request->from_the_api) {
              return implode(" | ",$validator->errors()->all());
            }
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $voucher = new Voucher($request->only(['individual_id','biocollection_id','biocollection_type','biocollection_number','number', 'notes', 'dataset_id']));

        //date and collector only if provided as they may be that of the individual
        if (isset($request->date_year)) {
          $voucher->setDate($request->date_month, $request->date_day, $request->date_year);
        } elseif (isset($request->date)) {
          $voucher->setDate($request->date);
        }
        $voucher->save();

        // common:
        if ($request->collector) {
            $idx = 0;
            foreach ($request->collector as $collector) {
                if ($idx==0) {
                  $values = [
                    'person_id' => $collector,
                    'main' => 1,
                  ];
                } else {
                  $values = [
                    'person_id' => $collector
                  ];
                }
                $voucher->collectors()->create($values);
                $idx = $idx+1;
            }
        }

        if (isset($request->bibreferences)) {
          foreach($request->bibreferences as $bib_reference_id) {
              $references[] = [
                'bib_reference_id' => $bib_reference_id,
              ];
          }
          if (count($references)>0) {
            $voucher->bibreferences()->attach($references);
          }
        }

        //for summary count updates
        /*
        $individual = Individual::findOrFail($request->individual_id);
        $newvalues =  [
                 "taxon_id" => $individual->identification->taxon_id,
                 "location_id" => $individual->locations->last()->id,
                 "project_id" => $request->project_id
        ];

         //UPDATE SUMMARY COUNTS
        $oldvalues =  [
              "taxon_id" => null,
              "location_id" => null,
              "project_id" => null,
        ];
        $target = 'vouchers';
        $datasets = null;

        Summary::updateSummaryCounts($newvalues,$oldvalues,$target,$datasets,0);
        */
        /* END SUMMARY UPDATE */

        if ($request->from_the_api) {
           return $voucher;
        }
        return redirect('vouchers/'.$voucher->id)->withStatus(Lang::get('messages.stored'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $voucher = Voucher::findOrFail($id);
        $dataset= null;
        if ($request->dataset_id) {
          $dataset = Dataset::findOrFail($request->dataset_id);
        }
        $this->authorize('update',[$voucher,$dataset]);
        $validator = $this->customValidate($request, $voucher);
        if ($validator->fails()) {
            if ($request->from_the_api) {
              return implode(" | ",$validator->errors()->all());
            }
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        //for summary counts
        /*
        $oldindividual = $voucher->individual;
        $oldvalues = [
            'location_id' => $oldindividual->locations->last()->id,
            'taxon_id' => $oldindividual->identification->taxon_id,
            'dataset_id' => $voucher->dataset_id
        ];

        //for summary counts
        $individual = Individual::findOrFail($request->individual_id);
        $newvalues = [
            'location_id' => $individual->locations->last()->id,
            'taxon_id' => $individual->identification->taxon_id,
            'dataset_id' => $request->dataset_id
        ];
        */
        $voucher->update($request->only(['individual_id','biocollection_id','biocollection_type','biocollection_number','number', 'notes', 'dataset_id']));

        if ($request->date_year) {
          $voucher->setDate($request->date_month, $request->date_day, $request->date_year);
          $voucher->save();
        } elseif ($request->date) {
          $voucher->setDate($request->date);
          $voucher->save();
        } else {
          $voucher->date = null;
          $voucher->save();
        }

        // COLLECTOR UPDATE:
        if ($request->collector) {
          //did collectors changed?
          // "sync" collectors. See app/Project.php / setusers()
          $current = $voucher->collectors->pluck('person_id');
          $detach = $current->diff($request->collector)->all();
          $attach = collect($request->collector)->diff($current)->all();
          if (count($detach) or count($attach)) {
              //delete old collectors
              if ($current->count()) {
                $voucher->collectors()->delete();
              }
              //save collectors and identify main collector
              $first = true;
              foreach ($request->collector as $collector) {
                  $thecollector = new Collector(['person_id' => $collector]);
                  if ($first) {
                      $thecollector->main = 1;
                  }
                  $voucher->collectors()->save($thecollector);
                  $first = false;
              }
          }
          //log changes in collectors if any
          ActivityFunctions::logCustomPivotChanges($voucher,$current->all(),$request->collector,'voucher','collector updated',$pivotkey='person');
        } else {
          //no more collectors? delete if not updating from the api
          if ($voucher->collectors->count() and !$request->from_the_api) {
              $current = $voucher->collectors->pluck('person_id');
              $voucher->collectors()->delete();
              ActivityFunctions::logCustomPivotChanges($voucher,$current->all(),[],'voucher','collector updated',$pivotkey='person');
          }
        }


        $currentbib = $voucher->bibreferences->pluck('bib_reference_id');
        if (isset($request->bibreferences)) {
            $detachbib = $currentbib->diff($request->bibreferences)->all();
            $attachbib = collect($request->bibreferences)->diff($currentbib)->all();
            if (count($detachbib) or count($attachbib)) {
                //delete old bibs
                $voucher->bibreferences()->detach();
                $bibrefs = [];
                foreach ($request->bibreferences as $bib_reference_id) {
                    $bibrefs[] = array(
                      'bib_reference_id' => $bib_reference_id,
                    );
                }
                $voucher->bibreferences()->attach($bibrefs);
            }
            //log changes
            if (count($detachbib)) {
              ActivityFunctions::logCustomPivotChanges($voucher,$currentbib->all(),$request->bibreferences,'voucher','Bibreferences updated',$pivotkey='bibreference');
            }
        } elseif ($currentbib->count() and !$request->from_the_api) {
          $voucher->bibreferences()->detach();
          ActivityFunctions::logCustomPivotChanges($voucher,$currentbib->all(),null,'voucher','Bibreferences updated',$pivotkey='bibreference');
        }



        /* UPDATE SUMMARY COUNTS */
        /*
        $target = 'vouchers';
        $datasets = array_unique($voucher->measurements()->withoutGlobalScopes()->pluck('dataset_id')->toArray());
        $measurements_count = $voucher->measurements()->withoutGlobalScopes()->count();
        Summary::updateSummaryCounts($newvalues,$oldvalues,$target,$datasets,$measurements_count);
        */
        /* END SUMMARY UPDATE */
        if ($request->from_the_api) {
           return $voucher;
        }

        return redirect('vouchers/'.$voucher->id)->withStatus(Lang::get('messages.saved'));
    }

     /**
      * Remove the specified resource from storage.
      *
      * @param int $id
      *
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         $voucher = Voucher::findOrFail($id);

         /* will authorize only if no related models are found */
         $this->authorize('delete', $voucher);

         try {
             /*remove in related if found */
             $voucher->delete();
         } catch (\Illuminate\Database\QueryException $e) {
             return redirect()->back()
                 ->withErrors([Lang::get('messages.fk_error')])->withInput();
         }
         return redirect('vouchers')->withStatus(Lang::get('messages.removed'));
     }



    public function activity($id, ActivityDataTable $dataTable)
    {
      $object = Voucher::findOrFail($id);
      return $dataTable->with('voucher', $id)->render('common.activity',compact('object'));
    }


        public function importJob(Request $request)
        {
          $this->authorize('create', Voucher::class);
          $this->authorize('create', UserJob::class);
          if (!$request->hasFile('data_file')) {
              $message = Lang::get('messages.invalid_file_missing');
          } else {
            /*
                Validate attribute file
                Validate file extension and maintain original if valid or else
                Store may save a csv as a txt, and then the Reader will fail
            */
            $valid_ext = array("csv","ods",'xlsx');
            $ext = mb_strtolower($request->file('data_file')->getClientOriginalExtension());
            if (!in_array($ext,$valid_ext)) {
              $message = Lang::get('messages.invalid_file_extension');
            } else {
              try {
                $data = SimpleExcelReader::create($request->file('data_file'),$ext)->getRows()->toArray();
              } catch (\Exception $e) {
                $data = [];
                $message = json_encode($e);
              }
              if (count($data)>0) {
                UserJob::dispatch(ImportVouchers::class,[
                  'data' => ['data' => $data],
                ]);
                $message = Lang::get('messages.dispatched',['url' => Route('userjobs.list') ]);
              } else {
                $message = 'Something wrong with file'.$message;
              }
            }
          }
          return redirect('import/vouchers')->withStatus($message);
        }

        public function mapOccurrences(Request $request)
        {
            $ids = explode(',',$request->idstomap);
            $ids = array_filter($ids);
            if (count($ids)>0) {
              $data  = Location::generateFeatureCollectionOccurrences(null,$ids);
              $location_json = $data['features'];
              $location_extent = $data['extent'];
              $location_centroid = $data['centroid'];

              $mapfor = 'Selected Vouchers';
              return view('common.occurrences-map',compact('mapfor','location_json','location_extent','location_centroid'));
            }
            return redirect()
            ->back()
            ->withStatus(Lang::get('messages.nothing-to-map'));
        }


        /*
          * Batch delete locations
        */
        public function batchDelete(Request $request)
        {
            $data = $request->all();
            $data['model'] = 'Voucher';
            UserJob::dispatch(DeleteMany::class,
            [
              'data' => ['data' => $data,]
            ]);
            return redirect('vouchers')->withStatus(Lang::get('messages.dispatched',['url' => Route('userjobs.list') ]));
        }


}
