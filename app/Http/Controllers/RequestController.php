<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ODBRequest;
use App\Models\UserJob;
use App\Models\User;
use App\Models\Vouchers;
use App\Models\Biocollection;
use App\Jobs\UserRequestIndividuals;
use App\Jobs\UserRequestVouchers;
use App\Jobs\UserRequestAnnotate;
use App\DataTables\VouchersDataTable;
use App\DataTables\RequestsDataTable;
use App\DataTables\IndividualsDataTable;

use Lang;
use Auth;
use Validator;

class RequestController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RequestsDatatable $dataTable)
    {
      return $dataTable->render('requests.index', []);
    }
    public function indexBioCollection($id,RequestsDatatable $dataTable)
    {
      $biocollection = BioCollection::findOrFail($id);
      return $dataTable->with(['biocollection_id'=> $biocollection->id])->render('requests.index', compact('biocollection'));
    }
    public function indexUser($id,RequestsDatatable $dataTable)
    {
      $user = User::findOrFail($id);
      return $dataTable->with(['user_id'=> $user->id])->render('requests.index', compact('user'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function create()
    {
        if (!Auth::user()) {
            return view('common.unauthorized');
        }
        $biocollections = Biocollection::orderBy('acronym')->cursor();
        return view('requests.create',compact('biocollections'));
    }

    protected function edit($id)
    {
        $odbrequest = ODBRequest::findOrFail($id);
        $this->authorize('update', $odbrequest);
        $biocollections = Biocollection::orderBy('acronym')->cursor();
        return view('requests.create',compact('odbrequest'));
    }

    protected function checkValid(Request $request, $id = null)
    {
        $this->validate($request, [
            'message' => 'required|string|max:500',
            'email' => 'required_if:user_id,null|nullable|email|max:191',
            'user_id' => 'required_if:email,null|nullable',
            'biocollection_id' => 'nullable',
            'institution' => 'nullable|string|max:191',
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */

    /*
    public function store(Request $request)
    {
        $this->authorize('create', ODBRequest::class);
        $this->checkValid($request);
        $odbrequest = ODBRequest::create([
          'message' => $request->message,
          'email' => $request->email,
          'institution' => $request->institution,
          'user_id' => $request->user_id,
          'biocollection_id' => $request->user_id,
        ]);
        return redirect('requests')->withStatus(Lang::get('messages.stored'));
    }

    public function update(Request $request, $id)
    {
        $odbrequest = ODBRequest::findOrFail($id);
        $this->authorize('update', $odbrequest);
        $this->checkValid($request);
        $data = $request->only(['message', 'email', 'user_id','institution','biocollection_id']);
        $odbrequest->update($data);
        return redirect('requests/'.$odbrequest->id)->withStatus(Lang::get('messages.saved'));
    }
    */


        /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id, VouchersDataTable $dataTable, IndividualsDataTable $individualDataTable)
    {
        $odbrequest = ODBRequest::findOrFail($id);
        if ($odbrequest->vouchers->count()) {
          $biocollection_id = array_unique($odbrequest->vouchers->pluck('biocollection_id')->toArray());
        } else {
          $biocollection_id = array_unique($odbrequest->individuals->pluck('pivot.biocollection_id')->toArray());
          $dataTable = $individualDataTable;
        }
        $biocollection = BioCollection::findOrFail($biocollection_id[0]);
        $nextBiocollectionNumber = $biocollection->vouchers()->orderBy('biocollection_number','desc')->first()->biocollection_number;
        $status_options = ODBRequest::STATUS;
        $status_options_individuals = ODBRequest::STATUS_INDIVIDUALS;
        return $dataTable->with([
               'request_id' => $id
           ])->render('requests.show', compact('odbrequest','biocollection','status_options','status_options_individuals','nextBiocollectionNumber'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $odbrequest = ODBRequest::findOrFail($id);
        $this->authorize('delete', $odbrequest);
        try {
            $odbrequest->individuals()->detach();
            $odbrequest->vouchers()->detach();
            $odbrequest->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->back()
                ->withErrors([Lang::get('messages.fk_error')]);
        }
        return redirect('biocollections')->withStatus(Lang::get('messages.removed'));
    }

    public function annotate($id,Request $request)
    {
        $this->authorize('create', UserJob::class);
        UserJob::dispatch(UserRequestAnnotate::class,
        [
          'data' => ['data' => $request->all(),
          'header' => ['not_external' => 1, 'request_id' => $id]
          ]
        ]);
        return redirect()->back()->withStatus(Lang::get('messages.request_dispatched',['url'=>url('userjobs')]));
    }

    public function individualsRequest(Request $request)
    {
        $this->authorize('create', UserJob::class);
        UserJob::dispatch(UserRequestIndividuals::class,
        [
          'data' => ['data' => $request->all(),
          'header' => ['not_external' => 1]
          ]
        ]);
        return redirect()->back()->withStatus(Lang::get('messages.request_dispatched',['url'=>url('userjobs')]));
    }

    public function userRequest(Request $request)
    {
      $voucher_ids = explode(',',$request->voucher_ids);
      if (count($voucher_ids)==0) {
        return redirect()->back()->withStatus(Lang::get('messages.select_rows'));
      }
      $this->checkValid($request);
      $this->authorize('create', UserJob::class);
      UserJob::dispatch(UserRequestVouchers::class,
      [
        'data' => ['data' => $request->all(),
        'header' => ['not_external' => 1]
        ]
      ]);
      return redirect()->back()->withStatus(Lang::get('messages.request_dispatched',['url'=>url('userjobs')]));

      //return redirect()->back()->withStatus(Lang::get('messages.stored'));
    }

}
