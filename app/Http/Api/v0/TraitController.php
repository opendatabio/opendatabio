<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use App\Jobs\ImportTraits;
use Illuminate\Http\Request;
use App\Models\ODBTrait;
use App\Models\UserJob;
use App\Jobs\ExportData;
use App\Queries\TraitSearchQuery;

use DB;
use Lang;
use Response;
use Log;
//use Illuminate\Support\Facades\App;

class TraitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //TODO: get should include in name and descriptions the arrays containing values for each translation
    //same for categories

    public function index(Request $request)
    {

        if ($request->save_job) {
          $this->authorize('create', UserJob::class);          
          $data = $request->all();
          unset($data['save_job']);
          $params = $data;
          $data['fields'] =  isset($data['fields']) ? $data['fields'] : 'simple';
          unset($params['fields']);
          $data['params'] =$params;
          $data['object_type'] ="trait";          
          $data['language'] = isset($data['language']) ? $data['language'] : 1;
          $data['filetype'] = $request->file_type ? $request->file_type : "csv";
          $jobid = UserJob::dispatch(ExportData::class, ['data' => ['data' => $data]]);
          $respond = UserJob::select(['id','status'])->where('id',$jobid)->get();
          return $this->wrap_response($respond);
        }

        $prepared_query = TraitSearchQuery::prepQuery($request);
        $fields = $prepared_query['fields'];
        $lang = $prepared_query['lang'];
        $odb_traits = $prepared_query['query'];
        $odb_traits = $odb_traits->cursor();
        if ($fields=="id") {
          $final = $odb_traits->pluck('id')->toArray();
        } elseif($fields!="raw") {
          $final = TraitSearchQuery::prepCategories($odb_traits,$fields,$lang);
        }
        return $this->wrap_response($final);
    }



    public function store(Request $request)
    {
        $this->authorize('create', ODBTrait::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(ImportTraits::class, ['data' => $request->post()]);

        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }
}
