<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use Illuminate\Support\Arr;
use App\Models\BibReference;
use Illuminate\Http\Request;
use App\Models\UserJob;
use App\Jobs\ExportData;
use App\Queries\BibReferenceSearchQuery;
use Response;
use DB;

class BibReferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if ($request->save_job) {
          $this->authorize('create', UserJob::class);          
          $data = $request->all();
          unset($data['save_job']);
          $params = $data;
          $data['fields'] =  isset($data['fields']) ? $data['fields'] : 'simple';
          unset($params['fields']);
          $data['params'] =$params;
          $data['object_type'] ="Bibreference";          
          $data['filetype'] = $request->file_type ? $request->file_type : "csv";
          //Log::info("FIRST TIME");
          //Log::info($data);
          $jobid = UserJob::dispatch(ExportData::class, ['data' => ['data' => $data]]);
          $respond = UserJob::select(['id','status'])->where('id',$jobid)->get();
          return $this->wrap_response($respond);
        }
        $prepared_query = BibReferenceSearchQuery::prepQuery($request);
        $fields = $prepared_query['fields'];
        $bibrefences = $prepared_query['query'];
        $bibrefences = $bibrefences->cursor();
        if ($fields=="id") {
          $bibrefences = $bibrefences->pluck('id')->toArray();
        } elseif($fields!="raw") {
          $bibrefences = $this->setFields($bibrefences, $fields, null);
        }
        return $this->wrap_response($bibrefences);
    }

}
