<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\ODBFunctions;
use App\Models\Language;

use DB;
use Lang;
use Response;
use Log;
//use Illuminate\Support\Facades\App;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tags = Tag::query();   
        $fields = ($request->fields ? $request->fields : 'id,name,description');
        $lang = 1;
        if ($request->language) {
           $lang = ODBFunctions::validRegistry(Language::select('id'),$request->language, ['id', 'code','name'])->id;           
        } 
        if ($request->limit && $request->offset) {
            $tags = $tags->offset($request->offset)->limit($request->limit);
        } else {
          if ($request->limit) {
            $tags = $tags->limit($request->limit);
          }
        }
        $tags = $tags->get()->map(function($obj) use($lang,$fields) {
            $result = [];
            $field_arr = explode(",",$fields);
            foreach ($field_arr as $field) {
                $result[$field] = isset($obj[$field]) ? $obj[$field] : null;
                if ($field=="name") {
                    $result[$field] = $obj->translate(0,$lang); 
                }
                if ($field=="description") {
                    $result[$field] = $obj->translate(1,$lang); 
                }                
            }
            return($result);
        });
        //$tags = $this->setFields($tags, $fields, ['id','name']);
        return $this->wrap_response($tags);
    }

}
