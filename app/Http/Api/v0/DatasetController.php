<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use App\Models\Dataset;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Spatie\SimpleExcel\SimpleExcelReader;
use App\Models\ODBFunctions;

use Response;
use Storage;
use Auth;
use Log;

class DatasetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $dts_ids =[];
      if ($request->name) {
        $ids = ODBFunctions::asIdList($request->name, Dataset::select('id'), 'name');
        $dts_ids = implode(",",$ids);
      } elseif ($request->id) {
        $dts_ids = $request->id;
      }

       //with(['users', 'tags.translations'])
       if ($request->list_versions and $dts_ids) {
          if (!Auth::user()) {
            return Response::json(['message' => 'Unauthenticated', 401]);
          }
          $dataset_id = explode(",",$dts_ids);
          $data = Dataset::whereIn('id',$dataset_id)->whereHas("versions");
          if ($data->count()) {
            $version = $data->get()->map(function($d) use($request) {
              $user_datasets = Auth::user()->editableDatasets();
              $user_datasets = $user_datasets!==null ? $user_datasets->pluck('datasets.id')->toArray() : [];
              $user_access = (in_array($d->id,$user_datasets) or $d->privacy >= Dataset::PRIVACY_PUBLIC) ? "You have access" : "You do not have access";
              $dataset_name = $d->name;
              $versions = $d->versions()->get();
              $versions = $versions->map(function($v) use($user_access,$d,$request) {
                 $files = $v->metadata ? json_decode($v->metadata,true)['files'] : [];
                 $results = [];
                 if (count($files)>0) {                                      
                    foreach($files as $file) {
                        $item = [
                        'dataset_id' => $d->id,
                        'dataset' => $d->name,
                        'version' => $v->version,
                        'filename' => $file,
                        'license' => $v->license ? $v->license." ".$v->license_version : null,
                        'access' => $v->data_access==0 ? "OpenAccess" : $user_access,                                         
                        //'get_params' => "params=list(file_name='".$file."',id=".$d->id.")",
                      ];
                      if ($request->include_url)
                      {
                        $item['url'] = Storage::disk("public")->url("datasets/".$file);
                      }
                      $results[]= $item;
                    }                    
                 }
                 return $results ? $results : null;
              });   
              return $versions->toArray();              
            })->sortByDesc('version');
            $results = $version->toArray();
            $final = [];
            array_walk($results, function ($item, $key) use(&$final) {
                $item  = $item[0];
                if ($item) {
                    $final[] = $item;                
                }
            });
            $final = array_filter($final);      
            return $this->wrap_response($final);     
        }
        return $this->wrap_response(null); 
       }

       if ($request->file_name and $dts_ids) {
          if (!Auth::user()) {
            return Response::json(['message' => 'Unauthenticated', 401]);
          }          
          $user_datasets = Auth::user()->editableDatasets();
          $user_datasets = $user_datasets!==null ? $user_datasets->pluck('datasets.id')->toArray() : [];
          $dataset = Dataset::find($dts_ids[0]);
          $user_access = (in_array($dts_ids[0],$user_datasets) or $dataset->privacy >= Dataset::PRIVACY_PUBLIC);
          if (!$user_access) {
            return Response::json(['message' => 'Unauthorized', 401]);
          }
          $path = Storage::disk("public")->path("datasets/".$request->file_name);          
          return $this->wrap_response(['path'=> $path,'url'=> Storage::disk("public")->url("datasets/".$request->file_name)]);          
      }


        $datasets =   Dataset::withCount(['measurements']);

        if ($dts_ids) {
            $datasets->whereIn('id', explode(',', $dts_ids));
        }

        if ($request->has_version) 
        {
          $datasets->whereHas("versions");
        }
        


        $fields = ($request->fields ? $request->fields : 'simple');
        $possible_fields = config('api-fields.datasets');
        $field_sets = array_keys($possible_fields);
        if (in_array($fields,$field_sets)) {
            $fields = implode(",",$possible_fields[$fields]);
        }
        $datasets = $datasets->cursor();
        if ($fields=="id") {
          $datasets = $datasets->pluck('id')->toArray();
        } elseif($fields!="raw") {
          $datasets = $this->setFields($datasets, $fields, null);
        }

        return $this->wrap_response($datasets);
    }

}
