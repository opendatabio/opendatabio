<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Individual;
use App\Models\UserJob;
use App\Jobs\ImportIndividuals;
use App\Jobs\UpdateIndividuals;
use App\Jobs\ExportData;
use App\Queries\IndividualSearchQuery;
use Response;
use DB;
use Log;
//use App\Models\Jobs\ImportIndividuals;

class IndividualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->save_job) {
          $this->authorize('create', UserJob::class);          
          $data = $request->all();
          unset($data['save_job']);
          $params = $data;
          $data['fields'] =  isset($data['fields']) ? $data['fields'] : 'simple';
          unset($params['fields']);
          $data['params'] =$params;
          $data['object_type'] ="Individual";          
          $data['filetype'] = $request->file_type ? $request->file_type : "csv";
          //Log::info("FIRST TIME");
          //Log::info($data);
          $jobid = UserJob::dispatch(ExportData::class, ['data' => ['data' => $data]]);
          $respond = UserJob::select(['id','status'])->where('id',$jobid)->get();
          return $this->wrap_response($respond);
        }
        $prepared_query = IndividualSearchQuery::prepQuery($request);                
        $fields = $prepared_query['fields'];
        $individuals = $prepared_query['query'];
        if ($fields=="id") {
          $individuals = $individuals->pluck('id')->toArray();
        } elseif($fields!="raw") {
          $individuals=  $individuals->cursor();
          $individuals = $this->setFields($individuals, $fields, null);
        } else {
          $individuals=  $individuals->cursor();
        }
        return $this->wrap_response($individuals);
      }


    public function store(Request $request)
    {
        $this->authorize('create', Individual::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(ImportIndividuals::class, ['data' => $request->post()]);

        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }

    public function update(Request $request)
    {
        $this->authorize('create', Individual::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(UpdateIndividuals::class, ['data' => $request->all()]);
        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }

}
