<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use App\Models\Project;
use App\Models\UserJob;
use App\Jobs\ExportData;
use Illuminate\Http\Request;
use Response;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->save_job) {
          $this->authorize('create', UserJob::class);          
          $data = $request->all();
          unset($data['save_job']);
          $params = $data;
          $data['fields'] =  isset($data['fields']) ? $data['fields'] : 'simple';
          unset($params['fields']);
          $data['params'] =$params;
          $data['object_type'] ="Project";          
          $data['filetype'] = $request->file_type ? $request->file_type : "csv";
          $jobid = UserJob::dispatch(ExportData::class, ['data' => ['data' => $data]]);
          $respond = UserJob::select(['id','status'])->where('id',$jobid)->get();
          return $this->wrap_response($respond);
        }

        $projects = Project::withCount(['datasets','measurements']);
        if ($request->id) {
            $projects->whereIn('id', explode(',', $request->id));
        }
        $fields = ($request->fields ? $request->fields : 'simple');
        $simple = ['id', 'fullname', 'notes','description','contactEmail','datasets_count','individuals_count','vouchers_count','locations_count','measurements_count','taxons_count','media_count','species_count'];
        //include here to be able to add mutators and categories
        if ('all' == $fields) {
            $keys = array_keys($persons->first()->toArray());
            $fields = array_merge($simple,$keys);
            $fields =  implode(',',$fields);
        }

        $projects = $projects->cursor();
        if ($fields=="id") {
          $projects = $projects->pluck('id')->toArray();
        } elseif($fields!="raw") {
          $projects = $this->setFields($projects, $fields, $simple);
        }

        return $this->wrap_response($projects);
    }

}
