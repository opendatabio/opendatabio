<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use App\Jobs\ImportTaxons;
use Illuminate\Http\Request;
use App\Models\Taxon;
use App\Models\UserJob;
use App\Jobs\ExportData;
use App\Queries\TaxonSearchQuery;

use Response;
use Log;
class TaxonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->save_job) {
          $this->authorize('create', UserJob::class);          
          $data = $request->all();
          unset($data['save_job']);
          $params = $data;
          $data['fields'] =  isset($data['fields']) ? $data['fields'] : 'simple';
          unset($params['fields']);
          $data['params'] =$params;
          $data['object_type'] ="Taxon";          
          $data['filetype'] = $request->file_type ? $request->file_type : "csv";
          $jobid = UserJob::dispatch(ExportData::class, ['data' => ['data' => $data]]);
          $respond = UserJob::select(['id','status'])->where('id',$jobid)->get();
          return $this->wrap_response($respond);
        }

        $prepared_query = TaxonSearchQuery::prepQuery($request);                
        $fields = $prepared_query['fields'];
        $taxons = $prepared_query['query'];
        $taxons = $taxons->cursor();
        if ($fields=="id") {
          $taxons = $taxons->pluck('id')->toArray();
        } elseif($fields!="raw") {
          $taxons = $this->setFields($taxons, $fields, null);
        }
        return $this->wrap_response($taxons);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Taxon::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(ImportTaxons::class, ['data' => $request->post()]);

        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }
}
