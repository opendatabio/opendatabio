<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use Illuminate\Http\Request;
use Spatie\SimpleExcel\SimpleExcelReader;
use Response;
use Auth;
use Storage;
use Log;

class UserJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!Auth::user()) {
            return Response::json(['message' => 'Unauthenticated', 401]);
        }

        if ($request->get_file and $request->id) {
            $files = Storage::disk('public')->files("downloads");
            if ($request->file_name) {
                $pattern = $request->file_name;
            } else {
                $pattern = "job-".$request->id;
            }
            $has_file = preg_grep("/".$pattern."/i",$files);
            if (count($has_file)==1) {
                $has_file = array_values($has_file);
                ##return url for local download                
                return $this->wrap_response(['url'=> Storage::disk("public")->url($has_file[0])]);                      
            } elseif (count($has_file)>1) {
                $job_id = $request->id;
                $data = collect($has_file)->map(function($q) use($job_id) {
                    $fn = explode("/",$q)[1];
                    return [
                        'filename' => $fn,
                        'problem' => "more than 1 file in the job",
                        'solution' => "Use params=list(file_name='".$fn."',get_file=1,id=".$job_id.") to get this file",
                    ];
                })->values();    
                return $this->wrap_response($data);     
            }
        }


        $jobs = Auth::user()->userjobs();
        if ($request->status) {
            $jobs->where('status', '=', $request->status);
        }
        if ($request->id) {
            $jobs->whereIn('id', explode(',', $request->id));            
        }
        $jobs = $jobs->get();
        $fields = ($request->fields ? $request->fields : 'simple');
        $jobs = $this->setFields($jobs, $fields, ['id', 'dispatcher', 'status', 'percentage', 'created_at', 'updated_at']);

        return $this->wrap_response($jobs);
    }
}
