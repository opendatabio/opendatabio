<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use Illuminate\Http\Request;
use App\Models\Location;
use App\Models\UserJob;
use App\Jobs\ExportData;
use App\Queries\LocationSearchQuery;
use App\Jobs\ImportLocations;
use App\Jobs\UpdateLocations;
use App\Jobs\ValidateLocations;
use Log;
use Response;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
      /* if request is just to get possible field names */
      if ($request->show_field_names) {
          $fields = config('api-fields.locations');
          $fields = $fields['all'];
          $first = Location::withGeom()->offset(10)->limit(1)->first()->toArray();
          $fields  = array_unique(array_merge($fields,array_keys($first)));
          return $this->wrap_response($fields);
      }

      if ($request->save_job) {
        $this->authorize('create', UserJob::class);          
        $data = $request->all();
        unset($data['save_job']);
        $params = $data;
        $data['fields'] =  isset($data['fields']) ? $data['fields'] : 'simple';
        unset($params['fields']);
        $data['params'] =$params;
        $data['object_type'] ="Location";          
        $data['filetype'] = $request->file_type ? $request->file_type : "csv";
        $jobid = UserJob::dispatch(ExportData::class, ['data' => ['data' => $data]]);
        $respond = UserJob::select(['id','status'])->where('id',$jobid)->get();
        return $this->wrap_response($respond);
      }

      $prepared_query = LocationSearchQuery::prepQuery($request);
      $fields = $prepared_query['fields'];
      $locations = $prepared_query['query'];

      if ($fields=="id") {
        $locations = $locations->pluck('id')->toArray();
      } elseif($fields!="raw") {
        $locations=  $locations->cursor();        
        $locations = $this->setFields($locations, $fields, null);
      } else {
        $locations=  $locations->cursor();        
      }
      return $this->wrap_response($locations);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Location::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(ImportLocations::class, ['data' => $request->post()]);

        return Response::json(['message' => 'OK', 'userjob' => $jobid]);

    }

    public function validateLocation(Request $request)
    {
        $this->authorize('create', Location::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(ValidateLocations::class, ['data' => $request->post()]);

        return Response::json(['message' => 'OK', 'userjob' => $jobid]);

    }

    public function update(Request $request)
    {
        $this->authorize('create', Location::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(UpdateLocations::class, ['data' => $request->post()]);
        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }
}
