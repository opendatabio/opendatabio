<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use App\Models\Biocollection;
use App\Models\ODBFunctions;
use App\Models\UserJob;
use App\Jobs\ExportData;
use App\Jobs\ImportBiocollections;
use Illuminate\Http\Request;
use Response;

class BiocollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        
        
        if ($request->save_job) {
            $this->authorize('create', UserJob::class);          
            $data = $request->all();
            unset($data['save_job']);
            $params = $data;
            $data['fields'] =  isset($data['fields']) ? $data['fields'] : 'simple';
            unset($params['fields']);
            $data['params'] =$params;
            $data['object_type'] ="Biocollection";          
            $data['filetype'] = $request->file_type ? $request->file_type : "csv";
            //Log::info("FIRST TIME");
            //Log::info($data);
            $jobid = UserJob::dispatch(ExportData::class, ['data' => ['data' => $data]]);
            $respond = UserJob::select(['id','status'])->where('id',$jobid)->get();
            return $this->wrap_response($respond);
        }
        
        $biocollections = Biocollection::select('*');

        if ($request->id) {
            $biocollections->whereIn('id', explode(',', $request->id));
        }
        if ($request->acronym) {
            $biocollections->whereIn('acronym', explode(',', $request->acronym));
        }


        $fields = ($request->fields ? $request->fields : 'simple');
        $possible_fields = config('api-fields.biocollections');
        $field_sets = array_keys($possible_fields);
        if (in_array($fields,$field_sets)) {
            $fields = implode(",",$possible_fields[$fields]);
        }


        $biocollections = $biocollections->cursor();
        if ($fields=="id") {
          $biocollections = $biocollections->pluck('id')->toArray();
        } elseif($fields!="raw") {
          $biocollections = $this->setFields($biocollections, $fields, null);
        }
        return $this->wrap_response($biocollections);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Biocollection::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(ImportBiocollections::class, ['data' => $request->post()]);
        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }
}
