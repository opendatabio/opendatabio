<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\UserJob;
use App\Models\Voucher;
use App\Jobs\ExportData;
use App\Models\ODBFunctions;
use App\Jobs\ImportVouchers;
use App\Jobs\UpdateVouchers;
use App\Queries\VoucherSearchQuery;

use Response;
use Log;
use DB;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      if ($request->save_job) {
        $this->authorize('create', UserJob::class);          
        $data = $request->all();
        unset($data['save_job']);
        $params = $data;
        $data['fields'] =  isset($data['fields']) ? $data['fields'] : 'simple';
        unset($params['fields']);
        $data['params'] =$params;
        $data['object_type'] ="Voucher";          
        $data['filetype'] = $request->file_type ? $request->file_type : "csv";
        $jobid = UserJob::dispatch(ExportData::class, ['data' => ['data' => $data]]);
        $respond = UserJob::select(['id','status'])->where('id',$jobid)->get();
        return $this->wrap_response($respond);
      }

      $prepared_query = VoucherSearchQuery::prepQuery($request);                
      $fields = $prepared_query['fields'];
      $vouchers = $prepared_query['query'];
      $vouchers = $vouchers->cursor();
      if ($fields=="id") {
          $vouchers = $vouchers->pluck('id')->toArray();
      } elseif($fields!="raw") {          
          $vouchers = $this->setFields($vouchers, $fields, null);
      }
      return $this->wrap_response($vouchers);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Voucher::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(ImportVouchers::class, ['data' => $request->post()]);

        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }


    public function update(Request $request)
    {
        $this->authorize('create', Voucher::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(UpdateVouchers::class, ['data' => $request->all()]);
        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }

}
