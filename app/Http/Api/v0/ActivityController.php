<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use App\Models\Individual;
use Activity;
use App\Models\ActivityFunctions;
use App\Models\ODBFunctions;
use App\Models\Language;
use App\Jobs\ImportActivity;
use App\Models\UserJob;
use App\Jobs\ExportData;
use Illuminate\Http\Request;
use App\Queries\ActivitySearchQuery;

use Response;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->save_job) {
            $this->authorize('create', UserJob::class);          
            $data = $request->all();
            unset($data['save_job']);
            $params = $data;
            $data['fields'] =  isset($data['fields']) ? $data['fields'] : 'simple';
            unset($params['fields']);
            $data['params'] =$params;
            $data['object_type'] ="activitie";          
            $data['filetype'] = $request->file_type ? $request->file_type : "csv";
            $jobid = UserJob::dispatch(ExportData::class, ['data' => ['data' => $data]]);
            $respond = UserJob::select(['id','status'])->where('id',$jobid)->get();
            return $this->wrap_response($respond);
        }

        $prepared_query = ActivitySearchQuery::prepQuery($request);
        $fields = $prepared_query['fields'];
        $lang = $prepared_query['lang'];
        $activities = $prepared_query['query'];
        $activities = $activities->cursor();
        if ($fields=="id") {
            $final = $activities->pluck('id')->toArray();
        } elseif($fields!="raw") {
            $fields = explode(',',$fields);
            $final = [];
            foreach($activities as $activity) {
                $res = ActivityFunctions::exportProperties($activity,$lang);                      
                $newres = [];
                foreach($res as $change) {
                foreach ($fields as $field) {
                    if (!in_array($field,["properties","created_at","updated_at"])) {                            
                    $change[$field] = isset($activity[$field]) ? $activity[$field] : null;
                    }                         
                }          
                $newres[] = $change;              
                }
                $final = array_merge($final,$newres);  
            } 
        }
        return $this->wrap_response($final);
    }

    public function store(Request $request)
    {
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(ImportActivity::class, ['data' => $request->post()]);
        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }
}
