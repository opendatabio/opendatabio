<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Measurement;
use App\Models\UserJob;
use App\Jobs\ExportData;
use App\Queries\MeasurementSearchQuery;
use App\Jobs\ImportMeasurements;
use App\Jobs\UpdateMeasurements;
use Response;

class MeasurementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->save_job) {
          $this->authorize('create', UserJob::class);          
          $data = $request->all();
          unset($data['save_job']);
          $params = $data;
          $data['fields'] =  isset($data['fields']) ? $data['fields'] : 'simple';
          unset($params['fields']);
          $data['params'] =$params;
          $data['object_type'] ="Measurement";          
          $data['filetype'] = $request->file_type ? $request->file_type : "csv";
          $jobid = UserJob::dispatch(ExportData::class, ['data' => ['data' => $data]]);
          $respond = UserJob::select(['id','status'])->where('id',$jobid)->get();
          return $this->wrap_response($respond);
        }
        $prepared_query = MeasurementSearchQuery::prepQuery($request);
        $fields = $prepared_query['fields'];
        $measurements = $prepared_query['query'];  
              
        $measurements = $measurements->cursor();
        if ($fields=="id") {
          $measurements = $measurements->pluck('measurements.id')->toArray();
        } elseif($fields!="raw") {
          $measurements = $this->setFields($measurements, $fields, null);
        }
        return $this->wrap_response($measurements);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Measurement::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(ImportMeasurements::class, ['data' => $request->post()]);

        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }

    public function update(Request $request)
    {
        $this->authorize('create', Measurement::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(UpdateMeasurements::class, ['data' => $request->post()]);
        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }
}
