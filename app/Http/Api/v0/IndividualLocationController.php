<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use App\Models\Individual;
use App\Models\IndividualLocation;
use App\Models\UserJob;
use App\Jobs\ExportData;
use App\Queries\IndividualLocationSearchQuery;
use App\Jobs\ImportIndividualLocations;
use App\Jobs\UpdateIndividualLocations;
use Illuminate\Http\Request;
use Response;
use DB;
use Log;


class IndividualLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
        if ($request->save_job) {
          $this->authorize('create', UserJob::class);          
          $data = $request->all();
          unset($data['save_job']);
          $params = $data;
          $data['fields'] =  isset($data['fields']) ? $data['fields'] : 'simple';
          unset($params['fields']);
          $data['params'] =$params;
          $data['object_type'] ="Individual-location";
          $data['filetype'] = $request->file_type ? $request->file_type : "csv";
          $jobid = UserJob::dispatch(ExportData::class, ['data' => ['data' => $data]]);
          $respond = UserJob::select(['id','status'])->where('id',$jobid)->get();
          return $this->wrap_response($respond);
        }
        $prepared_query = IndividualLocationSearchQuery::prepQuery($request);
        $fields = $prepared_query['fields'];
        $indlocations = $prepared_query['query'];
        $indlocations = $indlocations->cursor();
        if ($fields=="id") {
          $indlocations = $indlocations->pluck('id')->toArray();
        } elseif($fields!="raw") {
          $indlocations = $this->setFields($indlocations, $fields,null);
        } 
        return $this->wrap_response($indlocations);
    }


    public function store(Request $request)
    {
        $this->authorize('create', Individual::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(ImportIndividualLocations::class, ['data' => $request->post()]);
        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }

    public function update(Request $request)
    {
        $this->authorize('create', Individual::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(UpdateIndividualLocations::class, ['data' => $request->all()]);
        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }
}
