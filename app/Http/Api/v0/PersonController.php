<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */

namespace App\Http\Api\v0;

use App\Models\Person;
use App\Models\UserJob;
use App\Jobs\ExportData;
use App\Models\ODBFunctions;
use App\Jobs\ImportPersons;
use App\Jobs\UpdatePersons;
use Illuminate\Http\Request;
use App\Queries\PersonSearchQuery;

use Response;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->save_job) {
            $this->authorize('create', UserJob::class);          
            $data = $request->all();
            unset($data['save_job']);
            $params = $data;
            $data['fields'] =  isset($data['fields']) ? $data['fields'] : 'simple';
            unset($params['fields']);
            $data['params'] =$params;
            $data['object_type'] ="Person";          
            $data['filetype'] = $request->file_type ? $request->file_type : "csv";            
            $jobid = UserJob::dispatch(ExportData::class, ['data' => ['data' => $data]]);
            $respond = UserJob::select(['id','status'])->where('id',$jobid)->get();
            return $this->wrap_response($respond);
        }
          
        
        $prepared_query = PersonSearchQuery::prepQuery($request);                
        $fields = $prepared_query['fields'];
        $persons = $prepared_query['query'];
        $persons = $persons->cursor();
        if ($fields=="id") {
          $persons = $persons->pluck('id')->toArray();
        } elseif($fields!="raw") {
          $persons = $this->setFields($persons, $fields, null);
        }        
        return $this->wrap_response($persons);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Person::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(ImportPersons::class, ['data' => $request->post()]);

        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }

    public function update(Request $request)
    {
        $this->authorize('create', Person::class);
        $this->authorize('create', UserJob::class);
        $jobid = UserJob::dispatch(UpdatePersons::class, ['data' => $request->post()]);
        return Response::json(['message' => 'OK', 'userjob' => $jobid]);
    }
}
