<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\ODBTrait;
use App\Models\Form;
use App\Models\Dataset;
use App\Models\FormTask;

use Illuminate\Validation\Rule;
use Lang;
use Auth;

class FormTaskEdit extends Component
{
    public $form;
    public $form_task;
    public $users_list;
    public $form_id;
    public $measured_type;
    public $short_measured_type;

    public $datasets_list = [];    
    public $name;
    public $created_at;
  

    public function mount($form_id=null,$form_task_id=null)
    {
        $users_list = [];
        $users_list[] = [
            'id' => Auth::user()->id,
            'label' => Auth::user()->identifiableName()
        ];
        $this->users_list= $users_list;

        
        if ($form_task_id) {
            $this->form_task = FormTask::find($form_task_id);
            $this->form = $this->form_task->form;
            $this->form_id = $this->form->id;
            $this->name = $this->form_task->name;
            $this->users_list = $this->form_task->users->map(function($q){
                return [
                    'id' => $q->id,
                    'label' => $q->identifiableName()
                ];
            })->toArray();
            $this->created_at = $this->form_task->created_at;
            $this->measured_type = $this->form_task->form->measured_type;
            $metadata = json_decode($this->form_task->metadata,true);
            if (isset($metadata['datasets'])) {
                $this->datasets_list = $metadata['datasets'];
            } 
           
        } elseif ($form_id)
        {
            $this->form_id = $form_id;
            $this->form = $form_id ? Form::find($form_id) : null;        
            if ($this->form) { 
                $this->measured_type = $this->form->measured_type;       
                $metadata = json_decode($this->form->metadata,true);
                if (isset($metadata['datasets'])) {
                    $this->datasets_list = $metadata['datasets'];
                } 
            }
        }
        $this->short_measured_type = str_ireplace("App\Models\\","",$this->measured_type);
    }
    
    public function rules()
    {
        return [            
            'users_list' => 'required',
            'name' => 'required|string|min:10|max:100',
            'datasets_list' => 'required|array',
            'form_id' => 'required|integer|exists:forms,id',
        ];
    }

    public function submit()
    {
        //validation
        $this->validate();

        $task = [
            'name' => $this->name,
            'form_id' => $this->form_id,
            'progress_max' => $this->form_task ? $this->form_task->progress_max : 0,
            'progress' => $this->form_task ? $this->form_task->progress : 0,
            'status' => $this->form_task ? $this->form_task->status : "defined",
        ];
        $metadata = [ 
            'datasets' => $this->datasets_list,
        ];        
        if ($this->form_task) {          
          $agd = $this->form_task;
          $agd->update($task); 
          $agd->users()->detach();
        } else {
          $agd = FormTask::create($task);
          
        }
        $agd->metadata = $metadata;
        $agd->save();         

        $uids = collect($this->users_list)->pluck('id')->toArray();
        $agd->users()->attach($uids);
        return $this->redirectRoute('form_tasks.show',['form_task_id' => $agd->id]);

    }


    protected $listeners = [
        'mergeChildDataset',    
        'mergeChildUser',    
    ];
    
    public function mergeChildDataset($return_array)
    {
        $this->datasets_list = $return_array;
        $this->emit('clearBar');
    }

    public function mergeChildUser($return_array)
    {
        $this->users_list = $return_array;
        $this->emit('clearBar');
    }

    public function render()
    {
        return view('livewire.form-task-edit');
    }
}
