<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\ODBTrait;
use App\Models\Form;
use App\Models\FormTask;
use Lang;
use Auth;

class FormsShow extends Component
{
    public $form;
    public $notes;
    public $traits_list = [];    
    public $datasets_list = [];    

    
    public function mount($form_id) : void {
        
        $this->form = $form_id ? Form::find($form_id) : null;
        $form = $this->form;        
        $this->notes =  $form->notes;
        $this->name = $form->name;
        $this->form_user = $form->user;
        $metadata = json_decode($form->metadata,true);
        if (isset($metadata['traits'])) {
            $this->traits_list = $metadata['traits'];
        } 
        if (isset($metadata['datasets'])) {
            $this->datasets_list = $metadata['datasets'];
        } 
    }

    public function editForm()
    {
      
        return redirect()->route('forms.edit',[
            'form_id' => $this->form->id,
          ]);      
    }

    public function useForm()
    {
      
        return redirect()->route('forms.fill',[
            'form_id' => $this->form->id,
          ]);      
    }

    public function createTask()
    {
      
        return redirect()->route('form_tasks.create',[
            'form_id' => $this->form->id,
          ]);      
    }

    public function deleteTask($task_id)
    {
        $task = FormTask::find($task_id);
        if (Auth::user()->can('delete',$task)) {
            try {
                $task->delete();
            } catch (\Illuminate\Database\QueryException $e) {
                session()->flash('errors',"Could not delete task");
                return;
            }        
        }
        session()->flash('success',"Task deleted!");
        return;
    }

    public function processTask($task_id)
    {
        return redirect()->route('forms.fill.task',[
            'form_task_id' => $task_id,
          ]);     
    }


    public function render()
    {
        return view('livewire.forms-show');
    }
}
