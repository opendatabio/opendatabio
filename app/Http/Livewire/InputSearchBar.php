<?php

namespace App\Http\Livewire;

use Livewire\Component;

class InputSearchBar extends Component
{
    public $query;
    public $list_of_options;
    public $highlightIndex;
    public $field_name;
    public $return_field;
    public $return_id;
    public $search_model;
    public $search_field;
    public $emitup_function;
    public $nested_model;
    public $selected_models=[];
    public $multi_select;
    public $search_placeholder;
    public $batch_identify;
    public $trait_form_order;
    public $form_task_id;

    public function mount(
      $field_name,
      $return_field,
      $search_model,
      $search_field,
      $return_id,
      $query,
      $emitup_function,
      $nested_model,
      $multi_select=null,
      $selected_models=null,
      $search_placeholder=null,
      $batch_identify=null,
      $trait_form_order=null,
      $form_task_id = null,
      )
    {
      $this->multi_select = $multi_select;
      $this->selected_models = $selected_models ? $selected_models : [];
      $this->field_name = $field_name;
      $this->return_field = $return_field;
      $this->return_id = $return_id;
      $this->query = $query;
      $this->search_model = $search_model;
      $this->search_field = $search_field;
      $this->emitup_function = $emitup_function;
      $this->nested_model = $nested_model;      
      $this->search_placeholder = $search_placeholder;
      $this->batch_identify = $batch_identify;
      $this->trait_form_order = $trait_form_order;
      $this->form_task_id = $form_task_id;

    }

    public function resetBar()
    {
      $this->query = null;
      $this->list_of_options = [];
      $this->return_id = null;
      $this->return_label = null;
      $this->highlightIndex=0;
    }

    public function incrementHighlight()
    {
      if ($this->highlightIndex === count($this->list_of_options)-1 ) {
          $this->highlightIndex=0;
          return;
      }
      $this->highlightIndex++;
    }

    public function decrementHighLight()
    {
      if ($this->highlightIndex === 0 ) {
          $this->highlightIndex= count($this->list_of_options)-1;
          return;
      }
      $this->highlightIndex--;
    }
    public function changeIndex($id)
    {
      $this->highlightIndex = $id;
    }

    public function removeFilterModel($index)
    {
        $selected_models = $this->selected_models;
        unset($selected_models[$index]);
        $selected_models = array_values($selected_models);
        $this->selected_models = $selected_models;
        $this->emitUp($this->emitup_function, $this->selected_models);
    }


    public function selectListOption($currentIndex)
    {
      if ($currentIndex) {
        $option = $this->list_of_options[$currentIndex] ?? null;
      } else {
        $option = $this->list_of_options[$this->highlightIndex] ?? null;
      }
      if ($option) {
        if ($this->multi_select) {
          $this->query = $option[$this->return_field];
          $this->list_of_options = [];
          $this->return_id=$option['id'];
          $model =  app('App\Models\\'.$this->search_model)::find($option['id']);
          $tx = [
              'id' => $model->id,
              'label' => $model[$this->return_field],
          ];
          $this->selected_models[] = $tx;
          $this->highlightIndex=0;
          $this->emitUp($this->emitup_function, $this->selected_models);
          $this->resetBar();
        } else {
          $this->query = $option[$this->return_field];
          $this->list_of_options = [];
          $this->return_id=$option['id'];
          $this->highlightIndex=0;
          if ($this->trait_form_order!==null) {
            $this->emitUp($this->emitup_function, [
              'return_id' => $this->return_id,
              'order' => $this->trait_form_order]);
          } else {
            $this->emitUp($this->emitup_function, $this->return_id);
          }
          $this->resetBar();
        }
      } else {
        $this->resetBar();
      }
    }

    public function updatedQuery()
    {
      if (!$this->query or strlen($this->query)==1) {
        return;
      }
      if ($this->search_model=="Taxon") {
        $sql = app('App\Models\\'.$this->search_model)::whereRaw('odb_txname(taxons.name, taxons.level, taxons.parent_id, 0,0) like ?', ["%".$this->query."%"])->take(20)->get()
        ->map(function($t){ return ['id' => $t->id,'fullname' => $t->scientificName];})
        ->sortBy('fullname',SORT_NATURAL|SORT_FLAG_CASE)->values()
        ->toArray();
        $this->list_of_options = $sql;        
        return;
      } 

      if ($this->search_model=="User") {
        $sql = app('App\Models\\'.$this->search_model)::where('email','like',"%".$this->query."%")->orWhereHas("person",function($p){
          $p->where('full_name','like',$this->query."%");
        })->take(20)->get()
        ->map(function($t){ 
          return [
            'id' => $t->id,
            'name' => $t->identifiableName()
        ];})
        ->sortBy('name',SORT_NATURAL|SORT_FLAG_CASE)->values()
        ->toArray();
        $this->list_of_options = $sql;        
        return;
      } 

      if ($this->search_model=="Individual" and $this->batch_identify) {             
        $keyword = $this->query;
        if (substr($keyword,0,1)=="=") {
          $tag = str_replace("=","",$this->query);
          $sqlraw = "tag='".$tag."'";
        } else {
          $sqlraw = "SUBSTRING_INDEX(tag,'-',-1) LIKE '".$keyword."%' OR tag LIKE '".$keyword."%'";
        }
        $sql = app('App\Models\\'.$this->search_model)::withoutGlobalScopes()->whereRaw($sqlraw)->take(40)
        ->get()->map(function($t){ return ['id' => $t->id,'fullname' => $t->tag." - ".$t->scientificName." - ".$t->location_name." - ".$t->location_parent_name,];})
        ->sortBy('fullname',SORT_NATURAL|SORT_FLAG_CASE)->values()
        ->toArray();
        $this->list_of_options = $sql;
        return;
      }  
      if ($this->search_model=="FormTaskObject" and $this->form_task_id) {   
        $keyword = $this->query;
        if (substr($keyword,0,1)=="=") {
          $keyword = str_replace("=","",$this->query);
          $sqlraw = "search_label LIKE '".$keyword."'";          
        } else {
          $sqlraw = "label LIKE  '%".$keyword."%'";
        }          
        $object = app('App\Models\\'.$this->search_model)::where('form_task_id',$this->form_task_id)->where('status',0)
        ->whereRaw($sqlraw);          

        $object = $object->take(20)->get()->map(function ($obj) {
            return [
              'id' => $obj->measured_id,
              'label' => $obj->label,
            ];
        })
        ->sortBy('label',SORT_NATURAL|SORT_FLAG_CASE)->values()
        ->toArray();
        $this->list_of_options = $object;
        return;
      }

      if ($this->search_model=="Voucher") {   
        $keyword = $this->query;
        if (substr($keyword,0,1)=="=") {
          $tag = str_replace("=","",$this->query);
          $sqlraw = "tag='".$tag."'";
          $sqlraw2 = "number='".$tag."'";
        } else {
          $sqlraw = "tag LIKE '".$keyword."%'";
          $sqlraw2 = "number LIKE  '".$keyword."%'";
        }  
        $vouchers = app('App\Models\\'.$this->search_model)::withoutGlobalScopes()->whereHas('individual',function($i) use($sqlraw) {
          $i->whereRaw($sqlraw);
        })->orWhereRaw($sqlraw2)->take(20)->get()->map(function ($voucher) {
            return [
              'id' => $voucher->id,
              'fullname' => $voucher->fullname,
            ];
        })
        ->sortBy('fullname',SORT_NATURAL|SORT_FLAG_CASE)->values()
        ->toArray();
        $this->list_of_options = $vouchers;
        return;
      }

      if ($this->search_model=="ODBTrait") {
        $keyword = $this->query;
        $sql = app('App\Models\\'.$this->search_model)::whereHas('translations',function($trn) use ($keyword) { $trn->where('translation','like','%'.$keyword.'%');})->orWhere('export_name','like',$keyword.'%')->take(10)->get()
        ->map(function($t){ return ['id' => $t->id,'name' => $t->export_name." - ".$t->name];})
        ->sortBy('name',SORT_NATURAL|SORT_FLAG_CASE)->values()
        ->toArray();
        $this->list_of_options = $sql;        
        return;
      }
      if ($this->search_model=="Tag") {
        $keyword = $this->query;
        $sql = app('App\Models\\'.$this->search_model)::whereHas('translations',
          function($trn) use ($keyword) { 
            $trn->where('translation','like','%'.$keyword.'%')->where('translation_type',0);
          }
        )->take(10)->get()
        ->map(function($t){ return [
          'id' => $t->id,
          'name' => $t->name];
        })
        ->sortBy('name',SORT_NATURAL|SORT_FLAG_CASE)->values()
        ->toArray();
        $this->list_of_options = $sql;        
        return;
      } 


      if ($this->search_model=="BibReference") {
        $keyword = $this->query;
        $references = app('App\Models\\'.$this->search_model)::where('bibtex', 'LIKE', ['%'.$keyword.'%'])->orWhereRaw("odb_bibkey(bibtex) like '".$keyword."%'")->take(10)->get()->map(function($bib){
          return [
            'id' => $bib->id,
            'name' => $bib->bibkey,
          ];
        })->sortBy('name',SORT_NATURAL|SORT_FLAG_CASE)->values()
        ->toArray();
        $this->list_of_options = $references;        
        return;
      } 



      if ($this->nested_model) {
          $search_field = $this->search_field;
          $query = $this->query;
          $this->list_of_options = app('App\Models\\'.$this->search_model)::whereHas($this->nested_model, function($q) use ($search_field,$query) {
              $q->where($search_field,'like','%'.$query.'%');
          })->take(20)->get()->toArray();
          return;
      }     
      $this->list_of_options = app('App\Models\\'.$this->search_model)::where($this->search_field,'like','%'.$this->query.'%')->orderBy($this->search_field)->take(20)->get()->toArray();              
    }

    protected $listeners = [
        'clearBar'
    ];

    public function clearBar()
    {
      $this->resetBar();
    }

    public function render()
    {
        return view('livewire.common.input-search-bar');
    }

}
