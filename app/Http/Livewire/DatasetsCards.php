<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Dataset;

class DatasetsCards extends Component
{
    use WithPagination;

    protected $paginationTheme = 'simple-bootstrap';
    public $project;
    public $isproject;
    public $number_datasets;

    public function mount($project=null) {
        $this->project = $project;
        $this->isproject = $project ? true : false;
       
    }

    public function render()
    {
        if ($this->project) {
            $this->number_datasets = Dataset::where('project_id',$this->project)->count();
            $datasets = Dataset::where('project_id',$this->project)->inRandomOrder()->paginate(3);        
        } else {
            $datasets = Dataset::orderBy('created_at','desc')->paginate(1);
        }        
        return view('livewire.datasets-cards', ['datasets' => $datasets]);        
    }
}
