<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\BibReference;
use App\Models\ODBTrait;
use App\Models\TraitCategory;
use App\Models\Measurement;
use App\Models\Person;
use App\Models\Location;
use App\Models\MeasurementCategory;

use Auth;
use Lang;

class MeasurementForm extends Component
{

    public $trait_id;
    public $trait_name;
    public $odbtrait;
    
    public $measured_id;
    public $measured_type;
    public $measured_name;
    public $measurement;    
    public $no_object_editing = false;


    public $object_types=[];
    public $short_measured_type;
    public $parent_measurements=[];

    public $dataset_id;
    public $user_datasets=[];
    public $bibreference_id;
    public $parent_id;
    public $notes;
    public $location_id;  
    public $persons;  
    public $authors=[];  
    public $authors_order=[];
    
    public $value;
    public $value_i;
    public $value_a;
    public $selected_categories=[];
    public $category;
    
    public $date;
    public $date_year;
    public $date_month;
    public $date_day;


    public $bibreference_name;
    public $location_name;

    public $allow_duplicated;
    public $confirm_duplicated=false;
    public $existing_dups=0;

    public function mount($measurement=null,$measured_id=null,$measured_type=null)
    {
        $this->object_types = collect(ODBTrait::OBJECT_TYPES)->map(function($o){
            return [
                'value' => $o,
                'label' => str_replace("App\Models\\", "",$o),
            ];
        })->toArray();

        $dataset_users = Auth::user()->editableDatasets();
        if ($dataset_users!==null) {    
            $this->user_datasets = $dataset_users->get()->map(function($d) { 
                return [
                    'id' => $d->id,
                    'label' => $d->name,
                ];})->sortBy('label',SORT_NATURAL|SORT_FLAG_CASE)->values()->toArray();
        }
        $this->date_year = (int) today()->format("Y");
        $this->date_month = (int) today()->format("m");
        $this->date_day = (int) today()->format("d");    
        $this->date = today()->format("Y-m-d");    

        if ($measurement){
            $this->measurement = Measurement::find($measurement);
            $this->trait_id = $this->measurement->trait_id;
            $this->odbtrait = $this->trait_id ? ODBTrait::find($this->trait_id) : null;            
            
            $this->measured_id =$this->measurement->measured_id;
            $this->measured_type =$this->measurement->measured_type;
            $this->measured_name = $this->measurement->measured->rawLink();

            $this->dataset_id =$this->measurement->dataset_id;
            
            $this->bibreference_id =$this->measurement->bibreference_id;
            $this->bibreference_name = $this->bibreference_id ? BibReference::find($this->bibreference_id)->rawLink() : null;            
            
            $this->parent_id =$this->measurement->parent_id;
            
            $this->notes =$this->measurement->notes;
            
            $this->location_id =$this->measurement->location_id;
            $this->location_name = $this->location_id ? Location::find($this->location_id)->rawLink() : null;            

            $this->persons =$this->measurement->collectors->pluck('id')->toArray();
            $this->authors =$this->measurement->collectors->map(function($p){
                return [
                    'id' => $p->id,
                    'name' => $p->full_name
                ];
            })->toArray();
            $authors_order = array_keys($this->authors);
            $authors_order = array_combine($authors_order,$authors_order);
            $this->authors_order = $authors_order;  
            $this->value  =$this->measurement->raw_value_actual;            
        } elseif($measured_id and $measured_type) {
            $this->no_object_editing=true;
            $object = app($measured_type)->find($measured_id);
            $this->measured_id =$measured_id;
            $this->measured_type = get_class($object);
            $this->short_measured_type = str_ireplace("App\Models\\","",$this->measured_type);            
            $this->measured_name = $object->rawLink();            
        }       
        if ($this->measured_id and $this->date) {
            $this->parentMeasurements();
        }
    }

    public function rules()
    {
        return [
            'trait_id' => ['required','integer','exists:traits,id',function($attribute, $value, $fail) {
                $odbtrait = ODBTrait::find($value);
                $teste = !$odbtrait->valid_type($this->measured_type);
                if ($teste)
                {
                    return $fail(Lang::get('messages.invalid_trait_type_error'));
                }
                //must test for parent trait requirement
                //if trait has a parent trait, a measurement for it must exist
                if ($odbtrait->parent_id) {
                    $has_measurement = Measurement::withoutGlobalScopes()->
                    where('trait_id',$odbtrait->parent_id);
                    $has_measurement = $has_measurement->where('measured_id',$this->measured_id);
                    $has_measurement = $has_measurement->where('measured_type',$this->measured_type);
                    $colldate = [$this->date_month, $this->date_day, $this->date_year];
                    if (Measurement::checkDate($colldate)) {
                        $year =str_pad((int) $this->date_year, 4, '0', STR_PAD_LEFT);
                        $month = str_pad((int) $this->date_month, 2, '0', STR_PAD_LEFT);
                        $day = str_pad((int) $this->date_day, 2, '0', STR_PAD_LEFT);
                        $dt = $year."-".$month."-".$day;
                        $has_measurement = $has_measurement->where('date',$dt);
                    }
                    if ($has_measurement->count()==0) {
                        $parent = $odbtrait->parentTrait->export_name;
                        return $fail('Storing a measurement for this Trait requires having a measurement for trait '.$parent.' on the same date for this object. None found!');
                    }
                }
                return null;
            }],            
            'measured_id' => 'required|integer',
            'measured_type' => ['required','string'],
            'date_year' => ['required','integer',
            function($attribute, $value, $fail) {
                $colldate = [$this->date_month, $this->date_day, $value];
                if (!Measurement::checkDate($colldate)) {
                    return $fail(Lang::get('messages.invalid_date_error'));
                }
                // measurement date must be in the past or today
                if (!Measurement::beforeOrSimilar($colldate, date('Y-m-d'))) {
                    return $fail(Lang::get('messages.date_future_error'));
                }
                return null;
            }
            ],
            'dataset_id' => 'required|integer|exists:datasets,id',
            'authors' => 'required|array',  
            'value' => ['required',function($attribute, $value, $fail) {   
                $measurement_id = $this->measurement ? $this->measurement->id : null;      
                $valid = Measurement::valueValidation($value,$this->odbtrait,$measurement_id);
                if (count($valid)>0) {
                    return $fail(implode("<br>",$valid));
                }
                return null;
            }]
        ];
    }

    public function save()
    {
        if ($this->submit())
        {
            return redirect('measurements/'.$this->measurement->id);
        }
    }

    public function saveAnother()
    {
        if ($this->submit())
        {
            $this->value=null;
            $this->notes=null;        
            $this->bibreference_id=null;
            $this->bibreference_name=null;
            $this->parent_id=null;
            $this->trait_id=null;
            $this->measurement=null;
            $this->odbtrait=null;        
        }
    }


    public function submit()
    {
        $this->validate();
        $record = [
            'trait_id' => $this->trait_id,
            'measured_id'=> $this->measured_id,
            'measured_type'=> $this->measured_type,
            'dataset_id'=> $this->dataset_id,
            'bibreference_id'=> $this->bibreference_id,
            'notes'=> $this->notes,
            'parent_id'=> $this->parent_id,
            'location_id'=> $this->location_id,
        ];
        
        //check for duplicated measurements
        $registry = $record;
        $registry['value'] = $this->value;
        $registry['date_month'] = $this->date_month;
        $registry['date_day'] = $this->date_day;
        $registry['date_year'] = $this->date_year;

        
              /* if not editing and other records exists
         * ask to confirm duplication
        */
        if (!$this->measurement and !$this->allow_duplicated) {
            $existing_dups  = Measurement::checkDuplicateMeasurement($registry);
            if ($existing_dups>0)         
            {  
                $this->confirm_duplicated=true;
                $this->existing_dups = $existing_dups;
                return false;
            }
        }
        if ($this->measurement) {
            $measurement = $this->measurement;
            $measurement->update($record);            
            $measurement->collectors()->detach();
        } else {
            $measurement = new Measurement($record);
        }
        $measurement->setDate($this->date_month, $this->date_day, $this->date_year);
        $measurement->save();   
        $persons = collect($this->authors)->pluck('id')->toArray();             
        $measurement->collectors()->attach($persons);        
        $measurement->valueActual = $this->value;
        $measurement->save();
        $this->measurement = $measurement;
        return true;        
    }

    public function parentMeasurements()
    {
        if ($this->date_year) {
            $year = $this->date_year;
            $month = $this->date_month ? $this->date_month : "00";
            $day = $this->date_day ? $this->date_day : "00";
            $this->date = $year."-".$month."-".$day;
            $this->parent_measurements = Measurement::where('measured_type',$this->measured_type)->where('measured_id',$this->measured_id)->where('date',$this->date)->get()->map(function($m){
                return [
                    'id' => $m->id,
                    'label' => $m->selector_name,
                ];
            })->sortBy('label',SORT_NATURAL|SORT_FLAG_CASE)->values()->toArray();  
        }
    }

    /* this validates upon update the input fields in the blade*/
    public function updated($propertyName)
    {        
        $this->validateOnly($propertyName);
    }

    
    public function updatedIdentificationYear()
    {
        $this->parentMeasurements();
    }
    public function updatedIdentificationMonth()
    {
        $this->parentMeasurements();
    }
    public function updatedIdentificationDay()
    {
        $this->parentMeasurements();
    }

    public function updatedMeasuredType()
    {
        $measured_type = $this->measured_type;        
        $this->short_measured_type=null;
        $this->measured_id=null;
        $this->measured_name=null;
        if ($measured_type) {
            $this->short_measured_type = str_ireplace("App\Models\\","",$measured_type);            
        }         
    }

    public function updatedCategory()
    {
        $category = TraitCategory::find($this->category);
        $idx = (string) $category;
        $this->selected_categories[$idx] = [
            'id' => $category->id,
            'label' => $category->name,
        ];            
        $this->value = collect($this->selected_categories)->pluck('id')->toArray();
        $this->category =null;
    }

    public function removeCategory($index)
    {
        $categories = $this->selected_categories;
        if (isset($categories[$index])) {
            unset($categories[$index]);
            $this->selected_categories = $categories;
            $this->value = collect($this->selected_categories)->pluck('id')->toArray();
        }
    }

    protected $listeners = [
        'mergeChildLocation', 
        'mergeChildTrait',     
        'mergeChildBib', 
        'mergeParentMeasurement',
        'mergeChildPerson',
        'mergeChildObject',
    ];

    public function mergeChildTrait($return_id)
    {
        $this->trait_id = $return_id;
        $this->odbtrait = null;
        $this->selected_categories=[];
        $this->value=null;
        if ($this->trait_id) {
            if ($this->validateOnly('trait_id')) {
                $this->odbtrait = ODBTrait::find($this->trait_id);
            } else {
                $this->trait_id = null;                              
            }
        } else {
            $this->odbtrait = null;
        }        
        $this->emit('clearBar');
    }    

    public function mergeChildObject($return_id)
    {
        $this->measured_id = $return_id;
        $this->measured_name = app($this->measured_type)->find($return_id)->rawLink();       
        $this->emit('clearBar');
    }    

    public function mergeChildLocation($return_id)
    {
        $this->location_id = $return_id;
        $this->location_name = Location::find($return_id)->rawLink();       
        $this->emit('clearBar');
    }    

    public function mergeParentMeasurement($return_id)
    {
        $this->parent_id = $return_id;
        $this->parent_name = Measurement::find($return_id)->rawLink();       
        $this->emit('clearBar');
    }  

    public function mergeChildBib($return_id)
    {
        $this->bibreference_id = $return_id;
        $this->bibreference_name = BibReference::find($return_id)->rawLink();       
        $this->emit('clearBar');
    } 

    public function mergeChildPerson($return_id)
    {
        $authors = $this->authors;
        $count = $authors ? count($authors) : 0;
        $a = Person::find($return_id);
        $author = collect([[
                'id' => $a->id,
                'name' => $a->full_name,                
        ]]);
        if ($count>0) {
            $authors = collect($authors)->merge($author)->unique()->toArray();
        } else {
            $authors = $author->toArray();
        }
        $this->authors = $authors;
        $authors_order = array_keys($authors);
        $authors_order = array_combine($authors_order,$authors_order);
        $this->authors_order = $authors_order;     
        $this->author_id = null;
        $this->emit('clearBar');
    }

    public function changeAuthorOrder($fromIndex) : void {
        $authors = $this->authors;
        $authors_order = $this->authors_order;
        $toIndex = $authors_order[$fromIndex];      
        if ($fromIndex!=$toIndex) {  
            $this->moveArrayElement($authors,(int) $fromIndex,(int) $toIndex);
            $this->authors = $authors;     
            $authors_order = array_keys($authors);
            $authors_order = array_combine($authors_order,$authors_order);
            $this->authors_order = $authors_order;     

        }
    }

    public function removeAuthor($index) : void {
        $authors = $this->authors;     
        unset($authors[$index]);
        if (count($authors)) {
            $this->authors = array_values($authors);   
            $authors_order = array_keys($authors);
            $authors_order = array_combine($authors_order,$authors_order);
            $this->authors_order = $authors_order;              
        } else {
            $this->authors_order = [];
            $this->authors = [];
        }        
    }

    public function moveArrayElement(&$array, $fromIndex, $toIndex) {
        $out = array_splice($array, $fromIndex, 1);
        array_splice($array, $toIndex, 0, $out);
    }
   

    public function render()
    {
        return view('livewire.measurement-form');
    }
}
