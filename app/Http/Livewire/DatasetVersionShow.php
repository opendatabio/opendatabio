<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\DatasetVersion;
use App\Models\Dataset;
use App\Jobs\DownloadDataset;
use App\Models\UserJob;
use Auth;
use Lang;


class DatasetVersionShow extends Component
{

    public $dataset_version;
    public $authors;
    public $citation;
    public $data_files;
    public $edit_citation;
    public $citation_print;

    public function mount($dataset_version=null, $uuid=null) : void 
    {    
        if ($dataset_version) {
            $this->dataset_version = DatasetVersion::find($dataset_version);
        } else {
            $this->dataset_version = DatasetVersion::where('uuid',$uuid)->get()->first();
        }
        $this->authors = implode(' | ',$this->dataset_version->persons()->pluck('full_name')->toArray());            
        $metadata = $this->dataset_version->metadata ? json_decode($this->dataset_version->metadata,true) : [];        
        $this->data_files = isset($metadata['files']) ? $metadata['files'] : [];
        $this->citation = $this->dataset_version->citation;
        $this->citation_print = $this->dataset_version->citation_print;
    }
    

    public function editCitation()
    {
        $exists = $this->citation;
        if ($exists) {
            $this->edit_citation = true;        
        } 
    }

    public function citationGenerate()
    {
        $array = $this->generateCitation();
        $this->citation = $array['bibtex'];
        $this->citation_print = $array['citation'];
        $this->saveCitation();        
    }

    public function editVersion()
    {
      
        return redirect()->route('dataset.version.edit',[
            'dataset_version' => $this->dataset_version->id,
          ]);      
    }

    public function saveCitation()
    {
            //need to save here
        $this->dataset_version->citation = $this->citation;
        $this->dataset_version->save();
        $this->citation_print = $this->dataset_version->citation_print;
        $this->edit_citation = false;            

    }

   


    public function generateCitation(): array
    {
        $version = $this->dataset_version;
        $when = Date("Y-m-d",strtotime($version->created_at));
        $year = $when = Date("Y",strtotime($version->date));        
        $dataset_version = $version->version;
        $license = (null != $version->license) ? $version->license : 'Not defined, some restrictions may apply';
        if (preg_match("/CC0/i",$license)) {
            $license = "Public domain - CC0";
        }
        $dataset = $version->dataset;
        $data_filter = $version->filters ? json_decode($version->filters,true) : [];
        $filtered_by=[];
        if (count($data_filter)) {
            $taxon_root = isset($data_filter['selected_taxons']) ? implode(',',collect( $data_filter['selected_taxons'])->pluck('label')->toArray()) : null;
            $location_root = isset($data_filter['selected_locations']) ? implode(',',collect( $data_filter['selected_locations'])->pluck('id')->toArray()) : null;
            $persons = isset($data_filter['persons']) ? implode(',',collect( $data_filter['persons'])->pluck('label')->toArray()) : null;
            $traits = isset($data_filter['selected_traits']) ? implode(',',collect( $data_filter['selected_traits'])->pluck('label')->toArray()) : null;
            $min_date = isset($data_filter['filter_min_date']) ? $data_filter['filter_min_date'] : null;
            $max_date = isset($data_filter['filter_max_date']) ? $data_filter['filter_max_date'] : null;           
            $filter_params = [
              'Taxon' => $taxon_root,
              'Location' => $location_root,
              'Person' => $persons,
              'DateFrom' => $min_date,
              'DateTo' => $max_date,
              'Trait' => $traits,
            ];     
            $filter_params = array_filter($filter_params);            
            foreach($filter_params  as $key => $value) {
                $filtered_by[] = $key.": ".$value;
            }
        }
        $title = isset($dataset->title) ? $dataset->title : $dataset->name;
        if (count($filtered_by)) {
            $title = $title." - ".Lang::get('messages.partial_data')." - ".implode("; ", $filtered_by);
        }
        $authors = $version->persons ? $version->persons()->pluck('full_name')->toArray() : [];
        $authorsa = $version->persons ? $version->persons()->pluck('abbreviation')->toArray() : [];
        $bibk = mb_strtolower(explode(",",$authorsa[0])[0]);
        if (count($authorsa)>2) {
            $bibk = $bibk."etAl";
        } elseif(count($authorsa)==2) {
            $bibk2 = mb_strtolower(explode(",",$authorsa[1])[0]);
            $bibk = $bibk."_".$bibk2;
        } 
        $bibk = $bibk.$year."_".mt_rand(0,9);   
        $bibk = str_replace(" ","",$bibk); 
        $bib =  [
            'bibkey' => $bibk,
            'title' => $title,
            'year' => $year,
            'version' => $dataset_version." ".$version->date,
            'license' => $license." ".$version->license_version,
            'note' => $version->policy ? "Data use policy:".$version->policy : null,            
            'author' => count($authors)>0 ? implode(" AND ",$authors) : null,
            'uuid' => $version->uuid,
            'url' => url('dataset-uuid/'.$version->uuid),
            'type' => "Dataset",
        ];
        $bib = array_filter($bib);        
        if (count($authors)>3) {
            $authors_short = $authors[0]." et al.";
        } else {
            $authors_short = count($authors) ? implode(", ",$authors) : '';
        }
        $citation = $authors_short." (".$year."). <strong>".$title."</strong>. Version: ".$dataset_version.".";
        if ($license) {
            $citation .= " License: ".$license." ".$version->license_version;
        } elseif($version->dataset_access==1) 
        {
            $citation .= " License: No CC defined. Access restricted!";
        }
        $url = url('dataset-version-show/'.$version->id);
        $citation .= $url; 
        return [
            'citation' => $citation,
            'bibtex' => json_encode($bib,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
        ];
    }    

    public function generateDataFiles()
    {
        $dataset_version = $this->dataset_version;
        $dataset = $this->dataset_version->dataset;
        if (Auth::user()->can('delete', $dataset)) {
          UserJob::dispatch(DownloadDataset::class,
            [
            'data' => ['data' => [
                'id' => $dataset->id,
                'dataset_version' => $dataset_version->id,
                ]
                ]
            ]);
            $msg = Lang::get('messages.download_dispatched',['url' => Route('userjobs.list') ]);
            session()->flash('success',$msg);
        } else {
          $msg = Lang::get('messages.no_permission');
          session()->flash('errors',$msg);
        }
        return;
    }
    
    public function render()
    {
        return view('livewire.dataset-version-show');
    }
}
