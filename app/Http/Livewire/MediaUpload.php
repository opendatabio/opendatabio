<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\Media;
use Illuminate\Validation\Rule;
use CodeInc\StripAccents\StripAccents;
use App\Models\UserJob;
use App\Jobs\ImportMedia;
use Illuminate\Support\Str;
use Spatie\SimpleExcel\SimpleExcelReader;

use Auth;
use File;
use Lang;
use Storage;

class MediaUpload extends Component
{
    use WithFileUploads;

    public $uploadedFiles=[];
    public $media_attribute_table;
    public $project_link;

    public function mount() : void
    {
        $this->user_id = Auth::user()->id;            
    }

    /*
        Controll for batch upload of images
    */
    public function uploadFiles(): void
    {

        if (!Auth::user()->can('create', Media::class) or 
        !Auth::user()->can('create', UserJob::class)) {
            session()->flash("errors",Lang::get("messages.unathorized"));
            return;
        };
        $media = $this->uploadedFiles;
        if (!$this->media_attribute_table || count($media)==0) {
            session()->flash("errors",Lang::get("messages.media_attributes_file_missing"));
            return;
        } 
        /*
            Validate attribute file
            Validate file extension and maintain original if valid or else
            Store may save a csv as a txt, and then the Reader will fail
        */
        $org_name = $this->media_attribute_table->getClientOriginalName();
        $extension = explode(".",$org_name);
        $extension = mb_strtolower($extension[count($extension)-1]);
        $valid_extensions=["CSV","csv","ODS","ods","XLSX",'xlsx'];
        $valid_ext = in_array($extension,$valid_extensions);
        if(!$valid_ext) {
            session()->flash("errors",Lang::get("messages.wrong_file_type",[
            'valid_types' => implode(",",$valid_extensions),
            ]));
            return;
        }
        $hash = Str::random(40);
        $path = $this->media_attribute_table->storeAs('temp',$hash.".".$extension);
        $path = Storage::path($path);
        $media = collect($media)->map(function($m){
            return [
                'name' => $m->getClientOriginalName(),
                'path' => $m->getRealPath(),
            ];    
        })->toArray();
        /* send request as job */
        UserJob::dispatch(ImportMedia::class, [
            'data' => [
                'media_paths' => $media, 
                'media_attribute_table' => $path
            ]
        ]);
        $message = Lang::get('messages.dispatched',
            ['url' => Route('userjobs.list') 
        ]);
        session()->flash("success",$message);
        return;
    }
   
    
    public function manualBatchUpload(): void
    {

        if (!Auth::user()->can('create', Media::class) or 
        !Auth::user()->can('create', UserJob::class)) {
            session()->flash("errors",Lang::get("messages.unathorized"));
            return;
        };

        $allfile = File::allFiles(Storage::path('img_temp'));
        if (!$this->media_attribute_table || count($allfile)==0) {
            session()->flash("errors",Lang::get("messages.media_attributes_file_missing"));
            return;
        } 

        /*
            Validate attribute file
            Validate file extension and maintain original if valid or else
            Store may save a csv as a txt, and then the Reader will fail
        */
        $org_name = $this->media_attribute_table->getClientOriginalName();
        $extension = explode(".",$org_name);
        $extension = mb_strtolower($extension[count($extension)-1]);
        $valid_extensions=["CSV","csv","ODS","ods","XLSX",'xlsx'];
        $valid_ext = in_array($extension,$valid_extensions);
        if(!$valid_ext) {
            session()->flash("errors",Lang::get("messages.wrong_file_type",[
            'valid_types' => implode(",",$valid_extensions),
            ]));
            return;
        }
        $hash = Str::random(40);
        $path = $this->media_attribute_table->storeAs('temp',$hash.".".$extension);
        $path = Storage::path($path);

        $allfile = File::allFiles(Storage::path('img_temp'));
        $media = collect($allfile)->map(function($m){
            return [
                'name' => $m->getFileName(),
                'path' => $m->getRealPath(),
            ];    
        })->toArray();
        /* send request as job */
        UserJob::dispatch(ImportMedia::class, [
            'data' => [
                'media_paths' => $media, 
                'media_attribute_table' => $path
            ]
        ]);
        $message = Lang::get('messages.dispatched',
            ['url' => Route('userjobs.list') 
        ]);
        session()->flash("success",$message);
        return;
    }

    public function render()
    {
        return view('livewire.media-upload');
    }
}
