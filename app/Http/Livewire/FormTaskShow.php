<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\ODBTrait;
use App\Models\Form;
use App\Models\Dataset;
use App\Models\FormTask;
use App\Jobs\FormTaskPrepare;
use App\Models\FormTaskObject;

use Illuminate\Validation\Rule;
use Lang;
use App\Models\UserJob;

class FormTaskShow extends Component
{
    public $form;
    public $form_task;
    public $measured_type;
    public $short_measured_type;

    public $datasets_list = [];    
    public $name;
    public $created_at;
    public $object_order_list=[];
    public $object_order;    
    public $show_input_object=false;
    public $measured_name;
    public $from_form_task=false;

    public function mount($form_task_id)
    {
        $this->form_task = FormTask::find($form_task_id);
        $this->form = $this->form_task->form;
        $this->name = $this->form_task->name;
        $this->form_user = $this->form_task->user;
        $this->created_at = $this->form_task->created_at;
        $this->measured_type = $this->form_task->form->measured_type;
        $metadata = json_decode($this->form_task->metadata,true);
        if (isset($metadata['datasets'])) {
            $this->datasets_list = $metadata['datasets'];
        } 
        if (isset($metadata['object_order'])) {
            $this->object_order = $metadata['object_order'];
        } 
        $this->short_measured_type = str_ireplace("App\Models\\","",$this->measured_type);
    }
    
    public function editTask()
    {
        return $this->redirectRoute('form_tasks.edit',['form_task_id' => $this->form_task->id]);
    }

    public function generateTask()
    {
        UserJob::dispatch(FormTaskPrepare::class,
        [
            'data' => ['data' => ['form_task_id' => $this->form_task->id],
            'header' => ['not_external' => 1]
            ]
        ]
        );
        $this->form_task->status="preparing";        
        $this->form_task->save();
        session()->flash('success',Lang::get('messages.dispatched',['url' => Route('userjobs.list')]));
        return;     
    }
    
    public function toogleObjectInput()
    {
        $this->show_input_object=$this->show_input_object ? false: true;     
    }

    public function processTask()
    {
        return redirect()->route('forms.fill.task',[
            'form_task_id' => $this->form_task->id,
          ]);     
    }

    protected $listeners = [
        'mergeChildObject',
    ];

    public function mergeChildObject($return_id)
    {
        $measured = app($this->measured_type)->find($return_id);
        if ($measured) {

            if ($this->short_measured_type=="Individual")
            {                
                $label = $measured->selector_label;
                $tag = trim($measured->tag);
                $tag = str_replace(" ","-",$tag);
                $tag = str_replace("_","-",$tag);
                $tags = explode("-",$tag);
                if (count($tags)>1) {
                    $index = count($tags)-1;
                    $tag = $tags[$index] ? $tags[$index] : $tags[($index-1)];
                } 
                $search_label = $tag;

            } elseif ($short_measured_type=='Taxon') 
            {
                $label = $measured->scientificName;
                $search_label = $label;

            } elseif ($short_measured_type=='Voucher') 
            {
                $label = $measured->selector_label;
                $search_label = $measured->recordNumber;
            } elseif ($short_measured_type=='Location') 
            {
                $label = $measured->name." - ".$measured->parentName;
                $search_label = $label;
            } else {
                $label = $measured->name;
                $search_label = $label;
            }

            $records = [
                'form_task_id' => $this->form_task->id,
                'measured_id' =>$return_id,
                'measured_type' => $this->measured_type,
                'label' => $label,                
                'search_label' => $search_label,
            ];
            /* prevent duplications */
            $exists = FormTaskObject::where('form_task_id',$this->form_task->id)->where('measured_id',$return_id)->where('measured_type',$this->measured_type)->count();
            if ($exists) {
                session()->flash('errors',"Object ".$label." already in task!");                                
            } else {
                session()->flash('success',"Object ".$label." added to task!");  
                $form_task_measured = FormTaskObject::create($records);
                $form_task_measured_id = $form_task_measured->id;
                $this->form_task->progress_max=$this->form_task->progress_max+1;
                $this->form_task->save();                
            }
        }  
        $this->emit('clearBar');
        return;
    }  


    public function render()
    {
        return view('livewire.form-task-show');
    }
}
