<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Project;

class ProjectsCards extends Component
{
    use WithPagination;

    protected $paginationTheme = 'simple-bootstrap';


    public function render()
    {
        $theprojects = Project::paginate(1);
        return view('livewire.projects-cards', ['theprojects' => $theprojects]);        
    }
}
