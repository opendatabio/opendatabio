<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Dataset;
use App\Models\Person;
use App\Models\Location;
use App\Models\Taxon;
use App\Models\ODBTrait;
use App\Models\DatasetVersion;
use Illuminate\Validation\Rule;

use DB;
use Lang;

class DatasetVersioning extends Component
{

    public $dataset_id;
    public $version;
    public $license;
    public $license_version;
    public $policy;
    public $version_readonly;
    public $version_date;
    public $version_options=[];
    public $authors=[];
    public $author_id;
    public $authors_order=[];
    public $group_person_id;
    public $group_person_label;
    public $dataset_access;
    public $citation;
    public $privacy;
    public $notes;
    public $metadata;
    public $dataset_name;
    public $uuid;
    public $selected_taxons=[];
    public $selected_locations=[];
    public $selected_persons=[];
    public $selected_traits=[];
    public $filter_min_date;
    public $filter_max_date;

    public function mount($dataset_id=null,$dataset_version=null) : void {
        
        $this->dataset_version = $dataset_version ? DatasetVersion::find($dataset_version) : null;
        $this->dataset_access = 0;
        $this->license =  "CC0";
        $this->license_version = config('app.creativecommons_version')[0];
        if ($this->dataset_version) {
            $dataset_version = $this->dataset_version;        
            $this->license =  $dataset_version->license ? $dataset_version->license  : $this->license;
            $this->license_version =  $dataset_version->license_version ? $dataset_version->license_version : $this->license_version;
            $this->version = $dataset_version->version;                      
            $this->policy =  $dataset_version->policy;
            $this->notes =  $dataset_version->notes;
            $this->citation = $dataset_version->citation;
            $this->version_date = $dataset_version->date;
            $filters = $dataset_version->filters ? json_decode($dataset_version->filters,true) : null;
            if ($filters) {
                $this->selected_taxons = isset($filters['selected_taxons']) ? $filters['selected_taxons'] : [];
                $this->selected_locations = isset($filters['selected_locations']) ? $filters['selected_locations'] : [];
                $this->selected_persons = isset($filters['selected_persons']) ? $filters['selected_persons'] : [];
                $this->selected_traits = isset($filters['selected_traits']) ? $filters['selected_traits'] : [];
                $this->filter_min_date = isset($filters['filter_min_date']) ? $filters['filter_min_date'] : null;
                $this->filter_max_date = isset($filters['filter_max_date']) ? $filters['filter_max_date'] : null;
            }
            $this->metadata = $dataset_version->metadata ? json_decode($dataset_version->metadata,true) : null;
            $this->authors = $dataset_version->persons()->get()->map(function($p){
                return [
                    'id' => $p->id,
                    'name' => $p->full_name,
                ];
            })->toArray();
            $this->dataset = $dataset_version->dataset;
            $this->privacy = $this->dataset->privacy;
            $this->dataset_name = $this->dataset->rawLink();
            $this->dataset_access = $dataset_version->dataset_access;
            $this->dataset_id = $this->dataset->id;
        } elseif ($dataset_id) {
            $dataset = Dataset::find($dataset_id);
            $this->dataset = $dataset;
            $this->dataset_id = $dataset->id;
            $this->privacy = $dataset->privacy;
            $this->dataset_name = $dataset->rawLink();            
            if ($dataset->last_version) {
                $dataset_version = $dataset->last_version;
                $this->license =  $dataset_version->license;
                $this->license_version =  $dataset_version->license_version;
                $this->policy =  $dataset_version->policy;
                $this->filters = $dataset_version->filters ? json_decode($dataset_version->filters,true) : null;
                $this->metadata = null;
                $this->authors = $dataset_version->persons()->get()->map(function($p){
                    return [
                        'id' => $p->id,
                        'name' => $p->full_name,
                    ];
                })->toArray();
                $this->privacy = $dataset_version->dataset->privacy;
            } 

        }   
        if (!$this->version_date) {
            $this->version_date = today()->format("Y-m-d");
        }
        
        if ($this->authors) {
        $authors_order = array_keys($this->authors);
        $authors_order = array_combine($authors_order,$authors_order);
        $this->authors_order = $authors_order;     
        }
        $this->getVersionOptions();
    }

    public function rules()
    {
        return [            
            'dataset_id' => 'required',
            'version_date' => ['required','date',function($attribute,$value,$fail) {
                $dt = DatasetVersion::where('dataset_id',$this->dataset_id)->where('date','>',$value);  
                $c1 = $dt->count();
                $has =0;
                if ($this->dataset_version) {
                    $dt = DatasetVersion::where('dataset_id',$this->dataset_id)->where('date','<',$value);  
                    $dt = $dt->where("dataset_versions.id","<>",$this->dataset_version->id);                    
                    if ($dt->count()) {
                        $version = explode(".",$this->version);
                        $version = (float) $version[0].".".$version[1].$version[2];
                        $has = $dt->get()->map(function($q) use($version){
                            $v = explode(".",$q->version);
                            $v = (float) $v[0].".".$v[1].$v[2];
                            if ($v>$version) {
                                return 1;
                            }
                            return 0;                            
                        })->toArray();
                        $has = array_sum($has);
                    }
                    if ($has>0) {
                        return $fail("");    
                    }
                } elseif ($c1>0) {
                    return $fail("");
                } 
                return null;
            }],
            'version' => ['required','string',function($attribute, $value, $fail) {
                $dt = DatasetVersion::where('version',$value)->where('dataset_id',$this->dataset_id);                 
                if ($this->dataset_version) {
                    $dt = $dt->where("dataset_versions.id","<>",$this->dataset_version->id);                    
                }                
                if($dt->count()==0) {
                  return null;
                } else {
                  return $fail("");
                }
              }
            ],
            'dataset_access' => 'required',
            'license' => 'required_if:dataset_access,0',
            'license_version' => 'required_if:dataset_access,0',   
            'authors' => 'required_if:dataset_access,0',
        ];
    }
   
    public function messages() {
        return [
            'version' => 'Version number already in use',
            'version_date' => 'Date must be equal or after the date of other versions of the same dataset. Date and version must have the same order.',
        ];
    }

    public function getVersionOptions() : void
    {
        //get used versions numbers
        if ($this->dataset_version) {
            $this->version_options = [$this->dataset_version->version];
            $this->version = $this->dataset_version->version;
        } elseif ($this->dataset_id ) {            
            $versions = DatasetVersion::where('dataset_id',$this->dataset_id)->orderByDesc('version')->get();
            if ($versions->count()) {
                $version = $versions->last()->version;
                $version = explode(".",$version);
                $p1 = $version;
                $p1[2] = $p1[2]+1;
                $p2 = $version;
                $p2[1] = $p2[1]+1;
                $p2[2] = 0;
                $p3 = $version;
                $p3[0] = $p3[0]+1;
                $p3[1]=0;
                $p3[2]=0;
                $opt = [
                    implode(".",$p1),
                    implode(".",$p2),
                    implode(".",$p3),
                ];
            } else {
                $opt = [
                    "1.0.0",
                ];
            } 
            $this->version_options = $opt;
            $this->version = $opt[0];
        }
    }



    public function submit()
    {
        //validation
        $this->validate();

        $filters = [
            'selected_taxons' => $this->selected_taxons,// ? collect($this->selected_taxons)->pluck('id')->toArray() : null,
            'selected_locations' => $this->selected_locations,
            'selected_persons' => $this->selected_persons,
            'selected_traits' => $this->selected_traits,
            'filter_min_date' => $this->filter_min_date,
            'filter_max_date' => $this->filter_max_date,
        ];
        $filters = array_filter($filters);
        $version = [
            'dataset_id' => $this->dataset_id,
            'version' => $this->version,
            'dataset_access' => $this->dataset_access,
            'license' => $this->license,
            'license_version' => $this->license_version,
            'policy' => $this->policy,
            'citation' => $this->citation,
            'notes' => $this->notes,
            'filters' => count($filters) ? json_encode($filters) : null,
            'metadata' => $this->metadata ? json_encode($this->metadata) : null,
            'date' => $this->version_date,
        ];

        //dd($version);
        
        if ($this->dataset_version) {
          $agd = $this->dataset_version;
          $agd->update($version);          
        } else {
          $agd = DatasetVersion::create($version);
          /*create universal unique identifier for dataset */
          /* only on creation, will not be able to change on edition */
          $prefix = str_replace(" ","-",config("app.uuid_prefix"));
          $agd->uuid = $prefix."-".DB::select("SELECT UUID() as uuid")[0]->uuid;
          $agd->save();
        }

        if($this->authors) {            
            $aa = collect($this->authors)->pluck('id')->toArray();    
            $agd->persons()->detach();
            $agd->persons()->attach($aa);
        }

        return $this->redirectRoute('dataset.version.show',['dataset_version' => $agd->id]);

    }


    protected $listeners = [
        'mergeChildFilterTaxon',
        'mergeChildFilterLocation',
        'mergeChildFilterPerson',
        'mergeChildFilterTrait',
        'mergeChildAuthor',
    ];

    public function mergeChildFilterTaxon($return_array)
    {
        $this->selected_taxons = $return_array;
        $this->emit('clearBar');
    }
    public function mergeChildFilterLocation($return_array)
    {
        $this->selected_locations = $return_array;
        $this->emit('clearBar');
    }
    public function mergeChildFilterPerson($return_array)
    {        
        $this->selected_persons = $return_array;
        $this->emit('clearBar');
    }
    public function mergeChildFilterTrait($return_array)
    {
        $this->selected_traits = $return_array;
        $this->emit('clearBar');
    }    
    public function mergeChildAuthor($return_id)
    {
        $authors = $this->authors;
        $count = $authors ? count($authors) : 0;
        $a = Person::find($return_id);
        $author = collect([[
                'id' => $a->id,
                'name' => $a->full_name,                
        ]]);
        if ($count>0) {
            $authors = collect($authors)->merge($author)->unique()->toArray();
        } else {
            $authors = $author->toArray();
        }
        $this->authors = $authors;
        $authors_order = array_keys($authors);
        $authors_order = array_combine($authors_order,$authors_order);
        $this->authors_order = $authors_order;     
        $this->author_id = null;
        $this->emit('clearBar');
    }

    public function changeAuthorOrder($fromIndex) : void {
        $authors = $this->authors;
        $authors_order = $this->authors_order;
        $toIndex = $authors_order[$fromIndex];      
        if ($fromIndex!=$toIndex) {  
            $this->moveArrayElement($authors,(int) $fromIndex,(int) $toIndex);
            $this->authors = $authors;     
            $authors_order = array_keys($authors);
            $authors_order = array_combine($authors_order,$authors_order);
            $this->authors_order = $authors_order;     

        }
    }

    public function findAuthors() : void {
        $sql = $this->dataset->all_collectors_ids_query();
        $collector_ids = DB::select($sql);
        $collector_ids = array_map(function($v) { return (int)$v->person_id;},$collector_ids);
        $authors = Person::whereIn('id',$collector_ids)->orderBy('full_name');
        $authors = $authors->get()->map(function($p){
            return [
                'id' => $p->id,
                'name' => $p->full_name,        
            ];
        });        
        if ($this->authors) {
            $authors = collect($this->authors)->merge($authors)->unique()->toArray();
        } else {
            $authors = $authors->toArray();
        }
        $this->authors = $authors;     
        $authors_order = array_keys($authors);
        $authors_order = array_combine($authors_order,$authors_order);
        $this->authors_order = $authors_order;     
    }

    public function removeAuthor($index) : void {
        $authors = $this->authors;     
        unset($authors[$index]);
        if (count($authors)) {
            $this->authors = array_values($authors);   
            $authors_order = array_keys($authors);
            $authors_order = array_combine($authors_order,$authors_order);
            $this->authors_order = $authors_order;              
        } else {
            $this->authors_order = [];
            $this->authors = [];
        }        
    }

    public function moveArrayElement(&$array, $fromIndex, $toIndex) {
        $out = array_splice($array, $fromIndex, 1);
        array_splice($array, $toIndex, 0, $out);
    }
    

    public function render()
    {
        return view('livewire.dataset-versioning');
    }

}
