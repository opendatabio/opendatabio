<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Project;
use App\Models\Media;
use Livewire\WithPagination;
use Auth;

class ProjectShow extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $project;
    public $show_people;
    public $media_count=0;
    public $per_page=6;
    
    public function mount($project)
    {
      $project = Project::findOrFail($project);
      $this->project = $project;      
    }

    public function show($value)
    {
      $media = Media::findOrFail($value);     
      if ($media->count()) {
          return redirect()->route('media.show',[
            'medium' => $value,
          ]);        
      }
      session()->flash('errors',Lang::get('messages.unauthorized'));
      return;
    }

    public function render()
    {
        $media =[];
        $media_count = $this->project->themedia()->count();
        $this->media_count = $media_count;
        if ($media_count) {
         $media = $this->project->themedia()->orderBy('created_at','desc')->paginate($this->per_page);          
        }
        return view('livewire.project-show', ['media' => $media]);        
    }
}
