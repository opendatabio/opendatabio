<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\TraitUnit;
use App\Models\Language;
use App\Models\UserTranslation;
use Lang;


class TraitUnitForm extends Component
{
    public $unit;
    public $name;
    public $description;
    public $trait_unit;

    public function mount($trait_unit=null)
    {
        $this->languages = Language::all();
        $loc = config('app.locale');
        $this->current_locale = Language::where('code',$loc)->get()->first()->id;
        if ($trait_unit != null){
            $this->trait_unit = TraitUnit::find($trait_unit);
            $this->unit = $this->trait_unit->unit;
            $name = $this->trait_unit->translations()->where('translation_type',0)->pluck('translation','language_id')->toArray();
            $description = $this->trait_unit->translations()->where('translation_type',1)->pluck('translation','language_id')->toArray();
            $this->name = $name;
            $this->description = $description;
        }
    }

    public function rules()
    {
        $id = $this->trait_unit ? $this->trait_unit->id : null;
        $name = $this->name;

        return [
        'unit' => ['required','string','unique:trait_units,unit,'.$id],
        'name' => ['required','array',function($attribute, $value, $fail) {
            $langs = Language::pluck('id')->toArray();
            $values = array_unique(array_filter($value));
            $count = count($values);
            $keys = array_keys($value);  
            $keys_are_lang = count(array_diff($keys,$langs))==0;
            if ($count==count($langs) and $keys_are_lang)
            {
              return null;
            } else {
              return $fail("");
            }
            }
        ],
        'description' => ['nullable','array',function($attribute, $value, $fail) use($name) {
            $langs = Language::pluck('id')->toArray();
            $values = array_unique(array_filter($value));
            $count = count($values);
            if ($count>0) {
                $keys = array_keys($value);  
                $keys_are_lang = count(array_diff($keys,$langs))==0;

                if ($count==count($langs) and $keys_are_lang)
                {
                $comp = count(array_diff($value,$name))==count($value);
                if($comp){            
                    return null;
                }
                } 
                return $fail("");
            }
            return null;
        }
        ]
        ];
    }

    public function messages()
    {
        return [
            'unit' => ":attribute é obrigatório e precisa ser UNICO dentre todas as unidades cadastradas na base.",
            'name' => ":attribute é obrigatório e em todos os idiomas e não pode ser igual entre eles",
            'description' => ":attribute é opcional, mas se informado em todos os idiomas e não pode ser igual entre eles. ",            
        ];
    }
    public function submit()
    {
        $this->validate();   

        $unit =[
            'unit' => $this->unit,            
        ];
        if ($this->trait_unit) {
            $agd = $this->trait_unit;
            $agd->update($unit);          
            $agd->save();
        } else {
            $agd = TraitUnit::create($unit);
            $agd->save();
        }
        //Translations for name and descriptions
        foreach ($this->name as $lang => $translation) {
            $agd->setTranslation(UserTranslation::NAME, $lang, $translation);
        }
        foreach ($this->description as $lang => $translation) {
            $agd->setTranslation(UserTranslation::DESCRIPTION, $lang, $translation);
        }
        $agd->save();  
        return redirect()->route('trait_units.list');               
    }


    public function render()
    {
        return view('livewire.trait-unit-form');
    }
}
