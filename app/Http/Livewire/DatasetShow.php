<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Dataset;


class DatasetShow extends Component
{
    public $dataset;
    public $show_people;
    public $show_references;
    public $show_taxonomic_summary;
    public $taxonomic_summary;
    public $data_summary;
    public $show_data_summary;
    public $dataset_versions=[];
    public $dataset_versions_open=[];

    public function mount($dataset)
    {
        $dataset = Dataset::find($dataset);
        $this->dataset = $dataset;
        $this->dataset_versions = $dataset->versions->count() ? $dataset->versions()->get()->map(function($q){
            return [
                'raw_link' => $q->raw_link,
                'date' => $q->date,
            ];
        })->toArray() : [];
        $this->dataset_versions_open = $dataset->versions->count() ? $dataset->versions()->where('dataset_access', 0)->get()->map(function($q){
            return [
            'raw_link' => $q->raw_link,
            'date' => $q->date,
            ];
        })->toArray() : [];  
    }

    public function showPeople()
    {
        $this->show_people = $this->show_people ? false : true;
        $this->emit('changeFocus',"people_list");

    }
    public function showReferences()
    {
        $this->show_references = $this->show_references ? false : true;
        $this->emit('changeFocus',"references_list");

    }



    public function summarizeDataset()
    {
        if (!$this->data_summary) {
            $dataset = $this->dataset;
            $trait_summary = $dataset->traits_summary();
            //$plot_included = $dataset->plot_included();
            $plot_included = [];
            $html = view("datasets.summary",compact('dataset','trait_summary','plot_included'))
            ->render();
            $this->data_summary = $html;
            $this->show_data_summary = true;  
        } else {
            $this->show_data_summary = $this->show_data_summary ? false : true;  
        }
        $this->emit('changeFocus',"dataset_summary");
    }

    public function summarizeTaxons()
    {
        if (!$this->taxonomic_summary) {
            $dataset = $this->dataset;
            $identification_summary = $dataset->identification_summary();
            $taxonomic_summary = $dataset->taxonomic_summary();
            $show = count($taxonomic_summary)>0;
            $showt= count($identification_summary)>0;
            $show = $show or $showt;
            $html = view("datasets.taxoninfo",compact('dataset','identification_summary','taxonomic_summary','show'))->render();
            $this->taxonomic_summary = $html;
            $this->show_taxonomic_summary = true;  
        } else {
            $this->show_taxonomic_summary = $this->show_taxonomic_summary ? false : true;  
        }
        $this->emit('changeFocus',"taxonomic_summary");
    }




    public function render()
    {
        return view('livewire.dataset-show',[
            'data_summary' => $this->data_summary,
            'dataset' => $this->dataset,
        ]);
    }

}
