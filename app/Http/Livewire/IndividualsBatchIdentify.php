<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Taxon;
use App\Models\Biocollection;
use App\Models\Person;
use App\Models\Individual;
use App\Models\Project;
use App\Models\Dataset;

use App\Models\UserJob;
use App\Jobs\BatchUpdateIndividuals;
use Livewire\WithPagination;
use Illuminate\Support\Arr;

use Auth;
use Lang;

class IndividualsBatchIdentify extends Component
{

    use WithPagination;

    public $selected_individuals;
    public $modifier;
    public $date_year;
    public $date_month;
    public $date_day;
    public $biocollection_reference;
    public $identification_notes;
    public $batchupdate=true;
    public $update_nonself_identification;
    public $biocollection_id;
    public $taxon_id;
    public $person_id;
    public $identifiers = [];
    public $selected_taxon;
    public $selected_biocollection;
    #public $selected_identifier;
    public $selected_individuals_ids;
    public $show_confirmation;

    protected $paginationTheme = 'simple-bootstrap';

    protected $listeners = [
        'mergeChildTaxon',
        'mergeChildIdentifier',
        'mergeChildIndividual',
        'mergeChildBiocollection',
        'mergeChildTaxonSearch',
        'refreshComponent' => '$refresh',
    ];

    public function mount()
    {        
        $this->date_year = (int) today()->format("Y");
        $this->date_month = (int) today()->format("m");
        $this->date_day = (int) today()->format("d");
        if (Auth::user()) {
            $person = Auth::user()->person;
            if ($person) {
                $this->identifiers = [[
                    'id' => $person->id,
                    'name' => $person->full_name,
                    ]
                ];
            }
        }
        #$this->selected_identifier = $this->person_id ? Person::find($this->person_id)->full_name : null;
    }

    public function mergeChildBiocollection($return_id)
    {
        $this->biocollection_id = $return_id;
        $this->selected_biocollection = Biocollection::find($return_id)->acronym;
        $this->emit('clearBar');
    }

    public function removeBiocollection()
    {
        $this->biocollection_id = null;
        $this->selected_biocollection = null;
    }

    public function mergeChildTaxon($return_id)
    {        
        $this->taxon_id = $return_id;
        $this->selected_taxon = Taxon::find($return_id)->scientificName;
        $this->emit('clearBar');
    }
    public function mergeChildIdentifier($return_id)
    {
        #$this->person_id = $return_id;
        $identifiers = $this->identifiers;
        $count = $identifiers ? count($identifiers) : 0;
        $a = Person::find($return_id);
        $identifier = collect([[
                'id' => $a->id,
                'name' => $a->full_name,                
        ]]);
        if ($count>0) {
            $identifiers = collect($identifiers)->merge($identifier)->unique()->toArray();
        } else {
            $identifiers = $identifier->toArray();
        }
        $this->person_id = null;
        $this->identifiers = $identifiers;
        $this->emit('clearBar');
    }

    public function removeIdentifier($index) : void {
        $identifiers = $this->identifiers;     
        unset($identifiers[$index]);
        if (count($identifiers)) {
            $this->identifiers = array_values($identifiers);               
        } else {
            $this->identifiers = [];
        }        
    }

    public function mergeChildTaxonSearch($return_id)
    {   
        $user_datasets = Auth::user()->datasets()->whereIn('dataset_user.access_level',[Project::COLLABORATOR,Project::ADMIN])->pluck('datasets.id')->toArray();
        $project_datasets = Arr::flatten(Auth::user()->projects()->whereIn('project_user.access_level',[Project::COLLABORATOR,Project::ADMIN])->get()->map(function($p){
            return $p->datasets()->where('datasets.privacy',Dataset::PRIVACY_PROJECT)->pluck('datasets.id')->toArray();
        })->toArray());
        $user_datasets = array_unique(array_merge($user_datasets,$project_datasets));
        if (count($user_datasets)>0)  {
            $individuals = Individual::withoutGlobalScopes()->whereHas('identification',function($q)use($return_id){
                $q->where('taxon_id',$return_id);
            })->whereIn('dataset_id',$user_datasets)->orderBy('tag');
            if ($individuals->count()) {
                $this->selected_individuals_ids = $individuals->pluck('id')->toArray();                
                $this->emit('clearBar');
                return;
            } 
        }
        session()->flash('errors','Não existem indivíduos, ou você não tem permissão para alterar');
        return;
    }


    public function mergeChildIndividual($return_id)
    {        
        $selected_individuals = $this->selected_individuals;
        $individual = Individual::find($return_id);
        if (Auth::user()->can('update',$individual)) {
            if($this->selected_individuals_ids) {
                $this->selected_individuals_ids = array_merge([$individual->id],$this->selected_individuals_ids);
            } else {
                $this->selected_individuals_ids = [$individual->id];
            }
            $this->emit('clearBar');
        }
    }

    public function removeIndividual($id)
    {
        $selected_individuals_ids = $this->selected_individuals_ids;
        if (count($selected_individuals_ids)>1) {
            $key = array_search($id,$selected_individuals_ids);
            unset($selected_individuals_ids[$key]);
            $this->selected_individuals_ids = $selected_individuals_ids;        
        } else {
            $this->selected_individuals=null;
            $this->selected_individuals_ids=[];
        }        
    }

    public function rules()
    {
        return [            
            'taxon_id' => 'required',
            'identifiers' => 'required',
            'date_year' => 'required',
            'selected_individuals_ids' => 'required|array',
            'biocollection_reference' => 'required_with:biocollection_id',
        ];
    }

    public function resetForm()
    {
        $this->taxon_id = null;
        $this->biocollection_reference = null;
        $this->biocollection_id = null;
        $this->identification_notes = null;
        $this->update_nonself_identification = null;
        $this->selected_individuals_ids=[];
        $this->selected_taxon = null;
        $this->selected_biocollection = null;
        $this->modifier = null;
        $this->show_confirmation=false;        

    }

    public function submitNewIdentification()
    {
        $this->validate();
        $this->show_confirmation=true;        
    }

    public function saveNewIdentification()
    {
        $this->validate();        
        $individualids_list = $this->selected_individuals_ids;
        $identifiers = collect($this->identifiers)->pluck('id')->toArray();
        $new_det = [
            'taxon_id' => $this->taxon_id,
            'identifier' => implode(',',$identifiers),
            'identification_date_year' => $this->date_year,
            'identification_date_month' => $this->date_month,
            'identification_date_day' => $this->date_day,
            'identification_based_on_biocollection_id' => $this->biocollection_reference,
            'identification_based_on_biocollection' => $this->biocollection_id,
            'identification_notes' => $this->identification_notes,
            'modifier' => $this->modifier,
            'individualids_list' => implode(',',$individualids_list),
            'update_nonself_identification' => $this->update_nonself_identification,
        ];
        $new_det = array_filter($new_det);
        if (Auth::user()->can('create', Individual::class) and Auth::user()->can('create', UserJob::class))
        {
            UserJob::dispatch(BatchUpdateIndividuals::class,
            [
            'data' => ['data' => $new_det,
            'header' => ['not_external' => 1]
            ]
            ]);
            session()->flash('success',Lang::get('messages.dispatched',['url' => Route('userjobs.list')]));
            $this->resetForm();
            return;
        } 
        $this->resetForm();
        session()->flash('errors',Lang::get('messages.unauthorized'));
        return;
    }

    public function render()
    {
        
        return view('livewire.individuals-batch-identify');
    }
}
