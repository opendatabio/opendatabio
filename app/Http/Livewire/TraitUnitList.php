<?php

namespace App\Http\Livewire;
use Livewire\WithPagination;

use Livewire\Component;
use App\Models\TraitUnit;

class TraitUnitList extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $per_page=20;
    public $confirm_creation;
    public $confirmed;

    public function mount()
    {

    }

    public function newUnit($confirmed=null)
    {
        if ($confirmed) {
            return redirect()->route('trait_unit.form');   
        } else {
            $this->confirm_creation = true;
        }        
    }
    
    public function viewTraits($value)
    {
        return redirect()->route('traits.of.unit',[
            'id' => $value,
        ]);   
    }

    public function removeUnit()
    {

    }

    public function editUnit($value)
    {
        return redirect()->route('trait_unit.form',[
            'trait_unit' => $value,
        ]);   
    }


    public function render()
    {        
        $trait_units = TraitUnit::query()->paginate($this->per_page); 
        return view('livewire.trait-unit-list',['trait_units' => $trait_units]);
    }

}