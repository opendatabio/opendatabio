<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\ODBTrait;
use App\Models\Form;
use App\Models\Dataset;
use App\Models\Measurement;

use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;
use Auth;
use DB;
use Lang;

class FormsEdit extends Component
{

    public $form;
    public $notes;
    public $trait_id;    
    public $traits_list = [];    
    public $dataset_id;    
    public $datasets_list = [];    
    public $traits_order = [];  
    public $name;
    public $form_user;
    public $extra_validations=[];
    public $measured_type;
    public $objects_list=[];
    public $object_order;
    public $object_order_list=[];

    
    public function mount($form_id=null) : void {
        
        $this->form = $form_id ? Form::find($form_id) : null;
        $this->form_user = Auth::user();
        $this->extra_validations =[
            'standard',
            'greaterorequal_than_current',
            'greater_than_current',   
            'same_as_current',         
        ];        

        $this->objects_list = ODBTrait::FORM_OBJECT_TYPES;

        if ($this->form) {
            $form = $this->form;        
            $this->notes =  $form->notes;
            $this->name = $form->name;
            $this->form_user = $form->user;
            $metadata = json_decode($form->metadata,true);
            $this->measured_type = isset($form->measured_type) ? $form->measured_type : null;
            if (isset($metadata['traits'])) {
                $this->traits_list = $metadata['traits'];
            } 
            if (isset($metadata['datasets'])) {
                $this->datasets_list = $metadata['datasets'];
            } 
        }
        if ($this->traits_list) {
            $traits_order = array_keys($this->traits_list);
            $traits_order = array_combine($traits_order,$traits_order);
            $this->traits_order = $traits_order;     
        }
    }

    public function rules()
    {
        return [            
            'form_user' => 'required',
            'name' => 'required|string|min:15|max:100',
            'traits_list' => ['required','array',function($attribute, $value, $fail) {
                /* validate order */
                $traits_list = $value;
                $errors=[];
                $traits_in_form = collect($traits_list)->pluck('id')->toArray();   
                foreach($traits_list as $key => $trait) {
                    /* test trait validation */        
                    $odbtrait = ODBTrait::find($trait['id']);
                    //if trait has a parent trait, a measurement for it must exist
                    // and it also must be previously placed
                    
                    if ($odbtrait->parent_id) {     
                        $parent_id = $odbtrait->parent_id;
                        if (!in_array($parent_id,$traits_in_form)) {
                            $parent_name = ODBTrait::find($parent_id)->export_name;
                            $errors[] = $trait['export_name']." depends on trait ".$parent_name.", which must be included in form and BEFORE it".$trait['export_name'];
                        } else {
                            $parent_key = array_search($parent_id,$traits_in_form);
                            if ($parent_key>$key) {
                                $parent_name = $traits_list[$parent_key]['export_name'];
                                $errors[] = $trait['export_name']." position in form must be AFTER ".$parent_name;
                            }
                        }
                    }
                }
                if (count($errors)>0) {
                    return $fail(implode("<br>",$errors));
                }
                return null;
            }
            ],
            'measured_type' => ['required', function($attribute, $value, $fail) {
                $errors = [];
                $traits_list = $this->traits_list;

                foreach($traits_list as $key => $trait) {
                    $odbtrait = ODBTrait::find($trait['id']);
                    $teste = !$odbtrait->valid_type( $value);
                    if ($teste)
                    {
                        $errors[] = Lang::get('messages.invalid_trait_type_error',[
                            'export_name' => $odbtrait->export_name]);
                    }    
                }
                if (count($errors)>0) {
                    return $fail(implode("<br>",$errors));
                }
                return null;
            }],
        ];
    }

    public function updatedName()
    {
        $name = $this->name;
        $name = str_replace("  "," ",$name);
        $name = ucwords(mb_strtolower($this->name));
        $this->name= $name;
    }



    public function submit()
    {
        //validation
        $this->validate();
        $form = [
            'name' => $this->name,
            'notes' => $this->notes,
            'user_id' => $this->form_user->id,
            'measured_type' => $this->measured_type,            
        ];
        $metadata = [ 
            'traits' => $this->traits_list,
            'datasets' => $this->datasets_list,
        ];
        if ($this->form) {
          $agd = $this->form;
          $agd->update($form); 
        } else {
          $agd = Form::create($form);
        }
        $agd->metadata = $metadata;
        $agd->save();         

        return $this->redirectRoute('forms.show',['form_id' => $agd->id]);

    }


    protected $listeners = [
        'mergeChildTrait',  
        'mergeChildDataset',    
    ];

    public function mergeChildDataset($return_array)
    {
        $this->datasets_list = $return_array;
        $this->emit('clearBar');
    }
    
    public function extractFromDataset()
    {
        $dts_ids = collect($this->datasets_list)->pluck('id')->toArray();
        $traits = Measurement::withoutGlobalScopes()->whereIn('dataset_id',$dts_ids)->distinct( 'trait_id')->pluck('trait_id')->toArray();
        $objects = DB::table('measurements')->whereIn('dataset_id',$dts_ids)->selectRaw('measured_type, count(*) as count')->groupBy('measured_type');
        $objects = $objects->get();
        if (count($objects)) {
            $this->measured_type=$objects->sortByDesc('count')->first()->measured_type;
        }
        $traits_list = $this->traits_list;
        $count = $traits_list ? count($traits_list) : 0;
        $trait = ODBTrait::whereIn('id',$traits)->get()->map(function($a){
            return [
                'id' => $a->id,
                'name' => $a->name,   
                'export_name' => $a->export_name, 
                'raw_link' => $a->rawLink(),
                'mandatory' => false,
                'validation' => 'standard',
                'custom_validation' => in_array($a->type,[ODBTrait::QUANT_INTEGER,ODBTrait::QUANT_REAL]),
                'type' => $a->type,
                'rangeDisplay' => $a->rangeDisplay,
                'trait_unit' => ($a->trait_unit ? $a->trait_unit->unit : null),
            ];
        });    
        if ($count>0) {
            $traits_list = collect($traits_list)->merge($trait)->unique()->toArray();
        } else {
            $traits_list = $trait->toArray();
        }
        $this->traits_list = $traits_list;
        $traits_order = array_keys($traits_list);
        $traits_order = array_combine($traits_order,$traits_order);
        $this->traits_order = $traits_order;     
    }    

    public function mergeChildTrait($return_id)
    {
        $traits_list = $this->traits_list;
        $count = $traits_list ? count($traits_list) : 0;
        $a = ODBTrait::find($return_id);    
        $trait = collect([[
                'id' => $a->id,
                'name' => $a->name,   
                'export_name' => $a->export_name,
                'raw_link' => $a->rawLink(), 
                'mandatory' => false,
                'validation' => 'standard',
                'custom_validation' => in_array($a->type,[ODBTrait::QUANT_INTEGER,ODBTrait::QUANT_REAL]),
                'type' => $a->type,
                'rangeDisplay' => $a->rangeDisplay,
                'trait_unit' => ($a->trait_unit ? $a->trait_unit->unit : null),         
        ]]);
        if ($count>0) {
            $traits_list = collect($traits_list)->merge($trait)->unique()->toArray();
        } else {
            $traits_list = $trait->toArray();
        }
        $this->traits_list = $traits_list;
        $traits_order = array_keys($traits_list);
        $traits_order = array_combine($traits_order,$traits_order);
        $this->traits_order = $traits_order;     
        $this->trait_id = null;
        $this->emit('clearBar');
    }

    public function changeTraitOrder($fromIndex) : void {
        $traits_list = $this->traits_list;
        $traits_order = $this->traits_order;
        $toIndex = $traits_order[$fromIndex];      
        if ($fromIndex!=$toIndex) {  
            $this->moveArrayElement($traits_list,(int) $fromIndex,(int) $toIndex);
            $this->traits_list = $traits_list;     
            $traits_order = array_keys($traits_list);
            $traits_order = array_combine($traits_order,$traits_order);
            $this->traits_order = $traits_order;     
        }
    }

    public function removeTrait($index) : void {
        $traits_list = $this->traits_list;     
        unset($traits_list[$index]);
        if (count($traits_list)) {
            $this->traits_list = array_values($traits_list);   
            $traits_order = array_keys($this->traits_list);
            $traits_order = array_combine($traits_order,$traits_order);
            $this->traits_order = $traits_order;              
        } else {
            $this->traits_order = [];
            $this->traits_list = [];
        }        
    }

    public function moveArrayElement(&$array, $fromIndex, $toIndex) {
        $out = array_splice($array, $fromIndex, 1);
        array_splice($array, $toIndex, 0, $out);
    }

    public function render()
    {
        return view('livewire.forms-edit');
    }
}
