<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\BibReference;
use App\Models\ODBTrait;
use App\Models\Language;
use App\Models\Measurement;
use App\Models\MeasurementCategory;
use App\Models\TraitCategory;
use App\Models\TraitUnit;
use App\Models\Tag;

use Illuminate\Http\Request;


use CodeInc\StripAccents\StripAccents;
use Illuminate\Support\Arr;

use Lang;
use Auth;

class ODBTraitForm extends Component
{

    public $export_name;
    public $type;
    public $odbtrait;
    public $trait_types=[];
    public $objects=[];
    public $objects_list=[];
    public $canchange=true;
    public $current_objects=[];
    public $link_type;  
    public $unit;
    public $trait_unit_id;
    public $new_unit;
    public $units_list=[];
    public $add_unit=false;
    public $range_min;
    public $range_max;
    public $value_length;
    public $is_spectral=false;
    public $is_numeric=false;
    public $is_categorical=false;
    public $name=[];
    public $description=[];

    public $add_category=false;
    public $editing_category=false;
    public $categories=[];
    public $categories_order=[];
    public $cat_name=[];
    public $cat_description=[];

    public $methods=[];
    public $languages=[];
    public $current_locale=1;

    public $notes;
    public $parent_id;
    public $parent_name;

    public $bibreferences=[];
    public $tags=[];

    public function mount($odbtrait=null)
    {
        $this->languages = Language::all();
        $loc = config('app.locale');
        $this->current_locale = Language::where('code',$loc)->get()->first()->id;
        $this->objects_list = ODBTrait::OBJECT_TYPES;
        $this->trait_types = ODBTrait::TRAIT_TYPES;
        $this->units_list = TraitUnit::get()->map(function($u){
            return [
                'id' => $u->id,
                'symbol' => $u->unit,
                'name' => $u->name,
            ];
        })->sortBy('symbol')->values()->toArray();
        if ($odbtrait){
            $this->odbtrait = ODBTrait::find($odbtrait);
            $this->export_name = $this->odbtrait->export_name;
            $this->type = $this->odbtrait->type;
            $this->objects = $this->odbtrait->object_types()->pluck('object_type')->toArray();        
            $this->current_objects = $this->objects;    
            $this->canchange = $this->odbtrait->measurements()->withoutGlobalScopes()->count()==0;
            $this->range_min = $this->odbtrait->range_min;
            $this->range_max = $this->odbtrait->range_max;
            $this->value_length = $this->odbtrait->value_length;
            $this->parent_id = $this->odbtrait->parent_id;
            $this->parent_name = $this->parent_id ? ODBTrait::find($this->parent_id)->rawLink() : null;       

            if ($this->odbtrait->bibreferences->count()) {                
                $this->bibreferences = $this->odbtrait->bibreferences()->get()->map(function($bb){
                    return [
                        'id' => $bb->id,
                        'label' => $bb->rawLink(),
                    ];
                })->toArray();
            };
            
            if ($this->odbtrait->tags->count()) {
                $this->tags = $this->odbtrait->tags->map(function($tg){
                    return [
                        'id' => $tg->id,
                        'label' => $tg->rawLink(),
                    ];
                })->toArray();
            };
            
            $this->is_spectral = $this->odbtrait->type==ODBTrait::SPECTRAL;
            $this->is_numeric = in_array($this->odbtrait->type,[ODBTrait::QUANT_INTEGER,ODBTrait::QUANT_REAL]);
            $this->is_categorical = in_array($this->odbtrait->type,ODBTrait::CATEGORICAL_TRAITS);
            $this->value_length = $this->odbtrait->value_length;
            $this->notes = $this->odbtrait->notes;
            $name = $this->odbtrait->translations()->where('translation_type',0)->pluck('translation','language_id')->toArray();
            $description = $this->odbtrait->translations()->where('translation_type',1)->pluck('translation','language_id')->toArray();
            $this->name = $name;
            $this->description = $description;
            $this->trait_unit_id = $this->odbtrait->trait_unit_id;
            $this->unit = $this->odbtrait->trait_unit_id ? $this->odbtrait->trait_unit->unit : null;
            // categories

            if ($this->is_categorical) {
                $categories = $this->odbtrait->categories->map(function($cat) {
                    $names = $cat->translations()->where('translation_type',0)->pluck('translation','language_id')->toArray();
                    $descriptions = $cat->translations()->where('translation_type',1)->pluck('translation','language_id')->toArray();
                    return [
                        'name' => $names,
                        'description' => $descriptions,
                        'category_id' => $cat->id,       
                        'rank' => $cat->rank,        
                    ];
                });
                $categories = $categories->sortBy('rank')->values()->toArray();
                $this->categories = $categories;
                $categories_order = array_keys($this->categories);
                $categories_order = array_combine($categories_order,$categories_order);
                $this->categories_order = $categories_order;     
            }
        }
       
    }

    public function updatedType()
    {
        $this->is_spectral = $this->type==ODBTrait::SPECTRAL;
        $this->is_numeric = in_array($this->type,[ODBTrait::QUANT_INTEGER,ODBTrait::QUANT_REAL]);
        $this->is_categorical = in_array($this->type,ODBTrait::CATEGORICAL_TRAITS);
    }

    public function addCategory()
    {
        $this->add_category=true;
    }


    public function messages()
    {
        return [
            'export_name' => ":attribute é obrigatório e precisa ser UNICO dentre todas as variáveis cadastradas na base. Não pode ter acentos nem espaços em branco. E precisa ser informativo e ter, no mínimo, 6 caracteres",
            'name' => ":attribute é obrigatório e em todos os idiomas e não pode ser igual entre eles. Precisa ser mais informativo que export_name.",
            'description' => ":attribute é obrigatório e em todos os idiomas e não pode ser igual entre eles. Descrição não pode ser igual a nome",
            
            'categories' => 'Precisa cadastrar categorias, que precisam ter pelo menos o nome em todos os idiomas. Descrições opcional',
        ];
    }
 
    /* this validates upon update the input fields in the blade*/
    public function updated($propertyName)
    {        
        $this->validateOnly($propertyName);
    }

    public function updatedExportName()
    {        
        $export_name = StripAccents::strip( (string) $this->export_name);
        $export_name = ucfirst($export_name);
        $export_name = trim(str_replace(" ","",$export_name));        
        $this->export_name = $export_name;        
    }

    public function rules()
    {
        $id = $this->odbtrait ? $this->odbtrait->id : null;
        $name = $this->name;
        return [            
            'name' => ['required','array',function($attribute, $value, $fail) {
                $langs = Language::pluck('id')->toArray();
                $values = array_unique(array_filter($value));
                $count = count($values);
                $keys = array_keys($value);  
                $keys_are_lang = count(array_diff($keys,$langs))==0;

                //name must be different than exportName a longer
                $en = $this->export_name;
                $nlen = collect($value)->map(function($f) use($en) { 
                    $t1 = mb_strtolower($en)!==mb_strtolower($f);
                    $t2 =  strlen($f)>strlen($en);
                    return $t1 and $t2;
                })->sum(); 

                if ($count==count($langs) and $keys_are_lang and $nlen==count($value))
                {
                  return null;
                } else {
                  return $fail("");
                }
                }
            ],
            'description' => ['required','array',function($attribute, $value, $fail) use($name) {
                $langs = Language::pluck('id')->toArray();
                $values = array_unique(array_filter($value));
                $count = count($values);
                $keys = array_keys($value);  
                $keys_are_lang = count(array_diff($keys,$langs))==0;

                if ($count==count($langs) and $keys_are_lang)
                {
                  $comp = count(array_diff($value,$name))==count($value);
                  if($comp){            
                    return null;
                  }
                } 
                return $fail("");
            }
            ],
            'export_name' => ['required','string','unique:traits,export_name,'.$id,
                function($attribute, $value, $fail) {
                    $export_name = str_replace(" ","",$value);
                    $export_name = trim(StripAccents::strip( (string) $export_name));
                    preg_match('/^([a-zA-Z0-9]{1}?[a-zA-Z0-9|-|_|.]+)*$/', $export_name, $output_array);
                    $nchar =  strlen($export_name);
                    if ($output_array) {
                        if ($output_array[0]==$export_name and $nchar>=6) {
                        return null;
                        }
                    }
                    return  $fail("");                
                    }
            ],
            'categories' => ['array','required_if:type,2,3,4', function($attribute, $value, $fail) {
                $langs = Language::pluck('id')->toArray();
                $names = collect($value)->map(function($v)use($langs){                    
                    $name = $v['name'];
                    $keys = array_keys($name);
                    $n = count(array_unique(array_filter($name)));
                    $keys_are_lang = count(array_diff($keys,$langs))==0;
                    if ($n==count($langs) and $keys_are_lang) {
                        return 1;
                    } else {
                        return 0;
                    }
                })->toArray();
                $nvalues = array_sum($names);
                $unique_values = array_unique(Arr::flatten(collect($value)->map(function($v) {
                    $name = array_values($v['name']);
                    return array_unique($name);
                })->toArray()));
                $nmust = count($langs)*count($value);
                if ($nmust==count($unique_values) and $nvalues==count($value))
                {
                    return null;
                } else {
                    return $fail("");
                }
                }
            ],
            'type' => 'required|integer',
            'objects' => ['required','array',function($attribute, $value, $fail) {
                $errors=0;
                foreach ($value as $v) {
                    if (!in_array($v,ODBTrait::OBJECT_TYPES)) {
                        $errors++;
                    }
                }
                if ($errors>0 or count($value)==0) {
                    return $fail('');
                }
                return null;
            }],
            'trait_unit_id' => 'required_if:type,0,1,8',           
            'link_type' => 'required_if:type,7',
            'value_length' => 'required_if:type,8',
            'range_min' => ['required_if:type,0,1,8','required_with:range_max',function($attribute, $value, $fail) {
                $range_max = $this->range_max;
                if ($this->odbtrait and $this->type!=8) {
                    if ($value==0) {
                        return $fail("Obrigatório e precisa ser maior que 0");
                    }
                    $field = $this->type==ODBTrait::QUANT_INTEGER ? "value_i" : "value";
                    $min_value = Measurement::withoutGlobalScopes()->where('trait_id',$this->odbtrait->id)->min($field);
                    if ($value>$min_value and $min_value!==null) {
                        $val = $min_value+0;
                        return $fail("Obrigatório para o tipo de trait selecionado, ou porque você informou range_max. Precisa ser maior que ".$val);
                    }
                }
                if ($value>=$range_max and $range_max!==null)
                {
                    return $fail("Precisa ser menor que range_max: ".$range_max);
                }
                return null;
            }
            ],
            'range_max' => ['required_if:type,0,1,8','required_with:range_min',function($attribute, $value, $fail) {
                $range_min = $this->range_min;
                if ($this->odbtrait and $this->type!=8) {
                    $field = $this->type==ODBTrait::QUANT_INTEGER ? "value_i" : "value";
                    $max_value = Measurement::withoutGlobalScopes()->where('trait_id',$this->odbtrait->id)->max($field);
                    if ($value<$max_value and $max_value!==null) {
                        $val = $max_value;
                        return $fail("Obrigatório para o tipo de trait selecionado. Precisa ser maior que ".$val);
                    }
                }
                if ($value<=$range_min and $range_min!==null) {
                    return $fail("Precisa ser maior que range_min: ".$range_min);
                }
                return null;
                }
            ],
        ];
    }
   
    public function submit()
    {

        $this->validate();
        
        if (!Auth::user()->can('create', ODBTrait::class))
        {
            return null;
        };
        
        $odbtrait = [
            'export_name' => $this->export_name,
            'type' => $this->type,            
            'notes' => $this->notes,
            'parent_id' => $this->parent_id,
        ];
        if ($this->odbtrait)
        {
            $agd = $this->odbtrait;
            $agd->update($odbtrait);  
            $agd->bibreferences()->detach();
            $agd->tags()->detach();
            $agd->save();
        } else {
            $agd = ODBTrait::create($odbtrait);
            $agd->save();
        }
        $request = [
            'trait_unit_id' => $this->trait_unit_id,
            'range_max' => $this->range_max,
            'range_min' => $this->range_min,
            'type' => $this->type,
            'value_length' => $this->value_length,
            'name' => $this->name,
            'description' => $this->description,
            'objects' =>  $this->objects,
            'categories' => $this->categories,
        ];
        
        $saverequest = new Request;
        $saverequest->merge($request);
        $agd->setFieldsFromRequestNew($saverequest);

        if ($this->bibreferences) {
            $bibreferences = collect($this->bibreferences)->pluck('id')->toArray();
            $agd->bibreferences()->attach($bibreferences);
        }
        if ($this->tags) {
            $tags = collect($this->tags)->pluck('id')->toArray();
            $agd->tags()->attach($tags);
        }
        $agd->save();
       

        return $this->redirectRoute('traits.show',
            ['trait'=> $agd->id,
            'status' => Lang::get('messages.stored'),
        ]);
        
    }





    //CATEGORIES
    public function changeCategoryOrder($fromIndex) : void {
        $categories = $this->categories;
        $categories_order = $this->categories_order;
        $toIndex = $categories_order[$fromIndex];      
        if ($fromIndex!=$toIndex) {  
            $this->moveArrayElement($categories,(int) $fromIndex,(int) $toIndex);
            $this->categories = $categories;     
            $categories_order = array_keys($categories);
            $categories_order = array_combine($categories_order,$categories_order);
            $this->categories_order = $categories_order;     
        }
    }

    public function moveArrayElement(&$array, $fromIndex, $toIndex) {
        $out = array_splice($array, $fromIndex, 1);
        array_splice($array, $toIndex, 0, $out);
    }
    
    public function removeCategory($index) : void {
        $categories = $this->categories;     
        $exists = $categories[$index]['category_id'];
        $has_measurements=false;
        if ($exists) {
            $has_measurements = MeasurementCategory::where('category_id',$exists)->count()>0;
            if ($has_measurements) {
                session()->flash('categories',Lang::get('messages.used_category'));
                return;
            }
        }
        if (!$has_measurements) {
            unset($categories[$index]);
            if (count($categories)) {
                $this->categories = array_values($categories);   
                $categories_order = array_keys($this->categories);
                $categories_order = array_combine($categories_order,$categories_order);
                $this->categories_order = $categories_order;              
            } else {
                $this->categories_order = [];
                $this->categories = [];
            }        
        }
    }
    
    public function editCategory($index) : void {
        $categories = $this->categories;     
        $cat = $categories[$index];
        $this->cat_name = $cat['name'];
        $this->cat_description = $cat['description'];
        $this->add_category = true;
        $this->editing_category = $index+1;
    }
   
    public function updatedCatName()
    {
        $cat_name = $this->cat_name;
        $cats = [];
        foreach($cat_name as $k => $cat)
        {
            $cats[$k] = ucfirst(mb_strtolower($cat));
        }
        $this->cat_name = $cats;
    }



    public function validateCategory()
    {
        $cat_name = array_filter($this->cat_name);
        $cat_description = array_filter($this->cat_description);
        $langs = $this->languages->count();
        $valid_names = array_unique($cat_name);
        $valid_descriptions = array_unique($cat_description);
        /* multiple languages required for name */
        if ((count($cat_name)<$langs OR count($valid_names)==1) OR count($valid_descriptions)<=1) {
            session()->flash('cat_error',Lang::get('messages.invalid_category_langs'));
            return false;
        }
        $categories = $this->categories;
        if ($this->editing_category) {
            $idx = $this->editing_category-1;
            $categories[$idx]['name'] = $this->cat_name;
            $categories[$idx]['description'] = $this->cat_description;
        } else {
            $categories[] = [
                'name' => $this->cat_name,
                'description' => $this->cat_description,
                'category_id' => null,            
            ];
        }
        $error = 0;
        $lange =[];
        foreach($this->languages as $lang) {
            $all_names = collect($categories)->map(function($cat)use($lang){
                $name = mb_strtolower($cat['name'][$lang->id]);
                $name =StripAccents::strip( (string) $name); 
                return $name;
            })->toArray();  
            $n1 = count($all_names);
            $n2 = count(array_unique($all_names));
            if ($n1!=$n2) {
                $error++;
                $lange[] = $lang->id;
            }
        }
        //dd([$categories,$lange]);

        if ($error>0) {
         session()->flash('cat_error',Lang::get('messages.duplicated_category'));
          return false;
        }
        return true;
    }

    public function cancelCategory(): void
    {
        $this->cat_name = [];
        $this->cat_description = [];
        $this->add_category = false;
        $this->editing_category = null;
    }

    public function saveCategory() : void {
        
        if (!$this->validateCategory()) {
            return;
        }
        $categories = $this->categories;        
        if ($this->editing_category) {
            $idx = $this->editing_category-1;
            $categories[$idx]['name'] = $this->cat_name;
            $categories[$idx]['description'] = $this->cat_description;
        } else {
            $categories[] = [
                'name' => $this->cat_name,
                'description' => $this->cat_description,
                'category_id' => null,            
            ];
        }
        $this->categories = $categories;
        $this->cat_name = [];
        $this->cat_description = [];
        $this->add_category = false;
        $this->editing_category = null;
        
        $categories_order = array_keys($categories);
        $categories_order = array_combine($categories_order,$categories_order);
        $this->categories_order = $categories_order; 
    }
    
    protected $listeners = [
        'mergeChildTrait',     
        'mergeChildBib', 
        'mergeChildTag'
    ];

    public function mergeChildTrait($return_id)
    {
        $this->parent_id = $return_id;
        $this->parent_name = ODBTrait::find($return_id)->rawLink();       
        $this->emit('clearBar');
    }    

    public function mergeChildBib($return_id)
    {
        $bib = BibReference::find($return_id);  
        $bibreferences = $this->bibreferences;     
        $bibreferences[] = [
            'id' => $bib->id,
            'label' => $bib->rawLink(),
        ];        
        $bibreferences = collect($bibreferences)->unique()->values()->toArray();
        $this->bibreferences = $bibreferences;
        $this->emit('clearBar');
    } 
    public function removeBibReference($index) : void {
        $bibreferences = $this->bibreferences;    
        unset($bibreferences[$index]);
        $this->bibreferences = $bibreferences;        
    }

    public function mergeChildTag($return_id)
    {
        $tag = Tag::find($return_id);       
        $tags = $this->tags;     
        $tags[] = [
            'id' => $tag->id,
            'label' => $tag->rawLink(),
        ];       
        $tags = collect($tags)->unique()->values()->toArray(); 
        $this->tags = $tags;
        $this->emit('clearBar');
    } 

    public function removeTag($index) : void {
        $tags = $this->tags;    
        unset($tags[$index]);
        $this->tags = $tags;        
    }



    public function render()
    {
        return view('livewire.odbtrait-form');
    }
}
