<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\BibReference;
use App\Models\ODBTrait;
use App\Models\TraitCategory;
use App\Models\Measurement;
use App\Models\Form;
use App\Models\FormTask;
use App\Models\FormTaskObject;
use App\Models\Person;
use App\Models\Location;

use App\Models\MeasurementCategory;

use Auth;
use Lang;

class FormsFill extends Component
{

    /*
        this contain the definitions to use in the fill
    */
    public $form;
    public $form_task;

    /* this will collect the measurements */
    public $measured_id;    
    public $measured_type;    
    public $measured_name;    
    public $measurements =[];    
    public $short_measured_type;
    public $category=[];
    /* repeatable variables */
    public $dataset_id;
    public $user_datasets=[];

    public $authors=[];  
    public $date;
    public $date_year;
    public $date_month;
    public $date_day;


    public $location_id;
    public $location_name;

    public $allow_duplicated;
    public $confirm_duplicated=false;
    public $existing_dups=0;
    public $duplications_errors=[];
    public $confirm_missing=[];
    public $from_form_task=false;


    public function mount($form_id=null,$form_task_id=null)
    {
        $this->form_task = $form_task_id ? FormTask::find($form_task_id) : null;
        $this->form = $form_id ? Form::find($form_id) : ($form_task_id ? $this->form_task->form : null) ;
        $form = $this->form;        
        $this->notes =  $form->notes;
        $this->name = $form->name;
        $this->form_user = $form->user;
        $metadata = json_decode($form->metadata,true);
        $this->measured_type = isset($form->measured_type) ? $form->measured_type : null;
        $this->short_measured_type = isset($this->measured_type) ? str_ireplace("App\Models\\","",$this->measured_type) : null;
        if (isset($metadata['traits'])) {
            $traits_list = $metadata['traits'];            
            foreach ($traits_list as $order => $trait) {
                $odbtrait = ODBTrait::find($trait['id']);
                $categories = null;
                if ($odbtrait->categories)
                {
                    $categories = $odbtrait->categories->map(function($cat){
                        return [
                            'id' => $cat->id,
                            'label' => $cat->rank." ".$cat->name,
                        ];
                    })->sortBy('label',SORT_NATURAL|SORT_FLAG_CASE)->values()
                    ->toArray();
                }
                $trait = array_merge($trait,[
                    'value' => null,
                    'categories' => $categories,
                    'category' => null,
                    'selected_categories' => [],
                    'notes' => null,
                    'bibreference_id' => null,
                    'bibreference_name' => null,
                    'parent_id' => null,
                    'parent_name' => null,
                    'allow_missing' => false,
                ]
                );
                $traits_list[$order] = $trait;
            }
            $this->traits_list = $traits_list;            
        } 
        $objs_count = 0;
        if ($this->form_task) {
            $this->from_form_task=true;
            $objs = $this->form_task->taskObjects()->where('status',1)->orderByDesc('updated_at');
            $objs_count = $objs->count();
            if ($objs_count>0) {
                $objs = $objs->get()->first();
                //$this->dataset_id = $objs->dataset_id;
                $metadata = json_decode($objs->metadata,true);
                $this->dataset_id = $metadata['dataset_id'];
                $this->authors = $metadata['authors'];
                $this->date_day = $metadata['date_day'];
                $this->date_year = $metadata['date_year'];
                $this->date_month = $metadata['date_month'];
            }
        } 
        if ($objs_count==0) {
            $this->date_year = (int) today()->format("Y");
            $this->date_month = (int) today()->format("m");
            $this->date_day = (int) today()->format("d");    
            $this->date = today()->format("Y-m-d");    
        }
        $dataset_users = Auth::user()->editableDatasets();
        if ($dataset_users!==null) {            
            $this->user_datasets = $dataset_users->get()->map(function($d) { 
                return [
                    'id' => $d->id,
                    'label' => $d->name,
                ];})->sortBy('label',SORT_NATURAL|SORT_FLAG_CASE)->values()->toArray();
        }
    }

    public function allowMissing()
    {
        $traits_list = $this->traits_list;
        foreach($this->confirm_missing as $idx)
        {
            $traits_list[$idx]['allow_missing'] = true;
        }
        $this->traits_list = $traits_list;
        $this->confirm_missing=[];
    }
    
    public function rules()
    {
        return [                   
            'measured_id' => 'required|integer',
            'measured_type' => ['required','string'],
            'date_year' => ['required','integer',
            function($attribute, $value, $fail) {
                $colldate = [$this->date_month, $this->date_day, $value];
                if (!Measurement::checkDate($colldate)) {
                    return $fail(Lang::get('messages.invalid_date_error'));
                }
                // measurement date must be in the past or today
                if (!Measurement::beforeOrSimilar($colldate, date('Y-m-d'))) {
                    return $fail(Lang::get('messages.date_future_error'));
                }
                return null;
            }
            ],
            'dataset_id' => 'required|integer|exists:datasets,id',
            'authors' => 'required|array',  
            'traits_list' => ['required','array',function($attribute, $value, $fail) {
                $traits_list = $value;
                $errors=[];
                $traits_in_form = collect($traits_list)->pluck('id')->toArray();   
                $extra_validations_errors=[];
                foreach($traits_list as $idx => $trait) {
                    /* test trait validation */        
                    $trait_value = $trait['value'];
                    $mandatory = $trait['mandatory'];
                    if (!$trait_value and !$mandatory) {
                        continue;
                    }
                    $odbtrait = ODBTrait::find($trait['id']);
                    $teste = !$odbtrait->valid_type($this->measured_type);
                    if ($teste)
                    {
                        $errors[] = Lang::get('messages.invalid_trait_type_error',[
                            'export_name' => $odbtrait->export_name]);
                    }
                    //must test for parent trait requirement
                    //if trait has a parent trait, a measurement for it must exist
                    if ($odbtrait->parent_id) {     
                        $parent_id = $odbtrait->parent_id;
                        $tr = collect($traits_list)->filter(function($t) use($parent_id){
                            return $t['id']==$parent_id;
                        });                
                        $has_measurement = Measurement::withoutGlobalScopes()->
                        where('trait_id',$odbtrait->parent_id);
                        $has_measurement = $has_measurement->where('measured_id',$this->measured_id);
                        $has_measurement = $has_measurement->where('measured_type',$this->measured_type);
                        $colldate = [$this->date_month, $this->date_day, $this->date_year];
                        if (Measurement::checkDate($colldate)) {
                            $year =str_pad((int) $this->date_year, 4, '0', STR_PAD_LEFT);
                            $month = str_pad((int) $this->date_month, 2, '0', STR_PAD_LEFT);
                            $day = str_pad((int) $this->date_day, 2, '0', STR_PAD_LEFT);
                            $dt = $year."-".$month."-".$day;
                            $has_measurement = $has_measurement->where('date',$dt);
                        }
                        if ($has_measurement->count()==0 and count($traits_in_form)==0) {
                            $parent = $odbtrait->parentTrait->export_name;
                            $errors[] = 'Storing a measurement for Trait '.$odbtrait->export_name.' requires having a measurement for trait '.$parent.' on the same date for this object and '.$parent.' is not included in the form';
                        } elseif (count($tr)) {
                            $tr = collect($tr)->first();
                            $miss_parent_value = $tr["value"];
                            if (!$miss_parent_value) {
                                $parent = $tr["export_name"];
                                $errors[] = 'Storing a measurement for Trait '.$odbtrait->export_name.' requires having a measurement for trait '.$parent.' on the same date for this object. '.$parent.' is included in the form, but still has no value set.';
                            }
            
                        }
                    }        
                    if (!$trait_value and $mandatory and !$trait['allow_missing']) {
                        $this->confirm_missing[] = $idx;
                        $errors[] = 'Value is required for '.$trait['export_name'];
                    } elseif($trait_value) {
                        $valid = Measurement::valueValidation($trait_value,$odbtrait,null);
                        if (count($valid)>0) {
                            $errors = array_merge($errors,$valid);
                        }
                    }
                    if ($trait_value) {
                        $extra_validations_errors[]=$this->valueFormTaskValidation($trait_value,$odbtrait,$trait['validation'],$this->measured_id,$this->measured_type);
                    }
                }
                if (count($errors)>0) {
                    return $fail(implode("<br>",$errors));
                }
                $extra_validations_errors = array_filter($extra_validations_errors);
                if (count($extra_validations_errors)){
                    $msg = collect($extra_validations_errors)->map(function($e){
                        return implode("<br>",$e);
                    })->toArray();
                    $msg = implode("<br>",$msg);
                    return $fail($msg);
                }
                return null;
            }],          
            
        ];
    }

    public static function valueFormTaskValidation($value,$odbtrait,$extra_validation,$measured_id,$measured_type)
    {
        $messages=[];
        if (!$extra_validation or $extra_validation=='standard') {
          return [];
        }
         
        // Checks if integer variable is integer type
        $last_measurement = Measurement::where('trait_id',$odbtrait->id)->where('measured_id',$measured_id)->where('measured_type',$measured_type)->orderByDesc('date')->get();
        if ($last_measurement->count())
        {
            
          $old_value = $last_measurement->first()->rawValueActual;
          if ($extra_validation=="greaterorequal_than_current" and $value<=$old_value) 
          {
            $messages[] =  "Value ".$value." should be greater or equal than older value ".$old_value." for trait ".$odbtrait->export_name;            
          } elseif ($extra_validation=="greater_than_current" and $value<$old_value) 
            $messages[] =  "Value ".$value." should be greater than older value ".$old_value." for trait ".$odbtrait->export_name; 
          } elseif ($extra_validation=="same_as_current" and $value!=$old_value) 
          {
            $messages[] =  "Value ".$value." should be equal to older value".$old_value." for trait ".$odbtrait->export_name; 
        }                  
        return $messages;
    }

    public function submit()
    {
        $this->validate();

        $traits_list = $this->traits_list;
        /* filter empty values */
        $original_traits_list = $traits_list;
        $traits_list = collect($traits_list)->filter(function($f){
            if ($f['value']) {
                return true;
            } else {
                return false;
            }
        })->toArray();

        /* check for duplications */
        if (!$this->allow_duplicated) {
            $duplications_errors = [];        
            foreach($traits_list as $trait)
            {
                $record = [
                    'trait_id' => $trait['id'],
                    'measured_id'=> $this->measured_id,
                    'measured_type'=> $this->measured_type,
                    'dataset_id'=> $this->dataset_id,
                    'bibreference_id'=> $trait['bibreference_id'],
                    'notes'=> $trait['notes'],
                    'parent_id'=> $trait['parent_id'],
                    'location_id'=> $this->location_id,
                ];
            
                //check for duplicated measurements
                $registry = $record;
                $registry['value'] = $trait['value'];
                $registry['date_month'] = $this->date_month;
                $registry['date_day'] = $this->date_day;
                $registry['date_year'] = $this->date_year;
                $existing_dups  = Measurement::checkDuplicateMeasurement($registry);
                if ($existing_dups>0)         
                {  
                    $duplications_errors[] = $trait['export_name'].' has already '.$existing_dups.' other records for the same object, same date and same value';                
                }
            }
            if (count($duplications_errors))
            {
                $this->confirm_duplicated=true;
                session()->flash('duperrors',implode("<br>",$duplications_errors));
                return false;
            }
        }
        $stored_traits = [];
        foreach($traits_list as $k => $trait)
        {
            $record = [
                'trait_id' => $trait['id'],
                'measured_id'=> $this->measured_id,
                'measured_type'=> $this->measured_type,
                'dataset_id'=> $this->dataset_id,
                'bibreference_id'=> $trait['bibreference_id'],
                'notes'=> $trait['notes'],
                'parent_id'=> $trait['parent_id'],
                'location_id'=> $this->location_id,
            ];  
            $odbtrait = ODBTrait::find($trait['id']);
            if ($odbtrait->parent_id and !$record['parent_id']) {     
                $parent_id = $odbtrait->parent_id;
                $tr = collect($stored_traits)->filter(function($t) use($parent_id){
                    return $t['id']==$parent_id;
                });     
                if ($tr->count())  {
                    $tr = $tr->first();                    
                    $record['parent_id'] = $tr['measurement_id'];
                }
            }
            $measurement = new Measurement($record);            
            $measurement->setDate($this->date_month, $this->date_day, $this->date_year);
            $measurement->save();   
            $persons = collect($this->authors)->pluck('id')->toArray();             
            $measurement->collectors()->attach($persons);        
            $measurement->valueActual = $trait['value'];
            $measurement->save(); 
            $trait['measurement_id'] = $measurement->id;
            $stored_traits[] = $trait;
        }
        if ($this->form_task)
        {
            $ids = collect($stored_traits)->pluck('measurement_id')->toArray();    
            $form_task = $this->form_task;
            $form_task->progress = ($form_task->progress)+1;
            $form_task->save();

            $task_object = FormTaskObject::where('measured_id',$this->measured_id)->where('form_task_id',$this->form_task->id)->get()->first();            
            $task_object->status = 1;
            $metadata = [
                'measurement_ids' => $ids,
                'dataset_id' => $this->dataset_id,
                'authors' => $this->authors,
                'date_day' => $this->date_day,
                'date_year' => $this->date_year,
                'date_month' => $this->date_month,                
            ];            
            $task_object->metadata = $metadata;
            $task_object->save();

        }
        $msg = count($stored_traits)." medições foram armazenadas para o objecto ".$this->measured_name;
        /* clean form for a new entry */
        $clean_trait_list = [];
        foreach($original_traits_list as $trait)
        {
            $trait['value'] = null;
            $trait['bibreference_id'] = null;
            $trait['category'] = null;
            $trait['notes'] = null;
            $trait['bibreference_name'] = null;            
            $trait['selected_categories'] = [];
            $clean_trait_list[] = $trait;            
        }
        $this->traits_list = $clean_trait_list;
        $this->measured_id = null;
        $this->measured_name=null;        
        session()->flash('stored',$msg);
    }

   
    /* this validates upon update the input fields in the blade*/
    public function updatedCategory($value, $key)
    {   
        $order = (int) $key;
        $cat_id = (int) $value;
        $traits_list =$this->traits_list;
        $trait = $traits_list[$order];
        if ($trait['type']==3) {
            $selected_categories = $trait['selected_categories'];
            $category = TraitCategory::find($cat_id);
            $idx = (string) $cat_id;
            $selected_categories[$idx] = [
                'id' => $category->id,
                'label' => $category->name,
            ];   
            $trait['value'] = collect($selected_categories)->pluck('id')->toArray();
            $trait['selected_categories'] = $selected_categories;
            $this->category[$order] = null;
            $traits_list[$order] = $trait;
            $this->traits_list = $traits_list;
            //$this->emit('$refresh');
        }
    }


    protected $listeners = [
        'mergeChildLocation', 
        'mergeChildBib', 
        'mergeChildAuthor',
        'mergeChildObject',
    ];

    public function mergeChildLocation($return_id)
    {
        $this->location_id = $return_id;
        $this->location_name = Location::find($return_id)->rawLink();       
        $this->emit('clearBar');
    }    


    public function mergeChildAuthor($return_array)
    {
        $this->authors = $return_array;
        $this->emit('clearBar');
    }

    
    public function mergeChildObject($return_id)
    {
        $this->measured_id = $return_id;
        $this->measured_name = app($this->measured_type)->find($return_id)->rawLink();       
        $this->emit('clearBar');
    }    

    public function mergeChildBib($return_id)
    {
        $order = $return_id['order'];
        $bibid = $return_id['return_id'];
        $traits_list = $this->traits_list;
        $traits_list[$order]['bibreference_id'] = $bibid;
        $traits_list[$order]['bibreference_name'] =  BibReference::find($bibid)->rawLink();        
        $this->traits_list = $traits_list;
        $this->emit('clearBar');
    } 


    public function removeCategory($order,$index)
    {
        $trait = $this->traits_list[$order];
        $categories = $trait['selected_categories'];
        if (isset($categories[$index])) {
            unset($categories[$index]);
            $trait['selected_categories'] = $categories;
            $trait['value'] = collect($trait['selected_categories'])->pluck('id')->toArray();
            $this->traits_list[$order] = $trait;
        }
    }

    public function render()
    {
        return view('livewire.forms-fill');
    }
}
