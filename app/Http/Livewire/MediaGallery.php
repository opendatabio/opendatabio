<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Media; 
use App\Models\Taxon; 
use App\Models\Individual; 
use App\Models\Tag; 
use App\Models\Voucher; 
use App\Models\Location; 
use App\Models\Project; 

use Lang;
use Auth;
use Storage;

class MediaGallery extends Component
{    
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $taxon_id;
    public $taxon_name;
    public $individual_id;
    public $individual_name;
    public $individual_search_tip;
    public $tag_id;
    public $tag_name;
    public $project_id;
    public $project_name;
    public $voucher_id;
    public $voucher_name;
    public $voucher_search_tip;
    public $location_id;
    public $location_name;
    public $per_page=12;

    public function mount($taxon_id=null,$individual_id=null,$tag_id=null,$voucher_id=null,$location_id=null,$project_id=null)
    {
        if($taxon_id) {
            $this->taxon_id = $taxon_id;
            $this->taxon_name = Taxon::find($taxon_id)->scientificName;
        } elseif($individual_id) {
            $this->individual_id = $individual_id;
            $this->individual_name = Individual::withoutGlobalScopes()->find($individual_id)->rawLink();
        } elseif($voucher_id) {
            $this->voucher_id = $voucher_id;
            $this->voucher_name = Voucher::withoutGlobalScopes()->find($voucher_id)->rawLink();            
        } elseif($location_id) {
            $this->location_id = $location_id;
            $this->location_name = Location::find($location_id)->rawLink();            
        } 
        if ($this->project_id) {
            $this->project_id = $project_id;
            $this->project_name = Project::find($project_id)->acronym;
        }
        if ($this->tag_id) {
            $this->tag_id = $tag_id;
            $this->tag_name = Tag::find($tag_id)->name;
        }  

    }


    public function edit($value)
    {
      if (Auth::user()->can('update',Media::find($value)))
      {
        return redirect()->route('media.edit',[
            'medium' => $value,
          ]);
      }
      session()->flash('errors',Lang::get('messages.unauthorized'));
      return;
    }

    public function show($value)
    {
        $media = Media::findOrFail($value);     
        if ($media->count()) {
            return redirect()->route('media.show',[
            'medium' => $value,
            ]);        
        }        
      session()->flash('errors',Lang::get('messages.unauthorized'));
      return;
    }


    public function SearchTip($value)
    {
      if ($value='individual') {
        $this->individual_search_tip = $this->individual_search_tip ? false : true;
      }
      if ($value='voucher') {
        $this->voucher_search_tip = $this->voucher_search_tip ? false : true;
      }
      return;
    }
    
     protected $listeners = [
        'mergeChildTaxon',
        'mergeChildIndividual',
        'mergeChildTag',
        'mergeChildVoucher',
        'mergeChildLocation',
        'mergeChildProject',

    ];


    public function mergeChildVoucher($return_id)
    {        
        $this->voucher_id = $return_id;
        $this->voucher_name = Voucher::withoutGlobalScopes()->find($return_id)->rawLink();       
        $this->emit('clearBar');
    }

    public function clearVoucher()
    {        
        $this->voucher_id = null;
        $this->voucher_name = null;
    }

    public function mergeChildLocation($return_id)
    {        
        $this->location_id = $return_id;
        $this->location_name = Location::find($return_id)->rawLink();       
        $this->emit('clearBar');
    }

    public function clearLocation()
    {        
        $this->location_id = null;
        $this->location_name = null;
    }

    public function mergeChildTag($return_id)
    {        
        $this->tag_id = $return_id;
        $this->tag_name = Tag::find($return_id)->name;        
        $this->emit('clearBar');
    }

    public function mergeChildProject($return_id)
    {        
        $this->project_id = $return_id;
        $this->project_name = Project::find($return_id)->acronym;
        $this->emit('clearBar');
    }

    public function clearProject()
    {        
        $this->project_id = null;
        $this->project_name = null;
    }



    public function clearTag()
    {        
        $this->tag_id = null;
        $this->tag_name = null;
    }



    public function mergeChildIndividual($return_id)
    {        
        $this->taxon_id = null;
        $this->taxon_name = null;
        $this->individual_id = $return_id;
        $this->individual_name = Individual::withoutGlobalScopes()->find($return_id)->rawLink();        
        $this->emit('clearBar');
    }

    public function clearIndividual()
    {        
        $this->individual_id = null;
        $this->individual_name = null;
    }

    public function mergeChildTaxon($return_id)
    {        
        $this->individual_id = null;
        $this->individual_name = null;
        $this->taxon_id = $return_id;
        $this->taxon_name = Taxon::find($return_id)->scientificName;
        $this->emit('clearBar');
    }

    public function clearTaxon()
    {        
        $this->taxon_id = null;
        $this->taxon_name = null;
    }


    public function render()
    {
        $media = Media::query();
        if($this->taxon_id) {
            $model = Taxon::findOrFail($this->taxon_id);
            //get all the media for taxon and its descendants
            $media = $model->mediaDescendantsAndSelf();            
        } elseif($this->individual_id) {
            $model = Individual::withoutGlobalScopes()->findOrFail($this->individual_id);
            $media = $model->media();            
        } elseif($this->voucher_id) {
            $model = Voucher::withoutGlobalScopes()->findOrFail($this->voucher_id);
            $media = $model->media();            
        } elseif($this->location_id) {
            $model = Location::findOrFail($this->location_id);
            $media = $model->media();                    
        }        
        if ($this->project_id) {
            $media = $media->where(function($q){
                $q->where('project_id',$this->project_id)->orWhereHas('dataset',function($dt)
                {
                    $dt->where('project_id',$this->project_id);
                });            
                });
        }    
        if ($this->tag_id) {
            $media = $media->whereHas('tags',function($q){
                    $q->where('tags.id',$this->tag_id);
            });
        }         
        $media = $media->paginate($this->per_page);          
        return view('livewire.media-gallery', ['media' => $media]);
    }


}
