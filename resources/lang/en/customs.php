<?php

return [
  'landing_page_welcome' => "Opendatabio-INPA - Amazonian Biodiversity Data",
  'landing_page_short' => "Biodiversity and ecological data repository - data management, curation and distribution",
  'footer_address' => ["Instituto Nacional de Pesquisas da Amazônia","Av. André Araújo 2936, Aleixo, 69060-001","Manaus, Amazonas, Brazil"],
  'footer_phone' => "+55 92 36433377",
  'facebook_url' => null,
  'instagram_url' => null,
  'twitter_url' => null,
];
