<?php
return [
    'App\Models\Location' => 'Localidade',
    'App\Models\Individual' => 'Indivíduo',
    'App\Models\Voucher' => 'Voucher',
    'App\Models\Taxon' => 'Taxon',
    'App\Models\Person' => 'Pessoa',
    'App\Models\Media' => 'Mídia',
    'Location' => 'Localidade',
    'Individual' => 'Indivíduo',
    'Voucher' => 'Voucher',
    'Taxon' => 'Taxon',
    'Person' => 'Pessoa',
    'Media' => 'Mídia',
];
