<?php

return [
  'landing_page_welcome' => "OpenDataBio-INPA - Dados de biodiversidade da Amazônia",
  'landing_page_short' => "Repositório de dados de biodiversidade e ecologia - manejo, curadoria e distribuição de dados",
  'footer_address' => ["Instituto Nacional de Pesquisas da Amazônia","Av. André Araújo 2936, Aleixo, 69060-001","Manaus, Amazonas, Brazil"],
  'footer_phone' => "+55 92 36433377",
  'facebook_url' => null,
  'instagram_url' => null,
  'twitter_url' => null,
];
