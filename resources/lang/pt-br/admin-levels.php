<?php
/* each País may have its  own ames */
return [
    'br' => [
        'adm_level.-1' => 'Mundo',
        'adm_level.2' => 'País',
        'adm_level.3' => 'Região',
        'adm_level.4' => 'Estado',
        'adm_level.5' => 'Meso Região',
        'adm_level.6' => 'Metropolitana Região',
        'adm_level.7' => 'Micro Região',
        'adm_level.8' => 'Município',
        'adm_level.9' => 'Distrito',
        'adm_level.10' => 'SubDistrito',
        'adm_level.11' => 'Admin 11',
        'adm_level.12' => 'Admin 12',
        'adm_level.13' => 'Admin 13',
        'adm_level.14' => 'Admin 14',
        'adm_level.15' => 'Admin 15',
        'adm_level.16' => 'Admin 16',
        'adm_level.99' => 'Unidade de Conservação',
        'adm_level.98' => 'Território Indígena',
        'adm_level.97' => 'Ambiental',
        'adm_level.100' => 'Parcela',
        'adm_level.101' => 'Transecto ou Trilha',
        'adm_level.999' => 'Ponto',
    ],
    'pe' => [
        'adm_level.-1' => 'Mundo',
        'adm_level.2' => 'País',
        'adm_level.3' => 'Admin 3',
        'adm_level.4' => 'Departamento',
        'adm_level.5' => 'Admin 5',
        'adm_level.6' => 'Província',
        'adm_level.7' => 'Admin 7',
        'adm_level.8' => 'Município',
        'adm_level.9' => 'Admin 9',
        'adm_level.10' => 'Comunidade',
        'adm_level.11' => 'Admin 11',
        'adm_level.12' => 'Admin 12',
        'adm_level.13' => 'Admin 13',
        'adm_level.14' => 'Admin 14',
        'adm_level.15' => 'Admin 15',
        'adm_level.16' => 'Admin 16',
        'adm_level.99' => 'Unidade de Conservação',
        'adm_level.98' => 'Território Indígena',
        'adm_level.97' => 'Ambiental',
        'adm_level.100' => 'Parcela',
        'adm_level.101' => 'Transecto ou Trilha',
        'adm_level.999' => 'Ponto',
    ],
    'ec' => [
        'adm_level.-1' => 'Mundo',
        'adm_level.2' => 'País',
        'adm_level.3' => 'Admin 3',
        'adm_level.4' => 'Província',
        'adm_level.5' => 'Admin 5',
        'adm_level.6' => 'Canton',
        'adm_level.7' => 'Área Urbana',
        'adm_level.8' => 'Parroquia Urbana',
        'adm_level.9' => 'Bairro Urbano - Comunidade Rural ',
        'adm_level.10' => 'Admin 10',
        'adm_level.11' => 'Admin 11',
        'adm_level.12' => 'Admin 12',
        'adm_level.13' => 'Admin 13',
        'adm_level.14' => 'Admin 14',
        'adm_level.15' => 'Admin 15',
        'adm_level.16' => 'Admin 16',
        'adm_level.99' => 'Unidade de Conservação',
        'adm_level.98' => 'Território Indígena',
        'adm_level.97' => 'Ambiental',
        'adm_level.100' => 'Parcela',
        'adm_level.101' => 'Transecto ou Trilha',
        'adm_level.999' => 'Ponto',
    ],
    'co' => [
        'adm_level.-1' => 'Mundo',
        'adm_level.2' => 'País',
        'adm_level.3' => 'Região Administrativa de Planejamento',
        'adm_level.4' => 'Departamento',
        'adm_level.5' => 'Província',
        'adm_level.6' => 'Município',
        'adm_level.7' => 'Área Urbana - Comunidade Rural',
        'adm_level.8' => 'Comunidade - Village',
        'adm_level.9' => 'Bairro Urbano - UPZs',
        'adm_level.10' => 'Bairro em Bogotá',
        'adm_level.11' => 'Admin 11',
        'adm_level.12' => 'Admin 12',
        'adm_level.13' => 'Admin 13',
        'adm_level.14' => 'Admin 14',
        'adm_level.15' => 'Admin 15',
        'adm_level.16' => 'Admin 16',
        'adm_level.99' => 'Unidade de Conservação',
        'adm_level.98' => 'Território Indígena',
        'adm_level.97' => 'Ambiental',
        'adm_level.100' => 'Parcela',
        'adm_level.101' => 'Transecto ou Trilha',
        'adm_level.999' => 'Ponto',
    ],
    've' => [
        'adm_level.-1' => 'Mundo',
        'adm_level.2' => 'País',
        'adm_level.3' => 'Admin 3',
        'adm_level.4' => 'Estado - Distrito Capital - Dependência Federal',
        'adm_level.5' => 'Distrito Metropolitano - Alcaldia Mayor',
        'adm_level.6' => 'Município',
        'adm_level.7' => 'Parroquia',
        'adm_level.8' => 'Cidade - Vila',
        'adm_level.9' => 'Bairro',
        'adm_level.10' => 'Sub Bairro',
        'adm_level.11' => 'Admin 11',
        'adm_level.12' => 'Admin 12',
        'adm_level.13' => 'Admin 13',
        'adm_level.14' => 'Admin 14',
        'adm_level.15' => 'Admin 15',
        'adm_level.16' => 'Admin 16',
        'adm_level.99' => 'Unidade de Conservação',
        'adm_level.98' => 'Território Indígena',
        'adm_level.97' => 'Ambiental',
        'adm_level.100' => 'Parcela',
        'adm_level.101' => 'Transecto ou Trilha',
        'adm_level.999' => 'Ponto',
    ],
    'bo' => [
        'adm_level.-1' => 'Mundo',
        'adm_level.2' => 'País',
        'adm_level.3' => 'Admin 3',
        'adm_level.4' => 'Departamento',
        'adm_level.5' => 'Região',
        'adm_level.6' => 'Província',
        'adm_level.7' => 'Região Metropolitana ',
        'adm_level.8' => 'Município - Território Indígena - Povoado',
        'adm_level.9' => 'Sub Município com Autoridade',
        'adm_level.10' => 'Sub Município sem Autoridade',
        'adm_level.11' => 'Bairro - OTBs',
        'adm_level.12' => 'Admin 12',
        'adm_level.13' => 'Admin 13',
        'adm_level.14' => 'Admin 14',
        'adm_level.15' => 'Admin 15',
        'adm_level.16' => 'Admin 16',
        'adm_level.99' => 'Unidade de Conservação',
        'adm_level.98' => 'Território Indígena',
        'adm_level.97' => 'Ambiental',
        'adm_level.100' => 'Parcela',
        'adm_level.101' => 'Transecto ou Trilha',
        'adm_level.999' => 'Ponto',
    ],
    'sr' => [
        'adm_level.-1' => 'Mundo',
        'adm_level.2' => 'País',
        'adm_level.3' => 'Admin 3',
        'adm_level.4' => 'Distrito - Província',
        'adm_level.5' => 'Admin 5',
        'adm_level.6' => 'Admin 6',
        'adm_level.7' => 'Admin 7',
        'adm_level.8' => 'Resort - Município',
        'adm_level.9' => 'Admin 9',
        'adm_level.10' => 'Admin 10',
        'adm_level.11' => 'Admin 11',
        'adm_level.12' => 'Admin 12',
        'adm_level.13' => 'Admin 13',
        'adm_level.14' => 'Admin 14',
        'adm_level.15' => 'Admin 15',
        'adm_level.16' => 'Admin 16',
        'adm_level.99' => 'Unidade de Conservação',
        'adm_level.98' => 'Território Indígena',
        'adm_level.97' => 'Ambiental',
        'adm_level.100' => 'Parcela',
        'adm_level.101' => 'Transecto ou Trilha',
        'adm_level.999' => 'Ponto',
    ],
    'py' => [
        'adm_level.-1' => 'Mundo',
        'adm_level.2' => 'País',
        'adm_level.3' => 'Região Natural',
        'adm_level.4' => 'Estado - Departamento',
        'adm_level.5' => 'Admin 5',
        'adm_level.6' => 'Admin 6',
        'adm_level.7' => 'Admin 7',
        'adm_level.8' => 'Município - Distrito',
        'adm_level.9' => 'Admin 9',
        'adm_level.10' => 'Bairro Urbano - Comunidade Rural',
        'adm_level.11' => 'Admin 11',
        'adm_level.12' => 'Admin 12',
        'adm_level.13' => 'Admin 13',
        'adm_level.14' => 'Admin 14',
        'adm_level.15' => 'Admin 15',
        'adm_level.16' => 'Admin 16',
        'adm_level.99' => 'Unidade de Conservação',
        'adm_level.98' => 'Território Indígena',
        'adm_level.97' => 'Ambiental',
        'adm_level.100' => 'Parcela',
        'adm_level.101' => 'Transecto ou Trilha',
        'adm_level.999' => 'Ponto',
    ],
    'pa' => [
        'adm_level.-1' => 'Mundo',
        'adm_level.2' => 'País',
        'adm_level.3' => 'Admin 3',
        'adm_level.4' => 'Província - Comarca',
        'adm_level.5' => 'Região Indígena',
        'adm_level.6' => 'Município - Distrito',
        'adm_level.7' => 'Admin 7',
        'adm_level.8' => 'Corregimento - Comarca',
        'adm_level.9' => 'Localidade Urbana - Comunidade Rural',
        'adm_level.10' => 'Bairro',
        'adm_level.11' => 'Admin 11',
        'adm_level.12' => 'Admin 12',
        'adm_level.13' => 'Admin 13',
        'adm_level.14' => 'Admin 14',
        'adm_level.15' => 'Admin 15',
        'adm_level.16' => 'Admin 16',
        'adm_level.99' => 'Unidade de Conservação',
        'adm_level.98' => 'Território Indígena',
        'adm_level.97' => 'Ambiental',
        'adm_level.100' => 'Parcela',
        'adm_level.101' => 'Transecto ou Trilha',
        'adm_level.999' => 'Ponto',
    ],
    'cr' => [
        'adm_level.-1' => 'Mundo',
        'adm_level.2' => 'País',
        'adm_level.3' => 'Admin 3',
        'adm_level.4' => 'Província',
        'adm_level.5' => 'Admin 5',
        'adm_level.6' => 'Canton',
        'adm_level.7' => 'Admin 7',
        'adm_level.8' => 'Distrito',
        'adm_level.9' => 'Admin 9',
        'adm_level.10' => 'Bairro',
        'adm_level.11' => 'Admin 11',
        'adm_level.12' => 'Admin 12',
        'adm_level.13' => 'Admin 13',
        'adm_level.14' => 'Admin 14',
        'adm_level.15' => 'Admin 15',
        'adm_level.16' => 'Admin 16',
        'adm_level.99' => 'Unidade de Conservação',
        'adm_level.98' => 'Território Indígena',
        'adm_level.97' => 'Ambiental',
        'adm_level.100' => 'Parcela',
        'adm_level.101' => 'Transecto ou Trilha',
        'adm_level.999' => 'Ponto',
    ],
    'ni' => [
        'adm_level.-1' => 'Mundo',
        'adm_level.2' => 'País',
        'adm_level.3' => 'Admin 3',
        'adm_level.4' => 'Departamento',
        'adm_level.5' => 'Admin 5',
        'adm_level.6' => 'Município',
        'adm_level.7' => 'Território Indígena',
        'adm_level.8' => 'Limite Urbano',
        'adm_level.9' => 'Distrito - Comarca',
        'adm_level.10' => 'Bairro - Colônia',
        'adm_level.11' => 'Admin 11',
        'adm_level.12' => 'Admin 12',
        'adm_level.13' => 'Admin 13',
        'adm_level.14' => 'Admin 14',
        'adm_level.15' => 'Admin 15',
        'adm_level.16' => 'Admin 16',
        'adm_level.99' => 'Unidade de Conservação',
        'adm_level.98' => 'Território Indígena',
        'adm_level.97' => 'Ambiental',
        'adm_level.100' => 'Parcela',
        'adm_level.101' => 'Transecto ou Trilha',
        'adm_level.999' => 'Ponto',
    ],
];