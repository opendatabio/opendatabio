<?php

return array (
  'search' => 'Busca',
  'processing' => 'Processando...',
  'reload' => 'Recarregar',
  'csv' => 'CSV',
  'excel' => 'Excel',
  'print' => 'Imprimir',
  'info' => 'Resultados _START_ a _END_ de _TOTAL_',
  'info_empty' => 'Resultados 0 a 0',
  'info_filtered' => '(filtrados de _MAX_)',
  'zero_records' => 'Não há registros, ou você não tem acesso',
  'empty_table' => 'Não há registros, ou você não tem acesso',
  'previous' => 'Anterior',
  'next' => 'Próximo',
  'colvis' => 'Alternar colunas',
  'export' => 'Exportar',
  'delete'  => 'Apagar',
  'wrapnowrap' => 'Linearizar o conteúdo da célula',
  'expand' => 'Expandir ou comprimir a tabela',
  'filter' => 'Filtrar',
  'deselect_all' => 'Limpar linhas selecionadas',
);
