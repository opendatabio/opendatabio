<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'OpenDataBio') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('favicon_io/favicon.ico') }}" >
    <!-- Chart.js -->
    <script src="{{ asset('js/Chart.min.js') }}"></script>

    <!-- Styles required for livewire-tables -->
    <style>
       [x-cloak] { display: none !important; }
    </style>



    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    @vite(['resources/assets/sass/app.scss','resources/assets/js/app.js'])
    @stack('styles')
        <!-- Styles -->
</head>
<body>
<div id="app">
  <button id="odb-bntPgTop" title="Go to top">@lang('messages.page_top')</button>

  @include('layouts.navbar')

  @if (session('status'))
  <div class="container" id="odb-status-box">
    <br>
    <div class="col-md-6 offset-md-3 alert alert-success" ><!-- TODO: positioning! -->
          {!! session('status') !!}&nbsp;&nbsp;
          <button type="submit" class="odb-hide-status-bt unstyle" title="@lang('messages.close')" ><i class="far fa-eye-slash"></i></button>
    </div>
  </div>
  @endif

  <!-- Display Validation Errors -->
  <div class="container" id="odb-errors-box">
    @if(count($errors)>0)
    <br>
    <div class="col-sm-6 offset-sm-3 alert alert-danger" >
    <strong>
      @lang ('messages.whoops')
    </strong>
    <button type="submit" class="odb-hide-errors-bt unstyle" title="@lang('messages.close')" style="float: right;"><i class="far fa-eye-slash"></i></button>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{!! $error !!}</li>
      @endforeach
     </ul>
   </div>
   @endif
   <div id="ajax-error" class="col-sm-6 offset-sm-3 alert alert-danger collapse p-2 mt-5">
     @lang('messages.whoops')
   </div>
 </div>
  @yield('content')
  @if(isset($slot))
  <main class="container d-flex align-items-center justify-content-center">
         {{ $slot }}
  </main>  
  @endif
</div>

{!! View::make('layouts.footer')->with([
      'landing_page' => true,
]) !!}

<!-- Scripts <script src="{{ asset('js/app.js') }}"></script> -->
<!-- page-specific scripts -->
@stack('scripts')



</body>
</html>
