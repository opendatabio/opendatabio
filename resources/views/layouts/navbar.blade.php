<nav class="navbar navbar-expand-lg navbar-dark bg-dark  ml-auto">
  <div class="container-fluid">

    <!-- BRAND ICON AND NAME LEFT -->
    @if(asset('custom/default-logo.png'))
    <img class="navbar-brand" src="{{ asset('custom/default-logo.png') }}" height=30 alt='logo'>
    @endif
    <a class="navbar-brand" href="{{ url('/')}}" style="color: LightYellow;">
      <h3>{{ config('app.name')}}</h3>
    </a>

    <!-- TOOGLER BUTTON WHEN COLLAPSE -->
    <button class="navbar-toggler" type="button"
        data-bs-toggle="collapse"
        data-bs-target="#navbarContent"
        aria-controls="navbarContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- NAVBAR CONTENTS -->
    <div class="collapse navbar-collapse" id="navbarContent">
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">

      @if(!isset($landing_page))

      <li class="nav-item">
          <a class="nav-link"   href="{{ route('locations.index') }}">
          @lang('messages.locations')
        </a>
      </li>
			<li class="nav-item">
          <a class="nav-link"  href="{{ route('taxons.index') }}">
          @lang('messages.taxons')
        </a>
      </li>
			<li class="nav-item">
          <a class="nav-link"   href="{{ route('individuals.index') }}">
          @lang('messages.individuals')
        </a>
      </li>
			<li class="nav-item">
          <a class="nav-link"   href="{{ route('vouchers.index') }}">
          @lang('messages.vouchers')
        </a>
      </li>
			<li class="nav-item">
          <a class="nav-link"   href="{{ route('projects.index') }}">
          @lang('messages.projects')
        </a>
      </li>
      <li class="nav-item">
          <a class="nav-link"   href="{{ route('datasets.index') }}">
          @lang('messages.datasets')
        </a>
      </li>  
      <li class="nav-item">
          <a class="nav-link"    href="{{ route('media.all') }}">
              @lang('classes.Media')
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarTraits" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            @lang('messages.measurements') <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" aria-labelledby="navbarTraits">            
            <li>
              <a class="dropdown-item"  href="{{ route('measurements.index') }}">
                @lang('messages.measurements')
              </a>
            </li>
            <li>
              <a class="dropdown-item"  href="{{ route('traits.index') }}">
                @lang('messages.traits')
              </a>
            </li>       
            @can('view',"App\Models\TraitUnit")     
            <li>
              <a class="dropdown-item"  href="{{ route('trait_units.list') }}">
                @lang('messages.trait_units')
              </a>
            </li>
            @endcan
            @can('create',"App\Models\Form")
            <li>
              <a class="dropdown-item"  href="{{ route('forms.index') }}">
                @lang('messages.forms')
              </a>
            </li>            
            @endcan            
        </ul>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            @lang('messages.auxiliary') <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
          <li >
            <a class="dropdown-item"  href="{{ route('tags.index') }}">
            @lang('messages.tags')
            </a>  
          </li>
            <li>
              <a class="dropdown-item"  href="{{ route('references.index') }}">
                @lang('messages.references')
              </a>
            </li>
      			<li>
              <a class="dropdown-item"  href="{{ route('biocollections.index') }}">
                @lang('messages.biocollections')
              </a>
            </li>
      			<li>
              <a class="dropdown-item"  href="{{ route('persons.index') }}">
                @lang('messages.persons')
              </a>
            </li>
            @can ('show', App\Models\User::class)
            <li>
              <a class="dropdown-item"  href="{{ route('users.index') }}">
                @lang('messages.users')
              </a>
            </li>
            @endcan
          </ul>
        </li>

      @if(Auth::user())
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          @lang('messages.imports') <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" aria-labelledby="navbarDropdown2">
            <li>
                <a class="dropdown-item"  href="{{ route('references.create') }}">
                @lang('messages.references')
              </a>
            </li>
            <li>  <a class="dropdown-item"   href="{{ url('import/biocollections') }}">
              @lang('messages.biocollections')
            </a></li>
            <li>  <a class="dropdown-item"   href="{{ url('import/individuals') }}">
              @lang('messages.individuals')
            </a></li>
            <li>  <a class="dropdown-item"  href="{{ url('import/individuallocations') }}">
              @lang('messages.individuallocations')
            </a></li>
            <li>  <a class="dropdown-item"   href="{{ url('import/locations') }}">
              @lang('messages.locations')
            </a></li>
            <li>  <a class="dropdown-item"   href="{{ url('import/measurements') }}">
              @lang('messages.measurements')
            </a></li>
            <li>  <a class="dropdown-item"  href="{{ url('import/persons') }}">
              @lang('messages.persons')
            </a></li>
            <li>  <a class="dropdown-item"   href="{{ route('media.upload') }}">
              @lang('messages.media_files')
            </a></li>
            <li>  <a class="dropdown-item"   href="{{ url('import/traits') }}">
              @lang('messages.traits')
            </a></li>
            <li>  <a class="dropdown-item"  href="{{ url('import/taxons') }}">
              @lang('messages.taxons')
            </a></li>
            <li>  <a class="dropdown-item"   href="{{ url('import/vouchers') }}">
              @lang('messages.vouchers')
            </a></li>
          </ul>
        </li>
      @endif

      @endif <!-- END WHEN NOT $landing_page-->

      <!-- LOCALIZATION -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="localization_menu" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        <i class="fa-solid fa-language fa-lg"></i></i>
          <span class="caret"></span>
        </a>
        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="localization_menu">
          @foreach (Config::get('languages') as $lang => $language)
            @if(Session::get('applocale') !== $lang)
            <li>
              @if(Auth::user())
                <a class="dropdown-item" href="{{ url('home/'.$lang) }}">
              @else
                <a class="dropdown-item" href="{{ url('welcome/'.$lang) }}">
              @endif
                {{ $language }}</a></li>
            @endif
          @endforeach
        </ul>
      </li>

      <!-- Authentication Links  -->
      @if (Auth::guest())
        <li class="nav-item">
          <a class="nav-link"  href="{{ route('login') }}">
            @lang('messages.login')
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link"  href="{{ route('register') }}">
            @lang('messages.register')
          </a>
        </li>
      @else
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="user_profile" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        <i class="fa-solid fa-user-gear fa-lg"></i><span class="caret"></span>
        </a>
        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="user_profile">
            <li class="nav-item px-2 mb-2">
                <a class="dropdown-item" href="{{ route('home') }}">
                <i class="fa-solid fa-user-tag"></i>
                @if (Auth::user()->person)
                {{ Auth::user()->person->full_name }}
                <br>
                @else
                  {{ Auth::user()->email }}
                @endif
              </a>
            </li>
            <li class="nav-item px-2 mb-2">
                <a class="dropdown-item" href="{{ route('selfedit') }}">
                @lang('messages.edit_profile')
              </a>
            </li>
            <li class="nav-item px-2 mb-2">
                <a class="dropdown-item" href="{{ url('token') }}">
                @lang('messages.api_token')
              </a>
            </li>
            @can ('index', App\Models\UserJob::class)
              <li class="nav-item px-2 mb-2">
                  <a class="dropdown-item" href="{{ route('userjobs.index') }}">
                  @lang('messages.userjobs')
                </a>
              </li>
            @endcan
            <li class="nav-item px-2 mb-2">
                <a class="dropdown-item" href="{{ url('odbrequests/'. Auth::user()->id. '/user')  }}">
                @lang('messages.request_user')
              </a>
            </li>
            <li class="nav-item px-2 mb-2">
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Logout
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>
          </ul>
        </li>
      @endif
      <li class="nav-item px-2 mb-2">
        <a href="{{ url('docs')}}" class='nav-link'>
          <i class="fa-regular fa-circle-question fa-lg"></i>
        </a>
      </li>
      </ul>
    </div>
  </div>
</nav>
