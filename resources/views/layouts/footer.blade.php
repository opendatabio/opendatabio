<!-- ======= Footer =======
<footer class="w-100 py-4 flex-shrink-0"> -->
<footer class="bg-dark text-light text-center text-lg-start mt-5 pb-3" style='font-size: 0.9rem;'>
      <div class="container">
          <div class="row gy-4 gx-5 mx-auto">
        <div class="col-lg-4 col-md-6">
          <h4>{{ config('app.name')}}</h4>
          <p>
            @php
              $address = implode('<br>',Lang::get('customs.footer_address'));
            @endphp
            {!! $address !!}
            <br>
            <strong>@lang('messages.phone'):</strong>
              &nbsp;@lang('customs.footer_phone')<br>
            <strong>Email:</strong>
              &nbsp;{{ config('mail.from.address')}}<br>
          </p>
        </div>

        <div class="col-lg-4 col-md-6">
          <h4>@lang('messages.links')</h4>
          <ul>
            @php
              if (config('app.locale')=="en") {
                $url = config('app.documentation_site_url').'en/docs/overview';
              } else {
                $url = config('app.documentation_site_url').'docs/overview';
              }
            @endphp
            <li class='nav-link'><a href="{{ $url }}" target="_blank">
              OpenDataBio @lang('messages.docs')
            </a></li>
            <li class='nav-link'>
              <a  href="https://gitlab.com/opendatabio/opendatabio-r" target="_blank">
              <i class="fa-solid fa-diagram-project"></i>&nbsp;&nbsp;OpenDataBio R-Package
              </a>
            </li>
            <li class='nav-link'><a href="https://gitlab.com/opendatabio" target="_blank">
              <i class="fa-brands fa-gitlab"></i>&nbsp;&nbsp;OpenDataBio GitLab</a></li>
          </ul>
        </div>
        @php
          $twitter_url = Lang::get('customs.twitter_url')!='customs.twitter_url' ? Lang::get('customs.twitter_url') : null;
          $facebook_url = Lang::get('customs.facebook_url')!='customs.facebook_url' ? Lang::get('customs.facebook_url') : null;
          $instagram_url = Lang::get('customs.instagram_url')!='customs.instagram_url' ? Lang::get('customs.instagram_url') : null;
          $haslinks = isset($twitter_url) or isset($facebook_url) or isset($instagram_url);
        @endphp
        @if ($haslinks)
        <div class="col-lg-4 col-md-6">
          <h4>@lang('messages.social_links')</h4>
          <div class="">
            @if(isset($twitter_url))
            <a href="@lang('customs.twitter_url')" class="px-1">
            <i class="fa-brands fa-twitter fa-2x"></i></a>
            @endif
            @if(isset($facebook_url))
            <a href="@lang('customs.facebook_url')" class="px-1">
            <i class="fa-brands fa-square-facebook fa-2x"></i></a>
            @endif
            @if(isset($instagram_url))
            <a href="@lang('customs.instagram_url')" class="px-1">
            <i class="fa-brands fa-instagram fa-2x"></i></a>
            @endif           
          </div>
        </div>
        @endif
      </div>
    </div>
  <div class="container-fluid m-3">
    <div class="row g-3 text-center">
      <div class="credits">
        <!-- Please DO NOT remove this link to the software website  -->
        @lang('messages.poweredby')
        <a href="{{ config('app.documentation_site_url') }}" target="_blank">
          OpenDataBio {{ config('app.version') }}
        </a>
        <br>
        <small>@lang('messages.license')&nbsp;<a class='links' href="https://www.gnu.org/licenses/gpl-3.0.en.html">GPLv3</a></small>
      </div>
    </div>

  </div>
</footer><!-- End Footer -->
