@extends('layouts.app')

@section('content')
    <div class="container">
      <br>
      <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-header">
                    @lang('messages.api_token')
                </div>

                <div class="card-body">
		    <form class='row' action="{{ url('token') }}" method="POST" class="form-horizontal">
			 {{ csrf_field() }}
<p> @lang ('messages.token_help')</p>
<dl>
    <dt class="dt-odb">
      @lang('messages.api_token')
    </dt>
    <dd>
      <span class="large"> {{ Auth::user()->api_token }} </span>
    </dd>
</dl>

<p>@lang ('messages.reset_token_help')</p>
<div class="mb-3">
    <label for="password" class="form-label">
@lang('messages.current_password')
</label>
	    <div class="col-sm-6">
	<input type="password" autocomplete="current-password" name="password" id="password" class="form-control" value="">
            </div>
</div>
		        <div class="form-group">
			    <div class="col-sm-offset-3 col-sm-6">
				<button type="submit" class="btn btn-danger">
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.reset')

				</button>
				<a href="{{url()->previous()}}" class="btn btn-warning">
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.back')
				</a>
			    </div>
			</div>
		    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
