@extends('layouts.app')

@section('content')
<div class="container">
  <br>
  <div class="card col-md-6 offset-md-3">
      <div class="card-header">
        @lang('messages.login')
      </div>
      <div class="card-body">
        <form class="row" role="form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="mb-3{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="form-label">
              @lang('messages.email')
            </label>
            <input id="email" type="email" autocomplete='username' class="form-control" name="email" value="{{ old('email') }}" aria-describedby="emailHelp" required autofocus>
            @if ($errors->has('email'))
            <div id="emailHelp" class="form-text help-block">
                    <strong>{{ $errors->first('email') }}</strong>
            </div>
            @endif
        </div>
        <div class="mb-3{{ $errors->has('password') ? ' has-error' : '' }}">
          <label for="password" class="form-label">
            @lang('messages.password')
          </label>
          <input id="password" autocomplete='current-password' type="password" class="form-control" name="password" aria-describedby="pwdHelp" required>
          @if ($errors->has('password'))
          <div id="pwdHelp" class="form-text help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </div>
          @endif
        </div>
        <div class="mb-3">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
              @lang('messages.remember')
            </label>
          </div>
        </div>
        <div class="row g-3">
            <div class="col-sm-3">
                <button type="submit" class="btn btn-primary">
                    @lang('messages.login')
                </button>
              </div>
              <div class="col-sm-4">
                <a class="" href="{{ route('password.request') }}">
                    @lang('messages.forgot')
                </a>
              </div>

                <div class="col-sm-4">
                <a href="{{ route('register') }}" class="" >
                  @lang('messages.register')
                </a>
              </div>
            </div>
                        </div>
                    </form>
                </div>
            </div>
</div>
@endsection
