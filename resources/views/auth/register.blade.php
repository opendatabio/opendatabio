@extends('layouts.app')

@section('content')
<div class="container">
  <br>
  <div class="card col-md-6 offset-md-3">
      <div class="card-header">
        @lang('messages.register')
        &nbsp;
<a data-bs-toggle="collapse" href="#registration_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
<div id="registration_hint" class="odb-hint collapse">
@lang('messages.registration_hint')
</div>
</div>
                <div class="card-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="mb-3{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="form-label mandatory">
@lang('messages.email')
</label>

                                <input id="email" autocomplete='username'  type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="mb-3{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="form-label mandatory">
@lang('messages.password')
</label>

                                <input id="password" autocomplete='new-password'  type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="mb-3">
                            <label for="password-confirm" class="form-label mandatory">
                              @lang('messages.confirm_password')
                            </label>
                            <input id="password-confirm" autocomplete='confirm_password' type="password" class="form-control" name="password_confirmation" required>
                        </div>

                        <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @lang('messages.register')
                                </button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
