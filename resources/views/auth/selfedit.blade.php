@extends('layouts.app')

@section('content')
    <div class="container">
      <br>
      <div class="card col-md-6 offset-md-3">
          <div class="card-header">
            @lang('messages.edit_profile')
          </div>
          <div class="card-body">

                    <!-- Edit Person Form -->
		                  <form class='row' action="{{ url('selfupdate') }}" method="POST" class="form-horizontal">
			 {{ csrf_field() }}
                         {{ method_field('PUT') }}

<div class="mb-3">
    <label for="email" class="form-label mandatory">@lang('messages.email')</label>
	   <input type="text" autocomplete="username" name="email" id="email" class="form-control" value="{{ old('email', Auth::user()->email) }}">
</div>

<div class="mb-3">
    <label for="password" class="form-label mandatory">
      @lang('messages.current_password')
    </label>
	   <input type="password" autocomplete="current-password"  name="password" id="password" class="form-control" value="">
</div>
<div class="mb-3">
    <label for="new_password" class="form-label">
      @lang('messages.new_password')
    </label>
    <a data-bs-toggle="collapse" href="#hint1" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	  <input type="password" autocomplete="new-password" name="new_password" id="new_password" class="form-control" value="">
    <div id="hint1" class="odb-hint collapse">
	     @lang('messages.password_hint')
    </div>
</div>
<div class="mb-3">
    <label for="new_password_confirmation" class="form-label">
      @lang('messages.confirm_password')
    </label>
	   <input type="password" autocomplete="password_confirmation" name="new_password_confirmation" id="new_password_confirmation" class="form-control" value="">
</div>
<div class="mb-3">
    <label for="person_id" class="form-label">
      @lang('messages.default_person')
    </label>
    <a data-bs-toggle="collapse" href="#hint2" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <input type="text" name="person_autocomplete" id="person_autocomplete" class="form-control autocomplete"
    value="{{ old('person_autocomplete', (Auth::user()->person ? Auth::user()->person->full_name : null)) }}">
    <input type="hidden" name="person_id" id="person_id"
    value="{{ old('person_id', Auth::user()->person_id) }}">
    <div id="hint2" class="odb-hint collapse">
	@lang('messages.hint_default_person')
    </div>
</div>

<div class="mb-3">
    <label for="project_id" class="form-label">
      @lang('messages.default_project')
    </label>
    <a data-bs-toggle="collapse" href="#hint2p" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    @if (count($projects))
	  <?php $selected = old('project_id', Auth::user()->defaultProject ? Auth::user()->defaultProject->id : null); ?>
	   <select name="project_id" id="project_id" class="form-control" >
       <option value=""> </option>
	      @foreach ($projects as $project)
		      <option value="{{$project->id}}" {{ $project->id == $selected ? 'selected' : '' }}>
            {{ $project->name }}
		      </option>
	      @endforeach
	     </select>
    @else
        <div class="alert alert-warning">
        @lang ('messages.no_valid_project')
        </div>
  @endif
    <div id="hint2p" class="odb-hint collapse">
	     @lang('messages.hint_default_project')
    </div>
</div>

<div class="mb-3">
    <label for="dataset_id" class="form-label">
      @lang('messages.default_dataset')
    </label>
    <a data-bs-toggle="collapse" href="#hint2d" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    @if (count($datasets))
	   <?php $selected = old('dataset_id', Auth::user()->defaultDataset ? Auth::user()->defaultDataset->id : null); ?>
	    <select name="dataset_id" id="dataset_id" class="form-control" >
        <option value=""> </option>
	       @foreach ($datasets as $dataset)
		       <option value="{{$dataset->id}}" {{ $dataset->id == $selected ? 'selected' : '' }}>
            {{ $dataset->name }}
		        </option>
	           @endforeach
	          </select>
            @else
        <div class="alert alert-warning">
        @lang ('messages.no_valid_dataset')
        </div>
@endif
    <div id="hint2d" class="odb-hint collapse">
	@lang('messages.hint_default_dataset')
    </div>
</div>

<div class="mb-3">
    <label for="access" class="form-label">Access Level</label>
    <div class="form-control" >
		{{ Auth::user()->textAccess }}
    </div>
</div>

<div class="mb-3">
	<div class="col-sm-offset-3 col-sm-6">
				<button type="submit" class="btn btn-success">
				    <i class="fa fa-btn fa-plus"></i>
            @lang('messages.save')
				</button>
				<a href="{{url()->previous()}}" class="btn btn-warning">
				    <i class="fa fa-btn fa-plus"></i>
            @lang('messages.back')
				</a>
  </div>
</div>

</form>



                </div>
            </div>
        </div>
    </div>
@endsection

@once
  @push ('scripts')
    @vite('resources/assets/js/custom.js')
    <script type='module'>
      $(document).ready(function() {
        $("#person_autocomplete").odbAutocomplete("{{url('persons/autocomplete')}}","#person_id", "@lang('messages.noresults')");
      });
    </script>
  @endpush
@endonce
