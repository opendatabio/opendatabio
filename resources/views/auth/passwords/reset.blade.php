@extends('layouts.app')

@section('content')
<div class="container">
  <div class="col-lg-6 offset-lg-3 col-md-6 offset-md-3">
      <div class="card">
        <div class="card-header">
          @lang('messages.reset_password')
        </div>
        <div class="card-body">
        @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
        @endif
        <form class="row g-2" role="form" method="POST" action="{{ route('password.request') }}">
          {{ csrf_field() }}
          <input type="hidden" name="token" value="{{ $token }}">

          <div class="mb-3{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="form-label">
              @lang('messages.email')
            </label>
            <div class="">
              <input id="email" autocomplete="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
              @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="mb-3{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="form-label">
              @lang('messages.password')
            </label>
            <div class="">
              <input id="password" autocomplete="current-password" type="password" class="form-control" name="password" required>
              @if ($errors->has('password'))
              <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="mb-3{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label for="password-confirm" class="form-label">
              @lang('messages.confirm_password')
            </label>
            <div class="">
              <input id="password-confirm" autocomplete="password_confirmation" type="password" class="form-control" name="password_confirmation" required>
              @if ($errors->has('password_confirmation'))
              <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="mb-3">
            <button type="submit" class="btn btn-primary">
              @lang ('messages.reset_password')
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
