@extends('layouts.app')

@section('content')
<div id='contents' class="container">
  <br>
  <div class="card mt-3" style='min-width: 60vw;'>
<div class="card-header">
                    @lang('messages.taxon')
<div style="float:right;">
@if ($taxon->mobot)
<a href="http://tropicos.org/Name/{{$taxon->mobot}}" data-toggle="tooltip" rel="tooltip" data-bs-placement="right" title="Tropicos.org"  target="_blank"><img src="{{asset('images/TropicosLogo.gif')}}" alt="Tropicos"></a>
@endif
@if ($taxon->ipni)
&nbsp;
<a href="http://www.ipni.org/ipni/idPlantNameSearch.do?id={{$taxon->ipni}}" data-toggle="tooltip" rel="tooltip" data-bs-placement="right" title="IPNI.org"   target="_blank"><img src="{{asset('images/IpniLogo.png')}}" alt="IPNI" width="33px"></a>
@endif
@if ($taxon->mycobank)
&nbsp;
<a href="http://www.mycobank.org/Biolomics.aspx?Table=Mycobank&Rec={{$taxon->mycobank}}&Fields=All" data-toggle="tooltip" rel="tooltip" data-bs-placement="right" title="MycoBank.org"  target="_blank"><img src="{{asset('images/MBLogo.png')}}" alt="Mycobank" width="33px"></a>
@endif
@if ($taxon->zoobank)
&nbsp;
<a href=="http://zoobank.org/NomenclaturalActs/{{$taxon->zoobank}}" data-toggle="tooltip" rel="tooltip" data-bs-placement="right" title="ZooBank.org"  target="_blank"><img src="{{asset('images/zoobank.png')}}" alt="ZOOBANK" width="33px"></a>
@endif
@if ($taxon->gbif)
&nbsp;
<a href="https://www.gbif.org/species/{{$taxon->gbif}}" data-toggle="tooltip" rel="tooltip" data-bs-placement="right" title="GBIF.org" target="_blank"><img src="{{asset('images/GBIF-2015-mark.png')}}" height="33px"></a>
@endif
&nbsp;
<a href="http://servicos.jbrj.gov.br/flora/search/{{ urlencode($taxon->name) }}" data-toggle="tooltip" rel="tooltip" data-bs-placement="right" title="Flora-Brazil" target="_blank"><img src="{{asset('images/logofb.png')}}" height="33px"></a>

&nbsp;&nbsp;&nbsp;
  <a class="history" href="{{url('taxons/'.$taxon->id.'/activity')}}">
  @lang ('messages.see_history')</a>
</div>



</div>

<div class="card-body">
  <dl class="row">

<dt class="dt-odb">
@lang('messages.name')
</dt>
<dd> <em> {{ $taxon->fullname }} </em></dd>

@if(isset($taxon->author) or isset($author))
<dt class="dt-odb">
@lang('messages.author')
</dt>
<dd>
@if ($author)
{!! $author->rawLink() !!}
@else
{{ $taxon->author }}
@endif
</dd>
@endif

@if ( $bibref or $taxon->bibreference )
<dt class="dt-odb">
@lang('messages.bibreference')
</dt>
<dd>
@if ( $taxon->bibreference)
{{ $taxon->bibreference }}
@endif
@if ($bibref)
{!! $bibref->rawLink() !!}
@endif
</dd>
@endif

<dt class="dt-odb">
@lang('messages.level')
</dt>
<dd>
@lang ('levels.tax.' . $taxon->level)
</dd>

<dt class="dt-odb">
@lang('messages.valid_status')
</dt>
<dd>
@if ($taxon->author_id)
@lang ('messages.unpublished')
@elseif ($taxon->valid)
@lang ('messages.isvalid')
@else
        @lang ('messages.notvalid')
@endif
</dd>

@if ($taxon->higher_classification)
<dt class="dt-odb">
  @lang('messages.taxon_ancestors')
</dt>
<dd>
  {{ $taxon->higher_classification }}    
</dd>
@endif

@if ($taxon->descendants_count)
<dt class="dt-odb">
  @lang('messages.total_descendants')
</dt>
<dd>
<a  href="{{url('taxons/'.$taxon->id.'/taxon')}}" >
  {{ $taxon->descendants_count; }}
</a>
</dd>
@endif


@if ($taxon->persons->count())
<dt class="dt-odb">
@lang('messages.specialists')
</dt>
<dd>
<ul>
@foreach ($taxon->persons as $person)
<li>{!! $person->rawLink() !!}</li>
@endforeach
</ul>
</dd>
@endif

@if ($taxon->notes)
  {!! $taxon->formated_notes !!}
@endif

</dl>

<!-- BUTTONS -->
<nav class="navbar navbar-light navbar-expand-lg">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarbuttons" aria-controls="navbarbuttons" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarbuttons">
      <br>
      <ul class="navbar-nav mb-lg-0 g-3">
        @can ('update', $taxon)
          <li class="nav-item px-2 mb-2">
          <a href="{{ url('taxons/'. $taxon->id. '/edit')  }}" class="btn btn-success" name="submit" value="submit">
          @lang('messages.edit')
          </a>
          </li>
        @endcan

        
        <li class="nav-item px-2 mb-2">
        <a href="{{ url('taxons/'. $taxon->id. '/map')  }}" class="btn btn-primary">
          <i class="fas fa-map-marked-alt fa-lg"></i>
        </a>
        </li>
        
        @if ($taxon->references->count())
        <li class="nav-item px-2 mb-2">
          <a data-bs-toggle="collapse" href="#taxon_references" class="btn btn-outline-secondary">@lang('messages.references')</a>
        </li>
        @endif

        <li class="nav-item px-2 mb-2">
          <a href="{{ url('measurements/'. $taxon->id. '/taxon')  }}" class="btn btn-outline-secondary">
            <i class="fa fa-btn fa-search"></i>
            @lang('messages.measurements')
          </a>
        </li>

          <li class="nav-item px-2 mb-2">
            <a href="{{ url('individuals/'. $taxon->id. '/taxon')  }}" class="btn btn-outline-secondary">
              <i class="fa fa-btn fa-search"></i>
              @lang('messages.individuals')
            </a>
          </li>
       
        <li class="nav-item px-2 mb-2">
          <a href="{{ url('vouchers/'. $taxon->id. '/taxon')  }}" class="btn btn-outline-secondary">
            <i class="fa fa-btn fa-search"></i>
            @lang('messages.vouchers')
          </a>
      </li>
      

<!-- this will show only if no media as media are shown below -->
@can ('create', App\Models\Media::class)
  <li class="nav-item px-2 mb-2">
    <a href="{{ url('taxons/'. $taxon->id. '/media-create')  }}" class="btn btn-outline-secondary">
      <i class="fa fa-btn fa-plus"></i>
      <i class="fas fa-photo-video"></i>
      <i class="fas fa-headphones-alt"></i>
      @lang('messages.create_media')
    </a>
  </li>
@endcan
<li class="nav-item px-2 mb-2">
    <a href="{{ Route('media.taxon.list',[
      'taxon_id' => $taxon->id])}}" class="btn btn-outline-primary">
    <i class="fa fa-btn fa-plus"></i>
    <i class="fas fa-photo-video"></i>
    <i class="fas fa-headphones-alt"></i>
    @lang('messages.media_see_all')
    </a>
</li>

</ul>
</div>
</div>
</nav>


</div>

<!-- start REFERENCE BLOCK -->
<div class="collapse" id='taxon_references'>
  <br>
  <div class="card-header">
    @lang('messages.references')
  </div>
  <div class="card-body">
  @if ($taxon->references)
  <table class="table table-striped">
    <thead>
    <tr>
      <th>@lang('messages.bibtex_key')</th>
      <th>@lang('messages.author')</th>
      <th>@lang('messages.year')</th>
      <th>@lang('messages.title')</th>
   <tr>
   </thead>
   <tbody>
  @foreach($taxon->references as $reference)
    <tr>
      <td class="table-text">
        <a href='{{ url('references/'.$reference->bib_reference_id)}}'>
          {{ $reference->bibkey }}
        </a>
      </td>
      <td class="table-text">{{ $reference->first_author }}</td>
      <td class="table-text">{{ $reference->year }}</td>
      <td class="table-text">{{ $reference->title }}</td>
    </tr>
  @endforeach
  </tbody>
  </table>
  @endif
</div>
</div>

<!-- end REFERENCES BLOCK -->
<!-- Other details (specialist, biocollection, collects, etc?) -->
@if ($taxon->senior or $taxon->juniors->count())
<br>
  <div class="card-header">
    @lang('messages.taxon_sinonimia')
  </div>
  <div class="card-body">
    @if ($taxon->senior)
      <p>
      @lang ('messages.accepted_name'):
      {!! $taxon->senior->rawLink() !!}
      </p>
    @endif
    @if ($taxon->juniors->count())
      <p>
      @lang ('messages.juniors'):
      <ul>
      @foreach ($taxon->juniors as $junior)
        <li>{!! $junior->rawLink() !!}</li>
      @endforeach
      </ul>
    @endif
</div>
@endif


</div>
</div>

@endsection



@once
@push ('scripts')
@vite('resources/assets/js/custom.js')



<script type="module">
    $(document).ready(function() {
const table = $('#dataTableBuilder');
table.on('preXhr.dt',function(e,settings,data) {
  data.level = $("#taxon_level option").filter(':selected').val();
  data.project = $("input[name='project']").val();
  data.location = $("input[name='location_root']").val();
  data.taxon = $("input[name='taxon_root']").val();
  //console.log(data.level,data.project,data.location,data.taxon);
});
$('#taxon_level').on('change',function() {
   table.DataTable().ajax.reload();
   return false;
});
    });
</script>


@endpush
@endonce
