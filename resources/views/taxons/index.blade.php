@extends('layouts.app')

@section('content')
<div id='contents' class="container">
      <!--  <div class="col-sm-offset-2 col-sm-8">-->
        <!-- Registered Taxons -->
        <br>
        <div class="card">
            <div class="card-header">
              @if (isset($object))
                  @lang('messages.taxon_list')
                  <strong>
                    {{ class_basename($object )}}
                  </strong>
                  {!! $object->rawLink(true) !!}
                  @if (isset($object_second))
                  &nbsp;>>&nbsp;<strong>{{class_basename($object_second )}}</strong> 
                  {!! $object_second->rawLink(true) !!}
                  @endif
                  &nbsp;&nbsp;
                  <a href="#" id="about_list" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
                  <div id='about_list_text' class="odb-hint"></div>
             @else
                  @lang('messages.registered_taxons')
                  &nbsp;&nbsp;
                  <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
                 <div id="help" class="odb-hint collapse">
                     @lang('messages.taxon_index_hint')
                     <code>@lang('messages.download_login')</code>
                 </div>
             @endif
           </div>
           @php
              $export_params =[];
              if (isset($filterparams)) {
                $export_params = [
                'taxon_root' => isset($filterparams['filter_taxon_id']) ? implode(',',$filterparams['filter_taxon_id']) : null,
                'location_root' => isset($filterparams['filter_location_id']) ? implode(',',$filterparams['filter_location_id']) : null,
                'dataset' => isset($filterparams['filter_datasets_ids']) ? implode(',',$filterparams['filter_datasets_ids']) : null,
                'project' => isset($filterparams['filter_projects_ids']) ? implode(',',$filterparams['filter_projects_ids']) : null,
                'date_min' => null,
                'date_max' => null,
                'person' => null,
                'biocollection_id' => isset($filterparams['filter_biocollections_ids']) ? implode(',',$filterparams['filter_biocollections_ids']) : null,
                ];
                $export_params = array_filter($export_params);
              }
              if (isset($job_id)) {
                    $export_params['job_id'] = $job_id;
              }     
            @endphp
              @if(count($export_params)>0)
              <div class="mt-3 mx-3">
                <button class="btn btn-sm btn-outline-primary odb-filter-button">
                   {{  __('messages.filter_activated',['n' => count($export_params)]) }}
                </button>  
              </div>
              @endif
            {!! View::make('common.filter')->with([
                    'filter_type' => 'taxons',
                    'url' => 'taxons/filter',                      
                    'filter_taxon_id' => isset($filterparams) ? $filterparams['filter_taxon_id'] : [],
                    'filter_taxon_data' => isset($filterparams) ? $filterparams['filter_taxon_data'] : [],
                    'filter_location_id' => isset($filterparams) ? $filterparams['filter_location_id'] : [],
                    'filter_location_data' => isset($filterparams) ? $filterparams['filter_location_data'] : [],
                    'filter_persons_ids' =>  null,
                    'filter_persons_data' => null,
                    'filter_min_date' => null,
                    'filter_max_date' => null,
                    'filter_datasets_ids' => isset($filterparams) ?  $filterparams['filter_datasets_ids'] : [],
                    'filter_datasets_data' => isset($filterparams) ?  $filterparams['filter_datasets_data'] : [],
                    'filter_projects_ids' => isset($filterparams) ?  $filterparams['filter_projects_ids'] : [],
                    'filter_projects_data' => isset($filterparams) ?  $filterparams['filter_projects_data'] : [],
                    'filter_biocollections_ids' => isset($filterparams) ?  $filterparams['filter_biocollections_ids'] : [],
                    'filter_biocollections_data' => isset($filterparams) ?  $filterparams['filter_biocollections_data'] : [],
              ])  !!}

           @if (Auth::user())
            {!! View::make('common.exportdata')->with([
                  'object' => isset($object) ? $object : null,
                  'object_second' => isset($object_second) ? $object_second : null,
                  'export_what' => 'Taxon',
                  'filters' => isset($export_params) ? $export_params : null,
            ]) !!}

            <!--- delete form -->
            {!! View::make('common.batchdelete')->with([
                  'url' => url('taxons/batch_delete'),
            ]) !!}

            @else
              <br>
            @endif
          <div class="card-body">
          {!! $dataTable->table([],true) !!}
          </div>
        </div>
    <!-- </div> -->
</div>
@endsection
@push('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}

<script type="module">
    $(document).ready(function() {
    const table = $('#dataTableBuilder');
    table.on('preXhr.dt',function(e,settings,data) {
      data.level = $("#taxon_level option").filter(':selected').val();
      data.project = $("input[name='project']").val();
      data.location = $("input[name='location_root']").val();
      data.taxon = $("input[name='taxon_root']").val();
      //console.log(data.level,data.project,data.location,data.taxon);
    });
    $('#taxon_level').on('change',function() {
       table.DataTable().ajax.reload();
       return false;
    });

      $("#about_list").on('click',function(){
        if ($('#about_list_text').is(':empty')){
          var records = $('#dataTableBuilder').DataTable().ajax.json().recordsTotal;
          if (records == 0) {
            $('#about_list_text').html("<br>@lang('messages.no_permission_list')<br>");
          } else {
            $('#about_list_text').html("<br>@lang('messages.taxon_identification_index')<br>");
          }
        } else {
          $('#about_list_text').html(null);
        }
      });
  });
</script>
        {!! Multiselect::scripts('filter_taxon_id', url('taxons/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
        {!! Multiselect::scripts('filter_location_id', url('locations/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
        {!! Multiselect::scripts('filter_datasets_ids', url('datasets/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
        {!! Multiselect::scripts('filter_projects_ids', url('projects/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
        {!! Multiselect::scripts('filter_biocollections_ids', url('biocollections/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
        @vite('resources/assets/js/custom.js')
@endpush
