@extends('layouts.app')

@section('content')
<div class="container">

<div class="card mt-5" >
  <div class="card-header">    
    @lang('messages.forms')
    &nbsp;
    <a data-bs-toggle="collapse" href="#forms_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>    
    <div id="forms_hint" class="panel-collapse collapse">
        @lang('messages.forms_hint')
    </div>
  </div>
  <div class="card-body">    
    @if (Auth::user())
        <!--- delete form -->
        {!! View::make('common.batchdelete')->with([
              'url' => url('forms/batch_delete'),
        ]) !!}
    @endif
    <div class='mb-3'>
      {!! $dataTable->table() !!}
    </div>
  </div>
</div>
</div>

@endsection

@push('scripts')
  {{ $dataTable->scripts(attributes: ['type' => 'module']) }}   
  @vite('resources/assets/js/custom.js')
@endpush
