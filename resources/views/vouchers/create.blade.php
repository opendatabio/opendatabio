@extends('layouts.app')
@section('content')
    <div class="container">
      <br>
      <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
          <div class="card-header">
		                @lang('messages.new_voucher')
                    &nbsp;&nbsp;
                    <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
                  <div id="help" class="odb-hint collapse">
                    @lang('messages.hint_voucher_create')
                </div>
            </div>
                <div class="card-body">

                      @if (isset($voucher))
		                      <form action="{{ url('vouchers/' . $voucher->id)}}" method="POST" class="row g-2">
                            {{ method_field('PUT') }}
                      @else
		                       <form action="{{ url('vouchers')}}" method="POST" class="row g-2">
                      @endif

                      {{ csrf_field() }}


<!-- INDIVIDUAL THE VOUCHER BELONGS TO -->
<div class="mb-3">
  <label for="individual_id" class="form-label mandatory">
    @lang('messages.individual')
  </label>
  <a data-bs-toggle="collapse" href="#hintpp" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div >
    <div id='individual_taxon' hidden class='alert-success'></div>
    <input type="text" name="individual_autocomplete" id="individual_autocomplete" class="form-control autocomplete" value="{{ old('individual_autocomplete', isset($voucher) ? $voucher->individual->fullname : (isset($individual) ? $individual->fullname : null) ) }}">
    <input type="hidden" name="individual_id" id="individual_id" value="{{ old('individual_id', isset($voucher) ? $voucher->individual_id : (isset($individual) ? $individual->id : null)) }}">
  </div>
  <div id="hintpp" class="odb-hint collapse">
	     @lang('messages.voucher_individual_hint')
  </div>
</div>

<!-- THE BIOCOLLECTION THE VOUCHER BELONGS TO -->
<div class="mb-3">
  <label for="biocollection_id" class="form-label mandatory">
    @lang('messages.biocollection')
  </label>
  <a data-bs-toggle="collapse" href="#hint_biocollection" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div >
    <select name="biocollection_id" class="form-select" >
      @php
         $ovtype = old('biocollection_id', isset($voucher) ? $voucher->biocollection_id : $biocollection_id);
      @endphp
      <option >&nbsp;</option>
      @foreach ($biocollections as $biocollection)
      <option value="{{ $biocollection->id }}" {{ $biocollection->id == $ovtype ? 'selected' : '' }}>
        {{ $biocollection->acronym."  [".substr($biocollection->name,0,30)."..]"}}
      </option>
      @endforeach
    </select>
  </div>
    <div id="hint_biocollection" class="odb-hint collapse">
	     @lang('messages.voucher_biocollections_deposited_hint')
    </div>
</div>

<div class="mb-3">
  <label for="biocollection_number" class="form-label ">
    @lang('messages.biocollection_number')
  </label>
  <a data-bs-toggle="collapse" href="#hint_biocollection_number" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div >
    <input type="text" name="biocollection_number" value="{{ old('biocollection_number', isset($voucher) ? $voucher->biocollection_number : null) }}" class="form-control">
  </div>
    <div id="hint_biocollection_number" class="odb-hint collapse">
	     @lang('messages.voucher_biocollections_number_hint')
    </div>
</div>

<!-- is this a nomenclatural type -->
<div class="mb-3">
  <label for="biocollection_type" class="form-label mandatory">
    @lang('messages.voucher_isnomenclatural_type')?
  </label>
  <a data-bs-toggle="collapse" href="#hint_biocollection_type" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div >
    <select name="biocollection_type" class="form-select" style="width:auto;">
      @php
         $ovtype = old('biocollection_type', isset($voucher) ? $voucher->biocollection_type : 0);
      @endphp
      @foreach (\App\Models\Biocollection::NOMENCLATURE_TYPE as $vtype)
      <option value="{{ $vtype }}" {{ $vtype == $ovtype ? 'selected' : '' }}>
        @lang('levels.vouchertype.' . $vtype)
      </option>
      @endforeach
    </select>
  </div>
    <div id="hint_biocollection_type" class="odb-hint collapse">
	     @lang('messages.voucher_nomenclatural_type')
    </div>
</div>


@php
$collectors_self = '';
$collectors_individual = '';
if (empty(old())) { // no "old" value, we're just arriving
    if (isset($voucher)) {
      if ($voucher->collectors()->count()) {
          $collectors_self = 'checked';
          $collectors_individual = '';
      } else {
          $collectors_self = '';
          $collectors_individual = 'checked';
      }
   } else {
     if (isset($individual)) {
       $collectors_self = '';
       $collectors_individual = 'checked';
     }
   }
} else { // "old" value is available, work with it
  if (!empty(old('collectors_selfother'))) {
    if (old('collectors_selfother') == 1) {
      $collectors_self = 'checked';
      $collectors_individual = '';
    } else {
      $collectors_self = '';
      $collectors_individual = 'checked';
    }
  } else {
    $collectors_self = '';
    $collectors_individual = 'checked';
  }
}
@endphp
<div class="mb-3">  
<div class="form-check">
  <input class="form-check-input" type="radio" name="collectors_selfother" id="radio_1"  {{$collectors_individual}} value=1>
  <label class="form-check-label" for="radio_1">
    @lang('messages.voucher_collector_individual')
  </label>
  <a data-bs-toggle="collapse" href="#has_voucher" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
</div>
<div class="form-check">
  <input class="form-check-input" type="radio" name="collectors_selfother" id="radio_0"  {{$collectors_self}} value=0>
  <label class="form-check-label" for="radio_0">
    @lang('messages.voucher_collector_self')
  </label>
</div>
<div id="has_voucher" class="odb-hint collapse">
   @lang('messages.voucher_individual_collector')
</div>

</div>
<!--S
<div class="mb-3 alert-danger" id='select_individual'>
  @lang('messages.select_individual')
</div>
-->
<div class="mb-3 collectors_individual">
  <label class="form-label">
    @lang('messages.collector')
  </label>
  <div id='individual_collectors'  class='col-sm-6 alert-success'></div>
  <label class="form-label">
    @lang('messages.voucher_number')
  </label>
  <div id='individual_tag'  class='col-sm-6 alert-success'></div>
  <label class="form-label">
    @lang('messages.date')
  </label>
  <div id='individual_date'  class='col-sm-6 alert-success'></div>
</div>


<!-- collector -->
<div class="mb-3 collectors_self">
    <label for="collectors" class="form-label mandatory">
      @lang('messages.collector')
    </label>
    <a data-bs-toggle="collapse" href="#hint_collectors" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div >
      {!! Multiselect::autocomplete('collector',
        $persons->pluck('abbreviation', 'id'),
        (isset($voucher) and $voucher->collector_main->count()) ? $voucher->collectors->pluck('person_id')->toArray() : null,
        ['class' => 'multiselect form-control'])
      !!}
    </div>
    <div id="hint_collectors" class="odb-hint collapse">
       @lang('messages.voucher_collectors_hint')
     </div>
</div>


<div class="mb-3 collectors_self">
    <label for="number" class="form-label mandatory">
      @lang('messages.voucher_number')
    </label>
    <a data-bs-toggle="collapse" href="#hint1" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div >
	      <input type="text" name="number" id="number" class="form-control" value="{{ old('number', isset($voucher) ? $voucher->number : null) }}">
      </div>
        <div id="hint1" class="odb-hint collapse">
          @lang('messages.hint_voucher_number')
        </div>
</div>



<!-- date is also not mandatory -->
<div class="mb-3 collectors_self">
    <label for="date" class="form-label mandatory">
      @lang('messages.collection_date')
    </label>
    <a data-bs-toggle="collapse" href="#hintdate" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div >
        {!! View::make('common.incompletedate')->with(['object' => isset($voucher) ? $voucher : null, 'field_name' => 'date']) !!}
      </div>
      <div id="hintdate" class="odb-hint collapse">
	       @lang('messages.voucher_date_hint')
       </div>
</div>


<div class="mb-3">
    <label for="notes" class="form-label">
      @lang('messages.notes')
    </label>
    <a data-bs-toggle="collapse" href="#hintnotes" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	   <div >
	      <textarea name="notes" id="notes" class="form-control">{{ old('notes', isset($voucher) ? $voucher->notes : null) }}</textarea>
      </div>
        <div id="hintnotes" class="odb-hint collapse">
	         @lang('messages.voucher_note_hint')
         </div>
</div>

<div class="mb-3">
<label for="bibreferences" class="form-label">
@lang('messages.references')
</label>
<a data-bs-toggle="collapse" href="#voucher_bibrefs" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
<div >
{!! Multiselect::select(
    'bibreferences',
    $bibreferences->pluck('bibkey', 'id'), isset($voucher) ? $voucher->bibreferences->pluck('bib_reference_id') : [],
     ['class' => 'multiselect form-select']
) !!}
</div>
  <div id="voucher_bibrefs" class="odb-hint collapse">
    @lang('messages.voucher_bibreference_hints')
  </div>
</div>


<!-- dataset -->
<div class="mb-3">
    <label for="dataset" class="form-label mandatory">
      @lang('messages.dataset')
    </label>
    <a data-bs-toggle="collapse" href="#dataset_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div >
      <input type="text" name="dataset_autocomplete" id="dataset_autocomplete" class="form-control autocomplete"
      value="{{ old('dataset_autocomplete', (isset($voucher) and $voucher->dataset_id) ? $voucher->dataset->name : null) }}">
      <input type="hidden" name="dataset_id" id="dataset_id"
      value="{{ old('dataset_id', isset($voucher) ? $voucher->dataset_id : null) }}">
    </div>
      <div id="dataset_hint" class="odb-hint collapse">
	       @lang('messages.vocher_dataset_hint')
       </div>
</div>


<div class="mb-3">
    <button type="submit" class="btn btn-success" name="submit" value="submit">
      <i class="fa fa-btn fa-plus"></i>
      @lang('messages.add')
    </button>
				<a href="{{url()->previous()}}" class="btn btn-warning">
				    <i class="fa fa-btn fa-plus"></i>
            @lang('messages.back')
				 </a>
</div>

		    </form>
        </div>
    </div>
  </div>

@endsection


@once
    @push('scripts')
    {!! Multiselect::scripts('collector', url('persons/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
    @vite('resources/assets/js/custom.js')                

    <script type='module'>
      /** CUSTOM JS CODE HERE: */
$(document).ready(function(){

function onChangeIndividual() {
    var inid = $('#individual_id').val();
    if (inid) {
    //e.preventDefault(); // does not allow the form to submit
    $.ajax({
      type: "GET",
      url: "{{ route('getIndividualForVoucher') }}",
      dataType: 'json',
      data: {
        'id': inid
        },
      success: function (data) {
        //alert(data.individual[1]);
        $('.collectors_individual').show();
        $("#radio_1").prop("checked", true);
        $('.collectors_self').hide();
        //$('#select_individual').hide();
        $('#individual_taxon').html(data.individual[0]).show();
        $('#individual_collectors').html(data.individual[1]).show();
        $('#individual_date').html(data.individual[2]).show();
        $('#individual_tag').html(data.individual[3]).show();
        $('#dataset_id').val(data.individual[4]);
        $('#dataset_autocomplete').val(data.individual[5]);
      },
      error: function(e){
        alert( inid + " will be error" );
      }
      });
    }
  }

function selectCollectorFields(vel) {
  var fromindividual = $('input[name=collectors_selfother]:checked').val();
  if (fromindividual == 1) {
    if ($('#individual_id').val()) {
      $('.collectors_individual').show();
      //$('#select_individual').hide();
    } else {
      //$('#select_individual').show();
    }
    $('.collectors_self').hide();
  } else {
    //$('#select_individual').hide();
    $('.collectors_individual').hide();
    $('.collectors_self').show();
  }
}



$("#dataset_autocomplete").odbAutocomplete("{{url('datasets/autocomplete')}}","#dataset_id", "@lang('messages.noresults')");

//$(".collectors_self").hide();
//$('.collectors_individual').hide();
//$('#select_individual').hide();

selectCollectorFields(400);

//onChangeIndividual();

$("#individual_autocomplete").odbAutocomplete("{{url('individuals/autocomplete')}}", "#individual_id", "@lang('messages.noresults')",null,undefined,onChangeIndividual);

$('#collector').on('change',function(){
  $('#individual_collectors').html(null).hide();
});


$("input[name=collectors_selfother]").change(function() {
  selectCollectorFields();
});


});


    </script>
    
    @endpush
@endonce

