@extends('layouts.app')

@section('content')
<div class="container">
  <br>
  <div class="card col-lg-8 offset-lg-2 col-md-6 offset-md-3 col-sm-6 offset-sm-3">
      <!-- Project basic info block -->
      <div class="card-header">
          @lang('messages.voucher')
          &nbsp;&nbsp;
          {{ $voucher->fullname }}
          <span class="history" style="float:right">
          <a class="history" href="{{url('vouchers/'.$voucher->id.'/activity')}}">
          @lang ('messages.see_history')
          </a>
          </span>
      </div>

      <div class="card-body">
        <dl>
        <dt class="dt-odb">@lang('messages.collectors')</dt>
        <dd>
          {{ $voucher->recordedBy }}        
        </dd>


        <dt class="dt-odb">
          @lang('messages.voucher_number')</dt>
<dd>
          {{ $voucher->recordNumber }}
        </dd>
        <dt class="dt-odb">
          @lang('messages.collection_date')</dt>
        <dd>
          {{ $voucher->recordedDate }}
        </dd>

        <hr>
        <dt class="dt-odb">
@lang('messages.biocollection')</dt>
<dd>
          {!! $voucher->biocollection->rawLink() !!} -
          {{ $voucher->biocollection->name }}

</dd>
        @if ($voucher->biocollection_number)
        <dt class="dt-odb">
@lang('messages.biocollection_number')</dt>
<dd>
            {{ $voucher->biocollection_number }}

</dd>
        @endif
        <dt class="dt-odb">
@lang('messages.voucher_isnomenclatural_type')</strong>?
          {{ $voucher->is_type }}

</dd>

        <hr>
        <dt class="dt-odb">
@lang('messages.individual')</dt>
<dd>
            {!! $voucher->individual->rawLink() !!}

</dd>

<dt class="dt-odb">@lang('messages.identification')</dt>
<dd>
  @if (is_null($identification))
             @lang ('messages.unidentified')

</dd>
           @else
             {!! $identification->rawLink() !!}
</dd>
        <dt class="dt-odb">

             @lang('messages.identified_by')
      </dt>
      <dd>
             {!! $identification->person ? $identification->person->rawLink() : Lang::get('messages.not_registered') !!}
             ({{ $identification->formatDate }})
      </dd>
           @if ($identification->biocollection_id)
           <dt class="dt-odb">

               @lang('messages.identification_based_on')
             </dt>
             <dd>


               @lang('messages.voucher_deposited_at') {!! $identification->biocollection->rawLink() !!}  @lang('messages.under_biocollections_id') {{ $identification->biocollection_reference }}

</dd>
           @endif
           @if ($identification->notes)
             {!! $identification->formated_notes !!}
           @endif
          @endif


          <!-- END identification -->
          <hr>
          <dt class="dt-odb">
          @lang('messages.location')</dt>
          <dd>
                {!! $voucher->location_display !!}
              <br>
              <a href="{{ url('individuals/'. $voucher->individual_id. '/mapme')  }}" class="btn btn-primary btn-sm">
                <i class="fas fa-map-marked-alt fa-1x"></i>&nbsp;@lang('messages.map')
              </a>


</dd>
@if (isset($voucher))
          @if($voucher->bibreferences->count())
          <hr>
          <dt class="dt-odb">
            @lang('messages.cited_by')</dt>
          <dd>
            <br>
            {!! View::make('common.bibreferences')->with([
                'bibreferences' => isset($voucher->bibreferences) ? $voucher->bibreferences : null,
            ]) !!}
          </dd>
        @endif
@endif

<hr>
@if ($voucher->notes)
  {!! $voucher->formated_notes !!}
@endif


<dt class="dt-odb">
@lang('messages.dataset')</dt>
<dd>
{!! $voucher->dataset->rawLink() !!}
</dd>
</dl>

<div class="row g-3 p-2" id='odb-delete-confirmation-form' style="display: none;">
  <p class="col-auto alert alert-danger">
    @lang('messages.action_not_undone')
  </p>
  <form class='col-auto' action="{{ url('vouchers/'. $voucher->id)  }}" method="POST" >
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
    <button type="submit" class="btn btn-danger mt-3" title="@lang('messages.remove')">
      <i class="far fa-trash-alt"></i>
      @lang('messages.remove')
    </button>
  </form>
</div>


<!-- BUTTONS -->
<nav class="navbar navbar-light navbar-expand-lg">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarbuttons" aria-controls="navbarbuttons" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarbuttons">
      <br>
      <ul class="navbar-nav mb-lg-0 g-3">

        @can ('update', $voucher)
        <li class="nav-item px-2 mb-2">
        		<a href="{{ url('vouchers/'. $voucher->id. '/edit')  }}" class="btn btn-success" name="submit" value="submit">
              @lang('messages.edit')
            </a>
        </li>
        @endcan
        @can ('delete', $voucher)
          <li class="nav-item px-2 mb-2">
            <button class="odb-delete-confirmation-button btn btn-danger" type='button' >
              @lang('messages.remove')
            </button>
          </li>
        @endcan

  <li class="nav-item px-2 mb-2">
    <a href="{{ url('vouchers/'. $voucher->id. '/measurements')  }}" class="btn btn-outline-secondary">
        <i class="fa fa-btn fa-search"></i>
        {{ $voucher->measurements()->count() }}
        @lang('messages.measurements')
    </a>
  </li>
@can ('create', App\Models\Media::class)
<li class="nav-item px-2 mb-2">
    <a href="{{ url('vouchers/'. $voucher->id. '/media-create')  }}" class="btn btn-outline-secondary">
    <i class="fa fa-btn fa-plus"></i>
    <i class="fas fa-photo-video"></i>
    <i class="fas fa-headphones-alt"></i>
    @lang('messages.create_media')
  </a>
</li>
@endcan
<li class="nav-item px-2 mb-2">
    <a href="{{ Route('media.voucher.list',[
      'voucher_id' => $voucher->id])}}" class="btn btn-outline-primary">
    <i class="fa fa-btn fa-plus"></i>
    <i class="fas fa-photo-video"></i>
    <i class="fas fa-headphones-alt"></i>
    @lang('messages.media_see_all')
    </a>
</li>
</ul>
</div>
</div>
</nav>

</div>

</div>
</div>
@endsection
