@extends('layouts.app')

@section('content')
      <div id='contents' class="container">
          <br>
            <!--   <div class="col-sm-offset-2 col-sm-8"> -->
      <!-- Registered vouchers -->
          <div class="card">
              <div class="card-header">
              @if (isset($object)) <!-- we're inside a Location, Project or Taxon view -->
                  @lang('messages.voucher_list_for')<strong> {{ class_basename($object) }}</strong>
                  {!! $object->rawLink() !!}
              @else
                @lang('messages.registered_vouchers')
              @endif
              &nbsp;&nbsp;
              <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
              @if (isset($object_second))
              &nbsp;>>&nbsp;<strong>{{class_basename($object_second )}}</strong> {!! $object_second->rawLink(true) !!}
              @endif
              <div id="help" class="odb-hint collapse">
                @lang('messages.vouchers_hint')
                <code>@lang('messages.download_login')</code>
              </div>
          </div>


          @php
            $export_params = [];
            if (isset($filterparams)) {
              $export_params = [
              'taxon_root' => isset($filterparams['filter_taxon_id']) ? implode(',',$filterparams['filter_taxon_id']) : null,
              'location_root' => isset($filterparams['filter_location_id']) ? implode(',',$filterparams['filter_location_id']) : null,
              'person' => isset($filterparams['filter_persons_ids']) ? implode(',',$filterparams['filter_persons_ids']) : null,
              'dataset' => isset($filterparams['filter_datasets_ids']) ? implode(',',$filterparams['filter_datasets_ids']) : null,
              'project' => isset($filterparams['filter_projects_ids']) ? implode(',',$filterparams['filter_projects_ids']) : null,
              'date_min' => isset($filterparams['filter_min_date']) ? $filterparams['filter_min_date'] : null,
              'date_max' => isset($filterparams['filter_max_date']) ? $filterparams['filter_max_date'] : null,
              'biocollection_id' => isset($filterparams['filter_biocollections_ids']) ? implode(',',$filterparams['filter_biocollections_ids']) : null,
              'number' => isset($filterparams['filter_number']) ? $filterparams['filter_number'] : null,                                        
              ];
              $export_params = array_filter($export_params);
            }
            if (isset($job_id)) {
              $export_params['job_id'] = $job_id;
            }   
          @endphp
          @if(count($export_params)>0)
              <div class="mt-3 mx-3">
                <button class="btn btn-sm btn-outline-primary odb-filter-button">
                   {{  __('messages.filter_activated',['n' => count($export_params)]) }}
                </button>  
              </div>
          @endif
          {!! View::make('common.filter')->with([
                      'filter_type' => 'vouchers',
                      'url' => 'vouchers/filter',
                      'filter_taxon_id' => isset($filterparams) ? $filterparams['filter_taxon_id'] : [],
                      'filter_taxon_data' => isset($filterparams) ? $filterparams['filter_taxon_data'] : [],
                      'filter_location_id' => isset($filterparams) ? $filterparams['filter_location_id'] : [],
                      'filter_location_data' => isset($filterparams) ? $filterparams['filter_location_data'] : [],
                      'filter_persons_ids' => isset($filterparams) ? $filterparams['filter_persons_ids'] : [],
                      'filter_persons_data' => isset($filterparams) ? $filterparams['filter_persons_data'] : [],
                      'filter_min_date' => isset($filterparams) ? $filterparams['filter_min_date'] : null,
                      'filter_max_date' => isset($filterparams) ? $filterparams['filter_max_date'] : null,
                      'filter_datasets_ids' => isset($filterparams) ?  $filterparams['filter_datasets_ids'] : [],
                      'filter_datasets_data' => isset($filterparams) ?  $filterparams['filter_datasets_data'] : [],
                      'filter_projects_ids' => isset($filterparams) ?  $filterparams['filter_projects_ids'] : [],
                      'filter_projects_data' => isset($filterparams) ?  $filterparams['filter_projects_data'] : [],
                      'filter_biocollections_ids' => isset($filterparams) ?  $filterparams['filter_biocollections_ids'] : [],
                      'filter_biocollections_data' => isset($filterparams) ?  $filterparams['filter_biocollections_data'] : [],
                      'filter_number' => isset($filterparams) ?  $filterparams['filter_number'] : null,
          ])  !!}          
          @if (Auth::user())
              {!! View::make('common.exportdata')->with([
                    'object' => isset($object) ? $object : null,
                    'object_second' => isset($object_second) ? $object_second : null,
                    'export_what' => 'Voucher',
                    'filters' => isset($export_params) ? $export_params : null,
              ]) !!}
              <!--- delete form -->
              {!! View::make('common.batchdelete')->with([
                    'url' => url('vouchers/batch_delete'),
              ]) !!}
          @endif
          <form action="{{url('vouchers/map')}}" method="POST" class="form-horizontal" id='odb-map-occurrences'>
            <!-- csrf protection -->
            {{ csrf_field() }}
            <input type="hidden" name="idstomap" value="" id='odb-map-selected-rows'>
          </form>
          <div class="card-body mt-3">
            {!! $dataTable->table([],true) !!}
          </div>
      </div>
        <!--- </div> --->
    </div>
@endsection
@once
  @push ('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
    {!! Multiselect::scripts('filter_taxon_id', url('taxons/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
    {!! Multiselect::scripts('filter_location_id', url('locations/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
    {!! Multiselect::scripts('filter_persons_ids', url('persons/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
    {!! Multiselect::scripts('filter_datasets_ids', url('datasets/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
    {!! Multiselect::scripts('filter_projects_ids', url('projects/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
    {!! Multiselect::scripts('filter_biocollections_ids', url('biocollections/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
    
    @vite('resources/assets/js/custom.js')
  @endpush
@endonce