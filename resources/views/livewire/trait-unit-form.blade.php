<div class="card mt-5" style="min-width: 50vw;">
    <div class="card-header">
        @lang('messages.trait_unit')       
    </div>
     <div class="card-body">
     <form wire:submit.prevent="submit">
        {{ csrf_field() }}

<div class="mb-3">
    <label for="unit" class="form-label mandatory fw-bold">
        @lang('messages.unit_code')
    </label>
    <a data-bs-toggle="collapse" href="#unit_code" class="odb-unstyle">
        <i class="far fa-question-circle"></i></a>
    <div id="unit_code" class="odb-hint collapse">
        @lang('messages.unit_code_hint')
    </div>
    <div class="">
        <input type='text'  class="form-control" wire:model.lazy='unit'>
    </div>
    @error('unit') <span class="text-danger">{{ $message }}</span> @enderror
</div>

<div class="mb-3">
    <label for="export_name" class="form-label mandatory fw-bold">
        @lang('messages.name')
    </label>  
    @error('name')  <br> <span class="text-danger">{{ $message }}</span> @enderror
  @foreach ($languages as $language)
  <div class=''>
    <label class="form-label">
        <small>{{$language->name}}</small>
    </label>
    <input type='text' class='form-control' wire:model.lazy="name.{{$language->id}}">
    </div>
    @endforeach

</div>

<div class="mb-3">
    <label for="export_name" class="form-label fw-bold">
        @lang('messages.description')
    </label>  
    @error('description') <br> <span class="text-danger">{{ $message }}</span> @enderror
  @foreach ($languages as $language)
  <div class=''>
    <label class="form-label">
      <small>{{$language->name}}</small>
    </label>
    <textarea class='form-control' 
        wire:model.lazy="description.{{$language->id}}">
    </textarea>
    </div>  
    @endforeach
    
</div>

<div class="mb-3" >
          <button class="btn btn-success"
            wire:click="submit" >
            @lang("messages.save")
          </button>
</div>

</form>
</div>

</div>

@once
    @push ('styles')
        @livewireStyles
    @endpush
    @push ('scripts')
        @livewireScripts
        @vite('resources/assets/js/custom.js')
    @endpush
@endonce