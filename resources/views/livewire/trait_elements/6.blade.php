<label for="value" class="form-label mandatory fw-bold">
@lang('messages.value')
</label>
<div class="">
<input type="color" 
class="form-control form-control-color" 
id="myColor" value="#CCCCCC" 
title="Choose a color"
wire:model.lazy='value'
>
@error('value') <span class="text-danger">{!! $message !!}</span> @enderror
</div>




