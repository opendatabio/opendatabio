<label for="value" class="form-label mandatory  fw-bold">
@lang('messages.value')
</label>
<div class="">
<button 
    type="button" class="btn btn-primary" 
    id="check-gb" data-toggle="tooltip" 
    title="@lang('messages.genebank_search')"
    wire:click='checkGeneBank'
    wire:loading.remove 
>
<i class="fa fa-btn fa-search"></i>
<i class="fas fa-dna"></i>
</button>    
<button type="button" class="btn text-danger" 
    wire:loading wire:target="checkGeneBank">
    <i class="fa-solid fa-spinner fa-spin"></i>
    &nbsp;@lang('messages.be_patient')
</button>
</div>
<div class="">
@endif
<input name ='value' wire:model.lazy='value' class='form-control'>
<em>@lang('messages.genebank_accession')</em><br/>
@error('value') <br> <span class="text-danger">{!! $message !!}</span> @enderror

</div>
