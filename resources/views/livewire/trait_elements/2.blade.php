<label for="value" class="form-label mandatory fw-bold">
    @lang('messages.value')
</label>
<div class="">
    <select name='value' class="form-select" wire:model.lazy="value">
    <option value="" wire:key="tr2null">@lang('messages.select')</option>
	@foreach ($odbtrait->categories as $cat )
		<option value="{{ $cat->id }}" wire:key="tr2{{ $cat->id }}">
            {{ $cat->rank. " - ".$cat->name }}
		</option>
	@endforeach
	</select>    
</div>
@error('value') <br> <span class="text-danger">{!! $message !!}</span> @enderror

