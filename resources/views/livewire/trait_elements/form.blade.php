@if(in_array($trait['type'],[0,1]))
<div>
<input type='text'  
    wire:model.lazy='traits_list.{{$order}}.value' 
    class='form-control'
    placeholder="{{ __('messages.measured_value_unit',[
        'unit' => $trait['trait_unit']
        ])}}">
</div>
<div class="odb-hint">
@if (isset($trait['trait_unit']))
<em>@lang('messages.unit'): {{ $trait['trait_unit'] }} </em>
@endif
@if (isset($trait['rangeDisplay']))
&nbsp;&nbsp;
<em>@lang('messages.range'): {!! $trait['rangeDisplay'] !!} </em>
@endif
</div>
@endif

@if(in_array($trait['type'],[2,4]))
<div>
    <select name='value' class="form-select" 
    wire:model.lazy='traits_list.{{$order}}.value' 
    >
    <option value="" wire:key="tr2null{{$order}}">@lang('messages.select')</option>
    @foreach ($trait['categories'] as $cat )
        <option value="{{ $cat['id'] }}" wire:key="tr2{{$order}}{{ $cat['id'] }}">
            {{ $cat['label'] }}
        </option>
    @endforeach
    </select>        
    </div>
@endif


@if(in_array($trait['type'],[3]))
<div>
    @foreach($trait['selected_categories'] as $key => $cat) 
        <span class='text-nowrap'>
            {{$cat['label']}}
            <button class='btn btn-sm text-danger'
            wire:click="removeCategory({{$order}},{{$key}})">
            <i class="fa-regular fa-trash-can"></i>
            </button>            
            &nbsp;&nbsp;
        </span>            
    @endforeach    
    <select name='category.{{$order}}' 
        id = 'category.{{$order}}' 
        class="form-select" 
        wire:model.lazy='category.{{$order}}'         
    >
    <option value="" wire:key="tr2null{{$order}}">@lang('messages.select')</option>
    @foreach ($trait['categories'] as $cat )
        <option value="{{ $cat['id'] }}" wire:key="tr2{{$order}}{{ $cat['id'] }}">
            {{ $cat['label'] }}
        </option>
    @endforeach
    </select>        
</div>
@endif

@if(in_array($trait['type'],[5,8]))
<div>
    <textarea wire:model.lazy='traits_list.{{$order}}.value' class='form-control'>
    </textarea>
</div>
@endif

@if($trait['type']==6)
<div>
    <input type="color" 
    class="form-control form-control-color" 
    id="myColor" value="#CCCCCC" 
    title="Choose a color"
    wire:model.lazy='traits_list.{{$order}}.value'
    >
</div>
@endif

@if($trait['type']==9)
<div>
<input name ='value' wire:model.lazy='value' class='form-control'>
<em>@lang('messages.genebank_accession')</em><br/>
</div>
@endif
