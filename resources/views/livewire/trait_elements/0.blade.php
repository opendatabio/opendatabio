<label for="value_i" class="form-label mandatory fw-bold">
@lang('messages.value')
</label>
<div class="">
<input type='text'  
    wire:model.lazy='value' 
    class='form-control'
    placeholder="{{ __('messages.measured_value_unit',[
        'unit' => $odbtrait->trait_unit->name ]) }}">
</div>
<div class="odb-hint">
@if (isset($odbtrait->trait_unit))
<em>@lang('messages.unit'): {{ $odbtrait->trait_unit->unit }} </em>
@endif
@if (isset($odbtrait->range_min) or isset($odbtrait->range_max))
&nbsp;&nbsp;
<em>@lang('messages.range'): {!! $odbtrait->rangeDisplay !!} </em>
@endif
</div>
@error('value') <br> <span class="text-danger">{!! $message !!}</span> @enderror

