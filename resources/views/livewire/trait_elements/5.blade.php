<label for="value" class="form-label mandatory fw-bold">
    @lang('messages.value')
</label>
<div class="">
<textarea wire.model.lazy='value' class='form-control'>    
</textarea>
@error('value') <span class="text-danger">{!! $message !!}</span> @enderror

</div>
