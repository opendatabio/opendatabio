<label for="value" class="form-label mandatory  fw-bold">
    @lang('messages.value')
</label>
@if($selected_categories)   
<div>
    @foreach($selected_categories as $key => $cat) 
        <span class='text-nowrap'>
            {{$cat['label']}}
            <button class='btn btn-sm text-danger'
            wire:click="removeCategory({{$key}})">
            <i class="fa-regular fa-trash-can"></i>
            </button>            
            &nbsp;&nbsp;
        </span>
        
    @endforeach    
</div>
@endif

<div class="">
    <select name='category' class="form-select" wire:model.lazy="category">
    <option value="" wire:key="tr2null">@lang('messages.select')</option>
	@foreach ($odbtrait->categories as $cat )
		<option value="{{ $cat->id }}" wire:key="tr2{{ $cat->id }}">
            {{ $cat->rank. " - ".$cat->name }}
		</option>
	@endforeach
	</select>
</div>
@error('value') <br> <span class="text-danger">{!! $message !!}</span> @enderror
