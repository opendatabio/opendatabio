
<div class="card mt-3" style="min-width: 40vw;" >
  <div class="card-header">
    @lang('messages.individuals_batch_identify')
    &nbsp;
    <a data-bs-toggle="collapse" href="#hint_batch_identify" class="odb-unstyle">
      <i class="far fa-question-circle"></i></a>
    <div id="hint_batch_identify" class="odb-hint collapse">
      @lang('messages.individuals_batch_identify_hint')
    </div>
  </div>  
  <div class="card-body">    
    @include('livewire.common.identification')
    @if($show_confirmation)
    <div class="mt-3 mb-3 alert alert-danger">
        @lang("messages.individuals_batch_identify_confirm",['n'=>count($selected_individuals_ids)])
    </div>
    <button class="btn btn-danger" 
      wire:click='saveNewIdentification'>
        @lang('messages.confirm')
    </button>
    @else 
    <button class="btn btn-success" 
      wire:click='submitNewIdentification'>
        @lang('messages.save')
    </button>
    @endif
    @if (session()->has('errors'))
        <div class="mt-3 mb-3 alert alert-danger">
            {!! session('errors') !!}
        </div>
    @endif
    @if (session()->has('success'))
        <div class="mt-3 mb-3 alert alert-success">
            {!! session('success') !!}
        </div>
    @endif
    <div class="row mb-3 mt-4">
        
        <div class='col'>
          <label class="form-label">
            @lang('messages.select_individuals')
            &nbsp;
            <a data-bs-toggle="collapse" href="#individual_hint" class="odb-unstyle">
            <i class="far fa-question-circle"></i></a>
          </label>      
          <div id="individual_hint" class="odb-hint collapse">
            @lang('messages.individual_search_hint')
          </div>
          @livewire('input-search-bar', [
              'search_model' => "Individual",
              'field_name' => "id",
              'return_field' => "fullname",
              'search_field' => "fullname",
              'return_id' => null,
              'query' => null,
              'emitup_function' => "mergeChildIndividual",
              'nested_model' => "", 
              'batch_identify' => true,    
              ]
          )
          @error('selected_individuals_ids') <span class="text-danger">{{ $message }}</span> @enderror
        </div>
        <div class='col'>
          <label class="form-label">
            @lang('messages.select_by_taxon')
            &nbsp;
            <a data-bs-toggle="collapse" href="#select_by_taxon_hint" class="odb-unstyle">
            <i class="far fa-question-circle"></i></a>
          </label>      
          <div id="select_by_taxon_hint" class="odb-hint collapse">
            @lang('messages.select_by_taxon_hint')
          </div>
          @livewire('input-search-bar', [
              'search_model' => "Taxon",
              'field_name' => "id",
              'return_field' => "fullname",
              'search_field' => "fullname",
              'return_id' => null,
              'query' => null,
              'emitup_function' => "mergeChildTaxonSearch",
              'nested_model' => "", 
              ]
          )
        </div>
      </div>
      <input wire:model="selected_individuals_ids" type='text' hidden>
      @if($selected_individuals_ids)
        @php
          $selected_individuals = App\Models\Individual::whereIn('id',$this->selected_individuals_ids)->orderByRaw('FIELD(id,'.implode(",",$selected_individuals_ids).')')->paginate(10);
        @endphp
      <div class="row mb-3 ">
            <label  class="form-label">
            @lang('messages.selected_individuals')&nbsp;&nbsp;{{ count($selected_individuals_ids) }}
            </label>
            <div>
            <table class='table table-striped' >
                <tr>
                    <th></th>
                    <th>@lang('messages.tag')</th>
                    <th>@lang('messages.current_identification')</th>                    
                    <th>@lang('messages.location')</th>
                    <th>@lang('messages.individual')</th>
                    <th>@lang('messages.vouchers')</th>
                </tr>
                @foreach($selected_individuals as $individual)
                  <tr>
                      <td>
                        <button class='btn btn-sm text-danger'
                        wire:click="removeIndividual({{$individual->id}})">
                        <i class="fa-regular fa-trash-can"></i>
                        </button>
                      </td>   
                      <td>{{$individual->tag }}</td>
                      <td>
                        <em>{{$individual->scientific_name }}</em>    
                        <br>
                        <small>[Det: {{$individual->identified_by}} - {{ $individual->date_identified}}]</small>
                      </td>
                      <td>{!! $individual->location_parent_name." > ".$individual->location_name !!}</td>
                      <td>{!! $individual->rawLink() !!}</td>                      
                      <td>
                        @if($individual->vouchers->count())                                                   
                          @php
                            $vouchers = $individual->vouchers->map(function($q){
                              return $q->collector_link;
                            })->toArray();                            
                            $vouchers = implode("<br>",$vouchers);
                          @endphp
                          {!! $vouchers !!}
                        @endif
                      </td>                      
                  <tr>
                @endforeach
                </table>
                <div>
                {!! $selected_individuals->links() !!}
                </div>
            </div>
          @endif  
        </div>
    </div>
</div>


@once
    @push ('styles')
        @livewireStyles
    @endpush
    @push ('scripts')
        @livewireScripts        
        @vite('resources/assets/js/custom.js')
    @endpush
@endonce