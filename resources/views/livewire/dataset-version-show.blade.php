<div class="card mt-5" style="min-width: 50vw;">
    <div class="card-header">
        {{ __('messages.version_dataset')}}
    </div>
    <div class="card-body">    
    <div class="mb-3">
      <label for="dataset" class="form-label fw-bold">
         @lang('messages.dataset')
      </label>
      <div class="">
      {!! $dataset_version->dataset->title_link !!}
      </div>
    </div>
    
    <div class="mb-3">
      <label for="version" class="form-label fw-bold">
         @lang('messages.version')
      </label>
      <div class="">
            {{ $dataset_version->version }} - {{ $dataset_version->date }}
      </div>
    </div>
    <div class="mb-3">
      <label for="access" class="form-label fw-bold">
         @lang('messages.access')
      </label>
      <div class="">
      @if($dataset_version->dataset_access==0)
            @lang('messages.openaccess')
        @else 
            @lang('levels.privacy.' .$dataset_version->dataset->privacy)
        @endif
      </div>
    </div>
    @if($dataset_version->dataset_access==0)
        <div class="mb-3" id='creativecommons' >
            <label for="license" class="form-label fw-bold" >
            @lang('messages.public_license')        
</label>
            <div class="mt-1">
                {{ $dataset_version->license }} {{$dataset_version->license_version}}                    
             </div>    
         </div>
    @endif
    
    @if($dataset_version->policy)
         <div class="mb-3" >
             <label for="policy" class="form-label fw-bold">
               @lang('messages.data_policy')
             </label>             
             <div class="">
         	   {!! $dataset_version->policy !!}
            </div>
         </div>
    @endif
    @if($authors)   
        <div class="mb-3" x-data="{ open: true }">
          <label for="authors" class="form-label fw-bold">
            @lang('messages.authors')
            <button type="button" class="btn btn-sm" @click="open = ! open">
            <i class="far fa-eye-slash fa-lg" x-show="open"></i>
            <i class="fa-regular fa-eye fa-lg" x-show="!open"></i>
            {{ $dataset_version->persons->count() }} @lang('messages.authors')
            </button>  
          </label>           
          <div x-show="open">
            {{ $this->authors }}
          </div>
        </div>    
    @endif
    @if($edit_citation)
        <div class="mb-3" >
          <label for="citation" class="form-label fw-bold">
            @lang('messages.citation')
          </label>
          <a data-bs-toggle="collapse" href="#data_version_citation_hint" class="odb-unstyle">
          <i class="far fa-question-circle"></i></a>
          <div id="data_version_citation_hint" class="odb-hint collapse">
            @lang('messages.data_version_citation_hint')
          </div>
          <div class="">         
            <textarea name="citation" id="citation" class="form-control"
                wire:model="citation" rows=12>                
            </textarea>
            </div>
            <button class="btn btn-outline-success"
            wire:click="saveCitation" >
            @lang("messages.save")
          </button> 
      </div>
    @elseif($this->citation)
        <div class="mb-3" >
          <label for="citation" class="form-label fw-bold">
            @lang('messages.citation')
          </label>
          <div>            
            {!! $this->dataset_version->citation_print !!}
          </div>
          <div class="mt-2 code">  
            <pre>
            {{ $this->dataset_version->bibtex_print }}
            </pre>
        </div>
        @can('delete',$dataset_version->dataset)
        <button class="btn btn-outline-success"
            wire:click="editCitation" >
            @lang("messages.edit_citation")
          </button> 
        @endif
      </div>
    @endif

    @if(count($dataset_version->filters_show)>0)
    <div class="mb-3" >
          <label for="citation" class="form-label fw-bold">
            @lang('messages.applied_filters')
          </label>
          <div class="">
        @foreach($dataset_version->filters_show as $key => $filtro)
            {!! $key.": ".$filtro !!}
            <br>
        @endforeach
    </div>
    </div>
    @endif


    @if($data_files)
    <div class="mb-3" >
          <label for="citation" class="form-label fw-bold">
            @lang('messages.files')
          </label>
          <div class="">
        @foreach($data_files as $file)
            <a href="{{url('storage/datasets/'.$file)}}" download >{{$file}}</a>
            <br>
        @endforeach
    </div>
    </div>
    @endif


    @if (session()->has('errors'))
        <div class="mb-3 alert alert-danger">
            {!! session('errors') !!}
        </div>
    @endif
    @if (session()->has('success'))
        <div class="mb-3 alert alert-success">
            {!! session('success') !!}
        </div>
    @endif
    @can('delete',$dataset_version->dataset)
      <div class="row mb-3" >
        <div class='col-auto'>
          <button class="btn btn-primary"
            wire:click="editVersion" >
            @lang("messages.edit_version")
          </button>
          </div> 
        <div class='col-auto'>
          <button class="btn btn-outline-success"
            wire:click="generateDataFiles" >
            @lang("messages.generate_files")
          </button>
          </div> 
          <div class='col-auto'>
          <button class="btn btn-outline-primary"
            wire:click="citationGenerate" >
            @lang("messages.generate_citation")
          </button>
          </div>
      </div>
    @endif
      </div>
    </div>

  </div>

@once
    @push ('styles')
        @livewireStyles
    @endpush
    @push ('scripts')
        @livewireScripts
        @vite('resources/assets/js/custom.js')
    @endpush
@endonce