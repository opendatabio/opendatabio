<div class="card mt-5" style="min-width: 50vw;">
    <div class="card-header">
        {{ __('messages.forms')}}
        <a data-bs-toggle="collapse" href="#forms_hint" class="odb-unstyle">
        <i class="far fa-question-circle"></i></a>
        <div id="forms_hint" class="odb-hint collapse">
                  @lang('messages.forms_hint')
        </div>
    </div>
<div class="card-body">
        <div class="mb-3">
            <label for="name" class="form-label fw-bold mandatory">
                @lang('messages.name')            
            </label>
            <div class="">
            <input type="text" class='form-control' 
                wire:model="name" >
            </div>
            @error('name') <span class="text-danger">{{ $message }}</span> @enderror
        </div>    

        <div class="mb-3">
            <label for="form_type" class="form-label fw-bold mandatory">
                @lang('messages.type')            
            </label>
            <div class="">
              <select 
                wire:model="form_type" 
                class='form-control'>
                  <option value="0" 
                  wire:key = "tp0"                  
                  >
                    {{__('messages.form_type.0') }}
                  </option>
                  <option value="1" 
                  wire:key = "tp1"                  
                  >
                    {{__('messages.form_type.1') }}
                  </option>
              </select>
          </div>
        </div>


        <div class="mb-3" >
          <label for="datasets_list" class="form-label fw-bold">
            @lang('messages.datasets')
          </label> 
           <a data-bs-toggle="collapse" href="#form_datasets_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          <div id="form_datasets_hint" class="odb-hint collapse">
                    @lang('messages.form_datasets_hint')
          </div> 
          <div class="row">          
            <div class="col-6"> 
            <input type="text" 
            class="form-control" id="dataset_id" 
            wire:model="dataset_id" hidden>
            @livewire('input-search-bar', [
              'search_model' => "Dataset",
              'field_name' => "dataset_id",
              'return_field' => "name",
              'search_field' => "name",
              'return_id' => null,
              'query' => null,
              'emitup_function' => "mergeChildDataset",
              'nested_model' => "",
              'search_placeholder' => "Digite aqui para buscar Datasets..."
              ]
            )
          </div> 
          </div>
          @if($datasets_list)   
          <div x-data="{ open: true }">
            <button type="button" class="btn btn-sm" @click="open = ! open">
              <i class="far fa-eye-slash fa-lg" x-show="open"></i>
              <i class="fa-regular fa-eye fa-lg" x-show="!open"></i>
              {{ count($datasets_list )}} @lang('messages.datasets')
            </button>  
            <table class='table table-striped' x-show="open">            
                  <tr>
                      <th></th>
                      <th>@lang('messages.name')</th>
                      <th>@lang('messages.title')</th>                    
                  </tr>
                  @foreach($datasets_list as $order => $dataset) 
                    <tr>
                        <td>
                          <button class='btn btn-sm text-danger'
                          wire:click="removeDataset({{$order}})">
                          <i class="fa-regular fa-trash-can"></i>
                          </button>
                        </td>
                        <td> {{ $dataset['name'] }}</td>
                        <td> {{ $dataset['title'] }}</td>                      
                    </tr>
                  @endforeach
                  </table>
          </div>                      
          <div class="row mb-3">    
          <div class="col-6"> 
          <button type="button" class="btn btn-sm btn-secondary " wire:click='extractFromDataset' wire:loading.remove >
            @lang('messages.extract_traits_from_datasets')
          </button>  
          <button type="button" class="btn text-danger" 
            wire:loading wire:target="extractFromDataset">
              <i class="fa-solid fa-spinner fa-spin"></i>&nbsp;@lang('messages.be_patient')
            </button>
          </div>          
          @endif          
        </div>   

        <div class="mb-3" >
          <label for="traits_list" class="form-label fw-bold">
            @lang('messages.traits')
          </label> 
          <div class="row">    
          <div class="col-6"> 
            <input type="text" 
            class="form-control" id="trait_id" 
            wire:model="trait_id" hidden>
            @livewire('input-search-bar', [
              'search_model' => "ODBTrait",
              'field_name' => "trait_id",
              'return_field' => "name",
              'search_field' => "name",
              'return_id' => null,
              'query' => null,
              'emitup_function' => "mergeChildTrait",
              'nested_model' => "",
              'search_placeholder' => "Digite aqui para buscar um Trait..."
              ]
            )
          </div> 
          </div>
          @if($traits_list)   
          <div x-data="{ open: true }">
          <button type="button" class="btn btn-sm" @click="open = ! open">
            <i class="far fa-eye-slash fa-lg" x-show="open"></i>
            <i class="fa-regular fa-eye fa-lg" x-show="!open"></i>
            {{ count($traits_list )}} @lang('messages.traits')
          </button>  
          <table class='table table-striped' x-show="open">
                <tr>
                    <th></th>
                    <th>@lang('messages.export_name')</th>
                    <th>@lang('messages.trait')</th>
                    <th>@lang('messages.order')</th>  
                    <th>@lang('messages.mandatory')</th>  
                    @if($form_type==1)
                    <th>@lang('messages.validation_type')</th>  
                    @endif
                </tr>
                @foreach($traits_list as $order => $trait) 
                  <tr >
                      <td>
                        <button class='btn btn-sm text-danger'
                        wire:click="removeTrait({{$order}})">
                        <i class="fa-regular fa-trash-can"></i>
                        </button>
                      </td>
                      <td > {!! $trait['export_name'] !!} </td>
                      <td> {{ $trait['name'] }} </td>
                      <td><select 
                        wire:model="traits_order.{{$order}}" 
                        class='form-control'
                        style="max-width: 50px;"
                        wire:change="changeTraitOrder({{$order}})">
                        @foreach($traits_order as $key => $idx) 
                          <option value="{{$idx}}" 
                          wire:key = "p{{$order}}_{{$idx}}"
                          {{ $idx==$order ? 'selected' : ''}}
                          >
                            {{$idx+1}}
                          </option>
                        @endforeach
                        </select>
                      </td>
                      <td > 
                      <input type="checkbox" 
                          name="traits_list[$order]['mandatory']" 
                          wire:model='traits_list.{{$order}}.mandatory' 
                          value="{{ $trait['mandatory'] }}"
                      >                         
                      </td> 
                      @if($trait['custom_validation'] and $form_type==1)
                      <td > 
                        <select 
                          wire:model="traits_list.{{$order}}.validation" 
                          class='form-control'                         
                          >
                          @foreach($extra_validations as $k => $val) 
                            <option value="{{$val}}" 
                            wire:key = "vv{{$k}}"
                            >
                              {{ __("messages.".$val) }}
                            </option>
                          @endforeach
                        </select>
                      </td>                      
                      @else 
                      <td></td>                   
                      @endif
                  </tr>
                @endforeach
                </table>
                </div>                      
          @endif          
        </div>    



<div class="mb-3">
    <label for="measured_type" class="form-label mandatory fw-bold">
        @lang('messages.object_type')
    </label>   
    <a data-bs-toggle="collapse" href="#measured_type_hint" class="odb-unstyle">
    <i class="far fa-question-circle"></i></a>
    <div id="measured_type_hint" class="odb-hint collapse">
              @lang('messages.measured_type_hint')
    </div> 
	<div class="">
      <select 
        wire:model="measured_type" 
        class='form-control'>
        <option value="" wire:key='objNull'>
          {{__('messages.select')}}
        </option>
        @foreach($objects_list as $k => $opt)        
          <option value="{{$opt}}" 
          wire:key = "obj{{$k}}"          
          >
            {{ str_replace("App\\Models\\","",$opt) }}
          </option>
        @endforeach
        </select>
  </div>
  @error('measured_type') <span class="text-danger">{{ $message }}</span> @enderror
</div>

@if($form_type==1 and $measured_type)
<div class="mb-3">
    <label for="object_order" class="form-label mandatory fw-bold">
        @lang('messages.object_order')
    </label>   
    <a data-bs-toggle="collapse" href="#object_order_hint" class="odb-unstyle">
    <i class="far fa-question-circle"></i></a>
    <div id="object_order_hint" class="odb-hint collapse">
              @lang('messages.object_order_hint')
    </div> 
	<div class="">
      <select 
        wire:model="object_order" 
        class='form-control'>
        <option value="" wire:key='oboNull'>
          {{__('messages.select')}}
        </option>
        @php
          $obj_list = $object_order_list[$measured_type];
        @endphp
        @foreach($obj_list as $k => $opt)        
          <option value="{{$opt}}" 
          wire:key = "obo{{$k}}"          
          >
            {{  $opt }}
          </option>
        @endforeach
        </select>
  </div>
  @error('measured_type') <span class="text-danger">{{ $message }}</span> @enderror
</div>



@endif


        <div class="mb-3" id='show_policy' >
          <label for="notes" class="form-label fw-bold">
            @lang('messages.notes')
          </label>
          <div class="">         
            <textarea name="notes"  class="form-control"
                wire:model="notes" rows=12>                
            </textarea>
            </div>
        </div>

        <div class="mb-3" >
          <button class="btn btn-success"
            wire:click="submit" >
            @lang("messages.save")
          </button>
        </div>
      </div>
    </div>

  </div>

@once
    @push ('styles')
        @livewireStyles
    @endpush
    @push ('scripts')
        @livewireScripts
        @vite('resources/assets/js/custom.js')
    @endpush
@endonce