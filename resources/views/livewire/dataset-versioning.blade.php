<div class="card mt-5" style="min-width: 50vw;">
    <div class="card-header">
        {{ __('messages.version_dataset')}}
        <a data-bs-toggle="collapse" href="#publish_dataset_hint" class="odb-unstyle">
        <i class="far fa-question-circle"></i></a>
        <div id="publish_dataset_hint" class="odb-hint collapse">
                  @lang('messages.version_dataset_hint')
        </div>
    </div>
<div class="card-body">
    <div class="mb-3">
      {!! $dataset_name !!}
    </div>
    <div class="mb-3">
        <label for="version" class="form-label fw-bold mandatory">
            @lang('messages.version')
            <a data-bs-toggle="collapse" 
              href="#version_hint" class="odb-unstyle">
              <i class="far fa-question-circle"></i>
             </a>
        </label>
        <div id="version_hint" class="odb-hint collapse">
            @lang('messages.version_hint')
        </div>
        <div class="">
          <select class='form-control' wire:model="version" >
              @foreach($version_options as $k => $opt)
                <option value="{{$opt}}" wire:key="{{$k.'opt'}}">
                  {{$opt}}
                </option>
              @endforeach
          </select>
        </div>
        @error('version') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="mb-3">
        <label for="version_date" class="form-label fw-bold mandatory">
            @lang('messages.date')
            <a data-bs-toggle="collapse" 
              href="#version_date_hint" class="odb-unstyle">
              <i class="far fa-question-circle"></i>
             </a>
        </label>
        <div id="version_date_hint" class="odb-hint collapse">
            @lang('messages.version_date_hint')
        </div>
        <div style='max-width: 200px;'>
          <input type="date" class='form-control' wire:model='version_date'>          
        </div>
        @error('version_date') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    @include("livewire.dataset-filter")    
    
    <div class="mb-3">
      <label for="access" class="form-label fw-bold mandatory">
         @lang('messages.access')
      </label>
      <div class="">
         	<select class="form-select" 
            wire:model="dataset_access">
         	  <option value="0" wire:key="dta_0">
                @lang('messages.openaccess')
            </option>        
            @if($privacy<=2)
            <option value="1" wire:key="dta_1">
                @lang('levels.privacy.' .$privacy)
            </option>
            @endif
         	</select>
      </div>
      @error('dataset_access') <span class="text-danger">{{ $message }}</span> @enderror
    </div>

      <div class="mb-3" id='creativecommons' >
          <label for="license" class="form-label fw-bold {{ $dataset_access==0 ? 'mandatory' : ''}}"id='licenselabel'>
          @lang('messages.public_license')
          <a data-bs-toggle="collapse" href="#creativecommons_licenses_hint" class="odb-unstyle">
              <i class="far fa-question-circle"></i></a>
          </label>
          <div id="creativecommons_licenses_hint" class="odb-hint collapse">
              @lang('messages.creativecommons_dataset_version_hint')
          </div>
          <div class="row">  
              <div class="col-8">
              <select name="license" id="license" class="col form-select" wire:model="license">
                @foreach (config('app.creativecommons_licenses') as $key => $level)
                  <option value="{{ $level }}" 
                  wire:key="{{'cc_'.$key }}"
                  >
                    {{$level}} - @lang('levels.' . $level)
                  </option>
                @endforeach
              </select>      
              </div>   
              <div class="col-4 text-nowrap">
              @if ($version_readonly)
              <strong>CC-Version:</strong>&nbsp;{{ $license_version }}
              @else
                <select name="license_version" class="form-select" wire:model="license_version">
                  @foreach (config('app.creativecommons_version') as $key => $lic_version)
                    <option value="{{ $lic_version }}" 
                          wire:key="{{'lv_'.$key }}">
                          CC-Version - {{ $lic_version}}
                    </option>
                  @endforeach
                </select>
              @endif
              </div>                
            </div>    
            @error('license') <span class="text-danger">{{ $message }}</span> @enderror
            @error('license_version') <span class="text-danger">{{ $message }}</span> @enderror           
        </div>

      
        
         <!-- following creative commons, 
        filling here implicate the dataset has sui generis database rights,
        which will be indicated here -->
         <div class="mb-3" id='show_policy' >
             <label for="policy" class="form-label fw-bold">
               @lang('messages.data_policy')
             </label>
             <a data-bs-toggle="collapse" href="#dataset_policy_hint" class="odb-unstyle">
                <i class="far fa-question-circle"></i></a>
             <div id="dataset_policy_hint" class="odb-hint collapse">
               @lang('messages.data_policy_hint')
             </div>
             <div class="">
         	     <textarea name="policy" id="policy" class="form-control"
                   wire:model="policy" rows=4>
                </textarea>
            </div>
         </div>


        <div class="mb-3" x-data="{ open: true }">
          <label for="authors" class="form-label fw-bold">
            @lang('messages.authors')
          </label> 
          <div class="row">
            
          <div class="col-6"> 
            <input type="text" 
            class="form-control" id="author_id" 
            wire:model="author_id" hidden>
            @livewire('input-search-bar', [
              'search_model' => "Person",
              'field_name' => "author_id",
              'return_field' => "full_name",
              'search_field' => "full_name",
              'return_id' => null,
              'query' => null,
              'emitup_function' => "mergeChildAuthor",
              'nested_model' => "",
              'search_placeholder' => "Digite aqui para buscar pessoa ..."
              ]
            )
          </div> 
          <div class="col-4">
            <button class="btn btn-sm btn-outline-primary" 
            wire:click="findAuthors" wire:loading.remove >
                @lang('messages.find_authors_from_data')
            </button>
            <button type="button" class="btn text-danger" 
              wire:loading wire:target="findAuthors">
                <i class="fa-solid fa-spinner fa-spin"></i>&nbsp;@lang('messages.be_patient')
            </button>
          </div> 
          </div>
          @if($authors)   
          <div>
          <button type="button" class="btn btn-sm" @click="open = ! open">
            <i class="far fa-eye-slash fa-lg" x-show="open"></i>
            <i class="fa-regular fa-eye fa-lg" x-show="!open"></i>
            {{ count($authors )}} @lang('messages.authors')
          </button>  
          <table class='table table-striped' x-show="open">
                <tr>
                    <th></th>
                    <th>@lang('messages.person')</th>
                    <th>@lang('messages.order')</th>                    
                </tr>
                @foreach($authors as $order => $author) 
                  <tr>
                      <td>
                        <button class='btn btn-sm text-danger'
                        wire:click="removeAuthor({{$order}})">
                        <i class="fa-regular fa-trash-can"></i>
                        </button>
                      </td>
                      <td> {{ $author['name'] }}</td>
                      <td><select 
                        wire:model="authors_order.{{$order}}" 
                        class='form-control'
                        style="max-width: 50px;"
                        wire:change="changeAuthorOrder({{$order}})">
                        @foreach($authors_order as $key => $idx) 
                          <option value="{{$idx}}" 
                          wire:key = "p{{$order}}_{{$idx}}"
                          {{ $idx==$order ? 'selected' : ''}}
                          >
                            {{$idx+1}}
                          </option>
                        @endforeach
                        </select>
                      </td>
                  </tr>
                @endforeach
                </table>
                </div>                      
          @endif
          
        </div>    
        
      <div class="mb-3" >
          <button class="btn btn-success"
            wire:click="submit" >
            @lang("messages.save")
          </button>
      </div>
      </div>
    </div>

  </div>

@once
    @push ('styles')
        @livewireStyles
    @endpush
    @push ('scripts')
        @livewireScripts
        @vite('resources/assets/js/custom.js')
    @endpush
@endonce