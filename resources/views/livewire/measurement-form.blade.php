<div class="card mt-5" style="min-width: 50vw;">
    <div class="card-header">
        @if(isset($measurement))
          @lang('messages.edit_measurement')
        @else
          @lang('messages.new_measurement')
        @endif    
    </div>
<div class="card-body">
  
@if(!$no_object_editing)
  <div class="mb-3">
      <label for="measured_type" class="form-label mandatory fw-bold">
          @lang('messages.measurement_for')        
      </label>
      <div class="">
      <select name='value' class="form-select" wire:model="measured_type">
      <option value="" wire:key="tr2null">@lang('messages.select')</option>
      @foreach ($object_types as $k => $obj )
        <option value="{{ $obj['value'] }}" wire:key="tr2{{ $k }}">
                {{ $obj['label'] }}
        </option>
      @endforeach
    </select>    
  </div>
  @error('measured_type') <br> <span class="text-danger">{{ $message }}</span> @enderror
  </div>

  @if($short_measured_type)
    <div class="mb-3">
        <label for="measured_type" class="form-label mandatory fw-bold">
            {{ $short_measured_type }}
        </label>
        @if($measured_name)
          <br>
          {!! $measured_name !!}
        @endif
        <div class="">

    @if($short_measured_type=="Individual")
      @livewire('input-search-bar', [
        'search_model' => "Individual",
        'field_name' => "id",
        'return_field' => "fullname",
        'search_field' => "fullname",
        'return_id' => null,
        'query' => null,
        'emitup_function' => "mergeChildObject",
        'nested_model' => "", 
        'batch_identify' => true,    
        ]
      )
    @endif

    @if($short_measured_type=="Taxon")
      @livewire('input-search-bar', [
          'search_model' => "Taxon",
          'field_name' => "id",
          'return_field' => "fullname",
          'search_field' => "fullname",
          'return_id' => null,
          'query' => null,
          'emitup_function' => "mergeChildObject",
          'nested_model' => "", 
          ]
      )      
    @endif

    @if($short_measured_type=="Voucher")
      @livewire('input-search-bar', [
          'search_model' => "Voucher",
          'field_name' => "id",
          'return_field' => "fullname",
          'search_field' => "fullname",
          'return_id' => null,
          'query' => null,
          'emitup_function' => "mergeChildObject",
          'nested_model' => null, 
          ]
      )  
    @endif

    @if($short_measured_type=="Location")
      @livewire('input-search-bar', [
        'search_model' => "Location",
        'field_name' => "id",
        'return_field' => "name",
        'search_field' => "name",
        'return_id' => null,
        'query' => null,
        'emitup_function' => "mergeChildObject",
        'nested_model' => null, 
        ]
      )    
    @endif
    </div>
    @error('measured_id') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
  @endif
@else 
<div class="mb-3">
      <label for="measured_type" class="form-label mandatory fw-bold">
          @lang('messages.measurement_for')    
          {{ $short_measured_type }}    
      </label>
      <div>
        {!! $measured_name !!}
      </div>
</div>
@endif

@if($measured_id)

@if($short_measured_type=="Taxon")
<div class="mb-3">
    <label for="location_id" class="form-label fw-bold">
        @lang('messages.location')
    </label>
    <a data-bs-toggle="collapse" href="#measurement_location_id" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="measurement_location_id" class="odb-hint collapse">
            @lang('messages.measurement_location_id')
        </div>
    @if($location_name)
    <div >
        {!! $location_name !!}
    </div>    
    @endif
    <div class='mt-2'>
      @livewire('input-search-bar', [
                'search_model' => "Location",
                'field_name' => "id",
                'return_field' => "name",
                'search_field' => "name",
                'return_id' => null,
                'query' => null,
                'emitup_function' => "mergeChildLocation",
                'nested_model' => null, 
                ]
            )     
    @error('location_id') <br> <span class="text-danger">{{ $message }}</span> @enderror

  </div>  
</div>
@endif

<div class="mb-3">
    <label for="trait_id" class="form-label mandatory fw-bold">
        @lang('messages.trait')
    </label>    
    @if($odbtrait)
    <div >
      {!! $odbtrait->rawLink() !!} - {{ mb_strtolower($odbtrait->name) }} 
    </div>    
    @endif
    <div class='mt-2'>
        @livewire('input-search-bar', [
            'search_model' => "ODBTrait",
            'field_name' => "id",
            'return_field' => "name",
            'search_field' => null,
            'return_id' => null,
            'query' => null,
            'emitup_function' => "mergeChildTrait",
            'nested_model' => "",            
            ]
        )
        @error('trait_id') <span class="text-danger">{{ $message }}</span> @enderror
  </div>  


</div>

@if($odbtrait)
<div class="mb-3">
  @php
    $trait_view = "livewire.trait_elements.".($odbtrait->type);
  @endphp
  @include($trait_view)
</div>
@endif

<div class="mb-3">
    <label for="date" class="form-label mandatory fw-bold">
      @lang('messages.measurement_date')
  </label>
    <a data-bs-toggle="collapse" href="#measurement_date_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="measurement_date_hint" class="odb-hint collapse">
      @lang('messages.measurement_date_hint')
    </div>
	  <div class="">
      @include('livewire.common.incompletedate')
    </div>  
    @error('date_year') <span class="text-danger">{{ $message }}</span> @enderror
</div>


<div class="mb-3" x-data="{ open: true }">
      <label for="authors" class="form-label mandatory fw-bold">
        @lang('messages.measurement_measurer')
      </label> 
      <a data-bs-toggle="collapse" href="#measurement_person_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
      <div id="measurement_person_hint" class="odb-hint collapse">
        @lang('messages.measurement_person_hint')
      </div>
      @error('authors') <br> <span class="text-danger">{{ $message }}</span> @enderror
      <div class="">                  
        <input type="text" 
        class="form-control" id="author_id" 
        wire:model="author_id" hidden>
        @livewire('input-search-bar', [
          'search_model' => "Person",
          'field_name' => "author_id",
          'return_field' => "full_name",
          'search_field' => "full_name",
          'return_id' => null,
          'query' => null,
          'emitup_function' => "mergeChildPerson",
          'nested_model' => "",
          'search_placeholder' => "Digite aqui para buscar pessoa ..."
          ]
        )        
      </div>
      @if($authors)   
        <div>
        <button type="button" class="btn btn-sm" @click="open = ! open">
          <i class="far fa-eye-slash fa-lg" x-show="open"></i>
          <i class="fa-regular fa-eye fa-lg" x-show="!open"></i>
          {{ count($authors )}} @lang('messages.authors')
        </button>  
        <table class='table table-striped' x-show="open">
            <tr>
                <th></th>
                <th>@lang('messages.person')</th>
                <th>@lang('messages.order')</th>                    
            </tr>
            @foreach($authors as $order => $author) 
              <tr>
                  <td>
                    <button class='btn btn-sm text-danger'
                    wire:click="removeAuthor({{$order}})">
                    <i class="fa-regular fa-trash-can"></i>
                    </button>
                  </td>
                  <td> {{ $author['name'] }}</td>
                  <td><select 
                    wire:model="authors_order.{{$order}}" 
                    class='form-control'
                    style="max-width: 50px;"
                    wire:change="changeAuthorOrder({{$order}})">
                    @foreach($authors_order as $key => $idx) 
                      <option value="{{$idx}}" 
                      wire:key = "p{{$order}}_{{$idx}}"
                      {{ $idx==$order ? 'selected' : ''}}
                      >
                        {{$idx+1}}
                      </option>
                    @endforeach
                    </select>
                  </td>
              </tr>
            @endforeach
            </table>
        </div>                      
      @endif        
</div>    
      
<div class="mb-3">
    <label for="bibreference_id" class="form-label fw-bold">
      @lang('messages.measurement_bibreference')
    </label>
    <a data-bs-toggle="collapse" href="#measurement_bibreference_hint" class="odb-unstyle">
      <i class="far fa-question-circle"></i>
    </a>
    <div id="measurement_bibreference_hint" class="odb-hint collapse">
    @lang('messages.measurement_bibreference_hint')
    </div>
    @if($bibreference_name)
    <div >
        {!! $bibreference_name !!}
    </div>    
    @endif  
  <div class="">
    @livewire('input-search-bar', [
        'search_model' => "BibReference",
        'field_name' => "id",
        'return_field' => "name",
        'search_field' => "bibkey",
        'return_id' => null,
        'query' => null,
        'emitup_function' => "mergeChildBib",
        'nested_model' => "",            
        ]
    )
    @error('bibreference_id') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
</div>


@if (count($parent_measurements))  
<div class="mb-3">
    <label for="parent_id" class="form-label fw-bold">
        @lang('messages.measurement_parent')
    </label>
    <a data-bs-toggle="collapse" href="#measurement_parent_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="measurement_parent_hint" class="odb-hint collapse">
	     @lang('messages.measurement_parent_hint')
    </div>      
    <div class='mt-2'>
        <select wire:model.lazy="parent_id" id="parent_id" class="form-select" >
          <option value="" wire:key='pmnull'>@lang('messages.select')</option>
          @foreach ($parent_measurements as $parent)
            <option value="{{$parent['id']}}" wire:key="pm{{$parent['id']}}">
                    {{ $parent['label'] }}
            </option>
          @endforeach
        </select>
  </div>  
  @error('parent_id') <span class="text-danger">{{ $message }}</span> @enderror

</div>
@endif

<div class="mb-3">
  <label for="dataset_id" class="form-label mandatory fw-bold">
    @lang('messages.measurement_dataset')
  </label>
  <div class="">  
    @if (count($user_datasets))    	
    	<select wire:model.lazy="dataset_id" id="dataset_id" class="form-select" >
        <option value="" wire:key='datmnull'>@lang('messages.select')</option>
      	@foreach ($user_datasets as $dataset)
      		<option value="{{$dataset['id']}}" wire:key="dt{{$dataset['id']}}">
                  {{ $dataset['label'] }}
      		</option>
      	@endforeach
      </select>
    @else
      <div class="alert alert-danger">
        @lang ('messages.no_valid_dataset')
      </div>
    @endif
  </div>
  @error('dataset_id') <span class="text-danger">{{ $message }}</span> @enderror

</div>


<!-- notes -->
<div class="mb-3">
    <label for="notes" class="form-label fw-bold">
        @lang('messages.notes')
    </label>
    <textarea class="form-control" wire:model='notes'>
    </textarea>
</div>

@if($confirm_duplicated)
<div class="mb-3 alert alert-danger">
  <label class="form-label fw-bold">
      @lang('messages.allow_duplicated')
  </label>
  <div class="">
    @lang('messages.allow_duplicated_hint',['nrecords'=> $existing_dups])    
  </div>  
  <div class="form-check">
      <input class="form-check-input" type="checkbox" value="1" 
      wire:model.lazy='allow_duplicated'>
      <label class="form-check-label" for="allow_duplicated">
        @lang('messages.import_duplicated_measurement')
      </label>
  </div>
</div>
@endif


<div class="mb-3" >
    <button type='button' class="btn btn-success"
      wire:click="save" >
      @lang("messages.save_only")
    </button>
    &nbsp;&nbsp;
    <button type='button' class="btn btn-primary"
      wire:click="saveAnother" >
      @lang("messages.save_another")
    </button>
</div>

@endif


</div>

</div>

@once
    @push ('styles')
        @livewireStyles
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.1/spectrum.min.css">
    @endpush
@push ('scripts')
        @livewireScripts
        @vite('resources/assets/js/custom.js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.1/spectrum.min.js"></script>
@endpush


@endonce