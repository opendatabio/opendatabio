<div class="card mt-5" style="max-width: 60vw;">
    <div class="card-header">
        @lang('messages.trait_units')
        &nbsp;&nbsp;
        <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        <div id="help" class="odb-hint collapse">
            <hr>
            @lang('messages.trait_units_hint')
        </div>
    </div>
     <div class="card-body">
        @if (session()->has('success'))
            <div class="alert alert-success">
                {!! session('success') !!}
            </div>
        @endif
        @if (session()->has('errors'))
            <div class="alert alert-danger">
                {!! session('errors') !!}
            </div>
        @endif       
        @if(count($trait_units))
            <div class='row mb-3'>        
            @if(isset($per_page))
                <div class='col-auto col-sm-auto col-lg-auto'>          
                        <select wire:model='per_page' style='width: auto; font-size: 0.9em;' class="form-control">
                            <option value="6" >6 @lang('messages.per_page')</option>
                            <option value="12">12 @lang('messages.per_page')</option>
                            <option value="20">20 @lang('messages.per_page')</option>
                            <option value="40">40 @lang('messages.per_page')</option>
                            <option value="50">50 @lang('messages.per_page')</option>        
                        </select>
                </div>                
            @endif      
            @can('create','App\Models\TraitUnit')  
                @if($confirm_creation)
                    <div class='alert alert-warning col-auto col-sm-auto col-lg-auto'>
                        @lang('messages.confirm_unit_creation')
                    <button class='btn btn-sm btn-primary'
                        wire:click="newUnit(1)">
                        @lang('messages.create')
                    </button>            
                    </div>                
                @else            
                <div  class='col-auto col-sm-auto col-lg-auto'>                        
                    <button class='btn btn-sm btn-primary'
                        wire:click="newUnit">
                        @lang('messages.create')
                    </button>            
                </div>        
                @endif            
            @endcan
            <div class='mt-3'>                        
                {{ $trait_units->links() }}
            </div> 
            <div>
            <table class="table table-striped user-table">
            <thead>
                <th>           
                </th>
                <th>@lang('messages.unit_code')</th>
                <th>@lang('messages.name')</th>
                <th>@lang('messages.description')</th>
                <th>@lang('messages.traits')</th>
            </thead>
            <tbody>
            @foreach($trait_units as $unit)
                <tr>
                    <td>
                        @can('destroy',$unit)
                        <button class='btn btn-sm text-danger'
                            wire:click="removeUnit({{$unit->id}})">
                            <i class="fa-regular fa-trash-can"></i>
                        </button>
                        @endcan
                        @can('update',$unit)
                        <button class='btn btn-sm text-primary'
                            wire:click="editUnit({{$unit->id}})">
                        <i class="fa-regular fa-edit"></i>
                        </button>
                        @endcan
                    </td>
                    <td class="table-text">
                        {{ $unit->unit }}
                    </td>
                    <td class="table-text">
                        {{ $unit->name }}
                    </td>
                    <td class="table-text">
                        {{ $unit->description }}
                    </td>
                    <td class="table-text">
                        @if($unit->odbtraits->count())
                        <button class='btn btn-sm text-primary'
                            wire:click="viewTraits({{$unit->id}})">
                            <i class="fa-regular fa-eye"></i>
                            @lang('messages.traits')
                        </button>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
            </table>
            </div>
            <div class='col col-sm-auto col-lg-auto'>                        
                        {{ $trait_units->links() }}
            </div>
        @else 
            <div class="alert alert-danger">
                {!! __('messages.no_records') !!}
            </div>
        @endif
    </div>
</div>

@once
    @push ('styles')
        @livewireStyles
    @endpush
    @push ('scripts')
        @livewireScripts                
    @endpush
@endonce
