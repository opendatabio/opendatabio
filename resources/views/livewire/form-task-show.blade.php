<div class="card mt-5" style="min-width: 50vw;">
    <div class="card-header">
        {{ __('messages.form_task')}} - {{$form_task->name }}        
    </div>
<div class="card-body">
    <div class="mb-3">
        <label for="name" class="form-label fw-bold ">
            @lang('messages.name')            
        </label>
        <div class="">
            {{ $form_task->name }}
        </div>        
    </div>  

    <div class="mb-3">
        <label for="name" class="form-label fw-bold ">
            @lang('messages.form')            
        </label>
        <div class="">
            {!! $form_task->form->rawLink() !!}
        </div>        
    </div>  

@if($datasets_list)
<div class="mb-3" >
  <label for="datasets" class="form-label fw-bold">
    @lang('messages.datasets')
  </label>  
  <div class="">    
    @foreach($datasets_list as $dt)
        {{ $dt['label']}}
    @endforeach
  </div>
  </div>
@endif


<div class="mb-3">
    <label class="form-label fw-bold">
        @lang('messages.status')
    </label>       
	<div class="">
        {{  $form_task->status }}      
    </div>
</div>

@if($form_task->progress_max)
<div class="mb-3">
    <label class="form-label fw-bold">
        @lang('messages.progress')
    </label>       
	<div class="">
        {!!  $form_task->progress_display !!}
    </div>
</div>
@endif

<div class="mb-3">
    <label class="form-label fw-bold">
        @lang('messages.authorized_users')
    </label>       
	<div class="">
        {!!  $form_task->authorized_users !!}
    </div>
</div>

@if($show_input_object)

@include('livewire.common.form-models')

@endif


@if (session()->has('errors'))
<div class="mb-3 alert alert-danger">
    {!! session('errors') !!}
</div>
@endif

@if (session()->has('success'))
<div class="mb-3 alert alert-success">
    {!! session('success') !!}
</div>
@endif

@can("update",$form_task)
@if(!$this->show_input_object)
<div class="row mb-3" >    
    @if($form_task->status=="defined")
    <div class='col-auto'>
        <button type='button' class="btn btn-success"
        wire:click="editTask" >
        @lang("messages.edit")
        </button>
    </div>
    <div class='col-auto'>
        <button type='button' class="btn btn-primary"
        wire:click="generateTask" >
        @lang("messages.prepare_task")
        </button>
    </div>   
    @endif
    @if($form_task->is_prepared)
    <div class='col-auto'>
        <button type='button' class="btn btn-success"
        wire:click="processTask" >
        @lang("messages.process_task")
        </button>
    </div>
    <div class='col-auto'>
        <button type='button' class="btn btn-secondary"
        wire:click="generateTask" >
        @lang("messages.update_task_objects")
        </button>
    </div>
        <div class='col-auto'>
            <button type='button' class="btn btn-primary"
            wire:click="toogleObjectInput" >
            @lang("messages.add_object_task")
            </button>
        </div>      
    @endif
    
</div>
@else 
<div class="row mb-3" >    
    <div class='col-auto'>
        <button type='button' class="btn btn-outline-secondary"
        wire:click="toogleObjectInput" >
        @lang("messages.clear_object_task")
        </button>
    </div>
</div>
@endif
@endcan


</div>  
</div>  


@once
    @push ('styles')
        @livewireStyles
    @endpush
    @push ('scripts')
        @livewireScripts
        @vite('resources/assets/js/custom.js')
    @endpush
@endonce