<div class="{{isset($number_datasets) ? 'mx-auto' : 'col-lg-6 col-md-6 col-sm-6 mx-auto'}}" >
    <div class="mt-3 mb-0 pb-2 text-center" >
        <h4>
          @if(isset($number_datasets)) 
            {{$number_datasets}}&nbsp;&nbsp;<a href="{{ url('datasets/'.$project.'/project')  }}" >@lang('messages.datasets')</a>
          @else 
          <a href="{{ url('datasets')}}" >@lang('messages.datasets')</a>
          @endif
        </h4>
    </div>
    <div class="row">          
          @foreach($datasets as $dataset)
                <div class="{{isset($number_datasets) ? 'col-lg-3 col-md-3 col-sm-8 col-auto me-5 mb-2' : 'col'}}  card p-3" >
                  <div class="row no-gutters">
                    @if(isset($logoUrl) or !isset($isproject) or (isset($isproject) and !$isproject))
                    <div class="col-auto">
                      @if(isset($logoUrl))
                        <img src="{{$logoUrl}}" class="img-fluid" alt="..." style='object-fit: cover; max-height: 150px;'>
                      @else
                        <div class='bg-warning text-dark p-3'style="max-width: 150px; min-height: 150px;">
                          <i class="fas fa-list fa-7x mt-2"></i>
                        </div>
                      @endif
                    </div>
                    @endif
                    <div class="col">
                      <div class="card-block">
                        <h5 class="card-title">{{ isset($dataset->title) ? $dataset->title : $dataset->name }}</h5>
                        <p class="card-text">
                          <div class="odb-hint">
                          @php
                          //$title = $dataset->title_link ? $dataset->title : $dataset->name;
                          //$ret =  "<strong style='cursor:pointer;'>".$title."</strong>";
                          $ret = "";
                          $description_brief = "";
                          if ($dataset->description) {
                            $description_brief = substr($dataset->description,0,100);
                            if (strlen($dataset->description)>400) {
                            $description_brief .= "...";
                            }                                   
                          //$ret .= ".  ".$description_brief;
                            $ret = $description_brief;
                          }
                          $dt = $dataset->versions()->where('dataset_access',0);
                          $versions = $dt->count() ? $dt->get()->map(function($q){ 
                            return $q->raw_link;
                          })->toArray() : [];                          
                          @endphp
                          {!! $ret !!}
                          </div>
                          @if ($versions) 
                          <div class='mt-2'>
                              @lang('messages.public_versions'): {!! implode(" | ",$versions) !!}
                          </div>                        
                          @endif
                          @if ($dataset->tags()->count())
                            <div class='mt-2'>
                              {!! $dataset->tagLinks !!}
                            </div>
                          @endif
                          @php
                            $lockimage= '<i class="fas fa-lock"></i>';
                            $license_logo = 'images/cc_srr_primary.png';
                            if ($dataset->privacy >= App\Models\Dataset::PRIVACY_REGISTERED) {
                              $lockimage= '<i class="fas fa-lock-open"></i>';
                              $license = explode(" ",$dataset->license);
                              $license_logo = 'images/'.mb_strtolower($license[0]).".png";
                            }
                          @endphp
                          <div class="pt-3">
                            <div class="row g-3">
                              <div class="col-auto">
                                <a href="{{ url('dataset-show').'/'.$dataset->id}}" class="btn btn-sm btn-outline-warning">
                                  @lang('messages.details')
                                </a>
                              </div>
                                <!--
                                  <div class="col-auto">
                                  <small>{!! $lockimage !!} @lang('levels.privacy.'.$dataset->privacy)</small>
                                <a href="http://creativecommons.org/license" target="_blank">
                                  <img src="{{ asset($license_logo) }}" alt="{{ $dataset->license }}" width='80px'>
                                </a>
                                </div>
                              -->                              
                            </div>
                          </div>
                        </p>

                      </div>
                    </div>
                  </div>
              </div>            
          @endforeach          
    </div>
    <div class='mt-1'>
    {{ $datasets->links() }}
    </div>
  </div>
