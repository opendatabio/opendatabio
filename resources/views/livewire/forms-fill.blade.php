<div class="card mt-5" style="min-width: 50vw;">
    <div class="card-header">
        @lang('messages.measurements_form',[
          'form_name' => $form->name
          ])        
    </div>
<div class="card-body">
@if (session()->has('stored'))
        <div class="mb-3 alert alert-success">
            {!! session('stored') !!}
        </div>
@endif

@include('livewire.common.form-models')

@if($measured_id)

@if($short_measured_type=="Taxon")
<div class="mb-3">
    <label for="location_id" class="form-label fw-bold">
        @lang('messages.location')
    </label>
    <a data-bs-toggle="collapse" href="#measurement_location_id" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="measurement_location_id" class="odb-hint collapse">
            @lang('messages.measurement_location_id')
        </div>
    @if($location_name)
    <div >
        {!! $location_name !!}
    </div>    
    @endif
    <div class='mt-2'>
      @livewire('input-search-bar', [
                'search_model' => "Location",
                'field_name' => "id",
                'return_field' => "name",
                'search_field' => "name",
                'return_id' => null,
                'query' => null,
                'emitup_function' => "mergeChildLocation",
                'nested_model' => null, 
                ]
            )     
    @error('location_id') <br> <span class="text-danger">{{ $message }}</span> @enderror
  </div>  
</div>
@endif

<div class="mb-3">
    <label for="date" class="form-label mandatory fw-bold">
      @lang('messages.measurement_date')
  </label>
    <a data-bs-toggle="collapse" href="#measurement_date_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="measurement_date_hint" class="odb-hint collapse">
      @lang('messages.measurement_date_hint')
    </div>
	  <div class="">
      @include('livewire.common.incompletedate')
    </div>  
    @error('date_year') <span class="text-danger">{{ $message }}</span> @enderror
</div>

<div class="mb-3" >
      <label for="authors" class="form-label mandatory fw-bold">
        @lang('messages.measurement_measurer')
      </label> 
      <div class="">    
      @livewire('input-search-bar', [
            'search_model' => "Person",
            'field_name' => "id",
            'return_field' => "full_name",
            'search_field' => "full_name",
            'return_id' => null,
            'query' => null,
            'emitup_function' => "mergeChildAuthor",
            'nested_model' => "",
            'multi_select' => true,
            'selected_models' => $authors,
            'search_placeholder' => "Busque aqui 1 ou + Pessoas",
            ]
        )
      </div>
      @error('authors') <span class="text-danger">{{ $message }}</span> @enderror           
</div>    

<div class="mb-3">
  <label for="dataset_id" class="form-label mandatory fw-bold">
    @lang('messages.measurement_dataset')
  </label>
  <a data-bs-toggle="collapse" href="#measurement_dataset_hint" class="odb-unstyle">
        <i class="far fa-question-circle"></i></a>
  <div id="measurement_dataset_hint" class="odb-hint collapse">
            @lang('messages.measurement_dataset_hint')
  </div>
  <div class="">  
    @if (count($user_datasets))    	
    	<select 
        wire:model.lazy="dataset_id" id="dataset_id" class="form-select" >
        <option value="" wire:key='datmnull'>@lang('messages.select')</option>
      	@foreach ($user_datasets as $dataset)
      		<option value="{{$dataset['id']}}" wire:key="dt{{$dataset['id']}}">
                  {{ $dataset['label'] }}
      		</option>
      	@endforeach
      </select>
    @else
      <div class="alert alert-danger">
        @lang ('messages.no_valid_dataset')
      </div>
    @endif
  </div>
  @error('dataset_id') <span class="text-danger">{{ $message }}</span> @enderror
</div>

@if($traits_list)   
    <div x-data="{ open: true }" class='mb-3'>
      <button type="button" class="btn btn-sm" @click="open = ! open">
        <i class="far fa-eye-slash fa-lg" x-show="open"></i>
        <i class="fa-regular fa-eye fa-lg" x-show="!open"></i>
        {{ count($traits_list )}} @lang('messages.traits')
      </button>  
      
      <table class='table table-striped' x-show="open">
            <tr>
                <th>@lang('messages.trait')</th>
                <th>@lang('messages.value')</th>  
                <th>@lang('messages.notes')</th>
                <th>@lang('messages.bibreference')</th>  

            </tr>
            @foreach($traits_list as $order => $trait) 
              <tr >                  
                  <td >
                    <strong class="{{ $trait['mandatory'] ? 'mandatory' : ''}}">{!! $trait['export_name'] !!} </strong>
                    <br>
                    <small> {{ $trait['name']}} </small>
                  </td>
                  <td>
                      @include("livewire.trait_elements.form")
                  </td>                  
                   
                  <td > 
                  <textarea 
                    wire:model.lazy='traits_list.{{$order}}.notes'                    
                    class='form-control'>    
                  </textarea>
                  </td>   
                  <td> 
                    @if ($trait['bibreference_name']) 
                      {!! $trait['bibreference_name']!!}
                      <br>
                    @endif
                    @livewire('input-search-bar', [
                      'search_model' => "BibReference",
                      'field_name' => "id",
                      'return_field' => "name",
                      'search_field' => "bibkey",
                      'return_id' => null,
                      'query' => null,
                      'emitup_function' => "mergeChildBib",
                      'nested_model' => "",
                      'trait_form_order' => $order,            
                      ],
                      key('variable-'.$order)
                    )                  
                  </td>                                                    
              </tr>
            @endforeach
      </table>
    </div>    
    @error('traits_list') 
      <div class='mb-3 alert alert-danger'> 
      {!! $message !!}
      </div> 
    @enderror                  
  @endif 

@if(count($confirm_missing))
<div class="mb-3 alert alert-danger">
  <div class="">
    @lang("messages.allow_missing_hint")
    <br>
    <button type='button' class="btn btn-dark"
      wire:click="allowMissing" >
      @lang("messages.allow_missing")
    </button>    
  </div>
</div>
@endif




  @if($confirm_duplicated)
<div class="mb-3 alert alert-danger">
  <label class="form-label fw-bold">
      @lang('messages.allow_duplicated')
  </label>
  <div class="">
  @if (session()->has('duperrors'))
      {!! session('duperrors') !!}      
  @endif    
  </div>  
  <div class="form-check">
      <input class="form-check-input" type="checkbox" value="1" 
      wire:model.lazy='allow_duplicated'>
      <label class="form-check-label" for="allow_duplicated">
        @lang('messages.import_duplicated_measurement')
      </label>
  </div>
</div>
@endif

<div class="mb-3" >
    <button type='button' class="btn btn-success"
      wire:click="submit" >
      @lang("messages.save")
    </button>    
</div>

@endif


</div>

</div>

@once
    @push ('styles')
        @livewireStyles
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.1/spectrum.min.css">
    @endpush
@push ('scripts')
        @livewireScripts
        @vite('resources/assets/js/custom.js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.1/spectrum.min.js"></script>
@endpush


@endonce