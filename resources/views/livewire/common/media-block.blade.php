<div class="row">
        @foreach ($media as $item)
        @php
            $fileUrl = $item->getUrl();
            if (file_exists($item->getPath('thumb'))) {
            $thumbUrl = $item->getUrl('thumb');
            } else {
            $thumbUrl = $fileUrl;
            }
        @endphp            
            <!-- Gallery item -->
        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-5 mb-4 me-3 p-3">
            <div class="card rounded shadow-sm" >
                @if(preg_match("/image/i",$item->mime_type))
                <img src="{{ $thumbUrl }}" 
                style="
                width: 100%;
                object-fit: cover;
                object-position: left bottom;
                cursor: pointer;
                " 
                wire:click="show({{$item->id}})"                
                >             
                @elseif (preg_match("/video/i",$item->mime_type))
                    <video width="100%" 
                    style="
                    height: 250px; 
                    object-fit: cover;
                    object-position: left bottom;"
                    controls
                    >
                    <source src="{{ $thumbUrl }}" type="{{ $item->mime_type }}">
                    Your browser does not support the video tag.
                    </video>            
                @elseif (preg_match("/audio/i",$item->mime_type))
                    <center >
                        <br><br>
                        <i class="fas fa-file-audio fa-6x"></i>
                        <br><br>
                        <audio controls >
                        <source src="{{ $thumbUrl }}"  type="{{ $item->mime_type }}"/>
                        </audio>
                    </center>
                
                @endif
                <div class='row px-1' >
                    <div class='col col-sm-auto col-lg-auto'>
                        {!! $item->gallery_citation() !!}
                        @if($item->tags->count())
                        <br>
                        <small class='text-end'>
                        {{__("messages.tags")}}:
                        {!!
                            implode(" | ",
                            $item->tags->map(function($k){
                                return $k->rawLink();
                            })->toArray(),);
                        !!}    
                        </small>                    
                        @endif                    
                    </div>  
                </div>  
                <div class='row px-1' >
                    <div class='col  col-sm-auto col-lg-auto text-nowrap'>
                        @can('update',$item)
                            <button class="btn btn-link p-1" wire:click="edit({{$item->id}})" >
                                <i class="fa-regular fa-pen-to-square text-secondary" ></i>
                            </button>                        
                        @endcan                    
                        <button class="btn btn-link p-1" wire:click="show({{$item->id}})" >
                                <i class="fa-regular fa-eye text-secondary" ></i>
                        </button>                        
                    </div>                                       
                </div>                
            </div>

        </div>
        @endforeach
    </div>
    <div class='row'>
    {{ $media->links() }}
    </div> 