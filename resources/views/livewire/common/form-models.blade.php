<div class="mb-3">
    <label for="measured_type" class="form-label mandatory fw-bold">
        @lang('messages.measured_what',['object_type' => __('classes.'.$short_measured_type)])
        @if($measured_name)
        <br>
            {!! $measured_name !!}
        @endif
    </label>      
    @if($this->from_form_task)  
    <div class="">
        @livewire('input-search-bar', [
            'search_model' => "FormTaskObject",
            'field_name' => "id",
            'return_field' => "label",
            'search_field' => "label",
            'return_id' => null,
            'query' => null,
            'emitup_function' => "mergeChildObject",
            'nested_model' => "", 
            'batch_identify' => false,
            'form_task_id' => $this->form_task->id,
            ]
        )
        </div>
    @else
    <div class="">
        @if($short_measured_type=="Individual")
        @livewire('input-search-bar', [
            'search_model' => "Individual",
            'field_name' => "id",
            'return_field' => "fullname",
            'search_field' => "fullname",
            'return_id' => null,
            'query' => null,
            'emitup_function' => "mergeChildObject",
            'nested_model' => "", 
            'batch_identify' => true,
            ]
        )
        @endif

        @if($short_measured_type=="Taxon")
            @livewire('input-search-bar', [
                'search_model' => "Taxon",
                'field_name' => "id",
                'return_field' => "fullname",
                'search_field' => "fullname",
                'return_id' => null,
                'query' => null,
                'emitup_function' => "mergeChildObject",
                'nested_model' => "", 
                ]
            )      
        @endif

        @if($short_measured_type=="Voucher")
        @livewire('input-search-bar', [
            'search_model' => "Voucher",
            'field_name' => "id",
            'return_field' => "fullname",
            'search_field' => "fullname",
            'return_id' => null,
            'query' => null,
            'emitup_function' => "mergeChildObject",
            'nested_model' => null, 
            ]
        )  
        @endif

        @if($short_measured_type=="Location")
        @livewire('input-search-bar', [
            'search_model' => "Location",
            'field_name' => "id",
            'return_field' => "name",
            'search_field' => "name",
            'return_id' => null,
            'query' => null,
            'emitup_function' => "mergeChildObject",
            'nested_model' => null, 
            ]
        )    
        @endif
    </div>
    @endif
    @error('measured_id') <span class="text-danger">{{ $message }}</span> @enderror
</div>
