<div class="row mb-3">
        <div class="col form-group">
          <label class="form-label">
              @lang('messages.taxon')
              <a data-bs-toggle="collapse" 
              href="#taxon_hint" 
              class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          </label>
          <div id="taxon_hint" class="odb-hint collapse">
      	       @lang('messages.individual_taxon_hint')
          </div>
          @if($selected_taxon)
            <div class='p-2 text-nowrap'>
              <i class="fa-solid fa-check text-success"></i>&nbsp;{{ $selected_taxon }}
            </div>    
          @endif  
          <div> 
                @livewire('input-search-bar', [
                  'search_model' => "Taxon",
                  'field_name' => "id",
                  'return_field' => "fullname",
                  'search_field' => "fullname",
                  'return_id' => null,
                  'query' => null,
                  'emitup_function' => "mergeChildTaxon",
                  'nested_model' => "", 
                  ]
                )
          </div> 
          @error('taxon_id') <span class="text-danger">{{ $message }}</span> @enderror

        </div>
        <div class="col ">
          <label for="modifier" class="form-label">
            @lang('messages.modifier')
          </label>
          <a data-bs-toggle="collapse" href="#modifier_hint" 
          class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          <div id="modifier_hint" class="odb-hint collapse">
            @lang('messages.individual_modifier_hint')
          </div>
          <select name="modifier" class="form-select" 
          aria-label="modifier" wire:model='modifier'>
            @foreach (App\Models\Identification::MODIFIERS as $key => $modifier)
                <option value="{{$modifier}}" wire:key="md_{{$key}}">
                  @lang('levels.modifier.' . $modifier)
                </option>
            @endforeach
          </select>
        </div>
        @error('modifier') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="row mb-3">   
        <div class="col form-group">
          <label class="form-label">
              @lang('messages.identifier')
              <a data-bs-toggle="collapse" href="#identifier_hint" 
              class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          </label>
          <div id="identifier_hint" class="odb-hint collapse">
               @lang('messages.individual_identifier_id_hint')
          </div>    
          @if($identifiers)
            <div >
              @foreach($identifiers as $order => $person) 
              <div class='text-nowrap'>
                  <button class='btn btn-sm text-danger'
                        wire:click="removeIdentifier({{$order}})">
                        <i class="fa-regular fa-trash-can"></i>
                    </button>
                    &nbsp;{{ $person['name'] }}
              </div>
              @endforeach
            </div>    
          @endif  
          <div >
              @livewire('input-search-bar', [
                  'search_model' => "Person",
                  'field_name' => "id",
                  'return_field' => "full_name",
                  'search_field' => "full_name",
                  'return_id' => null,
                  'query' => null,
                  'emitup_function' => "mergeChildIdentifier",
                  'nested_model' => "",  
                  ]
              )
          </div>
          @error('identifiers') <span class="text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="col form-group">
          <label class="form-label">
              @lang('messages.identification_date')
              <a data-bs-toggle="collapse" href="#identification_date_hint" 
              class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          </label>
          <div id="identification_date_hint" class="odb-hint collapse">
               @lang('messages.individual_identification_date_hint')
          </div>
            @include('livewire.common.incompletedate')
          @error('date_year') <span class="text-danger">{{ $message }}</span> @enderror
          @error('date_month') <span class="text-danger">{{ $message }}</span> @enderror
          @error('date_day') <span class="text-danger">{{ $message }}</span> @enderror
        </div>      
    </div>
    <div class="row mb-3">
  <div class='col'>
    <label for="identification_notes" class="form-label">
      @lang('messages.identification_notes')
    </label>
	  <textarea wire:model="identification_notes" class="form-control" row=4></textarea>
    @error('identification_notes') <span class="text-danger">{{ $message }}</span> @enderror

  </div>
  </div>

    <div class="row mb-3">
      <div class='col'>
        <label for="biocollection_id" class="form-label">
          @lang('messages.id_biocollection')
        </label>
        <a data-bs-toggle="collapse" href="#biocollection_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        <div id="biocollection_hint" class="odb-hint collapse">
          @lang('messages.individual_biocollection_id_hint')
        </div>
        @if($selected_biocollection)
        <div class='p-2 text-nowrap'>
          <i class="fa-solid fa-check text-success"></i>&nbsp;{{ $selected_biocollection }}&nbsp;
          <button class='btn btn-sm text-danger'
                        wire:click="removeBiocollection">
                        <i class="fa-regular fa-trash-can"></i>
          </button>                    
        </div>    
        @endif
        <div >
          @livewire('input-search-bar', [
              'search_model' => "Biocollection",
              'field_name' => "id",
              'return_field' => "acronym",
              'search_field' => "acronym",
              'return_id' => null,
              'query' => null,
              'emitup_function' => "mergeChildBiocollection",
              'nested_model' => "", 
              ]
          )
        </div>
        @error('biocollection_id') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
   @if($biocollection_id>0)
   <div class="col">
    <label for="biocollection_reference" class="form-label mandatory">
      @lang('messages.biocollection_reference')
    </label>
    <div class="">
      <input type="text" wire:model="biocollection_reference" class="form-control" >
    </div>
    @error('biocollection_reference') <span class="text-danger">{{ $message }}</span> @enderror

  </div>
  @endif
</div>

  @if($batchupdate)
  <div class="row mb-3"> 
  <div class="col form-check">
    <input type="checkbox" wire:model="update_nonself_identification" value=1 >
    <label for="update_nonself_identification" class="form-label">
      @lang('messages.individual_update_nonself_identification')
    </label>
    <a data-bs-toggle="collapse" href="#update_nonself_identification_hint" class="odb-unstyle">
      <i class="far fa-question-circle"></i></a>
    <div id="update_nonself_identification_hint" class="odb-hint collapse">
    @lang('messages.individual_update_nonself_identification_hint')
    </div>
  </div>
  </div>
  @endif
