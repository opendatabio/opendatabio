<div class='row'>
<select class="form-select col mx-1" wire:model="date_year">
@for ($i = config('app.max_year'); $i >= config('app.min_year'); $i --)
    <option value="{{$i}}" wire:key="idy_{{$i}}">
        {{$i}}
    </option>
@endfor
</select> 
<select class="form-select col mx-1" wire:model="date_month">
    <option value=0 wire:key="idm_0">
        @lang('messages.unknown_date')
    </option>
@for ($i = 1; $i <= 12; $i ++)
    <option value="{{$i}}" wire:key="idm_{{$i}}">
        {{str_pad($i, 2, '0',STR_PAD_LEFT)}}
    </option>
@endfor
</select> 
<select class="form-select col mx-1" wire:model="date_day">
    <option value=0 wire:key="idd_0">
        @lang('messages.unknown_date')
    </option>
@for ($i = 1; $i <= 31; $i ++)
    <option value="{{$i}}" wire:key="idd_{{$i}}">
        {{str_pad($i, 2, '0',STR_PAD_LEFT)}}
    </option>
@endfor
</select>
</div>