<div class="relative">
      @if($selected_models)
          <div>
            @foreach($selected_models  as $index => $tx)
            <button class='btn btn-sm text-nowrap p-2'
                wire:click="removeFilterModel({{$index}})">
                <i class="fa-regular fa-trash-can text-danger "></i>
                {{$tx['label']}}
            </button>            
            @endforeach
          </div>
      @endif
      <input type="text" id="return_id" 
          name="return_id" 
          wire:model="return_id" 
          hidden>
      <input
          type="text"

          class="form-control"

          placeholder="{{$search_placeholder ? $search_placeholder : 'Buscar...'}}"

          wire:model="query"

          wire:keydown.escape="resetBar"

          wire:keydown.Arrow-Up="decrementHighLight"

          wire:keydown.Arrow-Down="incrementHighlight"

          wire:keydown.enter="selectListOption(null)"

      />
      @if(!empty($query))
        <div class="absolute z-10 list-group bg-white">
            @if($list_of_options)
              @foreach($list_of_options as $i => $option)
                <div wire:mouseover="changeIndex({{$i}})" 
                class="form-control list-item {{ $highlightIndex==$i ? 'bg-info' : ''}}" 
                wire:click="selectListOption({{$i}})" 
                wire:key="item_{{$i}}" >
                  {{ $option[$return_field]}}
                </div>
              @endforeach
            @else
             @if(!$return_id)
              <div class="list-item">
                  Não encontrado!
              </div>
              @endif
          @endif
        </div>
      @endif
</div>
