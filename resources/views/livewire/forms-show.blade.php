<div class="card mt-5" style="min-width: 50vw;">
    <div class="card-header">
        <strong>{{ __('messages.form')}}</strong>        
        <span style='float: right;'>
        {{ __('messages.created_by')}}&nbsp;
        {{ $this->form_user->identifiableName() }}
        </span>
    </div>
<div class="card-body">
        <div class="mb-3">
            <label class="form-label fw-bold">
                @lang('messages.name')            
            </label>
            <div class="">
                {{$name}}
            </div>            
        </div>    


      <div class="mb-3">
        <label for="measured_type" class="form-label  fw-bold">
            @lang('messages.object_type')
        </label>   
        <div class="">
          {{ str_replace("App\\Models\\","",$form->measured_type) }}
        </div>
      </div>


      
        <div class="mb-3" x-data="{ open: false }">
          <label class="form-label fw-bold">
            @lang('messages.traits')
            &nbsp;
            <button type="button" class="btn btn-sm" @click="open = ! open">
              <i class="far fa-eye-slash fa-lg" x-show="open"></i>
              <i class="fa-regular fa-eye fa-lg" x-show="!open"></i>
              {{ count($traits_list )}} @lang('messages.traits')
            </button>  
      
          </label> 
          @if($traits_list)   
          <div>
          <table class='table table-striped' x-show="open">
                <tr>
                    <th>@lang('messages.export_name')</th>
                    <th>@lang('messages.trait')</th>
                    <th>@lang('messages.order')</th>  
                    <th>@lang('messages.mandatory')</th>  
                    <th>@lang('messages.validation_type')</th>                   
                </tr>
                @foreach($traits_list as $order => $trait) 
                  <tr>
                      <td> {!! $trait['raw_link'] !!}</td>
                      <td> {{ $trait['name'] }}</td>
                      <td> {{ $order}} </td>
                      <td> {{ $trait['mandatory'] ? 'x' : '' }}</td>
                      <td> {{ __("messages.".$trait['validation']) }}</td>
                  </tr>
                @endforeach
                </table>
                </div>                      
          @endif          
        </div>    
        @if($notes)
        <div class="mb-3"  >
          <label for="notes" class="form-label fw-bold">
            @lang('messages.notes')
          </label>
          <div class="">         
            {{$notes}}
            </div>
        </div>
        @endif

        @can('update',$form)
        <div class="row mb-3" >
          <div class='col-auto'>
            <button class="btn btn-primary"
              wire:click="editForm" >
              @lang("messages.edit")
            </button>
          </div> 
          
          <div class='col-auto'>
            <button class="btn btn-secondary"
              wire:click="createTask" >
              @lang("messages.create_task")
            </button>
          </div>  

          <div class='col-auto'>
            <button class="btn btn-success"
              wire:click="useForm" >
              @lang("messages.use_this_form")
            </button>
          </div>  
           
        </div> 
        @endcan

@if($form->form_tasks->count())
        <div class='mb-3 ' x-data="{ open: true }">
        <button type="button" class="btn btn-sm" @click="open = ! open">
          <i class="far fa-eye-slash fa-lg" x-show="open"></i>
          <i class="fa-regular fa-eye fa-lg" x-show="!open"></i>
          @lang('messages.form_tasks')
        </button>  
          <div class="mt-2">   
          <table class='table table-striped' x-show="open">
                <tr>
                    <th>@lang('messages.actions')</th>  
                    <th>@lang('messages.name')</th>
                    <th>@lang('messages.created_at')</th>
                    <th>@lang('messages.status')</th>  
                    <th>@lang('messages.progress')</th>  
                </tr>
                @foreach($form->form_tasks as $task) 
                  <tr>
                      <td>
                        @can("delete",$task)                        
                        <button class='btn btn-sm text-danger'
                        wire:click="deleteTask({{$task->id}})" >
                        <i class="fa-regular fa-trash-can"></i>
                        </button>
                        @endcan

                        @can("fill",$task)                        
                        <button class='btn btn-sm text-success'
                        wire:click="processTask({{$task->id}})" >
                        <i class="fa-regular fa-square-plus"></i>
                        </button>
                        @endcan

                        
                      </td>
                      <td> {!! $task->rawLink() !!}</td>
                      <td> {{ $task->created_at }}</td>
                      <td> {{ $task->status }} </td>
                      <td> {!! $task->progess_display !!} </td>
                      
                  </tr>
                @endforeach
                </table>
          </div>  
        </div>  
        @endif

        @if (session()->has('errors'))
            <div class="mb-3 alert alert-danger">
                {!! session('errors') !!}
            </div>
        @endif
        @if (session()->has('success'))
            <div class="mb-3 alert alert-success">
                {!! session('success') !!}
            </div>
        @endif
      </div>
    </div>

  </div>

@once
    @push ('styles')
        @livewireStyles
    @endpush
    @push ('scripts')
        @livewireScripts
        @vite('resources/assets/js/custom.js')
    @endpush
@endonce