@if($theprojects->count())
<div class="col-lg-6 col-md-6 col-sm-6 mx-auto" >
  <div class="mt-3 mb-0 pb-2 text-center" >
      <h4><a href="{{ url('projects') }}" >
        @lang('messages.projects')</a></h4>
  </div>
  <div class="row">
        @foreach($theprojects as $project)
            @php
            $logoUrl = $project->logo_url;
            @endphp
              <div class="col card p-3" style='min-height: 150px;'>
                <div class="row no-gutters">
                  <div class="col-auto">
                    @if(isset($logoUrl))
                      <img src="{{$logoUrl}}" class="img-fluid" alt="..." style='object-fit: cover; max-height: 150px;'>
                    @else
                      <div class='bg-secondary text-white p-3'style="max-width: 150px; min-height: 150px;">
                      <i class="fa-solid fa-diagram-project fa-6x"></i>
                      </div>
                    @endif

                  </div>
                  <div class="col">
                    <div class="card-block">
                      <h5 class="card-title">
                        {{ isset($project->title) ? $project->title : $project->name }}</h5>
                      <p class="card-text">
                        @if(isset($project->description))
                        <div class="odb-hint">
                        {!! substr($project->description,0,100) !!}
                        ...
                        </div>
                        @endif
                        <div class='mt-2'>
                          <a href="{{ route('project.show',['project' => $project->id]) }}" 
                          class="btn btn-sm btn-outline-primary">
                            @lang('messages.details')
                          </a>
                          @if ($project->tags()->count())
                            &nbsp;&nbsp;
                            {!! $project->tagLinks !!}
                          @endif
                        </div>
                    </p>

                    </div>
                  </div>
                </div>
            </div>
        @endforeach
  </div>
  <div class='mt-1'>
    {{ $theprojects->links() }}
  </div>
</div>
@endif
