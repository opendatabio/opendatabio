<div >
<div class="card mt-3">
    <div class="card-header">
        @lang('messages.dataset')
        @if(isset($dataset))
            : <strong>{{$dataset->name}}</strong>
        @endif
        @can('update', $dataset)
            <span class="history" style="float:right">
            <a href="{{url('datasets/'.$dataset->id.'/activity')}}">
            @lang ('messages.see_history')
            </a>
            </span>
        @endcan
    </div>
      @php
        $lockimage= '<i class="fas fa-lock"></i>';
        if ($dataset->privacy >= App\Models\Dataset::PRIVACY_REGISTERED) {
          $lockimage= '<i class="fas fa-lock-open"></i>';          
        }
      @endphp
      <div class="card-body">
        <h4>
          {{$dataset->title}}
        </h4>
        <span style='float:right;'>
          <h2>
            {{ $dataset->downloads }}
          </h2>
          <small>Downloads</small>
        </span>
        @if (isset($dataset->description))
        <p>
          {{ $dataset->description }}
        </p>
        @endif

        @if ($dataset->tags->count())
        <br>
        <p>
          <strong>
            @lang('messages.tagged_with')
          </strong>:<br>
          {!! $dataset->tagLinks !!}
        </p>
        @endif

       

        @can('update',$dataset)
           @if ($dataset_versions)
            <p>
              <strong>@lang('messages.accessible_versions')</strong>:
              @foreach($dataset_versions as $ver) 
                  <br>
                  &nbsp;&nbsp;{!! $ver['raw_link'] !!} - @lang('messages.from') {{ $ver['date'] }}
              @endforeach              
            </p>            
          @endif
        @else 
          @if ($dataset_versions_open)
            <p>
              <strong>@lang('messages.published_versions')</strong>:
              <ul style="list-style-type: none;">
              @foreach($dataset_versions_open as $ver) 
                <li>
                  {!! $ver['raw_link'] !!} - @lang('messages.from') {{ $ver['date'] }}
                </li>
              @endforeach
              </ul>
            </p>
          @else 
          <p>
              <strong>@lang('messages.published_versions')</strong>:
              <br>
              <span class='text-danger'>
              @lang('messages.none')
              </span>
            </p>
          @endif
        @endif


        <!--
        @if (count($dataset->data_type)>0)
        <p>
          @can('export', $dataset)
            @if(Auth::user()->can('update',$dataset) or count($dataset_versions_open)==0)
            <a href="{{ url('datasets/'.$dataset->id.'/download') }}" 
            class="btn btn-sm btn-success">
              <i class="fa-solid fa-download"></i>&nbsp;
              @lang('messages.data_download')
            </a>
            @endif
          @endcan
          @if(count($dataset_versions_open)==0)
          <a href="{{ url('datasets/'.$dataset->id.'/request') }}" 
              class="btn btn-sm btn-warning">
                <i class="fa-solid fa-download"></i>&nbsp;
                @lang('messages.data_request')
              </a>
            @if ($dataset->privacy < App\Models\Dataset::PRIVACY_REGISTERED)
            
            @else
              <a href="{{ route('login') }}" class="btn btn-sm btn-warning">
                <i class="fa-solid fa-triangle-exclamation"></i>&nbsp;
                @lang('messages.download_login')
              </a>
            @endif
          @endif
        </p>
        @endif
        -->
        <!--
        @if ($dataset->policy)
          <p>
            <strong>
              @lang('messages.data_policy')
            </strong>:
            <br>
            {{$dataset->policy}}
          </p>
        @endif
        -->

    <div class="row" id='odb-delete-confirmation-form' style="display: none;">
      <div class="col-auto alert alert-danger">
        @lang('messages.everything_will_be_deleted')
        @lang('messages.action_not_undone')
        <form action="{{ url('datasets/'. $dataset->id)  }}" method="POST" >
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
          <button type="submit" class="btn btn-danger btn-sm mt-3" title="@lang('messages.remove')">
            <i class="far fa-trash-alt"></i>
            @lang('messages.remove')
          </button>
        </form>
      </div>
    </div>
        

<!-- BUTTONS -->
<nav class="navbar navbar-light navbar-expand-lg" 
    id="buttons-navbar">
  <div class="container-fluid">
      <div class="navbar-header">
        <button class="navbar-toggler" type="button" 
        data-bs-toggle="collapse" 
        data-bs-target="#navbarbuttons" 
        aria-controls="navbarbuttons" 
        aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="navbar-collapse collapse flex-column ml-lg-0 ml-3" id="navbarbuttons">
        <ul class="nav navbar-nav flex-row">
        @can ('update', $dataset)
          <li class="nav-item px-2 mb-2">
            <a href="{{ url('datasets/'. $dataset->id. '/edit')  }}" class="btn btn-success" name="submit" value="submit">
              @lang('messages.edit')
            </a>
          </li>          
          <li class="nav-item px-2 mb-2">
            <a href="{{ Route('dataset.version.create',[ 'dataset_id'=> $dataset->id ] ) }}" 
            class="btn btn-success" 
            data-toggle="tooltip" title="Publish a version">
            @lang('messages.new_version')
            </a>
          </li>         
        @endcan  
        @can ('delete', $dataset) 
        <li class="nav-item px-2 mb-2">
          <button class="odb-delete-confirmation-button btn btn-danger" type='button' >
            @lang('messages.remove')
          </button>
        </li>
        @endcan
        

          <li class="nav-item px-2 mb-2">
            <button type="button" class="btn btn-outline-secondary"
            wire:click="summarizeDataset" wire:loading.remove  >     
              @lang('messages.dataset_summary')
            </button>
            <button type="button" class="btn text-danger" 
            wire:loading wire:target="summarizeDataset">
              <i class="fa-solid fa-spinner fa-spin"></i>&nbsp;@lang('messages.be_patient')
            </button>
          </li>

          <li class="nav-item px-2 mb-2">
          <button type="button" class="btn btn-outline-secondary" 
            wire:click="summarizeTaxons" wire:loading.remove  >            
            @lang('messages.taxonomic_summary')
          </button>
          <button type="button" class="btn text-danger" 
          wire:loading wire:target="summarizeTaxons">
            <i class="fa-solid fa-spinner fa-spin"></i>&nbsp;@lang('messages.be_patient')
          </button>
          </li>

          <li class="nav-item px-2 mb-2">
          <button type="button" 
            class="btn btn-outline-secondary" 
            wire:click="showReferences">
            <i class="fas fa-book-open"></i>
            @lang('messages.references')
        </button>
          </li>

          <li class="nav-item px-2 mb-2">
            <button type="button" 
                class="btn btn-outline-secondary"
                wire:click="showPeople">
                <i class="fa-solid fa-users"></i>
                @lang('messages.persons')
            </button>
          </li>

          <li class="nav-item px-2 mb-2">
          <a href="{{ url('measurements/'. $dataset->id. '/dataset')  }}" 
            class="btn btn-outline-secondary" >
                <i class="fa fa-btn fa-search"></i>
                @lang('messages.measurements')
          </a>
          </li>
          <li class="nav-item px-2 mb-2">
            <a href="{{ url('individuals/'. $dataset->id. '/dataset')  }}" 
                class="btn btn-outline-secondary" >
                <i class="fa fa-btn fa-search"></i>
                @lang('messages.individuals')
              </a>
          </li>
          <li class="nav-item px-2 mb-2">
          <a href="{{ url('vouchers/'. $dataset->id. '/dataset')  }}" 
          class="btn btn-outline-secondary" 
          >
                <i class="fa fa-btn fa-search"></i>
                @lang('messages.vouchers')
          </a>
        </li>
        <li class="nav-item px-2 mb-2">
          <a href="{{ url('media/'. $dataset->id. '/datasets')  }}" 
          class="btn btn-outline-secondary" 
          >
                <i class="fa fa-btn fa-search"></i>
                @lang('messages.media_files')
          </a>
        </li>
        <li class="nav-item px-2 mb-2">
            <a href="{{ url('locations/'. $dataset->id. '/dataset')  }}" 
            class="btn btn-outline-secondary" 
            >
                  <i class="fa fa-btn fa-search"></i>
                  @lang('messages.locations')
            </a>
          </li>
          <li class="nav-item px-2 mb-2">
            <a href="{{ url('taxons/'. $dataset->id. '/dataset')  }}" 
            class="btn btn-outline-secondary" >
                  <i class="fa fa-btn fa-search"></i>
                  @lang('messages.taxons')
            </a>
          </li>
    </ul>
  </div>
</div>
</nav>



</div>
</div>

@if($show_data_summary)
  {!! $data_summary !!}    
@endif

@if($show_taxonomic_summary)
  {!! $taxonomic_summary !!}    
@endif


<!-- START dataset people block -->
@if($show_people)
<div id='people_list' class="card mt-3" x-data="{ open: true }">
    <div class="card-header">
        <strong>
          @lang('messages.persons')
        </strong>
        &nbsp;
        <button type="button" class="btn btn-sm" @click="open = ! open">
            <i class="far fa-eye-slash fa-lg" x-show="open"></i>
            <i class="fa-regular fa-eye fa-lg" x-show="!open"></i>
        </button>
    </div>
    <div class="card-body" x-show="open" >
      <table class="table table-striped user-table">
      <thead>
        <th>@lang('messages.email')</th>
        <th>@lang('messages.name')</th>
        <th>@lang('messages.role')</th>
      </thead>
      <tbody>
      @foreach($dataset->people as $role => $list)
      @foreach($list as $member)
      <tr>
      <td class="table-text">
          {{ $member[0] }}
      </td>
      <td class="table-text">
          {{ $member[1] }}
      </td>
      <td class="table-text">
          {{ $role }}
      </td>
      </tr>
      @endforeach
      @endforeach
      </tbody>
      </table>
  </div>
</div>
@endif


<!-- START dataset summary BLOCK -->



<!-- IDENTIFICATIONS SUMMARY PANEL DO NOT CHANGE THIS LINE  MAY BREAK jquery below-->

<!-- start REFERENCE BLOCK -->
@if($show_references)
<div id='references_list' class="card mt-3" x-data="{ open: true }">
<div class="card-header">
    <strong>@lang('messages.dataset_bibreference')</strong>
    &nbsp;    
    <button type="button" class="btn btn-sm" @click="open = ! open">
        <i class="far fa-eye-slash fa-lg" x-show="open"></i>
        <i class="fa-regular fa-eye fa-lg" x-show="!open"></i>
    </button>
</div>
<div class="card-body" x-show="open" >
{!! View::make('common.bibreferences')->with([
    'bibreferences' => isset($dataset->references) ? $dataset->references : null,
]) !!}
</div>
</div>
@endif
<!-- END REFERENCE BLOCK -->

</div>

@once
    @push ('styles')
        @livewireStyles
    @endpush
    @push ('scripts')
        @livewireScripts
        <script>
          Livewire.on('changeFocus', (ev) => {
            const el = window.document.getElementById(ev);
            el.scrollIntoView({
              behavior: 'smooth',
              block: 'start',
            });            
          });          
        </script>
        @vite('resources/assets/js/custom.js')
    @endpush
@endonce