<div>

<div style='min-width: 100vw;' >  
@if($project->banner_url) 
<div class="odb-top-img">  
  <div class="odb-top-img-overlay" >
    <div class="text-center" style="text-shadow: 2px 2px LightYellow; font-weight: bold; font-family: Arial, Helvetica, sans-serif;">
          <h1 class="display-2 mt-0 p-4">{{ $project->name }}</h1>
          <div class="display-3">
            {{ $project->acronym }}
        </div>
    </div>
  </div>
</div>
@else
<div class="row mx-5 mt-5" >
  @if($project->logo_url)
  <div class='col-lg-2 col-md-12 col-sm-12 text-center pb-5'>
    <img src='{{ $project->logoUrl }}' width='150'>
  </div>
  <div class='col-lg-10 col-md-12 col-sm-12'>
  @else 
  <div class='col-lg-12 col-md-12 col-sm-12'>
  @endif
    <div class="text-center" style="font-weight: bold; font-family: Arial, Helvetica, sans-serif;">
          <h1 class="display-4">{{ $project->name }}</h1>
          <div class="display-5">
            {{ $project->acronym }}
        </div>
    </div>
  </div>
</div>
@endif
<div class="text-center p-4" style='width: 100%;'>
      {{ $project->description }}
      <br><br>             
      @if($project->urls)
        {!! $project->external_links !!}
        <br><br>          
      @endif 
      {!! View::make('projects.navbar')->with([
        'project' => $project,
      ]) !!}
</div>
@if($project->pages)
<hr class='myhr'>
<div class='row justify-content-center text-center'>
  <div class='col mx-5 '>
      {!! $project->pages[App::getLocale()] !!}
  </div>
</div>
@endif

@if($project->datasets->count())
<hr class='myhr'>
<div class="row col-lg-10 offset-lg-1 g-3 text-center justify-content-center mb-3">
  <!-- DATASETS -->
  <livewire:datasets-cards :project="$project->id">

</div>
@endif

@if($media)
<hr class='myhr'>
<div class='row justify-content-center'>
  <div class="mt-3 mb-3 text-center" >
      <h3>
        {{$media_count}}&nbsp;&nbsp;
      <a href="{{ route('media.project.list',['project_id' => $project->id])}}" >
        @lang('messages.media_files')</a>
      </h3>
  </div>
</div>
<div class='row mb-3 justify-content-center'>
  <div class="col-lg-10 offset-lg-2 mx-4">
    @include('livewire.common.media-block')
  </div>
</div>
@endif

@if($project->tags->count())
<hr class='myhr'>
<div class='row mb-3 text-center'>
  <div class='col text-nowrap'>
  {!! $project->tagLinks !!}
  </div>
</div>         
@endif    
<!-- footer image -->
@php
$footerimgs = $project->media()->where('file_name','like','%footerimg%')->get();
@endphp
@if($footerimgs->count())
<div class='row mx-4 align-items-center justify-content-center'>
  @foreach($footerimgs as $img)
  <div class='col-auto m-auto p-3' >
    <img src='{{  $img->getUrl() }}' height='120px'>
  </div>
  @endforeach
</div>
<!-- <div class="odb-bottom-img"></div> -->
@endif
  
         
</div>

@once
    @push ('styles')
        @livewireStyles
    @endpush
    <style media="screen">    
      .myhr {
          width: 90%;
          margin: 2em auto; /* centred margin */
          height: 1px; /* thickness of the line */
          background-color: gray; /* colour of the line */
          border: none; /* no border */
      }  
      @if($project->banner_url)
      .odb-top-img {
          position: relative;
          padding-bottom: 2rem;
          background-image: url("{{url($project->banner_url)}}");
          background-size: cover;
          background-repeat: no-repeat;
          height: 45vh;
          width: 100%;
      }
      @endif
      
      @if($project->footerimg_url)
      .odb-bottom-img {
          position: relative;
          padding-bottom: 2rem;
          background-image: url("{{url($project->footerimg_url ? url($project->footerimg_url) : null)}}");
          background-size: cover;
          background-repeat: no-repeat;
          height: 20vh;
          width: 100%;
      }
      @endif

      .odb-top-img-overlay {
          position: relative;
          z-index: 1;
      }
    </style>
    @push('scripts')
      @livewireScripts
    @endpush

@endonce
</div>
