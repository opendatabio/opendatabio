<div class="mt-1 mb-3" x-data="{ open: false }">
<div >
  <label class="form-label text-nowrap">
    @lang('messages.filter_data')
    <button type="button" class="btn btn-sm" @click="open = ! open">
    <i class="far fa-eye-slash fa-lg  " x-show="open"></i>
    <i class="fa-regular fa-eye fa-lg" x-show="!open"></i>
    </button>
    <a data-bs-toggle="collapse" 
              href="#filter_data_hint" class="odb-unstyle">
              <i class="far fa-question-circle"></i>
    </a>  
  </label>
  <div id="filter_data_hint" class="odb-hint collapse">
          @lang('messages.filter_data_hint')
  </div>
</div>
<div x-show="open" class="bg-info bg-opacity-25 p-2">
  <!--- EXPORT FORM hidden -->
    <div class="row mb-3">
        <div class="col form-group">
          <label class="form-label">
              @lang('messages.taxon')
              <a data-bs-toggle="collapse" 
              href="#filter_taxon_hint" 
              class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          </label>
          <div id="filter_taxon_hint" class="odb-hint collapse">
      	       @lang('messages.filter_taxon_hint')
          </div>
          <div> 
                @livewire('input-search-bar', [
                  'search_model' => "Taxon",
                  'field_name' => "id",
                  'return_field' => "fullname",
                  'search_field' => "fullname",
                  'return_id' => null,
                  'query' => null,
                  'emitup_function' => "mergeChildFilterTaxon",
                  'nested_model' => "",
                  'multi_select' => true,
                  'selected_models' => $selected_taxons,
                  ]
                )
          </div> 
        </div>
        <div class="col form-group">
          <label class="form-label">
              @lang('messages.location')
              <a data-bs-toggle="collapse" href="#location_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>              
          </label>
          <div id="location_hint" class="odb-hint collapse">
               @lang('messages.filter_location_hint')
          </div>          
          <div >
              @livewire('input-search-bar', [
                  'search_model' => "Location",
                  'field_name' => "id",
                  'return_field' => "name",
                  'search_field' => "name",
                  'return_id' => null,
                  'query' => null,
                  'emitup_function' => "mergeChildFilterLocation",
                  'nested_model' => "",
                  'multi_select' => true,
                  'selected_models' => $selected_locations,
                  ]
              )
          </div>
        </div>
      </div>
      <div class="row mb-3">
      <div class="col form-group">
        <label class="form-label">
              @lang('messages.collectors')
              <a data-bs-toggle="collapse" href="#filter_persons_hint" 
              class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          </label>
          <div id="filter_persons_hint" class="odb-hint collapse">
               @lang('messages.filter_persons_hint')
          </div>          
          <div >
              @livewire('input-search-bar', [
                  'search_model' => "Person",
                  'field_name' => "id",
                  'return_field' => "full_name",
                  'search_field' => "full_name",
                  'return_id' => null,
                  'query' => null,
                  'emitup_function' => "mergeChildFilterPerson",
                  'nested_model' => "",
                  'multi_select' => true,
                  'selected_models' => $selected_persons,
                  ]
              )
          </div>
        </div>

        <div class="col form-group">
        <label class="form-label">
              @lang('messages.traits')
              <a data-bs-toggle="collapse" href="#filter_persons_hint" 
              class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          </label>
          <div id="filter_persons_hint" class="odb-hint collapse">
               @lang('messages.filter_trait_hint')
          </div>          
          <div >
              @livewire('input-search-bar', [
                  'search_model' => "ODBTrait",
                  'field_name' => "id",
                  'return_field' => "name",
                  'search_field' => null,
                  'return_id' => null,
                  'query' => null,
                  'emitup_function' => "mergeChildFilterTrait",
                  'nested_model' => "",
                  'multi_select' => true,
                  'selected_models' => $selected_traits,
                  ]
              )
          </div>
        </div>

        </div>


      <div class="row mb-3">
        <div class="col form-group">
          <label class="form-label">
              @lang('messages.date')
              <a data-bs-toggle="collapse" href="#filter_date_hints" 
              class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          </label>
          <div id="filter_date_hints" class="odb-hint collapse">
               @lang('messages.filter_date_hints')
          </div>
          <div class="row">
            <div class="col">
            @lang('messages.minimum')&nbsp;
            <input class="form-control" type="date" 
            wire:model="filter_min_date" >
            </div>
            <div class="col">
            @lang('messages.maximum')&nbsp;
            <input class="form-control" type="date"             
            wire:model="filter_max_date" >
            </div>
          </div>
        </div>      
      </div>          
</div>
</div>
