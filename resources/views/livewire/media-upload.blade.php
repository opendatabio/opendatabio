<div class="card" style="min-width: 60vw;">
<div class="card-header">
    @lang('messages.media_upload')
    &nbsp;
    <a data-bs-toggle="collapse" href="#help" class="odb-unstyle">
        <i class="far fa-question-circle"></i></a>
    <div id="help" class="odb-hint collapse">
        @lang('messages.media_upload_hint')
    </div>
</div>
<div class="card-body">
<form wire:submit.prevent="uploadFiles">
@if (session()->has('success'))
    <div class="alert alert-success">
        {!! session('success') !!}
    </div>
@endif
@if (session()->has('errors'))
    <div class="alert alert-danger">
        {!! session('errors') !!}
    </div>
@endif
{{ csrf_field() }}

<div class="row mb-3">
  <label for="imageattributes" class="form-label mandatory">
      @lang('messages.media_attribute_table')
  </label>
  <a data-bs-toggle="collapse" href="#hint_img_attributes" class="odb-unstyle">
    <i class="far fa-question-circle"></i></a>
  <div id="hint_img_attributes" class="odb-hint collapse">
        @lang('messages.media_attribute_table_hint',[
            'validlicenses' => implode(' | ',config('app.creativecommons_licenses'))
        ])
  </div>
  <div class="">
    <input type="file" wire:model="media_attribute_table" class='form-control'>
    @error('media_attribute_table') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
</div>
    <div class="row mb-3">
        <div class="col form-group">
            <button class='btn btn-primary' type="submit">{{__('messages.save')}}</button>    
        </div>
    </div>
    <div class="row mb-3">
        <div class="col form-group">
        <label for="uploadedFiles" class="form-label mandatory">
            @lang('messages.media_files') 
        </label>
        <a data-bs-toggle="collapse" href="#media_files_hint" class="odb-unstyle">
            <i class="far fa-question-circle"></i>
        </a>
        @error('uploadedFiles') <span class="text-danger">{{ $message }}</span> @enderror
        <x-filepond wire:model="uploadedFiles" multiple />
        </div>
    </div>
</form>    
    @if(Auth::user() and Auth::user()->access_level==2)
    <div class="row mb-3">
        <div class="col form-group">
            <span class='text-muted'>This is for uploading a very large number of files. Only SUPER-ADMIN users have access here. 
            Images must be placed in folder <code>storage/app/img_temp</code> 
            </span><br>
            <button class='btn btn-primary' type="button" wire:click='manualBatchUpload'>
                {{__('messages.save')}}</button>    
        </div>
    </div>
    @endif
</div>
</div>


@once
    @push ('styles')
        @livewireStyles
    @endpush
    @push ('scripts')
        @livewireScripts                
    @endpush
@endonce