<div class="card mt-5" style="min-width: 50vw;">
    <div class="card-header">
        {{ __('messages.form_task')}}
        <a data-bs-toggle="collapse" href="#forms_hint" class="odb-unstyle">
        <i class="far fa-question-circle"></i></a>
        <div id="forms_hint" class="odb-hint collapse">
                  @lang('messages.form_task_hint')
        </div>
    </div>
<div class="card-body">
    <div class="mb-3">
        <label for="name" class="form-label fw-bold mandatory">
            @lang('messages.name')            
        </label>
        <div class="">
        <input type="text" class='form-control' 
            wire:model.lazy="name" >
        </div>
        @error('name') <span class="text-danger">{{ $message }}</span> @enderror
    </div>  


<div class="mb-3" >
  <label for="datasets" class="form-label mandatory fw-bold">
    @lang('messages.datasets')
  </label> 
  <a data-bs-toggle="collapse" href="#form_task_dataset_hint" class="odb-unstyle">
  <i class="far fa-question-circle"></i></a>
  <div id="form_task_dataset_hint" class="odb-hint collapse">
      @lang('messages.form_task_dataset_hint')
  </div>
  @error('datasets_list') <br>
   <span class="text-danger">{{ $message }}</span> @enderror
  <div class="mb-3">    
    @livewire('input-search-bar', [
            'search_model' => "Dataset",
            'field_name' => "id",
            'return_field' => "name",
            'search_field' => "name",
            'return_id' => null,
            'query' => null,
            'emitup_function' => "mergeChildDataset",
            'nested_model' => "",
            'multi_select' => true,
            'selected_models' => $datasets_list,
            'search_placeholder' => "Busque aqui 1 ou + Datasets para extrair os objetos para medir",
            ]
    )
  </div>
  </div>


<div class="mb-3" >
  <label class="form-label mandatory fw-bold">
    @lang('messages.authorized_users')
  </label> 
  <a data-bs-toggle="collapse" href="#form_task_users_hint" class="odb-unstyle">
  <i class="far fa-question-circle"></i></a>
  <div id="form_task_users_hint" class="odb-hint collapse">
      @lang('messages.form_task_users_hint')
  </div>
  @error('users_list') <br>
   <span class="text-danger">{{ $message }}</span> @enderror
  <div class="mb-3">    
    @livewire('input-search-bar', [
            'search_model' => "User",
            'field_name' => "id",
            'return_field' => "name",
            'search_field' => "name",
            'return_id' => null,
            'query' => null,
            'emitup_function' => "mergeChildUser",
            'nested_model' => "",
            'multi_select' => true,
            'selected_models' => $users_list,
            'search_placeholder' => "Busque aqui 1 ou + usuários autorizados em editar a tarefa",
            ]
    )
  </div>
  </div>

<div class="mb-3" >
    <button class="btn btn-success"
    wire:click="submit" >
    @lang("messages.save")
    </button>
</div>
</div>  
</div>  


@once
    @push ('styles')
        @livewireStyles
    @endpush
    @push ('scripts')
        @livewireScripts
        @vite('resources/assets/js/custom.js')
    @endpush
@endonce