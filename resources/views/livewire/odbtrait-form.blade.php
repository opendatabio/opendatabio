<div class="card mt-5" style="min-width: 50vw;">
    <div class="card-header">
        @lang('messages.new_trait')
        &nbsp;&nbsp;
        <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        <div id="help" class="odb-hint collapse">
            <hr>
            @lang('messages.hint_trait_create')
        </div>
    </div>

     <div class="card-body">
     <form wire:submit.prevent="submit">     
        {{ csrf_field() }}

<div class="mb-3">
    <label for="export_name" class="form-label mandatory fw-bold">
        @lang('messages.export_name')
    </label>
    <a data-bs-toggle="collapse" href="#trait_export_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="trait_export_hint" class="odb-hint collapse">
        @lang('messages.trait_export_hint')
    </div>
    <div class="">
        <input type='text' name="export_name" class="form-control" wire:model.lazy='export_name'
        {{ !$canchange ? ' disabled ' : ''}}
        >
    </div>
    @error('export_name') <span class="text-danger">{{ $message }}</span> @enderror
</div>

<!-- TRAIT NAME AND DESCRIPTION TRANSLATIONS -->
<div class="mb-3">
    <label for="export_name" class="form-label mandatory fw-bold">
        @lang('messages.name')
    </label>  
    @error('name')  <br> <span class="text-danger">{{ $message }}</span> @enderror
  @foreach ($languages as $language)
  <div class=''>
    <label class="form-label">
        <small>{{$language->name}}</small>
    </label>
    <input type='text' class='form-control' wire:model.lazy="name.{{$language->id}}">
    </div>
    @endforeach

</div>

<div class="mb-3">
    <label for="export_name" class="form-label mandatory fw-bold">
        @lang('messages.description')
    </label>  
    @error('description') <br> <span class="text-danger">{{ $message }}</span> @enderror
  @foreach ($languages as $language)
  <div class=''>
    <label class="form-label">
      <small>{{$language->name}}</small>
    </label>
    <textarea class='form-control' 
        wire:model.lazy="description.{{$language->id}}">
    </textarea>
    </div>  
    @endforeach
    
</div>

<div class="mb-3">
    <label for="type" class="form-label mandatory fw-bold">
        @lang('messages.type')
    </label>
    <a data-bs-toggle="collapse" href="#trait_type_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="trait_type_hint" class="odb-hint collapse">
        @lang('messages.trait_type_hint')
    </div>
	<div class="">
        @if(!$canchange)
            @lang('levels.traittype.' . $type) 
        @else 
        <select name="type" id="type" class="form-select" wire:model='type'>
            <option value="" wire:key='tpnull'>
                @lang('messages.select')
            </option>
            @foreach ($trait_types as $ttype)
            <option value="{{ $ttype }}" wire:key='tp{{$ttype}}'>
                @lang('levels.traittype.' . $ttype)
            </option>
            @endforeach           
        </select>
        @endif
    </div>
    @error('type') <span class="text-danger">{{ $message }}</span> @enderror
</div>


<div class="mb-3">
    <label for="objects" class="form-label mandatory fw-bold">
        @lang('messages.object_types')
    </label>
    <a data-bs-toggle="collapse" href="#trait_objects_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="trait_objects_hint" class="odb-hint collapse">
        @lang('messages.trait_objects_hint')
    </div>
	<div class="">
        @foreach($objects_list as $k => $opt)        
        <label for="">
            @if(in_array($opt,$current_objects) and !$canchange)
                <input type="checkbox" 
                    name="objects[]" 
                    wire:model="objects"
                    value="{{ $opt }}"
                    disabled
                >     
            @else
                <input type="checkbox" 
                    name="objects[]" 
                    wire:model="objects"
                    value="{{ $opt }}"
                    
                >    
            @endif 
            {{ str_replace("App\\Models\\","",$opt) }}
        </label>&nbsp;
        @endforeach                   
  </div>
  @error('objects') <span class="text-danger">{{ $message }}</span> @enderror

</div>

@if($this->is_spectral or $this->is_numeric)
<div class="mb-3 ">
    <label for="unit" class="form-label mandatory fw-bold">
        @lang('messages.unit')
    </label>
    <a data-bs-toggle="collapse" href="#hint_trait_unit" class="odb-unstyle">
    <i class="far fa-question-circle"></i></a>
    <div id="hint_trait_unit" class="odb-hint collapse">
        @lang('messages.hint_trait_unit')
    </div>        
    @if(!$canchange)
    <div class="">
        {{ $unit }}
    </div>
    @else
    <div class='row mt-2'>  
        <div class='col-8'>
        <select wire:model="trait_unit_id" class="form-control" >              
            <option value="" wire:key='uunull'>
                @lang('messages.select')
            </option>
            @foreach($units_list as $k => $uu)
            <option value="{{ $uu['id'] }}" wire:key='uu{{$k}}'>
               {{$uu['symbol']." - ".$uu['name']}}
            </option>
            @endforeach                 
        </select>
        </div>            
    </div>    
    @endif    
    @error('trait_unit_id') <span class="text-danger">{{ $message }}</span> @enderror    
</div>
@endif

@if($this->is_numeric)
<div class="mb-3 ">
    <label for="range_min" class="form-label fw-bold">
        @lang('messages.range_min')
    </label>
    <a data-bs-toggle="collapse" href="#hint_trait_min" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="hint_trait_min" class="odb-hint collapse">
        @lang('messages.hint_trait_min')
    </div>
	<div class="">
	<input type="text" wire:model.defer='range_min' class="form-control">
    </div>
    @error('range_min') <span class="text-danger">{{ $message }}</span> @enderror    

</div>

<div class="mb-3 ">
    <label for="range_max" class="form-label fw-bold">
        @lang('messages.range_max')
    </label>
    <a data-bs-toggle="collapse" href="#hint_trait_max" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="hint_trait_max" class="odb-hint collapse">
        @lang('messages.hint_trait_max')
    </div>
    <div class="">
        <input type="text" wire:model.defer='range_max' class="form-control">
    </div>
    @error('range_max') <span class="text-danger">{{ $message }}</span> @enderror    

</div>
@endif

<!-- spectral -->
@if($this->is_spectral)
<div class="mb-3 ">
    <label for="range_min" class="form-label mandatory fw-bold">
        @lang('messages.wavenumber_start') <em>cm<sup>-1</sup></em>
    </label>
    <a data-bs-toggle="collapse" href="#hint_wavenumber_start" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="hint_wavenumber_start" class="odb-hint collapse">
        @lang('messages.hint_wavenumber_start')
    </div>
	<div class="">
        @if(!$canchange)
          {{ $range_min }}
        @else
            <input type="text" wire:model='range_min' class="form-control">
        @endif
    </div>
    @error('range_min') <span class="text-danger">{{ $message }}</span> @enderror    
</div>

<div class="mb-3 ">
    <label for="range_max" class="form-label mandatory fw-bold">
        @lang('messages.wavenumber_end') <em>cm<sup>-1</sup></em>
    </label>
    <a data-bs-toggle="collapse" href="#wavenumber_end" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="wavenumber_end" class="odb-hint collapse">
        @lang('messages.hint_wavenumber_end')
    </div>
    <div class="">
        @if(!$canchange)
          {{ $range_max }}
        @else
            <input type="text" wire:model='range_max' class="form-control">
        @endif
    </div>
    @error('range_max') <span class="text-danger">{{ $message }}</span> @enderror    

</div>

<div class="mb-3 ">
    <label for="value_length" class="form-label mandatory fw-bold">
        @lang('messages.wavenumber_step')
    </label>
    <a data-bs-toggle="collapse" href="#wavenumber_step" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="wavenumber_step" class="odb-hint collapse">
        @lang('messages.hint_wavenumber_step')
    </div>
	<div class="">
      @if(!$canchange)
        {{ $value_length }}
      @else
        <input type="text" wire:model='value_length' class="form-control">
      @endif
      </div>
      @error('value_length') <span class="text-danger">{{ $message }}</span> @enderror    

</div>
@endif



@if($this->is_categorical)
<div class="mb-3 ">
    <label for="categories" class="form-label mandatory fw-bold">
        @lang('messages.categories')
    </label>
    <a data-bs-toggle="collapse" href="#hint_categories" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="hint_categories" class="odb-hint collapse">
        @lang('messages.hint_categories')
    </div>
    <div>
        <button type='button' class='btn btn-sm btn-primary' wire:click='addCategory'>
            @lang('messages.add_category')
        </button>      
    </div>        
    @if($add_category)    
    <div class="bg-info bg-opacity-25 p-2 mt-3">
        @foreach ($languages as $language)
            <div class='row'>
                <div class='col-4'>
                    <label class="form-label mandatory">
                        <small>@lang('messages.name') - {{$language->name}}</small> 
                    </label>
                    <input type='text' class='form-control' 
                    wire:model.lazy="cat_name.{{$language->id}}">
                </div>
                <div class='col-8'>
                    <label class="form-label">
                        <small>@lang('messages.description') - {{$language->name}}</small> 
                    </label>
                    <textarea class='form-control' 
                    wire:model.lazy="cat_description.{{$language->id}}">
                    </textarea>
                </div>
            </div>            
        @endforeach
        <div class='row mt-2'>
            <div class='text-center'>
            <button type='button'  class='btn btn-sm btn-primary'
                wire:click="saveCategory">
                @lang('messages.save_category')
            </button>
            &nbsp;
            <button type='button'  class='btn btn-sm btn-outline   -secondary'
                wire:click="cancelCategory">
                @lang('messages.close')
            </button>
            </div>
        </div>
        @if (session()->has('cat_error'))
        <div class="alert alert-danger mt-2 ">
            {!! session('cat_error') !!}
        </div>
        @endif
    </div>
    @endif
    @if(count($categories))
        <div class='mt-2'>
          <table class='table table-striped' >
                <tr>
                    <th></th>
                    <th>id</th>                    
                    <th>@lang('messages.name')</th>                    
                    <th>@lang('messages.description')</th>                    
                    <th>@lang('messages.order')</th>
                </tr>
                @foreach($categories as $order => $category) 
                  <tr>
                      <td class='text-nowrap'>
                        <button type='button' class='btn btn-sm text-danger'
                        wire:click="removeCategory({{$order}})">
                        <i class="fa-regular fa-trash-can"></i>
                        </button>
                        <button type='button' class='btn btn-sm text-primary'
                        wire:click="editCategory({{$order}})">
                        <i class="fa-regular fa-edit"></i>
                        </button>
                      </td>
                      <td> {{ $category['category_id'] }}</td>
                      <td> {{ $category['name'][$current_locale] }}</td>
                      <td> {{ isset($category['description'][$current_locale]) ? $category['description'][$current_locale] : null }}</td>
                      <td><select 
                        wire:model="categories_order.{{$order}}" 
                        class='form-control'
                        style="max-width: 100px;"
                        wire:change="changeCategoryOrder({{$order}})">
                        @foreach($categories_order as $key => $idx) 
                          <option value="{{$idx}}" 
                          wire:key = "p{{$order}}_{{$idx}}"
                          {{ $idx==$order ? 'selected' : ''}}
                          >
                            {{$idx+1}}
                          </option>
                        @endforeach
                        </select>
                      </td>
                  </tr>
                @endforeach
            </table>
        </div>                      
    @endif
    @error('categories') <span class="text-danger">{{ $message }}</span> @enderror
</div>
@endif

<div class="mb-3">
    <label for="parentTrait" class="form-label fw-bold">
        @lang('messages.parent_trait')
    </label>
    <a data-bs-toggle="collapse" href="#parent_trait_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="parent_trait_hint" class="odb-hint collapse">
	     @lang('messages.parent_trait_hint')
    </div>      
    @if($parent_name)
    <div >
        {!! $parent_name !!}
    </div>    
    @endif
    <div class='mt-2'>
        @livewire('input-search-bar', [
            'search_model' => "ODBTrait",
            'field_name' => "id",
            'return_field' => "name",
            'search_field' => null,
            'return_id' => null,
            'query' => null,
            'emitup_function' => "mergeChildTrait",
            'nested_model' => "",            
            ]
        )
    @error('parent_id') <span class="text-danger">{{ $message }}</span> @enderror
  </div>
  
</div>


<div class="mb-3">
  <label class="form-label fw-bold">
    @lang('messages.trait_bibreference')
  </label>
  <a data-bs-toggle="collapse" href="#trait_bibreference_hint" class="odb-unstyle">
    <i class="far fa-question-circle"></i>
  </a>
  <div id="trait_bibreference_hint" class="odb-hint collapse">
	@lang('messages.trait_bibreference_hint')
  </div>
  <div class="">
        @livewire('input-search-bar', [
            'search_model' => "BibReference",
            'field_name' => "id",
            'return_field' => "name",
            'search_field' => "bibkey",
            'return_id' => null,
            'query' => null,
            'emitup_function' => "mergeChildBib",
            'nested_model' => "",            
            ]
        )
    </div>
    @if(count($bibreferences))
        <div class='mt-2'>
          <table class='table table-striped' >
                <tr>
                    <th></th>
                    <th>id</th>                    
                    <th>@lang('messages.name')</th>                                        
                </tr>
                @foreach($bibreferences as $ob => $bb) 
                  <tr>
                      <td class='text-nowrap'>
                        <button type='button' class='btn btn-sm text-danger'
                        wire:click="removeBibReference({{$ob}})">
                        <i class="fa-regular fa-trash-can"></i>
                        </button>                        
                      </td>
                      <td> {{ $bb['id'] }}</td>
                      <td> {!!  $bb['label'] !!}</td>                      
                  </tr>
                @endforeach
            </table>
        </div>                      
    @endif
    @error('bibreferences') <span class="text-danger">{{ $message }}</span> @enderror
</div>


<div class="mb-3">
  <label class="form-label fw-bold">
    @lang('messages.tag')
  </label>
  <a data-bs-toggle="collapse" href="#trait_tag_hint" class="odb-unstyle">
    <i class="far fa-question-circle"></i>
  </a>
  <div id="trait_tag_hint" class="odb-hint collapse">
	@lang('messages.trait_tag_hint')
  </div>
  <div class="">
    @livewire('input-search-bar', [
        'search_model' => "Tag",
        'field_name' => "id",
        'return_field' => "name",
        'search_field' => "name",
        'return_id' => null,
        'query' => null,
        'emitup_function' => "mergeChildTag",
        'nested_model' => null, 
        ]
    )        
    </div>
    @if(count($tags))
        <div class='mt-2'>
          <table class='table table-striped' >
                <tr>
                    <th></th>
                    <th>id</th>                    
                    <th>@lang('messages.name')</th>                                        
                </tr>
                @foreach($tags as $ot => $tg) 
                  <tr>
                      <td class='text-nowrap'>
                        <button type='button' class='btn btn-sm text-danger'
                        wire:click="removeTag({{$ot}})">
                        <i class="fa-regular fa-trash-can"></i>
                        </button>                        
                      </td>
                      <td> {{ $tg['id'] }}</td>
                      <td> {!!  $tg['label'] !!}</td>                      
                  </tr>
                @endforeach
            </table>
        </div>                      
    @endif
    @error('tags') <span class="text-danger">{{ $message }}</span> @enderror
</div>


<!-- notes -->
<div class="mb-3">
    <label for="notes" class="form-label fw-bold">
        @lang('messages.notes')
    </label>
    <textarea class="form-control" wire:model='notes'>
    </textarea>
</div>


<div class="mb-3" >
    <button type='button' class="btn btn-success"
    wire:click="submit" wire:loading.remove>
    @lang("messages.save")
    </button>
    <button type="button" class="btn text-danger" 
    wire:loading wire:target="submit">
        <i class="fa-solid fa-spinner fa-spin"></i>&nbsp;@lang('messages.be_patient')
    </button>
</div>

</form>
</div>

</div>

@once
    @push ('styles')
        @livewireStyles
    @endpush
    @push ('scripts')
        @livewireScripts
        @vite('resources/assets/js/custom.js')
    @endpush
@endonce