<div class="px-lg-3 mt-3" style='min-width: 90vw;'>   
    @if (session()->has('success'))
        <div class="alert alert-success">
            {!! session('success') !!}
        </div>
    @endif
    @if (session()->has('errors'))
        <div class="alert alert-danger">
            {!! session('errors') !!}
        </div>
    @endif
    <div class='row mb-3 p-2'>
            <div class='col col-sm-auto col-lg-auto'>            
            <label class="form-label fw-bold">
                @lang('messages.taxon')            
            </label>         
            @if($this->taxon_name)
                <br>{{$this->taxon_name}}&nbsp;<button type='button' wire:click='clearTaxon' 
                class='btn btn-sm'>
                <i class="fa-solid fa-broom text-secondary"></i>
                </button>
            @endif          
            @livewire('input-search-bar', [
                'search_model' => "Taxon",
                'field_name' => "id",
                'return_field' => "fullname",
                'search_field' => "fullname",
                'return_id' => null,
                'query' => null,
                'emitup_function' => "mergeChildTaxon",
                'nested_model' => "", 
                ]
            )                  
            </div>   
            <div class='col col-sm-auto col-lg-auto'>            
                <label class="form-label fw-bold">
                @lang('messages.individual')                        
                    <button type='button' wire:click="SearchTip('individual')" 
                    class='btn btn-sm'>
                        <i class="fa-regular fa-circle-question text-secondary"></i>
                    </button>
                </label>  
                @if($this->individual_search_tip)
                    <br>
                    <small>@lang('messages.number_only')</small>
                @endif
                @if($this->individual_name)
                    <br>{!! $this->individual_name !!}
                    &nbsp;<button type='button' wire:click='clearIndividual' 
                    class='btn btn-sm'>
                    <i class="fa-solid fa-broom text-secondary"></i>
                    </button>
                @endif                
            @livewire('input-search-bar', [
                'search_model' => "Individual",
                'field_name' => "id",
                'return_field' => "fullname",
                'search_field' => "fullname",
                'return_id' => null,
                'query' => null,
                'emitup_function' => "mergeChildIndividual",
                'nested_model' => "", 
                'batch_identify' => true,    
                ]
            )
            </div>  
            <div class='col col-sm-auto col-lg-auto'>            
            <label class="form-label fw-bold">
                @lang('messages.tag')            
            </label>         
            @if($this->tag_name)
                <br>{{$this->tag_name}}&nbsp;<button type='button' wire:click='clearTag' 
                class='btn btn-sm'>
                <i class="fa-solid fa-broom text-secondary"></i>
                </button>
            @endif          
            @livewire('input-search-bar', [
                'search_model' => "Tag",
                'field_name' => "id",
                'return_field' => "name",
                'search_field' => "name",
                'return_id' => null,
                'query' => null,
                'emitup_function' => "mergeChildTag",
                'nested_model' => null, 
                ]
            )                  
            </div>                                        
            <div class='col col-sm-auto col-lg-auto'>            
            <label class="form-label fw-bold">
                @lang('messages.voucher')            
                <button type='button' wire:click="SearchTip('voucher')" 
                    class='btn btn-sm'>
                        <i class="fa-regular fa-circle-question text-secondary"></i>
                    </button>
            </label>  
            @if($this->voucher_search_tip)
            <br>
            <small>@lang('messages.number_only')</small>
            @endif       
            @if($this->voucher_name)
                <br>{!! $this->voucher_name !!}
                &nbsp;<button type='button' wire:click='clearVoucher' 
                class='btn btn-sm'>
                <i class="fa-solid fa-broom text-secondary"></i>
                </button>
            @endif          
            @livewire('input-search-bar', [
                'search_model' => "Voucher",
                'field_name' => "id",
                'return_field' => "fullname",
                'search_field' => "fullname",
                'return_id' => null,
                'query' => null,
                'emitup_function' => "mergeChildVoucher",
                'nested_model' => null, 
                ]
            )                  
            </div>      
            <div class='col col-sm-auto col-lg-auto'>            
            <label class="form-label fw-bold">
                @lang('messages.location')            
            </label>         
            @if($this->location_name)
                <br>{!! $this->location_name !!}
                &nbsp;<button type='button' wire:click='clearLocation' 
                class='btn btn-sm'>
                <i class="fa-solid fa-broom text-secondary"></i>
                </button>
            @endif          
            @livewire('input-search-bar', [
                'search_model' => "Location",
                'field_name' => "id",
                'return_field' => "name",
                'search_field' => "name",
                'return_id' => null,
                'query' => null,
                'emitup_function' => "mergeChildLocation",
                'nested_model' => null, 
                ]
            )                  
            </div>   
            <div class='col col-sm-auto col-lg-auto'>            
            <label class="form-label fw-bold">
                @lang('messages.project')            
            </label>         
            @if($this->project_name)
                <br>{{$this->project_name}}&nbsp;<button type='button' wire:click='clearProject' 
                class='btn btn-sm'>
                <i class="fa-solid fa-broom text-secondary"></i>
                </button>
            @endif          
            @livewire('input-search-bar', [
                'search_model' => "Project",
                'field_name' => "id",
                'return_field' => "acronym",
                'search_field' => "acronym",
                'return_id' => null,
                'query' => null,
                'emitup_function' => "mergeChildProject",
                'nested_model' => "", 
                ]
            )                  
            </div>                                        
    </div>     
    @if($media->count())
    <div class='row mb-3'>
        <div class='col col-sm-auto col-lg-auto'>                        
    {{ $media->links() }}
        </div>
    @if(isset($per_page))
        <div class='col col-sm-auto col-lg-auto'>          
            <div class='form-inline'>
                <select wire:model='per_page' style='width: auto;' class="form-control">
                    <option value="6">6 @lang('messages.per_page')</option>
                    <option value="12">12 @lang('messages.per_page')</option>
                    <option value="20">20 @lang('messages.per_page')</option>
                    <option value="40">40 @lang('messages.per_page')</option>
                    <option value="50">50 @lang('messages.per_page')</option>        
                </select>
            </div>
        </div> 
    @endif
    </div>     
    @include('livewire.common.media-block')
    
    @else 
        <div class="alert alert-danger">
            {!! __('messages.no_records') !!}
        </div>
    @endif
</div>
@once
    @push ('styles')
        @livewireStyles
    @endpush
    @push ('scripts')
        @livewireScripts                
    @endpush
@endonce
