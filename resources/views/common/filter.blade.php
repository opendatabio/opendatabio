<div class="card mt-1" style="display: none;" id='filter_pannel' >
<div class="card-header">
  @lang('messages.filter_data')
  &nbsp;&nbsp;
  <a href="#" class='odb-filter-button odb-unstyle' >
    <i class="far fa-eye-slash"></i>
  </a>
</div>
<div class="card-body" >
  <!--- EXPORT FORM hidden -->
  <div class="col" >
    <form action="{{ url($url) }}" method="POST" id='filter_form'>
    <!-- csrf protection -->
      {{ csrf_field() }}
      <div class="row mb-3">
        <div class="col-sm-3 form-group">
          <label class="form-label mandatory">
              @lang('messages.taxon')
              <a data-bs-toggle="collapse" href="#filter_taxon_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          </label>
          <div>
              {!! Multiselect::autocomplete(
                  'filter_taxon_id',
                  isset($filter_taxon_data) ? $filter_taxon_data : [],
                  isset($filter_taxon_id) ? $filter_taxon_id : [],
                  ['class' => 'multiselect form-control']
              ) !!}
          </div>
          <div id="filter_taxon_hint" class="odb-hint collapse">
      	       @lang('messages.filter_taxon_hint')
          </div>

        </div>
        <div class="col-sm-3 form-group">
          <label class="form-label mandatory">
              @lang('messages.location')
              <a data-bs-toggle="collapse" href="#location_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          </label>
          <div >
              {!! Multiselect::autocomplete(
                  'filter_location_id',
                  isset($filter_location_data) ? $filter_location_data : [],
                  isset($filter_location_id) ? $filter_location_id : [],
                  ['class' => 'multiselect form-control']
              ) !!}
          </div>
          <div id="location_hint" class="odb-hint collapse">
               @lang('messages.filter_location_hint')
          </div>

        </div>
        @if($filter_type!="taxons")
        <div class="col-sm-3 form-group">
          <label class="form-label mandatory">
              @lang('messages.collectors')
              <a data-bs-toggle="collapse" href="#filter_persons_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          </label>
          <div >
              {!! Multiselect::autocomplete(
                  'filter_persons_ids',
                  isset($filter_persons_data) ? $filter_persons_data : [],
                  isset($filter_persons_ids) ? $filter_persons_ids : [],
                  ['class' => 'multiselect form-control']
              ) !!}
          </div>
          <div id="filter_persons_hint" class="odb-hint collapse">
               @lang('messages.filter_persons_hint')
          </div>

        </div>
        @if(in_array($filter_type,["vouchers","individuals"]))
        <div class="col-sm-3 form-group">
          <label class="form-label mandatory">
              @lang('messages.number')
              <a data-bs-toggle="collapse" href="#filter_number_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          </label>
          <div >
          <input class="form-control" type="text" name="filter_number" value="{{$filter_number}}">
          </div>
          <div id="filter_number_hint" class="odb-hint collapse">
               @lang('messages.filter_number_hint')
          </div>
        </div>
        @endif
      </div>
      <div class="row mb-3">
        <div class="col-sm-3 form-group">
          <label class="form-label mandatory">
              @lang('messages.date')
              <a data-bs-toggle="collapse" href="#filter_date_hints" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          </label>
          <div id="filter_date_hints" class="odb-hint collapse">
               @lang('messages.filter_date_hints')
          </div>
          <div >
            @lang('messages.minimum')&nbsp;<input class="form-control" type="date" name="filter_min_date" value="{{$filter_min_date}}">
            @lang('messages.maximum')&nbsp;<input class="form-control" type="date" name="filter_max_date" value="{{$filter_max_date}}">
          </div>
        </div>      
        @endif
      <div class="col-sm-3 form-group">
        <label class="form-label mandatory">
            @lang('messages.datasets')
            <a data-bs-toggle="collapse" href="#filter_datasets_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        </label>
        <div >
            {!! Multiselect::autocomplete(
                'filter_datasets_ids',
                isset($filter_datasets_data) ? $filter_datasets_data : [],
                isset($filter_datasets_ids) ? $filter_datasets_ids : [],
                ['class' => 'multiselect form-control']
            ) !!}
        </div>
        <div id="filter_datasets_hint" class="odb-hint collapse">
             @lang('messages.filter_datasets_hint')
        </div>

      </div>
      <div class="col-sm-3 form-group">
        <label class="form-label mandatory">
            @lang('messages.projects')
            <a data-bs-toggle="collapse" href="#filter_projects_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        </label>
        <div >
            {!! Multiselect::autocomplete(
                'filter_projects_ids',
                isset($filter_projects_data) ? $filter_projects_data : [],
                isset($filter_projects_ids) ? $filter_projects_ids : [],
                ['class' => 'multiselect form-control']
            ) !!}
        </div>
        <div id="filter_projects_hint" class="odb-hint collapse">
             @lang('messages.filter_projects_hint')
        </div>

      </div>
    </div>
    @if(in_array($filter_type,["vouchers","taxons"]))
    <div class="row mb-3">
      <div class="col-sm-3 form-group">
          <label class="form-label mandatory">
              @lang('messages.biocollection')              
          </label>
          <div>
              {!! Multiselect::autocomplete(
                  'filter_biocollections_ids',
                  isset($filter_biocollections_data) ? $filter_biocollections_data : [],
                  isset($filter_biocollections_ids) ? $filter_biocollections_ids : [],
                  ['class' => 'multiselect form-control']
              ) !!}
          </div>
        </div>
    </div>
    @endif
      <div class="row mb-3">
        <div class="col mx-auto p-1">
          <button type='submit' class="btn btn-success" >@lang('messages.filter')</button>
        </div>
      </div>
  </form>  
</div>
</div>
</div>
