<div class="row">
<div class='col text-center'>
<div class="alert alert-danger" id="individuals_batch_identify_alert" style="display: none;" >
{{__('messages.individuals_batch_identify_alert')}}</div>
<div class="alert alert-danger" id="request_select_records" style="display: none;" >
{{__('messages.request_select_records')}} & {{__('messages.request_message')}}</div>
</div>
<input type="hidden" name="individuals_batch_identify_confirm" id="individuals_batch_identify_confirm"   
value="{{__('messages.individuals_batch_identify_confirm')}}">
<input type="hidden" name="no_permission_list" id="no_permission_list"   
value="{{__('messages.no_permission_list')}}">
<input type="hidden" name="individual_object_list" id="individual_object_list"   
value="{{__('messages.individual_object_list')}}">
</div>
<div class="row">
<div class='col-sm-4'>
<div class="mb-3">
  <label class="form-label mandatory">
    @lang('messages.taxon')
  </label>
  <a data-bs-toggle="collapse" href="#hint6" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <input type="text" name="taxon_autocomplete" id="taxon_autocomplete" class="form-control autocomplete taxon_autocomplete" value="">
    <input type="hidden" name="taxon_id" id="taxon_id"   value="">
    <div id="hint6" class="odb-hint collapse">
    @lang('messages.individual_taxon_hint')
    </div>
</div>

<div class="mb-3">
  <label for="modifier" class="form-label">
    @lang('messages.modifier')
  </label>
  <a data-bs-toggle="collapse" href="#hint9" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <?php $selected = null; ?>

  <select name="modifier" class="form-select col-sm-2" aria-label="modifier">
    @foreach (App\Models\Identification::MODIFIERS as $modifier)
         <option {{ $modifier == $selected ? 'selected' : '' }} value="{{$modifier}}" >
          @lang('levels.modifier.' . $modifier)
         </option>
     @endforeach
  </select>
    <div id="hint9" class="odb-hint collapse">
       @lang('messages.individual_modifier_hint')
    </div>
</div>

<div class="mb-3">
    <label for="identifier_id" class="form-label mandatory">
@lang('messages.identifier')
</label>
<a data-bs-toggle="collapse" href="#hint7" class="odb-unstyle"><i class="far fa-question-circle"></i></a>

    <input type="text" name="identifier_autocomplete" id="identifier_autocomplete" class="form-control autocomplete identifier_autocomplete"
      value="{{ isset(Auth::user()->person) ? Auth::user()->person->full_name : null }}">
    <input type="hidden" name="identifier_id" id="identifier_id"
      value="{{ isset(Auth::user()->person) ? Auth::user()->person->id : null }}">
    <div id="hint7" class="odb-hint collapse">
	@lang('messages.individual_identifier_id_hint')
    </div>
</div>

<div class="mb-3">
    <label for="identification_date" class="form-label mandatory">
@lang('messages.identification_date')
</label>
<a data-bs-toggle="collapse" href="#hint8" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
<div>
{!! View::make('common.incompletedate')->with([
    'object' => null,
    'field_name' => 'identification_date'
]) !!}
</div>
    <div id="hint8" class="odb-hint collapse">
	@lang('messages.individual_identification_date_hint')
    </div>
</div>
</div>

<div class='col-sm-6'>
@if(isset($biocollections))
<div class="mb-3">
    <label for="biocollection_id" class="form-label">
@lang('messages.id_biocollection')
</label>
<a data-bs-toggle="collapse" href="#hint10" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
<div class="col-sm-6">
	<select name="identification_based_on_biocollection" id="biocollection_id" class="form-select col-sm-3"  >
		<option value='' >&nbsp;</option>
	@foreach ($biocollections as $biocollection)
		<option value="{{$biocollection->id}}" {{ '' }}>{{ $biocollection->acronym }} </option>
	@endforeach
	</select>
</div>
      <div id="hint10" class="odb-hint collapse">
	@lang('messages.individual_biocollection_id_hint')
    </div>
</div>

<div class="mb-3 biocollection_reference">
    <label for="biocollection_reference" class="form-label mandatory">
@lang('messages.biocollection_reference')
</label>
<div class="col-sm-6">
	<input type="text" name="identification_based_on_biocollection_id" id="biocollection_reference" class="form-control" value="">
</div>
</div>
@endif

<div class="mb-3">
    <label for="identification_notes" class="form-label">
    @lang('messages.identification_notes')
</label>
<div class="col-sm-6">
	<textarea name="identification_notes" id="identification_notes" class="form-control"></textarea>
</div>
</div>

@if($batchupdate)
<div class="form-check mb-3">
  <input type="checkbox" name="update_nonself_identification" value=1 >
  <label for="update_nonself_identification" class="form-label">
    @lang('messages.individual_update_nonself_identification')
  </label>
  <a data-bs-toggle="collapse" href="#update_nonself_identification" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div id="update_nonself_identification" class="odb-hint collapse">
  @lang('messages.individual_update_nonself_identification_hint')
  </div>
</div>
@endif
</div>

</div>
