<div class="card-body" style="display: none;" id='delete_pannel'>
  <input id="select_rows_msg" name="select_rows_msg" type="hidden" value="@lang('messages.select_rows_delmsg')">
  <input id="selected_rows_msg"  name="selected_rows_msg" type="hidden" value="@lang('messages.selected_rows_delmsg')">
  <p class="text-danger" id="delete_hint" ></p>
  <div class="col-sm-6 g-3 ps-2" id='delete_form' style="display: none;">
  <p  class="alert alert-danger" >@lang('messages.action_not_undone')</p>
  <form action="{{ $url }}" method="POST" class="form-horizontal" >
      <!-- csrf protection -->
      {{ csrf_field() }}
      <input type='hidden' name='ids_to_delete' id='ids_to_delete' value="" >
      <button type='submit' class="btn btn-danger" id='export_sumbit'>@lang('messages.confirm_deletion')</button>
  </form>
  </div>
</div>
