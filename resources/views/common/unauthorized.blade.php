@extends('layouts.app')

@section('content')
<div class="container">
  <br>
  <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
      <div class="card-header">
        @lang('messages.whoops')
    </div>
      <div class="card-body alert alert-danger">
	@lang('messages.unauthorized')
    </div>
  </div>
</div>

@endsection
