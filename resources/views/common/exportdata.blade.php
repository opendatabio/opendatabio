<div class="card-body" style="display: none;" id='export_pannel'>
  <!--- EXPORT FORM hidden -->
  <div class="col-md-12" >
    <form action="{{ url('exportdata')}}" method="POST" class="form-horizontal" id='export_form'>
    <!-- csrf protection -->
      {{ csrf_field() }}
    <!--- field to fill with ids to export --->
    @if (isset($object) and class_basename($object) == "ODBTrait")
      <input type='hidden' name='trait' value='{{ $object->id}}' >
    @elseif (isset($object_second) and class_basename($object_second) == "ODBTrait")
      <input type='hidden' name='trait' value='{{ $object_second->id}}' >
    @endif

    @if (isset($object) and class_basename($object) == "Dataset")
      <input type='hidden' name='dataset' value='{{ $object->id}}' >
    @elseif (isset($object_second) and class_basename($object_second) == "Dataset")
      <input type='hidden' name='dataset' value='{{ $object_second->id}}' >
    @endif

    @if (isset($object) and class_basename($object) == "Biocollection")
      <input type='hidden' name='biocollection' value='{{ $object->id}}' >
    @elseif (isset($object_second) and class_basename($object_second) == "Biocollection")
      <input type='hidden' name='biocollection' value='{{ $object_second->id}}' >
    @endif

    @if (isset($measured_type))
      <input type='hidden' name='measured_type' value='{{ $measured_type}}' >
    @endif
    @if (isset($request_id))
      <input type='hidden' name='request_id' value='{{ $request_id}}' >
    @endif
    @if (isset($object) and class_basename($object) == "Project")
      <input type='hidden' name='project' value='{{ $object->id}}' >
    @elseif (isset($object_second) and class_basename($object_second) == "Project")
      <input type='hidden' name='project' value='{{ $object_second->id}}' >
    @endif


    @if (isset($object) and class_basename($object) == "Taxon")
      Taxon:<input type='hidden' name='taxon_root' value='{{ $object->id}}' >
    @endif
    @if (isset($object) and class_basename($object) == "Location")
      <input type='hidden' name='location_root' value='{{ $object->id}}' >
    @endif
    @if (isset($object) and class_basename($object) == "Individual")
      <input type='hidden' name='individual' value='{{ $object->id}}' >
    @endif
    @if (isset($object) and class_basename($object) == "Voucher")
      <input type='hidden' name='voucher' value='{{ $object->id}}' >
    @endif
    @if (isset($object) and class_basename($object) == "Person")
      <input type='hidden' name='person' value='{{ $object->id}}' >
    @endif
    @if(isset($filters))
      @foreach($filters as $key => $val)
        @if($val)
          <input type='hidden' name='filters[{{$key}}]' value='{{$val}}' >
        @endif
      @endforeach
    @endif
    <input type='hidden' name='export_ids' id='export_ids' value='' >
    <input type='hidden' name='object_type' value='{{$export_what}}' >
      <input type="radio" name="filetype" checked value="csv" >&nbsp;<label>CSV</label>
      &nbsp;&nbsp;
      <input type="radio" name="filetype" value="ods">&nbsp;<label>ODS</label>
      &nbsp;&nbsp;
      <input type="radio" name="filetype" value="xlsx">&nbsp;<label>XLSX</label>
      &nbsp;&nbsp;
      &nbsp;&nbsp;
      <input type="radio" name="fields" value="all">&nbsp;<label>all fields</label>
      &nbsp;&nbsp;
      <input type="radio" name="fields" checked value="simple">&nbsp;<label>simple fields</label>
      &nbsp;&nbsp;
      <a data-bs-toggle="collapse" href="#hint_exports" class="odb-unstyle" title="@lang('messages.help')"><i class="far fa-question-circle"></i></a>
      &nbsp;&nbsp;
      <button type='button' class="btn btn-success" id='export_submit'>@lang('messages.submit')</button>
  </form>
</div>
<div class="col-md-12 odb-hint collapse" id='hint_exports'>
  <br>
  @lang('messages.export_hint')
</div>
</div>
