@extends('layouts.app')

@section('content')
<div class="container">
  <br>
  <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
      <div class="card-header">
          @lang('messages.imports')
          <strong>
          @lang('messages.'.$model)
          </strong>
          &nbsp;&nbsp;
          <a data-bs-toggle="collapse" href="#object_help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          <div id="object_help" class="odb-hint collapse">
           @php
             $txt = Lang::get('messages.import_'.$model.'_hint');
             if ( $txt != 'messages.import_'.$model.'_hint') {
               echo $txt;
             }
           @endphp
           <br><br>
           @lang('messages.imports_help')
     </div>
   </div>
     <div class="card-body">

        @php
        switch ($model) {
          case 'locations':
              $action = 'import-locations';
              break;
          case 'measurements':
              $action = 'import-measurements';
              break;
          case 'individuals':
              $action = 'import-individuals';
              break;
          case 'individuallocations':
              $action = 'import-individuallocations';
              break;
          case 'taxons':
              $action = 'import-taxons';
              break;
          case 'traits':
              $action = 'import-traits';
              break;
          case 'vouchers':
              $action = 'import-vouchers';
              break;
          case 'persons':
              $action = 'import-persons';
              break;
          case 'biocollections':
              $action = 'import-biocollections';
              break;
          default:
            $action = null;
            break;
        }
        @endphp

      <form action="{{ url($action) }}" method="POST" class="row g-2" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="MAX_FILE_SIZE" value="{{config('import_file_size')}}" />
        <div class="mb-3">
          <label for="data_file" class="form-label mandatory">
            @lang('messages.import_file')
          </label>
          <div class="">
            <input type="file" name="data_file" id="data_file">
            @php
              $geojson = $model=='locations' ? '  GeoJson' : '';
            @endphp
            <br>
            <div class='odb-hint' >@lang('messages.import_file_hint',['additionaltypes' => $geojson])</div>
          </div>
        </div>

        @if ($model == 'traits')
          <div class="mb-3">
            <label for="trait_categories_file" class="form-label mandatory">
              @lang('messages.import_trait_categories_file')
            </label>
            <a data-bs-toggle="collapse" href="#hint_trait_categories" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
            <div class="">
              <input type="file" name="trait_categories_file" id="import_trait_categories_file">
              <div class='odb-hint' >@lang('messages.import_file_hint',['additionaltypes' => ''])</div>
            </div>
              <div id="hint_trait_categories" class="odb-hint collapse">
                @lang('messages.import_trait_categories_hint')
              </div>
          </div>
        @endif

        <div class="mb-3">
            <button type="submit" class="btn btn-success" name="submit" value="submit">
              <i class="fa fa-btn fa-upload"></i>
              @lang('messages.submit')
            </button>
        </div>
      </form>
  </div>
</div>
</div>
@endsection
