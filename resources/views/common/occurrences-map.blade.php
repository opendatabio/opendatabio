@extends('layouts.app')
@section('content')
<div class="container">
    <br>
    <div class="card">
      <div class="card-header">
      @if(isset($mapfor))
        @lang('messages.mapfor') <strong>{!! $mapfor !!}</strong>
      @endif
      @if(!isset($addform))
        </div>
      @else
        &nbsp;&nbsp;&nbsp;
        <button id='show-form' type="button" class='btn btn-outline-secondary' >@lang('messages.customize_map')</button>
      <div class="card-body border border-1 border-odbgray rounded mt-3 mx-3" id='update-form' style="display: none;">
      <form class="row" action="{{url('locations/custom-map')}}" method="POST" id='map-refresh-form'>
          <!-- csrf protection -->
          {{ csrf_field() }}

        <div class="col-sm-4">
          <small>@lang('messages.maptaxons')</small><br>
          {!! Multiselect::autocomplete('taxons',
            isset($taxon) ? $taxon->pluck('name','id') : [],
            isset($taxons_ids) ? $taxons_ids : '',
            ['class' => 'multiselect form-control'])
          !!}
        </div>
        <div class="col-sm-4">
          <small>@lang('messages.maplocations')</small><br>
          {!! Multiselect::autocomplete('locations',
            isset($location) ? $location->pluck('name','id') : [],
            isset($locations_ids) ? $locations_ids : '',
            ['class' => 'multiselect form-control'])
          !!}
          <small>
            <input type="checkbox" name="with_ancestors" value="1" {{ isset($with_ancestors) ? 'checked' : null  }}>
            &nbsp;@lang('messages.with_ancestors')
            &nbsp;
            <input type="checkbox" name="with_parent" value="1" {{ isset($with_parent) ? 'checked' : null  }}>
            &nbsp;@lang('messages.with_parent')
            &nbsp;
            <input type="checkbox" name="with_children" value="1" {{ isset($with_children) ? 'checked' : null  }}>
            &nbsp;@lang('messages.with_children')
            &nbsp;
            <input type="checkbox" name="with_descendants" value="1" {{ isset($with_descendants) ? 'checked' : null  }}>
            &nbsp;@lang('messages.with_descendants')
          </small>
        </div>
      <div class="col-sm-2">
        <br>
        <a class="btn btn-primary" id='refresh-map'>
          <i class="fas fa-map-marked-alt"></i>
          @lang('messages.map_update')
        </a>
        <div class="spinner" id="spinner"> </div>
        &nbsp;
        <a data-bs-toggle="collapse" href="#hint6" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
      </div>
      </form>

        <div id="hint6" class="odb-hint collapse">
        	@lang('messages.custom_map_hint')
        </div>
      </div>

      @endif

      <div class="card-body">
        <input type="hidden" name="location_extent" value="{{ json_encode($location_extent) }}" id='location_extent'>
        <input type="hidden" name="location_centroid" value="{{ implode(',',$location_centroid) }}" id='location_centroid'>

        @foreach($location_json as $key => $value)
        <input type="hidden" name="location_json[]" value="{{ $value }}" title="{{ $key }}" >
        @endforeach
        <div id="osm_map" style="
            height: 600px;
            width: 100%;">
       </div>
       <div id="popup" class="ol-popup">
        <a href="#" id="popup-closer" class="ol-popup-closer"></a>
        <div id="popup-content" ></div>
        </div>
      </div>
    </div>
</div>
</div>

@endsection

@once
    @push('styles')
        @vite(['resources/assets/sass/odb-map.scss'])
    @endpush
    @push('scripts')
        @vite('resources/assets/js/custom.js')
        {!! Multiselect::scripts('taxons', url('taxons/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')], null) !!}
        {!! Multiselect::scripts('locations', url('locations/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')], null) !!}
        @vite(['resources/assets/js/odb-map.js'])
        <script type="module">

          $('#refresh-map').on('click',function(e){
            $("#spinner" ).css('display', 'inline-block');
            $('#map-refresh-form').submit();
          });

          $('input:radio[name=with_descendants]').on('click',function(e){
              if ($(this).prop( "checked")) {
                $(this).prop( "checked",false);
              } else {
                $(this).prop( "checked",true);
              }
          });

          $('#show-form').on('click',function(e){
            if ($("#update-form").is(":hidden")) {
              $('#update-form').css('display', 'block'); //to hide
              $(this).html(
              "@lang('messages.customize_map') <i class='fas fa-caret-down'></i>");
            } else {
              $('#update-form').css('display', 'none'); //to hide
              $(this).html(
              "@lang('messages.customize_map')");
            }
          });

    </script>

    @endpush
@endonce

