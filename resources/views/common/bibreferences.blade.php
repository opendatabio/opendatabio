@if(isset($bibreferences))
<table class="table table-striped">
  <thead>
  <tr>
    <th></th>
    <th>@lang('messages.bibtex_key')</th>
    <th>@lang('messages.author')</th>
    <th>@lang('messages.year')</th>
    <th>@lang('messages.title')</th>
 <tr>
 </thead>
 <tbody>
   @php
    $hasmandatory=0;
   @endphp
@foreach($bibreferences as $reference)
  <tr>
    <td class="table-text">
      @if(isset($reference->mandatory))
        @if(0 !== $reference->mandatory)
        <span class="glyphicon glyphicon-asterisk text-danger" data-toggle="tooltip" style="cursor:pointer" title="@lang('messages.dataset_bibreferences_mandatory')"></span>
        @php
         $hasmandatory=$hasmandatory+1;
        @endphp
        @endif
      @endif
    </td>
    <td class="table-text">
      <a href='{{ url('references/'.$reference->bib_reference_id)}}'>
        {{ $reference->bibkey }}
      </a>
    </td>
    <td class="table-text">{{ $reference->first_author }}</td>
    <td class="table-text">{{ $reference->year }}</td>
    <td class="table-text">{{ $reference->title }}</td>
  </tr>
@endforeach
</tbody>
</table>
  @if($hasmandatory>0)
    <span class="glyphicon glyphicon-asterisk text-danger"></span>
    @lang('messages.dataset_bibreferences_mandatory')
  @endif
@endif
