@extends('layouts.app')

@section('content')
<div id='contents' class="container">
    <br>
    <div class="card col-lg-10 offset-lg-1 col-md-6 offset-md-3">
        <div class="card-header">
          @lang('messages.registered_history')
          @if (isset($object)) <!-- we're inside a Location, Project or Taxon view -->

            @lang('messages.for')

            <strong>
              {{ class_basename($object) }}
            </strong>

            @if (null !== $object->rawLink())
              {!! $object->rawLink() !!}
            @else
              {{ $object->full_name }}
            @endif

            @if (class_basename($object) == 'Measurement')
                @lang('messages.for') {{ $object->measured->fullname }}
            @endif
          @endif
          &nbsp;&nbsp;
          <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          <div id="help" class="card-body odb-hint collapse">
            @lang('messages.history_hint')
          </div>
        </div>
        <!--- delete form -->
        {!! View::make('common.batchdelete')->with([
              'url' => url('activity/batch_delete'),
        ]) !!}
        <div class="card-body">
        {!! $dataTable->table([],true) !!}
        </div>
    </div>
</div>
@endsection
@once
  @push ('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
    @vite('resources/assets/js/custom.js')
  @endpush
@endonce
