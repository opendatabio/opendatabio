@extends('layouts.app')

@section('content')
<div id='contents' class="container">
  <br>
  <div class="col-md-12 offset-md-0">
      <!-- Registered Persons -->
      <div class="card">
          <div class="card-header">
            @lang('messages.new_request')
            <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
            <div id="help" class="odb-hint collapse">
              @lang('messages.odbrequest_hint')
            </div>
        </div>
        <div class="card-body">

        @if (isset($odbrequest))
        		    <form action="{{ url('odbrequests/' . $odbrequest->id)}}" method="POST" class="row g-2">
                  {{ method_field('PUT') }}
        @else
        		    <form action="{{ url('odbrequests')}}" method="POST" class="row g-2">
        @endif
		     {{ csrf_field() }}

         <div class="mb-3">
           <label for="email" class="form-label mandatory">
             @lang('messages.email')
           </label>
           <div class="">
             <input type="email" id="email" autocomplete="email" name="email" value="{{ isset($odbrequest) ? $odbrequest->email : old('email') }}" class='form-control'>
           </div>
         </div>
        <div class="mb-3">
             <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" >
             <input value="{{ !isset($odbrequest) ? Auth::user()->email : (null !== $odbrequest->user_id ? $odbrequest->user()->email :  null) }}" class='form-control' readonly>
        </div>

         <div class="mb-3">
           <label for="institution"  class="form-label ">
             @lang('messages.institution')
           </label>
           <a data-bs-toggle="collapse" href="#whereto" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
             <input type="institution" name="institution" value="{{ isset($odbrequest) ? $odbrequest->institution : old('institution') }}"  class="form-control">
             <div id="whereto" class="odb-hint collapse">
                @lang('messages.request_whereto')
             </div>
         </div>
         <!-- THE BIOCOLLECTION THE VOUCHER BELONGS TO -->
         <div class="mb-3">
         <label for="biocollection_id" class="form-label ">
           @lang('messages.biocollection')
         </label>
         <div class="">
           <select name="biocollection_id" class="form-select" >
             <option value="" >Not registered</option>
             @foreach ($biocollections as $biocollection)
              <option value="{{ $biocollection->id }}" "{{ ((isset($odbrequest) and $odbrequest->biocollection_id==$biocollection->id) or old('biocollection_id')==$biocollection->id) ? 'selected' : '' }}">
               {{ $biocollection->acronym."  [".substr($biocollection->name,0,30)."..]"}}
             </option>
             @endforeach
           </select>
         </div>
         </div>

         <div class="mb-3">
         <label class="form-label mandatory">
           @lang('messages.request_message')
         </label>
         <a data-bs-toggle="collapse" href="#hint6" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
         <div class="">
           <textarea name="message" class="form-control" rows=5 required>{{ isset($odbrequest) ? $odbrequest->message : old('message')}}</textarea>
         </div>
           <div id="hint6" class="odb-hint collapse">
              @lang('messages.request_message_hint')
           </div>
         </div>

         <div class="mb-3">
           <input id='export_sumbit' class="btn btn-success" name="submit" type='submit' value='@lang('messages.save')' >
         </div>
		    </form>
                </div>
            </div>
    </div>
  </div>
@endsection


