@extends('layouts.app')

@section('content')
<div id='contents' class="container">
  <br>
  <div class="card col-lg-8 offset-lg-2 col-md-6 offset-md-3">
        <div class="card-header">
        @lang('messages.request_title')
    </div>
	  <div class="card-body">
      @php
        $hasvouchers = false;
        $mapurl = url('individuals/map');
        if($odbrequest->vouchers->count()) {
          $mapurl = url('vouchers/map');
          $hasvouchers = true;
        }
      @endphp
      <dl class="">
        <dt class='dt-odb'>
          @lang('messages.request_date')
        </dt>
        <dd class='mb-3'>
         {!! $odbrequest->created_at !!}
         </dd>
         <dt class='dt-odb'>
          @lang('messages.request_updated')
        </dt>
        <dd class='mb-3'>
        @if($hasvouchers)
          {!! $odbrequest->vouchers->map(function($v) { return $v->pivot->updated_at;})->max()->format('Y-m-d') !!}
        @else
          {!! $odbrequest->individuals->map(function($v) { return $v->pivot->updated_at;})->max()->format('Y-m-d') !!}
        @endif
      </dd>
      <dt class='dt-odb'>
          @lang('messages.request_byuser')
        </dt>
        <dd class='mb-3'>
 {!! $odbrequest->userFormated !!}
  </dd>
        @if(isset($odbrequest->institutionFormated))
        <dt class='dt-odb'>
              @lang('messages.institution')
            </dt>
            <dd class='mb-3'>
              {{ $odbrequest->institutionFormated }}
         </dd>
       @endif
       <dt class='dt-odb'>
           @lang('messages.request_type'):
        </dt>
        <dd class='mb-3'>
       {{ $odbrequest->type }}
     </dd>
     <dt class='dt-odb'>
       @lang('messages.request_message'):
     </dt>
     <dd class='mb-3'>
       {{ $odbrequest->message }}
     </dd>
     <dt class='dt-odb'>
         @lang('messages.status'):
       </dt>
       <dd class='mb-3'>
        @if($odbrequest->status_closed)
        <i class="fas fa-lock fa-lg" style="color: red;"></i>
        @lang('messages.closed')!
      @else
        <i class="fas fa-lock-open fa-lg" style="color: green;"></i>
        @lang('messages.request_inprogress')
      @endif
    </dd>
  </dl>
      <p>
      <table class="table table-striped user-table" style="table_layout: auto; width: 30%">
      <thead>
        <th><strong>@lang('messages.status')</strong></th>
        <th ><strong>
          @if($hasvouchers)
            @lang('messages.vouchers')
          @else
            @lang('messages.individuals')
          @endif
        </strong></th>
      </thead>
      <tbody>
      @foreach($odbrequest->status as $status)
        <tr>
        <td class="table-text">
            {{ Lang::get('levels.status.'.$status->status ) }}
        </td>
        <td class="table-text">
            {{ $status->count }}
        </td>
        </tr>
      @endforeach
      <tr>
      <td class="table-text">
            <strong>Total</strong>
      </td>
      <td class="table-text">
        @if($hasvouchers)
          {{ $odbrequest->vouchers->count() }}
        @else
          {{ $odbrequest->individuals->count() }}
        @endif


      </td>
      </tr>

    </tbody>
    </table>
  </p>
  <div class="row">
    @if($hasvouchers)
      <button type="button" class="btn btn-default btntoogle" data='vouchers' >@lang('messages.vouchers')</button>
    @else
      <button type="button" class="btn btn-default btntoogle" data='vouchers' >@lang('messages.individuals')</button>
    @endif
  <!---
  @can ('update', $odbrequest)
  &nbsp;
  &nbsp;
    <a href="{{ url('odbrequests/'. $odbrequest->id. '/edit')  }}" class="btn btn-success" name="submit" value="submit">
      <i class="fa fa-btn fa-plus"></i>
      @lang('messages.edit')
    </a>
  @endcan
  --->
  @can ('delete', $odbrequest)
    <form action="{{ url('odbrequests/'.$odbrequest->id) }}" method="POST" class="form-horizontal">
     {{ csrf_field() }}
     {{ method_field('DELETE') }}
     <button type="submit" class="btn btn-danger">
       @lang('messages.remove')
     </button>
    </form>
  @endcan
 </div>
</div>
</div>

<div class="card vouchers collapse">
    <div class="card-header">
      @if($hasvouchers)
        @lang('messages.vouchers')
      @else
        @lang('messages.individuals')
      @endif
      @if (Auth::user()->can('annotate',$odbrequest))
          &nbsp;&nbsp;
          <button type="button" class="btn btn-primary" id='odb-request-annotate'>@lang('messages.request_annotate')</button>
      @endif
    </div>
    @if (Auth::user())
    {!! View::make('common.exportdata')->with([
          'object' => isset($object) ? $object : null,
          'request_id' => isset($odbrequest) ? $odbrequest->id : null,
          'object_second' => isset($object_second) ? $object_second : null,
          'export_what' => $hasvouchers ? 'Voucher' : 'Individual',
    ]) !!}
    <div class="card-body" id='annotate_panel' hidden>
      <div class="col-sm-offset-3 col-sm-6" id="user_annotate_hint" ></div>
      <div class="col-sm-12" id='user_annotate_form' hidden>
      <br>
      <form action="{{ url('odbrequests/'.$odbrequest->id.'/annotate') }}" method="POST" class="row g-2" >
          <!-- csrf protection -->
          {{ csrf_field() }}
          @if($hasvouchers)
          <input type='hidden' name='voucher_ids' id='voucher_ids' value="" >
          <input type='hidden' name='hasvouchers' id='hasvouchers' value="1" >
          @else
          <input type='hidden' name='hasvouchers' id='hasvouchers' value="0" >
          <input type='hidden' name='individual_ids' id='individual_ids' value="" >
          @endif
          <input type='hidden' name='biocollection_id' value="{{ isset($biocollection) ? $biocollection->id : null }}" >
          <input type='hidden' name='odbrequest_id' id='odbrequest_id' value="{{ isset($odbrequest) ? $odbrequest->id : null}}" >


          <div class="mb-3">
            <label class="form-label ">
              @lang('messages.request_type')
            </label>
            <div class="">
              <select class="form-control" name="request_type" id='request_type'>
                  <option value=""  ></option>
                  <option value="notes"  >@lang('messages.request_addnote')</option>
                  @if(!$odbrequest->status_closed)
                    <option value="status"  >@lang('messages.request_changestatus')</option>
                    @if(!$hasvouchers)
                    <option value="identify"  >@lang('messages.accept_identification')</option>
                    <option value="register"  >@lang('messages.register') @lang('messages.vouchers')</option>
                    @endif
                  @endif
              </select>
            </div>
          </div>
          <div class="mb-3" id='changestatus' hidden>
            <label class="form-label ">
              @lang('messages.request_changestatus')
            </label>
            <a data-toggle="collapse" href="#hint6" class="btn btn-default">?</a>
            <div class="">
              <select class="form-control" name="newstatus" >
                @php
                  if (!$hasvouchers) {
                    $status_options = $status_options_individuals;
                  }
                  $options = '<option value=""></option>';
                  foreach($status_options as $st ) {
                    $options .= '<option value="'.$st.'">'.Lang::get('levels.status.'.$st).'</option>';
                  }
                @endphp
                {!! $options !!}
              </select>
            </div>
          </div>
          <div class="voucher_registration_form"  hidden>
            <div class="mb-3">
            <label class="form-label mandatory" for="biocollection_voucher_number">
              @lang('messages.biocollection_startnumber')
            </label>
            <a data-bs-toggle="collapse" href="#registration_hint" class="odb-unstyle">
              <i class="far fa-question-circle"></i>
            </a>
            <div class="">
              <input type="text" name="biocollection_voucher_number"
              value="" class="form-control"  id="biocollection_voucher_number">
            </div>
            <div class="">
              @lang('messages.biocollection_lastnumber'):
              <strong>{{ $nextBiocollectionNumber }}</strong>
              @lang('messages.for') {!! $biocollection->rawLink() !!}
            </div>
            <div id="registration_hint" class="odb-hint collapse">
               @lang('messages.biocollection_startnumber_hint')
            </div>
          </div>

          <div class="mb-3">
            <label class="form-label mandatory" for="dataset_id">
              @lang('messages.dataset')
            </label>
            <div class="">
              <input type="text" name="dataset_autocomplete" id="dataset_autocomplete"
              class="form-control autocomplete" value="">
              <input type="hidden" name="dataset_id" id="dataset_id" value="">
            </div>
          </div>
          <div id="registration_hint" class="odb-hint collapse">
               @lang('messages.biocollection_startnumber_hint')
          </div>
        </div>

          <div class="mb-3" id='addnote' hidden>
            <label class="form-label" for='notes'>
              @lang('messages.request_addnote')
            </label>
            <div class="">
              <textarea name="notes" class="form-control" rows=5 id='notes'></textarea>
            </div>
          </div>

          <div class="mb-3">
            <span id='submitting' hidden><i class="fas fa-sync fa-spin"></i></span>
            <input id='export_sumbit' class="btn btn-success" name="submit" type='submit' value='@lang('messages.request_annotate')' >
            </div>
      </form>
      </div>
    </div>
    @else
      <br>
    @endif
    <br>
    <form action="{{ $mapurl }}" method="POST" class="form-horizontal" id='odb-map-occurrences'>
      <!-- csrf protection -->
      {{ csrf_field() }}
      <input type="hidden" name="idstomap" value="" id='odb-map-selected-rows'>
    </form>
    <div class="card-body">
      {!! $dataTable->table([],true) !!}
    </div>
</div>


        <!--- </div> -->
    </div>
@endsection
@once
  @push ('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}

    <script type="module">
    $(document).ready(function() {
    const table = $('#dataTableBuilder');
    table.on('preXhr.dt',function(e,settings,data) {
      data.status = $("#status option").filter(':selected').val();
      /* an any defined in the export form */
      data.request_id = $("input[name='odbrequest_id']").val();
      /*console.log(data.level,data.project,data.location,data.taxon); */
    });
    $('#status').on('change',function() {
       table.DataTable().ajax.reload();
       return false;
    });
  
$("#dataset_autocomplete").odbAutocomplete("{{url('datasets/autocomplete')}}","#dataset_id", "@lang('messages.noresults')");


/* generic function to close and focus on page elements */
$('.btntoogle').on('click',function() {
    var element = $(this).attr('data');
    var container = $('.container');
    if ($('.'+element).is(":hidden")) {
      $('.'+element).show();
      var scrollTo = $('.'+element);
      var position = scrollTo.offset().top;
      window.scrollTo(null,position);
      var table =  $('#dataTableBuilder').DataTable();
      table.columns.adjust().draw();
    } else {
      $('.'+element).hide();
      window.scrollTo(null,container.offset().top);
    }
});

$('#odb-request-annotate').on('click', function(e){
  var dt =  $('#dataTableBuilder').DataTable();
  var rows_selected = dt.column( 0 ).checkboxes.selected();
  var nrecords = rows_selected.length;
  var isvisible = $('#annotate_panel').is(":visible");
  if (nrecords == 0) {
    $('#voucher_ids').val('');
    var text1 = '<div class="alert alert-danger" >{{Lang::get("messages.request_select_records")}}</div>';
    var text2 = '<div class="alert alert-warning"><button class="btn btn-warning" id="annotateall">&nbsp;&nbsp;{{Lang::get("messages.request_annotate_all")}}</button></div>';
    $('#user_annotate_hint').html(text1+text2);
    //$("#user_annotate_form").attr("style", "display:none");
    $("#user_annotate_form").hide();
  } else {
    $('#user_annotate_hint').html('<div class="alert alert-success" >' + nrecords + '{{Lang::get("messages.selected_records")}}</div>');
    //$("#user_annotate_form").attr("style", "display:block");
    $("#user_annotate_form").show();
    var hasvouchers = "{{ !$hasvouchers }}";
    if (hasvouchers) {
      $('#individual_ids').val(rows_selected.join());
    } else {
      $('#voucher_ids').val(rows_selected.join());
    }
  }
  if (!isvisible) {
    $("#annotate_panel").show();
  } else {
    $("#annotate_panel").hide();
  }
});

$(document).on('click','#annotateall',function() {
  var isvisible = $("#user_annotate_form").is(':visible');
  if (!isvisible) {
    $("#user_annotate_form").show();
    $(".alert").hide();
  } else {
    $("#user_annotate_form").hide();
  }
});

$('#request_type').on('change',function() {
  var value = $('#request_type').find(":selected").val();
  if (value=='register') {
      $('.voucher_registration_form').show();
      $('#changestatus').hide();
      $('#addnote').hide();
  } else {
      $('.voucher_registration_form').hide();
      if (value=='status'){
        $('#changestatus').show();
        $('#addnote').hide();
      } else {
        $('#changestatus').hide();
        if (value=='notes'){
          $('#addnote').show();
        } else {
          $('#addnote').hide();
        }
      }
  }
});


$('tbody').on('click', 'tr',function () {
    //console.log( table.row( this ).data() );
    var table = $('#dataTableBuilder').DataTable();
    var id =  table.row( this ).data().id;
    var id = 'request_note_'+id;
    if($('#'+id).is(':hidden')) {
      $('#'+id).show();
    } else {
      $('#'+id).hide();
    }
});


});

</script>

@vite('resources/assets/js/custom.js')
  @endpush
@endonce
