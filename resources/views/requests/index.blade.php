@extends('layouts.app')

@section('content')
<div id='contents' class="container">
  <br>
  <div class="col-md-12 offset-md-0">
      <!-- Registered Persons -->
      <div class="card">
          <div class="card-header">
                @lang('messages.registered_requests')
                @if(isset($biocollection))
                  &nbsp;&nbsp;
                  <strong>
                  {{ $biocollection->name}}
                  </strong>
                @endif
                @if(isset($user))
                  &nbsp;&nbsp;
                  <strong>
                  {!! isset($user->person) ? $user->person->rawLink() : $user->email !!}
                  </strong>
                @endif
                &nbsp;&nbsp;
                <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
            <div id="help" class="odb-hint collapse">
              @lang('messages.requests_hint')
            </div>
          </div>
            @if(isset($biocollection))
              <input type="hidden" name="biocollection_id" value="{{ $biocollection->id }}">
            @endif
            @if(isset($user))
              <input type="hidden" name="user_id" value="{{ $user->id }}">
            @endif
            <div class="card-body">
            {!! $dataTable->table([],true) !!}
          </div>
          </div>
    </div>
  </div>    
@endsection
@once
  @push ('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}

    <script type="module">
    $(document).ready(function() {
      const table = $('#dataTableBuilder');
      table.on('preXhr.dt',function(e,settings,data) {
        data.status = $("#status option").filter(':selected').val();
        /* an any defined in the export form */
        data.biocollection_id = $("input[name='biocollection_id']").val();
        data.user_id = $("input[name='user_id']").val();
        /*console.log(data.level,data.project,data.location,data.taxon); */
      });
      table.DataTable().ajax.reload();
      $('#status').on('change',function() {
        table.DataTable().ajax.reload();
        return false;
      });
    });
  </script>
  @vite('resources/assets/js/custom.js')
  @endpush
@endonce