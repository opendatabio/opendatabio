@extends('layouts.app')
@section('content')
<div id='contents' class="container">
  <br>
  <div class="col-lg-8 offset-lg-2 col-md-6 offset-md-3 col-sm-6 offset-sm-3">
    <div class="card">

      <!-- Project basic info block -->
      <div class="card-header">
        @lang('messages.location') @lang('messages.for') @lang('messages.individual'): <strong>{{ $individual_location->individual->fullname}}</strong>
        <span class="history" style="float:right">
          <a href="{{url("individual-location/$individual_location->id/activity")}}">
            @lang ('messages.see_history')
          </a>
        </span>
      </div>
      <div class="card-body">
        <dl class="row">
        <dt class="dt-odb">@lang('messages.individual')</dt>
        <dd>
          {!! $individual_location->individual->rawLink() !!}
        </dd>


        <dt class="dt-odb">@lang('messages.location')</dt>
        <dd>
          {!! $individual_location->location->rawLink() !!}
        </dd>

        <dt class="dt-odb">@lang('messages.higherGeography')</dt>
        <dd>
          {!! $individual_location->location->higherGeography !!}
        </dd>

        <dt class="dt-odb">@lang('messages.latitude')</dt>
        <dd>
          {!! round($individual_location->latitude,6); !!}
        </dd>

        <dt class="dt-odb">@lang('messages.longitude')</dt>
        <dd>
          {!! round($individual_location->longitude,6); !!}
        </dd>

        <dt class="dt-odb">@lang('messages.date')</dt>
        <dd>
          {!! $individual_location->date_time !!}
        </dd>
        @if($individual_location->location->adm_level==999)
          @if($individual_location->angle)
        <dt class="dt-odb">@lang('messages.angle')</dt>
        <dd>
          {!! $individual_location->angle !!}
        </dd>
        @endif
        @if($individual_location->distance)
        <dt class="dt-odb">@lang('messages.distance')</dt>
        <dd>
          {!! $individual_location->distance !!}
        </dd>
        @endif
        @else
        @if($individual_location->x)
        <dt class="dt-odb">X</dt>
        <dd>
          {!! $individual_location->x !!}
        </dd>
        @endif

        @if($individual_location->x)
        <dt class="dt-odb">Y</dt>
        <dd>
          {!! $individual_location->y !!}
        </dd>
        @endif
        @endif
        @if ($individual_location->notes)
            {!! $individual_location->formated_notes !!}
        @endif
      </dl>

  </div>
  </div>

  </div>

</div>
@endsection
