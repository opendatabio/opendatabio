@extends('layouts.app')
@section('content')
<div id='contents' class="container">
  <br>
  <!--- <div class="col-sm-offset-2 col-sm-8"> -->
    <div class="card">
      <?php
        if (isset($dataset)) {
          $is_open = in_array($dataset->privacy,[App\Models\Dataset::PRIVACY_REGISTERED,App\Models\Dataset::PRIVACY_PUBLIC]);
        } else {
          $is_open = (null != Auth::user()) ? true : false;
        }
       ?>
      @if($is_open)
      <div class="card-header">
        @lang('messages.individuallocations') @lang('messages.for')
          @lang('messages.dataset'): <strong>{{ $dataset->name}}</strong>
      </div>
      <div class="card-body">
      @if (Auth::user())
        {!! View::make('common.exportdata')->with([
              'object' => isset($dataset) ? $dataset : null,
              'object_second' => null,
              'export_what' => 'individual-location'
        ]) !!}
        <br>
      @endif
      {!! $dataTable->table([],true) !!}
      </div>
      @endif
      </div>
    <!-- </div> -->
</div>
@endsection

@once
    @push('scripts')
      {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
      @vite('resources/assets/js/custom.js')  
    @endpush
@endonce
