@extends('layouts.app')

@section('content')
<div class="container" id='contents'>
  <br>
  <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
      <div class="card-header">
        @if(!isset($individual))
          @lang('messages.new_individual')
        @else
          @lang('messages.editing') @lang('messages.individual') <strong>{{ $individual->fullname }}</strong>
        @endif
        &nbsp;
        <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        <div id="help" class="odb-hint collapse">
            @lang('messages.hint_individual_create')
        </div>
      </div>

        <div class="card-body">
          @if (isset($individual))
    		    <form class='row g-2' action="{{ url('individuals/' . $individual->id)}}" method="POST" >
              {{ method_field('PUT') }}
            @else
  		    <form class='row g-2'  action="{{ url('individuals')}}" method="POST" >
            @endif

            {{ csrf_field() }}


          <!-- TAG OR NUMBER FOR INDIVIDUAL  -->
          <div class="mb-3">
              <label for="tag" class="form-label mandatory">
                @lang('messages.individual_tag')
              </label>
<a data-bs-toggle="collapse" href="#hint1" class="odb-unstyle"><i class="far fa-question-circle"></i></a>

          	     <input type="text" name="tag" id="tag" class="form-control" value="{{ old('tag', isset($individual) ? $individual->tag : null) }}">
                <div id="hint1" class="odb-hint collapse">
                  @lang('messages.hint_individual_tag')
                </div>
          </div>


          <!-- collector -->
          <div class="mb-3">
              <label for="collectors" class="form-label mandatory">
                @lang('messages.collector')
              </label>
              <a data-bs-toggle="collapse" href="#hint5" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          	  <div>
                {!! Multiselect::autocomplete('collector',
                  $persons->pluck('abbreviation', 'id'),
                  isset($individual) ? $individual->collectors->pluck('person_id') :
                  (empty(Auth::user()->person_id) ? '' : [Auth::user()->person_id] ),
                  ['class' => 'multiselect form-control'])
                !!}
              </div>
                <div id="hint5" class="odb-hint collapse">
          	       @lang('messages.individual_collectors_hint')
                 </div>
          </div>


          <!-- DATE INCOMPLETE DATE -->
          <div class="mb-3">
            <label for="date" class="form-label mandatory">
              @lang('messages.tag_date')
            </label>
            <a data-bs-toggle="collapse" href="#hint4" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
            <div >
              {!! View::make('common.incompletedate')->with([
                  'object' => isset($individual) ? $individual : null,
                  'field_name' => 'date'
              ]) !!}
            </div>
              <div id="hint4" class="odb-hint collapse">
               @lang('messages.individual_date_hint')
              </div>
          </div>



<!-- LOCATION BLOCK -->
  <!-- when creating an individual a single location is allowed -->
  <!-- when editing, multiple locations may be added -->
          @php
          $current_location_print = "";
          if (isset($individual)) {
              $current_location_print = $individual->LocationDisplay();
          }
          if (isset($location)) {
              $current_location_print = $location->fullname." ".$location->coordinatesSimple;
          }
          @endphp

          <!-- multiple locations may be added to existing records only -->
          <!-- modal option only when editing an individual or when inserting an individual without coming from a location record -->
          @if (!isset($location))
            <div class="mb-3 location-show">
                <label for="location_id" class="form-label mandatory">
                  @lang('messages.location')
                </label>
                <div>
                    {!! $current_location_print !!}
                    <!-- only for new individual location records -->
                    <div id='location_show' class='alert-success'></div>
                    <input type="hidden" name="location_id" >
                    <input type="hidden" name="x" >
                    <input type="hidden" name="y" >
                    <input type="hidden" name="angle" >
                    <input type="hidden" name="distance" >
                    <input type="hidden" name="altitude" >
                    <input type="hidden" name="location_date_time" >
                    <input type="hidden" name="location_notes" >

                    <!-- open location modal  -->
                    @if (isset($individual))
                    <br>
                    <button type='button' id='showlocationdatatable' class="unstyle text-primary px-3"><i class="far fa-edit fa-lg"></i></button>
                    @endif
                    <button type="button" class="unstyle text-success px-3" id="add_location" data-bs-toggle="modal" data-bs-target="#locationModal"><i class='fa fa-plus-square fa-lg'></i></button>



                    <!-- Modal -->
                     <div class="modal fade" id="locationModal" role="dialog">
                       <div class="modal-dialog">
                         <!-- Modal content-->
                         <div class="modal-content">
                           <div class="modal-header">
                             <button type="button" class="unstyle" data-bs-dismiss="modal"><i class="far fa-times-circle"></i></button>
                             <h6 class="modal-title">@lang('messages.location_individual')</h6>
                           </div>
                           <div class="modal-body">
                             {!! View::make('individuals.locationmodal')->with([
                                 'modal_time' => "00:00:00"
                             ]) !!}
                           </div>
                           <div class="modal-footer">
                          <!-- if individual is not set, nor location, then modal return value to form -->
                          @if (!isset($individual))
                              <button type="button" class="btn btn-success" data-bs-dismiss="modal" id='location_return'  >@lang('messages.save')</button>
                          @else
                            <!-- if else, editing a record the modal is eitheir adding or editing a location from the datatable list and hence, update is concluded by closing the modal -->
                            <button type="button" class="btn btn-success" id='location_save' >@lang('messages.save')</button>
                          @endif
                            <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">@lang("messages.close")</button>
                           </div>
                         </div>
                       </div>
                     </div>
                </div>
                @if (isset($individual))

                <!-- this fields is here only to be able to update the summary model -->
                <input type="hidden" name="oldlocation_id" value="{{ $individual->locations->last()->id }}">

                <div id='locationdatatable' >
                    <br><br>
                      {!! $dataTable->table([],true) !!}
                    <br><br>
                </div>
                @endif
            </div>
          @else
            <!-- will enter here only when creating a new individual for a specified location -->
            <!-- location is defined check for x and y if the case -->
            <div class="mb-3 location-show">
                <label for="location_id" class="form-label mandatory">
                  @lang('messages.location')
                </label>
                <div >
                    {!! $current_location_print !!}
                    <input type="hidden" name="location_id" value="{{ old('location_id', $location->id) }}">
                    <input type="hidden" id="location_type" name="location_type" value ="{{old('location_type', $location->adm_level)}}">
                  </div>
            </div>


            <div class="mb-3 super-relative">
                <label for="relative_position" class="form-label">@lang('messages.relative_position')</label>
                <a data-bs-toggle="collapse" href="#hint12" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
                 <div class="super-xy">
              X: <input type="text" name="x" id="x" class="form-control latlongpicker" value="{{ old('x', null) }}">(m)&nbsp;
              Y: <input type="text" name="y" id="y" class="form-control latlongpicker" value="{{ old('y', null) }}">(m)
                </div>
                 <div class="super-ang">
                    @lang('messages.angle'): <input type="text" name="angle" id="angle" class="form-control latlongpicker" value="{{ old('x', null) }}">&nbsp;
                    @lang('messages.distance'): <input type="text" name="distance" id="distance" class="form-control latlongpicker" value="{{ old('y', null) }}">(m)
                </div>

                  <div id="hint12" class="odb-hint collapse">
                     @lang('messages.individual_position_hint')
                   </div>
            </div>

          @endif






<!-- dataset -->
<!-- biocollection staff cannot change a dataset for an individual -->
@php
 $dataset_readonly = false;
 if(isset($individual)) {
    if (!Auth::user()->can('makerequest',$individual)) {
      $dataset_readonly = true;
    }
 }
@endphp
<div class="mb-3">
    <label for="dataset" class="form-label mandatory">
      @lang('messages.dataset')
    </label>
    <a data-bs-toggle="collapse" href="#dataset_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div >
      @if($dataset_readonly)
      <input type="text" name="dataset_autocomplete" class="form-control"
      value="{{ $individual->dataset->name }}" readonly>
      @else
      <input type="text" name="dataset_autocomplete" id="dataset_autocomplete" class="form-control autocomplete"
      value="{{ old('dataset_autocomplete', (isset($individual) and $individual->dataset_id) ? $individual->dataset->name : null) }}">
      @endif
      <input type="hidden" name="dataset_id" id="dataset_id"
      value="{{ old('dataset_id', isset($individual) ? $individual->dataset_id : null) }}">
    </div>
    <div id="dataset_hint" class="odb-hint collapse">
	     @lang('messages.individual_dataset_hint')
    </div>
</div>


<!-- NOTES -->
<div class="mb-3">
    <label for="notes" class="form-label">
      @lang('messages.notes')
    </label>
	  <textarea name="notes" id="notes" class="form-control">{{ old('notes', isset($individual) ? $individual->notes : null) }}</textarea>
</div>


<!-- IDENTIFICATION OPTIONS
1. an individual may have its own identification - in this case and Identification object is created for the individual
2. or have an identification that depends from another individual having a voucher in collection - in this case an identification relationship is established with the voucher_id field.
-->

@php
$identification_other = '';
$identification_self = '';
$identification_none = 'checked';

if (empty(old())) { // no "old" value, we're just arriving
    if (isset($individual)) {
      if ($individual->identification_individual_id != $individual->id) {
          $identification_other = 'checked';
      } elseif ($individual->identification_individual_id == $individual->id) {
          $identification_self = 'checked';
      } else {
          $identification_none = 'checked';
      }
   }
} else { // "old" value is available, work with it
  if (!empty(old('identification_selfother'))) {
    if (old('identification_selfother') == 1) {
        $identification_other = 'checked';
    } elseif (old('identification_selfother') == 0) {
        $identification_self = 'checked';
    } else {
        $identification_none = 'checked';
    }
  }
}
@endphp
<div class="mb-3">
  <div class="form-check">
    <input type="radio" id='no_identification' name="identification_selfother" value=2  {{$identification_none}} >
    <label class="form-check-label" for="no_identification">
    @lang('messages.identification_none')
    </label>
  </div>
  <div class="form-check">
    <input type="radio" id='other_identification' name="identification_selfother" value=1  {{$identification_other}} >
    <label class="form-check-label" for="other_identification">
    @lang('messages.identification_other')
    </label>
  </div>
  <div class="form-check">
    <input type="radio" id='self_identification' name="identification_selfother" value=0  {{$identification_self}} >
    <label class="form-check-label" for="self_identification">
    @lang('messages.identification_self')
    </label>
  </div>
</div>



<!-- OTHER IDENTIFICATION
Can only exists if individual has no vouchers, otherwise must have own id
-->
<div class="mb-3 identification_other">
  <label for="taxon_id" class="form-label mandatory">
    @lang('messages.identification_same_as')
  </label>
  <div class="col-sm-6">
    <input type="text" name="individual_autocomplete" id="individual_autocomplete" class="form-control autocomplete" value="">
    <input type="hidden" name="identification_individual_id" id="identification_individual_id" value="{{ old('identification_individual_id', (isset($individual) and $individual->identification_individual_id) ? $individual->identification_individual_id : null) }}">
  </div>
</div>


<!-- SELF IDENTIFICATION BLOCK --->
<div class="group-together identification_self">
  <div class="mb-3 identification_self">
    <div class="card-header">
        @lang('messages.identification')
    </div>
  </div>
  <div class="mb-3 identification_self">
    <label for="taxon_id" class="form-label mandatory">
      @lang('messages.taxon')
    </label>
    <a data-bs-toggle="collapse" href="#hint6" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
      <input type="text" name="taxon_autocomplete" id="taxon_autocomplete" class="form-control autocomplete"
    value="{{ old('taxon_autocomplete', (isset($individual) and $individual->identification and $individual->identification->taxon) ? $individual->identification->taxon->fullname : null) }}">
      <input type="hidden" name="taxon_id" id="taxon_id"
    value="{{ old('taxon_id', (isset($individual) and $individual->identification and $individual->identification->taxon) ? $individual->identification->taxon_id : null) }}">
      <div id="hint6" class="odb-hint collapse">
	       @lang('messages.individual_taxon_hint')
       </div>
   </div>

   <div class="mb-3 identification_self">
    <label for="modifier" class="form-label">
      @lang('messages.modifier')
    </label>
    <a data-bs-toggle="collapse" href="#hint9" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <?php $selected = old('modifier', (isset($individual) and $individual->identification) ? $individual->identification->modifier : null); ?>
    <select name="modifier" class="form-select col-sm-2" aria-label="modifier">
      @foreach (App\Models\Identification::MODIFIERS as $modifier)
           <option {{ $modifier == $selected ? 'selected' : '' }} value="{{$modifier}}" >
            @lang('levels.modifier.' . $modifier)
           </option>
       @endforeach
    </select>
      <div id="hint9" class="odb-hint collapse">
	       @lang('messages.individual_modifier_hint')
      </div>
  </div>

  <div class="mb-3 identification_self">
    <label for="identifier_id" class="form-label mandatory">
      @lang('messages.identifier')
    </label>
    <a data-bs-toggle="collapse" href="#hint7" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div>
        {!! Multiselect::autocomplete('identifiers',
          $persons->pluck('abbreviation', 'id'),
          isset($individual) ? $individual->identification->collectors->pluck('id') :
          (empty(Auth::user()->person_id) ? '' : [Auth::user()->person_id] ),
          ['class' => 'multiselect form-control'])
        !!}
    </div>
    <div id="hint7" class="odb-hint collapse">
      @lang('messages.individual_identifier_id_hint')
    </div>
  </div>

<div class="mb-3 identification_self">
    <label for="identification_date" class="form-label mandatory">
@lang('messages.identification_date')
</label>
<a data-bs-toggle="collapse" href="#hint8" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div >
{!! View::make('common.incompletedate')->with([
    'object' => (isset($individual) and $individual->identification) ? $individual->identification : null,
    'field_name' => 'identification_date'
]) !!}
            </div>
    <div id="hint8" class="odb-hint collapse">
	@lang('messages.individual_identification_date_hint')
  </div>
</div>
<div class="mb-3 identification_self">
    <label for="biocollection_id" class="form-label">
@lang('messages.id_biocollection')
</label>
<a data-bs-toggle="collapse" href="#hint10" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	<?php $selected = old('biocollection_id', (isset($individual) and $individual->identification) ? $individual->identification->biocollection_id : null); ?>

	<select name="biocollection_id" id="biocollection_id" class="form-select" >
		<option value='' >&nbsp;</option>
	@foreach ($biocollections as $biocollection)
		<option value="{{$biocollection->id}}" {{ $biocollection->id == $selected ? 'selected' : '' }}>
            {{ $biocollection->acronym }}
		</option>
	@endforeach
	</select>
    <div id="hint10" class="odb-hint collapse">
	@lang('messages.individual_biocollection_id_hint')
    </div>
</div>

<div class="mb-3 biocollection_reference">
    <label for="biocollection_reference" class="form-label mandatory">
@lang('messages.biocollection_reference')
</label>
	<input type="text" name="biocollection_reference" id="biocollection_reference" class="form-control" value="{{ old('biocollection_reference', (isset($individual) and $individual->identification) ? $individual->identification->biocollection_reference : null) }}">
</div>

<div class="mb-3 identification_self">
    <label for="identification_notes" class="form-label">
@lang('messages.identification_notes')
</label>
	<textarea name="identification_notes" id="identification_notes" class="form-control">{{ old('identification_notes', (isset($individual) and $individual->identification) ? $individual->identification->notes : null) }}</textarea>
</div>
</div>
<!-- END IDENTIFICATION BLOCK -->


<br>
<div class="mb-3">
			    <div class="col-sm-offset-3 col-sm-6">
                @if(isset($individual))
                  <input type="hidden" id='editing' value="1">
                @else
                  <input type="hidden" id='editing' value="0">
                @endif
                <button type="submit" class="btn btn-success" name="submit" value="submit">
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.add')

				</button>
				<a href="{{url()->previous()}}" class="btn btn-warning">
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.back')
				</a>
			    </div>
			</div>
		    </form>
        </div>
    </div>

</div>
</div>

@endsection

@once
    @push('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
    {!! Multiselect::scripts('collector', url('persons/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
    {!! Multiselect::scripts('identifiers', url('persons/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
    @vite('resources/assets/js/custom.js')                
    <script type='module'>
$(document).ready(function() {


function setBiocollectionReference(vel) {
    var adm = $('#biocollection_id option:selected').val();
    if ("undefined" === typeof adm) {
        return; // nothing to do here...
    }
    switch (adm) {
    case "": // no biocollection
        $(".biocollection_reference").hide(vel);
        break;
    default: // other
        $(".biocollection_reference").show(vel);
    }
}
function setAngXYFields(vel) {
  var adm = $('#location_type').val();
  if ("undefined" === typeof adm) {
      return; // nothing to do here...
  }
  switch (adm) {
  case "100": // plot
  case "101": // transect; fallover!
      $(".super-xy").show(vel);
      $(".super-relative").show(vel);
      $(".super-ang").hide(vel);
      break;
  case "999": // point
      $(".super-xy").hide(vel);
      $(".super-relative").show(vel);
      $(".super-ang").show(vel);
      break;
  default: // other
      $(".super-relative").hide(vel);
      break;
  }
}


function setIdentificationFields(vel) {
  var fromvoucher = $('input[name=identification_selfother]:checked').val();
  if (fromvoucher == 1) {
    $('.identification_other').show();
    $('.identification_self').hide();
  } else if (fromvoucher == 0) {
    $('.identification_other').hide();
    $('.identification_self').show();
  } else {
    $('.identification_other').hide();
    $('.identification_self').hide();
  }
}


function setLocationDate() {
  var year = $('#date_year option:selected').val();
  var month = $('#date_month option:selected').val();
  month = String(month).padStart(2, '0');
  var day = $('#date_day option:selected').val();
  day = String(day).padStart(2, '0');
  var date = year + "-" + month + "-" + day;
  var hasdate = $('input[name=modal_date]').val();
  var editing = $('#editing').val();
  if (day>0 & month>0 & hasdate === "" & editing==0) {
    //alert(hasdate + 'will be here' + date );
    $('input[name=modal_date]').val(date);
  }
}


$("#dataset_autocomplete").odbAutocomplete("{{url('datasets/autocomplete')}}","#dataset_id", "@lang('messages.noresults')");



$('#locationdatatable').hide();
$('#coordinates').hide();
$('.savedetect').hide();
$('.super-relative').hide();
$('.location-extra').hide();
$('#ajax-error-modal').hide();
$('.save_return').hide();
$('#location_show').hide();

$('#showlocationdatatable').on('click',function(){
if ($('#locationdatatable').is(":hidden")) {
    $('#locationdatatable').show();
} else {
    $('#locationdatatable').hide();
}
});


$("#location_autocomplete").odbAutocomplete(
    "{{url('locations/autocomplete')}}", "#location_id", "@lang('messages.noresults')", null, undefined,
    function(suggestion) {
        $("#location_type").val(suggestion.adm_level);
        $('.location-extra').show();
        setAngXYFields(0);
        setLocationDate(0);
});

$("#individual_autocomplete").odbAutocomplete("{{url('individuals/autocomplete')}}", "#identification_individual_id", "@lang('messages.noresults')");


$("#taxon_autocomplete").odbAutocomplete("{{url('taxons/autocomplete')}}", "#taxon_id","@lang('messages.noresults')",
    function() {
        // When the identification of a individual or voucher is changed, all related fields are reset
        $('input:radio[name=modifier][value=0]').trigger('click');
        $("#identifier_id").val('');
        $('#identifier_autocomplete').val('');
        $("#identification_date_year").val((new Date).getFullYear());
        $("#identification_date_month").val((new Date).getMonth());
        $("#identification_date_day").val((new Date).getDay());
        $("#biocollection_id").val('');
        $("#biocollection_reference").val('');
        $("#identification_notes").val('');
});


// trigger this on page load
setBiocollectionReference(0);
setAngXYFields(0);
setIdentificationFields(400);


$("input[name=identification_selfother]").change(function() {
  setIdentificationFields(400);
});


$("#biocollection_id").change(function() { setBiocollectionReference(400); });



/** USED IN THE LOCATION MODAL */
$("#autodetect").click(function(e) {
  $( "#spinner" ).css('display', 'inline-block');
  $(".savedetect").hide();
  $.ajaxSetup({ // sends the cross-forgery token!
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault(); // does not allow the form to submit
  $.ajax({
    type: "POST",
    url: $('input[name="route-url"]').val(),
    dataType: 'json',
          data: {
            'adm_level': 999,
            'geom': null,
            'lat1': $('input[name="lat1"]').val(),
            'lat2': $('input[name="lat2"]').val(),
            'lat3': $('input[name="lat3"]').val(),
            'latO': $("input[name='latO']:checked").val(),
            'long1': $('input[name="long1"]').val(),
            'long2': $('input[name="long2"]').val(),
            'long3': $('input[name="long3"]').val(),
            'longO': $("input[name='longO']:checked").val(),
            'geom_type': null
          },
    success: function (data) {
      $( "#spinner" ).hide();
      if ("error" in data) {
        $( "#ajax-error-modal" ).show();
        $( "#ajax-error-modal" ).text(data.error);
      } else {

        // ONLY removes the error if request is success
        var haslocation = data.detectedLocation[0];
        if (haslocation != null) {
          $("#detected_location").text("@lang('messages.location_exists'):  " + data.detectdata[1] );
          $('#locationfield').show();
          $("#location_id").val(data.detectedLocation[0]);
          $("#location_autocomplete").val(data.detectedLocation[1]);
          $("#location_type").val(data.detectedLocation[2]);
          $('#coordinates').hide();
          if ($('.location-extra').is(":hidden")) {
            $('.location-extra').show();
          }
          setAngXYFields(0);
          setLocationDate();
        } else {




          $("#detected_location").text("@lang('messages.location_belongs'):  " + data.detectdata[0] );
          $('#coordinates').hide();
          $(".savedetect").show();

          var detectrelated = data.detectrelated;
          var related = detectrelated.map(
            function (value, index, array) {
              return value[0];
            }
          );

          //create point name to be saved as a new location if confirmed by user
          //var name = data.detectdata[4].replace(/[A-Z\(\)-\.\s]/g,"");
          //if (name.length >12) {
            //name = name.substring(0,11);
          //}
          name = "{{config('app.unnamedPoint_basename')}}" + "_" + "{{ uniqid() }}";
          $("input[name=location_name]").val(name);
          $("input[name=location_parent_id]").val(data.detectdata[1]);
          $("input[name=location_geom]").val(data.detectdata[2]);
          $("input[name=location_related]").val(related);
          $("input[name=location_adm_level]").val("{{ App\Models\Location::LEVEL_POINT }}");

        }
        $( "#ajax-error-modal" ).hide();
      }
    },
    error: function(e){
      $( "#spinner" ).hide();
      $( "#ajax-error-modal" ).show();
      $( "#ajax-error-modal" ).text('Error sending AJAX request');
    }
  })
});




$("input[name=latlong]").change(function() {
$(".savedetect").hide();
var aslatlong = $('input[name=latlong]:checked').val();
if (aslatlong == 1) {
    $('#coordinates').show();
    $('#locationfield').hide();
    $('.location-extra').hide();
    $('.super-relative').hide();
    $(".save_return").hide();
} else {
    $('#coordinates').hide();
    $('#locationfield').show();
    var haslocation = $('input[name=location_id]').val();
    if ($('.location-extra').is(":hidden") & haslocation>0) {
    $('.location-extra').show();
    }
    $(".save_return").show();
    $('#location_type').val(null);
    $('#location_id').val(null);
    $('#location_autocomplete').val(null);
    setAngXYFields(400);
}
});

/** USED IN THE LOCATION MODAL TO SAVE A NEW Location*/
$("#savedetected").click(function(e) {
$( "#spinner-save" ).css('display', 'inline-block');
$.ajaxSetup({ // sends the cross-forgery token!
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
e.preventDefault(); // does not allow the form to submit
$.ajax({
    type: "POST",
    url: $('input[name="route-url-save"]').val(),
    dataType: 'json',
        data: {
            'geom': $('input[name=location_geom]').val(),
            'name': $('input[name=location_name]').val(),
            'parent_id': $('input[name=location_parent_id]').val(),
            'adm_level': $('input[name=location_adm_level]').val(),
            'related_locations': $("input[name=location_related]").val(),
        },
    success: function (data) {
    $( "#spinner-save" ).hide();
    if ("error" in data) {
        $( "#ajax-error-modal" ).text(data.error);
        $( "#ajax-error-modal" ).show();
    } else {
        if (!$( "#ajax-error-modal" ).is(':hidden')) {
            $( "#ajax-error-modal" ).hide();
        }
        $(".savedetect").hide();
        $('#locationfield').show();
        $("#location_id").val(data.savedLocation[0]);
        $("#location_autocomplete").val(data.savedLocation[1]);
        $("#location_type").val(data.savedLocation[2]);
        $('#coordinates').hide();
        if ($('.location-extra').is(":hidden")) {
            $('.location-extra').show();
        }
        setAngXYFields(0);
        setLocationDate();
        $( "#ajax-error-modal" ).hide();
        $(".save_return").show();
    }
    },
    error: function(e){
    $( "#spinner-save" ).hide();
    $( "#ajax-error-modal" ).show();
    $( "#ajax-error-modal" ).text('Error sending AJAX request');
    }
})
});


/* USED WHEN CLOSING THE LOCATION MODAL WHEN CREATING A NEW INDIVIDUAL */
/* returns values from modal inputs to from inputs */
$('#location_return').click(function(e) {
    var text = $('input[name=location_autocomplete]').val();
    $('input[name=location_id]').val(
    $('input[name=modal_location_id]').val()
    );
    var x = $('input[name=modal_x]').val();
    if (x) {
    $('input[name=x]').val(x);
    text = text + '<br><strong>X</strong>: ' + x + 'm ';
    }
    var y = $('input[name=modal_y]').val();
    if (y) {
    $('input[name=y]').val(y);
    text = text + '<br><strong>Y</strong>: ' + x + 'm ';
    }
    var angle = $('input[name=modal_angle]').val();
    if (angle) {
    $('input[name=angle]').val(angle);
    text = text + '<br><strong>Angle</strong>: ' + angle + '&#176; ';
    }
    var distance = $('input[name=modal_distance]').val();
    if (distance) {
    $('input[name=distance]').val(distance);
    text = text + '<br><strong>Distance</strong>: ' + distance;
    }
    var altitude = $('input[name=modal_altitude]').val();
    if (altitude) {
    $('input[name=altitude]').val(altitude);
    text = text + '<br><strong>Altitude</strong>: ' + altitude + 'm ';
    }
    var datetime = $('input[name=modal_date]').val() + " " +  $('input[name=modal_time]').val();
    if ($('input[name=modal_date]').val()) {
    $('input[name=location_date_time]').val(datetime);
    text = text + '<br><strong>DateTime</strong>: ' + datetime;
    }
    var locnotes =  $('textarea[name=modal_notes]').val();
    if (locnotes) {
    $('input[name=location_notes]').val(locnotes);
    text = text + '<br><strong>Notes</strong>: ' + locnotes;
    }
    $('#location_show').html(text);
    $('#location_show').show();
});

$('#add_location').on('click',function(e) {
$('input[name=indloc_id]').val(null);
$("#location_id").val(null);
$("#location_autocomplete").val(null);
$("#location_type").val(null);
$('input[name=modal_x]').val(null);
$('input[name=modal_y]').val(null);
$('input[name=modal_angle]').val(null);
$('input[name=modal_distance]').val(null);
$('input[name=modal_altitude]').val(null);
$('textarea[name=modal_notes]').val(null);
$('input[name=modal_date]').val(null);
$('input[name=modal_time]').val("00:00:00");
$("#latlong_1").prop("checked", true);
});

//when editing a location get old values and fill modal
$('#dataTableBuilder').on( 'click', '.editlocation', function (e) {
    var indloc = $(this).data('indloc');
    e.preventDefault(); // does not allow the form to submit
    $.ajax({
    type: "GET",
    url: "{{ route('getIndividualLocation') }}",
    dataType: 'json',
    data: {
        'id': indloc
    },
    success: function (data) {
        $('input[name=indloc_id]').val(data.indLocation[0]);
        $("#location_id").val(data.indLocation[1]);
        $("#location_autocomplete").val(data.indLocation[2]);
        $("#location_type").val(data.indLocation[3]);
        $('input[name=modal_x]').val(data.indLocation[4]);
        $('input[name=modal_y]').val(data.indLocation[5]);
        $('input[name=modal_angle]').val(data.indLocation[6]);
        $('input[name=modal_distance]').val(data.indLocation[7]);
        $('input[name=modal_altitude]').val(data.indLocation[8]);
        $('textarea[name=modal_notes]').val(data.indLocation[9]);
        $('input[name=modal_date]').val(data.indLocation[10]);
        $('input[name=modal_time]').val(data.indLocation[11]);
        $('#location_type_selector').hide();
        setAngXYFields(0);
        $('.location-extra').show();
    },
    error: function(e){
        alert( indloc + " will be error" );
    }
    });
});


//if editing or inserting on a new record, save location
$('#location_save').on('click',function(e) {
$.ajaxSetup({ // sends the cross-forgery token!
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
e.preventDefault(); // does not allow the form to submit
$.ajax({
    type: "POST",
    url: "{{ route('saveIndividualLocation') }}",
    dataType: 'json',
    data: {
    'individual_id': "{{ isset($individual) ? $individual->id : null }}",
    'id': $('input[name=indloc_id]').val(),
    'location_id': $('input[name=modal_location_id]').val(),
    'x': $('input[name=modal_x]').val(),
    'y': $('input[name=modal_y]').val(),
    'angle': $('input[name=modal_angle]').val(),
    'distance': $('input[name=modal_distance]').val(),
    'altitude': $('input[name=modal_altitude]').val(),
    'notes': $('textarea[name=modal_notes]').val(),
    'date_time': $('input[name=modal_date]').val() + " " + $('input[name=modal_time]').val(),
    },
    success: function (data) {
    alert(data.saved);
    $('#dataTableBuilder').DataTable().ajax.reload();
    $("#locationModal").modal('hide');
    },
    error: function(xhr, status, error) {
    alert(xhr.responseText);
    }
});
});


//when deleting a location
$('#dataTableBuilder').on( 'click', '.deletelocation', function (e) {
    var indloc = $(this).data('indloc');
    e.preventDefault(); // does not allow the form to submit
    $.ajax({
    type: "GET",
    url: "{{ route('deleteIndividualLocation') }}",
    dataType: 'json',
    data: {
        'individual_id': "{{ isset($individual) ? $individual->id : null }}",
        'id': indloc
    },
    success: function (data) {
        alert(data.deleted);
        $('#dataTableBuilder').DataTable().ajax.reload();
    },
    error: function(e){
        alert( indloc + " will be error" );
    }
    });
});

});

    </script>
    @endpush
@endonce
