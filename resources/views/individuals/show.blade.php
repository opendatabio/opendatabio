@extends('layouts.app')
@section('content')
<div id='contents' class="container">
  <br>
  <div class="col-lg-8 offset-lg-2 col-md-6 offset-md-3 col-sm-6 offset-sm-3">
    <div class="card">

      <!-- Project basic info block -->
      <div class="card-header">
        @lang('messages.individual'): <strong>{{ $individual->fullname}}</strong>
        <span class="history" style="float:right">
          <a href="{{url('individuals/'.$individual->id.'/activity')}}">
            @lang ('messages.see_history')
          </a>
        </span>
      </div>
      <div class="card-body">
        <dl class="row">
        <dt class="dt-odb">@lang('messages.identification')</dt>
        <dd>
          @if (is_null($identification))
              @lang ('messages.unidentified')
          </dd>
          @else
              {!! $identification->rawLink(); !!}
            </dd>
          <dt class="dt-odb">@lang('messages.identified_by')</dt>
          <dd>
            
              {!! $identification->identified_by !!} - ({{ $identification->formatDate }})            
          </dd>
          @if ($identification->biocollection_id)
            <dt class="dt-odb">@lang('messages.identification_based_on')</dt>
            <dd>
              @lang('messages.voucher') {{ $identification->biocollection_reference }} &#64;
              {!! $identification->biocollection->rawLink() !!}
            </dd>
          @endif
          @if ($identification->notes)
            {!! $identification->formated_notes !!}
          @endif
        @endif

        <hr>

        <dt class="dt-odb">
            @lang('messages.individual_tag')
          </dt>
          <dd>
          <i class="fa fa-hashtag" aria-hidden="true"></i>
          &nbsp;
            {{ $individual->tag }}
        </dd>

        <dt class="dt-odb">@lang('messages.collectors')</dt>
        <dd>
        {{ $individual->recordedBy }}
      </dd>

        <dt class="dt-odb">@lang('messages.date'):</dt>
        <dd>
          {{$individual->formatDate}}
        </dd>

        <hr>
        <!--- LOCATION -->
        <?php // TODO: NEED TO ADD MULTIPLE LOCATIONS VIEW HERE ?>
          <dt class="dt-odb">@lang('messages.location'): </dt>
          <dd>
          @if($individual->locations->count()>1)
            <small class='text-danger'>
            @lang('messages.individual_has_locations',['count'=>$individual->locations->count()])
            </small>
            <br>
          @endif
          @if($individual->locations->count())
            {!! $individual->locations->last()->rawLink() !!}
            <br>
            {!! $individual->LocationDisplay(); !!}
            <br>
            <a href="{{ url('individuals/'. $individual->id. '/mapme')  }}" class="btn btn-primary btn-sm mx-2">
              <i class="fas fa-map-marked-alt fa-1x"></i>&nbsp;@lang('messages.map')
            </a>
            @if ($individual->locations->count()>1)
            <button id='locationdatatable-btn' class="btn btn-outline-secondary btn-sm mx-2">@lang('messages.all')</button>
            @endif
          @else
            @lang('messages.unknown_location')
          @endif
        </dd>
        <!--
        <div id='hintp' class='panel-collapse collapse'>
            @lang('messages.location_precision_hint')
        </div>
      -->
        @if($individual->locations->count()>1)
         <div id='locationdatatable' >
          @if (Auth::user())
            {!! View::make('common.exportdata')->with([
                  'object' => isset($individual) ? $individual : null,
                  'object_second' => null,
                  'export_what' => 'individual-location'
            ]) !!}
            <br>
          @endif
          {!! $dataTable->table([],true) !!}
          <br>
        </div>
        @endif
      <hr>

      @if($individual->dataset)
      <dt class="dt-odb">@lang('messages.dataset')</dt>
      <dd>
        {!! $individual->dataset->rawLink() !!}
      </dd>
      @endif

      @if ($individual->notes)
        {!! $individual->formated_notes !!}
      @endif

      <hr>

      @can ('delete', $individual)
      <div class="row g-3 p-2" id='odb-delete-confirmation-form' style="display: none;">
        <p class="col-auto alert alert-danger">
          @lang('messages.action_not_undone')
        </p>
        <form class='col-auto' action="{{ url('individuals/'. $individual->id)  }}" method="POST" >
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
          <button type="submit" class="btn btn-danger mt-3" title="@lang('messages.remove')">
            <i class="far fa-trash-alt"></i>
            @lang('messages.remove')
          </button>
        </form>
      </div>
      @endcan


      <!-- BUTTONS -->
      <nav class="navbar navbar-light navbar-expand-lg">
        <div class="container-fluid">
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarbuttons" aria-controls="navbarbuttons" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarbuttons">
            <br>
            <ul class="navbar-nav mb-lg-0 g-3">

              @can ('update', $individual)
                    <li class="nav-item px-2 mb-2">
        			      <a href="{{ url('individuals/'. $individual->id. '/edit')  }}" class="btn btn-success"  data-toggle="tooltip"
                    data-placement="top"
                    title=@lang('messages.edit')>
                      <i class="far fa-edit"></i>
                    </a>
                  </li>
              @endcan

              @can ('delete', $individual)
                <li class="nav-item px-2 mb-2">
                  <button class="odb-delete-confirmation-button btn btn-danger" type='button'  data-toggle="tooltip"
                  data-placement="top"
                  title=@lang('messages.remove')>
                    <i class="far fa-trash-alt"></i>
                  </button>
                </li>
              @endcan

            @if(Auth::user())
            @if(Auth::user()->can('makerequest',$individual) and  !Auth::user()->can('update',$individual))
              <li class="nav-item px-2 mb-2">
              <div class='alert alert-warning' id='biocollection_controled'>
                  <i class="fas fa-ban fa-lg"></i>
                  @lang('messages.biocollection_controled_individual')
                  <button type="submit" class="odb-unstyle" title="@lang('messages.close')" id='biocollection_controled_button'  data-toggle="tooltip"
                  data-placement="top"
                  title=@lang('messages.request')><i class="far fa-eye-slash"></i></button>
              </div>
              </li>
              @endif
            @endif

      @if ($individual->measurements()->withoutGlobalScopes()->count())
        <li class="nav-item px-2 mb-2">
          <a href="{{ url('measurements/'. $individual->id. '/individual')  }}" class="btn btn-outline-secondary" data-toggle="tooltip"
          data-placement="top"
          title=@lang('messages.measurements')>
            <i class="fa fa-btn fa-search"></i>
            {{ $individual->measurements()->withoutGlobalScopes()->count() }}
            <i class="fas fa-swatchbook"></i>
          </a>
        </li>
      @else
        @can ('create', App\Models\Measurement::class)
        <li class="nav-item px-2 mb-2">
            <a href="{{ url('individuals/'. $individual->id. '/measurements/create')  }}" class="btn btn-outline-secondary" data-toggle="tooltip"
            data-placement="top"
            title="@lang('messages.create_measurements')">
              <i class="fas fa-swatchbook"></i>
            </a>
        </li>
        @endcan
      @endif

      @if ($individual->vouchers()->count())
          <li class="nav-item px-2 mb-2">
          <a href="{{ url('individuals/'. $individual->id. '/vouchers')  }}" class="btn btn-outline-secondary">
            <i class="fa fa-btn fa-search"></i>
            {{ $individual->vouchers()->count() }}
            @lang('messages.vouchers')
          </a>
        </li>
      @else
        @can ('create', App\Models\Voucher::class)
            <li class="nav-item px-2 mb-2">
            <a href="{{url ('individuals/' . $individual->id . '/vouchers/create')}}" class="btn btn-outline-secondary" data-toggle="tooltip"
            data-placement="top"
            title="@lang('messages.create_voucher')">
              <i class="fa fa-btn fa-plus"></i>
              @lang('messages.voucher')
            </a>
          </li>
        @endcan
      @endif

      <!-- this will show only if no media as media are shown below -->
      @can ('create', App\Models\Media::class)
          <li class="nav-item px-2 mb-2">
            <a href="{{ url('individuals/'. $individual->id. '/media-create')  }}"
              class="btn btn-outline-secondary"
              data-toggle="tooltip"
              data-placement="top"
              title="@lang('messages.create_media')"
            >
            <i class="fas fa-photo-video"></i>
            <i class="fas fa-headphones-alt"></i>
            <!-- @lang('messages.create_media') -->
          </a>
        </li>
      @endcan
      @if ($individual->media->count())
        <li class="nav-item px-2 mb-2">
            <a href="{{ Route('media.individual.list',[
              'individual_id' => $individual->id])}}" class="btn btn-outline-primary">
            <i class="fa fa-btn fa-plus"></i>
            <i class="fas fa-photo-video"></i>
            <i class="fas fa-headphones-alt"></i>
            @lang('messages.media_see_all')
            </a>
        </li>
      @endif

    </u>
  </div>
</div>
</nav>

  </div>
  </div>
  </div>

</div>
@endsection
@once
    @push('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}

<script type="module">
$(document).ready(function() {

// Handle form submission event
$("#biocollection_controled_button").on('click',function(){
  $("#biocollection_controled").hide();
});

$("#locationdatatable").hide()
//this fixes a misalignment of datatable body and header
//when scrollX is true and on large devices


  $("#locationdatatable-btn").on('click',function(e) {
    var visible = $("#locationdatatable").is(":visible");
    if (!visible) {
      $("#locationdatatable").show();
      $('#dataTableBuilder').css('margin-left','0px');
      var table =  $('#dataTableBuilder').DataTable();
      table.columns.adjust().draw();
    } else {
      $("#locationdatatable").hide();
    }
  });
});

</script>

    @vite('resources/assets/js/custom.js')                
    @endpush
@endonce

