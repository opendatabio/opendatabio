@extends('layouts.app')

@section('content')
<div id='contents' class="container" style='min-width: 95vw;'>
  <br>
      <!--   <div class="col-sm-offset-2 col-sm-8"> -->
      <input type="hidden" name="odbrequest-url" value="{{ route('identificationblock') }}">
      <input type="hidden" name="taxonsautocomplete" value="{{ url('taxons/autocomplete') }}">
      <input type="hidden" name="personsautocomplete" value="{{ url('persons/autocomplete') }}">
      <input type="hidden" name="noresults" value="@lang('messages.noresults')">
<!--- Batch Identification of individuals hidden only registered users will see and project collaborators will be able to use-->
<?php // TODO: Perhaps make only collaborators of any project having individuals to see it. ?>

<div class="card" id='batch_identification_panel' style="display: none;" >
  <div class="card-header">
    @lang('messages.individuals_batch_identify')
    &nbsp;&nbsp;
    <a data-bs-toggle="collapse" href="#hint_batch_identify" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    &nbsp;&nbsp;
    <a href="#" class="odb-unstyle" title="@lang('messages.close')" id='hide_batch_identify_panel'>
      <i class="far fa-eye-slash"></i>
    </a>
    <div id="hint_batch_identify" class="odb-hint collapse">
      @lang('messages.individuals_batch_identify_hint')
  </div>
  </div>

  <div class="card-body">
  <form class='row g-2' action="{{url('individuals/batchidentify')}}" method="POST" id='batch_identification_form'>
    <!-- csrf protection -->
    {{ csrf_field() }}
  <input type='hidden' name='individualids_list' id='batch_list' value="">
  <div id='batch_identification_block' ></div>
  </form>

<button class="btn btn-success" name="submit" id='submit_batch_identifications'>
<i class="fa fa-btn fa-plus"></i>
@lang('messages.add')
</button>
</div>
<br>
</div>
<!-- End of identification form -->

<!--- USER REQUESTS -->
<div class="card" id='odbrequest_pannel' style="display: none;" >
  <div class="card-header">
    @lang('messages.request_individuals')
    &nbsp;&nbsp;
    <a data-bs-toggle="collapse" href="#request_individuals_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    &nbsp;&nbsp;
    <a href="#" class="odb-unstyle" title="@lang('messages.close')" id='hide_request_individuals_hint'>
      <i class="far fa-eye-slash"></i>
    </a>
    <div id="request_individuals_hint" class="odb-hint collapse">
    @lang('messages.request_individuals_hint')
  </div>
</div>

  <div class="card-body">

<form action="{{url('odbrequests/individuals')}}" method="POST" class="row g-2" id='odbrequest_form'>
  <!-- csrf protection -->
  {{ csrf_field() }}
<input type='hidden' name='individualids_list' id='individualids_list' value="">
<div class="mb-3">
<label class="form-label mandatory">
  @lang('messages.user')
</label>
  <input type="hidden" name="user_id" value="{{ !Auth::user() ? null : Auth::user()->id }}" >
  <input value="{{ !Auth::user() ? null : Auth::user()->email }}" class='form-control' readonly>
</div>

<div class="mb-3">
<label for="request_biocollection_id" class="form-label mandatory ">
  @lang('messages.biocollection')
</label>
  <select name="request_biocollection_id" class="form-select" id='request_biocollection_id'>
    @foreach ($biocollections as $biocollection)
      @if($biocollection->users->count())
      <option value="{{ $biocollection->id }}" "{{  (old('request_biocollection_id')==$biocollection->id) ? 'selected' : '' }}">
        {{ $biocollection->acronym."  [".substr($biocollection->name,0,30)."..]"}}
      </option>
      @endif
    @endforeach
  </select>
</div>
<div class="mb-3">
  <label class="form-label mandatory">
    @lang('messages.request_type')
  </label>
  <div class="col-sm-6">
    <input type="radio" name="request_type" value="identification">&nbsp;&nbsp;@lang('messages.identification')
    <input type="radio" name="request_type" value="voucher registration">&nbsp;&nbsp;@lang('messages.vouchers_registration')
    <input type="radio" name="request_type" value="other">&nbsp;&nbsp;@lang('messages.other')
  </div>
</div>

<div class="mb-3">
  <label class="form-label mandatory">
    @lang('messages.request_message')
  </label>
    <textarea name="message" class="form-control" id='requestmessage' rows=5 required></textarea>
</div>
<div id='request_identification' ></div>
</form>
<div class="mb-3">
<span id='submitting' ><i class="fas fa-sync fa-spin"></i></span>
<input id='odb_request_submit' class="btn btn-success" name="submit" type='submit' value='@lang('messages.request_send')' >
</div>
</div>

</div>
<!-- USER REQUESTS -->




<!-- Registered individuals -->
          <div class="card">
              <div class="card-header">
                @if (isset($object)) <!-- we're inside a Location, Project or Taxon view -->
                    @lang('messages.individuals_list_for')<strong> {{ class_basename($object) }}</strong>
                    {!! $object->rawLink() !!}
                    @if (isset($object_second))
                      &nbsp;>>&nbsp;<strong>{{class_basename($object_second )}}</strong> {!! $object_second->rawLink(true) !!}
                    @endif
                    &nbsp;&nbsp;
                    <a href="#" id="about_list" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
                    <div id='about_list_text' class='odb-hint'></div>
                @else
                  @lang('messages.registered_individuals')
                  &nbsp;&nbsp;
                  <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
                  <div id="help" class="card-body odb-hint collapse">
                    @lang('messages.individuals_hint')
                    <code>@lang('messages.download_login')</code>

                  </div>
                @endif
              </div>
              @php
              $export_params =[];
                  if (isset($filterparams)) {
                    $export_params=[
                    'taxon_root' => isset($filterparams['filter_taxon_id']) ? implode(',',$filterparams['filter_taxon_id']) : null,
                    'location_root' => isset($filterparams['filter_location_id']) ? implode(',',$filterparams['filter_location_id']) : null,
                    'dataset' => isset($filterparams['filter_datasets_ids']) ? implode(',',$filterparams['filter_datasets_ids']) : null,
                    'project' => isset($filterparams['filter_projects_ids']) ? implode(',',$filterparams['filter_projects_ids']) : null,
                    'date_min' => isset($filterparams['filter_min_date']) ? $filterparams['filter_min_date'] : null,
                    'date_max' => isset($filterparams['filter_max_date']) ? $filterparams['filter_max_date'] : null,                                        
                    'tag' => isset($filterparams['filter_number']) ? $filterparams['filter_number'] : null,                                        
                    ];
                    $export_params = array_filter($export_params);
                  }
                  if (isset($job_id)) {
                    $export_params['job_id'] = $job_id;
                  }                  
              @endphp
              @if(count($export_params)>0)
              <div class="mt-3 mx-3">
                <button class="btn btn-sm btn-outline-primary odb-filter-button">
                   {{  __('messages.filter_activated',['n' => count($export_params)]) }}
                </button>  
              </div>
              @endif
              {!! View::make('common.filter')->with([
                      'filter_type' => 'individuals',
                      'url' => 'individuals/filter',                      
                      'filter_taxon_id' => isset($filterparams) ? $filterparams['filter_taxon_id'] : [],
                      'filter_taxon_data' => isset($filterparams) ? $filterparams['filter_taxon_data'] : [],
                      'filter_location_id' => isset($filterparams) ? $filterparams['filter_location_id'] : [],
                      'filter_location_data' => isset($filterparams) ? $filterparams['filter_location_data'] : [],
                      'filter_persons_ids' => isset($filterparams) ? $filterparams['filter_persons_ids'] : [],
                      'filter_persons_data' => isset($filterparams) ? $filterparams['filter_persons_data'] : [],
                      'filter_min_date' => isset($filterparams) ? $filterparams['filter_min_date'] : null,
                      'filter_max_date' => isset($filterparams) ? $filterparams['filter_max_date'] : null,
                      'filter_datasets_ids' => isset($filterparams) ?  $filterparams['filter_datasets_ids'] : [],
                      'filter_datasets_data' => isset($filterparams) ?  $filterparams['filter_datasets_data'] : [],
                      'filter_projects_ids' => isset($filterparams) ?  $filterparams['filter_projects_ids'] : [],
                      'filter_projects_data' => isset($filterparams) ?  $filterparams['filter_projects_data'] : [],
                      'filter_projects_ids' => isset($filterparams) ?  $filterparams['filter_projects_ids'] : [],
                      'filter_number' => isset($filterparams) ?  $filterparams['filter_number'] : null,
                ])  !!}

              @if (Auth::user())
                  {!! View::make('common.exportdata')->with([
                        'object' => isset($object) ? $object : null,
                        'object_second' => isset($object_second) ? $object_second : null,
                        'export_what' => 'Individual',
                        'filters' => isset($export_params) ? $export_params : null,
                  ]) !!}
                  <!--- delete form -->
                  {!! View::make('common.batchdelete')->with([
                        'url' => url('individuals/batch_delete'),
                  ]) !!}
              @endif
              <form action="{{url('individuals/map')}}" method="POST" class="form-horizontal" id='odb-map-occurrences'>
                <!-- csrf protection -->
                {{ csrf_field() }}
                <input type="hidden" name="idstomap" value="" id='odb-map-selected-rows'>
              </form>
            <div class="card-body mt-3">
              {!! $dataTable->table([],true) !!}
            </div>
          </div>
        <!-- </div> -->
    </div>
@endsection


@once
    @push('scripts')
        {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
        {!! Multiselect::scripts('filter_taxon_id', url('taxons/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
        {!! Multiselect::scripts('filter_location_id', url('locations/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
        {!! Multiselect::scripts('filter_persons_ids', url('persons/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
        {!! Multiselect::scripts('filter_datasets_ids', url('datasets/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
        {!! Multiselect::scripts('filter_projects_ids', url('projects/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
        @vite('resources/assets/js/custom.js')                
        @vite('resources/assets/js/individuals.js')                
    @endpush
@endonce

