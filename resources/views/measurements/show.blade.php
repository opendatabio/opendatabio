@extends('layouts.app')

@section('content')
    <div class="container">
      <br>
      <div class="col-lg-8 offset-lg-2 col-md-6 offset-md-3 col-sm-6 offset-sm-3">
      <div class="card ">
          <div class="card-header">
                    @lang('messages.measurement')
                    <span class="history" style="float:right">
                    <a href="{{url('measurements/'.$measurement->id.'/activity')}}">
                    @lang ('messages.see_history')
                    </a>
                    </span>
          </div>
		<div class="card-body">
      <dl class="">


<dt class='dt-odb'>
@lang('messages.object')
</dt>
<dd class='mb-3'>
{!! $measurement->measured->rawLink(true) !!}
</dd>
<dt class='dt-odb'>
@lang('messages.trait')
</dt>
<dd class='mb-3'>
{!! $measurement->odbtrait->rawLink() !!}
&nbsp;
[{{ $measurement->odbtrait->export_name }}]
&nbsp;
<a data-bs-toggle="collapse" href="#trait-details" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
<div class='odb-hint collapse' id='trait-details'>
  {{$measurement->odbtrait->description}}
</div>
</dd>
<dt class='dt-odb'>
@lang('messages.value')
</dt>
<dd class='mb-3'>
{{$measurement->valueDisplay}} {{ $measurement->measurement_unit }}
@if ($measurement->type == \App\Models\ODBTrait::COLOR)
&nbsp;<span class="measurement-thumb" style="background-color: {{$measurement->valueActual}}">
@endif
@if ($measurement->type == \App\Models\ODBTrait::GENEBANK)
&nbsp;<a href="https://www.ncbi.nlm.nih.gov/nuccore/{{$measurement->value_actual}}" target='_blank'>@lang('messages.genebank_seencbi')</a>
@endif
</dd>


<dt class='dt-odb'>
@lang('messages.measurement_date')
</dt>
<dd class='mb-3'>
{{$measurement->formatDate}}
</dd>

@if ($measurement->collectors)
<dt class='dt-odb'>
@lang('messages.measurement_measurer')
</dt>
<dd class='mb-3'>
{!! $measurement->recordedBy !!}
</dd>
@endif

@if ($measurement->parentMeasurement()->count())
<dt class='dt-odb'>
@lang('messages.measurement_parent')
</dt>
<dd class='mb-3'>
{!! $measurement->parentMeasurement->rawLinkWithSelectorName() !!}
</dd>
@endif

@if ($measurement->dependentMeasurements()->count())
<dt class='dt-odb'>
@lang('messages.measurement_related')
</dt>
<dd class='mb-3'>
  @foreach($measurement->dependentMeasurements as $dp)
    {!! $dp->rawLinkWithSelectorName() !!} <br>
  @endforeach
</dd>
@endif


<dt class='dt-odb'>
@lang('messages.dataset')
</dt>
<dd class='mb-3'>
{!! $measurement->dataset->rawLink() !!}
</dd>

@if ($measurement->notes)
  {!! $measurement->formated_notes !!}
@endif


@if ($measurement->bibreference)
<dt class='dt-odb'>
@lang('messages.reference')
</dt>
<dd class='mb-3'>
{!! $measurement->bibreference->rawLink() !!}
</dd>
@endif
</dl>





@can ('delete', $measurement)
<div class="row g-3 p-2" id='odb-delete-confirmation-form' style="display: none;">
  <p class="col-auto alert alert-danger">
    @lang('messages.action_not_undone')
  </p>
  <form class='col-auto' action="{{ url('measurements/'. $measurement->id)  }}" method="POST" >
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
    <button type="submit" class="btn btn-danger mt-3" title="@lang('messages.remove')">
      <i class="far fa-trash-alt"></i>
      @lang('messages.remove')
    </button>
  </form>
</div>
@endcan


<div class="row g-3">
  @can ('update', $measurement)
  <div class="col-auto">
  <a href="{{ route('measurement.form',[
            'measurement' => $measurement->id,
            ] )}}" class="btn btn-success" name="submit" value="submit">
    <i class="fa fa-btn fa-plus"></i>
    @lang('messages.edit')
  </a>
  </div>
  @endcan
  @can ('delete', $measurement)
  <div class="col-auto">
    <button class="odb-delete-confirmation-button btn btn-danger" type='button' >
      @lang('messages.remove')
    </button>
  </div>
  @endcan
</div>
</div>

@if (isset($chartjs))
  <br>
  <div class="card-body" >
    @if (isset($chartjs))
      <div style = "width:100%; height:400px; overflow:auto;">
        {!! $chartjs->render() !!}
      </div>
    @endif
  </div>
@endif




</div>
</div>


</div>

@endsection
