@extends('layouts.app')

@section('content')
<div id='contents' class="container">
        <!-- <div class="col-sm-offset-2 col-sm-8"> -->


<br>
<div class="card">
  <div class="card-header">
          @if (isset($object))
            @lang('messages.measurements_for')
            <strong>
              {{ class_basename($object )}}
            </strong>
            {!! $object->rawLink(true) !!}
          @else
            <strong>
            @lang('messages.measurements')
            </strong>
          @endif
          @if (isset($object_second))
          &nbsp;>>&nbsp;
          <strong>{{class_basename($object_second )}}</strong>
          {!! $object_second->rawLink(true) !!}
          @endif
          @if (isset($measured_type))
          &nbsp;>>&nbsp;
          <strong>{{$measured_type}}</strong>
          @endif
          &nbsp;&nbsp;
          <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          <div id="help" class="odb-hint collapse">
            @lang('messages.measurements_hint')
          </div>
    </div>

    @if (Auth::user())
    {!! View::make('common.exportdata')->with([
          'object' => isset($object) ? $object : null,
          'object_second' => isset($object_second) ? $object_second : null,
          'measured_type' => isset($measured_type) ? $measured_type : null,
          'export_what' => 'Measurement',
          'filters' => isset($job_id) ? ['job_id'=> $job_id] : null,
    ]) !!}
    <!--- delete form -->
    {!! View::make('common.batchdelete')->with([
          'url' => url('measurements/batch_delete'),
    ]) !!}
    <br>
    @endif
    <div class="card-body">
    {!! $dataTable->table([],true) !!}
    </div>
</div>


</div>

@endsection
@once
  @push ('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
    @vite('resources/assets/js/custom.js')
  @endpush
@endonce
