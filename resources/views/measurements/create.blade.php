@extends('layouts.app')

@section('content')
    <div class="container">
      <br>
      <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
          <div class="card-header">
                    @if(isset($measurement))
                      @lang('messages.edit_measurement') @lang('messages.for')
                    @else
		                  @lang('messages.new_measurement') @lang('messages.for')
                    @endif
                    :<br>
                    <div class="mt-2">
                    {{ str_replace("App\Models\\","",get_class($object))." " }}
                    {!! $object->rawLink() !!}
                    @if ($object->identification)
                      (<em>{{$object->identification->taxon->fullname}}</em>)
                    @endif
                  </div>

          </div>

                <div class="card-body">


@if (isset($measurement))
	<form action="{{ url('measurements/' . $measurement->id)}}" method="POST" class="row g-2">
  {{ method_field('PUT') }}

@else
  <form action="{{ url('measurements')}}" method="POST" class="row g-2">
@endif
  {{ csrf_field() }}
 <input type="hidden"  name="measured_id" value="{{$object->id}}">
 <input type="hidden"  name="measured_type" value="{{get_class($object)}}">


@if(get_class($object)=="App\Models\Taxon")
<div id="locationfield" class="mb-3">
  <label for="modal_location_id" class="form-label ">
    @lang('messages.location')
  </label>
  <a data-bs-toggle="collapse" href="#hint2" class="odb-unstyle">
    <i class="far fa-question-circle"></i></a>
    <div id="hint2" class="odb-hint collapse">
       @lang('messages.measurement_taxon_location_hint')
     </div>
     <input type="text" name="location_autocomplete" id="location_autocomplete" class="form-control autocomplete" value="{{ old('location_autocomplete', isset($location_name) ? $location_name : null) }}">
     <input type="hidden" name="location_id" id="location_id" value="{{ old('location_id',isset($location_id) ? $location_id : null) }}">
    
</div>
@endif


<div class="mb-3">
    <label for="trait_id" class="form-label mandatory">
      @lang('messages.trait')
</label>
    <div id="spinner"></div>
    <div class="">
    <input type="text" name="trait_autocomplete" id="trait_autocomplete" class="form-control autocomplete"
    value="{{ old('trait_autocomplete', (isset($measurement) and $measurement->odbtrait) ? $measurement->odbtrait->name : null) }}" {{ isset($measurement) ? 'disabled' : null }} >
    <input type="hidden" name="trait_id" id="trait_id"
    value="{{ old('trait_id', isset($measurement) ? $measurement->trait_id : null) }}">
    </div>
</div>

<div id="append_value" class="mb-3">
<?php if (isset($measurement)) {
echo View::make('traits.elements.' . $measurement->type,
[
    'odbtrait' => $measurement->odbtrait,
    'measurement' => $measurement,
]);
    } elseif (!empty(old())) {
        $odbtrait = \App\Models\ODBTrait::find(old('trait_id'));
        echo View::make('traits.elements.' . $odbtrait->type,
            [
                'odbtrait' => $odbtrait,
                'measurement' => null,
            ]);

    }
?>
</div>




<div class="mb-3">
    <label for="date" class="form-label mandatory">
@lang('messages.measurement_date')
</label>
        <a data-bs-toggle="collapse" href="#hintdate" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div class="">
{!! View::make('common.incompletedate')->with([
    'object' => isset($measurement) ? $measurement : null,
    'field_name' => 'date'
]) !!}
            </div>
  <div class="col-sm-12">
    <div id="hintdate" class="odb-hint collapse">
	@lang('messages.measurement_date_hint')
    </div>
  </div>
</div>

<div class="mb-3">
    <label for="persons" class="form-label mandatory">
      @lang('messages.measurement_measurer')
    </label>
    <a data-bs-toggle="collapse" href="#hintp" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div>
        {!! Multiselect::autocomplete('persons',
          $persons->pluck('abbreviation', 'id'),
          isset($measurement) ? $measurement->collectors->pluck('id') :
          (empty(Auth::user()->person_id) ? '' : [Auth::user()->person_id] ),
          ['class' => 'multiselect form-control'])
        !!}
    </div>
    <div id="hintp" class="odb-hint collapse">
	@lang('messages.measurement_person_hint')
    </div>
</div>

<div class="mb-3">
  <label for="dataset_id" class="form-label mandatory">
    @lang('messages.measurement_dataset')
  </label>
  <div class="">
    @if (count($datasets))
    	<?php $selected = old('dataset_id', isset($measurement) ? $measurement->dataset_id : (Auth::user()->defaultDataset ? Auth::user()->defaultDataset->id : null)); ?>
    	<select name="dataset_id" id="dataset_id" class="form-select" >
      	@foreach ( $datasets as $dataset )
      		<option value="{{$dataset->id}}" {{ $dataset->id == $selected ? 'selected' : '' }}>
                  {{ $dataset->name }}
      		</option>
      	@endforeach
      </select>
    @else
      <div class="alert alert-danger">
        @lang ('messages.no_valid_dataset')
      </div>
    @endif
  </div>
</div>



@if(count($other_measurements))
<div class="mb-3">
  <label for="parent_id" class="form-label">
    @lang('messages.measurement_parent')
  </label>
  <a data-bs-toggle="collapse" href="#measurement_parent_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div class="">
    <select name="parent_id" id="parent_id" class='form-control'>
      <option value="" >
          @lang('messages.none') 
      </option>
      @foreach($other_measurements as $other)
        <option value="{{$other['id']}}" {{ isset($measurement) ? ($other['id'] == $measurement->parent_id ? 'selected' : '') : '' }}>
          {{$other['label'] }}
        </option>
      @endforeach
    </select>  
  </div>
    <div id="measurement_parent_hint" class="odb-hint collapse">
      @lang('messages.measurement_parent_hint')
    </div>
</div>
@endif


<div class="mb-3">
  <label for="bibreference_id" class="form-label">
    @lang('messages.measurement_bibreference')
  </label>
  <a data-bs-toggle="collapse" href="#hintr" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div class="">
    <input type="text" name="bibreference_autocomplete" id="bibreference_autocomplete" class="form-control autocomplete"
    value="{{ old('bibreference_autocomplete', (isset($measurement) and $measurement->bibreference) ? $measurement->bibreference->bibkey : null) }}">
    <input type="hidden" name="bibreference_id" id="bibreference_id"
    value="{{ old('bibreference_id', isset($measurement) ? $measurement->bibreference_id : null) }}">
</div>
    <div id="hintr" class="odb-hint collapse">
	@lang('messages.measurement_bibreference_hint')
    </div>
</div>

<div class="mb-3">
  <label for="notes" class="form-label">
  @lang('messages.notes')
  </label>
  <div class="">
    <textarea name="notes" id="notes" class="form-control">{{ old('notes', isset($measurement) ? $measurement->notes : null) }}</textarea>
  </div>
</div>


		        <div class="mb-3">
                <button type="submit" class="btn btn-success" name="submit" value="submit"
@if (!count($datasets))
disabled
@endif
>
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.add')
				</button>
				<a href="{{url()->previous()}}" class="btn btn-warning">
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.back')
				</a>
			</div>
		    </form>
        </div>
    </div>
@endsection


@once
  @push ('scripts')
    @vite('resources/assets/js/custom.js')
    {!! Multiselect::scripts('persons', url('persons/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}

    <script type="module">

    $(document).ready(function() {
        $("#bibreference_autocomplete").odbAutocomplete("{{url('references/autocomplete')}}","#bibreference_id", "@lang('messages.noresults')");
        @if (isset($measurement) and $measurement->odbtrait->type==7)
          @if ($measurement->odbtrait->link_type == "App\Models\Taxon")
                $("#link_autocomplete").odbAutocomplete("{{url('taxons/autocomplete')}}","#link_id", "@lang('messages.noresults')");
          @endif
          @if ($measurement->odbtrait->link_type == "App\Models\Person")
                $("#link_autocomplete").odbAutocomplete("{{url('persons/autocomplete')}}","#link_id", "@lang('messages.noresults')");
          @endif
          @if ($measurement->odbtrait->link_type == "App\Models\Individuals")
                $("#link_autocomplete").odbAutocomplete("{{url('individuals/autocomplete')}}","#link_id", "@lang('messages.noresults')");
          @endif
        @endif
        
        $("#trait_autocomplete").devbridgeAutocomplete({
            serviceUrl: "{{url('traits/autocomplete')}}",
            /* adds the object type to request; doubles the namespace back slashes */
            params: {'type': '{{ str_replace('\\', '\\\\', get_class($object)) }}' },
            onSelect: function (suggestion) {
                $("#trait_id").val(suggestion.data);
                $( "#spinner" ).css('display', 'inline-block');
                $.ajax({
                type: "GET",
                    url: "{{url('traits/getformelement')}}",
                    dataType: 'json',
                    data: {'id': suggestion.data, 'measurement': null},
                    success: function (data) {
                        $("#spinner").hide();
                        $("#ajax-error").hide();
                        $("#append_value").html(data.html);
                    },
                    error: function(e){
                        $("#spinner").hide();
                        $("#ajax-error").show();
                        $("#ajax-error").text('Error sending AJAX request');
                    }
                })
            },
            onInvalidateSelection: function() {
                $("#trait_id").val(null);
            },
            minChars: 3,
            onSearchStart: function() {
                $(".minispinner").remove();
                $(this).after("<div class='spinner minispinner'></div>");
            },
            onSearchComplete: function() {
                $(".minispinner").remove();
            },
            showNoSuggestionNotice: true,
            noSuggestionNotice: "@lang('messages.noresults')"
        });

        $("#location_autocomplete").odbAutocomplete(
            "{{url('locations/autocomplete')}}", "#location_id", "@lang('messages.noresults')");

        $(document).on('click','#check-gb',function(e) {
          e.preventDefault(); // does not allow the form to submit
          var accession = $("input[name=value]").val();
          if(accession) {
            $( "#gb-spinner" ).css('display', 'inline-block');
            $.ajax({
              type: "GET",
                url: "{{url('measurements/check-gb')}}",
                dataType: 'json',
                data: {'accession': accession},
                success: function (data) {
                    $("#gb-spinner").hide();
                    $("#gb-status").html(data.gbdata);
                    $("#gb-status").show();
                },
                error: function(data){
                    $("#gb-status").show();
                    $("#gb-spinner").hide();
                    $("#gb-status").html(data.error);
                }
            });
          } else {
            $("#gb-status").show();
            $("#gb-status").html("@lang('messages.genebank_missing_accession')");
          }
        });

    });
    // NOTE! duplicated from view 6
    @if (isset($measurement) and $measurement->type==6)
      $("#value").spectrum({
        flat:true,
        showInput:true,
        showPalette: true,
        showPaletteOnly: true,
        togglePaletteOnly: true,
        togglePaletteMoreText: "@lang('spectrum.more')",
        togglePaletteLessText: "@lang('spectrum.less')",
        preferredFormat: "hex",
            showButtons: false,
        palette: {!! json_encode(config('app.spectrum')) !!}
    });
    @endif
    </script>
  @endpush
@endonce
