@extends('layouts.app')

@section('content')
    <div class="container">
      <br>
      <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
          <div class="card-header">
            @if(!isset($tag))
              @lang('messages.new_tag')
            @else
              @lang('messages.editing') @lang('messages.tag') <strong>id# {{ $tag->id }}</strong>
            @endif
            &nbsp;
            <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
            <div id="help" class="odb-hint collapse">
              @lang('messages.hint_tag_create')
            </div>
          </div>

          <div class="card-body">

@if (isset($tag))
		    <form class="row g-2" action="{{ url('tags/' . $tag->id)}}" method="POST" class="form-horizontal">
{{ method_field('PUT') }}

@else
		    <form class="row g-2"  action="{{ url('tags')}}" method="POST" class="form-horizontal">
@endif
		     {{ csrf_field() }}

@foreach ($languages as $language)
    <div class='row'>
      <h5 ><em>{{$language->name}}</em></h5>
      <div class='col-sm-6 mb-3'>
        <label for="tag_name{{$language->id}}" class='form-label mandatory'>@lang("messages.tag_name")</label>
        <input id="tag_name{{$language->id}}"  class='form-control' name="name[{{$language->id}}]" value="{{ old('name.' . $language->id, isset($tag) ? $tag->translate(\App\Models\UserTranslation::NAME, $language->id) : null) }}" >
      </div>
      <div class='col-sm-6 mb-3'>
        <label for="description{{$language->id}}" class='form-label'>@lang("messages.description")</label>
        <textarea id="description{{$language->id}}"  class='form-control' name="description[{{$language->id}}]">{{ old('description.' . $language->id, isset($tag) ? $tag->translate(\App\Models\UserTranslation::DESCRIPTION, $language->id) : null) }}</textarea>
      </div>
    </div>
    <hr>
@endforeach

<div class="mb-3 g-3">
   <button type="submit" class="btn btn-success" name="submit" value="submit">
	    <i class="fa fa-btn fa-plus"></i>
      @lang('messages.add')
  </button>
  <a href="{{url()->previous()}}" class="btn btn-warning">
	    <i class="fa fa-btn fa-plus"></i>
      @lang('messages.back')
	</a>
</div>

</form>

    </div>
</div>
@endsection
