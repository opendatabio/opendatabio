@extends('layouts.app')

@section('content')
<div class="container">
  <br>
  <div class="card col-lg-8 offset-lg-2 col-md-6 offset-md-3">
        <div class="card-header">
            Tag <strong>{{ $tag->name }}</strong>
        </div>
        <div class="card-body">
        <dl>
        <dt class="dt-odb">
          @lang('messages.name')
        </dt>
        <dd>
         {{ $tag->name }}
        </dd>


        <dt class="dt-odb">
          @lang('messages.description')
        </dt>
        <dd>
          {{ $tag->description }}
        </dd>
      </dl>
        <!-- BUTTONS -->
      <nav class="navbar navbar-light navbar-expand-lg" id="buttons-navbar">
        <div class="container-fluid">
          <div class="navbar-header">
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarbuttons" aria-controls="navbarbuttons" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
          <div class="navbar-collapse collapse flex-column ml-lg-0 ml-3" id="navbarbuttons">
            <br>
              <ul class="nav navbar-nav flex-row">
                @can ('update', $tag)
                <li class="nav-item px-2 mb-2">
                  <a href="{{ url('tags/'.$tag->id.'/edit') }}" class="btn btn-success">
                	  <i class="fa fa-btn fa-plus"></i>
                    @lang('messages.edit')
                	</a>
                </li>
                @endcan
                @can ('delete', $tag)
                <li class="nav-item px-2 mb-2">
                  <form class='col-auto' action="{{ url('tags/'. $tag->id)  }}" method="POST" >
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-danger" title="@lang('messages.remove')">
                      <i class="far fa-trash-alt"></i>
                        @lang('messages.remove')
                    </button>
                  </form>
                </li>
                @endcan
                <li class="nav-item px-2 mb-2">
              <a href="{{ url('datasets/'. $tag->id. '/tags')  }}" class="btn btn-outline-secondary">
                <i class="fa fa-btn fa-search"></i>
                {{ $tag->datasets()->count() }}
                @lang('messages.datasets')
              </a>
            </li>
            <li class="nav-item px-2 mb-2">
              <a href="{{ url('projects/'. $tag->id. '/tags')  }}" class="btn btn-outline-secondary">
                <i class="fa fa-btn fa-search"></i>
                {{ $tag->projects()->count() }}
                @lang('messages.projects')
              </a>
            </li>
            @if ($tag->media->count())
            <li class="nav-item px-2 mb-2">
                <a href="{{ Route('media.tag.list',[
                  'tag_id' => $tag->id])}}" class="btn btn-outline-primary">
                <i class="fa fa-btn fa-plus"></i>
                <i class="fas fa-photo-video"></i>
                <i class="fas fa-headphones-alt"></i>
                @lang('messages.media_see_all')
                </a>
            </li>
            @endif
          </ul>
        </div>
      </div>
    </nav>

    </div>
  </div>
</div>
@endsection
