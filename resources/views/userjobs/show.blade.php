@extends('layouts.app')

@section('content')
<div class="container">
      <br>
      <div class="card col-lg-8 offset-lg-2 col-md-6 offset-md-3">
            <div class="card-header">
                    @lang('messages.monitor_userjob')
            </div>

            <div class="card-body">
                  <dl class="">


<div class='dt-odb'>
@lang('messages.jobid')
</div>
<div class="mb-3">
      {{ $job->id }}
</div>

<div class='dt-odb'>
    @lang('messages.type')
</div>
<div class="mb-3">
  {{ $job->dispatcher }}
</div>

<div class='dt-odb'>
@lang('messages.actions')
</div>
<div class="row mb-3">
					@if ($job->status == 'Failed' or $job->status == 'Cancelled')
          <div class="col-auto">
            <form action="{{ url('userjobs/'.$job->id.'/retry') }}" method="POST">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-success">
              <i class="fas fa-redo-alt"></i>
              &nbsp;@lang('messages.retry')</button>
            </form>
            </div>
          @endif

          @if ($job->status == 'Submitted' or $job->status == 'Processing')
          <div class="col-auto">
              <form action="{{ url('userjobs/'.$job->id) }}" method="GET">
                <button type="submit" class="btn btn-outline-primary">
                  <i class="fa-solid fa-arrows-rotate"></i>
                  &nbsp;@lang('messages.refresh')
                </button>
              </form>
          </div>


          <div class="col-auto">
            <form action="{{ url('userjobs/'.$job->id.'/cancel') }}" method="POST">
            {{ csrf_field() }}
              <button type="submit" class="btn btn-warning">
                  <i class="fa-solid fa-ban"></i>
                  &nbsp;@lang('messages.cancel')
              </button>
            </form>
          </div>
@else
          <div class="col-auto">
            <form action="{{ url('userjobs/'.$job->id) }}" method="POST">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <button type="submit" class="btn btn-danger">
                <i class="far fa-trash-alt"></i>
                &nbsp;@lang('messages.remove')
              </button>
            </form>
          </div>
@endif
</div>

<div class='dt-odb'>
@lang('messages.status')
</div>
<div class="mb-3">
   {{ $job->status }}
</div>

<div class='dt-odb'>
@lang('messages.progress')
</div>
<div class="mb-3">
  {{ $job->percentage }}
</div>

@if($job->affected_model)
<div class='dt-odb'>
@lang('messages.summary')
</div>
<div class="mb-3">
  @lang('messages.job_records_summary',[
    'total' => $job->progress_max,
    'success' => $job->affected_ids_count,
  ]);
  <br>
  {!! $job->affected_records_link !!} 
</div>
@endif

<div class='dt-odb'>
@lang('messages.log')
</div>
<div class="mb-3">
<br>
				@if (empty($job->log))
					-null-
                    @else
                        <ul>
@foreach (json_decode($job->log, true) as $item)
@if(is_array($item))
  <li> {{ serialize($item) }} </li>
@else
  @php
    $pattern = "/error/i";
    if (preg_match($pattern, $item)) {
      $lineclass = 'alert alert-danger';
    } else {
      $lineclass = 'alert alert-success';
    }
  @endphp
  <li class="{{$lineclass}}" > {!! $item !!} </li>
@endif
@endforeach
				@endif
                </ul>
			</div>
                </div>
            </div>
        </div>
@endsection
