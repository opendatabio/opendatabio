@extends('layouts.app')

@section('content')
<div class="container">
  <br>
  <div class="col-lg-10 offset-lg-1 col-md-12 offset-md-0">
      <!-- Registered Persons -->
      <div class="card">
          <div class="card-header">
             @lang('messages.registered_userjobs')
              &nbsp;&nbsp;
              <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
              @if(count($jobs)>0)
                <div style='float:right;'>
                    <form action="{{ url('userjobs/purge') }}" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger btn-sm">
                      <i class="far fa-trash-alt"></i>&nbsp;@lang ('messages.remove_all')
                    </button>
                    </form>
               </div>
             @endif
             <div class="collapse odb-hint" id="help" >
               @lang('messages.userjobs_hint')
             </div>
            </div>

            <div class="card-body">
                        <table class="table table-striped user-table">
                            <thead>
                                <th>@lang('messages.id')</th>
                                <th>@lang('messages.status')</th>
                                <th>@lang('messages.type')</th>
                                <th>@lang('messages.created_at')</th>
                                <th>@lang('messages.updated_at')</th>
                                <th>@lang('messages.progress')</th>
                                <th colspan=2>@lang('messages.actions')</th>
                            </thead>
                            <tbody>
                                @foreach ($jobs as $job)
                                    <tr>
					<td class="table-text"><div>
					<a href="{{ url('userjobs/'.$job->id) }}">{{ $job->id }}</a>
					</div></td>
                                        <td class="table-text">{{ $job->status }}</td>
                                        <td class="table-text">{{ $job->dispatcher }}</td>                                        
                                        <td class="table-text">{{ $job->created_at }}</td>
                                        <td class="table-text">{{ $job->updated_at }}</td>
                                        <td class="table-text">{{ $job->percentage }}</td>
					<td class="table-text">
					@if ($job->status == 'Failed' or $job->status == 'Cancelled')
<form action="{{ url('userjobs/'.$job->id.'/retry') }}" method="POST">
{{ csrf_field() }}
<button type="submit" class="unstyle" title="@lang('messages.retry')">
  <i class="fas fa-redo-alt"></i>
</button>
</form>
					@endif
					@if ($job->status == 'Submitted' or $job->status == 'Processing')
<form action="{{ url('userjobs/'.$job->id.'/cancel') }}" method="POST">
{{ csrf_field() }}
<button type="submit" class="unstyle" title="@lang('messages.cancel')">
  <i class="fas fa-ban"></i>
</button>
</form>
					@endif
					</td>
					<td class="table-text">
<form action="{{ url('userjobs/'.$job->id) }}" method="POST">
{{ csrf_field() }}
{{ method_field('DELETE') }}
<button type="submit" class="unstyle" title="@lang('messages.remove')">
  <i class="far fa-trash-alt"></i>
</button>
</form>
					</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
			 {{ $jobs->links() }}
                    </div>
                </div>
        </div>
    </div>
@endsection
