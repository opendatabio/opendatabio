<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Open Data Bio</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="shortcut icon" href="{{ asset('favicon_io/favicon.ico') }}" >
    @livewireStyles
    <style media="screen">
      .odb-top-img {
          position: relative;
          padding-bottom: 5rem;
          background-image: url("custom/landing-page-background.jpg");
          background-size: cover;
          background-repeat: no-repeat;
          height: 42vh;
      }
      .odb-top-img-overlay {
          position: relative;
          z-index: 1;
      }
    </style>
    @vite(['resources/assets/sass/app.scss','resources/assets/js/app.js'])     
</head>
<body>

<!-- BUTTO TO PAGE TOP CONTROLED BY JAVASCRIPT IN CUSTOM.JS -->
<button id="odb-bntPgTop" title="Go to top">@lang('messages.page_top')</button>

<!-- navbar -->
<section class="odb-top-img">
  {!! View::make('layouts.navbar')->with([
        'landing_page' => true,
  ]) !!}
  <div class="container odb-top-img-overlay" >
    <div class="text-center" style="color: LightYellow;">
          <h1 class="display-2 mt-0 mt-md-3">@lang('customs.landing_page_welcome')</h1>
          <div class="pt-2 lead">
            @lang('customs.landing_page_short')
        </div>
  </div>
</div>
</section>

@if (config('app.env')=='local')
<section id="counts" class='alert alert-warning pb-3' >
  <div class="container-fluid">
    <div class="row col-lg-10 offset-lg-1 mx-auto">
      <div class="text-center">
          <i class="fa-solid fa-triangle-exclamation"></i>&nbsp;
          @lang('messages.development_installation')
      </div>
      <div class="odb-hint">
        @php
          $siteemail = config('mail.from.address');
          $installationname = config('app.name');
        @endphp
        @lang('messages.welcome_dev_installation',compact('siteemail','installationname'))
      </div>
    </div>
</div>
</section>
@endif
<!-- ======= Counts Section ======= -->
@if(isset($counts))
<section id="counts" class='pb-3' >
  <div class="container-fluid">
    <br>
      <nav class="navbar navbar-light navbar-expand-lg" id="buttons-navbar">
        <div class="container-fluid">
          <div class="navbar-header">
          <button class="navbar-toggler" 
          type="button" 
          data-bs-toggle="collapse" 
          data-bs-target="#navbarbuttons" 
          aria-controls="navbarbuttons" 
          aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
          <div class="navbar-collapse collapse flex-lg-column" id="navbarbuttons">
            <ul class="nav navbar-nav flex-row g-3">
                <li class="nav-item px-2 mb-2">
        @foreach($counts as $key => $value)
          @if ($value[0]>0)
          @php
            $duration = (2/(int) $value[0]);
          @endphp
          <li class="nav-item px-2 mb-2">
            <a class="btn btn-outline-odbgray" href="{{$value[1]}}">
            <h4>
              <!--
              <span data-purecounter-start="0" 
              data-purecounter-end="{{ $value[0] }}" 
              data-purecounter-duration="{{$duration}}" class="purecounter"></span>
               -->
              {{ $value[0] }}
            </h4>
            <p>{{$key}}</p>
          </a>
          </li>
         @endif
        @endforeach
        </ul>
    </div>
    </div>
  </nav>
  </div>
</section><!-- End Counts Section -->
@endif


<div class="row col-lg-10 offset-lg-1 g-5 px-5">
  <!-- DATASETS -->
  <livewire:datasets-cards>

<!-- PROJECTS -->
  <livewire:projects-cards>

</div>

<div class="row col-lg-8 offset-lg-2 g-5 px-5 mt-3">
<!-- IF THERE ARE BIOCOLLECTIONS ADMIN BY ODB -->
@if($biocollections->count())
<div class="col-lg-auto mx-auto" >
  <div class="mb-0 text-center" >
      <h4><a href="{{ url('biocollections') }}" >
        @lang('messages.biocollections')</a></h4>
        <span class='odb-hint text-center pb-2'>@lang('messages.biocollections_managed')</span>
  </div>
    <div class='row g-3 mx-3 p-3'>
      @foreach($biocollections as $biocollection)
            @php
            $logoUrl = null;
            if ($biocollection->media->count())
            {
              $logo = $biocollection->media()->first();
              $fileUrl = $logo->getUrl();
              if (file_exists($logo->getPath('thumb'))) {
                $logoUrl = $logo->getUrl('thumb');
              } else {
                $logoUrl = $fileUrl;
              }
            }
            @endphp
            <div class="col-auto">
            <div class="card p-3" style='min-height: 150px;'>
              <div class="row no-gutters">
                <div class="col-auto">
                  @if(isset($logoUrl))
                    <img src="{{$logoUrl}}" class="img-fluid" alt="..." style='object-fit: cover; max-width: 150px;'>
                  @else
                    <div class='bg-secondary text-light p-3 text-center'style="min-width: 150px; min-height: 150px;">
                      <h1 class='mt-4'>{{ $biocollection->acronym}}</h1>
                    </div>
                  @endif
                </div>
                <div class="col">
                  <div class="card-block">
                    <h5 class="card-title">{{ isset($biocollection->name) ? $biocollection->name : $biocollection->acronym }}</h5>
                    <p class="card-text">
                      @if(isset($biocollection->description))
                      {!! substr($biocollection->description,0,100) !!}
                      ...
                      @endif
                      <br>
                      <a href="{{ url('biocollections').'/'.$biocollection->id}}" 
                      class="btn btn-sm btn-outline-success">@lang('messages.details')</a>
                  </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
@endif

<div class="col-lg-auto mx-auto mb-5" >
  <div class="mb-0 text-center" >
      <h4>@lang('messages.api_service')</h4>
      <span class='odb-hint text-center pb-2'>@lang('messages.api_service_hint')</span>
  </div>
    <div class='row g-3 mx-3 p-3'>
      <div class="card p-3" style='min-height: 150px;'>
            <div class="card-block">
              <div class="row g-3">
              <div class='col-12 card-text'>
                @lang('messages.api_service_tip')

              </div>
              <div class="col-12">
                <div class="row">
                <dl class='col-auto'>
                  <dt class="dt-odb">
                    API BASE URL
                  </dt>
                 <dd >
                   {{ url('api/v0/')}}
                   <br>
                 </dd>
               </dl>
               <dl class='col-auto'>
                 <dt class="dt-odb">
                   @lang('messages.json_example')
                 </dt>
                <dd >
                  <a href="{{ url('api/v0/taxons?offset=50&limit=5') }}" target='_blank'>
                    {{ url('api/v0/taxons?offset=50&limit=5') }}
                  <a>
                </dd>
              </dl>
              </div>
            </div>
              <div class='col-auto'>
                <a class='btn btn-outline-primary' href="https://gitlab.com/opendatabio/opendatabio-r">
                <i class="fa-brands fa-r-project fa-2x"></i>                
                &nbsp;@lang('messages.rpackage_tip')
                </a>
              </div>
              @php
                if (config('app.locale')=="en") {
                  $url = config('app.documentation_site_url').'en/docs/tutorials';
                } else {
                  $url = config('app.documentation_site_url').'docs/tutorials';
                }
              @endphp
              <div class='col-auto'>
                <a class='btn btn-outline-secondary' href="{{ $url }}">
                <i class="fa-regular fa-circle-question fa-2x"></i>
                &nbsp;@lang('messages.tutorials')
                </a>
              </div>
              <div class='col-auto'>
                <a class='btn btn-outline-success ' href="{{url('register')}}">
                <i class="fa-solid fa-right-to-bracket fa-2x"></i>
                &nbsp;@lang('messages.register')
                </a>
              </div>
        </div>
      </div>
    </div>
</div>
</div>



</div>

{!! View::make('layouts.footer')->with([
      'landing_page' => true,
]) !!}

@livewireScripts

@vite('resources/assets/js/custom.js')                

</body>
</html>
