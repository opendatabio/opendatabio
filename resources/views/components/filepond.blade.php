@pushOnce('styles')
<link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet" />
@endPushOnce

<div  wire:ignore x-data x-init="
FilePond.setOptions({
  allowMultiple: {{ isset($attributes['multiple']) ? 'true' : 'false'}},
  server: {
    process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
      @this.upload('{{ $attributes['wire:model']}}',file,load,error,progress)
    },
    revert: (filename,load) => {
      @this.removeUpload('{{ $attributes['wire:model']}}',filename,load)
    }
  },
});  
FilePond.registerPlugin(FilePondPluginFileValidateType);
FilePond.registerPlugin(FilePondPluginFileValidateSize);
FilePond.create($refs.inputfile,{
    maxFileSize: '100MB',    
});
">
  <input type="file" x-ref="inputfile" />
</div>

@pushOnce('scripts')
<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
<script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.js"></script>
<script src="https://unpkg.com/filepond/dist/filepond.js"></script>
@endPushOnce