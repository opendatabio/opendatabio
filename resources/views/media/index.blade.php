@extends('layouts.app')
@section('content')
<div class="container-fluid">
  <div class="col-lg-10 offset-lg-1 col-md-6 offset-md-3">
    @if (isset($media))
    @php
      $objects  = [
          'model' => $model,
          'media' => $media
      ];
      @endphp
      {!! View::make('media.index-model',$objects) !!}
    @else
      @lang('messages.no_media_found')
    @endif

  </div>
</div>
@endsection
