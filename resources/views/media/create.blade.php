@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card mt-5">
      <div class="card-header">
        @if (isset($media))
          @lang('messages.media_edit_for_model'):
        @else
          @lang('messages.media_new_for_model'):
        @endif
        &nbsp;
        <strong>{{ class_basename($object) }}</strong>
        {!! $object->rawLink() !!}
        &nbsp;
        <a data-bs-toggle="collapse" href="#media_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        <div id="media_hint" class="odb-hint collapse">
          <br>
          @lang('messages.media_mimes_hint')
          <code>
            {{ $validMimeTypes }}
          </code>
        </div>
      </div>
      <div class="card-body">

@if (isset($media))
		<form action="{{ url('media/'.$media->id) }}" method="POST" class="row g-2" enctype="multipart/form-data">
      {{ method_field('PUT') }}
@else
		 <form action="{{ url('media') }}" method="POST" class="row g-2" enctype="multipart/form-data">
@endif

  <!-- csrf protection -->
  {{ csrf_field() }}

  <input type="hidden"  name="model_id" value="{{$object->id}}">
  <input type="hidden"  name="model_type" value="{{get_class($object)}}">
  @if (isset($customProperties))
    <input type="hidden"  name="custom_properties" value="{{ $customProperties}}">
  @endif


@if (isset($media))
  @php
    $fileUrl = $media->getUrl();
  @endphp
<div class="row mb-3">
  <div class="col">
    <input type="hidden"  name="media_id" value="{{$media->id}}">
    @if ($media->media_type == 'image')
        <img src="{{ $fileUrl }}" alt="" class='vertical_center' width='100%'>
    @endif
    @if ($media->media_type == 'video')
      <video width="100%"
          style="
          object-fit: cover;
          object-position: left bottom;"
          controls
          >
        <source src="{{ $fileUrl }}"  type="{{ $media->mime_type }}"/>
        Your browser does not support the video tag.
      </video>
    @endif

    @if ($media->media_type == 'audio')
        <center >
        <br><br>
        <i class="fas fa-file-audio fa-6x"></i>
        <br><br>
        <audio controls >
          <source src="{{ $fileUrl }}"  type="{{ $media->mime_type }}"/>
          </audio>
        </center>
    @endif
  </div>
</div>
@else
<div class="mb-3">
  <div class="col-sm-12">
    <label for="uploaded_media" class="form-label mandatory">
      @lang('messages.media_file')
    </label>
    <div class="">
    <input type="file" name="uploaded_media" id="uploaded_media" >
    </div>
  </div>
</div>
@endif

<div class="mb-3">
  <div class="form-label">
    @lang('messages.title') @lang('messages.translations')
    <a data-bs-toggle="collapse" href="#title_hint" class="odb-unstyle align-top"><i class="far fa-question-circle"></i></a>
  </div>
  <div id="title_hint" class="odb-hint collapse">
    @lang('messages.media_title_hint')
  </div>
  @foreach ($languages as $language)
  <div class='mb-3'>
      <label for="description{{$language->id}}" class='form-label align-top col-sm-2'>{{$language->name}}</label>
      @php
        $mediaTitle =
        old('description.'. $language->id,
        isset($media)
        ? $media->translate(\App\Models\UserTranslation::DESCRIPTION, $language->id)
        : null);
      @endphp
      <textarea id="description{{$language->id}}"  name="description[{{$language->id}}]" class='form-control'>{{$mediaTitle}}</textarea>

  </div>
  @endforeach
</div>

<hr>


<div class="mb-3">
<label for="tags" class="form-label">
@lang('messages.tags')
</label>
<a data-bs-toggle="collapse" href="#tags_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
<div id="tags_hint" class="odb-hint collapse">
  @lang('messages.tags_hint')
</div>
<div class="">
{!! Multiselect::select(
    'tags',
    $tags->pluck('name', 'id'), isset($media) ? $media->tags->pluck('id') : [],
     ['class' => 'multiselect form-control']
) !!}
</div>

</div>


<div class="mb-3">
  <label for="collector" class="form-label" id='authorslabel'>
    @lang('messages.credits')
  </label>
  <a data-bs-toggle="collapse" href="#credits_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div class="">
    {!! Multiselect::autocomplete('collector',
        $persons->pluck('abbreviation', 'id'),
        isset($media) ? $media->collectors->pluck('person_id') : '',
        ['class' => 'multiselect form-control'])
    !!}
  </div>
  <div id="credits_hint" class="odb-hint collapse">
    @lang('messages.credits_hint')
  </div>
</div>


<!-- license object must be an array with CreativeCommons license codes applied to the model --->
<div class="mb-3" id='creativecommons' >
    <label for="license" class="form-label mandatory">
      @lang('messages.public_license')
    </label>
    <a data-bs-toggle="collapse" href="#creativecommons_media_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div class="">
      @php
        $currentLicense = "CC0";
        $currentVersion = config('app.creativecommons_version')[0];
        if (isset($media)) {
           if (null != $media->license) {
             $license = explode(' ',$media->license);
             $currentLicense = $license[0];
             $currentVersion = $license[1];
           }
        }
        $oldLicense = old('license', $currentLicense);
        $oldVersion = old('version',$currentVersion);
        $readonly = null;
        if (count(config('app.creativecommons_version'))==1) {
          $readonly = 'readonly';
        }
      @endphp
      <select name="license" id="license" class="form-select" >
        @foreach (config('app.creativecommons_licenses') as $level)
          <option value="{{ $level }}" {{ $level == $oldLicense ? 'selected' : '' }}>
            {{$level}} - @lang('levels.' . $level)
          </option>
        @endforeach
      </select>
      <strong>version:</strong>
      @if (null != $readonly)
        <input type="hidden" name="license_version" value=" {{ $oldVersion }}">
        {{ $oldVersion }}
      @else
      <select name="license_version" class="form-select" {{ $readonly }}>
        @foreach (config('app.creativecommons_version') as $version)
          <option value="{{ $version }}" {{ $version == $oldVersion ? 'selected' : '' }}>
            {{ $version}}
          </option>
        @endforeach
      </select>
      @endif
    </div>
      <div id="creativecommons_media_hint" class="odb-hint collapse">
        <br>
        @lang('messages.creativecommons_media_hint')
      </div>
</div>


<!-- dataset -->
<div class="mb-3">
    <label for="dataset" class="form-label ">
      @lang('messages.dataset')
    </label>
    <a data-bs-toggle="collapse" href="#dataset_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div class="">
      <input type="text" name="dataset_autocomplete" id="dataset_autocomplete" class="form-control autocomplete"
      value="{{ old('dataset_autocomplete', (isset($media) and $media->dataset_id) ? $media->dataset->name : null) }}">
      <input type="hidden" name="dataset_id" id="dataset_id"
      value="{{ old('dataset_id', isset($media) ? $media->dataset_id : null) }}">
    </div>
      <div id="dataset_hint" class="odb-hint collapse">
	       @lang('messages.media_dataset_hint')
       </div>
</div>

<div class="mb-3" >
  <label for="date" class="form-label">@lang('messages.date')</label>
  <a data-bs-toggle="collapse" href="#date_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div class="">
    <input type="date" name="date" value="{{ old('date',isset($media) ? $media->date : null) }}" min="1600-01-01" max={{today()}}>
  </div>
  <div id="date_hint" class="odb-hint collapse">
    @lang('messages.media_date_hint')
  </div>
</div>


<div class="mb-3">
<label for="notes" class="form-label">
@lang('messages.notes')
</label>
<div class="">
  <textarea name="notes" id="notes" class="form-control">{{ old('notes', isset($media) ? $media->notes : null) }}</textarea>
</div>
</div>

  <div class="mb-3">
				<button type="submit" class="btn btn-success" name="submit" value="submit">
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.add')
				</button>
				<a href="{{url()->previous()}}" class="btn btn-warning">
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.back')
				</a>
			    </div>
		    </form>
        </div>
    </div>
  </div>


@endsection


@once
@push('style')
<style >
  .vertical_center {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 99%;
    width: 99%;
  }

  .max_width {
    max-width: 100%;
  }
</style>
@endpush

@push ('scripts')
{!! Multiselect::scripts('collector', url('persons/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
@vite('resources/assets/js/custom.js')

<script type='module'>
$(document).ready(function() {

$("#dataset_autocomplete").odbAutocomplete("{{url('datasets/autocomplete')}}","#dataset_id", "@lang('messages.noresults')");

/* if license is different than public domain, title and authors must be informed */
/* and sui generis database policies may be included */
$('#license').on('change',function() {
    var license = $('#license option:selected').val();
    if (license == "CC0") {
        $('#authorslabel').removeClass('mandatory');
    } else {
        $('#authorslabel').addClass('mandatory');
    }
});

});

</script>

@endpush
@endonce