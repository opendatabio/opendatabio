@extends('layouts.app')
@section('content')

<div class="container">
  <br>
  <div class="">
  <div class="card" style='min-width: 60vw;'>
      <div class="card-header">
        @lang('messages.media_for')
        @php
        $model = $media->model()->withoutGlobalScopes()->first();
        $basename = Lang::get("classes.".class_basename($model));
        $fullname = $model->rawLink();
        $fileUrl = $media->getUrl();
        @endphp
      <strong>{!! $basename." ".$fullname !!}</strong>

      <span class="history" style="float:right">
      <a href="{{url('media/'.$media->id.'/history')}}">
      @lang ('messages.see_history')
      </a>
      </span>
    </div>
		<div class="card-body">
      @if ($media->title)
        <h4>{{ $media->title }}</h4>
        @if ($media->all_collectors)
          By {{ $media->all_collectors }} {{$media->year }}
        @endif
      @endif

      <dl class="">
        @php
          $mediatype = ucfirst($media->media_type);
        @endphp
        <dt class='dt-odb'>
          @lang('messages.media_linked_to',compact('mediatype'))
        </dt>
        <dd class='mb-3'>
          {{ $basename }}&nbsp;
          {!! $model->rawLink() !!}
        </dd>

      <!-- DEFINE LICENSE IMAGE BASED ON LICENSE -->
      @if ($media->license)
        @php
          $license = explode(" ",$media->license);
          $license_logo = 'images/'.mb_strtolower($license[0]).".png";
        @endphp
        <dt class='dt-odb'>
          @lang('messages.license')
        </dt>
        <dd class='mb-3'>
          {{ $media->license }}
          <br>
          <a href="http://creativecommons.org/license" target="_blank">
            <img src="{{ asset($license_logo) }}" alt="{{ $media->license }}" width='100px'>
          </a>
        </dd>
      @endif

      @if ($media->tags->count())
      <dt class='dt-odb'>
          @lang('messages.tagged_with')
      </dt>
      <dd class='mb-3'>
        {!! $media->tagLinks !!}
      </dd>
      @endif



@if (isset($media->citation))
  <dt class='dt-odb'>
    @lang('messages.howtocite')
  </dt>
  <dd class='mb-3'>
    {!! $media->citation !!}
    &nbsp;&nbsp;
    <a data-bs-toggle="collapse" href="#bibtex" class="btn-sm btn-primary">BibTeX</a>
    <div id='bibtex' class='odb-hint collapse'>
      <pre><code>{{ $media->bibtex }}</code></pre>
    </div>
  </dd>


@endif

@if (isset($media->notes))
  {!! $media->formated_notes !!}
@endif

</dl>

<div class="row mb-5 mt-5">
@can ('update', $media)
<div class="col col-sm-auto col-lg-auto mx-2">
	<a class="btn btn-success" href="{{url ('media/' . $media->id . '/edit')}}">
	    <i class="fa fa-btn fa-plus"></i>
      @lang('messages.edit')
	</a>
</div>
@endcan
@can('create','App\Models\Measurement')
<div class="col col-sm-auto col-lg-auto mx-2">
  <a href="{{ url('measurements/'. $media->id. '/media')  }}" class="btn btn-outline-secondary text-nowrap">
          <i class="fa fa-btn fa-search"></i>
          {{ Lang::get('messages.measurements') }}
  </a>
</div>
@endcan

@can ('delete', $media)
  <div class="col col-sm-auto col-lg-auto mx-2">
  	<form action="{{ url('media/'.$media->id) }}" method="POST" class="form-horizontal">
      {{ csrf_field() }}
      {{ method_field('DELETE') }}
       <button type="submit" class="btn btn-danger">
         <i class="fa fa-btn fa-plus"></i>
         @lang('messages.remove_media')
       </button>
    </form>
  </div>
@endcan

</div>

@if ($media->media_type == 'image')
          <img src="{{ $fileUrl }}" alt="" class='vertical_center'>
      @endif


      @if ($media->media_type == 'video')
        <video width="100%"
          style="
          object-fit: cover;
          object-position: left bottom;"
          controls
          >
          <source src="{{ $fileUrl }}"  type="{{ $media->mime_type }}"/>
          Your browser does not support the video tag.
        </video>
      @endif

      @if ($media->media_type == 'audio')
          <center >
          <br><br>
          <i class="fas fa-file-audio fa-6x"></i>
          <br><br>
          <audio controls >
            <source src="{{ $fileUrl }}"  type="{{ $media->mime_type }}"/>
            </audio>
          </center>
      @endif

      <br>

</div>

</div>
</div>
</div>



@endsection

@once
@push('styles')
<style >

.vertical_center {
  display: flex;
  justify-content: center;
  align-items: center;
  height: 99%;
  width: 99%;
}

</style>
@endpush
@endonce