@extends('layouts.app')

@section('content')
<div class="container">
  <br>
  <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
      <div class="card-header">
        @lang('messages.media_upload')
        &nbsp;
        <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        <div id="help" class="odb-hint collapse">
            @lang('messages.media_upload_hint')
        </div>
      </div>

@can ('create', App\Models\Media::class)
    <div class="card-body">
<form action="{{ url('import/media')}}" method="POST" class="row g-2" enctype="multipart/form-data">
{{ csrf_field() }}
<div class="mb-3">
  <label for="imageattributes" class="form-label mandatory">
      @lang('messages.media_attribute_table')
  </label>
  <a data-bs-toggle="collapse" href="#hint_img_attributes" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div class="">
    <input type="file" name="media_attribute_table" id="media_attribute_table">
  </div>
    <div id="hint_img_attributes" class="odb-hint collapse">
      @lang('messages.media_attribute_table_hint',['validlicenses' => implode(' | ',config('app.creativecommons_licenses'))])
    </div>
</div>

<div class="mb-3">
  <label for="file" class="form-label mandatory">
      @lang('messages.media_files')
  </label>
  <a data-bs-toggle="collapse" href="#media_files_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div class="">
  <input type="file" 
    class="filepond"
    name="file[]"
    multiple 
    data-allow-reorder="true"
    data-max-file-size="30MB"
    data-max-files="3"
    id="mediaFiles" 
    >
  </div>
    <div id="media_files_hint" class="odb-hint collapse">
      @lang('messages.media_files_hint')
    </div>
</div>
<div class="mb-3">
    <button type="submit" class="btn btn-success" name="submit" value="submit">
    <i class="fa fa-btn fa-plus"></i>
    @lang('messages.add')
</button>
</div>
</form>

</div>
@endcan
</div>

</div>

@endsection

@once
@push('scripts')

<script type='module'>
    const inputElement = document.querySelector('input[type="file"].filepond');
    FilePond.create(inputElement).setOptions({
      server: {
          url: '../filepond/api',
          process: '/process',
          method: 'POST',
          revert: '/process',
          headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
          }
      },
      allowMultiple: true,
    });
</script>
@vite('resources/assets/js/custom.js')

@endpush
@endonce