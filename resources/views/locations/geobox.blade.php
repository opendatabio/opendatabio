<small><strong>@lang('messages.location')</strong></small>
<br>
{!! $location->rawLink() !!}
<br>
<small><strong>@lang('messages.adm_level')</strong></small>
<br>
{{ $location->country_adm_level }}


@if ($location->x or $location->y)
<br>
<small><strong>@lang('messages.dimensions')</strong></small>
<br>
@if ($location->adm_level == 101)
  @lang('messages.transect_length'): {{ $location->transect_length }}&nbsp;m
  <br>
  @lang('messages.transect_buffer'): {{$location->y}}&nbsp;m
@else
x: {{ $location->x }}&nbsp; m
<br>
y: {{$location->y}}&nbsp;
<br>
area: {{$location->area}}&nbsp;m<sup>2</sup>
@endif
@endif

@if ($location->startx or $location->starty)
<br>
<small><strong>@lang('messages.position')</strong></small>
<br>
X: {{ $location->startx }}m
<br>
Y: {{$location->starty}}m
</a>
@endif
@if ($location->notes)
<hr>
{!! $location->formated_notes !!}
<hr>
@endif

