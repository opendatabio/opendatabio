@extends('layouts.app')
@section('content')
<div id='contents' class="container">
  <br>
  <div class="card col-lg-10 offset-lg-1 col-md-6 offset-md-3">
      <!-- Project basic info block -->
      <div class="card-header">
        @lang('messages.location')
        <span class="history" style="float:right">
        <a href="{{url('locations/'.$location->id.'/activity')}}">
          @lang ('messages.see_history')
          </a>
        </span>
      </div>
      <div class="card-body">
        <dl class="row">

<dt class="dt-odb">@lang('messages.location_name')</dt>
<dd>{{ $location->name }}</dd>

<dt class="dt-odb">@lang('messages.adm_level')</dt>
<dd>
  {{ $location->country_code ? $location->country_adm_level : ""}} [@lang('levels.adm_level.' . $location->adm_level)]
  <br>
  [ @lang('messages.geometry'): {{ $location->geomType }}]
</dd>

<dt class="dt-odb">@lang('messages.centroid')</dt>
<dd>{{ $location->centroid_raw }}</dd>

<!-- Plot specific info when dimensions is present -->
<!-- X, Y, etc) -->
@if ($location->x or $location->y)
  <dt class="dt-odb">@lang('messages.dimensions')</dt>
  <dd>
    @if ($location->adm_level == 101)
     @lang('messages.transect_length'): {{ $location->transect_length }}&nbsp;m, @lang('messages.transect_buffer'): {{$location->y}}&nbsp;m
    @else
     x: {{ $location->x }} m  | y: {{$location->y}} m  | area: {{$location->area}} m<sup>2</sup>
    @endif
  </dd>
@endif


@if ($location->startx or $location->starty)
  <dt class="dt-odb">@lang('messages.position')</dt>
  <dd>X: {{ $location->startx }}m, Y: {{$location->starty}}m</dd>
@endif

<dt class="dt-odb">@lang('messages.higherGeography')</dt>
<dd>
{!! $location->higherGeographyLinks !!}
<dd>

@if ($location->relatedLocations->count())
<dt class="dt-odb">@lang('messages.location_belongs')</dt>
<dd>
  <ul >
  @foreach($location->relatedLocations as $related)
    <li >{!! $related->relatedLocation->rawLink() !!}</li>
  @endforeach
  </ul>
</dd>
@endif

@if($location->childrenCount()>0)
<dt class="dt-odb">@lang('messages.total_descendants')</dt>
<dd>
  <a  href="{{url('locations/'.$location->id.'/location')}}" >
    {{ $location->childrenCount() }}
  </a>
</dd>
@endif

@if ($location->altitude)
<dt class="dt-odb">@lang('messages.altitude')</dt>
<dd>{{ $location->altitude }}</dd>
@endif


@if ($location->notes)
  {!! $location->formated_notes !!}
@endif

<dd>
  <a href="{{ url('locations/'. $location->id. '/mapme')  }}" class="btn btn-primary">
    <i class="fas fa-map-marked-alt fa-1x"></i>&nbsp;@lang('messages.map')
</a>
</dd>

</dl>

</div>


<!-- BUTTONS -->
<nav class="navbar navbar-light navbar-expand-lg">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarbuttons" aria-controls="navbarbuttons" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarbuttons">
      <br>
      <ul class="navbar-nav mb-lg-0 g-3">


       @can ('update', $location)
       <li class="nav-item px-2 mb-2">
    	     <a href="{{ url('locations/'. $location->id. '/edit')  }}" class="btn btn-success" name="submit" value="submit">
                @lang('messages.edit')
    	        </a>
        </li>
      @endcan


    
        <li class="nav-item px-2 mb-2">
        <a href="{{ url('locations/'. $location->id. '/measurements')  }}" class="btn btn-outline-secondary text-nowrap">
          <i class="fa fa-btn fa-search"></i>
          {{ Lang::get('messages.measurements') }}
        </a>
      </li>
      <li class="nav-item px-2 mb-2">
        <a href="{{ url('individuals/'. $location->id. '/location')  }}" class="btn btn-outline-secondary">
          <i class="fa fa-btn fa-search"></i>
          @lang('messages.individuals')
        </a>
      </li>
    <li class="nav-item px-2 mb-2">
        <a href="{{ url('taxons/'. $location->id. '/location')  }}" class="btn btn-outline-secondary">
          <i class="fa fa-btn fa-search"></i>
          @lang('messages.taxons')
        </a>
      </li>
   <li class="nav-item px-2 mb-2">
        <a href="{{ url('locations/'. $location->id. '/vouchers')  }}" class="btn btn-outline-secondary">
         <i class="fa fa-btn fa-search"></i>
         @lang('messages.vouchers')
       </a>
     </li>


  <!-- this will show only if no media as media are shown below -->
  @can ('create', App\Models\Media::class)
  <li class="nav-item px-2 mb-2">
    <a href="{{ url('locations/'. $location->id. '/media-create')  }}" class="btn btn-outline-secondary">
        <i class="fa fa-btn fa-plus"></i>
        <i class="fas fa-photo-video"></i>
        <i class="fas fa-headphones-alt"></i>
        @lang('messages.create_media')
    </a>
  </li>
  @endcan
<li class="nav-item px-2 mb-2">
    <a href="{{ Route('media.location.list',[
      'location_id' => $location->id])}}" class="btn btn-outline-primary">
    <i class="fa fa-btn fa-plus"></i>
    <i class="fas fa-photo-video"></i>
    <i class="fas fa-headphones-alt"></i>
    @lang('messages.media_see_all')
    </a>
</li>
</ul>
</div>
</div>
</nav>

</div>

</div>
<!-- END -->

@endsection


