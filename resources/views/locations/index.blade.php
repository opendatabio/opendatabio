@extends('layouts.app')

@section('content')
<div id='contents' class="container">
    <br>
      <!--   <div class="col-sm-offset-2 col-sm-8"> -->
<!-- Registered Locations -->
    <div class="card">
        <div class="card-header">
          @lang('messages.registered_locations')
          @if(isset($object))
            @lang('messages.for')
            <strong>
            {{ class_basename($object) }}
            </strong>
            &nbsp;
            {!! $object->rawLink() !!}
          @endif
          &nbsp;&nbsp;
          <a data-bs-toggle="collapse" href="#help" class="odb-unstyle" title="@lang('messages.help')"><i class="far fa-question-circle"></i></a>
          <div id="help" class="odb-hint collapse">
      	     @lang('messages.location_hint')
             <code>@lang('messages.download_login')</code>
          </div>
        </div>

        @if (Auth::user())
          {!! View::make('common.exportdata')->with([
                'object' => isset($object) ? $object : null,
                'export_what' => 'Location',
                'filters' => isset($job_id) ? ['job_id'=> $job_id] : null,
          ]) !!}
          <!--- delete form -->
          {!! View::make('common.batchdelete')->with([
                'url' => url('locations/batch_delete'),
          ]) !!}
        @else
          <br>
        @endif
        <form action="{{url('locations/map')}}" method="POST" class="form-horizontal" id='odb-map-occurrences'>
          <!-- csrf protection -->
          {{ csrf_field() }}
          <input type="hidden" name="idstomap" value="" id='odb-map-selected-rows'>
        </form>
    <div class="card-body">
        {!! $dataTable->table([],true) !!}
    </div>
    </div>
        <!-- </div> -->
</div>

@endsection

@once
@push ('scripts')
  {{ $dataTable->scripts(attributes: ['type' => 'module']) }}

  <script type="module">

  $(document).ready(function() {
      const table = $('#dataTableBuilder');
      table.on('preXhr.dt',function(e,settings,data) {
        data.adm_level = $("#location_level option").filter(':selected').val();
        /* an any defined in the export form */
        data.project = $("input[name='project']").val();
        data.location = $("input[name='location_root']").val();
        data.taxon = $("input[name='taxon_root']").val();
        /*console.log(data.level,data.project,data.location,data.taxon); */
      });
      $('#location_level').on('change',function() {
        table.DataTable().ajax.reload();
        return false;
      });

      $(".odb-create-button").css("background-color","brand-success");
  });
  </script>
  @vite('resources/assets/js/custom.js')
@endpush
@endonce