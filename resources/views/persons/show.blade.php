@extends('layouts.app')

@section('content')
    <div class="container">
      <br>
      <div class="col-lg-8 offset-lg-2 col-md-6 offset-md-3 col-sm-6 offset-sm-3">
      <div class="card ">
          <div class="card-header">
                    @lang('messages.person_details')
                    <span class="history" style="float:right">                    
                    <a href="{{url('persons/'.$person->id.'/history')}}">
                    @lang ('messages.see_history')
                    </a>
                    </span>
         </div>

		<div class="card-body">
      <dl class="">
        <dt class="dt-odb">
  	@lang('messages.full_name')
  </dt>
  <dd class="mb-3">
  	{{ $person->full_name }}
  </dd>
  <dt class="dt-odb">
    	@lang('messages.abbreviation')
    </dt>
    <dd class="mb-3">
	{{ $person->abbreviation }}
</dd>
@php
  $valid = (null != Auth::user()) ? Auth::user()->access_level>=1 : false;
@endphp
@if (isset($person->email) and ($person->email_public==1 or $valid))
<dt class="dt-odb">
	@lang('messages.email')
</dt>
<dd class="mb-3">
	{{ $person->email }}
</dd>
@endif
@if ($person->institution)
<dt class="dt-odb">
	@lang('messages.institution')
</dt>
<dd class="mb-3">
	{{ $person->institution }}
</dd>
@endif
@if ($person->biocollection)
<dt class="dt-odb">
  	@lang('messages.biocollection')
  </dt>
  <dd class="mb-3">
{!! $person->biocollection->rawLink() !!}
</dd>
@endif


@if ($person->notes)
  {!! $person->formated_notes !!}
@endif

@if ($person->taxons->count())
<dt class="dt-odb">
  	@lang('messages.specialist_in')
  </dt>
  <dd class="mb-3">
<ul>
@foreach ($person->taxons as $taxon)
<li> {!! $taxon->rawLink() !!} </li>
@endforeach
</ul>
</dd>
@endif
</dl>

@can ('delete', $person)
<div class="row g-3 p-2" id='odb-delete-confirmation-form' style="display: none;">
  <p class="col-auto alert alert-danger">
    @lang('messages.action_not_undone')
  </p>
  <form class='col-auto' action="{{ url('persons/'. $person->id)  }}" method="POST" >
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
    <button type="submit" class="btn btn-danger mt-3" title="@lang('messages.remove')">
      <i class="far fa-trash-alt"></i>
      @lang('messages.remove')
    </button>
  </form>
</div>
@endcan

<!-- BUTTONS -->
<nav class="navbar navbar-light navbar-expand-lg" id="buttons-navbar">
<div class="container-fluid">
  <div class="navbar-header">
  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarbuttons" aria-controls="navbarbuttons" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
</div>
  <div class="navbar-collapse collapse flex-column ml-lg-0 ml-3" id="navbarbuttons">
    <br>
      <ul class="nav navbar-nav flex-row">
        @can ('update', $person)
        <li class="nav-item px-2 mb-2">
  	       <a class="btn btn-success" href="{{url ('persons/' . $person->id . '/edit')}}">
         @lang('messages.edit_person')
  			</a>
        </li>
        @endcan

        @can ('delete', $person)
          <li class="nav-item px-2 mb-2">
            <button class="odb-delete-confirmation-button btn btn-danger" type='button' >
              @lang('messages.remove')
            </button>
          </li>
        @endcan

        <li class="nav-item px-2 mb-2">
          <a href="{{ url('persons/'. $person->id. '/individuals')  }}" class="btn btn-outline-secondary">
        <i class="fa fa-btn fa-search"></i>
        @lang('messages.individuals')
    </a>
  </li>
  <li class="nav-item px-2 mb-2">
    <a href="{{ url('persons/'. $person->id. '/vouchers')  }}" class="btn btn-outline-secondary">
        <i class="fa fa-btn fa-search"></i>
@lang('messages.vouchers')
    </a>
  </li>
</ul>
</div>
</div>
</nav>
</div>
</div>
</div>
</div>
@endsection
