<div class="mb-3">
    <label for="full_name" class="form-label mandatory">
@lang('messages.full_name')
</label>
    <div class="">
	<input type="text" name="full_name" id="full_name" class="form-control" value="{{ old('full_name', isset($person) ? $person->full_name : null) }}">
    </div>
</div>
<div class="mb-3">
    <label for="abbreviation" class="form-label mandatory">
      @lang('messages.abbreviation')
</label>
        <a data-bs-toggle="collapse" href="#hint1" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div class="">
	<input type="text" name="abbreviation" id="abbreviation" class="form-control" value="{{ old('abbreviation', isset($person) ? $person->abbreviation : null) }}">
            </div>
    <div id="hint1" class="odb-hint collapse">
	@lang('messages.abbreviation_hint')
    </div>
</div>
<div class="mb-3">
    <label for="email" class="form-label">
@lang('messages.email')
</label>
	    <div class="">
	<input type="text" name="email" id="email" class="form-control" value="{{ old('email', isset($person) ? $person->email : null) }}">
    </div>
</div>

<div class="mb-3">
    <input type="checkbox" name="email_public" id="email_public"  value=1  {{ isset($person) ? ($person->email_public==1 ? 'checked' : '') : '' }} >
    <label class="form-check-label" for="ismanaged">
    @lang('messages.email_public')
    </label>
    <a data-bs-toggle="collapse" href="#email_public_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="email_public_hint" class="odb-hint collapse">
      @lang ('messages.email_public_hint')
    </div>
</div>

<div class="mb-3">
    <label for="institution" class="form-label">
@lang('messages.institution')
</label>
	    <div class="">
	<input type="text" name="institution" id="institution" class="form-control" value="{{ old('institution', isset($person) ? $person->institution : null) }}">
    </div>
</div>

<div class="mb-3">
    <label for="biocollection_id" class="form-label">
@lang('messages.biocollection')
</label>
        <a data-bs-toggle="collapse" href="#hint2" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div class="">
	<?php $selected = old('biocollection_id', isset($person) ? $person->biocollection_id : null); ?>

	<select name="biocollection_id" id="biocollection_id" class="form-select" >
		<option value='' >&nbsp;</option>
	@foreach ($biocollections as $biocollection)
		<option value="{{$biocollection->id}}" {{ $biocollection->id == $selected ? 'selected' : '' }}>{{$biocollection->acronym}}</option>
	@endforeach
	</select>
            </div>
    <div id="hint2" class="odb-hint collapse">
	@lang('messages.person_biocollection_hint')
    </div>
</div>

@if (isset($person))
<div class="mb-3">
    <label for="specialist" class="form-label">
@lang('messages.specialist_in')
</label>
        <a data-bs-toggle="collapse" href="#hint3" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div class="">
{!! Multiselect::autocomplete('specialist', $taxons->pluck('fullname', 'id'), $person->taxons->pluck('id'), ['class' => 'multiselect form-control']) !!}
            </div>
    <div id="hint3" class="odb-hint collapse">
	@lang('messages.person_specialist_hint')
    </div>
</div>
@endif 

<!-- isset person -->

<!-- notes for person -->
<div class="mb-3">
    <label for="notes" class="form-label">
@lang('messages.notes')
</label>
	    <div class="">
	<textarea name="notes" id="notes" class="form-control">{{ old('notes', isset($person) ? $person->notes : null) }}</textarea>
            </div>
</div>

@once
    @push('scripts')
    {!! Multiselect::scripts('specialist', url('taxons/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
    @endpush
@endonce