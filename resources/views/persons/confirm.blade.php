@extends('layouts.app')

@section('content')
    <div class="container">
      <br>
      <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
          <div class="card-header">
                    @lang('messages.confirm_person')
                </div>

                <div class="card-body">
		@lang ('messages.confirm_person_message')
    <hr>
		<strong>@lang('messages.full_name')</strong> {{old('full_name')}}
		<strong>@lang('messages.abbreviation')</strong> {{old('abbreviation')}}
    <hr>
		<strong>@lang ('messages.possible_dupes')</strong>
		<br><br>
			<ul>
		@foreach ( $dupes as $dupe)
			<li>{{$dupe->full_name}} ({{$dupe->abbreviation}})</li>
		@endforeach
			</ul>
		    <!-- Edit Person Form -->
		    <form action="{{ url('persons') }}" method="POST" class="row g-2">
			 {{ csrf_field() }}
<input type="hidden" name="confirm" value="1">
<input type="hidden" name="full_name" id="full_name" value="{{ old('full_name') }}">
<input type="hidden" name="abbreviation" id="abbreviation" value="{{ old('abbreviation') }}">
<input type="hidden" name="email" id="email" value="{{ old('email') }}">
<input type="hidden" name="institution" id="institution" value="{{ old('institution') }}">

<input type="hidden" name="notes" id="notes" value="{{ old('notes')}}">

<input type="hidden" name="biocollection_id" value="{{ old('biocollection_id') }}">
<div class="mb-3">
<button type="submit" class="btn btn-success">
    <i class="fa fa-btn fa-plus"></i>
    @lang('messages.confirm_person')
</button>
</div>

</form>

</div>
</div>
</div>
@endsection
