@extends('layouts.app')

@section('content')
<div class="container">
  <br>
  <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
      <div class="card-header">
                @if (isset($person))
                @lang('messages.edit_person')
                @else
                @lang('messages.new_person')
                @endif
                &nbsp;&nbsp;
                <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
            <div id="help" class="odb-hint collapse">
                 @lang('messages.person_new_hint')
            </div>
          </div>
            <div class="card-body">

    <!-- Edit Person Form -->
    @if (isset($person))
    <form action="{{ url('persons/'.$person->id) }}" method="POST" class="row g-2">
	                   {{ method_field('PUT') }}
    @else
     <form action="{{ url('persons')}}" method="POST" class="row g-2">
    @endif
    {{ csrf_field() }}

		@include('persons.form')

    <div class="mb-3">
			<div class="col-sm-offset-3 col-sm-6">
				<button type="submit" class="btn btn-success">
				    <i class="fa fa-btn fa-plus"></i>
              @lang('messages.save')
			</button>
      &nbsp;&nbsp;
			<a href="{{url()->previous()}}" class="btn btn-warning">
				    <i class="fa fa-btn fa-plus"></i>
            @lang('messages.back')
				</a>
			</div>
		</div>
</form>

@if (isset($person))
			@can ('delete', $person)
		    <form action="{{ url('persons/'.$person->id) }}" method="POST" class="form-horizontal">
			 {{ csrf_field() }}
                         {{ method_field('DELETE') }}
		        <div class="mb-3">
			    <div class="col-sm-offset-3 col-sm-6">
				<button type="submit" class="btn btn-danger">
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.remove_person')

				</button>
			    </div>
			</div>
		    </form>
		    @endcan <!-- end can delete -->
@endif
                </div>
	    </div>
<!-- Other details (specialist, collects, etc?) -->
        </div>
    </div>
@endsection

@once
@push ('scripts')
  {!! Multiselect::scripts('specialist', url('taxons/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
@endpush
@endonce
