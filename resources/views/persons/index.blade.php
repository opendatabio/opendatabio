@extends('layouts.app')

@section('content')
<div id='contents' class="container">
  <br>
  <div class="card">
      <div class="card-header">
      @lang('messages.registered_persons')
      &nbsp;&nbsp;
      <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
      <div id="help" class="odb-hint collapse">
           @lang('messages.person_hint')
      </div>
    </div>
    @if (Auth::user())
    {!! View::make('common.exportdata')->with([
          'export_what' => 'Person',
          'filters' => isset($job_id) ? ['job_id'=> $job_id] : null,
    ]) !!}
    <!--- delete form -->
    {!! View::make('common.batchdelete')->with([
          'url' => url('persons/batch_delete'),
    ]) !!}
    <br>
    @endif
    <div class="card-body">
      {!! $dataTable->table([],true) !!}
    </div>
  </div>
</div>
@endsection
@once
  @push ('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
    @vite('resources/assets/js/custom.js')
  @endpush
@endonce
