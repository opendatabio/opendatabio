@extends('layouts.app')

@section('content')
<div class="container">
  <br>
  <div class="card col-lg-8 offset-lg-2 col-md-6 offset-md-3">
        <div class="card-header">
        @lang('messages.bibreference')
        <span class="history" style="float:right">
            <a href="{{url('references/'.$reference->id.'/activity')}}">
            @lang ('messages.see_history')
          </a>
        </span>
      </div>

      <div class="card-body">
        <dl>
          <dt class="dt-odb">
            @lang('messages.authors')
          </dt>
          <dd class="mb-3">
            {{ $reference->author }}
          </dd>
          <dt class="dt-odb">
              @lang('messages.title')
          </dt>
          <dd class="mb-3">
            {{ $reference->title }}
          </dd>
          <dt class="dt-odb">
              @lang('messages.year')
            </dt>
            <dd class="mb-3">
            {{ $reference->year }} &nbsp;
        </dd>

        @if ($reference->doi)
        <dt class="dt-odb">
            @lang('messages.doi')
          </dt>
          <dd class="mb-3">
            <a href="https://dx.doi.org/{{ $reference->doi }}">{{$reference->doi}}</a>
          </dd>
        @else
          @if ($reference->url)

          <dt class="dt-odb">
            @lang('messages.externallink')
          </dt>
          <dd class="mb-3">
              <a href="{{ $reference->url}}">{{$reference->url}}</a>
            </dd>
          @endif
        @endif
        <dt class="dt-odb">
          @lang('messages.bibtex_entry')
        </dt>
        <dd class="mb-3">
          <pre class='mt-3 bg-light'>{{ $reference->bibtex }}</pre>
        </dd>
      </dl>
      <!-- BUTTONS -->
    <nav class="navbar navbar-light navbar-expand-lg" id="buttons-navbar">
      <div class="container-fluid">
        <div class="navbar-header">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarbuttons" aria-controls="navbarbuttons" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
        <div class="navbar-collapse collapse flex-column ml-lg-0 ml-3" id="navbarbuttons">
          <br>
            <ul class="nav navbar-nav flex-row">

              @can ('update', $reference)
              <li class="nav-item px-2 mb-2">
                  <a href="{{url('references/' . $reference->id . '/edit')}}" class="btn btn-success">
                     <i class="fa fa-btn fa-plus"></i>
                      @lang('messages.edit')
                    </a>
                  </li>
              @endcan

              <li class="nav-item px-2 mb-2">
                <a href="{{ url('taxons/'. $reference->id. '/bibreference')  }}" class="btn btn-outline-secondary" name="submit" value="submit">
                  @if ($reference->taxons_count>0)
                    {{ $reference->taxons_count }}
                    @else
                    <i class="fa fa-btn fa-search"></i>
                    @endif
                    @lang('messages.taxons')
                  </a>
                </li>
                <li class="nav-item px-2 mb-2">
                  <a href="{{ url('datasets/'. $reference->id. '/bibreference')  }}" class="btn btn-outline-secondary" name="submit" value="submit">
                    @if ($reference->datasets_count>0)
                    {{ $reference->datasets_count }}
                    @else
                    <i class="fa fa-btn fa-search"></i>
                    @endif
                    @lang('messages.datasets')
                  </a>
                </li>
                <li class="nav-item px-2 mb-2">
                  <a href="{{ url('measurements/'. $reference->id. '/bibreference')  }}" class="btn btn-outline-secondary" name="submit" value="submit">
                    @if ($reference->measurements_count>0)
                      {{ $reference->measurements_count }}
                    @else
                      <i class="fa fa-btn fa-search"></i>
                    @endif
                    @lang('messages.measurements')
                  </a>
                </li>
                <li class="nav-item px-2 mb-2">
                  <a href="{{ url('vouchers/'. $reference->id. '/bibreference')  }}" class="btn btn-outline-secondary" name="submit" value="submit">
                        @if ($reference->vouchers()->count()>0)
                          {{ $reference->vouchers()->count() }}
                        @else
                          <i class="fa fa-btn fa-search"></i>
                        @endif
                        @lang('messages.vouchers')
                  </a>
                </li>
              </ul>
              </div>
            </div>
          </nav>
  </div>
</div>
</div>
@endsection
