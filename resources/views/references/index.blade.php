@extends('layouts.app')

@section('content')
<div id='contents' class="container">
  <br>
      <div class="card">
          <div class="card-header">
        @if (isset($object)) 
            @lang('messages.bibliographic_references') @lang('messages.for')<strong> {{ class_basename($object) }}</strong>
            {!! $object->rawLink() !!}
        @else
          @lang('messages.bibliographic_references')
          &nbsp;&nbsp;
          <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          <div id="help" class="odb-hint collapse">
            @lang('messages.references_hint')
          </div>
        @endif
      </div>
      <div class="card-body">
        @if (Auth::user())
          {!! View::make('common.exportdata')->with([
                'object' => isset($object) ? $object : null,
                'object_second' => isset($object_second) ? $object_second : null,
                'export_what' => 'Bibreference',
                'filters' => isset($job_id) ? ['job_id'=> $job_id] : null,
          ]) !!}
        @else
          <br>
        @endif

        {!! $dataTable->table([],true) !!}
      </div>
</div>
</div>
@endsection
@once
  @push ('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
    @vite('resources/assets/js/custom.js')
  @endpush
@endonce

