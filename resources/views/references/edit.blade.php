@extends('layouts.app')

@section('content')
<div class="container">
      <br>
      <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
          <div class="card-header">
              @lang('messages.edit_reference')
              {!! $reference->rawLink() !!}
          </div>

                <div class="card-body">
                    <!-- Edit Person Form -->
		    <form class="row g-2" action="{{ url('references/'.$reference->id) }}" method="POST" class="form-horizontal">
			 {{ csrf_field() }}
                         {{ method_field('PUT') }}
<div class="mb-3">
  <label for="bibtex" class="form-label mandatory">
      @lang('messages.bibtex_entry')
  </label>
	<textarea name="bibtex" id="bibtex" class="form-control" rows=10 cols=80>{{ old('bibtex', isset($reference) ? $reference->bibtex : null) }}</textarea>
</div>

<div class="mb-3">
    <label for="doi" class="form-label">
      @lang('messages.doi')
    </label>
    <input type="text" name="doi" id="doi" class="form-control" value="{{ old('doi', isset($reference) ? $reference->doi : null) }}">
</div>

<div class="mb-3">
  			<button type="submit" class="btn btn-success">
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.save')
				</button>
        &nbsp;
				<a href="{{url()->previous()}}" class="btn btn-warning">
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.back')
				</a>
</div>
</form>

@can ('delete', $reference)
<div class="mb-3">
    <form action="{{ url('references/'.$reference->id) }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
    		<button type="submit" class="btn btn-danger">
  			    <i class="fa fa-btn fa-plus"></i>
            @lang('messages.remove_reference')
          </button>
        </form>
  </div>
  @endcan

</div>

</div>
</div>
</div>
@endsection
