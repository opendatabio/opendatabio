@extends('layouts.app')

@section('content')
<div class="container">
  <br>
  <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
      <div class="card-header">
            @lang('messages.import_references')
            &nbsp;&nbsp;
            <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        </div>
        <div id="help" class="odb-hint collapse">
          @lang('messages.references_hint')
        </div>
        <div class="card-body">

        <!-- Display Validation Errors -->

   <form action="{{ url('references')}}" method="POST" class="row g-2" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="mb-3">
        <input type="checkbox" name="standardize" id="standardize" class="form-check-input" checked >&nbsp;@lang('messages.standardize_keys')
        <label for="standardize" class="form-check-label"></label>
        <a data-bs-toggle="collapse" href="#hint_standardize_key" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        <div id="hint_standardize_key" class="odb-hint collapse">
    	     @lang('messages.hint_standardize_key')
        </div>
    </div>
    <div class="mb-3">
      <label for="file" class="form-label">@lang('messages.import_from_file')</label>
      <a data-bs-toggle="collapse" href="#hint_import_file" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
      <div id="hint_import_file" class="odb-hint collapse">
         @lang('messages.import_bibfile_hint')
      </div>
      <div >
        <span class="btn btn-success fileinput-button" id="fakerfile">
          <i class="fas fa-file-upload fa-lg"></i>
          &nbsp;
          &nbsp;*.bib
        </span>
        &nbsp;&nbsp;
        <input type="file" name="rfile" id="rfile" accept=".bib" style="display:none;">
        <input type="hidden" name="MAX_FILE_SIZE" value="30000">
      </div>
    </div>
    <div class="mb-3">
      <label for="doi" class="form-label">@lang('messages.doi_search')</label>
      &nbsp;
      <a data-bs-toggle="collapse" href="#hint_checkdoi" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
      <input type="text" class='form-control' name="doi" placeholder='DOI here, ex. 10.1038/nrd842' size='50'>
      <div id="hint_checkdoi" class="odb-hint collapse">
           @lang('messages.hint_checkdoi')
      </div>
      <br>
      <button id="checkdoi" type="button" class="btn btn-primary">@lang ('messages.doicheck')</button>

    </div>
    <div class="mb-3">
      <label for="references" class="form-label">@lang ('messages.import_from_text')</label>
      <div >
       <div class="spinner" id="spinner" hidden> </div>
       <div class="alert alert-danger" id='common_errors' hidden></div>
       <textarea id='bibtex' name="references" class='form-control' rows=10 placeholder="paste here one or more bibtex records to import"></textarea>
       <br>
       <button id="submit" type="submit" value="Submit" class="btn btn-success">
           @lang ('messages.import_from_text')
       </button>
     </div>
    </div>
</form>


</div>
  </div>

</div>
</div>
@endsection

@once
@push ('scripts')
@vite('resources/assets/js/custom.js')


<script type='module'>

$(document).ready(function() {

  /** USED IN THE LOCATION MODAL TO SAVE A NEW Location*/
  $("#checkdoi").click(function(e) {
    $( "#spinner" ).css('display', 'inline-block');
    $.ajaxSetup({ // sends the cross-forgery token!
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    e.preventDefault(); // does not allow the form to submit
    $.ajax({
      type: "POST",
      url: "{{ route('findbibtexfromdoi') }}",
      dataType: 'json',
            data: {
              'doi': $('input[name=doi]').val(),
            },
      success: function (data) {

        $( "#spinner" ).hide();
        var errors = data.errors;
        if (errors != null) {
          $( "#common_errors" ).show();
          var text = "<strong>" + "@lang ('messages.whoops')" + '</strong>here' + data.errors;
          $( "#common_errors" ).html(text);
        } else {
          var bibtex = data.bibtex;
          var curval = $("#bibtex").val();
          if (curval != null) {
            bibtex = bibtex + "\n" + curval;
          }
          $("#bibtex").val(bibtex);
        }
        //alert(data.errors);
      },
      error: function(e){
        $( "#spinner" ).hide();
        $( "#common_errors" ).show();
        var text = "<strong>" + "@lang ('messages.whoops')" + '</strong>' + e;
        $( "#common_errors" ).html(text);
      }
    })
  });




});

</script>


@endpush
@endonce