@extends('layouts.app')

@section('content')
<div id='contents'  class="container">
  <br>
  <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
      <div class="card-header">
        @lang('messages.registered_history')
        &nbsp;
        <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        <div id="help" class="odb-hint collapse">
            @lang('messages.history_hint')
        </div>
      </div>
      <div class="card-body">
        <br>
        {!! $dataTable->table([],true) !!}
      </div>
  </div>
</div>
@endsection

@once
  @push ('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
    @vite('resources/assets/js/custom.js')
  @endpush
@endonce
