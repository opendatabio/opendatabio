<div  id="dataset_summary" >
<div class='card px-0 mt-3' x-data="{ open: true }">
    <div class="card-header">
        <strong>@lang('messages.dataset_summary')</strong>
        &nbsp;
        <button type="button" class="btn btn-sm" @click="open = ! open">
        <i class="far fa-eye-slash fa-lg" x-show="open"></i>
        <i class="fa-regular fa-eye fa-lg" x-show="!open"></i>
    </button>   
    </div>    
    <div x-show="open">
    <div class="card-body row" >
        <div class="card col px-0 mx-1">
            <div class="card-header fw-bold">
                @lang('messages.content')                
            </div>
            <div class="card-body" >
                    @include('datasets.content')
            </div>
        </div>   

        @if(count($plot_included)>0)
        <div class="card col px-0 mx-1">
            <div class="card-header fw-bold">
                @lang('messages.plot_included')
            </div>
            <div class="card-body">
                @include('datasets.plot-summary')
            </div>
        </div>    
        @endif
    </div>    
    <div class="card-body row mt-2" >
        @if (count($trait_summary)>0)
        <div class="card col px-0 mx-1">    
            <div class="card-header fw-bold">
                @lang('messages.measurements_summary')
            </div>
            <div class="card-body">
                @include('datasets.measurements-summary')
            </div>
        </div>
        @endif
    </div>
    </div>
</div>
</div>