<small><strong>@lang('messages.dataset')</strong></small>
<br>
{!! $dataset->rawLink() !!}
@if(isset($dataset->description))
<br>
<small>{!! $dataset->description !!}</small>
@endif
<br>
{!! $dataset->formated_privacy !!}
<hr>
<a data-bs-toggle="collapse" href="#dataset_content_hint" class="btn btn-sm btn-primary ">
  @lang('messages.content')
</a>
<div id="dataset_content_hint" class="collapse">
  @include('datasets.content')
</div>

@if(isset($plot_included))
<br>
<a data-bs-toggle="collapse" href="#dataset_hasplot" class="btn btn-success btn-sm">
  @lang('messages.plot_included')
</a>
<div id="dataset_hasplot" class="collapse">
  @include('datasets.plot-summary')
</div>
@endif

@if (isset($trait_summary))
<br>
<a data-bs-toggle="collapse" href="#dataset_measurements_summary" class="btn btn-warning btn-sm">
  @lang('messages.measurements_summary')
</a>
<div id="dataset_measurements_summary" class="collapse">
  @include('datasets.measurements-summary')
</div>
@endif

<hr>
<small>@lang('messages.dataset_centroid_hint')</small>
