<table class="table table-striped user-table">
<thead>
  <th></th>
  <th>@lang('messages.total')</th>
</thead>
<tbody>
@foreach($plot_included as $key => $value)
<tr>
  <td class="table-text">
      <strong>{{ $key }}</strong>
  </td>
  <td class="table-text">
      {{ $value }}
  </td>
</tr>
@endforeach
</tbody>
</table>
