@extends('layouts.app')
@section('content')
<div class="container">
  <br>
  <div class="col-lg-8 offset-lg-2 col-md-6 offset-md-3 col-sm-6 offset-sm-3">
  <div class="card ">
      <div class="card-header">
        @lang('messages.dataset')
        @if(isset($dataset))
          : <strong>{{$dataset->name}}</strong>
        @endif
        @can('update', $dataset)
          <span class="history" style="float:right">
            <a href="{{url('datasets/'.$dataset->id.'/activity')}}">
            @lang ('messages.see_history')
            </a>
          </span>
        @endcan
      </div>
      @php
        $lockimage= '<i class="fas fa-lock"></i>';
        $license_logo = 'images/cc_srr_primary.png';
        if ($dataset->privacy >= App\Models\Dataset::PRIVACY_REGISTERED) {
          $lockimage= '<i class="fas fa-lock-open"></i>';
          $license = explode(" ",$dataset->license);
          $license_logo = 'images/'.mb_strtolower($license[0]).".png";
        }
      @endphp
      <div class="card-body">
        <h4>
          {{$dataset->title}}
        </h4>

        @if (isset($dataset->description))
        <p>
          {{ $dataset->description }}
        </p>
        @endif

        @if ($dataset->tags->count())
        <br>
        <p>
          <strong>
            @lang('messages.tagged_with')
          </strong>:
          {!! $dataset->tagLinks !!}
        </p>
        @endif


        <br>
        <span style='float:right;'>
          <h2>
            {{ $dataset->downloads }}
          </h2>
          <small>Downloads</small>
        </span>

        <p>
          <a href="http://creativecommons.org/license" target="_blank">
            <img src="{{ asset($license_logo) }}" alt="{{ $dataset->license }}" width='100px'>
          </a>
          {!! $lockimage !!} @lang('levels.privacy.'.$dataset->privacy)
        </p>


        @if (count($dataset->data_type)>0)
        <p>
          @can('export', $dataset)
            <a href="{{ url('datasets/'.$dataset->id.'/download') }}" class="btn btn-success">
              <i class="fa-solid fa-download"></i>&nbsp;
              @lang('messages.data_download')
            </a>
          @else
            @if ($dataset->privacy < App\Models\Dataset::PRIVACY_REGISTERED)
              <a href="{{ url('datasets/'.$dataset->id.'/request') }}" class="btn btn-warning">
                <i class="fa-solid fa-download"></i>&nbsp;
                @lang('messages.data_request')
              </a>
            @else
              <a href="{{ route('login') }}" class="btn btn-warning">
                <i class="fa-solid fa-triangle-exclamation"></i>&nbsp;
                @lang('messages.download_login')
              </a>
            @endif
          @endcan
        </p>
        @endif

        @if (isset($dataset->citation))
          <br>
          <p>
            <strong>@lang('messages.howtocite')</strong>:
            <br>
            {!! $dataset->citation !!}  <a data-bs-toggle="collapse" href="#bibtex" class="btn-sm btn-primary">BibTeX</a>
          </p>
          <div id='bibtex' class='collapse'>
            <pre class='mt-3 bg-lightgray'><code >{{ $dataset->bibtex }}</code></pre>
          </div>
        @endif

        @if ($dataset->policy)
          <br>
          <p>
            <strong>
              @lang('messages.data_policy')
            </strong>:
            <br>
            {{$dataset->policy}}
          </p>
        @endif
        <br>


  <div class="row g-3 p-2" id='odb-delete-confirmation-form' style="display: none;">
    <div class="col-auto alert alert-danger">
      @lang('messages.everything_will_be_deleted')
      <br>
      @lang('messages.action_not_undone')
      <form action="{{ url('datasets/'. $dataset->id)  }}" method="POST" >
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <button type="submit" class="btn btn-danger btn-sm mt-3" title="@lang('messages.remove')">
          <i class="far fa-trash-alt"></i>
          @lang('messages.remove')
        </button>
      </form>
    </div>

  </div>

<!-- BUTTONS -->
<nav class="navbar navbar-light navbar-expand-lg" id="buttons-navbar">
  <div class="container-fluid">
    <div class="navbar-header">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarbuttons" aria-controls="navbarbuttons" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  </div>
    <div class="navbar-collapse collapse flex-column ml-lg-0 ml-3" id="navbarbuttons">
      <br>
        <ul class="nav navbar-nav flex-row">

  @can ('update', $dataset)
  <li class="nav-item px-2 mb-2">
    <a href="{{ url('datasets/'. $dataset->id. '/edit')  }}" class="btn btn-success" name="submit" value="submit">
      <i class="fa fa-btn fa-plus"></i>
      @lang('messages.edit')
    </a>
  </li>
  <!---
  <li class="nav-item px-2 mb-2">
    <a href="{{ Route('dataset.versioning',[ 'dataset'=> $dataset->id ] ) }}" 
    class="btn btn-success" 
    data-toggle="tooltip" title="Publish a version">
    <i class="fa-solid fa-code-branch"></i>
    </a>
  </li>
  --->
  @endcan
  @can ('delete', $dataset)
    <li class="nav-item px-2 mb-2">
      <button class="odb-delete-confirmation-button btn btn-danger" type='button' >
        @lang('messages.remove')
      </button>
    </li>
  @endcan
  <li class="nav-item px-2 mb-2">
    <button id='dataset_summary_button' type="button" type="button" class="btn btn-outline-secondary">
      <span id='dataset_summary_loading' style="display: none;">
        <i class="fa-solid fa-spinner fa-spin"></i>
      </span>
      @lang('messages.dataset_summary')
    </button>
  </li>

  <li class="nav-item px-2 mb-2">
  <button id='identifications_summary_button' type="button" class="btn btn-outline-secondary">
    <span id='identifications_summary_loading' style="display: none;"><i class="fa-solid fa-spinner fa-spin"></i></span>
    @lang('messages.taxonomic_summary')
  </button>
  </li>

  <li class="nav-item px-2 mb-2">
  <button type="button" class="btn btn-outline-secondary btntoogle" data='dataset_references' >
   <i class="fas fa-book-open"></i>
   @lang('messages.references')
 </button>
  </li>

  <li class="nav-item px-2 mb-2">
    <a data-bs-toggle="collapse" href="#dataset_people" class="btn btn-outline-secondary">@lang('messages.persons')</a>
  </li>

  <li class="nav-item px-2 mb-2">
  <a href="{{ url('measurements/'. $dataset->id. '/dataset')  }}" class="btn btn-outline-secondary" name="submit" value="submit">
        <i class="fa fa-btn fa-search"></i>
        @lang('messages.measurements')
  </a>
  </li>
  <li class="nav-item px-2 mb-2">
    <a href="{{ url('individuals/'. $dataset->id. '/dataset')  }}" class="btn btn-outline-secondary" name="submit" value="submit">
        <i class="fa fa-btn fa-search"></i>
        @lang('messages.individuals')
      </a>
  </li>
  <li class="nav-item px-2 mb-2">
  <a href="{{ url('vouchers/'. $dataset->id. '/dataset')  }}" class="btn btn-outline-secondary" name="submit" value="submit">
        <i class="fa fa-btn fa-search"></i>
        @lang('messages.vouchers')
  </a>
</li>
<li class="nav-item px-2 mb-2">
  <a href="{{ url('media/'. $dataset->id. '/datasets')  }}" class="btn btn-outline-secondary" name="submit" value="submit">
        <i class="fa fa-btn fa-search"></i>
        @lang('messages.media_files')
  </a>
</li>
<li class="nav-item px-2 mb-2">
    <a href="{{ url('locations/'. $dataset->id. '/dataset')  }}" class="btn btn-outline-secondary" name="submit" value="submit">
          <i class="fa fa-btn fa-search"></i>
          @lang('messages.locations')
    </a>
  </li>
  <li class="nav-item px-2 mb-2">
    <a href="{{ url('taxons/'. $dataset->id. '/dataset')  }}" class="btn btn-outline-secondary" name="submit" value="submit">
          <i class="fa fa-btn fa-search"></i>
          @lang('messages.taxons')
    </a>
  </li>



</ul>
</div>
</div>
</nav>


</div>


</div>

<!-- START dataset people block -->
<!-- START people  BLOCK -->
<div class="collapse" id="dataset_people">
<br>
<div class="card">
    <div class="card-header">
        @lang('messages.persons')
    </div>

    <div class="card-body">
<table class="table table-striped user-table">
<thead>
  <th>@lang('messages.email')</th>
  <th>@lang('messages.name')</th>
  <th>@lang('messages.role')</th>
</thead>

<tbody>
@foreach($dataset->people as $role => $list)
@foreach($list as $member)
<tr>
<td class="table-text">
    {{ $member[0] }}
</td>
<td class="table-text">
    {{ $member[1] }}
</td>
<td class="table-text">
    {{ $role }}
</td>
</tr>
@endforeach
@endforeach
</tbody>
</table>
</div>
</div>
</div>

<!-- START dataset summary BLOCK -->
<div id='dataset_summary_block'></div>


<!-- IDENTIFICATIONS SUMMARY PANEL DO NOT CHANGE THIS LINE  MAY BREAK jquery below-->
<div id='identifications_summary_block'></div>

    <!-- END dataset summary BLOCK -->

<!-- start REFERENCE BLOCK -->
<div id='dataset_references'>
    <br>
    <div class="card">
    <div class="card-header">
      @lang('messages.dataset_bibreference')
    </div>
    <div class="card-body">
    {!! View::make('common.bibreferences')->with([
        'bibreferences' => isset($dataset->references) ? $dataset->references : null,
    ]) !!}
    </div>

  </div>

</div>
<!-- END REFERENCE BLOCK -->


</div>


</div>


</div>
@endsection


@once
  @push ('scripts')    
  @vite('resources/assets/js/custom.js')

<script type='module'>

$(document).ready(function() {

/*hide on load */
$("#dataset_summary_block").hide();
$("#identifications_summary_block").hide();
$("#dataset_references").hide();

/* generic function to close and focus on page elements */
$('.btntoogle').on('click',function() {
    var selected = $(this);
    var element = selected.attr('data');
    var container = $('.container');
    if ($('#'+element).is(":hidden")) {
      $('.hiddeninfo').hide();
      $('#'+element).show();
      var scrollTo = $('#'+element);
      var position = scrollTo.offset().top;
      window.scrollTo(null,position);
    } else {
      $('#'+element).hide();
      window.scrollTo(null,container.offset().top);
    }
});


 /* summarize identifications */
 function summarizeIdentifications() {
  return $.get("{{ route('dataset_identification_summary',$dataset->id) }}");
 }



$('#identifications_summary_button').on('click', function(e){
  if ($('#identifications_summary_block').is(':empty')){
    $('#identifications_summary_loading').show();
    $('.hiddeninfo').hide();
    //e.preventDefault();
    summarizeIdentifications().done(function(data) {
          $('#identifications_summary_block').show();
          $('#identifications_summary_block').html(data);
          $('#identifications_summary_loading').hide();

      }).fail(function(data) {
          $('#identifications_summary_block').show();
          $('#identifications_summary_block').html("NOT FOUND");
          $('#identifications_summary_loading').hide();
      });
      var scrollTo = $('#identifications_summary_block');
      var position = scrollTo.offset().top;
      window.scrollTo(null,position);
  } else {
    if ($('#identifications_summary_block').is(':visible')) {
      $('#identifications_summary_block').hide();
    } else {
      //$('.hiddeninfo').hide();
      $('#identifications_summary_block').show();
      var scrollTo = $('#identifications_summary_block');
      var position = scrollTo.offset().top;
      window.scrollTo(null,position);
    }
  }
});


$('#dataset_summary_button').on('click', function(e){
  if ($('#dataset_summary_block').is(':empty')){
    $('#dataset_summary_loading').show();
    $('.hiddeninfo').hide();
    $.ajax({
      type: 'GET',
      url: "{{ route('dataset_summary',$dataset->id) }}",
      success: function(data) {
          $('#dataset_summary_block').html(data);
          $('#dataset_summary_block').show();
          $('#dataset_summary_loading').hide();
      },
      error: function(data) {
          $('#dataset_summary_block').html("Nothing to display");
          $('#dataset_summary_block').show();
          $('#dataset_summary_loading').hide();
      }
    });
  } else {
    if ($('#dataset_summary_block').is(':visible')) {
      $('#dataset_summary_block').hide();
    } else {
      $('.hiddeninfo').hide();
      $('#dataset_summary_block').show();
    }
  }
});


});

</script>
@endpush
@endonce