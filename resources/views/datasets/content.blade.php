<table class="table table-striped user-table">
<thead>
  <th>@lang('messages.object')</th>
  <th>@lang('messages.total')</th>
  <th>@lang('messages.direct_linked')</th>
</thead>
<tbody>
<tr>
  <td class="table-text">
      <strong>@lang('messages.individuals')</strong>
  </td>
  <td class="table-text">
    {{ count($dataset->all_individuals_ids()) }}
  </td>
  <td class="table-text">
    {{ $dataset->individualsCount() }}
  </td>

</tr>

<tr>
  <td class="table-text">
      <strong>@lang('messages.locations')</strong>
  </td>
  <td class="table-text">
    {{ count($dataset->all_locations_ids()) }}
  </td>
  <td class="table-text"></td>
</tr>
<tr>
  <td class="table-text">
      <strong>@lang('messages.measurements')</strong>
  </td>
  <td class="table-text">
  </td>
  <td class="table-text">
    {{ $dataset->measurementsCount() }}
  </td>
</tr>
<tr>
  <td class="table-text">
      <strong>@lang('messages.media_files')</strong>
  </td>
  <td class="table-text">
    {{ count($dataset->all_media_ids()) }}
  </td>
  <td class="table-text">
    {{ $dataset->mediaCount() }}
  </td>
</tr>
<tr>
  <td class="table-text">
      <strong>@lang('messages.taxons')</strong>
  </td>
  <td class="table-text">
    {{ count($dataset->all_taxons_ids()) }}
  </td>
  <td class="table-text"></td>
</tr>
<tr>
  <td class="table-text">
      <strong>@lang('messages.vouchers')</strong>
  </td>
  <td class="table-text">
    {{ count($dataset->all_voucher_ids()) }}
  </td>
  <td class="table-text">
    {{ $dataset->vouchersCount() }}
  </td>
</tr>
</tbody>
</table>
