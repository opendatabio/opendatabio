@extends('layouts.app')

@section('content')
    <div class="container">
      <br>
      <div class="col-lg-8 offset-lg-2 col-md-6 offset-md-3 col-sm-6 offset-sm-3">
      <div class="card ">
            <div class="card-header">
              @lang('messages.dataset')
              @if(isset($dataset))
                : <strong>{{$dataset->name}}</strong>
              @endif
            </div>
            @php
              $lockimage= '<i class="fas fa-lock"></i>';
              if ($dataset->privacy != App\Models\Dataset::PRIVACY_AUTH) {
                $license = explode(" ",$dataset->license);
                $license_logo = 'images/'.mb_strtolower($license[0]).".png";
              } else {
                $license_logo = 'images/cc_srr_primary.png';
              }
              if ($dataset->privacy == App\Models\Dataset::PRIVACY_PUBLIC) {
                $lockimage= '<i class="fas fa-lock-open"></i>';
              }
            @endphp
            <div class="card-body">
              <h3>
                {{$dataset->title}}
              </h3>
              @if (isset($dataset->description))
                <p>
                  {{ $dataset->description }}
                </p>
              @endif
              <!--
              <br>
              <p>
                <a href="http://creativecommons.org/license" target="_blank">
                  <img src="{{ asset($license_logo) }}" alt="{{ $dataset->license }}" width=100>
                </a>
                {!! $lockimage !!} @lang('levels.privacy.'.$dataset->privacy)
              </p>
              -->

              @if ($dataset->policy)
                <br>
                <p>
                  <strong>
                    @lang('messages.data_policy')
                  </strong>:
                  <br>
                  {{$dataset->policy}}
                </p>
                <br>
              @endif
              <p>
                <strong>
                  @lang('messages.admins')</strong>:
                  <ul>
                    @foreach ($dataset->people['admins'] as $admin)
                      <li> {{ $admin[0] }} </li>
                    @endforeach
                  </ul>
              </p>

@if($dataset->references->where('mandatory',1)->count())
  <p>
<strong>
  @lang('messages.dataset_bibreferences_mandatory')
</strong>:
<ul>
  @foreach($dataset->references->where('mandatory',1) as $reference)
    <li>
    <a href='{{ url('references/'.$reference->bib_reference_id)}}'>
      {!!$reference->first_author.". ".$reference->year !!}
    </a>
    {!! $reference->title !!}
  </li>
  @endforeach
</ul>
</p>
@endif


</div>
</div>


<!--- DATASET REQUEST PANEL -->
<div class="card mt-3" id='dataset_request_panel' >
  <div class="card-header">
    @lang('messages.data_request')
    &nbsp;&nbsp;
    <a data-bs-toggle="collapse" href="#hint_dataset_resquest" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="hint_dataset_resquest" class="odb-hint collapse">
    @lang('messages.dataset_request_hint')
  </div>
  </div>
  <div class="card-body">

  <form class="row g-2" action="{{ url('datasets/'.$dataset->id.'/emailrequest')}}" method="POST" id='dataset_request_form'>
  <!-- csrf protection -->
    {{ csrf_field() }}
    <!-- hidden field to store selected rows not really in use now as single datasets only are implemented-->
    <input type='hidden' name='dataset_list' id='dataset_list' value="">

@if (!Auth::user())
  <div class="mb-3">
    <label class="form-label mandatory">
      @lang('messages.email')
    </label>
    <div class="col-sm-7">
      <input type="email" autocomplete="email" name="email" value="" >
    </div>
  </div>
@endif


<!--
<div class="mb-3">
  <label class="form-label mandatory">
    @lang('messages.dataset_request_use')
  </label>
  <br>
  <input class="form-check-input" id='dataset_use_type1' type="radio" name="dataset_use_type" value="@lang('messages.dataset_request_use_exploratory')" required>
  <label class="form-check-label" for="dataset_use_type1">
    @lang('messages.dataset_request_use_exploratory')
  </label>
  <br>
  <input class="form-check-input" id='dataset_use_type2' type="radio" name="dataset_use_type" value="@lang('messages.dataset_request_use_inpublications')" required>
  <label class="form-check-label" for="dataset_use_type2">
    @lang('messages.dataset_request_use_inpublications')
  </label>
</div>
-->

<div class="mb-3">
  <label class="form-label mandatory" for='dataset_use_description'>
    @lang('messages.description')
  </label>
  <a data-bs-toggle="collapse" href="#hint6" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <br>
  <textarea name="dataset_use_description" id='dataset_use_description' class='form-control' required></textarea>
  <div id="hint6" class="odb-hint collapse">
     @lang('messages.dataset_request_use_description_hint')
   </div>
 </div>

     <!---
     <div class="form-group" hidden>
       <label class="col-sm-3 control-label mandatory">
         @lang('messages.dataset_request_agreement')
       </label>
       <div class="col-sm-7">
         @lang('messages.dataset_request_agreement_text')
         <br><br>
         <input type="checkbox" name="dataset_agreement[]"  value="@lang('messages.dataset_request_distribution_agreement')" required >&nbsp;@lang('messages.dataset_request_distribution_agreement')
         <br>
         @if($dataset->policy)
         <input type="checkbox" name="dataset_agreement[]"  value="@lang('messages.dataset_request_policy_agreement')" required >&nbsp;@lang('messages.dataset_request_policy_agreement')
          @endif
          <br>
          @if($dataset->references->where('mandatory',1)->count())
          <input type="checkbox" name="dataset_agreement[]"  value="@lang('messages.dataset_request_citation_agreement')" required >&nbsp;@lang('messages.dataset_request_citation_agreement')
          @endif
       </div>
      </div>
      -->
			<div class="mb-3 col-sm-offset-3 col-sm-6">
        <span id='submitting'><i class="fas fa-sync fa-spin"></i></span>
        <input id='submit' class="btn btn-success" name="submit" type='submit' value='@lang('messages.submit')' >
      </div>
</form>

</div>
</div>
<!-- END DATASET REQUEST FORM -->


</div>
</div>
@endsection
@once
  @push ('scripts')    
<script type='module'>
$(document).ready(function() {
  $('#submitting').hide();
  /* summarize project */
  $('#submit').on('click', function(e){
        $('#submitting').show();
      });
});
</script>
@endpush
@endonce
