@extends('layouts.app')
@section('content')
<div id='contents' class="container">
      <br>
      <div class="card">
          <div class="card-header">
            @lang('messages.datasets')
            @if(isset($object))
              @lang('messages.for')
              <strong>
              {{ class_basename($object) }}
              </strong>
              {!! $object->rawLink() !!}
            @endif
            <a data-bs-toggle="collapse" href="#data_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
            @if ($mydatasets)
                &nbsp;&nbsp;
                <a data-bs-toggle="collapse" href="#mydatasets" class="btn btn-outline-secondary">@lang('messages.my_datasets')</a>
            @endif
            <div id="data_hint" class="card-body odb-hint collapse">
                @lang('messages.dataset_hint')
            </div>
          </div>

          @if ($mydatasets)
          <div id="mydatasets" class="card-body collapse">
            <br>
            <ul>
              @foreach ($mydatasets as $dataset)
                <li>{!! $dataset->rawLink() !!}
                  (@lang('levels.project.' . $dataset->pivot->access_level ))
                </li>
              @endforeach
            </ul>
          </div>
          @endif
          <form action="{{url('datasets/map')}}" method="POST" class="form-horizontal" id='odb-map-occurrences'>
            <!-- csrf protection -->
            {{ csrf_field() }}
            <input type="hidden" name="idstomap" value="" id='odb-map-selected-rows'>
          </form>
          <div class="card-body">
              {!! $dataTable->table([],true) !!}

          </div>
        </div>
        <!-- </div> -->
</div>
@endsection

@push ('scripts')

{{ $dataTable->scripts(attributes: ['type' => 'module']) }}



<script type="module">
$(document).ready(function() {
  //this fixes a misalignment of datatable body and header
  //when scrollX is true and on large devices

  var table = $('#dataTableBuilder').DataTable();
  $('tbody').on('click', 'tr',function (e) {
      var cell=$(e.target).closest('td');
      if( cell.index()>0){
      //e.stopPropagation();
      //console.log( table.row( this ).data() );
      var idi =  table.row( this ).data().id;
      var id = 'description_'+idi;
      if($('#'+id).is(':hidden')) {
        $('#'+id).show();
        $('#'+idi+'description_brief').hide();
      } else {
        $('#'+id).hide();
        $('#'+idi+'description_brief').show();      }
      }
  });
  $('.testeselected').on('click', function() {
      var tab = $('#dataTableBuilder').DataTable();
      //var data = tab.rows('.selected').ids().toArray();
      var selectedRows = tab.rows('.selected').data();
      var rowIds = selectedRows.pluck('id').toArray();
      console.log(rowIds);
      //console.log( data );
  });

});



</script>
@vite('resources/assets/js/custom.js')

@endpush
