<div id="taxonomic_summary" class='card px-0 mt-3' x-data="{ open: true }">
<div class="card-header">
    <strong>
      @lang('messages.taxonomic_summary')
    </strong>  
    &nbsp;
    <button type="button" class="btn btn-sm" @click="open = ! open">
        <i class="far fa-eye-slash fa-lg" x-show="open"></i>
        <i class="fa-regular fa-eye fa-lg" x-show="!open"></i>
    </button>  
  </div>
@if($show)
  <div class='card-body' x-show="open">
   @if($taxonomic_summary)
    <strong>@lang('messages.taxonomic_counts')</strong>:
    <p>
    <ul>
    @foreach($taxonomic_summary as $key => $count)
      <li>
        <strong>@lang('messages.'.$key)</strong>
        :
        {{ $count }}
      </li>
     @endforeach
    </ul>
    </p>
  @endif
   @if(count($identification_summary)>0)
    <br>
    <strong>@lang('messages.identification_level')</strong>:
    <p>
    <table class='table table-striped user-table'>
      <thead>
        <th>@lang('messages.taxon_level')</th>
        <th>@lang('messages.unpublished')</th>
        <th>@lang('messages.published')</th>
        <th>@lang('messages.total')</th>
      </thead>
      <tbody>
        @foreach($identification_summary as $detsummary)
        <tr>
            <td class='table-text'>
                @lang('levels.tax.'.$detsummary->level)
            </td>
            <td class='table-text'>
                {{ $detsummary->unpublished }}
            </td>
            <td class='table-text'>
                {{ $detsummary->published }}
            </td>
            <td class='table-text'>
                {{ $detsummary->total }}
            </td>
        </tr>
      @endforeach
      </tbody>
    </table>
    </p>
    @endif
</div>
@else
<div class="card-body">
  <div class="alert alert-danger">
    @lang('messages.nothing_to_show')
  </div>
</div>
@endif
</div>
