@extends('layouts.app')

@section('content')
<div class="container">
  <br>
  <div class="col-lg-10 offset-lg-1 col-md-6 offset-md-3 mb-5">
      <div class="card">
        <div class="card-header">
          <h4>@lang ('customs.landing_page_welcome')</h4>
        </div>
        <div class="card-body">
          <div class="col-sm-12">
          <p>@lang('messages.welcome_summary',compact('nindividuals', 'nvouchers','ndatasets','nprojects','nmeasurements'))</p>
        </div>
        <dl>

        <dt class='dt-odb'>@lang ('messages.your_default_person')</dt>
        @if (Auth::user()->person)
          <dd>{!! Auth::user()->person->rawLink() !!}</dd>
        @else
          <dd class='text-danger'>@lang ('messages.no_default_person')</dd>
        @endif
        <dt class="dt-odb">
          @lang('messages.api_token')
        </dt>
        <dd>
          <span class="large"> {{ Auth::user()->api_token }} </span>
        </dd>

          <dt class='dt-odb'>@lang ('messages.default_project')</dt>
        @if (Auth::user()->defaultProject)
          <dd>{!! Auth::user()->defaultProject->rawLink() !!}</dd>
        @else
          <dd class='text-danger'>@lang('messages.no_default_project')</dd>
        @endif
          <dt class='dt-odb'>@lang ('messages.default_dataset')</dt>
        @if (Auth::user()->defaultDataset)
          <dd>{!! Auth::user()->defaultDataset->rawLink() !!}</dd>
          @else
            <dd class='text-danger'>@lang('messages.no_default_dataset')</dd>
        @endif
        </dl>
        <br>

        <!-- BUTTONS -->
        <nav class="navbar navbar-light navbar-expand-lg">
          <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarbuttons" aria-controls="navbarbuttons" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarbuttons">
              <br>
              <ul class="navbar-nav mb-lg-0 g-3">
                <li class="nav-item px-2 mb-2">
                  <a href="{{ route('selfedit') }}" class="btn btn-success">
                    @lang('messages.edit_profile')
                  </a>
                </li>

                @if (Auth::user()->projects()->count())
                <li class="nav-item px-2 mb-2">
            <a data-bs-toggle="collapse" href="#myprojects" class="btn btn-outline-secondary">@lang('messages.my_projects')</a>
          </li>
          @endif

          @if (Auth::user()->datasets()->count())
            <li class="nav-item px-2 mb-2">
            <a data-bs-toggle="collapse" href="#mydatasets" class="btn btn-outline-secondary">@lang('messages.my_datasets')</a>
          </li>
          @endif

          @if (Auth::user()->forms()->count())
            <li class="nav-item px-2 mb-2">
            <a data-bs-toggle="collapse" href="#myforms" class="btn btn-outline-secondary">@lang('messages.forms')</a>
            </li>
          @endif

          @if(Auth::user())
            <li class="nav-item px-2 mb-2">
              <a href="{{ url('token') }}" class="btn btn-outline-secondary">
                @lang('messages.api_token')
              </a>
            </li>

            <li class="nav-item px-2 mb-2">
            <a href="{{ route('userjobs.index') }}" class="btn btn-outline-secondary">
              @lang('messages.userjobs')</a>
            </li>

          <li class="nav-item px-2 mb-2">
            <a href="{{ url('odbrequests/'. Auth::user()->id. '/user')  }}" class="btn btn-outline-secondary" name="submit" value="submit">
              <i class="fa fa-btn fa-search"></i>
              @lang('messages.request_user')
            </a>
          </li>
          @endif
        </ul>
      </div>
    </div>
  </nav>
    </div>

    <div class="card collapse" id='myprojects'>
      @if (Auth::user()->projects()->count())
        <div class="card-header">
          @lang('messages.my_projects')
        </div>
        <div class="card-body">
            <ul>
              @foreach (Auth::user()->projects as $project)
                <li>{!! $project->rawLink() !!} (@lang('levels.project.' . $project->pivot->access_level ))</li>
              @endforeach
            </ul>
        </div>
      @endif
    </div>

    <div class="card collapse" id='mydatasets'>
      @if (Auth::user()->datasets()->count())
      <div class="card-header">
        @lang('messages.datasets')
      </div>
      <div class="card-body">
        <ul>
          @foreach (Auth::user()->datasets as $dataset)
            <li>{!! $dataset->rawLink() !!}  (@lang('levels.project.' . $dataset->pivot->access_level ))</li>
          @endforeach
        </ul>
      </div>
      @endif
    </div>
    <div class="card collapse" id='myforms'>
      @if (Auth::user()->forms()->count())
      <div class="card-header">
        @lang('messages.forms')
      </div>
      <div class="card-body">
          <ul>
            @foreach (Auth::user()->forms as $form)
              <li>{!! $form->rawLink() !!}</li>
            @endforeach
          </ul>
      </div>
      @endif
    </div>
    </div>
</div>
</div>

@endsection
