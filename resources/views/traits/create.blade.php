@extends('layouts.app')

@section('content')
    <div class="container">
      <br>
      <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
          <div class="card-header">
        @lang('messages.new_trait')
        &nbsp;&nbsp;
        <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        <div id="help" class="odb-hint collapse">
          <hr>
          @lang('messages.hint_trait_create')
        </div>
  </div>

     <div class="card-body">

@if (isset($odbtrait))
		    <form action="{{ url('traits/' . $odbtrait->id)}}" method="POST" class="row g-2">
{{ method_field('PUT') }}

@else
		    <form action="{{ url('traits')}}" method="POST" class="row g-2">
@endif
		     {{ csrf_field() }}
<?php
// TODO: should be moved to be PSR compliant


function genInputTranslationTable($odbtrait, $type, $language, $order) {
  $txt  = "<div class='col-sm-5 mb-3'>";
  $id = 'cat_'.$type.'['. $order .']['. $language->id .']';
  switch($type) {
  case "name":
      $text = "<label for='".$id."' class='form-label mandatory'>".Lang::get("messages.name",[],$language->code)."</label>";
      $text .= "<input class='form-control'  id='".$id."' name='".$id."' value='";
      if (is_numeric($order)) {
          if (isset($odbtrait)) {
              $cat = $odbtrait->categories->where('rank', $order)->first();
              if($cat) {
                 $get_old = $cat->translate(\App\Models\UserTranslation::NAME, $language->id);
              }
        }
        $text .= old('cat_' . $type .'.' . $order . '.' . $language->id, isset($get_old) ? $get_old : null);
      }
      $text .= "' >";
      break;
    case "description":
      $text = "<label for='".$id."' class='form-label'>".Lang::get("messages.description",[],$language->code)."</label>";
      $text .= "<textarea class='form-control'  name='".$id."' rows='2' >";
      if (is_numeric($order)) {
          if (isset($odbtrait)) {
              $cat = $odbtrait->categories->where('rank', $order)->first();
              if($cat) {
                $get_old = $cat->translate(\App\Models\UserTranslation::DESCRIPTION, $language->id);
              }
          }
          $text .= old('cat_' . $type .'.' . $order . '.' . $language->id, isset($get_old) ? $get_old : null);
      }
      $text .= "</textarea>";
      break;
    }
  return $txt.$text."</div>";
}

// call this function with order = int to use OLD values, order = null produces a blank category (for use in js)
function genTraitCategoryTranslationTable($order, $odbtrait) {
    if (is_null($order)) $order = "__PLACEHOLDER__";
    $TH = "<div><h6>#<span class='odb-order'>".$order."</span></h6>";
    $TB = '';
    $languages = \App\Models\Language::all();
    $first = true;
    foreach ($languages as $language) {
        $line = "<div class='row'><div class='col-sm-2 form-label align-middle'><em>".$language->name."</em></div>";
        //if ($first) {
        //  $line .= '<div class="col-sm-2 align-middle">'. $order . "</div>";
        //}
        $line .= genInputTranslationTable($odbtrait, "name", $language, $order);
        $line .= genInputTranslationTable($odbtrait, "description", $language, $order);
        $line .= "</div>";
        $TB .= $line;
        $first = false;
    }
    $TF = "</div><hr>";
    return $TH . $TB . $TF;
}
?>

<!-- TRAIT NAME AND DESCRIPTION TRANSLATIONS -->
<div class="mb-3">
  @foreach ($languages as $language)
  <div class='row'>
    <div class="col-sm-2 form-label align-middle">
      <em>{{$language->name}}</em>
    </div>
    <div class='col-sm-5 mb-3'>
      <label for="name{{$language->id}}" class='form-label mandatory'>@lang("messages.name",[],$language->code)</label>
      <input id="name{{$language->id}}"  class='form-control' name="name[{{$language->id}}]" value="{{ old('name.' . $language->id, isset($odbtrait) ? $odbtrait->translate(\App\Models\UserTranslation::NAME, $language->id) : null) }}" >
    </div>
    <div class='col-sm-5 mb-3'>
      <label for="description{{$language->id}}" class='form-label'>@lang("messages.description",[],$language->code)</label>
      <textarea id="description{{$language->id}}"  class='form-control' name="description[{{$language->id}}]">{{ old('description.' . $language->id, isset($odbtrait) ? $odbtrait->translate(\App\Models\UserTranslation::DESCRIPTION, $language->id) : null) }}</textarea>
    </div>
    </div>
    <hr>
  @endforeach
</div>

<div class="mb-3">
    <label for="export_name" class="form-label mandatory">
        @lang('messages.export_name')
    </label>
    <a data-bs-toggle="collapse" href="#trait_export_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	  <div class="">
        <input name="export_name" value="{{ old('export_name', isset($odbtrait) ? $odbtrait->export_name : null) }}"
    class="form-control">
      </div>
    <div id="trait_export_hint" class="odb-hint collapse">
	@lang('messages.trait_export_hint')
    </div>
</div>


<div class="mb-3">
    <label for="type" class="form-label mandatory">
@lang('messages.type')
</label>
<a data-bs-toggle="collapse" href="#trait_type_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div class="">
	@php
      $selected = old('type', isset($odbtrait) ? $odbtrait->type : null);
      $canchange = isset($odbtrait) ? $odbtrait->measurements()->count()==0 : true;
      $trait_types  = \App\Models\ODBTrait::TRAIT_TYPES;
      $trait_categorical_types = [\App\Models\ODBTrait::CATEGORICAL, \App\Models\ODBTrait::CATEGORICAL_MULTIPLE, \App\Models\ODBTrait::ORDINAL];
      if (isset($odbtrait) and false == $canchange and in_array($odbtrait->type,$trait_categorical_types)) {
            $trait_types = $trait_categorical_types;
            $canchange = true;
      }
  @endphp
  @if($canchange)
     <select name="type" id="type" class="form-select" >
  @else
     <input name='type' value={{ $selected }} hidden>
	   <select id="type" class="form-control" disabled>
  @endif
    @foreach ($trait_types as $ttype)
      <option value="{{ $ttype }}" {{ $ttype == $selected ? 'selected' : '' }}>
        @lang('levels.traittype.' . $ttype)
       </option>
      @endforeach
     </select>
  </div>
  <div id="trait_type_hint" class="odb-hint collapse">
	@lang('messages.trait_type_hint')
    </div>
</div>


<div class="mb-3">
    <label for="objects" class="form-label mandatory">
@lang('messages.object_types')
</label>
<a data-bs-toggle="collapse" href="#trait_objects_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div class="">
{!! Multiselect::select(
    'objects',
    \App\Models\ODBTrait::getObjectTypeNames(),
    isset($odbtrait) ? $odbtrait->getObjectKeys() : [],
    ['class' => 'multiselect form-select']
) !!}
  </div>
    <div id="trait_objects_hint" class="odb-hint collapse">
	@lang('messages.trait_objects_hint')
    </div>
</div>

<div class="mb-3 trait-number trait-spectral">
    <label for="unit" class="form-label mandatory">
        @lang('messages.unit')
      </label>
        <a data-bs-toggle="collapse" href="#hint_trait_unit" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div class="">
            @if($canchange)
              <input type="text" name="unit" id="unit" class="form-control" value="{{ old('unit', isset($odbtrait) ? $odbtrait->unit : null) }}" >
            @else
            <input type="text"  id="unit" class="form-control" value="{{ old('unit', isset($odbtrait) ? $odbtrait->unit : null) }}" disabled>
              <input type="hidden"  name="unit" value="{{ old('unit', isset($odbtrait) ? $odbtrait->unit : null) }}" >
            @endif

            </div>
    <div id="hint_trait_unit" class="odb-hint collapse">
@lang('messages.hint_trait_unit')
    </div>
</div>





<div class="mb-3 trait-number">
    <label for="range_min" class="form-label">
@lang('messages.range_min')
</label>
        <a data-bs-toggle="collapse" href="#hint_trait_min" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div class="">
	<input type="text" name="range_min" id="range_min" class="form-control" value="{{ old('range_min', isset($odbtrait) ? $odbtrait->range_min : null) }}">
            </div>
    <div id="hint_trait_min" class="odb-hint collapse">
@lang('messages.hint_trait_min')
    </div>
</div>


<div class="mb-3 trait-number">
    <label for="range_max" class="form-label">
@lang('messages.range_max')
</label>
        <a data-bs-toggle="collapse" href="#hint_trait_max" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div class="">
	<input type="text" name="range_max" id="range_max" class="form-control" value="{{ old('range_max', isset($odbtrait) ? $odbtrait->range_max : null) }}">
            </div>
    <div id="hint_trait_max" class="odb-hint collapse">
@lang('messages.hint_trait_max')
    </div>
</div>


<div class="mb-3 trait-spectral">
    <label for="range_min" class="form-label mandatory">
@lang('messages.wavenumber_start') <em>cm<sup>-1</sup></em>
</label>
        <a data-bs-toggle="collapse" href="#hint_wavenumber_start" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div class="">
        @if($canchange)
          <input type="text" name="range_min" id="range_min" class="form-control" value="{{ old('range_min', isset($odbtrait) ? $odbtrait->range_min : null) }}" >
        @else
          <input type='hidden' name="range_min" value={{  $odbtrait->range_min }} >
          <input type="text" id="range_min" class="form-control" value="{{ old('range_min', isset($odbtrait) ? $odbtrait->range_min : null) }}"  disabled >
        @endif
     	</div>
    <div id="hint_wavenumber_start" class="odb-hint collapse">
@lang('messages.hint_wavenumber_start')
    </div>
</div>

<div class="mb-3 trait-spectral">
    <label for="range_max" class="form-label mandatory">
@lang('messages.wavenumber_end') <em>cm<sup>-1</sup></em>
</label>
        <a data-bs-toggle="collapse" href="#wavenumber_end" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div class="">
        @if($canchange)
          <input type="text" name="range_max" id="range_max" class="form-control" value="{{ old('range_max', isset($odbtrait) ? $odbtrait->range_max : null) }}" >
        @else
          <input type='hidden' name="range_max" value={{  $odbtrait->range_max }} >
          <input type="text" id="range_max" class="form-control" value="{{ old('range_max', isset($odbtrait) ? $odbtrait->range_max : null) }}"  disabled >
        @endif
      </div>
    <div id="wavenumber_end" class="odb-hint collapse">
@lang('messages.hint_wavenumber_end')
    </div>
</div>

<div class="mb-3 trait-spectral">
    <label for="value_length" class="form-label mandatory">
@lang('messages.wavenumber_step')
</label>
        <a data-bs-toggle="collapse" href="#wavenumber_step" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div class="">
      @if($canchange)
     	  <input type="text" name="value_length" id="value_length" class="form-control" value="{{ old('value_length', isset($odbtrait) ? $odbtrait->value_length : null) }}" >
      @else
        <input type='hidden' name="value_length" value={{  $odbtrait->value_length }} >
        <input type="text" id="value_length" class="form-control" value="{{ old('value_length', isset($odbtrait) ? $odbtrait->value_length : null) }}" disabled >
      @endif
      </div>
    <div id="wavenumber_step" class="odb-hint collapse">
@lang('messages.hint_wavenumber_step')
    </div>
</div>

<div class="mb-3 trait-category group-together">
<span class='align-center form-label mandatory'>@lang('messages.categories') </span>
<a data-bs-toggle="collapse" href="#hint_categories" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
<div id="hint_categories" class="odb-hint collapse">
@lang('messages.hint_categories')
</div>
<br><br>
<div class="col-sm-12" id="to_append_categories">
<?php
if (isset($odbtrait)) {
    // do we have "old" input?
    if (empty(old()) or empty(old("cat_name"))) {
        //fill with old data
        foreach($odbtrait->categories as $category) {
            echo genTraitCategoryTranslationTable($category->rank, $odbtrait);
        }
    } else {
        //restore from input
        foreach(array_keys(old("cat_name")) as $rank) {
            echo genTraitCategoryTranslationTable($rank, $odbtrait);
        }
    }
} else { // no odbtrait, so we're creating a new
    // do we have "old" input?
    if (empty(old()) or empty(old("cat_name"))) {
        foreach([1,2] as $rank) {
            echo genTraitCategoryTranslationTable($rank, null);
        }
    } else {
        foreach(array_keys(old("cat_name")) as $rank) {
            echo genTraitCategoryTranslationTable($rank, null);
        }
    }
}
?>
</div>

<div class="col-sm-12">
				<button type="submit" class="btn btn-outline-secondary" id="add_category">
				    <i class="glyphicon glyphicon-plus"></i>
            @lang('messages.add_category')
				</button>

</div>
</div>


<div class="mb-3 trait-link">
    <label for="link_type" class="form-label mandatory">
@lang('messages.link_type')
</label>
        <a data-bs-toggle="collapse" href="#hintlt" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	    <div class="">
	<?php $selected = old('type', isset($odbtrait) ? $odbtrait->link_type : null); ?>
	<select name="link_type" id="link_type" class="form-select"
  @if (!$canchange)
    'disabled'
  @endif
  >
	@foreach (\App\Models\ODBTrait::LINK_TYPES as $ttype)
		<option value="{{ $ttype }}" {{ $ttype == $selected ? 'selected' : '' }}>
@lang('classes.' . $ttype)
		</option>
	@endforeach
	</select>
            </div>
  <div id="hintlt" class="odb-hint collapse">
@lang('messages.hint_trait_link_type')
    </div>
</div>




<div class="mb-3">
  <label for="bibreference_id" class="form-label">
    @lang('messages.trait_bibreference')
  </label>
  <a data-bs-toggle="collapse" href="#trait_bibreference_hint" class="odb-unstyle">
    <i class="far fa-question-circle"></i>
  </a>
  <div class="">
    <input type="text" name="bibreference_autocomplete" id="bibreference_autocomplete" class="form-control autocomplete"
    value="{{ old('bibreference_autocomplete', (isset($odbtrait) and $odbtrait->bibreference) ? $odbtrait->bibreference->bibkey : null) }}">
    <input type="hidden" name="bibreference_id" id="bibreference_id"
    value="{{ old('bibreference_id', isset($odbtrait) ? $odbtrait->bibreference_id : null) }}">
</div>
    <div id="trait_bibreference_hint" class="odb-hint collapse">
	@lang('messages.trait_bibreference_hint')
    </div>
</div>

<!-- notes -->
<div class="mb-3">
    <label for="notes" class="form-label">
@lang('messages.notes')
</label>
  <textarea name="notes" id="notes" class="form-control">{{ old('notes', isset($odbtrait) ? $odbtrait->notes : null) }}</textarea>
</div>


<div class="mb-3">
    <label for="parentTrait" class="form-label">
@lang('messages.parent_trait')
</label>
<a data-bs-toggle="collapse" href="#parent_trait_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div >
    <input type="text" name="trait_autocomplete" id="trait_autocomplete" class="form-control autocomplete" value="{{ old('trait_autocomplete', isset($odbtrait) ? ($odbtrait->parentTrait ? $odbtrait->parentTrait->export_name : null) : null) }}">
    <input type="hidden" name="parent_trait_id" id="parent_trait_id" value="{{ old('parent_trait_id', isset($odbtrait) ? $odbtrait->parent_id : null) }}">
  </div>
  <div id="parent_trait_hint" class="odb-hint collapse">
	     @lang('messages.parent_trait_hint')
  </div>  
</div>



<div class="mb-3">
    <button type="submit" class="btn btn-success" name="submit" value="submit">
      <i class="fa fa-btn fa-plus"></i>
      @lang('messages.add')
    </button>
    <a href="{{url()->previous()}}" class="btn btn-warning">
      <i class="fa fa-btn fa-plus"></i>
      @lang('messages.back')
    </a>
</div>




</form>
  </div>
</div>
</div>
@endsection

@once
@push ('scripts')
@vite('resources/assets/js/custom.js')

<script type='module'>
$(document).ready(function() {
  $("#bibreference_autocomplete").odbAutocomplete("{{url('references/autocomplete')}}","#bibreference_id", "@lang('messages.noresults')");

  $("#trait_autocomplete").odbAutocomplete("{{url('traits/autocomplete')}}","#parent_id", "@lang('messages.noresults')");

	function setFields(vel) {
		var adm = $('#type option:selected').val();
		if ("undefined" === typeof adm) {
			return; // nothing to do here...
		}
		switch (adm) {
			case "0": // numeric
			case "1": // numeric FALL THROUGH
				$(".trait-category").hide(vel);
				$(".trait-link").hide(vel);
        $(".trait-spectral").hide(vel);
        $(".trait-number").show(vel);
				break;
			case "2": // categories
			case "3": // categories FALL THROUGH
				$(".trait-number").hide(vel);
				$(".table-ordinal").hide(vel);
				$(".trait-link").hide(vel);
        $(".trait-spectral").hide(vel);
        $(".trait-category").show(vel);
				break;
			case "4": // ordinal
				$(".trait-number").hide(vel);
				$(".trait-link").hide(vel);
        $(".trait-spectral").hide(vel);
        $(".trait-category").show(vel);
				$(".table-ordinal").show(vel);
				break;
      case "7": // link
				$(".trait-number").hide(vel);
				$(".trait-category").hide(vel);
				$(".table-ordinal").hide(vel);
        $(".trait-spectral").hide(vel);
        $(".trait-link").show(vel);
        break;
      case "8": // spectral
        $(".trait-number").hide(vel);
        $(".trait-category").hide(vel);
        $(".table-ordinal").hide(vel);
        $(".trait-link").hide(vel);
        $(".trait-spectral").show(vel);
        break;
			default: // other
				$(".trait-number").hide(vel);
				$(".trait-category").hide(vel);
				$(".trait-link").hide(vel);
        $(".trait-spectral").hide(vel);
		}
	}
	$("#type").change(function() { setFields(400); });
    // trigger this on page load
	setFields(0);


    $("#add_category").click(function(event) {
        event.preventDefault();
        var text = "{!! genTraitCategoryTranslationTable(null, null,null); !!}";
        // infers the number of categories already displayed by the number of table-ordinal headers
        var newcat = $('.odb-order').length + 1;
        text = text.replace(/__PLACEHOLDER__/g, newcat);
        $('#to_append_categories').append(text);
        setFields(0);
    });
});
</script>
@endpush
@endonce