@extends('layouts.app')

@section('content')
    <div class="container">
      <br>
      <div class="card col-lg-8 offset-lg-2 col-md-6 offset-md-3">
            <div class="card-header">
              @lang('messages.trait')
              &nbsp;
              <em>{{ $odbtrait->export_name }}</em>
              @if(Auth::user())
                    <span class="history" style="float:right">
                    <a class="history" href="{{url('traits/'.$odbtrait->id.'/activity')}}">
                      @lang ('messages.see_history')
                      </a>
                    </span>
                    @endif
          </div>
		<div class="card-body">
      <dl class="">
        <dt class="dt-odb">
@lang('messages.name')
</dt>
<dd class="mb-3">
{{ $odbtrait->name }}
</dd>
<dt class="dt-odb">
@lang('messages.id')
</dt>
<dd class="mb-3">
{{ $odbtrait->id }}
</dd>
<dt class="dt-odb">
@lang('messages.description')
</dt>
<dd class="mb-3">
{{ $odbtrait->description }}
</dd>
<dt class="dt-odb">
@lang('messages.type')
</dt>
<dd class="mb-3">
@lang ('levels.traittype.' . $odbtrait->type)
</dd>
<dt class="dt-odb">
@lang('messages.export_name')
</dt>
<dd class="mb-3">
{{ $odbtrait->export_name }}
</dd>

@if ($odbtrait->object_types)
<dt class="dt-odb">
    @lang('messages.object_types')
  </dt>
  <dd class="mb-3">
<ul>
@foreach ($odbtrait->object_types as $obtype)
    <li>{{ $obtype->name }}</li>
@endforeach
</ul>
</dd>

@endif

@if ($odbtrait->bibreferences->count())
<dt class="dt-odb">
@lang('messages.bibreference')
</dt>
<dd class="mb-3">
  {!! $odbtrait->bibreference_links !!}
</dd>
@endif

@if ($odbtrait->tags->count())
<dt class="dt-odb">
  @lang('messages.tagged_with')
</dt>
<dd class="mb-3">
  {!! $odbtrait->tag_links !!}
</dd>
@endif


@if ($odbtrait->parent_id)
<dt class="dt-odb">
@lang('messages.parent_trait')
</dt>
<dd class="mb-3">
{!! $odbtrait->parentTrait->rawLink() !!}
</dd>
@endif

@if ($odbtrait->dependentTraits()->count())
<dt class='dt-odb'>
@lang('messages.dependent_traits')
</dt>
<dd class='mb-3'>
  @foreach($odbtrait->dependentTraits as $dp)
    &nbsp;&nbsp;- {!! $dp->rawLink() !!} <br>
  @endforeach
</dd>
@endif

@if ( in_array( $odbtrait->type, [\App\Models\ODBTrait::QUANT_INTEGER, \App\Models\ODBTrait::QUANT_REAL]))
    @if ($odbtrait->trait_unit_id)
    <dt class="dt-odb">
    @lang('messages.unit')
  </dt>
  <dd class="mb-3">
    {{ $odbtrait->trait_unit->display_unit }}
  </dd>
    @endif
    @if ($odbtrait->range_min or $odbtrait->range_max)
    <dt class="dt-odb">
    @lang('messages.range')
  </dt>
  <dd class="mb-3">

    {!! $odbtrait->rangeDisplay !!}
  </dd>
    @endif
@endif

@if ( in_array( $odbtrait->type, [\App\Models\ODBTrait::SPECTRAL]))
    @if ($odbtrait->range_min or $odbtrait->range_max or $odbtrait->value_length)
    <dt class="dt-odb">
    @lang('messages.wavenumber_start') cm<sup>-1</sup>
  </dt>
  <dd class="mb-3">
    {{ $odbtrait->range_min }}
  </dd>
    <dt class="dt-odb">
    @lang('messages.wavenumber_end') cm<sup>-1</sup>
  </dt>
  <dd class="mb-3">

    {{ $odbtrait->range_max }}
  </dd>
    <dt class="dt-odb">
    @lang('messages.wavenumber_step')
  </dt>
  <dd class="mb-3">
    {{ $odbtrait->value_length }}
  </dd>
    @endif
@endif

@if ( in_array( $odbtrait->type, [\App\Models\ODBTrait::CATEGORICAL, \App\Models\ODBTrait::CATEGORICAL_MULTIPLE, \App\Models\ODBTrait::ORDINAL]) and $odbtrait->categories)
<dt class="dt-odb">@lang('messages.categories')</dt>
<dd class="mb-4">
<table class="table table-striped" style='font-size: 0.9em;'> <thead>
  @if ($odbtrait->type == \App\Models\ODBTrait::ORDINAL)
      <th>@lang('messages.rank')</th>
  @else
      <th></th>
  @endif
 <th>
   @lang('messages.name')
  </th>
  <th>
   @lang('messages.description')
  </th>
</thead>
<tbody>
@foreach ($odbtrait->categories as $cat)
<tr>
    <td> {{$cat->rank}}</td>
    <td> {{$cat->name}}</td>
    <td> {{$cat->description}}</td>
</tr>
@endforeach
</tbody>
</table>
</dd>
@endif

@if ($odbtrait->type == \App\Models\ODBTrait::LINK)
<dt class="dt-odb">
@lang('messages.link_type')
</dt>
<dd class="mb-3">
@lang('classes.' . $odbtrait->link_type)
</dd>
@endif

<div class='mt-3 mb-5'>
@if ($odbtrait->notes)
  {!! $odbtrait->formated_notes !!}
@endif
</div>

</dl>

<div class="row g-3 p-2" id='odb-delete-confirmation-form' style="display: none;">
  <p class="col-auto alert alert-danger">
    @lang('messages.action_not_undone')
  </p>
  <form class='col-auto' action="{{ url('traits/'. $odbtrait->id)  }}" method="POST" >
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
    <button type="submit" class="btn btn-danger mt-3" title="@lang('messages.remove')">
      <i class="far fa-trash-alt"></i>
      @lang('messages.remove')
    </button>
  </form>
</div>

<div class='row g-3' >
  @can ('update', $odbtrait)
  			    <div class="col-auto px-2">
  				<a href="{{ route('trait.form',[
            'odbtrait' => $odbtrait->id,
            ] )}}" class="btn btn-success" name="submit" value="submit">
  @lang('messages.edit')
  				</a>
  			    </div>
  @endcan

  @can ('delete', $odbtrait)
  <div class="col-auto px-2">
    <button class="odb-delete-confirmation-button btn btn-danger" type='button' >
      @lang('messages.remove')
    </button>
  </div>
  @endcan

  <div class="col-auto px-2">
    <a href="{{ url('measurements/'. $odbtrait->id. '/trait')  }}" class="btn btn-outline-secondary" name="submit" value="submit">
      <i class="fa fa-btn fa-search"></i>
      @lang('messages.measurements')
    </a>
</div>




</div>
                </div>
            </div>
    </div>
@endsection

@once
  @push ('scripts')
  @vite('resources/assets/js/custom.js')
  @endpush
@endonce