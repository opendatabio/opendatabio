@if (!isset($index))
<label for="value" class="form-label mandatory">
@lang('messages.value')
</label>
<button type="button" class="btn btn-primary" id="check-gb" data-toggle="tooltip" title="@lang('messages.genebank_search')">
  <div class="spinner" id="gb-spinner"></div>
  <i class="fa fa-btn fa-search"></i>
  <i class="fas fa-dna"></i>
</button>
<div class="">
@endif
<input name ='value{{ isset($index) ? "[$index][$traitorder]" : "" }}' id='value{{ isset($index) ? "[$index][$traitorder]" : "" }}' type="text" class="form-control" value="{{
    isset($index) ?
    old('value.' . $index . '.' . $traitorder, isset($measurement) ? $measurement->valueActual : null) :
    old('value', isset($measurement) ? $measurement->valueActual : null)
}}"
@if (isset($index) and isset($measurement))
    disabled
@endif
>
@if (isset($index) and isset($measurement))
<span style="float:right">
    <a href="{{url('measurements/' . $measurement->id . '/edit')}}" target="_blank">
            @lang('messages.edit')
        <i class="glyphicon glyphicon-new-window"></i>
    </a>
</span>
@endif
<em>@lang('messages.genebank_accession')</em><br/>

@if (!isset($index))
</div>
<div class="alert alert-warning" id="gb-status" hidden> </div>
@endif
