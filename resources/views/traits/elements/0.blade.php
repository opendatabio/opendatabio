@if (!isset($index))
<label for="value" class="form-label mandatory">
@lang('messages.value')
</label>
<div class="">
@endif
<input name ='value{{ isset($index) ? "[$index][$traitorder]" : "" }}' id='value{{ isset($index) ? "[$index][$traitorder]" : "" }}' type="text" class="form-control" value="{{
    isset($index) ?
    old('value.' . $index . '.' . $traitorder, isset($measurement) ? $measurement->valueActual : null) :
    old('value', isset($measurement) ? $measurement->valueActual : null)
}}"
@if (isset($index) and isset($measurement))
    disabled
@endif
>
@if (isset($index) and isset($measurement))
<span style="float:right">
    <a href="{{url('measurements/' . $measurement->id . '/edit')}}" target="_blank">
            @lang('messages.edit')
        <i class="glyphicon glyphicon-new-window"></i>
    </a>
</span>
@endif
<div class="odb-hint">
@if (isset($odbtrait->unit))
<em>@lang('messages.unit'): {{ $odbtrait->unit }} </em>
@endif
@if (isset($odbtrait->range_min) or isset($odbtrait->range_max))
&nbsp;&nbsp;
<em>@lang('messages.range'): {!! $odbtrait->rangeDisplay !!} </em>
@endif
</div>

@if (!isset($index))
</div>
@endif
