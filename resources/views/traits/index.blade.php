@extends('layouts.app')

@section('content')
<div id='contents' class="container">
  <br>
  <div class="col-md-12 offset-md-0">
      <div class="card">
          <div class="card-header">
              @lang('messages.traits')
              @if(isset($object))
                @lang('messages.for'):&nbsp;<strong>@lang('messages.unit') 
                &nbsp;{{ $object->unit }}&nbsp;-&nbsp;{{ $object->name }}
                </strong>
              @else 
                &nbsp;&nbsp;      
                <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
                <div id="help" class="odb-hint collapse">
                  @lang('messages.traits_hint')
                </div>
              @endif
          </div>
          @if (Auth::user())
            {!! View::make('common.exportdata')->with([
                  'object' => isset($object) ? $object : null,
                  'export_what' => 'trait',
                  'filters' => isset($job_id) ? ['job_id'=> $job_id] : null,
            ]) !!}
          @else
            <br>
          @endif
          @if (Auth::user())
          <!--- delete form -->
          {!! View::make('common.batchdelete')->with([
                'url' => url('traits/batch_delete'),
          ]) !!}
          <br>
          @endif
          <div class="card-body">
          {!! $dataTable->table([],true) !!}
          </div>
      </div>
</div>
</div>

@endsection
@push ('scripts')
@push('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}

    <script type="module">
    $(document).ready(function() {
    const table = $('#dataTableBuilder');
    table.on('preXhr.dt',function(e,settings,data) {
      data.type = $("#trait_type option").filter(':selected').val();
      /* an any defined in the export form */
      //data.project = $("input[name='project']").val();
      //data.location = $("input[name='location_root']").val();
      //data.taxon = $("input[name='taxon_root']").val();
      /*console.log(data.level,data.project,data.location,data.taxon); */
    });
    $('#trait_type').on('change',function() {
       table.DataTable().ajax.reload();
       return false;
    });
  });
</script>
@vite('resources/assets/js/custom.js')

@endpush
