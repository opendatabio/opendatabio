@extends('layouts.app')

@section('content')
<div id='contents' class="container">
            <br>
            <div class="card">
                <div class="card-header">
                        @lang('messages.projects')
                        &nbsp;&nbsp;
                        <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>

                        @if ($myprojects)
                          &nbsp;&nbsp;
                          <a data-bs-toggle="collapse" href="#myprojects" class="btn btn-outline-secondary">@lang('messages.my_projects')</a>
                        @endif
                        <div id="help" class="odb-hint collapse">
                          @lang('messages.projects_hint')
                        </div>
                  </div>
                  @if ($myprojects)
                  <div class="card-body collapse" id='myprojects'>
                    <br>
                    <ul>
                      @foreach ($myprojects as $project)
                        <li>
                          {!! $project->rawLink() !!}
                          (
                            @lang('levels.project.' . $project->pivot->access_level )
                          )
                        </li>
                      @endforeach
                    </ul>
                  </div>
                  @endif
                  <div class="card-body">
                    {!! $dataTable->table([],true) !!}
                  </div>
              </div>
    </div>
@endsection

@once
  @push ('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
    @vite('resources/assets/js/custom.js')
  @endpush
@endonce
