 <!-- BUTTONS -->
 <nav class="navbar navbar-light navbar-expand-lg" id="buttons-navbar">
            <div class="container-fluid">
              <div class="navbar-header">
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarbuttons" aria-controls="navbarbuttons" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
            </div>
              <div class="navbar-collapse collapse flex-column ml-lg-0 ml-3" id="navbarbuttons">
                <br>
                  <ul class="nav navbar-nav flex-row">

            @can ('update', $project)
            <li class="nav-item px-2 mb-2">
              <a href="{{ url('projects/'. $project->id. '/edit')  }}" class="btn btn-success" name="submit" value="submit">
                @lang('messages.edit')
              </a>
            </li>
            @endcan

            @can ('delete', $project)
              <li class="nav-item px-2 mb-2">
                <button class="odb-delete-confirmation-button btn btn-danger" type='button' >
                  @lang('messages.remove')
                </button>
              </li>
            @endcan

            @can('view_details',$project)
              @if(isset($project->details))
              <li class="nav-item px-2 mb-2">
                <a data-bs-toggle="collapse" href="#project_details" class="btn btn-outline-secondary">@lang('messages.project_details')</a>
              </li>
              @endif
            @endcan
              <!--
              <li class="nav-item px-2 mb-2">
                <button id='summary_button' type="button" class="btn btn-outline-secondary">
                  <span id='summary_loading' hidden><i class="fas fa-sync fa-spin"></i></span>
                  @lang('messages.summary')
                </button>
              </li>
              <li class="nav-item px-2 mb-2">
                  <a data-bs-toggle="collapse" href="#project_people" class="btn btn-outline-secondary">@lang('messages.persons')</a>
              </li>
              -->
              
              <li class="nav-item px-2 mb-2">
                  <a href="{{ url('datasets/'. $project->id. '/project')  }}" class="btn btn-outline-secondary">
                    <i class="fa fa-btn fa-search"></i>
                    @lang('messages.datasets')
                  </a>
              </li>

              <li class="nav-item px-2 mb-2">
                <a href="{{ url('individuals/'. $project->id. '/project')  }}" class="btn btn-outline-secondary">
                  <i class="fa fa-btn fa-search"></i>
                  @lang('messages.individuals')
                </a>
              </li>

              <li class="nav-item px-2 mb-2">
                <a href="{{ url('vouchers/'. $project->id. '/project')  }}" class="btn btn-outline-secondary">
                  <i class="fa fa-btn fa-search"></i>
                  @lang('messages.vouchers')
                </a>
              </li>

              <li class="nav-item px-2 mb-2">
                <a href="{{ url('taxons/'. $project->id. '/project')  }}" class="btn btn-outline-secondary">
                  <i class="fa fa-btn fa-search"></i>
                  @lang('messages.taxons')
                </a>
              </li>
              <li class="nav-item px-2 mb-2">
                <a href="{{ url('locations/'. $project->id. '/project')  }}" class="btn btn-outline-secondary">
                  <i class="fa fa-btn fa-search"></i>
                  @lang('messages.locations')
                </a>
              </li>
            </ul>
            </div>
            </div>
</nav>