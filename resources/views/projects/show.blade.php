@extends('layouts.app')

@section('content')
<div class="container">
  <br>
  <div class="col-lg-8 offset-lg-2 col-md-6 offset-md-3 col-sm-6 offset-sm-3">
  <div class="card ">
      <div class="card-header">
        @lang('messages.project')
        <strong>
          {{ $project->name }}
        </strong>
        <span class="history" style="float:right">
        <a class="history" href="{{url('projects/'.$project->id.'/activity')}}">
        @lang ('messages.see_history')
        </a>
        </span>
      </div>
      <div class="card-body">
        <div class="row">

          <div class="col-sm-9">
            <h5>
              {{ isset($project->title) ? $project->title : $project->name }}
            </h5>
            <!-- short description -->
            @if (isset($project->description))
            <br>
            {{ $project->description }}
            <br>
          @endif
          </div>
          @if($project->logo_url))
          <div class="col">
            <img src='{{ url($project->logo_url) }}'  width="150px;" alt="...">
          </div>
          @endif
         

        </div>
        @if ($project->tags()->count())
        <br>
        <dl>
          <dt class='dt-odb'>@lang('messages.tagged_with')</dt>
          <dd>
          {!! $project->tagLinks !!}
          </dd>
        <dl>
        @endif

        <p class="collapse" id='project_details'>
            {!! $project->details !!}
        </p>

        <br>

        @can ('delete', $project)
        <div class="row g-3 p-2" id='odb-delete-confirmation-form' style="display: none;">
          <p class="col-auto alert alert-danger">
            @lang('messages.action_not_undone')
          </p>
          <form class='col-auto' action="{{ url('projects/'. $project->id)  }}" method="POST" >
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger mt-3" title="@lang('messages.remove')">
              <i class="far fa-trash-alt"></i>
              @lang('messages.remove')
            </button>
          </form>
        </div>
        @endcan



        {!! View::make('projects.navbar')->with([
        'project' => $project,
          ]) !!}

</div>
</div>

<!-- START people  BLOCK -->
<div class="hiddeninfo collapse mt-3" id='project_people'>
  <div class="card ">
    <div class="card-header">
        @lang('messages.persons')
    </div>
    <div class="card-body">
  <table class="table table-striped user-table">
  <thead>
    <th>@lang('messages.email')</th>
    <th>@lang('messages.name')</th>
    <th>@lang('messages.role')</th>
  </thead>
  <tbody>
  @foreach($project->people as $role => $list)
  @foreach($list as $member)
  <tr>
  <td class="table-text">
      {{ $member[0] }}
  </td>
  <td class="table-text">
      {{ $member[1] }}
  </td>
  <td class="table-text">
      {{ $role }}
  </td>
  </tr>
  @endforeach
  @endforeach
</tbody>
</table>
</div>
</div>
</div>


<!-- START summary BLOCK -->
<div class="hiddeninfo" id='project_summary' ></div>
<!-- end -->

</div>
</div>
@endsection


@once
@push ('scripts')
@vite('resources/assets/js/custom.js')

<script type='module'>

$(document).ready(function() {
  $('#project_summary').hide();

  /* summarize project */
  $('#summary_button').on('click', function(e){
      if ($('#project_summary').is(':empty')){
        $('#summary_loading').show();
        e.preventDefault();
        $.ajax({
          type: 'POST',
          url: "{{ route('project_summary',$project->id) }}",
          data: {
            "id" : "{{ $project->id }}",
            "_token" : "{{ csrf_token() }}"
          },
          success: function(data) {
              $('#project_summary').html(data);
              $('#project_summary').show();
              $('#summary_loading').hide();
          },
          error: function(data) {
              $('#project_summary').html("NOT FOUND");
              $('#project_summary').show();
              $('#summary_loading').hide();
          }
        });
      } else {
        if ($('#project_summary').is(':visible')) {
          $('#project_summary').hide();
        } else {
          $('#project_summary').show();
        }
      }
  });

});

</script>
@endpush
@endonce
