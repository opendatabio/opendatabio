@extends('layouts.app')
@section('content')
<div class='container'>
<div class="card mt-5" style='min-width: 60vw; max-width: 90vw; '>
          <div class="card-header">
            @if(!isset($project))
		          @lang('messages.new_project')
            @else
              @lang('messages.editing') @lang('messages.project') <strong>id# {{ $project->id }}</strong>
            @endif

          </div>

          <div class="card-body">
@if (isset($project))
		    <form class="row g-2" action="{{ url('projects/' . $project->id)}}" method="POST" enctype="multipart/form-data">
{{ method_field('PUT') }}

@else
		    <form class="row g-2"  action="{{ url('projects')}}" method="POST" enctype="multipart/form-data">
@endif
		     {{ csrf_field() }}

<div class="row mb-3">
  <div class='col'>
    <label for="acronym" class="form-label mandatory">
      @lang('messages.name')
    </label>
    <a data-bs-toggle="collapse" href="#project_name_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="project_name_hint" class="odb-hint collapse">
         @lang('messages.project_name_hint')
    </div>
    <div class="">
	     <input type="text" name="acronym" id="acronym" class="form-control" value="{{ old('acronym', isset($project) ? $project->acronym : null) }}">
    </div>
    
  </div>
</div>

@foreach ($languages as $language)
    <div class='row'>      
      <div class='col-sm-6 mb-3'>
        <label for="title_{{$language->id}}" class='form-label mandatory  '>@lang("messages.title") {{$language->name}}</label>        
        <a data-bs-toggle="collapse" href="#project_title_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        <div>
        <input class='form-control' name="title[{{$language->id}}]" value="{{ old('title.' . $language->id, isset($project) ? $project->name_locale($language->code) : null) }}" >
        @error('titles') <span class="text-danger">{{ $message }}</span> @enderror
        </div>
        <div id="project_title_hint" class="odb-hint collapse">
         @lang('messages.project_title_hint')
       </div>
      </div>
      <div class='col-sm-6 mb-3'>
        <label for="description_{{$language->id}}" class='form-label'>@lang("messages.dataset_short_description") {{$language->name}}</label>
        <a data-bs-toggle="collapse" href="#dataset_short_description" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        <div class="">
          <textarea class='form-control' name="description[{{$language->id}}]">{{ old('description.' . $language->id, isset($project) ? $project->description_locale($language->code) : null) }}</textarea>
          @error('descriptions') <span class="text-danger">{{ $message }}</span> @enderror
          </div>
          <div id="dataset_short_description" class="odb-hint collapse">
            @lang('messages.project_short_description_hint')
          </div>
      </div>
    </div>
@endforeach



@foreach ($languages as $language)
<div class="row mb-3">
  <div class="col">
    <label for="pages_{{$language->code}}" class="form-label ">
      @lang('messages.project_details') - {{$language->name}}
    </label>
    <a data-bs-toggle="collapse" href="#dataset_details" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <div id="dataset_details" class="odb-hint collapse">
      @lang('messages.project_details_hint')
     </div>

    </div> 
    <div class="">
    <textarea class='form-control' name="pages[{{$language->code}}]">{{ old('details.' . $language->code, (isset($project) ? (isset($project->pages[$language->code]) ? $project->pages[$language->code] : null) : null)) }}</textarea>
    </div>
  
</div>
@endforeach

<div class="row mb-5">
  
  <div class="col-6">
    <label for="logo" class="form-label">
      @lang('messages.logo')
    </label>
    <a data-bs-toggle="collapse" href="#logo_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
    <input type="file" name="logo" class='form-control' >
    <div id="logo_hint" class="odb-hint collapse col-12">
      @lang('messages.logo_project_hint')
    </div>
  </div>
  <div class="col-3">
    @if(isset($project) and $project->logoUrl)
        <img src='{{ $project->logoUrl }}' width='80'>
    @endif   
  </div>
</div>

<div class="row mb-5">
       
  <div class="col-6">
    <label for="banner" class="form-label">
        @lang('messages.banner')
    </label>
    <a data-bs-toggle="collapse" href="#banner_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>    
    <input type="file" name="banner" class='form-control' >
    <div id="banner_hint" class="odb-hint collapse">
      @lang('messages.banner_project_hint')
    </div>
  </div>
  <div class="col-3">
    @if(isset($project) and $project->bannerUrl)
      <img src='{{ $project->bannerUrl }}' height='100px'>
    @endif
  </div>   
</div>

<div class="row mb-5">        
  <div class="col">
    <label for="footerimgs" class="form-label">
        @lang('messages.footerimg')
    </label>
    <a data-bs-toggle="collapse" href="#footerimg_hint" class="odb-unstyle">
      <i class="far fa-question-circle"></i></a>    
    <div id="footerimg_hint" class="odb-hint collapse">
      @lang('messages.footerimg_project_hint')
    </div>  
    <input type="file" name="footerimgs[]" class='form-control'  multiple>    
  </div>  
</div>  
@if(isset($project))
  @php
    $exists = $project->media()->where('file_name','like','%footerimg%')->get();
  @endphp
  @if($exists->count())
  <div class="row mb-5">        
        @foreach($exists as $img) 
        <div class="col-auto mx-2">        
          <img src='{{  $img->getUrl() }}' height='100px'>
          <br>
          <input type="checkbox" name='deletefooterimgs[{{$img->id}}]' >&nbsp;<i class="far fa-trash-alt text-danger"></i>
        </div>  
        @endforeach      
  </div>
  @endif
@endif

<div class="row mb-5">
  <div>
  <label for="tags" class="form-label ">
      @lang('messages.tags')
  </label>
  <a data-bs-toggle="collapse" href="#tags_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div id="tags_hint" class="odb-hint collapse">
    @lang('messages.tags_hint')
  </div>
  </div>
  <div class="" style='max-width: 400px;'>
    {!! Multiselect::select(
        'tags',
        $tags->pluck('name', 'id'), isset($project) ? $project->tags->pluck('id') : [],
         ['class' => 'multiselect form-select']
    ) !!}
  </div>
    
</div>


<div class="row mb-5">
  <div>
  <label for="urls" class="form-label ">
      @lang('messages.urls')
  </label> 
  </div>
  <div class="" >
    <table class='table table-striped'>
      <tr>
        <th>URL</th>
        <th>{{__('messages.title') }}</th>
        <th>{{__('messages.type') }}</th>
      </tr>
      @if(isset($project) and $project->urls)
        @foreach($project->urls as $key => $theurl)
            <tr>                  
                <td>
                <input type="url" name="urls[{{$key}}][url]" class='form-control' value="{{$theurl['url']}}">                    
                </td>
                <td>
                <input type="text" name="urls[{{$key}}][label]" class='form-control' value="{{$theurl['label']}}">                    
                </td>
                <td>
                <select name="urls[{{$key}}][icon]" class='form-control'>
                  @foreach($url_icons as $key => $icon)                  
                    <option value="{{$icon['class']}}" "{{$theurl["icon"]==$icon['class'] ? 'selected' : ''}}" >{{$icon['name']}}</option>                  
                  @endforeach                    
                </select>                  
                </td>
            </tr>
        @endforeach  
        @php 
          $key = count($project->urls);
        @endphp
      @else 
        @php 
          $key = 0;
        @endphp
      @endif
        
        <tr>        
        <td>
          <input type="url" name="urls[{{$key}}][url]"  class='form-control'>
        </td>
        <td>
          <input type="text" name="urls[{{$key}}][label]"  class='form-control'>
        </td>
        <td>                                    
          <select name="urls[{{$key}}][icon]" class='form-control'>
            @foreach($url_icons as $key => $icon)                  
              <option value="{{$icon['class']}}" >{{$icon['name']}}</option>                  
            @endforeach                    
          </select>
        </td>
      </tr>   
      </table>          
  </div>
</div>  

<div class="row mb-3">
  <div>
  <label for="admins" class="form-label mandatory">
  @lang('messages.admins')
  </label>
  <a data-bs-toggle="collapse" href="#hint2" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
  <div id="hint2" class="odb-hint collapse">
   @lang('messages.project_admins_hint')
 </div>
 </div>
  <div class="">
    {!! Multiselect::autocomplete(
        'admins',
        $fullusers->pluck('email', 'id'), isset($project) ? $project->admins->pluck('id') : [Auth::user()->id],
        ['class' => 'multiselect form-control']
    ) !!}
</div>
</div>

<div class="row mb-3">
  <div class="col">
  <label for="collabs" class="form-label">
  @lang('messages.collabs')
  </label>
  <div class="">
  {!! Multiselect::autocomplete(
      'collabs',
      $fullusers->pluck('email', 'id'), isset($project) ? $project->collabs->pluck('id') : [],
      ['class' => 'multiselect form-control']
  ) !!}
  </div>
  </div>
</div>

<div class="row mb-3 g-3">
  <div class='col-auto'>
    <button type="submit" class="btn btn-success" name="submit" value="submit">
      <i class="fa fa-btn fa-plus"></i>
      @lang('messages.add')
    </button>
  </div>
  <div class='col-auto'>
    <a href="{{url()->previous()}}" class="btn btn-warning">
      <i class="fa fa-btn fa-plus"></i>
      @lang('messages.back')
    </a>
  </div> 
</div>


        </form>
        </div>
        </div>
        @endsection

@once
@push ('scripts')
@vite('resources/assets/js/custom.js')

<script type='module'>
$(document).ready(function() {

/* show or hide elements depending on type of privacy */
$('#privacy').on('change',function() {
    var privacy = $('#privacy option:selected').val();
    if (privacy == {{ App\Models\Dataset::PRIVACY_AUTH}}) {
        $('#licenselabel').removeClass('mandatory');

        //$('#titlelabel').removeClass('mandatory');
        //$('#authorslabel').removeClass('mandatory');
    } else {
        //$('#authorslabel').addClass('mandatory');
        $('#licenselabel').addClass('mandatory');
        //$('#titlelabel').addClass('mandatory');
    }
});


/* if license is different than public domain, title and authors must be informed */
/* and sui generis database policies may be included */
$('#license').on('change',function() {
    var license = $('#license option:selected').val();
    if (license == "CC0") {
        $('#titlelabel').removeClass('mandatory');
        //$('#authorslabel').removeClass('mandatory');
        $('#policy').val(null);
        $('#show_policy').hide();
    } else {
        //$('#authorslabel').addClass('mandatory');
        $('#titlelabel').addClass('mandatory');
        $('#show_policy').show();
    }
});


});

</script>
{!! Multiselect::scripts('admins', url('users/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
{!! Multiselect::scripts('collabs', url('users/autocomplete'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
@endpush
@endonce
