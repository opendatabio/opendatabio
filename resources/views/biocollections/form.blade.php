
<div class="mb-3">
    <label for="acronym" class="form-label mandatory">
      @lang('messages.acronym')
    </label>
    <a data-bs-toggle="collapse" href="#hint1" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
	   <div class="">
	      <input type="text" name="acronym" id="acronym" class="form-control" value="{{ isset($biocollection) ? $biocollection->acronym : old('acronym'); }}">
      </div>
      <div id="hint1" class="odb-hint collapse">
	       @lang ('messages.hint_biocollections')
  </div>
</div>
<div class="mb-3">
    <label for="name" class="form-label mandatory">
@lang('messages.institution')
</label>
    <div class="">
    @php
    $readonly = "readonly";
    if (isset($biocollection)) {
      $readonly = "";
    }
    @endphp
	<input type="text" name="name" id="name" class="form-control" value="{{ isset($biocollection) ? $biocollection->name : old('name'); }}" "{{ $readonly }}">
    </div>
</div>
<div class="mb-3">
    <label for="irn" class="form-label mandatory">
@lang('messages.irn')
</label>
	    <div class="">
	<input type="text" name="irn" id="irn" class="form-control" value="{{ isset($biocollection) ? $biocollection->irn : old('irn'); }}" "{{ $readonly }}">
    </div>
</div>
