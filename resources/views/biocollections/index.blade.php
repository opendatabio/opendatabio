@extends('layouts.app')

@section('content')
<div id='contents' class="container">
<br>
<div class="card">
    <div class="card-header">
              @lang('messages.registered_biocollections')
              &nbsp;&nbsp;
              <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
              <div id="help" class="odb-hint collapse">
                @lang('messages.hint_biocollections_page')
              </div>
          </div>
            <div class="card-body">
            {!! $dataTable->table([],true) !!}
        </div>
        </div>
  <!--    </div> -->
  </div>
@endsection
@once
  @push ('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
    @vite('resources/assets/js/custom.js')
  @endpush
@endonce
