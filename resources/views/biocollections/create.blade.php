@extends('layouts.app')

@section('content')
<div id='contents' class="container">
  <br>
  <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
      <div class="card-header">
            @lang('messages.new_biocollection')
            &nbsp;&nbsp;
            <a data-bs-toggle="collapse" href="#help" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
            <div id="help" class="odb-hint collapse">
              @lang('messages.biocollections_hint')
            </div>
        </div>
        <div class="card-body">
      

        @if (isset($biocollection))
        		    <form action="{{ url('biocollections/' . $biocollection->id)}}" method="POST" class="row g-2" enctype="multipart/form-data">
                  {{ method_field('PUT') }}
        @else
        		    <form action="{{ url('biocollections')}}" method="POST" class="row g-2" enctype="multipart/form-data">
                  <input type="hidden" name="route-url" value="{{ route('checkih') }}">
        @endif
		     {{ csrf_field() }}

         @php
           $admins = isset($biocollection) ? $biocollection->admins->pluck('id')->toArray() : [];
           $managed = '';
           if (count($admins)>0 or old('ismanaged')) {
             $managed = 'checked';
           }
         @endphp

		    @include('biocollections.form')

        @if(!isset($biocollection))
		     <div class="mb-3">
			    <div class="col-sm-offset-3 col-sm-6">
				<button type="submit" class="btn btn-primary" id="checkih">
				    <i class="fa fa-btn fa-plus"></i>
            @lang('messages.checkih')
				</button>
				<div class="spinner" id="spinner"> </div>
				<div class="btn btn-sm btn-link text-nowrap" id="noih" >
				    <i class="fa fa-btn fa-plus"></i>
            @lang('messages.noih')
          </div>
			   </div>
			  </div>
        @endif

      @if(Auth::user()->access_level==2 or in_array(Auth::user()->id,$admins))
      <div class="mb-3">
          <input type="checkbox" name="ismanaged" id="ismanaged"  value=1  {{ $managed }}>
          <label class="form-check-label" for="ismanaged">
          @lang('messages.biocollection_managed')
          </label>
          <a data-bs-toggle="collapse" href="#biocolmanaged" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
          <div id="biocolmanaged" class="odb-hint collapse">
            @lang ('messages.biocollection_managed_hint')
          </div>
      </div>
      @endif


      <div class="mb-3 userbox">
      <label for="admins" class="form-label mandatory">
      @lang('messages.curators')
      </label>
      <a data-bs-toggle="collapse" href="#hint2" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
      <div id="hint2" class="odb-hint collapse">
        @lang ('messages.biocollection_users_hint')
      </div>
      <div class="">
      {!! Multiselect::autocomplete(
          'admins',
          $fullusers->pluck('email', 'id'), isset($biocollection) ? $biocollection->admins->pluck('id') : [Auth::user()->id],
           ['class' => 'multiselect form-control']
      ) !!}
      </div>

      </div>

      <div class="mb-3 userbox">
      <label for="collabs" class="form-label">
      @lang('messages.collabs')
      </label>
      <div class="">
      {!! Multiselect::autocomplete(
          'collabs',
          $fullusers->pluck('email', 'id'), isset($biocollection) ? $biocollection->collabs->pluck('id') : [],
           ['class' => 'multiselect form-control']
      ) !!}
      </div>
      </div>

      <div class="mb-3 userbox">
        <div class="float-end me-5">
          @if(isset($logoUrl))
              <img src='{{ $logoUrl }}' width='150'>
          @endif
        </div>
        <div class="col-sm-6">
        <label for="logo" class="form-label">
            @lang('messages.logo')
        </label>
        <a data-bs-toggle="collapse" href="#logo_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
        <input type="file" name="logo" id="logo"  >
        </div>

        <div id="logo_hint" class="odb-hint collapse">
          @lang('messages.logo_biocollection_hint')
        </div>

      </div>

      <div class="mb-3 userbox">
          <label for="description" class="form-label" >
            @lang('messages.dataset_short_description')
      </label>
      <a data-bs-toggle="collapse" href="#biocollection_short_description_hint" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
      <div class="">
        <textarea name="description" id="description" class="form-control" maxlength="500">{{ old('description', isset($biocollection) ? $biocollection->description : null) }}</textarea>
      </div>
        <div id="biocollection_short_description_hint" class="odb-hint collapse">
          {!! Lang::get('messages.biocollection_short_description_hint') !!}
        </div>
      </div>




      <div class="mb-3">
			   <button type="submit" class="btn btn-success" name="submit" value="submit">
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.add')
        </button>
			</div>
    </form>
                </div>
            </div>
    </div>
@endsection
@once
@push ('scripts')

<script type="module">


$(document).ready(function() {

if ($('#ismanaged').is(':checked')) {
  $(".userbox").show();
} else {
  $(".userbox").hide();
}

$('#ismanaged').on('change',function(e){
  if ($(this).is(':checked')) {
    $(".userbox").show();
  } else {
    $(".userbox").hide();
  }
});

$("#noih").click(
  function() {
    var currentclass = $(this).hasClass('btn-link');
    if (currentclass) {
        $("#checkih").removeClass("btn-link");
        $("#checkih").addClass("btn-link");
        $(this).removeClass("btn-link");
        $(this).addClass("btn-primary");
        $('#name').attr("readonly", false);
        $('#name').val(null);
        $('#irn').val(-1);
    } else {
        $("#checkih").removeClass("btn-link");
        $("#checkih").addClass("btn-primary");
        $(this).removeClass("btn-primary");
        $(this).addClass("btn-link");
        $('#name').attr("readonly", true);
        $('#name').val(null);
        $('#irn').val(null);
    }
  }
);

});
</script>
  {!! Multiselect::scripts('admins', url('users/autocomplete_all'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
  {!! Multiselect::scripts('collabs', url('users/autocomplete_all'), ['noSuggestionNotice' => Lang::get('messages.noresults')]) !!}
  @vite('resources/assets/js/custom.js')

@endpush
@endonce