@extends('layouts.app')

@section('content')
<div id='contents' class="container"> 
      <!--   <div class="col-sm-offset-2 col-sm-8"> -->
      <input type="hidden" name="hasloggedUser" id='hasloggedUser' value="{{((null !== Auth::user()) ? 1 : 0)}}">
      <div class="card mt-5">
        <div class="card-header">
                @lang('messages.biocollection')
        </div>
        <div class="card-body">
        <!-- IN CASE THE COLLECTIONS USES THIS INSTALATION FOR DATA MANAGEMENT -->
        @if($biocollection->admins->count() )
        <div class="row">
          <div class="col-sm-9">
              <h5>
                {{ isset($biocollection->name) ? $biocollection->name : '' }}
              </h5>
              <!-- short description -->
              @if (isset($biocollection->description))
              <br>
              {!! $biocollection->description !!}
              <br>
            @endif
          </div>
          @if(isset($logoUrl))
            <div class="col">
              <img src='{{ url($logoUrl) }}'  class="card-img-top" width="150" alt="...">
            </div>
          @else
            <div class="col">
              <h1>{{$biocollection->acronym}}</h1>
            </div>
          @endif
        </div>
        @endif
       <dl>
          <dt class="dt-odb">
          @lang('messages.acronym'):
          </dt>
          <dd class='mb-3'>
            {{ $biocollection->acronym }}
          </dd>
          <dt class="dt-odb">
            @lang('messages.institution')
          </dt>
          <dd class='mb-3'>
            {{ $biocollection->name }}
          </dd>
        @if($biocollection->irn >0)
          <dt class="dt-odb">
            @lang('messages.details')
          </dt>
          <dd class='mb-3'>
            {!! $biocollection->irn_url !!}
          </dd>
        @endif
      </dl>




      <!-- BUTTONS -->
    <nav class="navbar navbar-light navbar-expand-lg" id="buttons-navbar">
        <div class="navbar-header">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarbuttons" aria-controls="navbarbuttons" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
        <div class="navbar-collapse collapse flex-column ml-lg-0 ml-3" id="navbarbuttons">
          <br>
            <ul class="nav navbar-nav flex-row">

              @can ('update', $biocollection)
              <li class="nav-item px-2 mb-2">
                  <a href="{{ url('biocollections/'. $biocollection->id. '/edit')  }}" class="btn btn-success" name="submit" value="submit">
                  <i class="fa fa-btn fa-plus"></i>
                  @lang('messages.edit')
                </a>
              </li>
              @endcan


              @if($biocollection->admins->count()>0)
              <li class="nav-item px-2 mb-2">
                <a data-bs-toggle="collapse" href="#people" class="btn btn-outline-secondary">@lang('messages.persons')</a>
              </li>
              @endif

              <li class="nav-item px-2 mb-2">
                <button type="button" class="btn btn-outline-secondary btntoogle" data='vouchers' >@lang('messages.vouchers')</button>
              </li>

              @if(count($biocollection->bibreferences_ids()))
              <li class="nav-item px-2 mb-2">
                  <a href="{{ url('references/'.$biocollection->id.'/biocollection')  }}" class="btn btn-outline-secondary" name="submit" value="submit">
                  @lang('messages.references')
                </a>
              </li>
              @endif

              @if(null !== Auth::user())
                @if($biocollection->isUser(Auth::user()))
                  <li class="nav-item px-2 mb-2">
                    <a href="{{ url('odbrequests/'.$biocollection->id.'/biocollection')  }}" class="btn btn-primary" name="submit" value="submit">
                    @lang('messages.request_management')
                  </a>
                </li>
                  @endif
              @endif

              @if($biocollection->vouchers()->count() == 0)
                @can ('delete', $biocollection)
                <li class="nav-item px-2 mb-2">
                  <form action="{{ url('biocollections/'.$biocollection->id) }}" method="POST" class="form-horizontal">
                   {{ csrf_field() }}
                   {{ method_field('DELETE') }}
                   <button type="submit" class="btn btn-danger">
                     <i class="far fa-trash-alt"></i>
                     &nbsp;
                     @lang('messages.remove_biocollection')
                   </button>
                  </form>
                </li>
                @endcan
              @endif
            </ul>
          </div>
      </nav>
    </div>

</div>

<div class="collapse" id='people'>
@if($biocollection->admins->count()>0)
<div class="card">
      <div class="card-header">
          @lang('messages.persons')
      </div>
      <div class="card-body">
      <table class="table table-striped user-table">
      <thead>
        <th>@lang('messages.email')</th>
        <th>@lang('messages.name')</th>
        <th>@lang('messages.role')</th>
      </thead>
      <tbody>
      @foreach($biocollection->people as $role => $list)
        @foreach($list as $member)
        <tr>
        <td class="table-text">
            {{ $member[0] }}
        </td>
        <td class="table-text">
            {!! $member[1] !!}
        </td>
        <td class="table-text">
            {{ $role }}
        </td>
        </tr>
        @endforeach
      @endforeach
    </tbody>
    </table>
  </div>
</div>
@endif

@if ($biocollection->persons->count())
<div class="card">
    <div class="card-header">
        @lang('messages.specialists')
    </div>
    <div class="card-body">
      <table class="table table-striped person-table">
        <thead>
          <th>
            @lang('messages.abbreviation')
          </th>
          <th>
            @lang('messages.name')
          </th>
          <th>
            @lang('messages.email')
          </th>
        </thead>
        <tbody>
          @foreach ($biocollection->persons as $person)
            <tr>
              <td class="table-text">
                <div>
                  {!! $person->rawLink() !!}
                </div>
              </td>
              <td class="table-text">
                {{ $person->full_name }}
              </td>
              <td class="table-text">
                {{ $person->email }}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endif
</div>


<div class="card vouchers collapse mt-5">
    <div class="card-header">
        @lang('messages.vouchers')
        @if($biocollection->users->count())
          &nbsp;&nbsp;
          <button type="button" class="btn btn-primary" id='odb-voucher-request'>@lang('messages.request_title')</button>
        @endif
    </div>
    @if (Auth::user())
    {!! View::make('common.exportdata')->with([
          'object' => isset($object) ? $object : null,
          'object_second' => isset($object_second) ? $object_second : null,
          'export_what' => 'Voucher'
    ]) !!}
    @else
      <br>
    @endif
    <div class="card-body" id='voucher_request_panel' >
      <div class="col-sm-offset-3 col-sm-6" id="user_request_hint" ></div>
      <div class="col-sm-12" id='user_request_form' >
      <br>
      <form action="{{ url('odbrequest') }}" method="POST" class="row g-2" >
          <!-- csrf protection -->
          {{ csrf_field() }}
          <input type='hidden' name='voucher_ids' id='voucher_ids' value="" >

            <div class="mb-3">
              <label for="email" class="form-label mandatory">
                @lang('messages.email')
              </label>
              @if (!Auth::user())
              <div class="col-sm-6">
                <input id="email" autocomplete="email" type="email" name="email" value="" class='form-control'>
              </div>
              @else
              <div class="col-sm-6">
                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" >
                <input id="email" value="{{ Auth::user()->email }}" class='form-control' readonly>
              </div>
              @endif
          </div>

          <div class="mb-3">
            <label for="institution"  class="form-label ">
              @lang('messages.institution')
            </label>
            <a data-bs-toggle="collapse" href="#whereto" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
            <div class="col-sm-6">
              <input type="institution" name="institution" value="" class="form-control">
            </div>
            <div id="whereto" class="odb-hint collapse">
                 @lang('messages.request_whereto')
            </div>
          </div>
          <!-- THE BIOCOLLECTION THE VOUCHER BELONGS TO -->
          <div class="mb-3">
            <label for="biocollection_id" class="form-label ">
              @lang('messages.biocollection')
            </label>
            <div class="col-sm-6">
              <select name="biocollection_id" class="form-select" >
                <option value="" >Not registered</option>
                @foreach ($biocollections as $biocollection)
                <option value="{{ $biocollection->id }}" >
                  {{ $biocollection->acronym."  [".substr($biocollection->name,0,30)."..]"}}
                </option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="mb-3">
            <label class="form-label mandatory">
              @lang('messages.request_message')
            </label>
            <a data-bs-toggle="collapse" href="#hint6" class="odb-unstyle"><i class="far fa-question-circle"></i></a>
            <div class="col-sm-6">
              <textarea name="message" class="form-control" rows=5 required></textarea>
            </div>
              <div id="hint6" class="odb-hint collapse">
      	         @lang('messages.request_message_hint')
               </div>
          </div>

          <div class="mb-3">
            <span id='submitting' hidden><i class="fas fa-sync fa-spin"></i></span>
            <input id='export_sumbit' class="btn btn-success" name="submit" type='submit' value='@lang('messages.request_send')' >
          </div>
      </form>
      </div>
    </div>
    <br>
    <form action="{{url('vouchers/map')}}" method="POST" class="form-horizontal" id='odb-map-occurrences'>
      <!-- csrf protection -->
      {{ csrf_field() }}
      <input type="hidden" name="idstomap" value="" id='odb-map-selected-rows'>
    </form>
    <div class="card-body">
      {!! $dataTable->table([],true) !!}
    </div>
</div>
</div>

@endsection
@once
  @push ('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
    @vite('resources/assets/js/custom.js')
  
<script type='module'>


$(document).ready(function() {

//this fixes a misalignment of datatable body and header
//when scrollX is true and on large devices
//$('#dataTableBuilder').css('margin-left','0px');
$("#voucher_request_panel").hide();
$("#user_request_form").hide();



/* generic function to close and focus on page elements */
$('.btntoogle').on('click',function() {
    var element = $(this).attr('data');
    var container = $('.container');
    if ($('.'+element).is(":hidden")) {
      $('.'+element).show();
      var scrollTo = $('.'+element);
      var position = scrollTo.offset().top;
      window.scrollTo(null,position);
      var table =  $('#dataTableBuilder').DataTable();
      table.columns.adjust().draw();
    } else {
      $('.'+element).hide();
      window.scrollTo(null,container.offset().top);
    }
});

$('#odb-voucher-request').on('click', function(e){
  var hasuser = $("#hasloggedUser").val();
  var isvisible = $('#voucher_request_panel').is(":visible");
  if (!isvisible) {
    $("#voucher_request_panel").show();
  } else {
    $("#voucher_request_panel").hide();
  }
  if (hasuser==1) {
    var dt =  $('#dataTableBuilder').DataTable();
    var rows_selected = dt.column( 0 ).checkboxes.selected();
    var nrecords = rows_selected.length;
    $('#voucher_ids').val('');
    if (nrecords == 0) {
      var text = '<div class="alert alert-danger" >Select records for your request</div>';
      $('#user_request_hint').html(text);
      $("#user_request_form").hide();
    } else {
      $('#user_request_hint').html('<div class="alert alert-success" >' + nrecords + '{{Lang::get("messages.selected_records")}}</div>');
      $("#user_request_form").show();
      $('#voucher_ids').val(rows_selected.join());
    }
  } else {
    var text = '<div class="alert alert-danger" >{{ Lang::get("messages.login_to_request") }}</div>';
    $('#user_request_hint').html(text);
    $("#user_request_form").hide();
  }

});




});

</script>

@endpush
@endonce