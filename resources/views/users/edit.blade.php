@extends('layouts.app')

@section('content')
    <div class="container">
      <br>
      <div class="card col-lg-6 offset-lg-3 col-md-6 offset-md-3">
          <div class="card-header">
                    @lang('messages.edit_user')
                </div>
<div class="card-body">

                    <!-- Edit Person Form -->
		    <form action="{{ url('users/'.$user->id) }}" method="POST" class="row g-2">
			 {{ csrf_field() }}
                         {{ method_field('PUT') }}

<div class="mb-3">
    <label for="email" class="form-label mandatory">
@lang('messages.email')
</label>
    <div class="">
	<input type="text" name="email" id="email" class="form-control" value="{{ old('email', isset($user) ? $user->email : null) }}">
    </div>
</div>
<div class="mb-3">
  <label for="password" class="form-label mandatory">
    @lang('messages.password')
  </label>
  <a data-bs-toggle="collapse" href="#hint1" class="odb-unstyle">
    <i class="far fa-question-circle"></i></a>
  <div class="">
    <input autocomplete="current-password" type="text" name="password" id="password" class="form-control" value="">
  </div>
  <div id="hint1" class="odb-hint collapse">
    @lang('messages.password_change_hint')
  </div>
</div>

<div class="mb-3">
  <label for="access" class="form-label mandatory">@lang('messages.access_level')</label>
  <a data-bs-toggle="collapse" href="#hint2" class="odb-unstyle">
    <i class="far fa-question-circle"></i></a>
    <div class="">
	     <?php $selected = old('access_level', $user->access_level); ?>
	<select name="access_level" id="access_level" class="form-select" >
	@foreach (App\Models\User::LEVELS as $al)
		<option value="{{$al}}" {{ $al == $selected ? 'selected' : '' }}>
			 @lang ('levels.access.' . $al)
		</option>
	@endforeach
	</select>
    </div>
    <div id="hint2" class="odb-hint collapse">
	@lang('messages.access_level_change_hint')
    </div>
</div>
		        <div class="mb-3">
				<button type="submit" class="btn btn-success">
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.save')

				</button>
				<a href="{{url()->previous()}}" class="btn btn-warning">
				    <i class="fa fa-btn fa-plus"></i>
@lang('messages.back')
				</a>
			    </div>

		    </form>

        <form action="{{ url('users/'.$user->id) }}" method="POST" class="row g-2">
			 {{ csrf_field() }}
                         {{ method_field('DELETE') }}
		        <div class="mb-3">
				<button type="submit" class="btn btn-danger">
				    <i class="fa fa-btn fa-plus"></i>
            @lang('messages.remove_user')
				</button>
			</div>
		    </form>
                </div>
            </div>
        </div>
@endsection
