/**
 * Here we load all of the JavaScript dependencies. Custom code should be on custom.js
*/
import './bootstrap';

import Alpine from 'alpinejs';
window.Alpine = Alpine;


//Import Datatables
import jszip from 'jszip';
import pdfmake from 'pdfmake';
import DataTable from 'datatables.net-dt';
import 'datatables.net-buttons-dt';
import 'datatables.net-buttons/js/buttons.colVis.mjs';
import 'datatables.net-buttons/js/buttons.html5.mjs';
import 'datatables.net-buttons/js/buttons.print.mjs';
//import 'datatables.net-fixedcolumns-dt';
//import 'datatables.net-fixedheader-dt';
import 'datatables.net-responsive-dt';
import 'datatables.net-scroller-dt';
import 'datatables.net-searchbuilder-dt';
import 'datatables.net-searchpanes-dt';
import 'datatables.net-select-dt';
import 'datatables.net-staterestore-dt';

//require('datatables.net-buttons');
//require('datatables.net-buttons/js/buttons.colVis.js');
import '@fortawesome/fontawesome-free/scss/fontawesome.scss';
import '@fortawesome/fontawesome-free/scss/brands.scss';
import '@fortawesome/fontawesome-free/scss/regular.scss';
import '@fortawesome/fontawesome-free/scss/solid.scss';
import '@fortawesome/fontawesome-free/scss/v4-shims.scss';


import PureCounter from '@srexi/purecounterjs/purecounter.js';
const pure = new PureCounter();


// Import FilePond
import * as FilePond from 'filepond';
window.FilePond = FilePond;

// Import the plugin code
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';


import 'devbridge-autocomplete';


import '/vendor/betovicentini/laravel-multiselect/resources/assets/js/multiselect.js';

Alpine.start();
