/** CUSTOM JS CODE HERE: */
$(document).ready(function(){
    // chainable custom autocomplete with some global defaults
    // use invalidateCallback for cleanups
    // If this is edited, NOTICE that measurements/create use a pure devbridge version and
    // must be updated as well!
    $.fn.odbAutocomplete = function(url, id_element, noResult, invalidateCallback, params, selectCallback) {
        return this.devbridgeAutocomplete({
            serviceUrl: url,
            onSelect: function (suggestion) {
                $(id_element).val(suggestion.data);
                if (typeof selectCallback === "function")
                    selectCallback(suggestion);
            },
            onInvalidateSelection: function() {
                $(id_element).val(null);
                if (typeof invalidateCallback === "function")
                    invalidateCallback();
            },
            minChars: 1,
            onSearchStart: function() {
                $(".minispinner").remove();
                $(this).after("<div class='spinner minispinner'></div>");
            },
            onSearchComplete: function() {
                $(".minispinner").remove();
            },
            showNoSuggestionNotice: true,
            noSuggestionNotice: noResult,
            params: params
        });
    };

    /* DATADATABLES EXPORT BUTTONS */
    $('.odb-export-button').on('click',function(e) {
      var isvisible = document.getElementById('export_pannel').style.display;
      if (isvisible == 'none') {
        document.getElementById('export_pannel').style.display = 'block';
      } else {
          document.getElementById('export_pannel').style.display = 'none';
      }
    });
    $('#export_submit').on('click',function(e){
      var table =  $('#dataTableBuilder').DataTable();
      var selectedRows = table.rows('.selected').data();
      var rows_selected = selectedRows.pluck('id').toArray();
      $('#export_ids').val( rows_selected.join());
      $("#export_form"). submit();
    });


    $('.odb-filter-button').on('click',function(e) {
      var isvisible = document.getElementById('filter_pannel').style.display;
      if (isvisible == 'none') {
          document.getElementById('filter_pannel').style.display = 'block';
      } else {
          document.getElementById('filter_pannel').style.display = 'none';
      }
    });

    /*  MAP BUTTONS */
    $('.odb-map-selected').on('click', function(e){
        //check if mandatory fields are filled
        var table =  $('#dataTableBuilder').DataTable();
        var selectedRows = table.rows('.selected').data();
        var rows_selected = selectedRows.pluck('id').toArray();
        var idstomap = null;
        if (rows_selected.length==0) {
            var total = table.data().count();
            if (total>5000) {
              var txt = "@lang('messages.map_too_many')";
              alert(txt);
            } else {
              idstomap = table.rows().data().pluck('id').toArray().join();
            }
        } else {
          idstomap = rows_selected.join();
        }
        if (idstomap!=null) {
          $('#odb-map-selected-rows').val(idstomap);
          $("#odb-map-occurrences").submit();
        }
    });
    $(".odb-map-selected").css("background",'#3097D1').css('color','white');


    /* DATATABLE DELETE BUTTONS */
    $('.odb-deselect-button').on('click',function ( e) {
        var dt =  $('#dataTableBuilder').DataTable();
        dt.rows().deselect();
    });

    $('.odb-delete-button').on('click',function ( e) {
      var dt =  $('#dataTableBuilder').DataTable();
      var selectedRows = dt.rows('.selected').data();
      var rows_selected = selectedRows.pluck('id').toArray();
      var nrecords = rows_selected.length;
      var isvisible = document.getElementById('delete_pannel').style.display;
      document.getElementById('ids_to_delete').value = '';
      if (isvisible == 'none') {
        document.getElementById('delete_pannel').style.display = 'block';
      } else {
        document.getElementById('delete_pannel').style.display = 'none';
      }
      if (nrecords == 0) {
        var msg = $('#select_rows_msg').val();
        document.getElementById('delete_hint').innerHTML = msg;
        document.getElementById('delete_form').style.display = 'none';
      } else {
        var msg = $('#selected_rows_msg').val();
        document.getElementById('delete_hint').innerHTML = nrecords + ' ' + msg;
        document.getElementById('delete_form').style.display = 'block';
        document.getElementById('ids_to_delete').value = rows_selected.join();
      }
    });
    $(".odb-delete-button").css("background",'#bf5329').css('color','white');


    $('.odb-delete-confirmation-button').on('click',function ( e) {
      var form =  $('#odb-delete-confirmation-form');
      if (form.is(":hidden")) {
        form.show();
      } else {
        form.hide();
      }
    });




    $('.odb-hide-status-bt').on('click',function (e) {
      $('#odb-status-box').hide();
    });

    $('.odb-hide-errors-bt').on('click',function (e) {
      $('#odb-errors-box').hide();
    });
    //this fixes a misalignment of datatable body and header
    //when scrollX is true and on large devices
    $('#dataTableBuilder').css('margin-left','0px');
    $(".odb-create-button").css("background",'#2ab27b').css('color','white');


    /* DATATABLE unwrap and expand buttons */
    $('.odb-expand-dt').on('click',function(e) {
      var contents = $('#contents').hasClass('container-fluid');
      if (!contents) {
        $('#expand_button').removeClass('fa-expand-arrows-alt');
        $('#expand_button').addClass('fa-compress-arrows-alt');
        $('#contents').removeClass('container');
        $('#contents').addClass('container-fluid');
        $('#dataTableBuilder').removeClass('wrap');
        $('#dataTableBuilder').addClass('nowrap');
        $('#dataTableBuilder').css('margin-left','0px');
      } else {
        $('#expand_button').removeClass('fa-compress-arrows-alt');
        $('#expand_button').addClass('fa-expand-arrows-alt');
        $('#contents').addClass('container');
        $('#contents').removeClass('container-fluid');
        $('#dataTableBuilder').removeClass('nowrap');
        $('#dataTableBuilder').addClass('wrap');
      }
      var table =  $('#dataTableBuilder').DataTable();
      table.columns.adjust().draw();
    });

    $('.odb-wrap-dt').on('click',function(e) {
      var wrap = $('#dataTableBuilder').hasClass('wrap');
      if (wrap) {
        $('#wrap_button').addClass('fa-expand-alt');
        $('#wrap_button').removeClass('fa-compress-alt');
        $('#dataTableBuilder').addClass('nowrap').removeClass('wrap');
      } else {
        $('#wrap_button').removeClass('fa-expand-alt');
        $('#wrap_button').addClass('fa-compress-alt');
        $('#dataTableBuilder').addClass('wrap').removeClass('nowrap');
      }
      var table =  $('#dataTableBuilder').DataTable();
      table.columns.adjust().draw();
    });


  	/** Abbreviation helper for the Person entity */
  	$("#full_name").blur(function () {
  		/* Only changes value if the field is empty */
  		var abb = $("#abbreviation");
  		if (abb.val() === "") {
  			var txt = $(this).val().toUpperCase().trim().split(" ");
  			/* Stores the person last name, which will not be abbreviated */
  			var lname = txt.pop();
  			for (i = 0; i < txt.length; i++) {
  				txt[i] = txt[i].substring(0, 1) + ".";
  			}
  			if (txt.length > 0) {
  				abb.val(lname + ", " + txt.join(" "));
  			} else {
  				abb.val(lname);
  			}
  		}
  	});

	/** The following functions allow a "fake" submit button to replace a file input control.
	 *  Used on every view that accepts a file input */
	$("#fakerfile").click(function(e) {
		e.preventDefault();
		$("#rfile").trigger("click");
	});
	$("#rfile").change(function (){
		$("#submit").trigger("click");
	});

	/** Ajax handling for registering biocollections */
  /** index Herbariorum api */
	$("#checkih").click(function(e) {
		$( "#spinner" ).css('display', 'inline-block');
		$.ajaxSetup({ // sends the cross-forgery token!
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		})
		e.preventDefault(); // does not allow the form to submit
		$.ajax({
			type: "POST",
			url: $('input[name="route-url"]').val(),
			dataType: 'json',
			data: {'acronym': $('input[name="acronym"]').val()},
			success: function (data) {
				$( "#spinner" ).hide();
				if ("error" in data) {
					$( "#ajax-error" ).show();
					$( "#ajax-error" ).text(data.error);
				} else {
					// ONLY removes the error if request is success
					$( "#ajax-error" ).hide();
					$("#irn").val(data.ihdata[0]);
					$("#name").val(data.ihdata[1]);
				}
			},
			error: function(e){
				$( "#spinner" ).hide();
				$( "#ajax-error" ).show();
				$( "#ajax-error" ).text('Error sending AJAX request');
			}
		})
	});


  /* page top button */
  //Get the button
  var bntPgTop = document.getElementById("odb-bntPgTop");
  // When the user scrolls down 20px from the top of the document, show the button
  window.onscroll = function() {scrollFunction()};
  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      bntPgTop.style.display = "block";
    } else {
      bntPgTop.style.display = "none";
    }
  }
  // When the user clicks on the button, scroll to the top of the document
  $('#odb-bntPgTop').on('click',function(e){
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
  });


});
