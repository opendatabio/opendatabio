import {Map, View, Overlay} from 'ol';
import LayerTile from 'ol/layer/Tile';
import LayerGroup from 'ol/layer/Group';
import SourceOSM from 'ol/source/OSM';
import SourceXYZ from 'ol/source/XYZ';
import SourceStamen from 'ol/source/Stamen';
import LayerSwitcher from 'ol-layerswitcher';
import { BaseLayerOptions, GroupLayerOptions } from 'ol-layerswitcher';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import GeoJSON from 'ol/format/GeoJSON';
import {Circle as CircleStyle, RegularShape,Fill, Stroke, Style} from 'ol/style';
import {fromLonLat} from 'ol/proj';
import {FullScreen, defaults as defaultControls,ZoomToExtent} from 'ol/control';
import * as olExtent from 'ol/extent';
import {transformExtent} from 'ol/proj';

var cores = ['rgb(242,183,1)','rgb(207,28,144)', 'rgb(128,186,90)', 'rgb(230,131,16)', 'rgb(57,105,172)','#e60049', 'rgb(127,60,141)',  'rgb(75,75,143)', '#e6d800',  '#0bb4ff','rgb(249,123,114)','#50e991', 'rgb(0,134,149)', '#b3d4ff', '#00bfa0','rgb(165,170,153)'];

var occurrence_map = {                       // <-- add this line to declare the object
    display: function () {           // <-- add this line to declare a method
      var singlestyle = new Style({
          image: new CircleStyle({
           radius: 5,
           fill: new Fill({color: 'yellow'}),
           stroke: new Stroke({color: 'black', width: 0.2}),
         })
      });
      var datasetstyle = new Style({
          image: new RegularShape({
           radius: 10,
           fill: new Fill({color: cores[1]}),
           stroke: new Stroke({color: 'black', width: 0.2}),
           points: 4,
           angle: Math.PI / 4,
         })
      });

      function random_rgba(opacity=0.1) {
        var o = Math.round, r = Math.random, s = 255;
        return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(1) + ','+ opacity +')';
      }

      var haselement = document.getElementsByName("location_json[]");
      var layername = 'Occurrences';
      var alocation = null;
      var mylocations = [];
      for (var i = 0; i <haselement.length; i++) {
          var item=haselement[i].value;
          var index=haselement[i].title;
          if (index!='single') {
           layername = index;
          }
          var title = '<i class="fa fa-circle" style="color: '+cores[i]+';"></i> '+layername;
          var thestyle = new Style({
              image: new CircleStyle({
               radius: 5,
               fill: new Fill({color: cores[i]}),
               stroke: new Stroke({color: 'black', width: 0.2}),
             })
          });
          if (index=='datasets') {
            layername = 'Datasets';
            thestyle = datasetstyle;
            title = '<i class="fa fa-square" style="color: '+cores[1]+';"></i> '+layername;
          }
          alocation = new VectorLayer({
          title: title,
          visible: true,
          source: new VectorSource({
              features: new GeoJSON().readFeatures(item,{
                  featureProjection:  'EPSG:3857'
              }),
          }),
          style: thestyle,
         });
         //console.info(alocation);
         mylocations.push(alocation);   // Adds "Kiwi"
      }
      const osm = new LayerTile({
          title: 'OSM',
          type: 'base',
          visible: true,
          source: new SourceOSM()
      });
      const watercolor = new LayerTile({
          title: 'Water color',
          type: 'base',
          visible: false,
          source: new SourceStamen({
            layer: 'watercolor'
          })
      });
      const topographic = new LayerTile({
          title: 'ESRI Topo',
          type: 'base',
          visible: false,
          source: new SourceXYZ({
              attributions:
                'Tiles © <a href="https://services.arcgisonline.com/ArcGIS/' +
                'rest/services/World_Topo_Map/MapServer">ArcGIS</a>',
              url:
                'https://server.arcgisonline.com/ArcGIS/rest/services/' +
                'World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
          }),
      });
      const worldimagery = new LayerTile({
          title: 'ESRI Satelite',
          type: 'base',
          visible: false,
          source: new SourceXYZ({
            attributions:
              'Tiles © <a href="https://services.arcgisonline.com/ArcGIS/' +
              'rest/services/World_Imagery/MapServer">ArcGIS</a>',
            url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
            maxZoom: 19
          })
      });
      const baseMaps = new LayerGroup({
          title: 'Base maps',
          layers: [watercolor, worldimagery, topographic,osm],
          fold: 'close',
      });
      if (mylocations.length>1) {
        var mylayer = new LayerGroup({
            title: 'Occurrences',
            layers: mylocations,
            fold: 'close',
        });
      } else {
        var mylayer = mylocations[0];
        /*
        new VectorLayer({
          title: "Occurrences",
          visible: true,
          source: mylocations,
          style: singlestyle,
        });
      */
     }

      const layerSwitcher = new LayerSwitcher({
          groupSelectStyle: ['none','group']
      });

      var centroid = document.getElementById("location_centroid");
      if (centroid != null) {
          var centroid_wm = fromLonLat(centroid.value.split(","));
      } else {
          var centroid_wm = fromLonLat([-66,-2]);
      }
      var theextent = document.getElementById("location_extent");
      theextent = JSON.parse(theextent.value);
      theextent = transformExtent([theextent.xmin,theextent.ymin,theextent.xmax,theextent.ymax], 'EPSG:4326', 'EPSG:3857');
      const map = new Map({
          controls: defaultControls().extend([
            new FullScreen(),
            new ZoomToExtent({
                 extent: theextent,
            }),
          ]),
          target: 'osm_map',
          layers: [baseMaps,mylayer],
          view: new View({
              center: centroid_wm,
              zoom: 0
          })
      });
      map.addControl(layerSwitcher);
      let view = map.getView();
      view.fit(theextent, {padding: [100, 100, 100, 100]});


        /**
         * Popup
         **/
        var container = document.getElementById('popup');
        var content_element = document.getElementById('popup-content');
        var closer = document.getElementById('popup-closer');

        closer.onclick = function() {
            overlay.setPosition(undefined);
            closer.blur();
            return false;
        };
        var overlay = new Overlay({
            element: container,
            autoPan: true,
            offset: [0,0]
        });
        map.addOverlay(overlay);

        function getCenterOfExtent(Extent){
            var X = Extent[0] + (Extent[2]-Extent[0])/2;
            var Y = Extent[1] + (Extent[3]-Extent[1])/2;
            return [X, Y];
        }
        map.on("pointermove", function (evt) {
            var hit = this.forEachFeatureAtPixel(evt.pixel, function(feature, layer) {
                return true;
            });
            if (hit) {
                this.getTargetElement().style.cursor = 'pointer';
            } else {
                this.getTargetElement().style.cursor = '';
            }
        });
        map.on('click', function(evt){
            var feature = map.forEachFeatureAtPixel(evt.pixel,
              function(feature, layer) {
                return feature;
              });
            if (feature) {
                var geometry = feature.getGeometry();
                var coord = getCenterOfExtent(geometry.getExtent());
                var content = feature.get('description');
                content_element.innerHTML = content;
                overlay.setPosition(coord);
                //console.info(feature.getProperties());
            }
        });
    }                                // <-- close the method
};                                   // <-- close the object
export default occurrence_map;               // <-- and export the object
