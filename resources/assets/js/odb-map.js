import {Map, View, Overlay} from 'ol';
import LayerTile from 'ol/layer/Tile';
import LayerGroup from 'ol/layer/Group';
import SourceOSM from 'ol/source/OSM';
import SourceXYZ from 'ol/source/XYZ';
import SourceStamen from 'ol/source/Stamen';
import LayerSwitcher from 'ol-layerswitcher';
import { BaseLayerOptions, GroupLayerOptions } from 'ol-layerswitcher';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import GeoJSON from 'ol/format/GeoJSON';
import {Circle as CircleStyle,RegularShape, Fill, Stroke, Style} from 'ol/style';
import {fromLonLat} from 'ol/proj';
import {FullScreen, defaults as defaultControls,ZoomToExtent} from 'ol/control';
import * as olExtent from 'ol/extent';
import {transformExtent} from 'ol/proj';



//var odb_map = {                       // <-- add this line to declare the object
    //display: function () {           // <-- add this line to declare a method

       var styleFunction = function(feature, resolution) {
        var adm_level = feature.getProperties().adm_level;
        var thecolor = feature.getProperties().color;
        var thefill = feature.getProperties().fill;
        //if point
        if (adm_level==1000) {
            var style = [new Style({
              image: new CircleStyle({
               radius: 5,
               fill: new Fill({color: thecolor }),
               stroke: new Stroke({color: 'black', width: 0.2}),
             })
           })];
        } else if(adm_level==999) {
           var style = new Style({
               image: new RegularShape({
                radius: 5,
                fill: new Fill({color: thecolor}),
                stroke: new Stroke({color: 'black', width: 0.2}),
                points: 4,
                angle: Math.PI / 4,
              })
           });
        } else if(adm_level==101) {
            var style = [new Style({
                stroke: new Stroke({
                    color: thecolor,
                    width: 2
                })
            })];
        } else {
          var style = [new Style({
              stroke: new Stroke({
                  color: thecolor,
                  width: 2
              }),
              fill: new Fill({
                  color: thefill
              })
          })];
        }
        return style;
       };
       var haselement = document.getElementsByName("location_json[]");
       var layername = null;
       var alocation = null;
       var adm_level = null;
       var features = null;
       var feature = null;
       var thefill = null;
       var mylocations = [];
       for (var i = 0; i <haselement.length; i++) {
           var item=haselement[i].value;
           layername=haselement[i].title;
           features = new GeoJSON().readFeatures(item,{
               featureProjection:  'EPSG:3857'
           });
           feature = features[0];
           adm_level = feature.getProperties().adm_level;
           thefill = feature.getProperties().color;
           //console.info('The adm_level ' + adm_level);
           //console.info(feature.getProperties());
           if (adm_level==1000) {
             var title = '<i class="fa fa-circle" style="color: '+thefill+';"></i> '+layername;
           } else {
             var title = '<i class="fas fa-square" style="color: '+thefill+';"></i> '+layername;
           }
           alocation = new VectorLayer({
             title: title,
             visible: true,
             source: new VectorSource({
                 features: features,
             }),
             style: styleFunction,
          });
          //console.info("THIS HAS " + features.length);
          //console.info(feature);
          mylocations.push(alocation);   // Adds "Kiwi"
       }


        const osm = new LayerTile({
          title: 'OSM',
          type: 'base',
          visible: false,
          source: new SourceOSM()
        });

        const watercolor = new LayerTile({
          title: 'Water color',
          type: 'base',
          visible: false,
          source: new SourceStamen({
            layer: 'watercolor'
          })
        });


        const topographic = new LayerTile({
          title: 'ESRI Topo',
          type: 'base',
          visible: false,
          source: new SourceXYZ({
              attributions:
                'Tiles © <a href="https://services.arcgisonline.com/ArcGIS/' +
                'rest/services/World_Topo_Map/MapServer">ArcGIS</a>',
              url:
                'https://server.arcgisonline.com/ArcGIS/rest/services/' +
                'World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
          }),
        });

        const worldimagery = new LayerTile({
          title: 'ESRI Satelite',
          type: 'base',
          visible: true,
          source: new SourceXYZ({
            attributions:
              'Tiles © <a href="https://services.arcgisonline.com/ArcGIS/' +
              'rest/services/World_Imagery/MapServer">ArcGIS</a>',
            url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
            maxZoom: 19
          })
        });

        const baseMaps = new LayerGroup({
          title: 'Base maps',
          layers: [watercolor, worldimagery, topographic,osm],
          fold: 'close',
        });

        var mylayer = new LayerGroup({
              title: 'Selected',
              layers: mylocations,
              fold: 'open',
        });

        const layerSwitcher = new LayerSwitcher({
            groupSelectStyle: ['none','group'],
        });

        var centroid = document.getElementById("location_centroid");
        if (centroid != null) {
            var centroid_wm = fromLonLat(centroid.value.split(","));
        } else {
            var centroid_wm = fromLonLat([-66,-2]);
        }
        var theextent = document.getElementById("location_extent");
        theextent = JSON.parse(theextent.value);
        theextent = transformExtent([theextent.xmin,theextent.ymin,theextent.xmax,theextent.ymax], 'EPSG:4326', 'EPSG:3857');
        const map = new Map({
            controls: defaultControls().extend([
              new FullScreen(),
              new ZoomToExtent({
                   extent: theextent,
              }),
            ]),
            target: 'osm_map',
            layers: [baseMaps,mylayer],
            view: new View({
                center: centroid_wm,
                zoom: 0
            })
        });
        map.addControl(layerSwitcher);
        let view = map.getView();
        view.fit(theextent, {padding: [100, 100, 100, 100]});


          /**
           * Popup
           **/
          var container = document.getElementById('popup');
          var content_element = document.getElementById('popup-content');
          var closer = document.getElementById('popup-closer');

          closer.onclick = function() {
              overlay.setPosition(undefined);
              closer.blur();
              return false;
          };
          var overlay = new Overlay({
              element: container,
              autoPan: true,
              offset: [0,0]
          });
          map.addOverlay(overlay);

          function getCenterOfExtent(Extent){
              var X = Extent[0] + (Extent[2]-Extent[0])/2;
              var Y = Extent[1] + (Extent[3]-Extent[1])/2;
              return [X, Y];
          }
          map.on("pointermove", function (evt) {
              var hit = this.forEachFeatureAtPixel(evt.pixel, function(feature, layer) {
                  return true;
              });
              if (hit) {
                  this.getTargetElement().style.cursor = 'pointer';
              } else {
                  this.getTargetElement().style.cursor = '';
              }
          });
          map.on('click', function(evt){
              var feature = map.forEachFeatureAtPixel(evt.pixel,
                function(feature, layer) {
                  return feature;
                });
              if (feature) {
                  var geometry = feature.getGeometry();
                  var coord = getCenterOfExtent(geometry.getExtent());
                  var content = feature.get('description');
                  content_element.innerHTML = content;
                  overlay.setPosition(coord);
                  //console.info(feature.getProperties());
              }
          });


    //}                                // <-- close the method
//};                                   // <-- close the object
//export default odb_map;               // <-- and export the object
//window.odb_map = odb_map;