/** CUSTOM JS CODE HERE: */
$(document).ready(function(){
    $("#about_list").on('click',function(){
      if ($('#about_list_text').is(':empty')){
        var records = $('#dataTableBuilder').DataTable().ajax.json().recordsTotal;
        if (records == 0) {
            var txt = $('#no_permission_list').val();
            $('#about_list_text').html(txt);
        } else {
            var txt = $('#individual_object_list').val();
            $('#about_list_text').html(txt);
        }
      } else {
        $('#about_list_text').html(null);
      }
    });
    
    function setIdentificationFields(vel) {
        var adm = $('#biocollection_id option:selected').val();
        if ("undefined" === typeof adm) {
            return; // nothing to do here...
        }
        switch (adm) {
        case "": // no biocollection
            $(".biocollection_reference").hide(vel);
            break;
        default: // other
            $(".biocollection_reference").show(vel);
        }
    }
    
    // Handle form submission event
    function runAjaxIdentification(url,tagid,batchidentify, e) {
      $.ajaxSetup({ // sends the cross-forgery token!
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      })
      e.preventDefault(); // does not allow the form to submit
      $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',
        data: { 'batchidentify' : batchidentify },
        success: function (answer) {
          //alert(JSON.stringify(answer));
          if ("error" in answer) {
          } else {
            $("#"+tagid).html(answer.html);
            $("#"+tagid).show();
            var taxonurl =  $('input[name="taxonsautocomplete"]').val();
            var personsurl =  $('input[name="personsautocomplete"]').val();
            var noresults  =  $('input[name="noresults"]').val();
            $("#taxon_autocomplete").odbAutocomplete(taxonurl, "#taxon_id",noresults,
              function() {
                  // When the identification clean this
                  $('input:radio[name=modifier][value=0]').trigger('click');
                  $("#identification_notes").val('');
                  $("#biocollection_id option:selected").removeAttr("selected");
                  // trigger").val('');
            });
            $("#identifier_autocomplete").odbAutocomplete(personsurl,"#identifier_id",noresults);
            $("#biocollection_id").change(function() { setIdentificationFields(400); });
            // trigger this on page load
            setIdentificationFields(0);
          }
        },
        error: function(e){
          $("#"+tagid).html('');
          $("#"+tagid).hide();
        }
      });
    }
    
    $(".odb-batch-identify-bt").css("background",'#cbb956').css('color','white');
    
    // Handle form submission event
    $('.odb-make-request-bt').on('click', function(e){
        if (!$('#odbrequest_pannel').is(":visible")) {
          $('#batch_identification_block').html('');
          $('#batch_identification_panel').hide();
          $('#request_select_records').hide();
          $('#individuals_batch_identify_alert').hide();
          $('#odbrequest_pannel').show();
          $('#request_identification').hide();
          $('#submitting').hide();
        } else {
          $('#request_identification').html(''); // getting respo
          $('#odbrequest_pannel').hide();
        }
    });
    
    $('input[type=radio][name=request_type]').change(function(e) {
      var value = this.value;
      if (value=='identification') {
        var url =  $('input[name="odbrequest-url"]').val();
        var batchidentify = false;
        runAjaxIdentification(url,"request_identification",batchidentify,e);
      } else {
        $("#request_identification").html('');
        $("#request_identification").hide();
      }
    });
    
    
    $('#submit_batch_identifications').on('click',function(e){
        //check if mandatory fields are filled
        var table =  $('#dataTableBuilder').DataTable();
        var selectedRows = table.rows('.selected').data();
        var rows_selected = selectedRows.pluck('id').toArray();
        var taxon = $('#taxon_id').val();
        var identifier = $('#identifier_id').val();
        var adate = $('#identification_date_year').val();
        if (rows_selected.length==0 || taxon==null || identifier==null || adate==0) {
            $('#individuals_batch_identify_alert').show();            
        } else {
          $('#batch_list').val( rows_selected.join());
          var txt = $('#individuals_batch_identify_confirm').val();
          var txt = rows_selected.length+" "+txt;
          if (confirm(txt)) {
              $("#batch_identification_form"). submit();
          }
        }
    });
    
    
    $('#hide_batch_identify_panel').on('click',function(e) {
      $('#batch_identification_block').html('');
      $("#batch_identification_panel").hide();
      $('#request_select_records').hide();
      $('#individuals_batch_identify_alert').hide();

    });
    
    $('#hide_request_individuals_hint').on('click',function(e) {
      $("#request_identification").html('');
      $("#odbrequest_pannel").hide();
    });
    
      
    $('#odb_request_submit').on('click',function(e){
        //check if mandatory fields are filled
        var table =  $('#dataTableBuilder').DataTable();
        var selectedRows = table.rows('.selected').data();
        var rows_selected = selectedRows.pluck('id').toArray();
        var requestmessage = $('#requestmessage').val();
        var biocollectionid = $("#request_biocollection_id option:selected").val();
        if (rows_selected.length==0 || requestmessage==null || requestmessage=='' || biocollectionid==null ) {
          $('#request_select_records').show();
        } else {
          $('#individualids_list').val( rows_selected.join());
          $("#odbrequest_form"). submit();
        }
    });

});