<?php

return [
    'Individual' => 'individuals.index.userjob',
    'Voucher' => 'vouchers.index.userjob',
    'ODBTrait' => 'traits.index.userjob',
    'Measurement' => 'measurements.index.userjob',
    'Person' => 'persons.index.userjob',
    'BibReference' => 'references.index.userjob',
    'Biocollection' => 'biocollections.index.userjob',
    'Taxon' => 'taxons.index.userjob',
    'Location' => 'locations.index.userjob',
];
