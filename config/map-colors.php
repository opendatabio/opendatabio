<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https=>//github.com/opendatabio
 */
/* fields for get APIs */
return  [
         '2'=> [
           'color'=> 'rgb(127,60,141)',
           'fill'=> 'rgb(127,60,141, 0.1)',
         ],
         '3'=> [
            'color'=> 'rgb(17,165,121)',
            'fill'=> 'rgb(17,165,121, 0.1)',
         ],
         '4'=> [
          'color'=> 'rgb(57,105,172)',
          'fill'=> 'rgb(57,105,172, 0.1)'
         ],
         '5'=> [
              'color'=> 'rgb(242,183,1)',
              'fill'=> 'rgb(242,183,1, 0.1)',
         ],
         '6'=> [
           'color'=> 'rgb(231,63,116)',
           'fill'=> 'rgb(231,63,116, 0.1)',
         ],
         '7'=> [
            'color'=> 'rgb(128,186,90)',
            'fill'=> 'rgb(128,186,90, 0.1)',
         ],
         '8'=> [
           'color'=> 'rgb(230,131,16)',
           'fill'=> 'rgb(230,131,16, 0.1)',
         ],
         '9'=> [
              'color'=> 'rgb(75,75,143)',
              'fill'=> 'rgb(75,75,143, 0.1)',
          ],
         '10'=> [
              'color'=> 'rgb(165,170,153)',
              'fill'=> 'rgb(165,170,153, 0.1)',
          ],
         '100'=> [
           'color'=> 'rgb(0,134,149)',
           'fill'=> 'rgb(0,134,149, 0.2)',
         ],
         '99'=> [
           'color'=> 'rgb(207,28,144)',
           'fill'=> 'rgb(207,28,144, 0.1)',
         ],
         '97'=> [
           'color'=> 'rgb(75,75,143)',
           'fill'=> 'rgb(75,75,143, 0.1)',
         ],
         '98'=> [
           'color'=> 'rgb(255, 153, 0)',
           'fill'=> 'rgb(255, 153, 0, 0.1)',
         ],
         '101'=> [
           'color'=> 'yellow',
           'fill'=> 'yellow',
         ],
         '102'=> [
           'color'=> 'rgb(249,123,114)',
           'fill'=> 'rgb(249,123,114, 0.5)',
         ],
         '1000'=> [
           'color'=> 'yellow',
           'fill'=> 'yellow',
         ],
         '999'=> [
           'color'=> 'red',
           'fill'=> 'red',
         ],
      ];
