<?php

/*
 * This file is part of the OpenDataBio app.
 * (c) OpenDataBio development team https://github.com/opendatabio
 */
/* fields for get APIs */
return [
  'activities' => [
    'all' =>  ['id','log_name', 'description', 'subject_type', 'subject_id', 'causer_type', 'causer_id', 'properties', 'created_at','updated_at'],
    'simple' =>  ['id','log_name', 'description', 'subject_type', 'subject_id', 'causer_type', 'causer_id', 'properties', 'created_at','updated_at'],
  ],
  'taxons' => [
    'all' =>  ['id','senior_id', 'parent_id', 'author_id', 'scientificName', 'taxonRank', 'scientificNameAuthorship', 'namePublishedIn', 'parentName',
    'family','higherClassification', 'taxonRemarks','taxonomicStatus','acceptedNameUsage','acceptedNameUsageID','parentNameUsage', 'scientificNameID','basisOfRecord'],
    'simple' =>  ['id','parent_id', 'author_id', 'scientificName', 'taxonRank', 'scientificNameAuthorship', 'namePublishedIn', 'parentName',
    'family','taxonRemarks','taxonomicStatus','scientificNameID','basisOfRecord'],
  ],
  'locations' => [
    'all' => ['id','basisOfRecord','locationName','adm_level','country_adm_level','x','y','startx','starty','distance_to_search','parent_id','parentName','higherGeography','footprintWKT','locationRemarks','decimalLatitude','decimalLongitude','georeferenceRemarks','geodeticDatum'],
    'simple' => ['id','basisOfRecord','locationName','adm_level','country_adm_level','x','y','startx','starty','distance_to_search','parent_id','parentName','higherGeography','footprintWKT','locationRemarks','decimalLatitude','decimalLongitude','georeferenceRemarks','geodeticDatum'],
  ],
  'individuals' => [
    'all' => ['id','basisOfRecord','organismID','recordedByMain','recordNumber','recordedDate','recordedBy','scientificName','taxonPublishedStatus','genus','family','identificationQualifier','identifiedBy','dateIdentified','identificationRemarks','identificationBiocollection','identificationBiocollectionReference',
    'locationName','higherGeography','decimalLatitude','decimalLongitude','georeferenceRemarks','locationParentName','x','y','gx','gy','angle','distance','organismRemarks','datasetName'],
    'simple' => ['id','basisOfRecord','organismID','recordedByMain','recordNumber','recordedDate','family','scientificName','identificationQualifier','identifiedBy','dateIdentified','locationName','locationParentName','decimalLatitude','decimalLongitude','x','y','gx','gy','angle','distance','datasetName'],
  ],
  'vouchers' => [
    'all' => ['id',"individual_id",'basisOfRecord','occurrenceID', 'organismID','collectionCode','catalogNumber','typeStatus','recordedByMain','recordNumber','recordedDate','recordedBy','scientificName','taxonPublishedStatus','genus','family','identificationQualifier','identifiedBy','dateIdentified','identificationRemarks', 'locationName','higherGeography','decimalLatitude','decimalLongitude','georeferenceRemarks','occurrenceRemarks','datasetName'],
    'simple' => ['id',"individual_id",'basisOfRecord','occurrenceID', 'organismID','collectionCode','catalogNumber','typeStatus','recordedByMain','recordNumber','recordedDate','recordedBy','scientificName','family','identificationQualifier','identifiedBy','dateIdentified','identificationRemarks', 'locationName','decimalLatitude','decimalLongitude','occurrenceRemarks','datasetName'],
  ],
  'measurements' => [
    'all' => ['id', 'basisOfRecord','measured_type', 'measured_id','measurementType', 'measurementValue','measurementUnit','measurementDeterminedDate', 'measurementDeterminedBy','measurementRemarks','resourceRelationship','resourceRelationshipID','relationshipOfResource','scientificName','family','datasetName','measurementMethod','sourceCitation','measurementLocationId','measurementParentId','decimalLatitude','decimalLongitude'],
    'simple' => ['id','basisOfRecord', 'measured_type', 'measured_id','measurementType', 'measurementValue','measurementUnit','measurementDeterminedDate','scientificName','datasetName','family','sourceCitation'],
  ],
  'odbtraits' => [
    'all' => ['id', 'type', 'typename','export_name','measurementType','measurementUnit', 'range_min', 'range_max','link_type','value_length','name','description','objects','measurementMethod','MeasurementTypeBibkeys','TaggedWith','categories'],
    'simple' => ['id', 'type', 'typename','export_name','unit', 'range_min', 'range_max','link_type','value_length','name','description','objects','measurementType','categories'],
    'exceptcategories' => ['id', 'type', 'typename','export_name','unit', 'range_min', 'range_max','link_type','value_length','name','description','objects','measurementType','MeasurementTypeBibkeys','TaggedWith'],
  ],
  'individuallocations' => [
    'all' => ['id',"individual_id",'basisOfRecord','occurrenceID','organismID','scientificName','family','recordedDate', 'locationName','higherGeography','decimalLatitude','decimalLongitude','georeferenceRemarks','x','y','angle','distance','minimumElevation','occurrenceRemarks','organismRemarks','datasetName'],
    'simple' => ['id',"individual_id",'basisOfRecord','occurrenceID','organismID','recordedDate', 'locationName','higherGeography','decimalLatitude','decimalLongitude','x','y','angle','distance','minimumElevation','occurrenceRemarks','scientificName','family','datasetName'],
  ],
  'bibreferences' => [
    'simple' => ['id', 'bibkey', 'year', 'author','title','doi','url','bibtex'],
    'all' => ['id', 'bibkey', 'year', 'author','title','doi','url','bibtex'],
  ],
  'biocollections' => [
    'all' => ['id', 'acronym', 'name', 'irn'],
    'simple' => ['id', 'acronym', 'name', 'irn'],
  ],
  'persons' => [
    'simple' => ['id', 'full_name', 'abbreviation', 'emailAddress', 'institution','notes'],
    'all' => ['id', 'full_name', 'abbreviation', 'emailAddress', 'institution','notes'],
  ],
  'media' => [
    'all' => ['id', 'model_type', 'model_id','basisOfRecord','recordedBy','recordedDate', 'dwcType', 'resourceRelationship', 'resourceRelationshipID', 'relationshipOfResource', 'scientificName', 'family', 'datasetName', 'accessRights', 'bibliographicCitation','license','file_name','file_url'],
    'simple' => ['id', 'model_type', 'model_id','basisOfRecord','recordedBy','recordedDate', 'dwcType', 'resourceRelationship', 'resourceRelationshipID', 'relationshipOfResource', 'scientificName', 'family', 'datasetName', 'accessRights', 'license','file_name'],
  ],
  'datasets' => [
    'all' => ['id', 'name', 'projectName','notes', 'privacyLevel','policy','description','measurements_count','contactEmail','taggedWidth'],
    'simple' => ['id', 'name', 'projectName','description','notes', 'contactEmail','taggedWidth'],
  ],
];
