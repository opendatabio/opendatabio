<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('biocollection_id')->unsigned()->nullable();
            $table->string('email')->nullable();
            $table->string('institution')->nullable();
            $table->string('message',500);
            $table->string('type');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('biocollection_id')->references('id')->on('biocollections')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('voucher_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id')->unsigned();
            $table->integer('voucher_id')->unsigned();
            $table->integer('status')->unsigned();
            $table->json('notes')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('request_id')->references('id')->on('requests')->onDelete('cascade');
            $table->foreign('voucher_id')->references('id')->on('vouchers')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('individual_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id')->unsigned();
            $table->integer('individual_id')->unsigned();
            $table->integer('biocollection_id')->unsigned();
            $table->integer('status')->unsigned();
            $table->json('notes')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('request_id')->references('id')->on('requests')->onDelete('cascade');
            $table->foreign('individual_id')->references('id')->on('individuals')->onDelete('cascade');
            $table->foreign('biocollection_id')->references('id')->on('biocollections')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('voucher_request');
        Schema::dropIfExists('individual_request');
        Schema::dropIfExists('requests');
        Schema::enableForeignKeyConstraints();
    }
}
