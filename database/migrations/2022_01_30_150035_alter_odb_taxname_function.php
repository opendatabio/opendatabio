<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterOdbTaxnameFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       DB::unprepared('DROP FUNCTION IF EXISTS odb_txname');
       DB::unprepared("CREATE FUNCTION odb_txname( name VARCHAR(191), level INT, parent_id INT,modifier INT,includepart INT)
       RETURNS VARCHAR(191) DETERMINISTIC
       BEGIN
             DECLARE p_name, p_p_name, part,s_modifier VARCHAR(191);
             DECLARE p_parent_id INT;
             DECLARE p_unpublished INT;

             SELECT odb_modifier(modifier) INTO s_modifier;
             IF level < 200 THEN
               IF (modifier>0) THEN
                 RETURN CONCAT(name,' ',s_modifier);
               ELSE
                 RETURN name;
               END IF;
             END IF;
             IF level = 210 THEN
             SELECT IF(name LIKE '% %',1,0) INTO p_unpublished;
             IF p_unpublished=0 THEN
               SELECT taxons.name INTO p_name FROM taxons WHERE id = parent_id;
               /* case is cf. or aff. */
               IF (modifier IN(3,4)) THEN
                 RETURN CONCAT(p_name,' ',s_modifier,' ',name);
               ELSE
                 IF (modifier>0) THEN
                 RETURN CONCAT(p_name, ' ', name,' ',s_modifier);
                 ELSE
                 RETURN CONCAT(p_name, ' ', name);
                 END IF;
               END IF;
             ELSE
               IF (modifier>0) THEN
                 RETURN CONCAT(name,' ',s_modifier);
               ELSE
                 RETURN name;
               END IF;
             END IF;
             END IF;

             SELECT taxons.name, taxons.parent_id INTO p_name, p_parent_id FROM taxons WHERE id = parent_id;
             SELECT taxons.name INTO p_p_name FROM taxons WHERE id = p_parent_id;
             SELECT CASE level WHEN 220 THEN 'subsp.' WHEN 240 THEN 'var.' WHEN 270 THEN 'f.' ELSE '' END INTO part;
             IF (includepart=1) THEN
                IF (modifier IN(3,4)) THEN
                RETURN CONCAT_WS(' ', p_p_name, s_modifier, p_name, part, name);
                ELSE
                  IF (modifier>0) THEN
                    RETURN CONCAT_WS(' ', p_p_name, p_name, part, name, s_modifier);
                  ELSE
                    RETURN CONCAT_WS(' ', p_p_name, p_name, part, name);
                  END IF;
                END IF;
              ELSE
                IF (modifier IN(3,4)) THEN
                RETURN CONCAT_WS(' ', p_p_name, s_modifier, p_name, name);
                ELSE
                  IF (modifier>0) THEN
                    RETURN CONCAT_WS(' ', p_p_name, p_name, name, s_modifier);
                  ELSE
                    RETURN CONCAT_WS(' ', p_p_name, p_name, name);
                  END IF;
                END IF;
              END IF;
        END
       ");

     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       DB::unprepared('DROP FUNCTION IF EXISTS odb_txname');
       DB::unprepared("CREATE FUNCTION odb_txname( name VARCHAR(191), level INT, parent_id INT,modifier INT)
       RETURNS VARCHAR(191) DETERMINISTIC
       BEGIN
      DECLARE p_name, p_p_name, part,s_modifier VARCHAR(191);
      DECLARE p_parent_id INT;
      DECLARE p_unpublished INT;

      SELECT odb_modifier(modifier) INTO s_modifier;
      IF level < 200 THEN
        IF (modifier>0) THEN
          RETURN CONCAT(name,' ',s_modifier);
        ELSE
          RETURN name;
        END IF;
      END IF;
      IF level = 210 THEN
      SELECT IF(name LIKE '% %',1,0) INTO p_unpublished;
      IF p_unpublished=0 THEN
        SELECT taxons.name INTO p_name FROM taxons WHERE id = parent_id;
        /* case is cf. or aff. */
        IF (modifier IN(3,4)) THEN
          RETURN CONCAT(p_name,' ',s_modifier,' ',name);
        ELSE
          IF (modifier>0) THEN
          RETURN CONCAT(p_name, ' ', name,' ',s_modifier);
          ELSE
          RETURN CONCAT(p_name, ' ', name);
          END IF;
        END IF;
      ELSE
        IF (modifier>0) THEN
          RETURN CONCAT(name,' ',s_modifier);
        ELSE
          RETURN name;
        END IF;
      END IF;
      END IF;

      SELECT taxons.name, taxons.parent_id INTO p_name, p_parent_id FROM taxons WHERE id = parent_id;
      SELECT taxons.name INTO p_p_name FROM taxons WHERE id = p_parent_id;
      SELECT CASE level WHEN 220 THEN 'subsp.' WHEN 240 THEN 'var.' WHEN 270 THEN 'f.' ELSE '' END INTO part;
      IF (modifier IN(3,4)) THEN
        RETURN CONCAT_WS(' ', p_p_name, s_modifier, p_name, part, name);
      ELSE
        IF (modifier>0) THEN
        RETURN CONCAT_WS(' ', p_p_name, p_name, part, name, s_modifier);
        ELSE
        RETURN CONCAT_WS(' ', p_p_name, p_name, part, name);
        END IF;
      END IF;
      END
       ");
     }
 }
