<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "\n";
        echo "\e[0;31;43m Creating FormTasks for data remeasurements using forms\e[0m\n";      

        Schema::create('form_tasks', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('form_id')->unsigned();
            $table->foreign('form_id')->references('id')->on('forms');            
            $table->json('metadata')->nullable();
            $table->integer('progress_max');
            $table->integer('progress');
            $table->string('status');
            $table->timestamps();
        });
        Schema::create('form_task_user', function (Blueprint $table) {
            $table->bigInteger('form_task_id')->unsigned();
            $table->foreign('form_task_id')->references('id')->on('form_tasks');            
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');                        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_task_user');
        Schema::dropIfExists('form_tasks');

    }

};
