<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixOdbCollectorNumberFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::unprepared('DROP FUNCTION IF EXISTS odb_collector_number');
      DB::unprepared("CREATE FUNCTION odb_collector_number(individual_id INT, voucher_id INT, tag VARCHAR(191))
        RETURNS VARCHAR(191) DETERMINISTIC
        BEGIN
          DECLARE colnum, person,result,type VARCHAR(191);
          DECLARE hascollector INT(10) DEFAULT 0;
          DECLARE object_id INT(10) DEFAULT 0;
          SET object_id = individual_id;
          SELECT individuals.tag INTO colnum FROM individuals WHERE id=individual_id;
          SELECT persons.abbreviation into person FROM collectors JOIN persons ON collectors.person_id=persons.id WHERE collectors.object_id=object_id AND collectors.object_type LIKE '%Individual'  AND collectors.main=1;
          IF (voucher_id>0) THEN
              SELECT COUNT(*) INTO hascollector FROM collectors WHERE collectors.object_id=voucher_id AND collectors.object_type LIKE '%Voucher'  AND main=1;
              IF (hascollector>0) THEN
                  SET colnum = tag;
                  SET object_id = voucher_id;
                  SELECT persons.abbreviation into person FROM collectors JOIN persons ON collectors.person_id=persons.id WHERE collectors.object_id=voucher_id AND collectors.object_type LIKE  '%Voucher'   AND collectors.main=1;
              END IF;              
          END IF;
          SET result = CONCAT(person,' #',colnum);
          RETURN result;
        END;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::unprepared('DROP FUNCTION IF EXISTS odb_collector_number');
      DB::unprepared("CREATE FUNCTION odb_collector_number(individual_id INT, voucher_id INT, tag VARCHAR(191))
        RETURNS VARCHAR(191) DETERMINISTIC
        BEGIN
          DECLARE colnum, person,result,type VARCHAR(191);
          DECLARE hascollector INT(10) DEFAULT 0;
          DECLARE object_id INT(10) DEFAULT 0;
          SET object_id = individual_id;
          SET colnum = tag;
          SELECT persons.abbreviation into person FROM collectors JOIN persons ON collectors.person_id=persons.id WHERE collectors.object_id=object_id AND collectors.object_type LIKE \"%Individual\"  AND collectors.main=1;
          IF (voucher_id>0) THEN
              SELECT 1 INTO hascollector FROM collectors WHERE object_id=voucher_id AND object_type LIKE \"%Voucher\" AND main=1;
              IF (hascollector=1) THEN
                  SET colnum = tag;
                  SET object_id = voucher_id;
                  SELECT persons.abbreviation into person FROM collectors JOIN persons ON collectors.person_id=persons.id WHERE collectors.object_id=voucher_id AND collectors.object_type LIKE  \"%Voucher\"  AND collectors.main=1;
              ELSE
                 SELECT individuals.tag INTO colnum FROM individuals WHERE id=individual_id;
              END IF;
          END IF;
          SET result = CONCAT(person,' #',colnum);
          RETURN result;
        END;");
    }
}
