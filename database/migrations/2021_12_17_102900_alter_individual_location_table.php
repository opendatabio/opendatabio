<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\IndividualLocation;
use App\Models\Location;

class AlterIndividualLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('individual_location', function (Blueprint $table) {
        $table->point('global_position')->after('relative_position')->nullable();
      });
      //update 1
      $sql = "UPDATE individual_location,locations SET global_position=locations.geom WHERE (individual_location.relative_position IS NULL) AND locations.adm_level=999 AND individual_location.location_id=locations.id;";
      DB::statement($sql);
      $sql = "UPDATE individual_location,locations SET global_position=ST_Centroid(locations.geom) WHERE locations.adm_level<100 AND individual_location.location_id=locations.id;";
      DB::statement($sql);
      //remaining must use manual conversion
      $locations  = IndividualLocation::withoutGlobalScopes()->whereNull('global_position')->cursor();
      foreach($locations as $location) {
          $x = $location->x;
          $y = $location->y;
          $distance = $location->distance;
          $angle = $location->angle;
          $newgeom = Location::calculateGlobalPosition($x,$y,$angle,$distance,$location->location_id);
          DB::statement("UPDATE individual_location SET global_position=ST_GeomFromText('".$newgeom."') WHERE id=".$location->id);
      }
      DB::statement('SET FOREIGN_KEY_CHECKS=0;');
      DB::statement("ALTER TABLE `individual_location` CHANGE `global_position` `global_position` POINT NOT NULL;");
      DB::statement("CREATE SPATIAL INDEX spatial_index ON individual_location (global_position);");
      DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
