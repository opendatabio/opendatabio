<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Location;
use CodeInc\StripAccents\StripAccents;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->string('country_code')->nullable()->after('geojson');            
        });
        //get the countries and save the codes
        $sql ="SELECT Child.id,Parent.name FROM locations as Parent, locations as Child WHERE Child.lft BETWEEN Parent.lft AND Parent.rgt AND Parent.adm_level=2";
        $crts = DB::select($sql);
        $crcodes = Lang::get("countries",[],'en');
        $crcodes = array_map('mb_strtolower', $crcodes);
        $crcodes2 = Lang::get("countries",[],'pt');
        $crcodes2 = array_map('mb_strtolower', $crcodes2);
        echo "\e[0;31;43m Will update locations table. Wait!!! May take some time!\e[0m\n";      
        $nlocs = count($crts);
        //$crcodes = StripAccents::strip($crcodes);
        foreach($crts as $k => $loc) {            
            $cr = StripAccents::strip(mb_strtolower($loc->name));
            $cr2 = mb_strtolower($loc->name);
            $ak = array_search($cr,$crcodes);
            if (!$ak) {
              $ak = array_search($cr,$crcodes2);
            }            
            if (!$ak) {
                $ak = array_search($cr2,$crcodes);              
            }
            if (!$ak) {
                $ak = array_search($cr2,$crcodes2);              
            }
            if ($ak) {
                $theloc = Location::find($loc->id);
                $theloc->update(['country_code'=>$ak]);
                $theloc->save();
            }
            if ($k % 1000 === 0) {
                echo "\n".$k." of ".$nlocs;
                //echo ".";
            }            
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropColumn('country_code');   
        });
    }
};
