<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
Use App\Models\Project;
Use App\Models\Language;
Use App\Models\UserTranslation;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->string('acronym')->nullable()->after('id');
            $table->json('pages')->nullable()->after('acronym');
            $table->json('urls')->nullable()->after('pages');            
        });
        $projects = Project::all();
        foreach($projects as $project) {
            $name = DB::select(DB::raw('SELECT name FROM projects WHERE id='.$project->id));
            $name = $name[0] ? $name[0]->name : null;
            if ($name) {
                $project->acronym = $name;
            }
            $project->pages = $project->details ? ['pt-br' => $project->details ] : null;
            $translation = DB::select(DB::raw('SELECT title FROM projects WHERE id='.$project->id));
            $translation = $translation[0] ? $translation[0]->title : null;
            if ($translation) {
                $key = Language::where('code','pt-br')->get()->first()->id;
                $project->setTranslation(UserTranslation::NAME, $key, $translation);
            }
            $translation = DB::select(DB::raw('SELECT description FROM projects WHERE id='.$project->id));
            $translation = $translation[0] ? $translation[0]->description : null;
            if ($translation) {
                $project->setTranslation(UserTranslation::DESCRIPTION, $key, $translation);
            }
            $project->save();            
        }
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('name');               
            $table->dropColumn('description');               
            $table->dropColumn('privacy');         
            $table->dropColumn('details');         
            $table->dropColumn('policy');         
            $table->dropColumn('license');         
            $table->dropColumn('title');         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('pages');               
            $table->dropColumn('urls');               
            $table->dropColumn('acronym');               
        });
    }
};
