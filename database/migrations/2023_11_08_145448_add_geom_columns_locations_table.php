<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "\n\e[0;31;43m Will update locations table. Wait!!! May take some time! Do not cancel!\e[0m\n";      

        Schema::table('locations', function (Blueprint $table) {
            $table->string('geom_type')->nullable()->after('country_code');
            $table->string('start_point')->nullable()->after('geom_type');
            $table->string('centroid_raw')->nullable()->after('start_point');
            $table->string('area_raw')->nullable()->after('centroid_raw');
        });
        echo "\e[0;31;43m Added fields\e[0m\n";      

        DB::statement(DB::raw("UPDATE locations SET centroid_raw=centroid_raw=(CASE
        WHEN locations.adm_level=999 THEN ST_AsText(geom)
        ELSE ST_AsText(ST_Centroid(geom)) END)"));
        echo "Updated centroids\n";      
        DB::statement(DB::raw("UPDATE locations SET geom_type=ST_GeometryType(geom)"));
        echo "Updated geom_type\n";      
        DB::statement(DB::raw("UPDATE locations SET start_point=(CASE
        WHEN locations.adm_level=999 THEN ST_AsText(geom)
        WHEN locations.adm_level=101 THEN ST_AsText(ST_StartPoint(geom))
        WHEN ST_GeometryType(geom) LIKE 'MULTIPOLYGON' THEN ST_AsText(ST_Centroid(geom))
        ELSE ST_AsText(ST_StartPoint(St_ExteriorRing(geom)))
        END)"));
        echo "Updated start_point\n";
        DB::statement(DB::raw("UPDATE locations SET area_raw=ST_Area(geom) WHERE ST_GeometryType(geom) LIKE '%POLYGON'"));
        echo "Updated area_raw\n";      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropColumn('geom_type');   
            $table->dropColumn('start_point');   
            $table->dropColumn('centroid_raw');   
            $table->dropColumn('area_raw');   
        });
    }
};
