<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\TraitUnit;
use App\Models\Language;
use App\Models\UserTranslation;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        Schema::create('methods', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('trait_id');
            $table->foreign('trait_id')->references('id')->on('traits')->onDelete('restrict');
            $table->unsignedInteger('bibreference_id')->nullable();
            $table->foreign('bibreference_id')->references('id')->on('bib_references')->onDelete('restrict');
            $table->string('short_name');
            $table->text('notes')->nullable();
            $table->timestamps();
        });
        */
        echo "\e[0;31;43m Updating the measurements table with location_id. Wait, may take some time!\e[0m\n";      
        Schema::table('measurements', function (Blueprint $table) {
            $table->integer('location_id')->unsigned()->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('restrict');                  
            //$table->unsignedBigInteger('method_id')->nullable();
            //$table->foreign('method_id')->references('id')->on('methods')->onDelete('restrict');                  
        });

        Schema::create('trait_units', function (Blueprint $table) {
            $table->id();
            $table->string('unit');
            $table->timestamps();
            $table->unique('unit');
        });
        echo "\e[0;31;43m Creating the TraitUnit table and moving current Traits unit to it! Wait\e[0m\n";      
        DB::statement(DB::raw("INSERT INTO trait_units (unit) (SELECT DISTINCT unit FROM traits WHERE unit IS NOT NULL ORDER BY unit)"));                    
        $units = TraitUnit::all();
        $langs = Language::all();
        foreach($units as $unit) {
            $uu = [];
            foreach( $langs as $lang ) {
                $unit->setTranslation(UserTranslation::NAME, $lang->id, $unit->unit);                
            }                        
        }            
        Schema::table('traits', function (Blueprint $table) {
            $table->unsignedBigInteger('trait_unit_id')->nullable();
            $table->foreign('trait_unit_id')->references('id')->on('trait_units')->onDelete('restrict');                  
        });

        DB::statement(DB::raw("UPDATE traits as tr,trait_units as u SET tr.trait_unit_id=u.id WHERE tr.unit=u.unit"));
        
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('measurements', function (Blueprint $table) {            
            
            $table->dropForeign(['location_id']);      
            $table->dropColumn('location_id');
            
        });

        Schema::table('traits', function (Blueprint $table) {
            $table->dropForeign(['trait_unit_id']);            
            $table->dropColumn(['trait_unit_id']);            
        });

        Schema::dropIfExists('trait_units');
        
    }
};
