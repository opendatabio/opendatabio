<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFunctionOdbSortableAdmLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      /* THIS FUNCTION IS FOR THE LOCATION MODEL TO GET THE RELATED LOCATION IN THE PROPER PLACE */
      DB::unprepared('DROP FUNCTION IF EXISTS odb_ordered_admlevel');
      DB::unprepared("CREATE FUNCTION odb_ordered_admlevel(adm_level INT,parent_id INT)
        RETURNS INT(5) DETERMINISTIC
        BEGIN
          DECLARE p_name VARCHAR(191);
          DECLARE plevel INT;
          SET plevel=adm_level;
          IF (adm_level>96 AND adm_level<100) THEN
            IF (parent_id>0) THEN
              SELECT locations.adm_level into plevel FROM locations WHERE locations.id=parent_id;
            ELSE
              SET plevel = 0;
            END IF;
          END IF;
          RETURN plevel;
        END;
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::unprepared('DROP FUNCTION IF EXISTS odb_ordered_admlevel');
    }
}
