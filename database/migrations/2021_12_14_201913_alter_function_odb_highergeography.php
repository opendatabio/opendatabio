<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFunctionOdbHighergeography extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::unprepared('DROP FUNCTION IF EXISTS odb_highergeography');
      DB::unprepared("CREATE FUNCTION odb_highergeography(location_id INT,individual_id INT)
        RETURNS VARCHAR(191) DETERMINISTIC
        BEGIN
          DECLARE p_name VARCHAR(191);
          DECLARE lft INT;
          IF (individual_id>0) THEN
            SELECT locations.lft into lft FROM individual_location as ind JOIN locations ON locations.id=ind.location_id WHERE ind.individual_id=individual_id LIMIT 0,1;
          ELSE
            SELECT locations.lft into lft FROM locations WHERE id=location_id;
          END IF;
          SELECT GROUP_CONCAT(locations.name ORDER BY locations.adm_level DESC SEPARATOR ' < ') into p_name FROM locations WHERE locations.lft <= lft AND locations.rgt >= lft AND locations.adm_level>0 AND locations.adm_level<>999;
          RETURN p_name;
        END;
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::unprepared('DROP FUNCTION IF EXISTS odb_highergeography');
      DB::unprepared("CREATE FUNCTION odb_highergeography(location_id INT,individual_id INT)
        RETURNS VARCHAR(191) DETERMINISTIC
        BEGIN
          DECLARE p_name VARCHAR(191);
          DECLARE lft INT;
          IF (individual_id>0) THEN
            SELECT locations.lft into lft FROM individual_location as ind JOIN locations ON locations.id=ind.location_id WHERE ind.individual_id=individual_id LIMIT 0,1;
          ELSE
            SELECT locations.lft into lft FROM locations WHERE id=location_id;
          END IF;
          SELECT GROUP_CONCAT(locations.name ORDER BY locations.id DESC SEPARATOR ' < ') into p_name FROM locations WHERE locations.lft <= lft AND locations.rgt >= lft AND locations.adm_level>0 AND locations.adm_level<>999;
          RETURN p_name;
        END;
      ");
    }
}
