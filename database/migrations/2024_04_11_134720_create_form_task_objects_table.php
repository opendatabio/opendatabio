<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "\n";
        echo "\e[0;31;43m Creating FormTaskObjects for data remeasurements using forms\e[0m\n";      

        Schema::create('form_task_objects', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('form_task_id')->unsigned();
            $table->foreign('form_task_id')->references('id')->on('form_tasks');
            $table->integer('measured_id')->unsigned();
            $table->string('measured_type');
            $table->string('label');
            $table->string('search_label');
            $table->tinyInteger('status')->default(0);
            $table->json('metadata')->nullable();
            $table->index(['measured_id', 'measured_type']);
            $table->index('label');
            $table->index('search_label');
            $table->index('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_task_objects');
    }
};
