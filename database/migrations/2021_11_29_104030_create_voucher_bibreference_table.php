<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucherBibreferenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('voucher_bibreference', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('voucher_id')->unsigned();
        $table->foreign('voucher_id')->references('id')->on('vouchers');
        $table->integer('bib_reference_id')->unsigned();
        $table->foreign('bib_reference_id')->references('id')->on('bib_references');
        $table->timestamps();
        $table->unique(['bib_reference_id', 'voucher_id']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_bibreference');
    }
}
