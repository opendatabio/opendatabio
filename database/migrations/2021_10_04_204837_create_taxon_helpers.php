<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxonHelpers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      DB::unprepared('DROP FUNCTION IF EXISTS odb_txname');
      DB::unprepared("CREATE FUNCTION odb_txname( name VARCHAR(191), level INT, parent_id INT,modifier INT)
      RETURNS VARCHAR(191) DETERMINISTIC
      BEGIN
      DECLARE p_name, p_p_name, part,s_modifier VARCHAR(191);
      DECLARE p_parent_id INT;
      DECLARE p_unpublished INT;

      SELECT odb_modifier(modifier) INTO s_modifier;
      IF level < 200 THEN
        IF (modifier>0) THEN
          RETURN CONCAT(name,' ',s_modifier);
        ELSE
          RETURN name;
        END IF;
      END IF;
      IF level = 210 THEN
      SELECT IF(name LIKE '% %',1,0) INTO p_unpublished;
      IF p_unpublished=0 THEN
        SELECT taxons.name INTO p_name FROM taxons WHERE id = parent_id;
        IF (modifier>0) THEN
          RETURN CONCAT(p_name,' ',s_modifier,' ',name);
        ELSE
          RETURN CONCAT(p_name, ' ', name);
        END IF;
      ELSE
        IF (modifier>0) THEN
          RETURN CONCAT(name,' ',s_modifier);
        ELSE
          RETURN name;
        END IF;
      END IF;
      END IF;

      SELECT taxons.name, taxons.parent_id INTO p_name, p_parent_id FROM taxons WHERE id = parent_id;
      SELECT taxons.name INTO p_p_name FROM taxons WHERE id = p_parent_id;
      SELECT CASE level WHEN 220 THEN 'subsp.' WHEN 240 THEN 'var.' WHEN 270 THEN 'f.' ELSE '' END INTO part;
      IF (modifier>0) THEN
          RETURN CONCAT_WS(' ', p_p_name, s_modifier, p_name, part, name);
      ELSE
          RETURN CONCAT_WS(' ', p_p_name, p_name, part, name);
      END IF;
      END;
      ");

      DB::unprepared('DROP FUNCTION IF EXISTS odb_collector_number');
      DB::unprepared("CREATE FUNCTION odb_collector_number(individual_id INT, voucher_id INT, tag VARCHAR(191))
        RETURNS VARCHAR(191) DETERMINISTIC
        BEGIN
          DECLARE colnum, person,result,type VARCHAR(191);
          DECLARE hascollector INT(10) DEFAULT 0;
          DECLARE object_id INT(10) DEFAULT 0;
          SET object_id = individual_id;
          SET colnum = tag;
          SELECT persons.abbreviation into person FROM collectors JOIN persons ON collectors.person_id=persons.id WHERE collectors.object_id=object_id AND collectors.object_type LIKE \"%Individual\"  AND collectors.main=1;
          IF (voucher_id>0) THEN
              SELECT 1 INTO hascollector FROM collectors WHERE object_id=voucher_id AND object_type LIKE \"%Voucher\" AND main=1;
              IF (hascollector=1) THEN
                  SET colnum = tag;
                  SET object_id = voucher_id;
                  SELECT persons.abbreviation into person FROM collectors JOIN persons ON collectors.person_id=persons.id WHERE collectors.object_id=voucher_id AND collectors.object_type LIKE  \"%Voucher\"  AND collectors.main=1;
              ELSE
                 SELECT individuals.tag INTO colnum FROM individuals WHERE id=individual_id;
              END IF;
          END IF;
          SET result = CONCAT(person,' #',colnum);
          RETURN result;
        END;");

      DB::unprepared('DROP FUNCTION IF EXISTS odb_modifier');
      DB::unprepared("CREATE FUNCTION odb_modifier(modifier INT)
        RETURNS VARCHAR(191) DETERMINISTIC
        BEGIN
          DECLARE result VARCHAR(20);
          SELECT (CASE
            WHEN modifier = 1 THEN 's.s.'
            WHEN modifier = 2 THEN 's.l.'
            WHEN modifier = 3 THEN 'cf.'
            WHEN modifier = 4 THEN 'aff.'
            WHEN modifier = 5 THEN 'vel aff.'
            ELSE ''
          END) into result;
          RETURN result;
        END;");

      DB::unprepared('DROP FUNCTION IF EXISTS odb_identification');
      DB::unprepared("CREATE FUNCTION odb_identification(individual_id INT)
        RETURNS VARCHAR(191) DETERMINISTIC
        BEGIN
          DECLARE result VARCHAR(191);
          SELECT odb_txname(taxons.name, taxons.level, taxons.parent_id, identifications.modifier) into result FROM identifications JOIN taxons ON taxons.id=identifications.taxon_id WHERE object_id=individual_id;
          RETURN result;
        END;");

      DB::unprepared('DROP FUNCTION IF EXISTS odb_txparent');
      DB::unprepared("CREATE FUNCTION odb_txparent(lft INT, level INT)
        RETURNS VARCHAR(191) DETERMINISTIC
        BEGIN
          DECLARE p_name VARCHAR(191);
          SELECT odb_txname(taxons.name,taxons.level,taxons.parent_id,0) INTO p_name FROM taxons WHERE taxons.lft <= lft AND taxons.rgt >= lft AND taxons.level=level;
          RETURN p_name;
        END;
      ");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
