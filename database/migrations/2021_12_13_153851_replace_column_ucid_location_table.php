<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/* remove the uc_id column that is no longer used,
since the location_related pivot table was created

rename this column to annotate when a plot or transect is entered as point geom
in this case store the spatial (polygon geometry in table) and add 1 to this column

*/
class ReplaceColumnUcidLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('locations', function(Blueprint $table) {
           $table->dropForeign('locations_uc_id_foreign');
           $table->dropColumn('uc_id');
           $table->integer('as_point')->after('geom')->default(0);
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
