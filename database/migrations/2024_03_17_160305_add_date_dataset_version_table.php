<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::disableForeignKeyConstraints();
        Schema::table('dataset_versions', function (Blueprint $table) {
            $table->dropForeign('dataset_versions_dataset_id_foreign');
            $table->dropUnique('version');
            $table->string('uuid')->after('id')->nullable();
            $table->date('date')->nullable()->after('version');
            $table->foreign('dataset_id')->references('id')->on('datasets')->onDelete('restrict');
            $table->unique(['dataset_id','version'],'dataset_version');
        });
        $prefix = config('app.uuid_prefix');
        DB::statement(DB::raw("UPDATE dataset_versions SET uuid=CONCAT('".$prefix."-',UUID())"));
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dataset_versions', function (Blueprint $table) {
            // Drop new columns
            $table->dropColumn('uuid');
            $table->dropColumn('date');
            $table->dropForeign('dataset_versions_dataset_id_foreign');
            $table->dropUnique('dataset_version');
            // Recreate old index
            $table->unique('dataset_id','version');
        });
    }
};
