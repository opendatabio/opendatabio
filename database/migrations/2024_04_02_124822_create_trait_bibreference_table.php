<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {             
        /* PIVOT TABLES FOR MANY TO MANY RELATIONS SHOULD NOT HAVE AN ID
        OTHERWISE LARAVEL MAY MIX THINGS*/   
        echo "\e[0;31;43m Creating pivot tables for traits bireferences and traits tags!\e[0m\n";      

        Schema::create('trait_bibreference', function (Blueprint $table) {
            $table->integer('trait_id')->unsigned();
            $table->foreign('trait_id')->references('id')->on('traits');
            $table->integer('bibreference_id')->unsigned();
            $table->foreign('bibreference_id')->references('id')->on('bib_references');
        });
        Schema::create('trait_tag', function (Blueprint $table) {
            $table->integer('trait_id')->unsigned();
            $table->foreign('trait_id')->references('id')->on('traits');
            $table->integer('tag_id')->unsigned();
            $table->foreign('tag_id')->references('id')->on('tags');
        });    
        DB::unprepared('INSERT INTO trait_bibreference (trait_id,bibreference_id) SELECT id,bibreference_id FROM traits WHERE bibreference_id IS NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trait_bibreference');
        Schema::dropIfExists('trait_tag');
    }
    
};
