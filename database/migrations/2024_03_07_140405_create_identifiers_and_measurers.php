<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('identification_person', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('person_id')->unsigned();
            $table->foreign('person_id')->references('id')->on('persons');
            $table->integer('identification_id')->unsigned();
            $table->foreign('identification_id')->references('id')->on('identifications');
            $table->unique(['identification_id', 'person_id']);
        });
        DB::unprepared('INSERT INTO identification_person (person_id,identification_id) SELECT person_id,id FROM identifications');
        Schema::create('measurement_person', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('person_id')->unsigned();
            $table->foreign('person_id')->references('id')->on('persons');
            $table->integer('measurement_id')->unsigned();
            $table->foreign('measurement_id')->references('id')->on('measurements');
            $table->unique(['measurement_id', 'person_id']);
        });
        DB::unprepared('INSERT INTO measurement_person (person_id,measurement_id) SELECT person_id,id FROM measurements');        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identification_person');
        Schema::dropIfExists('measurement_person');

    }


};
   