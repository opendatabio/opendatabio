<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixOdbFunctionIdentification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::unprepared('DROP FUNCTION IF EXISTS odb_identification');
      DB::unprepared("CREATE FUNCTION odb_identification(individual_id INT,includepart INT)
        RETURNS VARCHAR(191) DETERMINISTIC
        BEGIN
          DECLARE result VARCHAR(191);
          SELECT odb_txname(taxons.name, taxons.level, taxons.parent_id, identifications.modifier,includepart) into result FROM identifications JOIN taxons ON taxons.id=identifications.taxon_id WHERE object_id=individual_id;
          RETURN result;
        END;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::unprepared('DROP FUNCTION IF EXISTS odb_identification');
      DB::unprepared("CREATE FUNCTION odb_identification(individual_id INT)
        RETURNS VARCHAR(191) DETERMINISTIC
        BEGIN
          DECLARE result VARCHAR(191);
          SELECT odb_txname(taxons.name, taxons.level, taxons.parent_id, identifications.modifier) into result FROM identifications JOIN taxons ON taxons.id=identifications.taxon_id WHERE object_id=individual_id;
          RETURN result;
        END;");
    }
}
