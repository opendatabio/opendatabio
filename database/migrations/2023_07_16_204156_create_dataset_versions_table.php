<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dataset_versions', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('dataset_id');
            $table->foreign('dataset_id')->references('id')->on('datasets')->onDelete('restrict');
            $table->string('version');
            $table->integer('dataset_access');
            $table->string('license')->nullable();
            $table->string('license_version')->nullable();
            $table->text('policy')->nullable();
            $table->text('citation')->nullable();
            $table->text('notes')->nullable();
            $table->json('filters')->nullable();
            $table->json('metadata')->nullable();
            $table->timestamps();
            $table->unique('dataset_id','version'); // obs: this mistake was corrected on migration 2024_03_17_160305_add_date_dataset_version_table.php
        });

        Schema::create('dataset_version_person', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('dataset_version_id');
            $table->foreign('dataset_version_id')->references('id')->on('dataset_versions')->onDelete('restrict');
            $table->unsignedInteger('person_id');
            $table->foreign('person_id')->references('id')->on('persons')->onDelete('restrict');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dataset_version_person', function (Blueprint $table) {
            $table->dropForeign(['dataset_version_id']);
            $table->dropForeign(['person_id']);
        });
        Schema::table('dataset_versions', function (Blueprint $table) {
            $table->dropForeign(['dataset_id']);
        });
        Schema::dropIfExists('dataset_versions');
        Schema::dropIfExists('dataset_version_person');

    }
};
