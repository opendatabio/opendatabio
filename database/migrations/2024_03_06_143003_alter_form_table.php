<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forms', function (Blueprint $table) {
            $table->string('measured_type')->nullable()->change();            
            $table->json('metadata')->nullable();
        });
        Schema::table('form_traits', function (Blueprint $table) {
            $table->integer('order')->nullable()->change();            
        });
        Schema::table('traits', function (Blueprint $table) {
            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('traits')->onDelete('restrict');      
        });
        Schema::table('measurements', function (Blueprint $table) {
            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('measurements')->onDelete('restrict');      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
    */

    public function down()
    {
        Schema::table('forms', function (Blueprint $table) {
            $table->string('measured_type')->nullable(false)->change();            
            $table->dropColumn('metadata');

        });
        Schema::table('form_traits', function (Blueprint $table) {
            $table->integer('order')->nullable(false)->change();            
        });
        Schema::table('traits', function (Blueprint $table) {
            $table->dropForeign(['parent_id']);
            $table->dropColumn('parent_id');
        });
        Schema::table('measurements', function (Blueprint $table) {
            $table->dropForeign(['parent_id']);
            $table->dropColumn('parent_id');
        });
    }
};
