<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOdbHighertaxonFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::unprepared('DROP FUNCTION IF EXISTS odb_highertaxon');
      DB::unprepared("CREATE FUNCTION odb_highertaxon(lft INT)
        RETURNS VARCHAR(191) DETERMINISTIC
        BEGIN
          DECLARE p_name VARCHAR(191);
          SELECT GROUP_CONCAT(taxons.name ORDER BY level DESC SEPARATOR ' ') into p_name FROM taxons WHERE taxons.lft <= lft AND taxons.rgt >= lft;
          RETURN p_name;
        END;
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::unprepared('DROP FUNCTION IF EXISTS odb_highertaxon');
    }
}
